<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balita extends Model
{
    protected $table = 'spm_balita';
    protected $fillable = [
        'id',
        'nik',
        'status_balita'];
}
