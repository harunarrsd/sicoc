<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BayiBaruLahir extends Model
{
    protected $table = 'spm_bayi_baru_lahir';
    protected $fillable = [
        'id',
        'nik',
        'status_kelahiran'];
}
