<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DM extends Model
{
    protected $table = 'spm_diabetes_melitus';
    protected $fillable = [
        'id', 'nik','status','tanggal_menderita'
    ];
}
