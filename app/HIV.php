<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HIV extends Model
{
    protected $table = 'spm_risiko_hiv';
    protected $fillable = [
        'id', 'nik','status','tanggal_menderita'
    ];
}
