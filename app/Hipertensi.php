<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hipertensi extends Model
{
    protected $table = 'spm_hipertensi';
    protected $fillable = [
        'id', 'nik', 'status','tanggal_menderita'
    ];
}
