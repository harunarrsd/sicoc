<?php

namespace App\Http\Controllers;
use DB;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\User;
use App\Warga;
use App\Pelayanan;
use App\IbuBersalin;
use App\IbuHamil;

use Auth;
use Hash;

class APIController extends Controller
{
    //Auth
    public function AuthLogin(Request $request){
        $message ="error";
        $token_id = str_random(16);
        $token_key = str_random(30);
        $passbcrip = User::where('username',$request->username)->get();
        foreach($passbcrip as $user){
            if(Hash::check($request->password,$user->password)){
                $message = "success";
                $user_id = $user->id;
                break;
            }
        }
        if($message == "success"){
            return response()->json([
                'message'=>'success',
                'status'=>200,
                'data'=> ([
                    'access_token' => $token_key,
                    'user_id' => $user_id
                    ])
            ]);
        }
        else{
            return response()->json([
                'message'=>'error',
                'status'=>404,
                'data'=> ([
                    'access_token' => null,
                    'user_id' => null
                    ])
            ]);
        }
    }

    //Ibu Hamil
    public function dataIbuHamil(){
        $data_ibu_hamil = [];
        $token_id = str_random(16);
        $token_key = str_random(30);
        $total_ibu_hamil = 0;
        $total_sesuai_spm = 0;
        $total_tsesuai_spm = 0; 
        $total_tahap_pelayanan = 0;
        $data = DB::table('spm_ibu_hamil')
        ->join('warga','spm_ibu_hamil.nik','=','warga.nik')
        ->select('spm_ibu_hamil.*','warga.*')
        ->get();

        //Looping Semua Ibu Hamil
        foreach($data as $index =>$ibuhamil){
            $total_ibu_hamil++;
            $hayo3bulanlagi = date('Y-m-d', strtotime('+3 month', strtotime($ibuhamil->tanggal_hamil)));
            $hayo6bulanlagi = date('Y-m-d', strtotime('+6 month', strtotime($ibuhamil->tanggal_hamil)));
            $hayo9bulanlagi = date('Y-m-d', strtotime('+9 month', strtotime($ibuhamil->tanggal_hamil)));
            if($ibuhamil->status_kehamilan == "Tahap Pelayanan"){
                $total_tahap_pelayanan++;
            }
            if($ibuhamil->status_kehamilan == "Sesuai SPM"){
                $total_sesuai_spm++;
            }
            if($ibuhamil->status_kehamilan == "Tidak Sesuai SPM"){
                $total_tsesuai_spm++;
            }
        
            $pelayanan[$index] = Pelayanan::where('table_spm', 'ibu_hamil')->where('id_spm',$ibuhamil->id)->get();
        
            $capaian_berat_periode_1 =0;$capaian_berat_periode_2 =0;$capaian_berat_periode_3 =0;
            $capaian_tinggi_periode_1 =0;$capaian_tinggi_periode_2 =0;$capaian_tinggi_periode_3 =0;
            $capaian_tekanan_darah_periode_1 =0;$capaian_tekanan_darah_periode_2 =0;$capaian_tekanan_darah_periode_3 =0;
            $capaian_lila_periode_1 =0;$capaian_lila_periode_2 =0;$capaian_lila_periode_3 =0;
            $capaian_tinggi_puncak_rahim_periode_1 =0;$capaian_tinggi_puncak_rahim_periode_2 =0;$capaian_tinggi_puncak_rahim_periode_3 =0;
            $capaian_presensi_periode_1 =0;$capaian_presensi_periode_2 =0;$capaian_presensi_periode_3 =0;
            $capaian_djj_periode_1 =0;$capaian_djj_periode_2 =0;$capaian_djj_periode_3 =0;
            $capaian_skrining_periode_1 =0;$capaian_skrining_periode_2 =0;$capaian_skrining_periode_3 =0;
            $capaian_imun_tt_periode_1 =0;$capaian_imun_tt_periode_2 =0;$capaian_imun_tt_periode_3 =0;
            $capaian_tes_kehamilan_periode_1 = 0;$capaian_tes_kehamilan_periode_2 =0;$capaian_tes_kehamilan_periode_3 =0;
            $capaian_tes_hb_periode_1 = 0;$capaian_tes_hb_periode_2= 0;$capaian_tes_hb_periode_3 = 0;
            $capaian_tes_gol_dar_periode_1 = 0;$capaian_tes_gol_dar_periode_2 = 0;$capaian_tes_gol_dar_periode_3 = 0;
            $capaian_tes_urin_periode_1 = 0;$capaian_tes_urin_periode_2 = 0;$capaian_tes_urin_periode_3 = 0;        
            $capaian_tatalaksana_periode_1 =0;$capaian_tatalaksana_periode_2 =0;$capaian_tatalaksana_periode_3 =0;
            $capaian_temu_periode_1 =0;$capaian_temu_periode_2 =0;$capaian_temu_periode_3 =0;        
            $capaian_tablet_all_periode =0;        
            $pel_by_id = [];
            $pel_by_id2 = [];
            $pel_by_id3 = [];   
            // Inisialisai Nilai Kriteria per Pelayanan dan Hitung Pencapaian Pelayanan per Pelayanan
            foreach($pelayanan[$index] as $i => $pel){
                $capaian_berat[$i] = 0;$capaian_tinggi[$i] = 0;
                $capaian_tekanan_darah[$i] = 0;
                $capaian_lila[$i] = 0;
                $capaian_tinggi_puncak_rahim[$i] = 0;
                $capaian_presensi[$i] = 0;
                $capaian_djj[$i] = 0;
                $capaian_skrining[$i] = 0;
                $capaian_imun_tt[$i] = 0;
                $capaian_tes_kehamilan[$i] = 0;
                $capaian_tes_hb[$i] = 0;
                $capaian_tes_gol_dar[$i] = 0;
                $capaian_tes_urin[$i] = 0;
                $capaian_tatalaksana[$i] = 0;
                $capaian_temu[$i] = 0;
                $capaian_tablet[$i] =0;

                //Berat Tinggi
                if($pel['berat_tinggi'] != null){
                    $bt = explode('_',$pel['berat_tinggi']);
                    if($bt[0] > 45 && $bt[1] > 100){
                        $capaian_berat[$i] = 0.5;
                        $capaian_tinggi[$i] = 0.5;
                    }else{
                    if($bt[0] >45){
                        $capaian_berat[$i] = 0.5;
                    }            
                    if($bt[1] >100){
                        $capaian_tinggi[$i] = 0.5;
                    }}         
                }
                
                // Tekanan Darah
                if($pel['tekanan_darah'] != null){
                    $td = explode('/',$pel['tekanan_darah']);
                    if($td[0] > 90 && $td[1] > 60){
                        $capaian_tekanan_darah[$i] = 1;
                    }            
                }
        
                //Lila
                if($pel['lila'] != null && $pel['lila'] > 2.35 ){
                    $capaian_lila[$i] = 1;            
                }
        
                //Tinggi Puncak Rahim
                if($pel['tinggi_puncak_rahim'] != null){
                    $capaian_tinggi_puncak_rahim[$i] = 1;            
                }
        
                //Presentasi DJJ
                if($pel['presentasi_djj']!= null){
                    $pdjj = explode('_',$pel['presentasi_djj']);
                    if($pdjj[0] != null && $pdjj[1] >120){
                        $capaian_presensi[$i] = 0.5;            
                        $capaian_djj[$i] = 0.5;            
                    }else{
                    if($pdjj[0] != null){
                        $capaian_presensi[$i] = 0.5;
                    }
                    if($pdjj[1] > 120){
                        $capaian_djj[$i] = 0.5;
                    }}
                }
        
                //Imunisasi Tetanus TT
                if($pel['imunisasi_tetanus_tt'] != null){
                    $itt = explode('_',$pel['imunisasi_tetanus_tt']);
                    if($itt[0] == "y" && $itt[1] == "y"){
                        $capaian_skrining[$i] = 0.5;            
                        $capaian_imun_tt[$i] = 0.5;            
                    }else{
                    if($itt[0] == "y"){
                        $capaian_skrining[$i] = 0.5;
                    }
                    if($itt[1] == "y"){
                        $capaian_imun_tt[$i] = 0.5;
                    }}
                }
                
                //Tablet Tambah Darah
                if($pel['tablet_tambah_darah'] != null){
                    $capaian_tablet[$i] = 1;
                    $capaian_tablet_all_periode = $capaian_tablet_all_periode + $pel['tablet_tambah_darah'];          
                }
                else{
                    $capaian_tablet_all_periode = $capaian_tablet_all_periode + 0;            
                }
        
                //Tes Laboratorium
                if($pel['tes_laboratorium'] != null){
                    $tl = explode('_',$pel['tes_laboratorium']);
                    
                    if($tl[0] == "y"){
                        $capaian_tes_kehamilan[$i] = 0.25;            
                    }
                    if($tl[1] == "y"){
                        $capaian_tes_hb[$i] = 0.25;            
                    }
                    if($tl[2] == "y"){
                        $capaian_tes_gol_dar[$i] = 0.25;            
                    }
                    if($tl[3] == "y"){
                        $capaian_tes_urin[$i] = 0.25;            
                    }
                }
        
                //Tatalaksana
                if($pel['tatalaksana'] == "y"){
                    $capaian_tatalaksana[$i] = 1;            
                }
        
                //Temu Wicara
                if($pel['temu_wicara'] == "y"){
                    $capaian_temu[$i] = 1;            
                }

                //Perhitungan per Pelayanan
                if($pel['tanggal_pelayanan'] <= $hayo3bulanlagi && $pel['tanggal_pelayanan'] > $ibuhamil->tanggal_hamil || $pel['tanggal_pelayanan'] == $ibuhamil->tanggal_hamil ){
                    $pel_by_id[$pel['id']] = $capaian_berat[$i]+$capaian_tinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                    $capaian_tinggi_puncak_rahim[$i]+$capaian_presensi[$i]+$capaian_djj[$i]+$capaian_skrining[$i]+$capaian_imun_tt[$i]+
                    $capaian_tes_kehamilan[$i]+$capaian_tes_hb[$i]+$capaian_tes_gol_dar[$i]+$capaian_tes_urin[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet[$i];
                    // $pel_by_id2[$pel['id']] = 0;
                    // $pel_by_id3[$pel['id']] = 0;
                }else if($pel['tanggal_pelayanan'] <= $hayo6bulanlagi && $pel['tanggal_pelayanan'] > $hayo3bulanlagi){
                    $pel_by_id2[$pel['id']] = $capaian_berat[$i]+$capaian_tinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                    $capaian_tinggi_puncak_rahim[$i]+$capaian_presensi[$i]+$capaian_djj[$i]+$capaian_skrining[$i]+$capaian_imun_tt[$i]+
                    $capaian_tes_kehamilan[$i]+$capaian_tes_hb[$i]+$capaian_tes_gol_dar[$i]+$capaian_tes_urin[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet[$i];
                    // $pel_by_id1[$pel['id']] = 0;
                    // $pel_by_id3[$pel['id']] = 0;
                }else if($pel['tanggal_pelayanan'] <= $hayo9bulanlagi && $pel['tanggal_pelayanan'] > $hayo6bulanlagi){
                    $pel_by_id3[$pel['id']] = $capaian_berat[$i]+$capaian_tinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                    $capaian_tinggi_puncak_rahim[$i]+$capaian_presensi[$i]+$capaian_djj[$i]+$capaian_skrining[$i]+$capaian_imun_tt[$i]+
                    $capaian_tes_kehamilan[$i]+$capaian_tes_hb[$i]+$capaian_tes_gol_dar[$i]+$capaian_tes_urin[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet[$i];
                    // $pel_by_id2[$pel['id']] = 0;
                    // $pel_by_id1[$pel['id']] = 0;
                }
                //End Perhitungan per Pelayanan
            }
            // End Inisialisai Nilai Kriteria per Pelayanan dan Hitung Pencapaian Pelayanan per Pelayanan
            // Inisialisai Nilai Kriteria per Periode
            foreach($pelayanan[$index] as $i => $pel){
                if($pel['tanggal_pelayanan'] <= $hayo3bulanlagi && $pel['tanggal_pelayanan'] > $ibuhamil->tanggal_hamil || $pel['tanggal_pelayanan'] == $ibuhamil->tanggal_hamil ){
                    if($capaian_berat_periode_1 < 0.5){$capaian_berat_periode_1 = $capaian_berat_periode_1 + $capaian_berat[$i];}
                    if($capaian_tinggi_periode_1 < 0.5){$capaian_tinggi_periode_1 = $capaian_tinggi_periode_1 + $capaian_tinggi[$i];}
                    if($capaian_tekanan_darah_periode_1 < 1){$capaian_tekanan_darah_periode_1 = $capaian_tekanan_darah_periode_1 + $capaian_tekanan_darah[$i];}
                    if($capaian_lila_periode_1 < 1){$capaian_lila_periode_1 = $capaian_lila_periode_1 + $capaian_lila[$i];}
                    if($capaian_tinggi_puncak_rahim_periode_1 < 1){$capaian_tinggi_puncak_rahim_periode_1 = $capaian_tinggi_puncak_rahim_periode_1 + $capaian_tinggi_puncak_rahim[$i];}
                    if($capaian_presensi_periode_1 < 0.5){$capaian_presensi_periode_1 = $capaian_presensi_periode_1 + $capaian_presensi[$i];}
                    if($capaian_djj_periode_1 < 0.5){$capaian_djj_periode_1 = $capaian_djj_periode_1 + $capaian_djj[$i];}
                    if($capaian_imun_tt_periode_1 < 0.5){$capaian_imun_tt_periode_1 = $capaian_imun_tt_periode_1 + $capaian_imun_tt[$i];}
                    if($capaian_skrining_periode_1 < 0.5){$capaian_skrining_periode_1 = $capaian_skrining_periode_1 + $capaian_skrining[$i];}
                    if($capaian_tes_kehamilan_periode_1 < 0.25){$capaian_tes_kehamilan_periode_1 = $capaian_tes_kehamilan_periode_1 + $capaian_tes_kehamilan[$i];}
                    if($capaian_tes_hb_periode_1 < 0.25){$capaian_tes_hb_periode_1 = $capaian_tes_hb_periode_1 + $capaian_tes_hb[$i];}
                    if($capaian_tes_gol_dar_periode_1 < 0.25){$capaian_tes_gol_dar_periode_1 = $capaian_tes_gol_dar_periode_1 + $capaian_tes_gol_dar[$i];}
                    if($capaian_tes_urin_periode_1 < 0.25){$capaian_tes_urin_periode_1 = $capaian_tes_urin_periode_1 + $capaian_tes_urin[$i];}
                    if($capaian_tatalaksana_periode_1 < 1){$capaian_tatalaksana_periode_1 = $capaian_tatalaksana_periode_1 + $capaian_tatalaksana[$i];}
                    if($capaian_temu_periode_1 < 1){$capaian_temu_periode_1 = $capaian_temu_periode_1 + $capaian_temu[$i];}
                }else if($pel['tanggal_pelayanan'] <= $hayo6bulanlagi && $pel['tanggal_pelayanan'] > $hayo3bulanlagi){
                    if($capaian_berat_periode_2 < 0.5){$capaian_berat_periode_2 = $capaian_berat_periode_2 + $capaian_berat[$i];}
                    if($capaian_tinggi_periode_2 < 0.5){$capaian_tinggi_periode_2 = $capaian_tinggi_periode_2 + $capaian_tinggi[$i];}
                    if($capaian_tekanan_darah_periode_2 < 1){$capaian_tekanan_darah_periode_2 = $capaian_tekanan_darah_periode_2 + $capaian_tekanan_darah[$i];}
                    if($capaian_lila_periode_2 < 1){$capaian_lila_periode_2 = $capaian_lila_periode_2 + $capaian_lila[$i];}
                    if($capaian_tinggi_puncak_rahim_periode_2 < 1){$capaian_tinggi_puncak_rahim_periode_2 = $capaian_tinggi_puncak_rahim_periode_2 + $capaian_tinggi_puncak_rahim[$i];}
                    if($capaian_presensi_periode_2 < 0.5){$capaian_presensi_periode_2 = $capaian_presensi_periode_2 + $capaian_presensi[$i];}
                    if($capaian_djj_periode_2 < 0.5){$capaian_djj_periode_2 = $capaian_djj_periode_2 + $capaian_djj[$i];}
                    if($capaian_imun_tt_periode_2 < 0.5){$capaian_imun_tt_periode_2 = $capaian_imun_tt_periode_2 + $capaian_imun_tt[$i];}
                    if($capaian_skrining_periode_2 < 0.5){$capaian_skrining_periode_2 = $capaian_skrining_periode_2 + $capaian_skrining[$i];}
                    if($capaian_tes_kehamilan_periode_2 < 0.25){$capaian_tes_kehamilan_periode_2 = $capaian_tes_kehamilan_periode_2 + $capaian_tes_kehamilan[$i];}
                    if($capaian_tes_hb_periode_2 < 0.25){$capaian_tes_hb_periode_2 = $capaian_tes_hb_periode_2 + $capaian_tes_hb[$i];}
                    if($capaian_tes_gol_dar_periode_2 < 0.25){$capaian_tes_gol_dar_periode_2 = $capaian_tes_gol_dar_periode_2 + $capaian_tes_gol_dar[$i];}
                    if($capaian_tes_urin_periode_2 < 0.25){$capaian_tes_urin_periode_2 = $capaian_tes_urin_periode_2 + $capaian_tes_urin[$i];}
                    if($capaian_tatalaksana_periode_2 < 1){$capaian_tatalaksana_periode_2 = $capaian_tatalaksana_periode_2 + $capaian_tatalaksana[$i];}
                    if($capaian_temu_periode_2 < 1){$capaian_temu_periode_2 = $capaian_temu_periode_2 + $capaian_temu[$i];}
                }else if($pel['tanggal_pelayanan'] <= $hayo9bulanlagi && $pel['tanggal_pelayanan'] > $hayo6bulanlagi){
                    if($capaian_berat_periode_3 < 1){$capaian_berat_periode_3 = $capaian_berat_periode_3 + $capaian_berat[$i];}
                    if($capaian_tinggi_periode_3 < 1){$capaian_tinggi_periode_3 = $capaian_tinggi_periode_3 + $capaian_tinggi[$i];}
                    if($capaian_tekanan_darah_periode_3 < 2){$capaian_tekanan_darah_periode_3 = $capaian_tekanan_darah_periode_3 + $capaian_tekanan_darah[$i];}
                    if($capaian_lila_periode_3 < 2){$capaian_lila_periode_3 = $capaian_lila_periode_3 + $capaian_lila[$i];}
                    if($capaian_tinggi_puncak_rahim_periode_3 < 2){$capaian_tinggi_puncak_rahim_periode_3 = $capaian_tinggi_puncak_rahim_periode_3 + $capaian_tinggi_puncak_rahim[$i];}
                    if($capaian_presensi_periode_3 < 1){$capaian_presensi_periode_3 = $capaian_presensi_periode_3 + $capaian_presensi[$i];}
                    if($capaian_djj_periode_3 < 1){$capaian_djj_periode_3 = $capaian_djj_periode_3 + $capaian_djj[$i];}
                    if($capaian_imun_tt_periode_3 < 1){$capaian_imun_tt_periode_3 = $capaian_imun_tt_periode_3 + $capaian_imun_tt[$i];}
                    if($capaian_skrining_periode_3 < 1){$capaian_skrining_periode_3 = $capaian_skrining_periode_3 + $capaian_skrining[$i];}
                    if($capaian_tes_kehamilan_periode_3 < 0.5){$capaian_tes_kehamilan_periode_3 = $capaian_tes_kehamilan_periode_3 + $capaian_tes_kehamilan[$i];}
                    if($capaian_tes_hb_periode_3 < 0.5){$capaian_tes_hb_periode_3 = $capaian_tes_hb_periode_3 + $capaian_tes_hb[$i];}
                    if($capaian_tes_gol_dar_periode_3 < 0.5){$capaian_tes_gol_dar_periode_3 = $capaian_tes_gol_dar_periode_3 + $capaian_tes_gol_dar[$i];}
                    if($capaian_tes_urin_periode_3 < 0.5){$capaian_tes_urin_periode_3 = $capaian_tes_urin_periode_3 + $capaian_tes_urin[$i];}
                    if($capaian_tatalaksana_periode_3 < 2){$capaian_tatalaksana_periode_3 = $capaian_tatalaksana_periode_3 + $capaian_tatalaksana[$i];}
                    if($capaian_temu_periode_3 < 2){$capaian_temu_periode_3 = $capaian_temu_periode_3 + $capaian_temu[$i];}
                }
                // print "capaian berat tinggi ";
                // print" pelayanan ke";
                // print $i+1;
                // print " periode ";
                // print " ".$pel['jenis_tw']." : ";
                // print $capaian_berat_tinggi[$i]."<br>";   
            }
            if($capaian_tablet_all_periode >=90){
                $capaian_tablet_all_periode = 1;
            }
            else{
                $capaian_tablet_all_periode = 0;
            }
            // End Inisialisai Nilai Kriteria per Periode

            // Coba Test Data Ibu Hamil
            // print "capaian berat tinggi periode 1 : ".$capaian_berat_tinggi_periode_1."<br>";
            // print "capaian berat tinggi periode 2 : ".$capaian_berat_tinggi_periode_2."<br>";
            // print "capaian berat tinggi periode 3 : ".$capaian_berat_tinggi_periode_3."<br>";

            // print $total."<br><br>";
            //End Coba Test Data Ibu Hamil
            //Hitung Pencapaian Pelayanan per Periode
            $capaian_periode1[$index] = (($capaian_berat_periode_1+$capaian_tinggi_periode_1+$capaian_tekanan_darah_periode_1+$capaian_lila_periode_1+
                                        $capaian_tinggi_puncak_rahim_periode_1+$capaian_presensi_periode_1+$capaian_djj_periode_1+$capaian_skrining_periode_1+$capaian_imun_tt_periode_1+
                                        $capaian_tes_kehamilan_periode_1+$capaian_tes_hb_periode_1+$capaian_tes_gol_dar_periode_1+$capaian_tes_urin_periode_1+$capaian_tatalaksana_periode_1+
                                        $capaian_temu_periode_1+$capaian_tablet_all_periode) * 100 ) / 10;
            $capaian_periode2[$index] = (($capaian_berat_periode_2+$capaian_tinggi_periode_2+$capaian_tekanan_darah_periode_2+$capaian_lila_periode_2+
                                        $capaian_tinggi_puncak_rahim_periode_2+$capaian_presensi_periode_2+$capaian_djj_periode_2+$capaian_skrining_periode_2+$capaian_imun_tt_periode_2+
                                        $capaian_tes_kehamilan_periode_2+$capaian_tes_hb_periode_2+$capaian_tes_gol_dar_periode_2+$capaian_tes_urin_periode_2+$capaian_tatalaksana_periode_2+
                                        $capaian_temu_periode_2+$capaian_tablet_all_periode) * 100 ) / 10;
            $capaian_periode3[$index] = (($capaian_berat_periode_3+$capaian_tinggi_periode_3+$capaian_tekanan_darah_periode_3+$capaian_lila_periode_3+
                                        $capaian_tinggi_puncak_rahim_periode_3+$capaian_presensi_periode_3+$capaian_djj_periode_3+$capaian_skrining_periode_3+$capaian_imun_tt_periode_3+
                                        $capaian_tes_kehamilan_periode_3+$capaian_tes_hb_periode_3+$capaian_tes_gol_dar_periode_3+$capaian_tes_urin_periode_3+$capaian_tatalaksana_periode_3+
                                        $capaian_temu_periode_3+$capaian_tablet_all_periode) * 100 ) / 20;
            //End Hitung Pencapaian Pelayanan per Periode

            //Hitung Pencapaian Pelayanan per Total
            $super_pel_total = ($capaian_berat_periode_1+$capaian_tinggi_periode_1+$capaian_tekanan_darah_periode_1+$capaian_lila_periode_1+
            $capaian_tinggi_puncak_rahim_periode_1+$capaian_presensi_periode_1+$capaian_djj_periode_1+$capaian_skrining_periode_1+$capaian_imun_tt_periode_1+
            $capaian_tes_kehamilan_periode_1+$capaian_tes_hb_periode_1+$capaian_tes_gol_dar_periode_1+$capaian_tes_urin_periode_1+$capaian_tatalaksana_periode_1+
            $capaian_temu_periode_1+$capaian_tablet_all_periode) +  
            ($capaian_berat_periode_2+$capaian_tinggi_periode_2+$capaian_tekanan_darah_periode_2+$capaian_lila_periode_2+
            $capaian_tinggi_puncak_rahim_periode_2+$capaian_presensi_periode_2+$capaian_djj_periode_2+$capaian_skrining_periode_2+$capaian_imun_tt_periode_2+
            $capaian_tes_kehamilan_periode_2+$capaian_tes_hb_periode_2+$capaian_tes_gol_dar_periode_2+$capaian_tes_urin_periode_2+$capaian_tatalaksana_periode_2+
            $capaian_temu_periode_2+$capaian_tablet_all_periode) +
            ($capaian_berat_periode_3+$capaian_tinggi_periode_3+$capaian_tekanan_darah_periode_3+$capaian_lila_periode_3+
            $capaian_tinggi_puncak_rahim_periode_3+$capaian_presensi_periode_3+$capaian_djj_periode_3+$capaian_skrining_periode_3+$capaian_imun_tt_periode_3+
            $capaian_tes_kehamilan_periode_3+$capaian_tes_hb_periode_3+$capaian_tes_gol_dar_periode_3+$capaian_tes_urin_periode_3+$capaian_tatalaksana_periode_3+
            $capaian_temu_periode_3+$capaian_tablet_all_periode);

            $totalcapaian[$index] = ($capaian_periode1[$index]+$capaian_periode2[$index]+$capaian_periode3[$index])/3;
            //End Hitung Pencapaian Pelayanan per Total

            $data_ibu_hamil[$index]['id'] = $ibuhamil->id;
            $data_ibu_hamil[$index]['status_kehamilan'] = $ibuhamil->status_kehamilan;
            $data_ibu_hamil[$index]['nama'] = $ibuhamil->nama;
            $data_ibu_hamil[$index]['nik'] = $ibuhamil->nik;
            $data_ibu_hamil[$index]['tanggal_hamil'] = $ibuhamil->tanggal_hamil;
            $data_ibu_hamil[$index]['totalcapaian'] = $totalcapaian[$index];
        }
        return response()->json([
            'message'=>'success',
            'status'=>200,
            'data'=> ([
                'access_token' => $token_key,
                'data_ibu_hamil' => $data_ibu_hamil,
                'total_ibu_hamil' => $total_ibu_hamil,
                'total_sesuai_spm' => $total_sesuai_spm,
                'total_tsesuai_spm' => $total_tsesuai_spm, 
                'total_tahap_pelayanan' => $total_tahap_pelayanan
            ])
        ]);
    }
    public function dropdownNNIKTambahIbuHamil(){
        $token_id = str_random(16);
        $token_key = str_random(30);
        date_default_timezone_set('Asia/Jakarta');
        $data_nik = [];
        $urutan = 0;
        $warga_all = Warga::all();
        foreach($warga_all as $i => $warga){
            if($warga['jenis_kelamin'] == "P"){
                $usia = APIController::usia($warga['tgl_lahir'],date("Y/m/d/h:i:s"));
                if($usia["years"] >= 22){
                    $data = IbuHamil::where('nik', $warga['nik'])->where('status_kehamilan','Tahap Pelayanan')->first();
                    if(!$data){
                        $data_nik[$urutan]['nik'] = $warga['nik'];
                        $data_nik[$urutan]['nama'] = $warga['nama'];
                        $urutan++;
                    }
                }
            }
        }
        
        return response()->json([
            'message'=>'success',
            'status'=>200,
            'data'=> ([
                'access_token' => $token_key,
                'data_nik' => $data_nik
            ])
        ]);
    }
    public function tambahIbuHamil(Request $request) {  
        $a = date('Y-m-d', strtotime($request->tanggal_hamil));
        $token_id = str_random(16);
        $token_key = str_random(30);
        $data = New IbuHamil();
        $data->nik = $request->nik;
        $data->tanggal_hamil = $a;
        $data->status_kehamilan = "Tahap Pelayanan";
        if($data->save()){
            return response()->json([
                'message'=>'success',
                'status'=>200,
                'data'=> ([
                    'access_token' => $token_key
                    ])
            ]);
        }
        else{
            return response()->json([
                'message'=>'error',
                'status'=>404,
                'data'=> ([
                    'access_token' => null
                    ])
            ]);
        }
    }
    public function tambahPelayananIbuHamil(Request $request){

    }
    
    
    //Convert Usia
    public function usia($tgl1, $tgl2)
    {
        $tgl1 = (is_string($tgl1) ? strtotime($tgl1) : $tgl1);
        $tgl2 = (is_string($tgl2) ? strtotime($tgl2) : $tgl2);
        $diff_secs = abs($tgl1-$tgl2);
        $base_year = min(date("Y",$tgl1), date("Y",$tgl2));
        $diff = mktime(0,0,$diff_secs,1,1,$base_year);
        return array("years"=>date("Y",$diff) - $base_year, "months_total" => (date("Y",$diff) - $base_year) * 12 + date("n",$diff) - 1, 
        "months" => date("n",$diff) -1, "days_total" => floor($diff_secs/ (3600 * 24)), "days" => date("j",$diff) - 1, 
        "hours_total" => floor($diff_secs/3600), "hours" => date("G", $diff), "minutes_total" => floor($diff_secs/60), "minutes" => (int)date("i",$diff),
        "seconds_total" => $diff_secs, "seconds" => (int) date("s",$diff));
    }







    
    public function AllUser(){
        $alluser = User::all();
        if(is_null($alluser)){
            return response()->json([
                'message'=>'error',
                'status'=>404
            ]);
        }
        else{
            return response()->json([
                'message'=>'success',
                'status'=>200,
                'data'=>$alluser
            ]);
        }
    }

    public function login(Request $request){
        $token = 'fndjfhndksjwfnwehu57TGBVScnskjc';
        $user = User::where('username',$request->username)->first();
        if(count($user) > 0 ){
            return response()->json([
                'message'=>'success',
                'status'=>200,
                'data'=> (['access_token' => $token])
            ]);
        }
        else{
            return response()->json([
                'message'=>'error',
                'status'=>404,
                'data'=> (['access_token' => null])
            ]);
        }
    }

    public function saveWarga (Request $request){
        $token = 'fndjfhndksjwfnwehu57TGBVScnskjc';
        date_default_timezone_set('Asia/Jakarta');
        $warga = Warga::where('nik',$request->nik)->first();
        if(count($warga) == 0){
            $data = Warga::create([
                'nik' => $request->nik,
                'nama' => $request->nama_pasien,
                'tempat_lahir' => "Depok",
                'tgl_lahir' => date("Y/m/d/h:i:s"),
                'jenis_kelamin' => "P",
                'gol_darah' => "AB",
                'alamat' => $request->alamat,
                'kelurahan' => "Beji",
                'kecamatan' => "Beji",
                'kota' => "Depok",
                'provinsi' => "Jawa Barat",
                'agama' => "Islam",
                'no_telp' => $request->no_telp,
                'status_perkawinan' => "Belum Menikah",
                'pekerjaan' => "Belum Bekerja",
                'kewarganegaraan' => "Indonesia"
            ]);        
            if($data->save()){
                return response()->json([
                    'message'=>'success',
                    'status'=>200,
                    'data'=> (['access_token' => $token])
                ]);
            }  
            else{
                return response()->json([
                    'message'=>'error',
                    'status'=>404,
                    'data'=> (['access_token' => null])
                ]);
            }            
        }
        else{
            return response()->json([
                'message'=>'nik sudah ada',
                'status'=>404,
                'data'=> (['access_token' => null])
            ]);
        }
    }
    public function dataWarga(){
        $allwarga = Warga::all();
        if(is_null($allwarga)){
            return response()->json([
                'message'=>'warga null',
                'status'=>404
            ]);
        }
        else{
            return response()->json([
                'message'=>'success',
                'status'=>200,
                'data'=>$allwarga
            ]);
        }
    }

    public function tambahibubersalin(Request $request){
        date_default_timezone_set('Asia/Jakarta');
        $new_ibu_bersalin = IbuBersalin::create([
            'nik' => $request->nik,
            'tanggal_hamil' => date("Y/m/d/h:i:s"),
            'tanggal_bersalin' => date("Y/m/d/h:i:s"), 
            'fasilitas_kesehatan' => $request->fasilitas,
            'lokasi_fasilitas' => $request->lokasi,
            'tenaga_kesehatan' => $request->tenaga,
            'nama_tenaga' => "Dr. Ahmad Riza",
            'status_persalinan' => "Normal",
            'hasil_persalinan' => $request->keterangan
        ]);
        if($new_ibu_bersalin->save()){
            return response()->json([
                'message'=>'success',
                'status'=>200,
                'data' => $new_ibu_bersalin
            ]);
        }
        else{
            return response()->json([
                'message'=>'input gagal',
                'status'=>404,
                'data'=>null
            ]);
        }
    }
}

