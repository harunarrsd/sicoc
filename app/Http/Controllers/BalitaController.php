<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Requests;
use Illuminate\Support\Facades\Session;
use Redirect;
use App\Pelayanan;
use App\Warga;
use App\Balita;


class BalitaController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        return view('spm.balita.index');
    }

    public function data(){
        $balita_all = Balita::all();
        $total_spm = 0;
        $total_tidak_spm = 0;
        $total_tahap_pelayanan = 0;
        $total_balita = 0;
        foreach($balita_all as $index => $balita){
            $datawarga[$index] = Warga::where('nik',$balita['nik'])->first();
            $total_balita++;
            if($balita['status_balita'] == "Sesuai SPM"){
                $total_spm++;
            }
            else if($balita['status_balita'] == "Tidak Sesuai SPM"){
                $total_tidak_spm ++;
            }
            else if($balita['status_balita'] == "Tahap Pelayanan"){
                $total_tahap_pelayanan++;
            }
            date_default_timezone_set('Asia/Jakarta');
            $usia[$index] = BalitaController::Usia($datawarga[$index]['tgl_lahir'],date("Y/m/d/h:i:s"));
            $batasperiode1 = date('Y-m-d', strtotime('+12 month', strtotime($datawarga[$index]['tgl_lahir'])));
            $batasperiode2 = date('Y-m-d', strtotime('+24 month', strtotime($datawarga[$index]['tgl_lahir'])));           
            $batasperiode3 = date('Y-m-d', strtotime('+36 month', strtotime($datawarga[$index]['tgl_lahir'])));
            $batasperiode4 = date('Y-m-d', strtotime('+48 month', strtotime($datawarga[$index]['tgl_lahir'])));
            $batasperiode5 = date('Y-m-d', strtotime('+59 month', strtotime($datawarga[$index]['tgl_lahir'])));
            $pelayananBalita[$index] = Pelayanan::where('table_spm', 'balita')->where('id_spm',$balita['id'])->get();
            foreach($pelayananBalita[$index] as $i =>$data_pelayanan){
                if($data_pelayanan != null){
                    if($data_pelayanan['berat_tinggi'] != null){
                        if(explode('_',$data_pelayanan['berat_tinggi'])[0] != null && explode('_',$data_pelayanan['berat_tinggi'])[0] > 0){
                            $capaian1[$i] = 1;
                        }
                        else{
                            $capaian1[$i] = 0;
                        }
                        if(explode('_',$data_pelayanan['berat_tinggi'])[1] != null && explode('_',$data_pelayanan['berat_tinggi'])[1] > 0){
                            $capaian2[$i] = 1;
                        }
                        else{
                            $capaian2[$i] = 0;
                        }
                    }
                    else{
                        $capaian1[$i] = 0;
                        $capaian2[$i] = 0;
                    }
                    if($data_pelayanan['kapsul_vit_a'] != null && $data_pelayanan['kapsul_vit_a'] == "y" ){
                        $capaian3[$i] = 1;
                    }else{
                        $capaian3[$i] = 0;
                    }
                    if($data_pelayanan['imunisasi_d_lengkap'] != null && $data_pelayanan['imunisasi_d_lengkap'] == "y" ){
                        $capaian4[$i] = 1;
                    }
                    else{
                        $capaian4[$i] = 0;
                    }
                }
                else{
                    $capaian1[$i] = 0;
                    $capaian2[$i] = 0;
                    $capaian3[$i] = 0;
                    $capaian4[$i] = 0;
                }
            }
            $capaian_berat1=0;$capaian_tinggi1=0;$capaian_kapsul1=0;$capaian_imunisasi1=0;
            $capaian_berat2=0;$capaian_tinggi2=0;$capaian_kapsul2=0;$capaian_imunisasi2=0;
            $capaian_berat3=0;$capaian_tinggi3=0;$capaian_kapsul3=0;$capaian_imunisasi3=0;
            $capaian_berat4=0;$capaian_tinggi4=0;$capaian_kapsul4=0;$capaian_imunisasi4=0;
            $capaian_berat5=0;$capaian_tinggi5=0;$capaian_kapsul5=0;$capaian_imunisasi5=0;
            foreach($pelayananBalita[$index] as $j => $pel){
                if($pel['tanggal_pelayanan'] <= $batasperiode1 && $pel['tanggal_pelayanan'] >= $datawarga[$index]['tgl_lahir']){
                    if($capaian_berat1 < 8){$capaian_berat1= $capaian_berat1+$capaian1[$j];}
                    if($capaian_tinggi1 < 2){$capaian_tinggi1= $capaian_tinggi1+$capaian2[$j];}
                    if($capaian_kapsul1 < 2){$capaian_kapsul1= $capaian_kapsul1+$capaian3[$j];}
                    if($capaian_imunisasi1 < 1){$capaian_imunisasi1= $capaian_imunisasi1+$capaian4[$j];}
                }else if($pel['tanggal_pelayanan'] <= $batasperiode2 && $pel['tanggal_pelayanan'] > $batasperiode1){
                    if($capaian_berat2 < 8){$capaian_berat2= $capaian_berat2+$capaian1[$j];}
                    if($capaian_tinggi2 < 2){$capaian_tinggi2= $capaian_tinggi2+$capaian2[$j];}
                    if($capaian_kapsul2 < 2){$capaian_kapsul2= $capaian_kapsul2+$capaian3[$j];}
                    if($capaian_imunisasi2 < 1){$capaian_imunisasi2= $capaian_imunisasi2+$capaian4[$j];}
                }else if($pel['tanggal_pelayanan'] <= $batasperiode3 && $pel['tanggal_pelayanan'] > $batasperiode2){
                    if($capaian_berat3 < 8){$capaian_berat3= $capaian_berat3+$capaian1[$j];}
                    if($capaian_tinggi3 < 2){$capaian_tinggi3= $capaian_tinggi3+$capaian2[$j];}
                    if($capaian_kapsul3 < 2){$capaian_kapsul3= $capaian_kapsul3+$capaian3[$j];}
                    if($capaian_imunisasi3 < 1){$capaian_imunisasi3= $capaian_imunisasi3+$capaian4[$j];}
                }else if($pel['tanggal_pelayanan'] <= $batasperiode4 && $pel['tanggal_pelayanan'] > $batasperiode3){
                    if($capaian_berat4 < 8){$capaian_berat4= $capaian_berat4+$capaian1[$j];}
                    if($capaian_tinggi4 < 2){$capaian_tinggi4= $capaian_tinggi4+$capaian2[$j];}
                    if($capaian_kapsul4 < 2){$capaian_kapsul4= $capaian_kapsul4+$capaian3[$j];}
                    if($capaian_imunisasi4 < 1){$capaian_imunisasi4= $capaian_imunisasi4+$capaian4[$j];}
                }else if($pel['tanggal_pelayanan'] <= $batasperiode5 && $pel['tanggal_pelayanan'] > $batasperiode4){
                    if($capaian_berat5 < 8){$capaian_berat5= $capaian_berat5+$capaian1[$j];}
                    if($capaian_tinggi5 < 2){$capaian_tinggi5= $capaian_tinggi5+$capaian2[$j];}
                    if($capaian_kapsul5 < 2){$capaian_kapsul5= $capaian_kapsul5+$capaian3[$j];}
                    if($capaian_imunisasi5 < 1){$capaian_imunisasi5= $capaian_imunisasi5+$capaian4[$j];}
                }
            }
            $capaian_all_periode1[$index] = ((($capaian_berat1*100)/8)+(($capaian_tinggi1*100)/2)+(($capaian_kapsul1*100)/2)+(($capaian_imunisasi1*100)/1))/4;
            $capaian_all_periode2[$index] = ((($capaian_berat2*100)/8)+(($capaian_tinggi2*100)/2)+(($capaian_kapsul2*100)/2)+(($capaian_imunisasi2*100)/1))/4;
            $capaian_all_periode3[$index] = ((($capaian_berat3*100)/8)+(($capaian_tinggi3*100)/2)+(($capaian_kapsul3*100)/2)+(($capaian_imunisasi3*100)/1))/4;
            $capaian_all_periode4[$index] = ((($capaian_berat4*100)/8)+(($capaian_tinggi4*100)/2)+(($capaian_kapsul4*100)/2)+(($capaian_imunisasi4*100)/1))/4;
            $capaian_all_periode5[$index] = ((($capaian_berat5*100)/8)+(($capaian_tinggi5*100)/2)+(($capaian_kapsul5*100)/2)+(($capaian_imunisasi5*100)/1))/4;
            $capaian_total[$index] = ($capaian_all_periode1[$index]+$capaian_all_periode2[$index]+$capaian_all_periode3[$index]+$capaian_all_periode4[$index]+$capaian_all_periode5[$index])/5;
        }
        return view('spm.balita.data',compact('pelayananBalita','datawarga','capaian_total','balita_all','usia',
        'total_spm','total_tidak_spm','total_tahap_pelayanan','total_balita'));
    }

    public function delete($id){
        $balita=Balita::where('id',$id)->first();
        $delete = $balita->delete();
        
        if($delete){
            return Redirect::route('data-balita')->with('success','Berhasil Hapus Balita');
        }  
        else{
            return Redirect::route('data-balita')->with('danger','Gagal Hapus Balita');
        }
    }

    public function delete_pelayanan($id){
        $pelayanan=Pelayanan::where('id',$id)->first();
        $delete = $pelayanan->delete();
        
        if($delete){
            return Redirect::route('data-balita')->with('success','Berhasil Hapus Pelayanan');
        }  
        else{
            return Redirect::route('data-balita')->with('danger','Gagal Hapus Pelayanan');
        }
    }

    public function detail($id){
        $balita = Balita::where('id',$id)->first();
        $pelayanan_all = Pelayanan::where('table_spm','balita')->where('id_spm',$balita['id'])->get();
        $datawarga = Warga::where('nik',$balita['nik'])->first();
        Session::put('withnik3',$datawarga['nik']);
        date_default_timezone_set('Asia/Jakarta');
        $usia_balita =  BalitaController::Usia($datawarga['tgl_lahir'],date("Y/m/d/h:i:s"));
        $batasperiode1 = date('Y-m-d', strtotime('+12 month', strtotime($datawarga['tgl_lahir'])));
        $batasperiode2 = date('Y-m-d', strtotime('+24 month', strtotime($datawarga['tgl_lahir'])));
        $batasperiode3 = date('Y-m-d', strtotime('+36 month', strtotime($datawarga['tgl_lahir'])));
        $batasperiode4 = date('Y-m-d', strtotime('+48 month', strtotime($datawarga['tgl_lahir'])));
        $batasperiode5 = date('Y-m-d', strtotime('+59 month', strtotime($datawarga['tgl_lahir'])));
        foreach($pelayanan_all as $i => $data_pelayanan){
            if($data_pelayanan != null){
                if($data_pelayanan['tanggal_pelayanan'] <= $batasperiode1 && $data_pelayanan['tanggal_pelayanan'] >= $datawarga->tgl_lahir){
                    $periode[$i] = 1;
                }else if($data_pelayanan['tanggal_pelayanan'] <= $batasperiode2 && $data_pelayanan['tanggal_pelayanan'] > $batasperiode1){
                    $periode[$i] = 2;
                }else if($data_pelayanan['tanggal_pelayanan'] <= $batasperiode3 && $data_pelayanan['tanggal_pelayanan'] > $batasperiode2){
                    $periode[$i] = 3;
                }else if($data_pelayanan['tanggal_pelayanan'] <= $batasperiode4 && $data_pelayanan['tanggal_pelayanan'] > $batasperiode3){
                    $periode[$i] = 4;
                }else if($data_pelayanan['tanggal_pelayanan'] <= $batasperiode5 && $data_pelayanan['tanggal_pelayanan'] > $batasperiode4){
                    $periode[$i] = 5;
                }else{
                    $periode[$i] = 0;
                }
                if($data_pelayanan['berat_tinggi'] != null){
                    if(explode('_',$data_pelayanan['berat_tinggi'])[0] != null && explode('_',$data_pelayanan['berat_tinggi'])[0] > 0){
                        $capaian1[$i] = 1;
                    }
                    else{
                        $capaian1[$i] = 0;
                    }
                    if(explode('_',$data_pelayanan['berat_tinggi'])[1] != null && explode('_',$data_pelayanan['berat_tinggi'])[1] > 0){
                        $capaian2[$i] = 1;
                    }
                    else{
                        $capaian2[$i] = 0;
                    }
                }
                else{
                    $capaian1[$i] = 0;
                    $capaian2[$i] = 0;
                }
                if($data_pelayanan['kapsul_vit_a'] != null && $data_pelayanan['kapsul_vit_a'] == "y" ){
                    $capaian3[$i] = 1;
                }else{
                    $capaian3[$i] = 0;
                }
                if($data_pelayanan['imunisasi_d_lengkap'] != null && $data_pelayanan['imunisasi_d_lengkap'] == "y" ){
                    $capaian4[$i] = 1;
                }
                else{
                    $capaian4[$i] = 0;
                }
                $cap_pel_total[$i] = (($capaian1[$i]+$capaian2[$i]+$capaian3[$i]+$capaian4[$i])*100)/4;
            }
            else{
                $capaian1[$i] = 0;
                $capaian2[$i] = 0;
                $capaian3[$i] = 0;
                $capaian4[$i] = 0;
                $cap_pel_total[$i] =0;
            }
        }
        $capaian_berat1=0;$capaian_tinggi1=0;$capaian_kapsul1=0;$capaian_imunisasi1=0;
        $capaian_berat2=0;$capaian_tinggi2=0;$capaian_kapsul2=0;$capaian_imunisasi2=0;
        $capaian_berat3=0;$capaian_tinggi3=0;$capaian_kapsul3=0;$capaian_imunisasi3=0;
        $capaian_berat4=0;$capaian_tinggi4=0;$capaian_kapsul4=0;$capaian_imunisasi4=0;
        $capaian_berat5=0;$capaian_tinggi5=0;$capaian_kapsul5=0;$capaian_imunisasi5=0;
        foreach($pelayanan_all as $j => $pel){   
            if($pel['tanggal_pelayanan'] <= $batasperiode1 && $pel['tanggal_pelayanan'] >= $datawarga->tgl_lahir){
                if($capaian_berat1 < 8){$capaian_berat1= $capaian_berat1+$capaian1[$j];}
                if($capaian_tinggi1 < 2){$capaian_tinggi1= $capaian_tinggi1+$capaian2[$j];}
                if($capaian_kapsul1 < 2){$capaian_kapsul1= $capaian_kapsul1+$capaian3[$j];}
                if($capaian_imunisasi1 < 1){$capaian_imunisasi1= $capaian_imunisasi1+$capaian4[$j];}
            }else if($pel['tanggal_pelayanan'] <= $batasperiode2 && $pel['tanggal_pelayanan'] > $batasperiode1){
                if($capaian_berat2 < 8){$capaian_berat2= $capaian_berat2+$capaian1[$j];}
                if($capaian_tinggi2 < 2){$capaian_tinggi2= $capaian_tinggi2+$capaian2[$j];}
                if($capaian_kapsul2 < 2){$capaian_kapsul2= $capaian_kapsul2+$capaian3[$j];}
                if($capaian_imunisasi2 < 1){$capaian_imunisasi2= $capaian_imunisasi2+$capaian4[$j];}
            }else if($pel['tanggal_pelayanan'] <= $batasperiode3 && $pel['tanggal_pelayanan'] > $batasperiode2){
                if($capaian_berat3 < 8){$capaian_berat3= $capaian_berat3+$capaian1[$j];}
                if($capaian_tinggi3 < 2){$capaian_tinggi3= $capaian_tinggi3+$capaian2[$j];}
                if($capaian_kapsul3 < 2){$capaian_kapsul3= $capaian_kapsul3+$capaian3[$j];}
                if($capaian_imunisasi3 < 1){$capaian_imunisasi3= $capaian_imunisasi3+$capaian4[$j];}
            }else if($pel['tanggal_pelayanan'] <= $batasperiode4 && $pel['tanggal_pelayanan'] > $batasperiode3){
                if($capaian_berat4 < 8){$capaian_berat4= $capaian_berat4+$capaian1[$j];}
                if($capaian_tinggi4 < 2){$capaian_tinggi4= $capaian_tinggi4+$capaian2[$j];}
                if($capaian_kapsul4 < 2){$capaian_kapsul4= $capaian_kapsul4+$capaian3[$j];}
                if($capaian_imunisasi4 < 1){$capaian_imunisasi4= $capaian_imunisasi4+$capaian4[$j];}
            }else if($pel['tanggal_pelayanan'] <= $batasperiode5 && $pel['tanggal_pelayanan'] > $batasperiode4){
                if($capaian_berat5 < 8){$capaian_berat5= $capaian_berat5+$capaian1[$j];}
                if($capaian_tinggi5 < 2){$capaian_tinggi5= $capaian_tinggi5+$capaian2[$j];}
                if($capaian_kapsul5 < 2){$capaian_kapsul5= $capaian_kapsul5+$capaian3[$j];}
                if($capaian_imunisasi5 < 1){$capaian_imunisasi5= $capaian_imunisasi5+$capaian4[$j];}
            }
        }
        // print_r($capaian4);
        // print "<br>".$capaian_imunisasi1;
        $capaian_all_periode1 = ((($capaian_berat1*100)/8)+(($capaian_tinggi1*100)/2)+(($capaian_kapsul1*100)/2)+(($capaian_imunisasi1*100)/1))/4;
        $capaian_all_periode2 = ((($capaian_berat2*100)/8)+(($capaian_tinggi2*100)/2)+(($capaian_kapsul2*100)/2)+(($capaian_imunisasi2*100)/1))/4;
        $capaian_all_periode3 = ((($capaian_berat3*100)/8)+(($capaian_tinggi3*100)/2)+(($capaian_kapsul3*100)/2)+(($capaian_imunisasi3*100)/1))/4;
        $capaian_all_periode4 = ((($capaian_berat4*100)/8)+(($capaian_tinggi4*100)/2)+(($capaian_kapsul4*100)/2)+(($capaian_imunisasi4*100)/1))/4;
        $capaian_all_periode5 = ((($capaian_berat5*100)/8)+(($capaian_tinggi5*100)/2)+(($capaian_kapsul5*100)/2)+(($capaian_imunisasi5*100)/1))/4;
        $capaian_total = ($capaian_all_periode1+$capaian_all_periode2+$capaian_all_periode3+$capaian_all_periode4+$capaian_all_periode5)/5;
        return view('spm.balita.detail',compact('usia_balita','datawarga','balita','capaian_total','capaian_all_periode1','capaian_all_periode2','capaian_all_periode3',
        'capaian_all_periode4','capaian_all_periode5','pelayanan_all','cap_pel_total','periode'));
    }

    public function ubah($id){
        date_default_timezone_set('Asia/Jakarta');
        $balita = Balita::where('id',$id)->first();
        $datawarga = Warga::where('nik',$balita['nik'])->first();
        $warga_all = Warga::all();
        $list_nik = [];$list_name = [];$list_ttl = [];
        foreach($warga_all as $indexwarga => $warga){
            $conditions = ['nik' => $warga['nik'], 'status_balita' => 'Tahap Pelayanan'];
            $data = Balita::where($conditions)->toSql();

            dd($data);
            if($data){
                $usia[$indexwarga] = BalitaController::Usia($warga['tgl_lahir'],date("Y/m/d/h:i:s"));
                if($usia[$indexwarga]["years"] == 0 && $usia[$indexwarga]["months"] == 0 && $usia[$indexwarga]["days"] <= 28){
                    $list_nik[$indexwarga] = $warga['nik'];
                    $list_name[$indexwarga] = $warga['nama'];
                    $list_ttl[$indexwarga] = $warga['tgl_lahir'];
                }
            }  
        }
        $pelayananBalita = Pelayanan::where('table_spm', 'balita')->where('id_spm',$balita['id'])->get();
        $batasperiode1 = date('Y-m-d', strtotime('+12 month', strtotime($datawarga['tgl_lahir'])));
        $batasperiode2 = date('Y-m-d', strtotime('+24 month', strtotime($datawarga['tgl_lahir'])));           
        $batasperiode3 = date('Y-m-d', strtotime('+36 month', strtotime($datawarga['tgl_lahir'])));
        $batasperiode4 = date('Y-m-d', strtotime('+48 month', strtotime($datawarga['tgl_lahir'])));
        $batasperiode5 = date('Y-m-d', strtotime('+59 month', strtotime($datawarga['tgl_lahir'])));
        foreach($pelayananBalita as $i =>$data_pelayanan){
            if($data_pelayanan != null){
                if($data_pelayanan['berat_tinggi'] != null){
                    if(explode('_',$data_pelayanan['berat_tinggi'])[0] != null && explode('_',$data_pelayanan['berat_tinggi'])[0] > 0){
                        $capaian1[$i] = 1;
                    }
                    else{
                        $capaian1[$i] = 0;
                    }
                    if(explode('_',$data_pelayanan['berat_tinggi'])[1] != null && explode('_',$data_pelayanan['berat_tinggi'])[1] > 0){
                        $capaian2[$i] = 1;
                    }
                    else{
                        $capaian2[$i] = 0;
                    }
                }
                else{
                    $capaian1[$i] = 0;
                    $capaian2[$i] = 0;
                }
                if($data_pelayanan['kapsul_vit_a'] != null && $data_pelayanan['kapsul_vit_a'] == "y" ){
                    $capaian3[$i] = 1;
                }else{
                    $capaian3[$i] = 0;
                }
                if($data_pelayanan['imunisasi_d_lengkap'] != null && $data_pelayanan['imunisasi_d_lengkap'] == "y" ){
                    $capaian4[$i] = 1;
                }
                else{
                    $capaian4[$i] = 0;
                }
            }
            else{
                $capaian1[$i] = 0;
                $capaian2[$i] = 0;
                $capaian3[$i] = 0;
                $capaian4[$i] = 0;
            }
        }
        $capaian_berat1=0;$capaian_tinggi1=0;$capaian_kapsul1=0;$capaian_imunisasi1=0;
        $capaian_berat2=0;$capaian_tinggi2=0;$capaian_kapsul2=0;$capaian_imunisasi2=0;
        $capaian_berat3=0;$capaian_tinggi3=0;$capaian_kapsul3=0;$capaian_imunisasi3=0;
        $capaian_berat4=0;$capaian_tinggi4=0;$capaian_kapsul4=0;$capaian_imunisasi4=0;
        $capaian_berat5=0;$capaian_tinggi5=0;$capaian_kapsul5=0;$capaian_imunisasi5=0;
        $capaian_berat1=0;$capaian_tinggi1=0;$capaian_kapsul1=0;$capaian_imunisasi1=0;
        $capaian_berat2=0;$capaian_tinggi2=0;$capaian_kapsul2=0;$capaian_imunisasi2=0;
        $capaian_berat3=0;$capaian_tinggi3=0;$capaian_kapsul3=0;$capaian_imunisasi3=0;
        $capaian_berat4=0;$capaian_tinggi4=0;$capaian_kapsul4=0;$capaian_imunisasi4=0;
        $capaian_berat5=0;$capaian_tinggi5=0;$capaian_kapsul5=0;$capaian_imunisasi5=0;
        foreach($pelayananBalita as $j => $pel){   
            if($pel['tanggal_pelayanan'] <= $batasperiode1 && $pel['tanggal_pelayanan'] >= $datawarga->tgl_lahir){
                if($capaian_berat1 < 8){$capaian_berat1= $capaian_berat1+$capaian1[$j];}
                if($capaian_tinggi1 < 2){$capaian_tinggi1= $capaian_tinggi1+$capaian2[$j];}
                if($capaian_kapsul1 < 2){$capaian_kapsul1= $capaian_kapsul1+$capaian3[$j];}
                if($capaian_imunisasi1 < 1){$capaian_imunisasi1= $capaian_imunisasi1+$capaian4[$j];}
            }else if($pel['tanggal_pelayanan'] <= $batasperiode2 && $pel['tanggal_pelayanan'] > $batasperiode1){
                if($capaian_berat2 < 8){$capaian_berat2= $capaian_berat2+$capaian1[$j];}
                if($capaian_tinggi2 < 2){$capaian_tinggi2= $capaian_tinggi2+$capaian2[$j];}
                if($capaian_kapsul2 < 2){$capaian_kapsul2= $capaian_kapsul2+$capaian3[$j];}
                if($capaian_imunisasi2 < 1){$capaian_imunisasi2= $capaian_imunisasi2+$capaian4[$j];}
            }else if($pel['tanggal_pelayanan'] <= $batasperiode3 && $pel['tanggal_pelayanan'] > $batasperiode2){
                if($capaian_berat3 < 8){$capaian_berat3= $capaian_berat3+$capaian1[$j];}
                if($capaian_tinggi3 < 2){$capaian_tinggi3= $capaian_tinggi3+$capaian2[$j];}
                if($capaian_kapsul3 < 2){$capaian_kapsul3= $capaian_kapsul3+$capaian3[$j];}
                if($capaian_imunisasi3 < 1){$capaian_imunisasi3= $capaian_imunisasi3+$capaian4[$j];}
            }else if($pel['tanggal_pelayanan'] <= $batasperiode4 && $pel['tanggal_pelayanan'] > $batasperiode3){
                if($capaian_berat4 < 8){$capaian_berat4= $capaian_berat4+$capaian1[$j];}
                if($capaian_tinggi4 < 2){$capaian_tinggi4= $capaian_tinggi4+$capaian2[$j];}
                if($capaian_kapsul4 < 2){$capaian_kapsul4= $capaian_kapsul4+$capaian3[$j];}
                if($capaian_imunisasi4 < 1){$capaian_imunisasi4= $capaian_imunisasi4+$capaian4[$j];}
            }else if($pel['tanggal_pelayanan'] <= $batasperiode5 && $pel['tanggal_pelayanan'] > $batasperiode4){
                if($capaian_berat5 < 8){$capaian_berat5= $capaian_berat5+$capaian1[$j];}
                if($capaian_tinggi5 < 2){$capaian_tinggi5= $capaian_tinggi5+$capaian2[$j];}
                if($capaian_kapsul5 < 2){$capaian_kapsul5= $capaian_kapsul5+$capaian3[$j];}
                if($capaian_imunisasi5 < 1){$capaian_imunisasi5= $capaian_imunisasi5+$capaian4[$j];}
            }
        }
        $capaian_all_periode1 = ((($capaian_berat1*100)/8)+(($capaian_tinggi1*100)/2)+(($capaian_kapsul1*100)/2)+(($capaian_imunisasi1*100)/1))/4;
        $capaian_all_periode2 = ((($capaian_berat2*100)/8)+(($capaian_tinggi2*100)/2)+(($capaian_kapsul2*100)/2)+(($capaian_imunisasi2*100)/1))/4;
        $capaian_all_periode3 = ((($capaian_berat3*100)/8)+(($capaian_tinggi3*100)/2)+(($capaian_kapsul3*100)/2)+(($capaian_imunisasi3*100)/1))/4;
        $capaian_all_periode4 = ((($capaian_berat4*100)/8)+(($capaian_tinggi4*100)/2)+(($capaian_kapsul4*100)/2)+(($capaian_imunisasi4*100)/1))/4;
        $capaian_all_periode5 = ((($capaian_berat5*100)/8)+(($capaian_tinggi5*100)/2)+(($capaian_kapsul5*100)/2)+(($capaian_imunisasi5*100)/1))/4;
        $capaian_total = ($capaian_all_periode1+$capaian_all_periode2+$capaian_all_periode3+$capaian_all_periode4+$capaian_all_periode5)/5;
        return view('spm.balita.ubah',compact('capaian_total','list_nik','list_name','list_ttl','datawarga','balita'));
    }

    public function edit(Request $request){
        $balita = Balita::where('id',$request->id)->first();
        $balita['nik'] = $request->nik;
        if($balita->save()){
            return redirect('/data-balita')->with('alert', 'Berhasil Ubah NIK Balita');
        }  
        else{
            return redirect('/data-balita')->with('alert', 'Gagal Ubah NIK Balita');
        }
    }
    public function tambah(){
        date_default_timezone_set('Asia/Jakarta');
        $all_warga = Warga::all();
        $list_ca_balita = [];
        foreach($all_warga as $index => $warga){
            $usia = BalitaController::Usia($warga['tgl_lahir'],date("Y/m/d/h:i:s"));
            if($usia["years"] < 5 && $usia["months"] < 59 || $usia["months"] == 59 && $usia["days"] <= 0){
                $data = Balita::where('nik', $warga['nik'])->where('status_balita','tahap pelayanan')->first();
                if(!$data){
                    $list_ca_balita[$index] = $warga;
                    $list_nik[$index] = $warga['nik'];
                    $list_name[$index] = $warga['nama'];
                    $list_ttl[$index] = $warga['tgl_lahir'];                        
                }
            }            
        }
        return view('spm.balita.tambah',compact('list_nik','list_name','list_ttl','list_ca_balita'));
    }
    
    public function save(Request $request){
        $warga = Warga::where('id',$request->select)->first();
        $data = New Balita();
        $data['nik'] = $warga->nik;
        $data['status_balita'] = "Tahap Pelayanan";
        if($data->save()){
            return Redirect::route('data-balita')->with('success','Berhasil Tambah Balita');
        }  
        else{
            return Redirect::route('tambah-balita')->with('danger','Gagal Tambah Balita');
        }
    }

    public function indexPelayanan(){
        $pelayanan_all = Pelayanan::where('table_spm','balita')->get();
        foreach($pelayanan_all as $i => $data_pelayanan){
            $balita[$i] = Balita::where('id',$data_pelayanan['id_spm'])->first();
            $datawarga[$i] = Warga::where('nik',$balita[$i]['nik'])->first();
            if($data_pelayanan != null){
                if($data_pelayanan['berat_tinggi'] != null){
                    if(explode('_',$data_pelayanan['berat_tinggi'])[0] != null && explode('_',$data_pelayanan['berat_tinggi'])[0] > 0){
                        $capaian1[$i] = 1;
                    }
                    else{
                        $capaian1[$i] = 0;
                    }
                    if(explode('_',$data_pelayanan['berat_tinggi'])[1] != null && explode('_',$data_pelayanan['berat_tinggi'])[1] > 0){
                        $capaian2[$i] = 1;
                    }
                    else{
                        $capaian2[$i] = 0;
                    }
                }
                else{
                    $capaian1[$i] = 0;
                    $capaian2[$i] = 0;
                }
                if($data_pelayanan['kapsul_vit_a'] != null && $data_pelayanan['kapsul_vit_a'] == "y" ){
                    $capaian3[$i] = 1;
                }else{
                    $capaian3[$i] = 0;
                }
                if($data_pelayanan['imunisasi_d_lengkap'] != null && $data_pelayanan['imunisasi_d_lengkap'] == "y" ){
                    $capaian4[$i] = 1;
                }
                else{
                    $capaian4[$i] = 0;
                }
                $cap_pel[$i] = (($capaian1[$i]+$capaian2[$i]+$capaian3[$i]+$capaian4[$i])*100)/4;
            }
            else{
                $capaian1[$i] = 0;
                $capaian2[$i] = 0;
                $capaian3[$i] = 0;
                $capaian4[$i] = 0;
            }
        }
        
        return view('spm.balita.pelayanan.data',compact('pelayanan_all','balita','datawarga','cap_pel'));
    }

    public function inputnikPelayanan(){
        if(!empty(Session::get('withnik3'))){
            Session::forget('withnik3');
            return view('spm.balita.pelayanan.masukan_nik');  
        }
        else{
            return view('spm.balita.pelayanan.masukan_nik');  
        }
    }

    public function inputPelayanan(Request $request){
        date_default_timezone_set('Asia/Jakarta');
        Session::put('withnik3',$request->nik);
        $nik = Session::get('withnik3');
        $datanik = Warga::where('nik', $nik)->first();
        if(count($datanik) > 0 ){
            $usia = BalitaController::Usia($datanik['tgl_lahir'],date("Y/m/d/h:i:s"));
            if($usia["months"] < 59 || $usia["months"] == 59 && $usia["days"] <= 0){
                $data = Balita::where('nik', $nik)->where('status_balita','tahap pelayanan')->first();
                if(count($data) > 0 ){
                    $request_id = $data->id;
                    $tgl_pel = [];
                    $batasperiode1 = date('Y-m-d', strtotime('+12 month', strtotime($datanik->tgl_lahir)));
                    $batasperiode2 = date('Y-m-d', strtotime('+24 month', strtotime($datanik->tgl_lahir)));
                    $batasperiode3 = date('Y-m-d', strtotime('+36 month', strtotime($datanik->tgl_lahir)));
                    $batasperiode4 = date('Y-m-d', strtotime('+48 month', strtotime($datanik->tgl_lahir)));
                    $batasperiode5 = date('Y-m-d', strtotime('+59 month', strtotime($datanik->tgl_lahir)));
                        
                    $pelayanan_tgl_pel = Pelayanan::where('table_spm', 'balita')->where('id_spm',$request_id)->get();
                    foreach($pelayanan_tgl_pel as $i => $data_pelayanan){
                        $tgl_pel[$i] = $data_pelayanan['tanggal_pelayanan'];
                        if($data_pelayanan != null){
                            if($data_pelayanan['berat_tinggi'] != null){
                                if(explode('_',$data_pelayanan['berat_tinggi'])[0] != null && explode('_',$data_pelayanan['berat_tinggi'])[0] > 0){
                                    $capaian1[$i] = 1;
                                }
                                else{
                                    $capaian1[$i] = 0;
                                }
                                if(explode('_',$data_pelayanan['berat_tinggi'])[1] != null && explode('_',$data_pelayanan['berat_tinggi'])[1] > 0){
                                    $capaian2[$i] = 1;
                                }
                                else{
                                    $capaian2[$i] = 0;
                                }
                            }
                            else{
                                $capaian1[$i] = 0;
                                $capaian2[$i] = 0;
                            }
                            if($data_pelayanan['kapsul_vit_a'] != null && $data_pelayanan['kapsul_vit_a'] == "y" ){
                                $capaian3[$i] = 1;
                            }else{
                                $capaian3[$i] = 0;
                            }
                            if($data_pelayanan['imunisasi_d_lengkap'] != null && $data_pelayanan['imunisasi_d_lengkap'] == "y" ){
                                $capaian4[$i] = 1;
                            }
                            else{
                                $capaian4[$i] = 0;
                            }
                        }
                        else{
                            $capaian1[$i] = 0;
                            $capaian2[$i] = 0;
                            $capaian3[$i] = 0;
                            $capaian4[$i] = 0;
                        }
                    }
                    // print_r($capaian1);
                    $capaian_berat1=0;$capaian_tinggi1=0;$capaian_kapsul1=0;$capaian_imunisasi1=0;
                    $capaian_berat2=0;$capaian_tinggi2=0;$capaian_kapsul2=0;$capaian_imunisasi2=0;
                    $capaian_berat3=0;$capaian_tinggi3=0;$capaian_kapsul3=0;$capaian_imunisasi3=0;
                    $capaian_berat4=0;$capaian_tinggi4=0;$capaian_kapsul4=0;$capaian_imunisasi4=0;
                    $capaian_berat5=0;$capaian_tinggi5=0;$capaian_kapsul5=0;$capaian_imunisasi5=0;
                    foreach($pelayanan_tgl_pel as $j => $pel){
                        // print "n";
                        if($pel['tanggal_pelayanan'] <= $batasperiode1 && $pel['tanggal_pelayanan'] >= $datanik->tgl_lahir){
                            //Periode 1
                            // print $capaian_berat1;
                            if($capaian1[$j] < 8){$capaian_berat1= $capaian_berat1+$capaian1[$j];}
                            if($capaian2[$j] < 2){$capaian_tinggi1= $capaian_tinggi1+$capaian2[$j];}
                            if($capaian3[$j] < 2){$capaian_kapsul1= $capaian_kapsul1+$capaian3[$j];}
                            if($capaian4[$j] < 1){$capaian_imunisasi1= $capaian_imunisasi1+$capaian4[$j];}
                        }else if($pel['tanggal_pelayanan'] <= $batasperiode2 && $pel['tanggal_pelayanan'] > $batasperiode1){
                            //Periode 2
                            if($capaian1[$j] < 8){$capaian_berat2= $capaian_berat2+$capaian1[$j];}
                            if($capaian2[$j] < 2){$capaian_tinggi2= $capaian_tinggi2+$capaian2[$j];}
                            if($capaian3[$j] < 2){$capaian_kapsul2= $capaian_kapsul2+$capaian3[$j];}
                            if($capaian4[$j] < 1){$capaian_imunisasi2= $capaian_imunisasi2+$capaian4[$j];}
                        }else if($pel['tanggal_pelayanan'] <= $batasperiode3 && $pel['tanggal_pelayanan'] > $batasperiode2){
                            // Periode 3
                            if($capaian1[$j] < 8){$capaian_berat3= $capaian_berat3+$capaian1[$j];}
                            if($capaian2[$j] < 2){$capaian_tinggi3= $capaian_tinggi3+$capaian2[$j];}
                            if($capaian3[$j] < 2){$capaian_kapsul3= $capaian_kapsul3+$capaian3[$j];}
                            if($capaian4[$j] < 1){$capaian_imunisasi3= $capaian_imunisasi3+$capaian4[$j];}
                        }else if($pel['tanggal_pelayanan'] <= $batasperiode4 && $pel['tanggal_pelayanan'] > $batasperiode3){
                            // Periode 4
                            if($capaian1[$j] < 8){$capaian_berat4= $capaian_berat4+$capaian1[$j];}
                            if($capaian2[$j] < 2){$capaian_tinggi4= $capaian_tinggi4+$capaian2[$j];}
                            if($capaian3[$j] < 2){$capaian_kapsul4= $capaian_kapsul4+$capaian3[$j];}
                            if($capaian4[$j] < 1){$capaian_imunisasi4= $capaian_imunisasi4+$capaian4[$j];}
                        }else if($pel['tanggal_pelayanan'] <= $batasperiode5 && $pel['tanggal_pelayanan'] > $batasperiode4){
                            // Periode 5
                            if($capaian1[$j] < 8){$capaian_berat5= $capaian_berat5+$capaian1[$j];}
                            if($capaian2[$j] < 2){$capaian_tinggi5= $capaian_tinggi5+$capaian2[$j];}
                            if($capaian3[$j] < 2){$capaian_kapsul5= $capaian_kapsul5+$capaian3[$j];}
                            if($capaian4[$j] < 1){$capaian_imunisasi5= $capaian_imunisasi5+$capaian4[$j];}
                        }
                    }
                    $capaian_all_periode1 = ((($capaian_berat1*100)/8)+(($capaian_tinggi1*100)/2)+(($capaian_kapsul1*100)/2)+(($capaian_imunisasi1*100)/1))/4;
                    $capaian_all_periode2 = ((($capaian_berat2*100)/8)+(($capaian_tinggi2*100)/2)+(($capaian_kapsul2*100)/2)+(($capaian_imunisasi2*100)/1))/4;
                    $capaian_all_periode3 = ((($capaian_berat3*100)/8)+(($capaian_tinggi3*100)/2)+(($capaian_kapsul3*100)/2)+(($capaian_imunisasi3*100)/1))/4;
                    $capaian_all_periode4 = ((($capaian_berat4*100)/8)+(($capaian_tinggi4*100)/2)+(($capaian_kapsul4*100)/2)+(($capaian_imunisasi4*100)/1))/4;
                    $capaian_all_periode5 = ((($capaian_berat5*100)/8)+(($capaian_tinggi5*100)/2)+(($capaian_kapsul5*100)/2)+(($capaian_imunisasi5*100)/1))/4;
                    $capaian_total = ($capaian_all_periode1+$capaian_all_periode2+$capaian_all_periode3+$capaian_all_periode4+$capaian_all_periode5)/5;
                    
                    // print $capaian_berat1;
                    // print $capaian_tinggi1;
                    // print $capaian_kapsul1;
                    // print $capaian_imunisasi1;
                    // print $capaian_berat2;
                    // print $capaian_berat3;
                    // print $capaian_all_periode1."mmmmm<br>";
                    // print $capaian_all_periode2."mmmmm<br>";
                    // print $capaian_all_periode3."mmmmm<br>";
                    // print $capaian_all_periode4."mmmmm<br>";
                    // print $capaian_all_periode5."mmmmm<br>";
                    // print $capaian_total;
                    // print_r($tgl_pel);
                    return view('spm.balita.pelayanan.tambah', compact('data','tgl_pel','datanik','capaian_total','capaian_all_periode1','capaian_all_periode2','capaian_all_periode3','capaian_all_periode4','capaian_all_periode5'));
                }
                else{
                    return view('spm.bayibarulahir.pelayanan.masukan_nik')->with('alert','NIK belum terdaftar sebagai Balita');
                }
            }
            else{
                return view('spm.balita.pelayanan.masukan_nik')->with('alert','NIK bukan beridentitas Balita');                
            }
        }
        else{
            return view('spm.balita.pelayanan.masukan_nik')->with('alert','NIK tidak terdaftar');
        }
    }

    public function addPelayanan(Request $request){
        $a = date('Y-m-d', strtotime($request->tanggal_pelayanan));
        $fix_tgl = $a." ".$request->waktu_pelayanan.":00";
            
        if($request->vita != null){$vit_a="y";}else{$vit_a="t";}
        if($request->imunisasi != null){$imun="y";}else{$imun="t";}
        
        $data = Pelayanan::create([
            'tanggal_pelayanan' => $fix_tgl,
            'id_spm' => $request->id,
            'table_spm' => 'balita',
            'tenaga_kerja' => $request->tenaga_kesehatan."_".$request->nama_stk,
            'lokasi' => $request->fasilitas_kesehatan."_".$request->lokasi_pelayanan,
            'berat_tinggi' => $request->berat."_".$request->tinggi,
            'kapsul_vit_a' => $vit_a,
            'imunisasi_d_lengkap' => $imun,
            'created_by' => $request->created_by
        ]);

        if($data->save()){
            return Redirect::route('detail-balita',[$request->id])->with('success','Berhasil Tambah Pelayanan Balita');        
        }  
        else{
            return Redirect::route('catat-balita')->with('danger','Gagal Tambah Pelayanan Balita');
        }
    }

    public function detailPelayanan($id){
        $pelayanan = Pelayanan::where('id',$id)->first();
        $balita = Balita::where('id',$pelayanan['id_spm'])->first();
        $datawarga = Warga::where('nik',$balita['nik'])->first();
        $pelayanan_all = Pelayanan::where('table_spm','balita')->where('id_spm',$balita['id'])->get();
        $batasperiode1 = date('Y-m-d', strtotime('+12 month', strtotime($datawarga['tgl_lahir'])));
        $batasperiode2 = date('Y-m-d', strtotime('+24 month', strtotime($datawarga['tgl_lahir'])));
        $batasperiode3 = date('Y-m-d', strtotime('+36 month', strtotime($datawarga['tgl_lahir'])));
        $batasperiode4 = date('Y-m-d', strtotime('+48 month', strtotime($datawarga['tgl_lahir'])));
        $batasperiode5 = date('Y-m-d', strtotime('+59 month', strtotime($datawarga['tgl_lahir'])));
        foreach($pelayanan_all as $i => $data_pelayanan){
            if($data_pelayanan != null){
                if($data_pelayanan['tanggal_pelayanan'] <= $batasperiode1 && $data_pelayanan['tanggal_pelayanan'] >= $datawarga->tgl_lahir){
                    $periode[$i] = 1;
                }else if($data_pelayanan['tanggal_pelayanan'] <= $batasperiode2 && $data_pelayanan['tanggal_pelayanan'] > $batasperiode1){
                    $periode[$i] = 2;
                }else if($data_pelayanan['tanggal_pelayanan'] <= $batasperiode3 && $data_pelayanan['tanggal_pelayanan'] > $batasperiode2){
                    $periode[$i] = 3;
                }else if($data_pelayanan['tanggal_pelayanan'] <= $batasperiode4 && $data_pelayanan['tanggal_pelayanan'] > $batasperiode3){
                    $periode[$i] = 4;
                }else if($data_pelayanan['tanggal_pelayanan'] <= $batasperiode5 && $data_pelayanan['tanggal_pelayanan'] > $batasperiode4){
                    $periode[$i] = 5;
                }
                if($data_pelayanan['berat_tinggi'] != null){
                    if(explode('_',$data_pelayanan['berat_tinggi'])[0] != null && explode('_',$data_pelayanan['berat_tinggi'])[0] > 0){
                        $capaian1[$i] = 1;
                    }
                    else{
                        $capaian1[$i] = 0;
                    }
                    if(explode('_',$data_pelayanan['berat_tinggi'])[1] != null && explode('_',$data_pelayanan['berat_tinggi'])[1] > 0){
                        $capaian2[$i] = 1;
                    }
                    else{
                        $capaian2[$i] = 0;
                    }
                }
                else{
                    $capaian1[$i] = 0;
                    $capaian2[$i] = 0;
                }
                if($data_pelayanan['kapsul_vit_a'] != null && $data_pelayanan['kapsul_vit_a'] == "y" ){
                    $capaian3[$i] = 1;
                }else{
                    $capaian3[$i] = 0;
                }
                if($data_pelayanan['imunisasi_d_lengkap'] != null && $data_pelayanan['imunisasi_d_lengkap'] == "y" ){
                    $capaian4[$i] = 1;
                }
                else{
                    $capaian4[$i] = 0;
                }
                $cap_pel_total[$i] = (($capaian1[$i]+$capaian2[$i]+$capaian3[$i]+$capaian4[$i])*100)/4;
            }
            else{
                $capaian1[$i] = 0;
                $capaian2[$i] = 0;
                $capaian3[$i] = 0;
                $capaian4[$i] = 0;
                $cap_pel_total[$i] =0;
            }
        }
        $capaian_berat1=0;$capaian_tinggi1=0;$capaian_kapsul1=0;$capaian_imunisasi1=0;
        $capaian_berat2=0;$capaian_tinggi2=0;$capaian_kapsul2=0;$capaian_imunisasi2=0;
        $capaian_berat3=0;$capaian_tinggi3=0;$capaian_kapsul3=0;$capaian_imunisasi3=0;
        $capaian_berat4=0;$capaian_tinggi4=0;$capaian_kapsul4=0;$capaian_imunisasi4=0;
        $capaian_berat5=0;$capaian_tinggi5=0;$capaian_kapsul5=0;$capaian_imunisasi5=0;
        foreach($pelayanan_all as $j => $pel){   
            if($pel['tanggal_pelayanan'] <= $batasperiode1 && $pel['tanggal_pelayanan'] >= $datawarga->tgl_lahir){
                if($capaian_berat1 < 8){$capaian_berat1= $capaian_berat1+$capaian1[$j];}
                if($capaian_tinggi1 < 2){$capaian_tinggi1= $capaian_tinggi1+$capaian2[$j];}
                if($capaian_kapsul1 < 2){$capaian_kapsul1= $capaian_kapsul1+$capaian3[$j];}
                if($capaian_imunisasi1 < 1){$capaian_imunisasi1= $capaian_imunisasi1+$capaian4[$j];}
            }else if($pel['tanggal_pelayanan'] <= $batasperiode2 && $pel['tanggal_pelayanan'] > $batasperiode1){
                if($capaian_berat2 < 8){$capaian_berat2= $capaian_berat2+$capaian1[$j];}
                if($capaian_tinggi2 < 2){$capaian_tinggi2= $capaian_tinggi2+$capaian2[$j];}
                if($capaian_kapsul2 < 2){$capaian_kapsul2= $capaian_kapsul2+$capaian3[$j];}
                if($capaian_imunisasi2 < 1){$capaian_imunisasi2= $capaian_imunisasi2+$capaian4[$j];}
            }else if($pel['tanggal_pelayanan'] <= $batasperiode3 && $pel['tanggal_pelayanan'] > $batasperiode2){
                if($capaian_berat3 < 8){$capaian_berat3= $capaian_berat3+$capaian1[$j];}
                if($capaian_tinggi3 < 2){$capaian_tinggi3= $capaian_tinggi3+$capaian2[$j];}
                if($capaian_kapsul3 < 2){$capaian_kapsul3= $capaian_kapsul3+$capaian3[$j];}
                if($capaian_imunisasi3 < 1){$capaian_imunisasi3= $capaian_imunisasi3+$capaian4[$j];}
            }else if($pel['tanggal_pelayanan'] <= $batasperiode4 && $pel['tanggal_pelayanan'] > $batasperiode3){
                if($capaian_berat4 < 8){$capaian_berat4= $capaian_berat4+$capaian1[$j];}
                if($capaian_tinggi4 < 2){$capaian_tinggi4= $capaian_tinggi4+$capaian2[$j];}
                if($capaian_kapsul4 < 2){$capaian_kapsul4= $capaian_kapsul4+$capaian3[$j];}
                if($capaian_imunisasi4 < 1){$capaian_imunisasi4= $capaian_imunisasi4+$capaian4[$j];}
            }else if($pel['tanggal_pelayanan'] <= $batasperiode5 && $pel['tanggal_pelayanan'] > $batasperiode4){
                if($capaian_berat5 < 8){$capaian_berat5= $capaian_berat5+$capaian1[$j];}
                if($capaian_tinggi5 < 2){$capaian_tinggi5= $capaian_tinggi5+$capaian2[$j];}
                if($capaian_kapsul5 < 2){$capaian_kapsul5= $capaian_kapsul5+$capaian3[$j];}
                if($capaian_imunisasi5 < 1){$capaian_imunisasi5= $capaian_imunisasi5+$capaian4[$j];}
            }
        }
        // print_r($capaian4);
        // print "<br>".$capaian_imunisasi1;
        $capaian_all_periode1 = ((($capaian_berat1*100)/8)+(($capaian_tinggi1*100)/2)+(($capaian_kapsul1*100)/2)+(($capaian_imunisasi1*100)/1))/4;
        $capaian_all_periode2 = ((($capaian_berat2*100)/8)+(($capaian_tinggi2*100)/2)+(($capaian_kapsul2*100)/2)+(($capaian_imunisasi2*100)/1))/4;
        $capaian_all_periode3 = ((($capaian_berat3*100)/8)+(($capaian_tinggi3*100)/2)+(($capaian_kapsul3*100)/2)+(($capaian_imunisasi3*100)/1))/4;
        $capaian_all_periode4 = ((($capaian_berat4*100)/8)+(($capaian_tinggi4*100)/2)+(($capaian_kapsul4*100)/2)+(($capaian_imunisasi4*100)/1))/4;
        $capaian_all_periode5 = ((($capaian_berat5*100)/8)+(($capaian_tinggi5*100)/2)+(($capaian_kapsul5*100)/2)+(($capaian_imunisasi5*100)/1))/4;
        $capaian_total = ($capaian_all_periode1+$capaian_all_periode2+$capaian_all_periode3+$capaian_all_periode4+$capaian_all_periode5)/5;
        return view('spm.balita.pelayanan.detail',compact('balita','datawarga','pelayanan','capaian_total'));
    }

    public function ubahPelayanan($id){
        $pelayanan = Pelayanan::where('id',$id)->first();
        $balita = Balita::where('id',$pelayanan['id_spm'])->first();
        $datawarga = Warga::where('nik',$balita['nik'])->first();
        $pelayanan_all = Pelayanan::where('table_spm','balita')->where('id_spm',$balita['id'])->get();
        $batasperiode1 = date('Y-m-d', strtotime('+12 month', strtotime($datawarga['tgl_lahir'])));
        $batasperiode2 = date('Y-m-d', strtotime('+24 month', strtotime($datawarga['tgl_lahir'])));
        $batasperiode3 = date('Y-m-d', strtotime('+36 month', strtotime($datawarga['tgl_lahir'])));
        $batasperiode4 = date('Y-m-d', strtotime('+48 month', strtotime($datawarga['tgl_lahir'])));
        $batasperiode5 = date('Y-m-d', strtotime('+59 month', strtotime($datawarga['tgl_lahir'])));
        foreach($pelayanan_all as $i => $data_pelayanan){
            if($data_pelayanan != null){
                if($data_pelayanan['tanggal_pelayanan'] <= $batasperiode1 && $data_pelayanan['tanggal_pelayanan'] >= $datawarga->tgl_lahir){
                    $periode[$i] = 1;
                }else if($data_pelayanan['tanggal_pelayanan'] <= $batasperiode2 && $data_pelayanan['tanggal_pelayanan'] > $batasperiode1){
                    $periode[$i] = 2;
                }else if($data_pelayanan['tanggal_pelayanan'] <= $batasperiode3 && $data_pelayanan['tanggal_pelayanan'] > $batasperiode2){
                    $periode[$i] = 3;
                }else if($data_pelayanan['tanggal_pelayanan'] <= $batasperiode4 && $data_pelayanan['tanggal_pelayanan'] > $batasperiode3){
                    $periode[$i] = 4;
                }else if($data_pelayanan['tanggal_pelayanan'] <= $batasperiode5 && $data_pelayanan['tanggal_pelayanan'] > $batasperiode4){
                    $periode[$i] = 5;
                }
                if($data_pelayanan['berat_tinggi'] != null){
                    if(explode('_',$data_pelayanan['berat_tinggi'])[0] != null && explode('_',$data_pelayanan['berat_tinggi'])[0] > 0){
                        $capaian1[$i] = 1;
                    }
                    else{
                        $capaian1[$i] = 0;
                    }
                    if(explode('_',$data_pelayanan['berat_tinggi'])[1] != null && explode('_',$data_pelayanan['berat_tinggi'])[1] > 0){
                        $capaian2[$i] = 1;
                    }
                    else{
                        $capaian2[$i] = 0;
                    }
                }
                else{
                    $capaian1[$i] = 0;
                    $capaian2[$i] = 0;
                }
                if($data_pelayanan['kapsul_vit_a'] != null && $data_pelayanan['kapsul_vit_a'] == "y" ){
                    $capaian3[$i] = 1;
                }else{
                    $capaian3[$i] = 0;
                }
                if($data_pelayanan['imunisasi_d_lengkap'] != null && $data_pelayanan['imunisasi_d_lengkap'] == "y" ){
                    $capaian4[$i] = 1;
                }
                else{
                    $capaian4[$i] = 0;
                }
                $cap_pel_total[$i] = (($capaian1[$i]+$capaian2[$i]+$capaian3[$i]+$capaian4[$i])*100)/4;
            }
            else{
                $capaian1[$i] = 0;
                $capaian2[$i] = 0;
                $capaian3[$i] = 0;
                $capaian4[$i] = 0;
                $cap_pel_total[$i] =0;
            }
        }
        $capaian_berat1=0;$capaian_tinggi1=0;$capaian_kapsul1=0;$capaian_imunisasi1=0;
        $capaian_berat2=0;$capaian_tinggi2=0;$capaian_kapsul2=0;$capaian_imunisasi2=0;
        $capaian_berat3=0;$capaian_tinggi3=0;$capaian_kapsul3=0;$capaian_imunisasi3=0;
        $capaian_berat4=0;$capaian_tinggi4=0;$capaian_kapsul4=0;$capaian_imunisasi4=0;
        $capaian_berat5=0;$capaian_tinggi5=0;$capaian_kapsul5=0;$capaian_imunisasi5=0;
        foreach($pelayanan_all as $j => $pel){   
            if($pel['tanggal_pelayanan'] <= $batasperiode1 && $pel['tanggal_pelayanan'] >= $datawarga->tgl_lahir){
                if($capaian_berat1 < 8){$capaian_berat1= $capaian_berat1+$capaian1[$j];}
                if($capaian_tinggi1 < 2){$capaian_tinggi1= $capaian_tinggi1+$capaian2[$j];}
                if($capaian_kapsul1 < 2){$capaian_kapsul1= $capaian_kapsul1+$capaian3[$j];}
                if($capaian_imunisasi1 < 1){$capaian_imunisasi1= $capaian_imunisasi1+$capaian4[$j];}
            }else if($pel['tanggal_pelayanan'] <= $batasperiode2 && $pel['tanggal_pelayanan'] > $batasperiode1){
                if($capaian_berat2 < 8){$capaian_berat2= $capaian_berat2+$capaian1[$j];}
                if($capaian_tinggi2 < 2){$capaian_tinggi2= $capaian_tinggi2+$capaian2[$j];}
                if($capaian_kapsul2 < 2){$capaian_kapsul2= $capaian_kapsul2+$capaian3[$j];}
                if($capaian_imunisasi2 < 1){$capaian_imunisasi2= $capaian_imunisasi2+$capaian4[$j];}
            }else if($pel['tanggal_pelayanan'] <= $batasperiode3 && $pel['tanggal_pelayanan'] > $batasperiode2){
                if($capaian_berat3 < 8){$capaian_berat3= $capaian_berat3+$capaian1[$j];}
                if($capaian_tinggi3 < 2){$capaian_tinggi3= $capaian_tinggi3+$capaian2[$j];}
                if($capaian_kapsul3 < 2){$capaian_kapsul3= $capaian_kapsul3+$capaian3[$j];}
                if($capaian_imunisasi3 < 1){$capaian_imunisasi3= $capaian_imunisasi3+$capaian4[$j];}
            }else if($pel['tanggal_pelayanan'] <= $batasperiode4 && $pel['tanggal_pelayanan'] > $batasperiode3){
                if($capaian_berat4 < 8){$capaian_berat4= $capaian_berat4+$capaian1[$j];}
                if($capaian_tinggi4 < 2){$capaian_tinggi4= $capaian_tinggi4+$capaian2[$j];}
                if($capaian_kapsul4 < 2){$capaian_kapsul4= $capaian_kapsul4+$capaian3[$j];}
                if($capaian_imunisasi4 < 1){$capaian_imunisasi4= $capaian_imunisasi4+$capaian4[$j];}
            }else if($pel['tanggal_pelayanan'] <= $batasperiode5 && $pel['tanggal_pelayanan'] > $batasperiode4){
                if($capaian_berat5 < 8){$capaian_berat5= $capaian_berat5+$capaian1[$j];}
                if($capaian_tinggi5 < 2){$capaian_tinggi5= $capaian_tinggi5+$capaian2[$j];}
                if($capaian_kapsul5 < 2){$capaian_kapsul5= $capaian_kapsul5+$capaian3[$j];}
                if($capaian_imunisasi5 < 1){$capaian_imunisasi5= $capaian_imunisasi5+$capaian4[$j];}
            }
        }
        // print_r($capaian4);
        // print "<br>".$capaian_imunisasi1;
        $capaian_all_periode1 = ((($capaian_berat1*100)/8)+(($capaian_tinggi1*100)/2)+(($capaian_kapsul1*100)/2)+(($capaian_imunisasi1*100)/1))/4;
        $capaian_all_periode2 = ((($capaian_berat2*100)/8)+(($capaian_tinggi2*100)/2)+(($capaian_kapsul2*100)/2)+(($capaian_imunisasi2*100)/1))/4;
        $capaian_all_periode3 = ((($capaian_berat3*100)/8)+(($capaian_tinggi3*100)/2)+(($capaian_kapsul3*100)/2)+(($capaian_imunisasi3*100)/1))/4;
        $capaian_all_periode4 = ((($capaian_berat4*100)/8)+(($capaian_tinggi4*100)/2)+(($capaian_kapsul4*100)/2)+(($capaian_imunisasi4*100)/1))/4;
        $capaian_all_periode5 = ((($capaian_berat5*100)/8)+(($capaian_tinggi5*100)/2)+(($capaian_kapsul5*100)/2)+(($capaian_imunisasi5*100)/1))/4;
        $capaian_total = ($capaian_all_periode1+$capaian_all_periode2+$capaian_all_periode3+$capaian_all_periode4+$capaian_all_periode5)/5;
        return view('spm.balita.pelayanan.ubah',compact('balita','datawarga','pelayanan_all','capaian_total','pelayanan'));
    }

    public function editPelayanan(Request $request){
        // dd($request);

        $pelayanan = Pelayanan::where('id',$request->id)->first();
        $a = date('Y-m-d', strtotime($request->tanggal_pelayanan));
        $fix_tgl = $a." ".$request->waktu_pelayanan;
        if($request->vita != null){$vit_a="y";}else{$vit_a="t";}
        if($request->imunisasi != null){$imun="y";}else{$imun="t";}

        $pelayanan['tanggal_pelayanan'] = $fix_tgl;
        $pelayanan['tenaga_kerja'] = $request->tenaga_kesehatan."/".$request->nama_stk;
        $pelayanan['lokasi'] = $request->fasilitas_kesehatan."_".$request->lokasi_pelayanan;
        $pelayanan['berat_tinggi'] = $request->berat."_".$request->tinggi;
        $pelayanan['kapsul_vit_a'] = $vit_a;
        $pelayanan['kapsul_vit_a'] = $imun;

        // dd($pelayanan);
        if($pelayanan->save()){
            return redirect('/data-balita')->with('alert', 'Berhasil Ubah Pelayanan Balita');
        }  
        else{
            return redirect('/balita')->with('alert', 'Gagal Ubah Pelayanan Balita');
        }
    }


    public function Usia($tgl1, $tgl2){
        $tgl1 = (is_string($tgl1) ? strtotime($tgl1) : $tgl1);
        $tgl2 = (is_string($tgl2) ? strtotime($tgl2) : $tgl2);
        $diff_secs = abs($tgl1-$tgl2);
        $base_year = min(date("Y",$tgl1), date("Y",$tgl2));
        $diff = mktime(0,0,$diff_secs,1,1,$base_year);
        return array(
            "years"=>date("Y",$diff) - $base_year, 
            "months_total" => (date("Y",$diff) - $base_year) * 12 + date("n",$diff) - 1, 
            "months" => date("n",$diff) -1, 
            "days_total" => floor($diff_secs/ (3600 * 24)), 
            "days" => date("j",$diff) - 1, 
            "hours_total" => floor($diff_secs/3600), 
            "hours" => date("G", $diff), 
            "minutes_total" => floor($diff_secs/60), 
            "minutes" => (int)date("i",$diff),
            "seconds_total" => $diff_secs, 
            "seconds" => (int) date("s",$diff)
        );
    }
}
