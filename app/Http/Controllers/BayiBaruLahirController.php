<?php

namespace App\Http\Controllers;
use DB;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Redirect;

use App\Warga;
use App\Pelayanan;
use App\BayiBaruLahir;

class BayiBaruLahirController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('spm.bayibarulahir.index');
    }
    
    public function data(){
        $total_bayi = 0;
        $total_sesuai_spm = 0;
        $total_tsesuai_spm = 0; 
        $total_tahap_pelayanan = 0;
        $data_bayi = BayiBaruLahir::all();

        // print $data_bayi;
        foreach($data_bayi as $index => $bayi){
            date_default_timezone_set('Asia/Jakarta');
            $usia_bayi[$index] =  BayiBaruLahirController::Usia($bayi->tgl_lahir,date("Y/m/d/h:i:s"));
            // print "Bayi ke ".$index." : ".$usia_bayi[$index]["months"]." Bulan"." ".$usia_bayi[$index]["days"]." Hari ==> ".$bayi->tgl_lahir."<br>";
            $total_bayi++;
            if($bayi->status_kelahiran == "Tahap Pelayanan"){
              $total_tahap_pelayanan++;
            }
            elseif($bayi->status_kelahiran == "Sesuai SPM"){
                $total_sesuai_spm++;
            }
            elseif($bayi->status_kelahiran == "Tidak Sesuai SPM"){
                $total_tsesuai_spm++;
            }
            $pelayanan[$index] = Pelayanan::where('table_spm', 'bayi_baru_lahir')->where('id_spm',$bayi->id)->get();
            //6 Jam
            $batas6jam = date('Y-m-d h:i:s', strtotime('+6 hour', strtotime($bayi->tgl_lahir)));
            //48 Jam / 2 Hari
            $batas2hari = date('Y-m-d h:i:s', strtotime('+2 day', strtotime($bayi->tgl_lahir)));
            //7 Hari
            $batas7hari = date('Y-m-d h:i:s', strtotime('+7 day', strtotime($bayi->tgl_lahir)));
            //28 Hari
            $batas28hari = date('Y-m-d h:i:s', strtotime('+28 day', strtotime($bayi->tgl_lahir)));
            
            //Untuk Mengetahui Presentase per Pelayanan
            foreach($pelayanan[$index] as $i => $pel){
                if($pel['suhu_tubuh'] != null){
                    $$pel['suhu_tubuh'] = explode(',',$pel['suhu_tubuh'])[0].".".explode(',',$pel['suhu_tubuh'])[1];
                    if(floatval($pel['suhu_tubuh']) >= floatval(36.4) && floatval($pel['suhu_tubuh']) <= floatval(37.5)){
                        $capaian0[$index][$i] = 1;
                    }else{
                        $capaian0[$index][$i] = 0;
                    }
                }
                else{
                    $capaian0[$index][$i] = 0;
                }
        
                if($pel['neo_pertama'] == null || $pel['neo_pertama'] == "_______" || $pel['neo_pertama'] == "t_t_t_t_t_t_t_t" ){
                    $capaian1[$index][$i] = 0;$capaian2[$index][$i] = 0;$capaian3[$index][$i] = 0;$capaian4[$index][$i] = 0;
                    $capaian5[$index][$i] = 0;$capaian6[$index][$i] = 0;$capaian7[$index][$i] = 0;$capaian8[$index][$i] = 0;
                }
                else{
                    if(explode("_",$pel['neo_pertama'])[0] == "y"){$capaian1[$index][$i] = 1;}else{$capaian1[$index][$i] = 0;}
                    if(explode("_",$pel['neo_pertama'])[1] == "y"){$capaian2[$index][$i] = 1;}else{$capaian2[$index][$i] = 0;}
                    if(explode("_",$pel['neo_pertama'])[2] == "y"){$capaian3[$index][$i] = 1;}else{$capaian3[$index][$i] = 0;}
                    if(explode("_",$pel['neo_pertama'])[3] == "y"){$capaian4[$index][$i] = 1;}else{$capaian4[$index][$i] = 0;}
                    if(explode("_",$pel['neo_pertama'])[4] == "y"){$capaian5[$index][$i] = 1;}else{$capaian5[$index][$i] = 0;}
                    if(explode("_",$pel['neo_pertama'])[5] == "y"){$capaian6[$index][$i] = 1;}else{$capaian6[$index][$i] = 0;}
                    if(explode("_",$pel['neo_pertama'])[6] == "y"){$capaian7[$index][$i] = 1;}else{$capaian7[$index][$i] = 0;}
                    if(explode("_",$pel['neo_pertama'])[7] == "y"){$capaian8[$index][$i] = 1;}else{$capaian8[$index][$i] = 0;}
        
                }
                
                if($pel['neo_kedua'] == null || $pel['neo_kedua'] == "_____" || $pel['neo_kedua'] == "t_t_t_t_t_t" ){
                    $capaian9[$index][$i] = 0;$capaian10[$index][$i] = 0;$capaian11[$index][$i] = 0;
                    $capaian12[$index][$i] = 0;$capaian13[$index][$i] = 0;$capaian14[$index][$i] = 0;
                }
                else{
                    if(explode("_",$pel['neo_kedua'])[0] == "y"){$capaian9[$index][$i] = 1;}else{$capaian9[$index][$i] = 0;}
                    if(explode("_",$pel['neo_kedua'])[1] == "y"){$capaian10[$index][$i] = 1;}else{$capaian10[$index][$i] = 0;}
                    if(explode("_",$pel['neo_kedua'])[2] == "y"){$capaian11[$index][$i] = 1;}else{$capaian11[$index][$i] = 0;}
                    if(explode("_",$pel['neo_kedua'])[3] == "y"){$capaian12[$index][$i] = 1;}else{$capaian12[$index][$i] = 0;}
                    if(explode("_",$pel['neo_kedua'])[4] == "y"){$capaian13[$index][$i] = 1;}else{$capaian13[$index][$i] = 0;}
                    if(explode("_",$pel['neo_kedua'])[5] == "y"){$capaian14[$index][$i] = 1;}else{$capaian14[$index][$i] = 0;}
                }
        
                if($pel['neo_fisik_apgar'] == null){
                    // nilai apgar 0
                    $nilai_apgar[$index][$i] = 0;
                }else{
                    if(explode("_",$pel['neo_fisik_apgar'])[4] == null){$apgar4[$index][$i] = 0;}
                    else if(explode("_",$pel['neo_fisik_apgar'])[4] >= 100){$apgar4[$index][$i] = 2;}
                    else if(explode("_",$pel['neo_fisik_apgar'])[4] < 100 && explode("_",$pel['neo_fisik_apgar'])[4] > 0){$apgar4[$index][$i] = 1;}
                    else if(explode("_",$pel['neo_fisik_apgar'])[4] <= 0){$apgar4[$index][$i] = 0;}
                    $nilai_apgar[$index][$i] =  explode("_",$pel['neo_fisik_apgar'])[0]+explode("_",$pel['neo_fisik_apgar'])[1]+
                                    explode("_",$pel['neo_fisik_apgar'])[2]+explode("_",$pel['neo_fisik_apgar'])[3]+$apgar4[$index][$i];                       
                }
        
                if($nilai_apgar[$index][$i] >= 7){
                    // cek geo
                    if($pel['neo_fisik_geo'] != null && $pel['neo_fisik_geo'] != "__" && explode("_",$pel['neo_fisik_geo'])[0] != 0 && explode("_",$pel['neo_fisik_geo'])[1] != 0 && explode("_",$pel['neo_fisik_geo'])[2] != 0 ){
                        //cek kl
                        if($pel['neo_fisik_kl'] != null && $pel['neo_fisik_kl'] != "____" && explode("_",$pel['neo_fisik_kl'])[0] == "y" && explode("_",$pel['neo_fisik_kl'])[1] == "y" && explode("_",$pel['neo_fisik_kl'])[2] == "y" && explode("_",$pel['neo_fisik_kl'])[3] == "y" && explode("_",$pel['neo_fisik_kl'])[4] == "y"){
                            //cek mulut
                            if($pel['neo_fisik_mulut'] != null && $pel['neo_fisik_mulut'] != "_" && explode("_",$pel['neo_fisik_mulut'])[0] == "y" && explode("_",$pel['neo_fisik_mulut'])[1] == "y"){
                                //cek pk
                                if($pel['neo_fisik_pk'] != null && $pel['neo_fisik_pk'] != "__" && explode("_",$pel['neo_fisik_pk'])[0] == "y" && explode("_",$pel['neo_fisik_pk'])[1] == "y" && explode("_",$pel['neo_fisik_pk'])[2] == "y"){
                                    //cek tbtk
                                    if($pel['neo_fisik_tbtk'] != null && $pel['neo_fisik_tbtk'] != "__" && explode("_",$pel['neo_fisik_tbtk'])[0] == "y" && explode("_",$pel['neo_fisik_tbtk'])[1] == "y" && explode("_",$pel['neo_fisik_tbtk'])[2] == "y"){
                                        //cek tbtk
                                        $capaian15[$index][$i] = 1;                        
                                    }else{
                                        $capaian15[$index][$i] = 0;
                                    }
                                }else{
                                    //gak perlu cek tbtk
                                    $capaian15[$index][$i] = 0;
                                }
                            }else{
                                //gak perlu cek pk dll
                                $capaian15[$index][$i] = 0;
                            }
                        }else{
                            //gak perlu cek mulut dll
                            $capaian15[$index][$i] = 0;
                        }
                    }else{
                        //gak perlu cek kl dll
                        $capaian15[$index][$i] = 0;
                    }
                }else{
                    //gak perlu cek geo dll
                    $capaian15[$index][$i] = 0;
                }
        
                if($pel['tenaga_kerja'] != null && $pel['tenaga_kerja'] != "_" && explode("_",$pel['tenaga_kerja'])[1] != null)
                {$capaian16[$index][$i] = 1;}else{$capaian16[$index][$i] = 0;}
                if($pel['lokasi'] != null && $pel['lokasi'] != "_" && explode("_",$pel['lokasi'])[1] != null)
                {$capaian17[$index][$i] = 1;}else{$capaian17[$index][$i] = 0;}
                if($pel['skr_shk_hsd'] != null){$capaian18[$index][$i] = 1;}else{$capaian18[$index][$i] = 0;}
                
                if($pel['kie_nama'] != null){$capaian19[$index][$i] = 1;}else{$capaian19[$index][$i] = 0;}
                if($pel['kie_informasi'] != null && $pel['kie_informasi'] != "t_t_t_t_t" ){
                    if(explode("_",$pel['kie_informasi'])[0] == "y"){$capaian20[$index][$i] = 1;}else{$capaian20[$index][$i] = 0;}
                    if(explode("_",$pel['kie_informasi'])[1] == "y"){$capaian21[$index][$i] = 1;}else{$capaian21[$index][$i] = 0;}
                    if(explode("_",$pel['kie_informasi'])[2] == "y"){$capaian22[$index][$i] = 1;}else{$capaian22[$index][$i] = 0;}
                    if(explode("_",$pel['kie_informasi'])[3] == "y"){$capaian23[$index][$i] = 1;}else{$capaian23[$index][$i] = 0;}
                    if(explode("_",$pel['kie_informasi'])[4] == "y"){$capaian24[$index][$i] = 1;}else{$capaian24[$index][$i] = 0;}            
                }else{
                    $capaian20[$index][$i] = 0;$capaian21[$index][$i] = 0;$capaian22[$index][$i] = 0;
                    $capaian23[$index][$i] = 0;$capaian24[$index][$i] = 0;
        
                }
                if($pel['kie_media'] != null){$capaian25[$index][$i] = 1;}else{$capaian25[$index][$i] = 0;}            
                
                if($pel['tanggal_pelayanan'] >= $batas6jam && $pel['tanggal_pelayanan'] < $batas2hari){
                    $cap_neo[$index] = (($capaian0[$index][$i]+$capaian1[$index][$i]
                    +$capaian2[$index][$i]+$capaian3[$index][$i]+$capaian4[$index][$i]
                    +$capaian5[$index][$i]+$capaian6[$index][$i]+$capaian7[$index][$i]
                    +$capaian8[$index][$i]+$capaian15[$index][$i]) *100) /10;
                    $form = "pertama";
                }
                else if($pel['tanggal_pelayanan'] > $batas2hari && $pel['tanggal_pelayanan'] < $batas28hari){
                    $cap_neo[$index] = (($capaian0[$index][$i]+$capaian9[$index][$i]
                    +$capaian10[$index][$i]+$capaian11[$index][$i]+$capaian12[$index][$i]
                    +$capaian13[$index][$i]+$capaian14[$index][$i]+$capaian15[$index][$i]) *100) /8;
                    $form = "kedua";
                }
                else{
                    $form ="non";
                    $cap_neo[$index] = 0;
                }
                $cap_skr[$index] = (($capaian16[$index][$i]+$capaian17[$index][$i]+$capaian18[$index][$i]) *100) /3;
                $cap_info[$index] = (($capaian19[$index][$i]+$capaian20[$index][$i]+$capaian21[$index][$i]+$capaian22[$index][$i]+$capaian23[$index][$i]
                +$capaian24[$index][$i]+$capaian25[$index][$i]) *100) /7;
                $cap_total[$index] = ($cap_neo[$index]+$cap_skr[$index]+$cap_info[$index])/3;
            }
            
            //Untuk Mengetahui Presentase per Periode
            $capaian0_periode1[$index] =0;$capaian0_periode2[$index] =0;$capaian0_periode3[$index] =0;
            $capaian1_periode1[$index] =0;$capaian1_periode2[$index] =0;$capaian1_periode3[$index] =0;
            $capaian2_periode1[$index] =0;$capaian2_periode2[$index] =0;$capaian2_periode3[$index] =0;
            $capaian3_periode1[$index] =0;$capaian3_periode2[$index] =0;$capaian3_periode3[$index] =0;
            $capaian4_periode1[$index] =0;$capaian4_periode2[$index] =0;$capaian4_periode3[$index] =0;
            $capaian5_periode1[$index] =0;$capaian5_periode2[$index] =0;$capaian5_periode3[$index] =0;
            $capaian6_periode1[$index] =0;$capaian6_periode2[$index] =0;$capaian6_periode3[$index] =0;
            $capaian7_periode1[$index] =0;$capaian7_periode2[$index] =0;$capaian7_periode3[$index] =0;
            $capaian8_periode1[$index] =0;$capaian8_periode2[$index] =0;$capaian8_periode3[$index] =0;
            $capaian9_periode1[$index] =0;$capaian9_periode2[$index] =0;$capaian9_periode3[$index] =0;
            $capaian10_periode1[$index] =0;$capaian10_periode2[$index] =0;$capaian10_periode3[$index] =0;
            $capaian11_periode1[$index] =0;$capaian11_periode2[$index] =0;$capaian11_periode3[$index] =0;
            $capaian12_periode1[$index] =0;$capaian12_periode2[$index] =0;$capaian12_periode3[$index] =0;
            $capaian13_periode1[$index] =0;$capaian13_periode2[$index] =0;$capaian13_periode3[$index] =0;
            $capaian14_periode1[$index] =0;$capaian14_periode2[$index] =0;$capaian14_periode3[$index] =0;
            $capaian15_periode1[$index] =0;$capaian15_periode2[$index] =0;$capaian15_periode3[$index] =0;
            $capaian16_periode1[$index] =0;$capaian16_periode2[$index] =0;$capaian16_periode3[$index] =0;
            $capaian17_periode1[$index] =0;$capaian17_periode2[$index] =0;$capaian17_periode3[$index] =0;
            $capaian18_periode1[$index] =0;$capaian18_periode2[$index] =0;$capaian18_periode3[$index] =0;
            $capaian19_periode1[$index] =0;$capaian19_periode2[$index] =0;$capaian19_periode3[$index] =0;
            $capaian20_periode1[$index] =0;$capaian20_periode2[$index] =0;$capaian20_periode3[$index] =0;
            $capaian21_periode1[$index] =0;$capaian21_periode2[$index] =0;$capaian21_periode3[$index] =0;
            $capaian22_periode1[$index] =0;$capaian22_periode2[$index] =0;$capaian22_periode3[$index] =0;
            $capaian23_periode1[$index] =0;$capaian23_periode2[$index] =0;$capaian23_periode3[$index] =0;
            $capaian24_periode1[$index] =0;$capaian24_periode2[$index] =0;$capaian24_periode3[$index] =0;
            $capaian25_periode1[$index] =0;$capaian25_periode2[$index] =0;$capaian25_periode3[$index] =0;
            foreach($pelayanan[$index] as $i => $pel){
                if($pel['tanggal_pelayanan'] > $batas6jam && $pel['tanggal_pelayanan'] < $batas2hari){
                    if($capaian0[$index][$i] < 1){$capaian0_periode1[$index] = $capaian0_periode1[$index] + $capaian0[$index][$i];}
                    if($capaian1[$index][$i] < 1){$capaian1_periode1[$index] = $capaian1_periode1[$index] + $capaian1[$index][$i];}
                    if($capaian2[$index][$i] < 1){$capaian2_periode1[$index] = $capaian2_periode1[$index] + $capaian2[$index][$i];}
                    if($capaian3[$index][$i] < 1){$capaian3_periode1[$index] = $capaian3_periode1[$index] + $capaian3[$index][$i];}
                    if($capaian4[$index][$i] < 1){$capaian4_periode1[$index] = $capaian4_periode1[$index] + $capaian4[$index][$i];}
                    if($capaian5[$index][$i] < 1){$capaian5_periode1[$index] = $capaian5_periode1[$index] + $capaian5[$index][$i];}
                    if($capaian6[$index][$i] < 1){$capaian6_periode1[$index] = $capaian6_periode1[$index] + $capaian6[$index][$i];}
                    if($capaian7[$index][$i] < 1){$capaian7_periode1[$index] = $capaian7_periode1[$index] + $capaian7[$index][$i];}
                    if($capaian8[$index][$i] < 1){$capaian8_periode1[$index] = $capaian8_periode1[$index] + $capaian8[$index][$i];}
                    if($capaian9[$index][$i] < 1){$capaian9_periode1[$index] = $capaian9_periode1[$index] + $capaian9[$index][$i];}
                    if($capaian10[$index][$i] < 1){$capaian10_periode1[$index] = $capaian10_periode1[$index] + $capaian10[$index][$i];}
                    if($capaian11[$index][$i] < 1){$capaian11_periode1[$index] = $capaian11_periode1[$index] + $capaian11[$index][$i];}
                    if($capaian12[$index][$i] < 1){$capaian12_periode1[$index] = $capaian12_periode1[$index] + $capaian12[$index][$i];}
                    if($capaian13[$index][$i] < 1){$capaian13_periode1[$index] = $capaian13_periode1[$index] + $capaian13[$index][$i];}
                    if($capaian14[$index][$i] < 1){$capaian14_periode1[$index] = $capaian14_periode1[$index] + $capaian14[$index][$i];}
                    if($capaian15[$index][$i] < 1){$capaian15_periode1[$index] = $capaian15_periode1[$index] + $capaian15[$index][$i];}
                    if($capaian16[$index][$i] < 1){$capaian16_periode1[$index] = $capaian16_periode1[$index] + $capaian16[$index][$i];}
                    if($capaian17[$index][$i] < 1){$capaian17_periode1[$index] = $capaian17_periode1[$index] + $capaian17[$index][$i];}
                    if($capaian18[$index][$i] < 1){$capaian18_periode1[$index] = $capaian18_periode1[$index] + $capaian18[$index][$i];}
                    if($capaian19[$index][$i] < 1){$capaian19_periode1[$index] = $capaian19_periode1[$index] + $capaian19[$index][$i];}
                    if($capaian20[$index][$i] < 1){$capaian20_periode1[$index] = $capaian20_periode1[$index] + $capaian20[$index][$i];}
                    if($capaian21[$index][$i] < 1){$capaian21_periode1[$index] = $capaian21_periode1[$index] + $capaian21[$index][$i];}
                    if($capaian22[$index][$i] < 1){$capaian22_periode1[$index] = $capaian22_periode1[$index] + $capaian22[$index][$i];}
                    if($capaian23[$index][$i] < 1){$capaian23_periode1[$index] = $capaian23_periode1[$index] + $capaian23[$index][$i];}
                    if($capaian24[$index][$i] < 1){$capaian24_periode1[$index] = $capaian24_periode1[$index] + $capaian24[$index][$i];}
                    if($capaian25[$index][$i] < 1){$capaian25_periode1[$index] = $capaian25_periode1[$index] + $capaian25[$index][$i];}
                }
                else if($pel['tanggal_pelayanan'] > $batas2hari && $pel['tanggal_pelayanan'] < $batas7hari){
                    if($capaian0[$index][$i] < 1){$capaian0_periode2[$index] = $capaian0_periode2[$index] + $capaian0[$index][$i];}
                    if($capaian1[$index][$i] < 1){$capaian1_periode2[$index] = $capaian1_periode2[$index] + $capaian1[$index][$i];}
                    if($capaian2[$index][$i] < 1){$capaian2_periode2[$index] = $capaian2_periode2[$index] + $capaian2[$index][$i];}
                    if($capaian3[$index][$i] < 1){$capaian3_periode2[$index] = $capaian3_periode2[$index] + $capaian3[$index][$i];}
                    if($capaian4[$index][$i] < 1){$capaian4_periode2[$index] = $capaian4_periode2[$index] + $capaian4[$index][$i];}
                    if($capaian5[$index][$i] < 1){$capaian5_periode2[$index] = $capaian5_periode2[$index] + $capaian5[$index][$i];}
                    if($capaian6[$index][$i] < 1){$capaian6_periode2[$index] = $capaian6_periode2[$index] + $capaian6[$index][$i];}
                    if($capaian7[$index][$i] < 1){$capaian7_periode2[$index] = $capaian7_periode2[$index] + $capaian7[$index][$i];}
                    if($capaian8[$index][$i] < 1){$capaian8_periode2[$index] = $capaian8_periode2[$index] + $capaian8[$index][$i];}
                    if($capaian9[$index][$i] < 1){$capaian9_periode2[$index] = $capaian9_periode2[$index] + $capaian9[$index][$i];}
                    if($capaian10[$index][$i] < 1){$capaian10_periode2[$index] = $capaian10_periode2[$index] + $capaian10[$index][$i];}
                    if($capaian11[$index][$i] < 1){$capaian11_periode2[$index] = $capaian11_periode2[$index] + $capaian11[$index][$i];}
                    if($capaian12[$index][$i] < 1){$capaian12_periode2[$index] = $capaian12_periode2[$index] + $capaian12[$index][$i];}
                    if($capaian13[$index][$i] < 1){$capaian13_periode2[$index] = $capaian13_periode2[$index] + $capaian13[$index][$i];}
                    if($capaian14[$index][$i] < 1){$capaian14_periode2[$index] = $capaian14_periode2[$index] + $capaian14[$index][$i];}
                    if($capaian15[$index][$i] < 1){$capaian15_periode2[$index] = $capaian15_periode2[$index] + $capaian15[$index][$i];}
                    if($capaian16[$index][$i] < 1){$capaian16_periode2[$index] = $capaian16_periode2[$index] + $capaian16[$index][$i];}
                    if($capaian17[$index][$i] < 1){$capaian17_periode2[$index] = $capaian17_periode2[$index] + $capaian17[$index][$i];}
                    if($capaian18[$index][$i] < 1){$capaian18_periode2[$index] = $capaian18_periode2[$index] + $capaian18[$index][$i];}
                    if($capaian19[$index][$i] < 1){$capaian19_periode2[$index] = $capaian19_periode2[$index] + $capaian19[$index][$i];}
                    if($capaian20[$index][$i] < 1){$capaian20_periode2[$index] = $capaian20_periode2[$index] + $capaian20[$index][$i];}
                    if($capaian21[$index][$i] < 1){$capaian21_periode2[$index] = $capaian21_periode2[$index] + $capaian21[$index][$i];}
                    if($capaian22[$index][$i] < 1){$capaian22_periode2[$index] = $capaian22_periode2[$index] + $capaian22[$index][$i];}
                    if($capaian23[$index][$i] < 1){$capaian23_periode2[$index] = $capaian23_periode2[$index] + $capaian23[$index][$i];}
                    if($capaian24[$index][$i] < 1){$capaian24_periode2[$index] = $capaian24_periode2[$index] + $capaian24[$index][$i];}
                    if($capaian25[$index][$i] < 1){$capaian25_periode2[$index] = $capaian25_periode2[$index] + $capaian25[$index][$i];}
                }
                else if($pel['tanggal_pelayanan'] > $batas7hari && $pel['tanggal_pelayanan'] < $batas28hari){
                    if($capaian0[$index][$i] < 1){$capaian0_periode3[$index] = $capaian0_periode3[$index] + $capaian0[$index][$i];}
                    if($capaian1[$index][$i] < 1){$capaian1_periode3[$index] = $capaian1_periode3[$index] + $capaian1[$index][$i];}
                    if($capaian2[$index][$i] < 1){$capaian2_periode3[$index] = $capaian2_periode3[$index] + $capaian2[$index][$i];}
                    if($capaian3[$index][$i] < 1){$capaian3_periode3[$index] = $capaian3_periode3[$index] + $capaian3[$index][$i];}
                    if($capaian4[$index][$i] < 1){$capaian4_periode3[$index] = $capaian4_periode3[$index] + $capaian4[$index][$i];}
                    if($capaian5[$index][$i] < 1){$capaian5_periode3[$index] = $capaian5_periode3[$index] + $capaian5[$index][$i];}
                    if($capaian6[$index][$i] < 1){$capaian6_periode3[$index] = $capaian6_periode3[$index] + $capaian6[$index][$i];}
                    if($capaian7[$index][$i] < 1){$capaian7_periode3[$index] = $capaian7_periode3[$index] + $capaian7[$index][$i];}
                    if($capaian8[$index][$i] < 1){$capaian8_periode3[$index] = $capaian8_periode3[$index] + $capaian8[$index][$i];}
                    if($capaian9[$index][$i] < 1){$capaian9_periode3[$index] = $capaian9_periode3[$index] + $capaian9[$index][$i];}
                    if($capaian10[$index][$i] < 1){$capaian10_periode3[$index] = $capaian10_periode3[$index] + $capaian10[$index][$i];}
                    if($capaian11[$index][$i] < 1){$capaian11_periode3[$index] = $capaian11_periode3[$index] + $capaian11[$index][$i];}
                    if($capaian12[$index][$i] < 1){$capaian12_periode3[$index] = $capaian12_periode3[$index] + $capaian12[$index][$i];}
                    if($capaian13[$index][$i] < 1){$capaian13_periode3[$index] = $capaian13_periode3[$index] + $capaian13[$index][$i];}
                    if($capaian14[$index][$i] < 1){$capaian14_periode3[$index] = $capaian14_periode3[$index] + $capaian14[$index][$i];}
                    if($capaian15[$index][$i] < 1){$capaian15_periode3[$index] = $capaian15_periode3[$index] + $capaian15[$index][$i];}
                    if($capaian16[$index][$i] < 1){$capaian16_periode3[$index] = $capaian16_periode3[$index] + $capaian16[$index][$i];}
                    if($capaian17[$index][$i] < 1){$capaian17_periode3[$index] = $capaian17_periode3[$index] + $capaian17[$index][$i];}
                    if($capaian18[$index][$i] < 1){$capaian18_periode3[$index] = $capaian18_periode3[$index] + $capaian18[$index][$i];}
                    if($capaian19[$index][$i] < 1){$capaian19_periode3[$index] = $capaian19_periode3[$index] + $capaian19[$index][$i];}
                    if($capaian20[$index][$i] < 1){$capaian20_periode3[$index] = $capaian20_periode3[$index] + $capaian20[$index][$i];}
                    if($capaian21[$index][$i] < 1){$capaian21_periode3[$index] = $capaian21_periode3[$index] + $capaian21[$index][$i];}
                    if($capaian22[$index][$i] < 1){$capaian22_periode3[$index] = $capaian22_periode3[$index] + $capaian22[$index][$i];}
                    if($capaian23[$index][$i] < 1){$capaian23_periode3[$index] = $capaian23_periode3[$index] + $capaian23[$index][$i];}
                    if($capaian24[$index][$i] < 1){$capaian24_periode3[$index] = $capaian24_periode3[$index] + $capaian24[$index][$i];}
                    if($capaian25[$index][$i] < 1){$capaian25_periode3[$index] = $capaian25_periode3[$index] + $capaian25[$index][$i];}
                }
            }
            
            $capaian_all_pel_periode1 = (((($capaian0_periode1[$index]+$capaian1_periode1[$index]+
                $capaian2_periode1[$index]+$capaian3_periode1[$index]+$capaian4_periode1[$index]+
                $capaian5_periode1[$index]+$capaian6_periode1[$index]+$capaian7_periode1[$index]+
                $capaian8_periode1[$index]+$capaian15_periode1[$index])*100) /10)+
                ((($capaian16_periode1[$index]+$capaian17_periode1[$index]+$capaian18_periode1[$index])*100)/3)+
                ((($capaian19_periode1[$index]+$capaian20_periode1[$index]+$capaian21_periode1[$index]+$capaian22_periode1[$index]+
                $capaian23_periode1[$index]+$capaian24_periode1[$index]+$capaian25_periode1[$index])*100) /7) )/3;

            $capaian_all_pel_periode2 = (((($capaian0_periode2[$index]+$capaian9_periode2[$index]+
                $capaian10_periode2[$index]+$capaian11_periode2[$index]+$capaian12_periode2[$index]+
                $capaian13_periode2[$index]+$capaian14_periode2[$index]+$capaian15_periode2[$index])*100) /8)+
                ((($capaian16_periode2[$index]+$capaian17_periode2[$index]+$capaian18_periode2[$index])*100)/3)+
                ((($capaian19_periode2[$index]+$capaian20_periode2[$index]+$capaian21_periode2[$index]+$capaian22_periode2[$index]+
                $capaian23_periode2[$index]+$capaian24_periode2[$index]+$capaian25_periode2[$index])*100) /7) )/3;

            $capaian_all_pel_periode3 = (((($capaian0_periode3[$index]+$capaian9_periode3[$index]+
                $capaian10_periode3[$index]+$capaian11_periode3[$index]+$capaian12_periode3[$index]+
                $capaian13_periode3[$index]+$capaian14_periode3[$index]+$capaian15_periode3[$index])*100) /8)+
                ((($capaian16_periode3[$index]+$capaian17_periode3[$index]+$capaian18_periode3[$index])*100)/3)+
                ((($capaian19_periode3[$index]+$capaian20_periode3[$index]+$capaian21_periode3[$index]+$capaian22_periode3[$index]+
                $capaian23_periode3[$index]+$capaian24_periode3[$index]+$capaian25_periode3[$index])*100) /7) )/3;    

                $capaian[$index] = number_format(($capaian_all_pel_periode1+$capaian_all_pel_periode2+$capaian_all_pel_periode3)/3,2);            
        }
        // $data_bayi = DB::table('spm_bayi_baru_lahir')
        // ->join('warga','spm_bayi_baru_lahir.nik','=','warga.nik')
        // ->select('spm_bayi_baru_lahir.*','warga.*')
        // ->get();
        // // $pelayanan = Pelayanan::all();
        // foreach($data_bayi as $index => $bayi){
        //     date_default_timezone_set('Asia/Jakarta');
        //     $usia_bayi[$index] =  BayiBaruLahirController::Usia($bayi->tgl_lahir,date("Y/m/d/h:i:s"));
        //     // print "Bayi ke ".$index." : ".$usia_bayi[$index]["months"]." Bulan"." ".$usia_bayi[$index]["days"]." Hari ==> ".$bayi->tgl_lahir."<br>";
        //     $total_bayi++;
        //     if($bayi->status_kelahiran == "Tahap Pelayanan"){
        //       $total_tahap_pelayanan++;
        //     }
        //     elseif($bayi->status_kelahiran == "Sesuai SPM"){
        //         $total_sesuai_spm++;
        //     }
        //     elseif($bayi->status_kelahiran == "Tidak Sesuai SPM"){
        //         $total_tsesuai_spm++;
        //     }
        //     $pelayanan[$index] = Pelayanan::where('table_spm', 'bayi_baru_lahir')->where('id_spm',$bayi->id)->get();
        //     //6 Jam
        //     $batas6jam = date('Y-m-d h:i:s', strtotime('+6 hour', strtotime($bayi->tgl_lahir)));
        //     //48 Jam / 2 Hari
        //     $batas2hari = date('Y-m-d h:i:s', strtotime('+2 day', strtotime($bayi->tgl_lahir)));
        //     //7 Hari
        //     $batas7hari = date('Y-m-d h:i:s', strtotime('+7 day', strtotime($bayi->tgl_lahir)));
        //     //28 Hari
        //     $batas28hari = date('Y-m-d h:i:s', strtotime('+28 day', strtotime($bayi->tgl_lahir)));
            
        //     foreach($pelayanan[$index] as $i => $pel){
        //         if($pel['suhu_tubuh'] != null){
        //             $$pel['suhu_tubuh'] = explode(',',$pel['suhu_tubuh'])[0].".".explode(',',$pel['suhu_tubuh'])[1];
        //             if(floatval($pel['suhu_tubuh']) >= floatval(36.4) && floatval($pel['suhu_tubuh']) <= floatval(37.5)){
        //                 $capaian0 = 1;
        //             }else{
        //                 $capaian0 = 0;
        //             }
        //         }
        //         else{
        //             $capaian0 = 0;
        //         }
        
        //         if($pel['neo_pertama'] == null || $pel['neo_pertama'] == "_______" || $pel['neo_pertama'] == "t_t_t_t_t_t_t_t" ){
        //             $capaian1 = 0;$capaian2 = 0;$capaian3 = 0;$capaian4 = 0;
        //             $capaian5 = 0;$capaian6 = 0;$capaian7 = 0;$capaian8 = 0;
        //         }
        //         else{
        //             if(explode("_",$pel['neo_pertama'])[0] == "y"){$capaian1 = 1;}else{$capaian1 = 0;}
        //             if(explode("_",$pel['neo_pertama'])[1] == "y"){$capaian2 = 1;}else{$capaian2 = 0;}
        //             if(explode("_",$pel['neo_pertama'])[2] == "y"){$capaian3 = 1;}else{$capaian3 = 0;}
        //             if(explode("_",$pel['neo_pertama'])[3] == "y"){$capaian4 = 1;}else{$capaian4 = 0;}
        //             if(explode("_",$pel['neo_pertama'])[4] == "y"){$capaian5 = 1;}else{$capaian5 = 0;}
        //             if(explode("_",$pel['neo_pertama'])[5] == "y"){$capaian6 = 1;}else{$capaian6 = 0;}
        //             if(explode("_",$pel['neo_pertama'])[6] == "y"){$capaian7 = 1;}else{$capaian7 = 0;}
        //             if(explode("_",$pel['neo_pertama'])[7] == "y"){$capaian8 = 1;}else{$capaian8 = 0;}
        
        //         }
                
        //         if($pel['neo_kedua'] == null || $pel['neo_kedua'] == "_____" || $pel['neo_kedua'] == "t_t_t_t_t_t" ){
        //             $capaian9 = 0;$capaian10 = 0;$capaian11 = 0;
        //             $capaian12 = 0;$capaian13 = 0;$capaian14 = 0;
        //         }
        //         else{
        //             if(explode("_",$pel['neo_kedua'])[0] == "y"){$capaian9 = 1;}else{$capaian9 = 0;}
        //             if(explode("_",$pel['neo_kedua'])[1] == "y"){$capaian10 = 1;}else{$capaian10 = 0;}
        //             if(explode("_",$pel['neo_kedua'])[2] == "y"){$capaian11 = 1;}else{$capaian11 = 0;}
        //             if(explode("_",$pel['neo_kedua'])[3] == "y"){$capaian12 = 1;}else{$capaian12 = 0;}
        //             if(explode("_",$pel['neo_kedua'])[4] == "y"){$capaian13 = 1;}else{$capaian13 = 0;}
        //             if(explode("_",$pel['neo_kedua'])[5] == "y"){$capaian14 = 1;}else{$capaian14 = 0;}
        //         }
        
        //         if($pel['neo_fisik_apgar'] == null){
        //             // nilai apgar 0
        //             $nilai_apgar = 0;
        //         }else{
        //             if(explode("_",$pel['neo_fisik_apgar'])[4] == null){$apgar4 = 0;}
        //             else if(explode("_",$pel['neo_fisik_apgar'])[4] >= 100){$apgar4 = 2;}
        //             else if(explode("_",$pel['neo_fisik_apgar'])[4] < 100 && explode("_",$pel['neo_fisik_apgar'])[4] > 0){$apgar4 = 1;}
        //             else if(explode("_",$pel['neo_fisik_apgar'])[4] <= 0){$apgar4 = 0;}
        //             $nilai_apgar =  explode("_",$pel['neo_fisik_apgar'])[0]+explode("_",$pel['neo_fisik_apgar'])[1]+
        //                             explode("_",$pel['neo_fisik_apgar'])[2]+explode("_",$pel['neo_fisik_apgar'])[3]+$apgar4;                       
        //         }
        
        //         if($nilai_apgar >= 7){
        //             // cek geo
        //             if($pel['neo_fisik_geo'] != null && $pel['neo_fisik_geo'] != "__" && explode("_",$pel['neo_fisik_geo'])[0] != 0 && explode("_",$pel['neo_fisik_geo'])[1] != 0 && explode("_",$pel['neo_fisik_geo'])[2] != 0 ){
        //                 //cek kl
        //                 if($pel['neo_fisik_kl'] != null && $pel['neo_fisik_kl'] != "____" && explode("_",$pel['neo_fisik_kl'])[0] == "y" && explode("_",$pel['neo_fisik_kl'])[1] == "y" && explode("_",$pel['neo_fisik_kl'])[2] == "y" && explode("_",$pel['neo_fisik_kl'])[3] == "y" && explode("_",$pel['neo_fisik_kl'])[4] == "y"){
        //                     //cek mulut
        //                     if($pel['neo_fisik_mulut'] != null && $pel['neo_fisik_mulut'] != "_" && explode("_",$pel['neo_fisik_mulut'])[0] == "y" && explode("_",$pel['neo_fisik_mulut'])[1] == "y"){
        //                         //cek pk
        //                         if($pel['neo_fisik_pk'] != null && $pel['neo_fisik_pk'] != "__" && explode("_",$pel['neo_fisik_pk'])[0] == "y" && explode("_",$pel['neo_fisik_pk'])[1] == "y" && explode("_",$pel['neo_fisik_pk'])[2] == "y"){
        //                             //cek tbtk
        //                             if($pel['neo_fisik_tbtk'] != null && $pel['neo_fisik_tbtk'] != "__" && explode("_",$pel['neo_fisik_tbtk'])[0] == "y" && explode("_",$pel['neo_fisik_tbtk'])[1] == "y" && explode("_",$pel['neo_fisik_tbtk'])[2] == "y"){
        //                                 //cek tbtk
        //                                 $capaian15 = 1;                        
        //                             }else{
        //                                 $capaian15 = 0;
        //                             }
        //                         }else{
        //                             //gak perlu cek tbtk
        //                             $capaian15 = 0;
        //                         }
        //                     }else{
        //                         //gak perlu cek pk dll
        //                         $capaian15 = 0;
        //                     }
        //                 }else{
        //                     //gak perlu cek mulut dll
        //                     $capaian15 = 0;
        //                 }
        //             }else{
        //                 //gak perlu cek kl dll
        //                 $capaian15 = 0;
        //             }
        //         }else{
        //             //gak perlu cek geo dll
        //             $capaian15 = 0;
        //         }
        
        //         if($pel['tenaga_kerja'] != null && $pel['tenaga_kerja'] != "_" && explode("_",$pel['tenaga_kerja'])[1] != null)
        //         {$capaian16 = 1;}else{$capaian16 = 0;}
        //         if($pel['lokasi'] != null && $pel['lokasi'] != "_" && explode("_",$pel['lokasi'])[1] != null)
        //         {$capaian17 = 1;}else{$capaian17 = 0;}
        //         if($pel['skr_shk_hsd'] != null){$capaian18 = 1;}else{$capaian18 = 0;}
                
        //         if($pel['kie_nama'] != null){$capaian19 = 1;}else{$capaian19 = 0;}
        //         if($pel['kie_informasi'] != null && $pel['kie_informasi'] != "t_t_t_t_t" ){
        //             if(explode("_",$pel['kie_informasi'])[0] == "y"){$capaian20 = 1;}else{$capaian20 = 0;}
        //             if(explode("_",$pel['kie_informasi'])[1] == "y"){$capaian21 = 1;}else{$capaian21 = 0;}
        //             if(explode("_",$pel['kie_informasi'])[2] == "y"){$capaian22 = 1;}else{$capaian22 = 0;}
        //             if(explode("_",$pel['kie_informasi'])[3] == "y"){$capaian23 = 1;}else{$capaian23 = 0;}
        //             if(explode("_",$pel['kie_informasi'])[4] == "y"){$capaian24 = 1;}else{$capaian24 = 0;}            
        //         }else{
        //             $capaian20 = 0;$capaian21 = 0;$capaian22 = 0;
        //             $capaian23 = 0;$capaian24 = 0;
        
        //         }
        //         if($pel['kie_media'] != null){$capaian25 = 1;}else{$capaian25 = 0;}            
                
        //         if($pel['tanggal_pelayanan'] >= $batas6jam && $pel['tanggal_pelayanan'] < $batas2hari){
        //             $cap_neo[$index] = (($capaian0+$capaian1+$capaian2+$capaian3+$capaian4+$capaian5+$capaian6+$capaian7+$capaian8+$capaian15) *100) /10;
        //             $form = "pertama";
        //         }
        //         else if($pel['tanggal_pelayanan'] > $batas2hari && $pel['tanggal_pelayanan'] < $batas28hari){
        //             $cap_neo[$index] = (($capaian0+$capaian9+$capaian10+$capaian11+$capaian12+$capaian13+$capaian14+$capaian15) *100) /8;
        //             $form = "kedua";
        //         }
        //         else{
        //             $form ="non";
        //             $cap_neo[$index] = 0;
        //         }
        //         $cap_skr[$index] = (($capaian16+$capaian17+$capaian18) *100) /3;
        //         $cap_info[$index] = (($capaian19+$capaian20+$capaian21+$capaian22+$capaian23+$capaian24+$capaian25) *100) /7;
        //         $cap_total[$index] = ($cap_neo[$index]+$cap_skr[$index]+$cap_info[$index])/3;
        //     }
        //     $pel = []; $pel2 = []; $pel3 = [];
        //     if($pelayanan[$index] != null){
        //         foreach($pelayanan[$index] as $i => $pel){  
        //             if($pel['tanggal_pelayanan'] > $batas6jam && $pel['tanggal_pelayanan'] < $batas2hari){
        //                 $CapaianPelayananperiode1[$i] = BayiBaruLahirController::CekCapaian($pel['suhu_tubuh'],$pel['neo_pertama'],$pel['neo_kedua'],$pel['neo_fisik_apgar'],
        //                 $pel['neo_fisik_geo'],$pel['neo_fisik_kl'],$pel['neo_fisik_mulut'],$pel['neo_fisik_pk'],$pel['neo_fisik_tbtk'],
        //                 $pel['tenaga_kerja'],$pel['lokasi'],$pel['skr_shk_hsd'],$pel['kie_nama'],$pel['kie_informasi'],$pel['kie_media'],$i);
        //             }
        //             else if($pel['tanggal_pelayanan'] > $batas2hari && $pel['tanggal_pelayanan'] < $batas7hari){
        //                 $CapaianPelayananperiode2[$i] = BayiBaruLahirController::CekCapaian($pel['suhu_tubuh'],$pel['neo_pertama'],$pel['neo_kedua'],$pel['neo_fisik_apgar'],
        //                 $pel['neo_fisik_geo'],$pel['neo_fisik_kl'],$pel['neo_fisik_mulut'],$pel['neo_fisik_pk'],$pel['neo_fisik_tbtk'],
        //                 $pel['tenaga_kerja'],$pel['lokasi'],$pel['skr_shk_hsd'],$pel['kie_nama'],$pel['kie_informasi'],$pel['kie_media'],$i);
        //             }
        //             else if($pel['tanggal_pelayanan'] > $batas7hari && $pel['tanggal_pelayanan'] < $batas28hari){
        //                 $CapaianPelayananperiode3[$i] = BayiBaruLahirController::CekCapaian($pel['suhu_tubuh'],$pel['neo_pertama'],$pel['neo_kedua'],$pel['neo_fisik_apgar'],
        //                 $pel['neo_fisik_geo'],$pel['neo_fisik_kl'],$pel['neo_fisik_mulut'],$pel['neo_fisik_pk'],$pel['neo_fisik_tbtk'],
        //                 $pel['tenaga_kerja'],$pel['lokasi'],$pel['skr_shk_hsd'],$pel['kie_nama'],$pel['kie_informasi'],$pel['kie_media'],$i);
        //             }
        //         }

        //         $kri = [];
        //         $kri2 = [];
        //         $kri3 = [];
        //         $kri[0] = 0;
        //         $kri[1] = 0;
        //         $kri[2] = 0;
        //         $kri[3] = 0;
        //         $kri[4] = 0;
        //         $kri[5] = 0;
        //         $kri[6] = 0;
        //         $kri[7] = 0;
        //         $kri[8] = 0;
        //         $kri[9] = 0;
        //         $kri[10] = 0;
        //         $kri[11] = 0;
        //         $kri[12] = 0;
        //         $kri[13] = 0;
        //         $kri[14] = 0;
        //         $kri[15] = 0;
        //         $kri[16] = 0;
        //         $kri[17] = 0;
        //         $kri[18] = 0;
        //         $kri[19] = 0;
        //         $kri[20] = 0;
        //         $kri[21] = 0;
        //         $kri[22] = 0;
        //         $kri[23] = 0;
        //         $kri[24] = 0;
        //         $kri[25] = 0;
        //         $kri[26] = 0;

        //         $kri2[0] = 0;
        //         $kri[1] = 0;
        //         $kri2[2] = 0;
        //         $kri2[3] = 0;
        //         $kri2[4] = 0;
        //         $kri2[5] = 0;
        //         $kri2[6] = 0;
        //         $kri2[7] = 0;
        //         $kri2[8] = 0;
        //         $kri2[9] = 0;
        //         $kri2[10] = 0;
        //         $kri2[11] = 0;
        //         $kri2[12] = 0;
        //         $kri2[13] = 0;
        //         $kri2[14] = 0;
        //         $kri2[15] = 0;
        //         $kri2[16] = 0;
        //         $kri2[17] = 0;
        //         $kri2[18] = 0;
        //         $kri2[19] = 0;
        //         $kri2[20] = 0;
        //         $kri2[21] = 0;
        //         $kri2[22] = 0;
        //         $kri2[23] = 0;
        //         $kri2[24] = 0;
        //         $kri2[25] = 0;
        //         $kri2[26] = 0;

        //         $kri3[0] = 0;
        //         $kri3[1] = 0;
        //         $kri3[2] = 0;
        //         $kri3[3] = 0;
        //         $kri3[4] = 0;
        //         $kri3[5] = 0;
        //         $kri3[6] = 0;
        //         $kri3[7] = 0;
        //         $kri3[8] = 0;
        //         $kri3[9] = 0;
        //         $kri3[10] = 0;
        //         $kri3[11] = 0;
        //         $kri3[12] = 0;
        //         $kri3[13] = 0;
        //         $kri3[14] = 0;
        //         $kri3[15] = 0;
        //         $kri3[16] = 0;
        //         $kri3[17] = 0;
        //         $kri3[18] = 0;
        //         $kri3[19] = 0;
        //         $kri3[20] = 0;
        //         $kri3[21] = 0;
        //         $kri3[22] = 0;
        //         $kri3[23] = 0;
        //         $kri3[24] = 0;
        //         $kri3[25] = 0;
        //         $kri3[26] = 0; 

        //         for($i=0;$i<26;$i++){    
        //             foreach($pelayanan[$index] as $j => $pel){
        //                 if($pel['tanggal_pelayanan'] > $batas6jam && $pel['tanggal_pelayanan'] < $batas2hari){
        //                     if($kri[$i] < 1)
        //                     {
        //                         $kri[$i] = intval($kri[$i])+intval($CapaianPelayananperiode1[$j][$i][$j]);
        //                     } 
        //                 }
        //                 else if($pel['tanggal_pelayanan'] > $batas2hari && $pel['tanggal_pelayanan'] < $batas7hari){
        //                     if($kri[$i] <1){
        //                     $kri[$i]=intval($kri[$i])+intval($CapaianPelayananperiode2[$j][$i][$j]);
        //                     }
        //                 }
        //                 else if($pel['tanggal_pelayanan'] > $batas7hari && $pel['tanggal_pelayanan'] < $batas28hari){
                            
        //                     if($kri3[$i] <1){
        //                     $kri3[$i]= intval($kri3[$i])+intval($CapaianPelayananperiode3[$j][$i][$j]);
                            
        //                     }
        //                     // print $CapaianPelayananperiode3[$j][$i][$j];
        //                     // print $kri3[$i];
        //                 }
        //                 // print "<br>";
        //             }
        //             // print "<br>";
        //         }


        //         // print_r($kri);
        //         // print_r($kri2);
        //         // print_r($kri3);
        //         // print_r($CapaianPelayananperiode1);
        //         // print_r($CapaianPelayananperiode3);
        //         if($kri != null){
        //             $capaian_all_pel_periode1 = (((($kri[0]+$kri[1]+$kri[2]+$kri[3]+$kri[4]+$kri[5]+$kri[6]+$kri[7]+$kri[8]+$kri[15])*100) /10)+((($kri[16]+$kri[17]+$kri[18])*100)/3)+((($kri[19]+$kri[20]+$kri[21]+$kri[22]+$kri[23]+$kri[24]+$kri[25])*100) /7) )/3;
        //         }
        //         else{
        //             $capaian_all_pel_periode1 = 0;
        //         }
        //         if($kri2 != null){
        //             $capaian_all_pel_periode2 = (((($kri2[0]+$kri2[9]+$kri2[10]+$kri2[11]+$kri2[12]+$kri2[13]+$kri2[14]+$kri2[15])*100) /8)+((($kri2[16]+$kri2[17]+$kri2[18])*100)/3)+((($kri2[19]+$kri2[20]+$kri2[21]+$kri2[22]+$kri2[23]+$kri2[24]+$kri2[25])*100) /7) )/3;
        //         }
        //         else{
        //             $capaian_all_pel_periode2 = 0;
        //         }
        //         if($kri3 != null){
        //             $capaian_all_pel_periode3 = (((($kri3[0]+$kri3[9]+$kri3[10]+$kri3[11]+$kri3[12]+$kri3[13]+$kri3[14]+$kri3[15])*100) /8)+((($kri3[16]+$kri3[17]+$kri3[18])*100)/3)+((($kri3[19]+$kri3[20]+$kri3[21]+$kri3[22]+$kri3[23]+$kri3[24]+$kri3[25])*100) /7) )/3;
        //         }
        //         else{
        //             $capaian_all_pel_periode3 = 0;
        //         }
        //         // print "<br>----------------------------------------<br>";

        //         $capaian[$index] = number_format(($capaian_all_pel_periode1+$capaian_all_pel_periode2+$capaian_all_pel_periode3)/3,2);
        //         // $capaian[$index] = $capaian_all_pel_periode1."+".$capaian_all_pel_periode2."+".$capaian_all_pel_periode3;
        //     }
        //     else{
        //         // $capaian[$index] = 0;
        //         $capaian[$index] = "0";
        //     }
        // }
        return view('spm.bayibarulahir.data',compact('data_bayi','usia_bayi','total_bayi','total_sesuai_spm','total_tsesuai_spm','total_tahap_pelayanan','capaian'));
    }

    public function tambah(){
        date_default_timezone_set('Asia/Jakarta');
        $all_warga = Warga::all();
        $list_nik = [];
        $list_name = [];
        $list_ttl = [];
        $list_ibu = [];
        foreach($all_warga as $index => $warga){
            $usia[$index] = BayiBaruLahirController::Usia($warga['tgl_lahir'],date("Y/m/d/h:i:s"));
            if($usia[$index]["years"] >= 15 && $warga['jenis_kelamin'] == "P"){
                $list_ibu[$index] = $warga;
            }
        }
        return view('spm.bayibarulahir.tambah',compact('list_nik','list_name','list_ttl','list_ibu'));
    }
    
    public function save(Request $request){
        $request->tanggal_lahir = date('Y-m-d', strtotime($request->tanggal_lahir));
        $request->tanggal_lahir = $request->tanggal_lahir." ".$request->waktu_lahir.":00";
        $data = New BayiBaruLahir();
        $data['nik_ibu'] = $request->nik_ibu;
        $data['nama'] = $request->nama_bayi;
        $data['created_by'] = $request->created_by;
        $data['tgl_lahir'] = $request->tanggal_lahir;
        $data['status_kelahiran'] = "Tahap Pelayanan";
        if($data->save()){
            return redirect('/data-bayi-baru-lahir')->with('alert', 'Berhasil Tambah Bayi Baru Lahir');
        }
    }

    public function edit(Request $request){
        $bayi = BayiBaruLahir::where('id',$request->id)->first();
        $bayi['nik'] = $request->nik;
        if($bayi->save()){
            return redirect('/data-bayi-baru-lahir')->with('alert', 'Berhasil Ubah NIK Bayi Baru Lahir');
        }  
        else{
            return redirect('/data-bayi-baru-lahir')->with('alert', 'Gagal Ubah NIK Bayi Baru Lahir');
        }
    }

    public function delete($id){
        $bayi=BayiBaruLahir::where('id',$id)->first();
        $delete = $bayi->delete();
        if($delete){
            return Redirect::route('data-bayi-baru-lahir')->with('success','Berhasil Hapus Bayi Baru Lahir');
        }  
        else{
            return Redirect::route('data-bayi-baru-lahir')->with('danger','Gagal Hapus Bayi Baru Lahir');
        }
    }

    public function detail($id){
        $data = BayiBaruLahir::where('id', $id)->first();
        date_default_timezone_set('Asia/Jakarta');
        Session::put('withid',$data['id']);
        $cap_pel = [];$cap_pel2 = []; $cap_pel3 = [];
        $usia_bayi =  BayiBaruLahirController::Usia($data['tgl_lahir'],date("Y/m/d/h:i:s"));
        $pelayanan = Pelayanan::where('table_spm', 'bayi_baru_lahir')->where('id_spm',$data['id'])->get();
        //6 Jam
        $batas6jam = date('Y-m-d h:i:s', strtotime('+6 hour', strtotime($data['tgl_lahir'])));
        //48 Jam / 2 Hari
        $batas2hari = date('Y-m-d h:i:s', strtotime('+2 day', strtotime($data['tgl_lahir'])));
        //7 Hari
        $batas7hari = date('Y-m-d h:i:s', strtotime('+7 day', strtotime($data['tgl_lahir'])));
        //28 Hari
        $batas28hari = date('Y-m-d h:i:s', strtotime('+28 day', strtotime($data['tgl_lahir'])));
        
        //Untuk Mengetahui Presentase per Pelayanan
                    foreach($pelayanan as $i => $pel){
                        if($pel['suhu_tubuh'] != null){
                            $$pel['suhu_tubuh'] = explode(',',$pel['suhu_tubuh'])[0].".".explode(',',$pel['suhu_tubuh'])[1];
                            if(floatval($pel['suhu_tubuh']) >= floatval(36.4) && floatval($pel['suhu_tubuh']) <= floatval(37.5)){
                                $capaian0[$i] = 1;
                            }else{
                                $capaian0[$i] = 0;
                            }
                        }
                        else{
                            $capaian0[$i] = 0;
                        }
                
                        if($pel['neo_pertama'] == null || $pel['neo_pertama'] == "_______" || $pel['neo_pertama'] == "t_t_t_t_t_t_t_t" ){
                            $capaian1[$i] = 0;$capaian2[$i] = 0;$capaian3[$i] = 0;$capaian4[$i] = 0;
                            $capaian5[$i] = 0;$capaian6[$i] = 0;$capaian7[$i] = 0;$capaian8[$i] = 0;
                        }
                        else{
                            if(explode("_",$pel['neo_pertama'])[0] == "y"){$capaian1[$i] = 1;}else{$capaian1[$i] = 0;}
                            if(explode("_",$pel['neo_pertama'])[1] == "y"){$capaian2[$i] = 1;}else{$capaian2[$i] = 0;}
                            if(explode("_",$pel['neo_pertama'])[2] == "y"){$capaian3[$i] = 1;}else{$capaian3[$i] = 0;}
                            if(explode("_",$pel['neo_pertama'])[3] == "y"){$capaian4[$i] = 1;}else{$capaian4[$i] = 0;}
                            if(explode("_",$pel['neo_pertama'])[4] == "y"){$capaian5[$i] = 1;}else{$capaian5[$i] = 0;}
                            if(explode("_",$pel['neo_pertama'])[5] == "y"){$capaian6[$i] = 1;}else{$capaian6[$i] = 0;}
                            if(explode("_",$pel['neo_pertama'])[6] == "y"){$capaian7[$i] = 1;}else{$capaian7[$i] = 0;}
                            if(explode("_",$pel['neo_pertama'])[7] == "y"){$capaian8[$i] = 1;}else{$capaian8[$i] = 0;}
                
                        }
                        
                        if($pel['neo_kedua'] == null || $pel['neo_kedua'] == "_____" || $pel['neo_kedua'] == "t_t_t_t_t_t" ){
                            $capaian9[$i] = 0;$capaian10[$i] = 0;$capaian11[$i] = 0;
                            $capaian12[$i] = 0;$capaian13[$i] = 0;$capaian14[$i] = 0;
                        }
                        else{
                            if(explode("_",$pel['neo_kedua'])[0] == "y"){$capaian9[$i] = 1;}else{$capaian9[$i] = 0;}
                            if(explode("_",$pel['neo_kedua'])[1] == "y"){$capaian10[$i] = 1;}else{$capaian10[$i] = 0;}
                            if(explode("_",$pel['neo_kedua'])[2] == "y"){$capaian11[$i] = 1;}else{$capaian11[$i] = 0;}
                            if(explode("_",$pel['neo_kedua'])[3] == "y"){$capaian12[$i] = 1;}else{$capaian12[$i] = 0;}
                            if(explode("_",$pel['neo_kedua'])[4] == "y"){$capaian13[$i] = 1;}else{$capaian13[$i] = 0;}
                            if(explode("_",$pel['neo_kedua'])[5] == "y"){$capaian14[$i] = 1;}else{$capaian14[$i] = 0;}
                        }
                
                        if($pel['neo_fisik_apgar'] == null){
                            // nilai apgar 0
                            $nilai_apgar[$i] = 0;
                        }else{
                            if(explode("_",$pel['neo_fisik_apgar'])[4] == null){$apgar4[$i] = 0;}
                            else if(explode("_",$pel['neo_fisik_apgar'])[4] >= 100){$apgar4[$i] = 2;}
                            else if(explode("_",$pel['neo_fisik_apgar'])[4] < 100 && explode("_",$pel['neo_fisik_apgar'])[4] > 0){$apgar4[$index][$i] = 1;}
                            else if(explode("_",$pel['neo_fisik_apgar'])[4] <= 0){$apgar4[$i] = 0;}
                            $nilai_apgar[$i] =  explode("_",$pel['neo_fisik_apgar'])[0]+explode("_",$pel['neo_fisik_apgar'])[1]+
                                            explode("_",$pel['neo_fisik_apgar'])[2]+explode("_",$pel['neo_fisik_apgar'])[3]+$apgar4[$i];                       
                        }
                
                        if($nilai_apgar[$i] >= 7){
                            // cek geo
                            if($pel['neo_fisik_geo'] != null && $pel['neo_fisik_geo'] != "__" && explode("_",$pel['neo_fisik_geo'])[0] != 0 && explode("_",$pel['neo_fisik_geo'])[1] != 0 && explode("_",$pel['neo_fisik_geo'])[2] != 0 ){
                                //cek kl
                                if($pel['neo_fisik_kl'] != null && $pel['neo_fisik_kl'] != "____" && explode("_",$pel['neo_fisik_kl'])[0] == "y" && explode("_",$pel['neo_fisik_kl'])[1] == "y" && explode("_",$pel['neo_fisik_kl'])[2] == "y" && explode("_",$pel['neo_fisik_kl'])[3] == "y" && explode("_",$pel['neo_fisik_kl'])[4] == "y"){
                                    //cek mulut
                                    if($pel['neo_fisik_mulut'] != null && $pel['neo_fisik_mulut'] != "_" && explode("_",$pel['neo_fisik_mulut'])[0] == "y" && explode("_",$pel['neo_fisik_mulut'])[1] == "y"){
                                        //cek pk
                                        if($pel['neo_fisik_pk'] != null && $pel['neo_fisik_pk'] != "__" && explode("_",$pel['neo_fisik_pk'])[0] == "y" && explode("_",$pel['neo_fisik_pk'])[1] == "y" && explode("_",$pel['neo_fisik_pk'])[2] == "y"){
                                            //cek tbtk
                                            if($pel['neo_fisik_tbtk'] != null && $pel['neo_fisik_tbtk'] != "__" && explode("_",$pel['neo_fisik_tbtk'])[0] == "y" && explode("_",$pel['neo_fisik_tbtk'])[1] == "y" && explode("_",$pel['neo_fisik_tbtk'])[2] == "y"){
                                                //cek tbtk
                                                $capaian15[$i] = 1;                        
                                            }else{
                                                $capaian15[$i] = 0;
                                            }
                                        }else{
                                            //gak perlu cek tbtk
                                            $capaian15[$i] = 0;
                                        }
                                    }else{
                                        //gak perlu cek pk dll
                                        $capaian15[$i] = 0;
                                    }
                                }else{
                                    //gak perlu cek mulut dll
                                    $capaian15[$i] = 0;
                                }
                            }else{
                                //gak perlu cek kl dll
                                $capaian15[$i] = 0;
                            }
                        }else{
                            //gak perlu cek geo dll
                            $capaian15[$i] = 0;
                        }
                
                        if($pel['tenaga_kerja'] != null && $pel['tenaga_kerja'] != "_" && explode("_",$pel['tenaga_kerja'])[1] != null)
                        {$capaian16[$i] = 1;}else{$capaian16[$i] = 0;}
                        if($pel['lokasi'] != null && $pel['lokasi'] != "_" && explode("_",$pel['lokasi'])[1] != null)
                        {$capaian17[$i] = 1;}else{$capaian17[$i] = 0;}
                        if($pel['skr_shk_hsd'] != null){$capaian18[$i] = 1;}else{$capaian18[$i] = 0;}
                        
                        if($pel['kie_nama'] != null){$capaian19[$i] = 1;}else{$capaian19[$i] = 0;}
                        if($pel['kie_informasi'] != null && $pel['kie_informasi'] != "t_t_t_t_t" ){
                            if(explode("_",$pel['kie_informasi'])[0] == "y"){$capaian20[$i] = 1;}else{$capaian20[$i] = 0;}
                            if(explode("_",$pel['kie_informasi'])[1] == "y"){$capaian21[$i] = 1;}else{$capaian21[$i] = 0;}
                            if(explode("_",$pel['kie_informasi'])[2] == "y"){$capaian22[$i] = 1;}else{$capaian22[$i] = 0;}
                            if(explode("_",$pel['kie_informasi'])[3] == "y"){$capaian23[$i] = 1;}else{$capaian23[$i] = 0;}
                            if(explode("_",$pel['kie_informasi'])[4] == "y"){$capaian24[$i] = 1;}else{$capaian24[$i] = 0;}            
                        }else{
                            $capaian20[$i] = 0;$capaian21[$i] = 0;$capaian22[$i] = 0;
                            $capaian23[$i] = 0;$capaian24[$i] = 0;
                
                        }
                        if($pel['kie_media'] != null){$capaian25[$i] = 1;}else{$capaian25[$i] = 0;}            
                        
                        if($pel['tanggal_pelayanan'] >= $batas6jam && $pel['tanggal_pelayanan'] < $batas2hari){
                            $cap_neo = (($capaian0[$i]+$capaian1[$i]
                            +$capaian2[$i]+$capaian3[$i]+$capaian4[$i]
                            +$capaian5[$i]+$capaian6[$i]+$capaian7[$i]
                            +$capaian8[$i]+$capaian15[$i]) *100) /10;
                            $form = "pertama";
                        }
                        else if($pel['tanggal_pelayanan'] > $batas2hari && $pel['tanggal_pelayanan'] < $batas28hari){
                            $cap_neo = (($capaian0[$i]+$capaian9[$i]
                            +$capaian10[$i]+$capaian11[$i]+$capaian12[$i]
                            +$capaian13[$i]+$capaian14[$i]+$capaian15[$i]) *100) /8;
                            $form = "kedua";
                        }
                        else{
                            $form ="non";
                            $cap_neo = 0;
                        }
                        $cap_skr = (($capaian16[$i]+$capaian17[$i]+$capaian18[$i]) *100) /3;
                        $cap_info = (($capaian19[$i]+$capaian20[$i]+$capaian21[$i]+$capaian22[$i]+$capaian23[$i]
                        +$capaian24[$i]+$capaian25[$i]) *100) /7;
                        $cap_total = ($cap_neo+$cap_skr+$cap_info)/3;
                        if($pel['tanggal_pelayanan'] > $batas6jam && $pel['tanggal_pelayanan'] < $batas2hari){
                            $cap_pel[$pel['id']] = $cap_total;
                        }
                        else if($pel['tanggal_pelayanan'] > $batas2hari && $pel['tanggal_pelayanan'] < $batas7hari){
                            $cap_pel2[$pel['id']] = $cap_total;
                        }
                        else if($pel['tanggal_pelayanan'] > $batas7hari && $pel['tanggal_pelayanan'] < $batas28hari){
                            $cap_pel3[$pel['id']] = $cap_total;
                        }
                    }
        // End Untuk Mengetahui Presentase per Pelayanan

        //Untuk Mengetahui Presentase per Periode
                    $capaian0_periode1=0;$capaian0_periode2=0;$capaian0_periode3=0;
                    $capaian1_periode1=0;$capaian1_periode2=0;$capaian1_periode3=0;
                    $capaian2_periode1=0;$capaian2_periode2=0;$capaian2_periode3=0;
                    $capaian3_periode1=0;$capaian3_periode2=0;$capaian3_periode3=0;
                    $capaian4_periode1=0;$capaian4_periode2=0;$capaian4_periode3=0;
                    $capaian5_periode1=0;$capaian5_periode2=0;$capaian5_periode3=0;
                    $capaian6_periode1=0;$capaian6_periode2=0;$capaian6_periode3=0;
                    $capaian7_periode1=0;$capaian7_periode2=0;$capaian7_periode3=0;
                    $capaian8_periode1=0;$capaian8_periode2=0;$capaian8_periode3=0;
                    $capaian9_periode1=0;$capaian9_periode2=0;$capaian9_periode3=0;
                    $capaian10_periode1=0;$capaian10_periode2=0;$capaian10_periode3=0;
                    $capaian11_periode1=0;$capaian11_periode2=0;$capaian11_periode3=0;
                    $capaian12_periode1=0;$capaian12_periode2=0;$capaian12_periode3=0;
                    $capaian13_periode1=0;$capaian13_periode2=0;$capaian13_periode3=0;
                    $capaian14_periode1=0;$capaian14_periode2=0;$capaian14_periode3=0;
                    $capaian15_periode1=0;$capaian15_periode2=0;$capaian15_periode3=0;
                    $capaian16_periode1=0;$capaian16_periode2=0;$capaian16_periode3=0;
                    $capaian17_periode1=0;$capaian17_periode2=0;$capaian17_periode3=0;
                    $capaian18_periode1=0;$capaian18_periode2=0;$capaian18_periode3=0;
                    $capaian19_periode1=0;$capaian19_periode2=0;$capaian19_periode3=0;
                    $capaian20_periode1=0;$capaian20_periode2=0;$capaian20_periode3=0;
                    $capaian21_periode1=0;$capaian21_periode2=0;$capaian21_periode3=0;
                    $capaian22_periode1=0;$capaian22_periode2=0;$capaian22_periode3=0;
                    $capaian23_periode1=0;$capaian23_periode2=0;$capaian23_periode3=0;
                    $capaian24_periode1=0;$capaian24_periode2=0;$capaian24_periode3=0;
                    $capaian25_periode1=0;$capaian25_periode2=0;$capaian25_periode3=0;
                    foreach($pelayanan as $i => $pel){
                        if($pel['tanggal_pelayanan'] > $batas6jam && $pel['tanggal_pelayanan'] < $batas2hari){
                            if($capaian0_periode1 < 1){$capaian0_periode1 = $capaian0_periode1 + $capaian0[$i];}
                            if($capaian1_periode1 < 1){$capaian1_periode1 = $capaian1_periode1 + $capaian1[$i];}
                            if($capaian2_periode1 < 1){$capaian2_periode1 = $capaian2_periode1 + $capaian2[$i];}
                            if($capaian3_periode1 < 1){$capaian3_periode1 = $capaian3_periode1 + $capaian3[$i];}
                            if($capaian4_periode1 < 1){$capaian4_periode1 = $capaian4_periode1 + $capaian4[$i];}
                            if($capaian5_periode1 < 1){$capaian5_periode1 = $capaian5_periode1 + $capaian5[$i];}
                            if($capaian6_periode1 < 1){$capaian6_periode1 = $capaian6_periode1 + $capaian6[$i];}
                            if($capaian7_periode1 < 1){$capaian7_periode1 = $capaian7_periode1 + $capaian7[$i];}
                            if($capaian8_periode1 < 1){$capaian8_periode1 = $capaian8_periode1 + $capaian8[$i];}
                            if($capaian9_periode1 < 1){$capaian9_periode1 = $capaian9_periode1 + $capaian9[$i];}
                            if($capaian10_periode1 < 1){$capaian10_periode1 = $capaian10_periode1 + $capaian10[$i];}
                            if($capaian11_periode1 < 1){$capaian11_periode1 = $capaian11_periode1 + $capaian11[$i];}
                            if($capaian12_periode1 < 1){$capaian12_periode1 = $capaian12_periode1 + $capaian12[$i];}
                            if($capaian13_periode1 < 1){$capaian13_periode1 = $capaian13_periode1 + $capaian13[$i];}
                            if($capaian14_periode1 < 1){$capaian14_periode1 = $capaian14_periode1 + $capaian14[$i];}
                            if($capaian15_periode1 < 1){$capaian15_periode1 = $capaian15_periode1 + $capaian15[$i];}
                            if($capaian16_periode1 < 1){$capaian16_periode1 = $capaian16_periode1 + $capaian16[$i];}
                            if($capaian17_periode1 < 1){$capaian17_periode1 = $capaian17_periode1 + $capaian17[$i];}
                            if($capaian18_periode1 < 1){$capaian18_periode1 = $capaian18_periode1 + $capaian18[$i];}
                            if($capaian19_periode1 < 1){$capaian19_periode1 = $capaian19_periode1 + $capaian19[$i];}
                            if($capaian20_periode1 < 1){$capaian20_periode1 = $capaian20_periode1 + $capaian20[$i];}
                            if($capaian21_periode1 < 1){$capaian21_periode1 = $capaian21_periode1 + $capaian21[$i];}
                            if($capaian22_periode1 < 1){$capaian22_periode1 = $capaian22_periode1 + $capaian22[$i];}
                            if($capaian23_periode1 < 1){$capaian23_periode1 = $capaian23_periode1 + $capaian23[$i];}
                            if($capaian24_periode1 < 1){$capaian24_periode1 = $capaian24_periode1 + $capaian24[$i];}
                            if($capaian25_periode1 < 1){$capaian25_periode1 = $capaian25_periode1 + $capaian25[$i];}
                        }
                        else if($pel['tanggal_pelayanan'] > $batas2hari && $pel['tanggal_pelayanan'] < $batas7hari){
                            if($capaian0_periode2 < 1){$capaian0_periode2 = $capaian0_periode2 + $capaian0[$i];}
                            if($capaian1_periode2 < 1){$capaian1_periode2 = $capaian1_periode2 + $capaian1[$i];}
                            if($capaian2_periode2 < 1){$capaian2_periode2 = $capaian2_periode2 + $capaian2[$i];}
                            if($capaian3_periode2 < 1){$capaian3_periode2 = $capaian3_periode2 + $capaian3[$i];}
                            if($capaian4_periode2 < 1){$capaian4_periode2 = $capaian4_periode2 + $capaian4[$i];}
                            if($capaian5_periode2 < 1){$capaian5_periode2 = $capaian5_periode2 + $capaian5[$i];}
                            if($capaian6_periode2 < 1){$capaian6_periode2 = $capaian6_periode2 + $capaian6[$i];}
                            if($capaian7_periode2 < 1){$capaian7_periode2 = $capaian7_periode2 + $capaian7[$i];}
                            if($capaian8_periode2 < 1){$capaian8_periode2 = $capaian8_periode2 + $capaian8[$i];}
                            if($capaian9_periode2 < 1){$capaian9_periode2 = $capaian9_periode2 + $capaian9[$i];}
                            if($capaian10_periode2 < 1){$capaian10_periode2 = $capaian10_periode2 + $capaian10[$i];}
                            if($capaian11_periode2 < 1){$capaian11_periode2 = $capaian11_periode2 + $capaian11[$i];}
                            if($capaian12_periode2 < 1){$capaian12_periode2 = $capaian12_periode2 + $capaian12[$i];}
                            if($capaian13_periode2 < 1){$capaian13_periode2 = $capaian13_periode2 + $capaian13[$i];}
                            if($capaian14_periode2 < 1){$capaian14_periode2 = $capaian14_periode2 + $capaian14[$i];}
                            if($capaian15_periode2 < 1){$capaian15_periode2 = $capaian15_periode2 + $capaian15[$i];}
                            if($capaian16_periode2 < 1){$capaian16_periode2 = $capaian16_periode2 + $capaian16[$i];}
                            if($capaian17_periode2 < 1){$capaian17_periode2 = $capaian17_periode2 + $capaian17[$i];}
                            if($capaian18_periode2 < 1){$capaian18_periode2 = $capaian18_periode2 + $capaian18[$i];}
                            if($capaian19_periode2 < 1){$capaian19_periode2 = $capaian19_periode2 + $capaian19[$i];}
                            if($capaian20_periode2 < 1){$capaian20_periode2 = $capaian20_periode2 + $capaian20[$i];}
                            if($capaian21_periode2 < 1){$capaian21_periode2 = $capaian21_periode2 + $capaian21[$i];}
                            if($capaian22_periode2 < 1){$capaian22_periode2 = $capaian22_periode2 + $capaian22[$i];}
                            if($capaian23_periode2 < 1){$capaian23_periode2 = $capaian23_periode2 + $capaian23[$i];}
                            if($capaian24_periode2 < 1){$capaian24_periode2 = $capaian24_periode2 + $capaian24[$i];}
                            if($capaian25_periode2 < 1){$capaian25_periode2 = $capaian25_periode2 + $capaian25[$i];}
                        }
                        else if($pel['tanggal_pelayanan'] > $batas7hari && $pel['tanggal_pelayanan'] < $batas28hari){
                            if($capaian0_periode3 < 1){$capaian0_periode3 = $capaian0_periode3 + $capaian0[$i];}
                            if($capaian1_periode3 < 1){$capaian1_periode3 = $capaian1_periode3 + $capaian1[$i];}
                            if($capaian2_periode3 < 1){$capaian2_periode3 = $capaian2_periode3 + $capaian2[$i];}
                            if($capaian3_periode3 < 1){$capaian3_periode3 = $capaian3_periode3 + $capaian3[$i];}
                            if($capaian4_periode3 < 1){$capaian4_periode3 = $capaian4_periode3 + $capaian4[$i];}
                            if($capaian5_periode3 < 1){$capaian5_periode3 = $capaian5_periode3 + $capaian5[$i];}
                            if($capaian6_periode3 < 1){$capaian6_periode3 = $capaian6_periode3 + $capaian6[$i];}
                            if($capaian7_periode3 < 1){$capaian7_periode3 = $capaian7_periode3 + $capaian7[$i];}
                            if($capaian8_periode3 < 1){$capaian8_periode3 = $capaian8_periode3 + $capaian8[$i];}
                            if($capaian9_periode3 < 1){$capaian9_periode3 = $capaian9_periode3 + $capaian9[$i];}
                            if($capaian10_periode3 < 1){$capaian10_periode3 = $capaian10_periode3 + $capaian10[$i];}
                            if($capaian11_periode3 < 1){$capaian11_periode3 = $capaian11_periode3 + $capaian11[$i];}
                            if($capaian12_periode3 < 1){$capaian12_periode3 = $capaian12_periode3 + $capaian12[$i];}
                            if($capaian13_periode3 < 1){$capaian13_periode3 = $capaian13_periode3 + $capaian13[$i];}
                            if($capaian14_periode3 < 1){$capaian14_periode3 = $capaian14_periode3 + $capaian14[$i];}
                            if($capaian15_periode3 < 1){$capaian15_periode3 = $capaian15_periode3 + $capaian15[$i];}
                            if($capaian16_periode3 < 1){$capaian16_periode3 = $capaian16_periode3 + $capaian16[$i];}
                            if($capaian17_periode3 < 1){$capaian17_periode3 = $capaian17_periode3 + $capaian17[$i];}
                            if($capaian18_periode3 < 1){$capaian18_periode3 = $capaian18_periode3 + $capaian18[$i];}
                            if($capaian19_periode3 < 1){$capaian19_periode3 = $capaian19_periode3 + $capaian19[$i];}
                            if($capaian20_periode3 < 1){$capaian20_periode3 = $capaian20_periode3 + $capaian20[$i];}
                            if($capaian21_periode3 < 1){$capaian21_periode3 = $capaian21_periode3 + $capaian21[$i];}
                            if($capaian22_periode3 < 1){$capaian22_periode3 = $capaian22_periode3 + $capaian22[$i];}
                            if($capaian23_periode3 < 1){$capaian23_periode3 = $capaian23_periode3 + $capaian23[$i];}
                            if($capaian24_periode3 < 1){$capaian24_periode3 = $capaian24_periode3 + $capaian24[$i];}
                            if($capaian25_periode3 < 1){$capaian25_periode3 = $capaian25_periode3 + $capaian25[$i];}

                        }
                    }
                    
                    $capaian_all_pel_periode1 = (((($capaian0_periode1+$capaian1_periode1+
                        $capaian2_periode1+$capaian3_periode1+$capaian4_periode1+
                        $capaian5_periode1+$capaian6_periode1+$capaian7_periode1+
                        $capaian8_periode1+$capaian15_periode1)*100) /10)+
                        ((($capaian16_periode1+$capaian17_periode1+$capaian18_periode1)*100)/3)+
                        ((($capaian19_periode1+$capaian20_periode1+$capaian21_periode1+$capaian22_periode1+
                        $capaian23_periode1+$capaian24_periode1+$capaian25_periode1)*100) /7) )/3;
        
                    $capaian_all_pel_periode2 = (((($capaian0_periode2+$capaian9_periode2+
                        $capaian10_periode2+$capaian11_periode2+$capaian12_periode2+
                        $capaian13_periode2+$capaian14_periode2+$capaian15_periode2)*100) /8)+
                        ((($capaian16_periode2+$capaian17_periode2+$capaian18_periode2)*100)/3)+
                        ((($capaian19_periode2+$capaian20_periode2+$capaian21_periode2+$capaian22_periode2+
                        $capaian23_periode2+$capaian24_periode2+$capaian25_periode2)*100) /7) )/3;
        
                    $capaian_all_pel_periode3 = (((($capaian0_periode3+$capaian9_periode3+
                        $capaian10_periode3+$capaian11_periode3+$capaian12_periode3+
                        $capaian13_periode3+$capaian14_periode3+$capaian15_periode3)*100) /8)+
                        ((($capaian16_periode3+$capaian17_periode3+$capaian18_periode3)*100)/3)+
                        ((($capaian19_periode3+$capaian20_periode3+$capaian21_periode3+$capaian22_periode3+
                        $capaian23_periode3+$capaian24_periode3+$capaian25_periode3)*100) /7) )/3;    
        
                        $capaian = number_format(($capaian_all_pel_periode1+$capaian_all_pel_periode2+$capaian_all_pel_periode3)/3,2);            
                
    //End Untuk Mengetahui Presentase per Periode
        return view('spm.bayibarulahir.detail',compact('cap_pel','cap_pel2','cap_pel3','batas6jam','batas2hari','batas7hari','batas28hari','pelayanan','data','capaian','capaian_all_pel_periode1','capaian_all_pel_periode2','capaian_all_pel_periode3','usia_bayi'));
    }

    public function ubah($id){
        date_default_timezone_set('Asia/Jakarta');
        $bayi = BayiBaruLahir::where('id',$id)->first();
        $warganya = Warga::where('nik',$bayi['nik'])->first();
        $warga_all = Warga::all();
        $list_nik = [];
        $list_name = [];
        $list_ttl = [];
        foreach($warga_all as $indexwarga => $warga){
            $data = BayiBaruLahir::where('nik', $warga['nik'])->where('status_kelahiran','tahap pelayanan')->first();
            if(!$data){
                $usia[$indexwarga] = BayiBaruLahirController::Usia($warga['tgl_lahir'],date("Y/m/d/h:i:s"));
                if($usia[$indexwarga]["years"] == 0 && $usia[$indexwarga]["months"] == 0 && $usia[$indexwarga]["days"] <= 28){
                    $list_nik[$indexwarga] = $warga['nik'];
                    $list_name[$indexwarga] = $warga['nama'];
                    $list_ttl[$indexwarga] = $warga['tgl_lahir'];
                }
            }  
        }
        $pelayanan = Pelayanan::where('table_spm', 'bayi_baru_lahir')->where('id_spm',$bayi->id)->get();
        $batas6jam = date('Y-m-d h:i:s', strtotime('+6 hour', strtotime($warganya->tgl_lahir)));
        $batas2hari = date('Y-m-d h:i:s', strtotime('+2 day', strtotime($warganya->tgl_lahir)));
        $batas7hari = date('Y-m-d h:i:s', strtotime('+7 day', strtotime($warganya->tgl_lahir)));
        $batas28hari = date('Y-m-d h:i:s', strtotime('+28 day', strtotime($warganya->tgl_lahir)));
        if($pelayanan != null){
            foreach($pelayanan as $i => $pel){  
                if($pel['tanggal_pelayanan'] > $batas6jam && $pel['tanggal_pelayanan'] < $batas2hari){
                    $CapaianPelayananperiode1[$i] = BayiBaruLahirController::CekCapaian($pel['suhu_tubuh'],$pel['neo_pertama'],$pel['neo_kedua'],$pel['neo_fisik_apgar'],
                    $pel['neo_fisik_geo'],$pel['neo_fisik_kl'],$pel['neo_fisik_mulut'],$pel['neo_fisik_pk'],$pel['neo_fisik_tbtk'],
                    $pel['tenaga_kerja'],$pel['lokasi'],$pel['skr_shk_hsd'],$pel['kie_nama'],$pel['kie_informasi'],$pel['kie_media'],$i);
                }
                else if($pel['tanggal_pelayanan'] > $batas2hari && $pel['tanggal_pelayanan'] < $batas7hari){
                    $CapaianPelayananperiode2[$i] = BayiBaruLahirController::CekCapaian($pel['suhu_tubuh'],$pel['neo_pertama'],$pel['neo_kedua'],$pel['neo_fisik_apgar'],
                    $pel['neo_fisik_geo'],$pel['neo_fisik_kl'],$pel['neo_fisik_mulut'],$pel['neo_fisik_pk'],$pel['neo_fisik_tbtk'],
                    $pel['tenaga_kerja'],$pel['lokasi'],$pel['skr_shk_hsd'],$pel['kie_nama'],$pel['kie_informasi'],$pel['kie_media'],$i);
                }
                else if($pel['tanggal_pelayanan'] > $batas7hari && $pel['tanggal_pelayanan'] < $batas28hari){
                    $CapaianPelayananperiode3[$i] = BayiBaruLahirController::CekCapaian($pel['suhu_tubuh'],$pel['neo_pertama'],$pel['neo_kedua'],$pel['neo_fisik_apgar'],
                    $pel['neo_fisik_geo'],$pel['neo_fisik_kl'],$pel['neo_fisik_mulut'],$pel['neo_fisik_pk'],$pel['neo_fisik_tbtk'],
                    $pel['tenaga_kerja'],$pel['lokasi'],$pel['skr_shk_hsd'],$pel['kie_nama'],$pel['kie_informasi'],$pel['kie_media'],$i);
                }
            }

            $kri = [];
            $kri2 = [];
            $kri3 = [];

                $kri[0] = 0;
                $kri[1] = 0;
                $kri[2] = 0;
                $kri[3] = 0;
                $kri[4] = 0;
                $kri[5] = 0;
                $kri[6] = 0;
                $kri[7] = 0;
                $kri[8] = 0;
                $kri[9] = 0;
                $kri[10] = 0;
                $kri[11] = 0;
                $kri[12] = 0;
                $kri[13] = 0;
                $kri[14] = 0;
                $kri[15] = 0;
                $kri[16] = 0;
                $kri[17] = 0;
                $kri[18] = 0;
                $kri[19] = 0;
                $kri[20] = 0;
                $kri[21] = 0;
                $kri[22] = 0;
                $kri[23] = 0;
                $kri[24] = 0;
                $kri[25] = 0;
                $kri[26] = 0;

                $kri2[0] = 0;
                $kri[1] = 0;
                $kri2[2] = 0;
                $kri2[3] = 0;
                $kri2[4] = 0;
                $kri2[5] = 0;
                $kri2[6] = 0;
                $kri2[7] = 0;
                $kri2[8] = 0;
                $kri2[9] = 0;
                $kri2[10] = 0;
                $kri2[11] = 0;
                $kri2[12] = 0;
                $kri2[13] = 0;
                $kri2[14] = 0;
                $kri2[15] = 0;
                $kri2[16] = 0;
                $kri2[17] = 0;
                $kri2[18] = 0;
                $kri2[19] = 0;
                $kri2[20] = 0;
                $kri2[21] = 0;
                $kri2[22] = 0;
                $kri2[23] = 0;
                $kri2[24] = 0;
                $kri2[25] = 0;
                $kri2[26] = 0;

                $kri3[0] = 0;
                $kri3[1] = 0;
                $kri3[2] = 0;
                $kri3[3] = 0;
                $kri3[4] = 0;
                $kri3[5] = 0;
                $kri3[6] = 0;
                $kri3[7] = 0;
                $kri3[8] = 0;
                $kri3[9] = 0;
                $kri3[10] = 0;
                $kri3[11] = 0;
                $kri3[12] = 0;
                $kri3[13] = 0;
                $kri3[14] = 0;
                $kri3[15] = 0;
                $kri3[16] = 0;
                $kri3[17] = 0;
                $kri3[18] = 0;
                $kri3[19] = 0;
                $kri3[20] = 0;
                $kri3[21] = 0;
                $kri3[22] = 0;
                $kri3[23] = 0;
                $kri3[24] = 0;
                $kri3[25] = 0;
                $kri3[26] = 0; 
            for($i=0;$i<26;$i++){    
                foreach($pelayanan as $j => $pel){
                    if($pel['tanggal_pelayanan'] > $batas6jam && $pel['tanggal_pelayanan'] < $batas2hari){
                        if($kri[$i] <1){   
                            $kri[$i]= intval($kri[$i])+intval($CapaianPelayananperiode1[$j][$i][$j]);
                        }
                    }
                    else if($pel['tanggal_pelayanan'] > $batas2hari && $pel['tanggal_pelayanan'] < $batas7hari){
                        if($kri2[$i] <1){   
                            $kri2[$i]= intval($kri2[$i])+intval($CapaianPelayananperiode2[$j][$i][$j]);
                        }
                    }
                    else if($pel['tanggal_pelayanan'] > $batas7hari && $pel['tanggal_pelayanan'] < $batas28hari){
                        if($kri3[$i] <1){   
                            $kri3[$i]= intval($kri3[$i])+intval($CapaianPelayananperiode3[$j][$i][$j]);
                        }
                    }
                }
            }

            if($kri != null){
                $capaian_all_pel_periode1 = (((($kri[0]+$kri[1]+$kri[2]+$kri[3]+$kri[4]+$kri[5]+$kri[6]+$kri[7]+$kri[8]+$kri[15])*100) /10)+((($kri[16]+$kri[17]+$kri[18])*100)/3)+((($kri[19]+$kri[20]+$kri[21]+$kri[22]+$kri[23]+$kri[24]+$kri[25])*100) /7) )/3;
            }
            else{
                $capaian_all_pel_periode1 = 0;
            }
            if($kri2 != null){
                $capaian_all_pel_periode2 = (((($kri2[0]+$kri2[9]+$kri2[10]+$kri2[11]+$kri2[12]+$kri2[13]+$kri2[14]+$kri2[15])*100) /8)+((($kri2[16]+$kri2[17]+$kri2[18])*100)/3)+((($kri2[19]+$kri2[20]+$kri2[21]+$kri2[22]+$kri2[23]+$kri2[24]+$kri2[25])*100) /7) )/3;
            }
            else{
                $capaian_all_pel_periode2 = 0;
            }
            if($kri3 != null){
                $capaian_all_pel_periode3 = (((($kri3[0]+$kri3[9]+$kri3[10]+$kri3[11]+$kri3[12]+$kri3[13]+$kri3[14]+$kri3[15])*100) /8)+((($kri3[16]+$kri3[17]+$kri3[18])*100)/3)+((($kri3[19]+$kri3[20]+$kri3[21]+$kri3[22]+$kri3[23]+$kri3[24]+$kri3[25])*100) /7) )/3;
            }
            else{
                $capaian_all_pel_periode3 = 0;
            }
            $capaian = ($capaian_all_pel_periode1+$capaian_all_pel_periode2+$capaian_all_pel_periode3)/3;
        }
        else{
            $capaian = "0";
        }

        return view('spm.bayibarulahir.ubah',compact('list_nik','list_name','list_ttl','bayi','warganya','capaian'));
    }    
    


    public function indexPelayanan(){
        $pelayanan = Pelayanan::where('table_spm', 'bayi_baru_lahir')->get();
        
        foreach($pelayanan as $index => $pel){
            $temp_bayibarulahir = BayiBaruLahir::where('id',$pel['id_spm'])->first();
            $bayi[$index] = Warga::where('nik',$temp_bayibarulahir['nik'])->first();
            $batas6jam = date('Y-m-d h:i:s', strtotime('+6 hour', strtotime($bayi[$index]['tgl_lahir'])));
            $batas2hari = date('Y-m-d h:i:s', strtotime('+2 day', strtotime($bayi[$index]['tgl_lahir'])));
            $batas7hari = date('Y-m-d h:i:s', strtotime('+7 day', strtotime($bayi[$index]['tgl_lahir'])));
            $batas28hari = date('Y-m-d h:i:s', strtotime('+28 day', strtotime($bayi[$index]['tgl_lahir'])));
            if($pel != null){
                if($pel['suhu_tubuh'] != null){
                    $pel['suhu_tubuh'] = explode(',',$pel['suhu_tubuh'])[0].".".explode(',',$pel['suhu_tubuh'])[1];
                    if(floatval($pel['suhu_tubuh']) >= floatval(36.4) && floatval($pel['suhu_tubuh']) <= floatval(37.5)){
                        $capaian0 = 1;
                    }else{
                        $capaian0 = 0;
                    }
                }
                else{
                    $capaian0 = 0;
                }
        
                if($pel['neo_pertama'] == null || $pel['neo_pertama'] == "_______" || $pel['neo_pertama'] == "t_t_t_t_t_t_t_t" ){
                    $capaian1 = 0;$capaian2 = 0;$capaian3 = 0;$capaian4 = 0;
                    $capaian5 = 0;$capaian6 = 0;$capaian7 = 0;$capaian8 = 0;
                }
                else{
                    if(explode("_",$pel['neo_pertama'])[0] == "y"){$capaian1 = 1;}else{$capaian1 = 0;}
                    if(explode("_",$pel['neo_pertama'])[1] == "y"){$capaian2 = 1;}else{$capaian2 = 0;}
                    if(explode("_",$pel['neo_pertama'])[2] == "y"){$capaian3 = 1;}else{$capaian3 = 0;}
                    if(explode("_",$pel['neo_pertama'])[3] == "y"){$capaian4 = 1;}else{$capaian4 = 0;}
                    if(explode("_",$pel['neo_pertama'])[4] == "y"){$capaian5 = 1;}else{$capaian5 = 0;}
                    if(explode("_",$pel['neo_pertama'])[5] == "y"){$capaian6 = 1;}else{$capaian6 = 0;}
                    if(explode("_",$pel['neo_pertama'])[6] == "y"){$capaian7 = 1;}else{$capaian7 = 0;}
                    if(explode("_",$pel['neo_pertama'])[7] == "y"){$capaian8 = 1;}else{$capaian8 = 0;}
        
                }
                
                if($pel['neo_kedua'] == null || $pel['neo_kedua'] == "_____" || $pel['neo_kedua'] == "t_t_t_t_t_t" ){
                    $capaian9 = 0;$capaian10 = 0;$capaian11 = 0;
                    $capaian12 = 0;$capaian13 = 0;$capaian14 = 0;
                }
                else{
                    if(explode("_",$pel['neo_kedua'])[0] == "y"){$capaian9 = 1;}else{$capaian9 = 0;}
                    if(explode("_",$pel['neo_kedua'])[1] == "y"){$capaian10 = 1;}else{$capaian10 = 0;}
                    if(explode("_",$pel['neo_kedua'])[2] == "y"){$capaian11 = 1;}else{$capaian11 = 0;}
                    if(explode("_",$pel['neo_kedua'])[3] == "y"){$capaian12 = 1;}else{$capaian12 = 0;}
                    if(explode("_",$pel['neo_kedua'])[4] == "y"){$capaian13 = 1;}else{$capaian13 = 0;}
                    if(explode("_",$pel['neo_kedua'])[5] == "y"){$capaian14 = 1;}else{$capaian14 = 0;}
                }
        
                if($pel['neo_fisik_apgar'] == null){
                    // nilai apgar 0
                    $nilai_apgar = 0;
                }else{
                    if(explode("_",$pel['neo_fisik_apgar'])[4] == null){$apgar4 = 0;}
                    else if(explode("_",$pel['neo_fisik_apgar'])[4] >= 100){$apgar4 = 2;}
                    else if(explode("_",$pel['neo_fisik_apgar'])[4] < 100 && explode("_",$pel['neo_fisik_apgar'])[4] > 0){$apgar4 = 1;}
                    else if(explode("_",$pel['neo_fisik_apgar'])[4] <= 0){$apgar4 = 0;}
                    $nilai_apgar =  explode("_",$pel['neo_fisik_apgar'])[0]+explode("_",$pel['neo_fisik_apgar'])[1]+
                                    explode("_",$pel['neo_fisik_apgar'])[2]+explode("_",$pel['neo_fisik_apgar'])[3]+$apgar4;                       
                }
        
                if($nilai_apgar >= 7){
                    // cek geo
                    if($pel['neo_fisik_geo'] != null && $pel['neo_fisik_geo'] != "__" && explode("_",$pel['neo_fisik_geo'])[0] != 0 && explode("_",$pel['neo_fisik_geo'])[1] != 0 && explode("_",$pel['neo_fisik_geo'])[2] != 0 ){
                        //cek kl
                        if($pel['neo_fisik_kl'] != null && $pel['neo_fisik_kl'] != "____" && explode("_",$pel['neo_fisik_kl'])[0] == "y" && explode("_",$pel['neo_fisik_kl'])[1] == "y" && explode("_",$pel['neo_fisik_kl'])[2] == "y" && explode("_",$pel['neo_fisik_kl'])[3] == "y" && explode("_",$pel['neo_fisik_kl'])[4] == "y"){
                            //cek mulut
                            if($pel['neo_fisik_mulut'] != null && $pel['neo_fisik_mulut'] != "_" && explode("_",$pel['neo_fisik_mulut'])[0] == "y" && explode("_",$pel['neo_fisik_mulut'])[1] == "y"){
                                //cek pk
                                if($pel['neo_fisik_pk'] != null && $pel['neo_fisik_pk'] != "__" && explode("_",$pel['neo_fisik_pk'])[0] == "y" && explode("_",$pel['neo_fisik_pk'])[1] == "y" && explode("_",$pel['neo_fisik_pk'])[2] == "y"){
                                    //cek tbtk
                                    if($pel['neo_fisik_tbtk'] != null && $pel['neo_fisik_tbtk'] != "__" && explode("_",$pel['neo_fisik_tbtk'])[0] == "y" && explode("_",$pel['neo_fisik_tbtk'])[1] == "y" && explode("_",$pel['neo_fisik_tbtk'])[2] == "y"){
                                        //cek tbtk
                                        $capaian15 = 1;                        
                                    }else{
                                        $capaian15 = 0;
                                    }
                                }else{
                                    //gak perlu cek tbtk
                                    $capaian15 = 0;
                                }
                            }else{
                                //gak perlu cek pk dll
                                $capaian15 = 0;
                            }
                        }else{
                            //gak perlu cek mulut dll
                            $capaian15 = 0;
                        }
                    }else{
                        //gak perlu cek kl dll
                        $capaian15 = 0;
                    }
                }else{
                    //gak perlu cek geo dll
                    $capaian15 = 0;
                }
        
                if($pel['tenaga_kerja'] != null && $pel['tenaga_kerja'] != "_" && explode("_",$pel['tenaga_kerja'])[1] != null)
                {$capaian16 = 1;}else{$capaian16 = 0;}
                if($pel['lokasi'] != null && $pel['lokasi'] != "_" && explode("_",$pel['lokasi'])[1] != null)
                {$capaian17 = 1;}else{$capaian17 = 0;}
                if($pel['skr_shk_hsd'] != null){$capaian18 = 1;}else{$capaian18 = 0;}
                
                if($pel['kie_nama'] != null){$capaian19 = 1;}else{$capaian19 = 0;}
                if($pel['kie_informasi'] != null && $pel['kie_informasi'] != "t_t_t_t_t" ){
                    if(explode("_",$pel['kie_informasi'])[0] == "y"){$capaian20 = 1;}else{$capaian20 = 0;}
                    if(explode("_",$pel['kie_informasi'])[1] == "y"){$capaian21 = 1;}else{$capaian21 = 0;}
                    if(explode("_",$pel['kie_informasi'])[2] == "y"){$capaian22 = 1;}else{$capaian22 = 0;}
                    if(explode("_",$pel['kie_informasi'])[3] == "y"){$capaian23 = 1;}else{$capaian23 = 0;}
                    if(explode("_",$pel['kie_informasi'])[4] == "y"){$capaian24 = 1;}else{$capaian24 = 0;}            
                }else{
                    $capaian20 = 0;$capaian21 = 0;$capaian22 = 0;
                    $capaian23 = 0;$capaian24 = 0;
        
                }
                if($pel['kie_media'] != null){$capaian25 = 1;}else{$capaian25 = 0;}            
                
                if($pel['tanggal_pelayanan'] >= $batas6jam && $pel['tanggal_pelayanan'] < $batas2hari){
                    $cap_neo[$index] = (($capaian0+$capaian1+$capaian2+$capaian3+$capaian4+$capaian5+$capaian6+$capaian7+$capaian8+$capaian15) *100) /10;
                    $form = "pertama";
                }
                else if($pel['tanggal_pelayanan'] > $batas2hari && $pel['tanggal_pelayanan'] < $batas28hari){
                    $cap_neo[$index] = (($capaian0+$capaian9+$capaian10+$capaian11+$capaian12+$capaian13+$capaian14+$capaian15) *100) /8;
                    $form = "kedua";
                }
                else{
                    $form ="non";
                    $cap_neo[$index] = 0;
                }
                $cap_skr[$index] = (($capaian16+$capaian17+$capaian18) *100) /3;
                $cap_info[$index] = (($capaian19+$capaian20+$capaian21+$capaian22+$capaian23+$capaian24+$capaian25) *100) /7;
                $cap_total[$index] = ($cap_neo[$index]+$cap_skr[$index]+$cap_info[$index])/3;
            }
            else{
                $cap_total[index] = 0;
            
            }


        }
        return view('spm.bayibarulahir.pelayanan.data',compact('pelayanan','bayi','cap_neo','cap_skr','cap_info','cap_total'));
    }

    public function inputnikPelayanan(){
        if(!empty(Session::get('withnik2'))){
            Session::forget('withnik2');
            return view('spm.bayibarulahir.pelayanan.masukan_nik');  
        }
        else{
            return view('spm.bayibarulahir.pelayanan.masukan_nik');  
        }
    }

    public function inputPelayanan(Request $request){
        date_default_timezone_set('Asia/Jakarta');
        Session::put('withid',$request->id);
        $id = Session::get('withid');
        $data = BayiBaruLahir::where('id', $id)->where('status_kelahiran','tahap pelayanan')->first();
        if(count($data) > 0 ){
            $request_id = $data->id;
            $tgl_pel = [];
            $pelayanan_tgl_pel = Pelayanan::where('table_spm', 'bayi_baru_lahir')->where('id_spm',$request->id)->get();
            foreach($pelayanan_tgl_pel as $i => $data_pelayanan){
                $tgl_pel[$i] = $data_pelayanan['tanggal_pelayanan'];
            }
            $usia = BayiBaruLahirController::Usia($data['tgl_lahir'],date("Y/m/d/h:i:s"));
            $umur = $usia["years"]."_".$usia["months"]."_".$usia["days"]."_".$usia["hours"];
            if($usia["years"] == 0 && $usia["months"]== 0 && $usia["days"] == 0 && $usia["hours"] <= 6 ){
                $form = "pertama";
            }
            else if ($usia["years"] == 0 && $usia["months"]== 0 && $usia["days"] > 0 && $usia["days"] < 28 ||
            $usia["years"] == 0 && $usia["months"]== 0 && $usia["days"] == 0 && $usia["hours"] > 6 ){
                $form = "kedua";
            }
            return view('spm.bayibarulahir.pelayanan.tambah', compact('form','nik','data','datanik','totalcapaian','super_pel_total','tgl_pel','umur','request_id'));
        }
        else{
            return view('spm.bayibarulahir.pelayanan.masukan_nik')->with('alert','NIK belum terdaftar sebagai Bayi Baru Lahir');
        }
    }

    public function addPelayanan(Request $request){
        $a = date('Y-m-d', strtotime($request->tanggal_pelayanan));
        $fix_tgl = $a." ".$request->waktu_pelayanan.":00";
        $neo_pertama = "t_t_t_t_t_t_t_t";
        $neo_kedua = "t_t_t_t_t_t";
        
        //Neoesensial
        if($request->form == "pertama"){
            //if pertama 6 jam kebawah
            if($request->imd != null){$imd="y";}else{$imd="t";}
            if($request->svk1 != null){$svk1="y";}else{$svk1="t";}
            if($request->ptp != null){$ptp="y";}else{$ptp="t";}
            if($request->sma != null){$sma="y";}else{$sma="t";}
            if($request->ihb0 != null){$ihb0="y";}else{$ihb0="t";}
            if($request->ptb != null){$ptb="y";}else{$ptb="t";}
            if($request->pab != null){$pab="y";}else{$pab="t";}
            if($request->pti != null){$pti="y";}else{$pti="t";}
            $neo_pertama = $imd."_".$svk1."_".$ptp."_".$sma."_".$ihb0."_".$ptb."_".$pab."_".$pti;
        }
        elseif($request->form == "kedua"){
           //if kedua 6 jam keatas
           if($request->ptpusat != null){$ptpusat="y";}else{$ptpusat="t";}
           if($request->pmk != null){$pmk="y";}else{$pmk="t";}
           if($request->psvk1p != null){$psvk1p="y";}else{$psvk1p="t";}
           if($request->imun != null){$imun="y";}else{$imun="t";}
           if($request->pbbls != null){$pbbls="y";}else{$pbbls="t";}
           if($request->pkb != null){$pkb="y";}else{$pkb="t";}
           $neo_kedua = $ptpusat."_".$pmk."_".$psvk1p."_".$imun."_".$pbbls."_".$pkb;
        }
            //fisik
                //fisik apgar
               $neo_fisik_apgar = $request->fisik_apgar_otot."_".$request->fisik_apgar_respon."_".$request->fisik_apgar_warna."_".$request->fisik_apgar_napas."_".$request->fisik_apgar_jantung;
                //fisik ges
                $neo_fisik_geo = $request->fisik_ges_berat."_".$request->fisik_ges_panjang."_".$request->fisik_ges_lingkarKepala;
                //fisik kl
                if($request->fisik_kl_kepala != null){$fisik_kl_kepala = "y";}else{$fisik_kl_kepala = "t";}
                if($request->fisik_kl_leher != null){$fisik_kl_leher = "y";}else{$fisik_kl_leher = "t";}
                if($request->fisik_kl_mata != null){$fisik_kl_mata = "y";}else{$fisik_kl_mata = "t";}
                if($request->fisik_kl_hidung != null){$fisik_kl_hidung = "y";}else{$fisik_kl_hidung = "t";}
                if($request->fisik_kl_telinga != null){$fisik_kl_telinga = "y";}else{$fisik_kl_telinga = "t";}
                $neo_fisik_kl = $fisik_kl_kepala."_".$fisik_kl_leher."_".$fisik_kl_mata."_".$fisik_kl_hidung."_".$fisik_kl_telinga;
                //fisik mulut
                if($request->fisik_mulut_gusi != null){$fisik_mulut_gusi = "y";}else{$fisik_mulut_gusi = "t";}
                if($request->fisik_mulut_langit != null){$fisik_mulut_langit = "y";}else{$fisik_mulut_langit = "t";}
                $neo_fisik_mulut = $fisik_mulut_gusi."_".$fisik_mulut_langit;
                //fisik pk
                if($request->fisik_pk_bentukPerut != null){$fisik_pk_bentukPerut = "y";}else{$fisik_pk_bentukPerut = "t";}
                if($request->fisik_pk_lingkarPerut != null){$fisik_pk_lingkarPerut = "y";}else{$fisik_pk_lingkarPerut = "t";}
                if($request->fisik_pk_organPerut != null){$fisik_pk_organPerut = "y";}else{$fisik_pk_organPerut = "t";}
                if($request->fisik_pk_organKelamin != null){$fisik_pk_organKelamin = "y";}else{$fisik_pk_organKelamin = "t";}
                $neo_fisik_pk = $fisik_pk_bentukPerut."_".$fisik_pk_lingkarPerut."_".$fisik_pk_organPerut."_".$fisik_pk_organKelamin;
                //fisik tbtk
                if($request->fisik_tbtk_tulangbel != null){$fisik_tbtk_tulangbel = "y";}else{$fisik_tbtk_tulangbel = "t";}
                if($request->fisik_tbtk_tangan != null){$fisik_tbtk_tangan = "y";}else{$fisik_tbtk_tangan = "t";}
                if($request->fisik_tbtk_kaki != null){$fisik_tbtk_kaki = "y";}else{$fisik_tbtk_kaki = "t";}
                $neo_fisik_tbtk = $fisik_tbtk_tulangbel."_".$fisik_tbtk_tangan."_".$fisik_tbtk_kaki;
        // Skrining
            //shk
                if($request->s_shk_hsd != null){$s_shk_hsd = "y";}else{$s_shk_hsd = "t";}
        //Pemberian Komunikasi, Informasi, Edukasi
        if($request->kie_perawatan != null){$kie_perawatan = "y";}else{$kie_perawatan = "t";}   
        if($request->kie_skrining != null){$kie_skrining = "y";}else{$kie_skrining = "t";}   
        if($request->kie_bahaya != null){$kie_bahaya = "y";}else{$kie_bahaya = "t";}   
        if($request->kie_pelayanan != null){$kie_pelayanan = "y";}else{$kie_pelayanan = "t";}   
        if($request->kie_asi != null){$kie_asi = "y";}else{$kie_asi = "t";}   
        $kie_informasi = $kie_perawatan."_".$kie_skrining."_".$kie_bahaya."_".$kie_pelayanan."_".$kie_asi;  
        if($request->kie_media != null){$kie_media = "y";}else{$kie_media = "t";}

            $data = Pelayanan::create([
                'tanggal_pelayanan' => $fix_tgl,
                'id_spm' => $request->id,
                'table_spm' => 'bayi_baru_lahir',
                'tenaga_kerja' => $request->s_tenaga_kesehatan."_".$request->s_nama_stk,
                'lokasi' => $request->s_fasilitas_kesehatan."_".$request->s_lokasi_pelayanan,
                'suhu_tubuh' => $request->suhu_tubuh1.",".$request->suhu_tubuh2,
                'neo_pertama' => $neo_pertama,
                'neo_kedua' => $neo_kedua,
                'neo_fisik_apgar' => $neo_fisik_apgar,
                'neo_fisik_geo' => $neo_fisik_geo,
                'neo_fisik_kl' => $neo_fisik_kl,
                'neo_fisik_mulut' => $neo_fisik_mulut,
                'neo_fisik_pk' => $neo_fisik_pk,
                'neo_fisik_tbtk' => $neo_fisik_tbtk,
                'skr_shk_hsd' => $s_shk_hsd,
                'kie_nama' => $request->kie_nama_pi,
                'kie_informasi' => $kie_informasi,
                'kie_media' => $kie_media,
                'created_by'=> $request->created_by
            ]);

            if($data->save()){
                return Redirect::route('detail-bayi-baru-lahir',[$request->id])->with('success','Berhasil Tambah Pelayanan Bayi Baru Lahir');
            }  
            else{
                return Redirect::route('catat-bayi-baru-lahir')->with('danger','Gagal Tambah Pelayanan Bayi Baru Lahir');
            }
    }

    public function detailPelayanan($id){
        $pelayanan = Pelayanan::where('id',$id)->first();
        $bayi = BayiBaruLahir::where('id',$pelayanan['id_spm'])->first();
        $databayi = Warga::where('nik',$bayi['nik'])->first();
        $batas6jam = date('Y-m-d h:i:s', strtotime('+6 hour', strtotime($databayi->tgl_lahir)));
        $batas2hari = date('Y-m-d h:i:s', strtotime('+2 day', strtotime($databayi->tgl_lahir)));
        $batas7hari = date('Y-m-d h:i:s', strtotime('+7 day', strtotime($databayi->tgl_lahir)));
        $batas28hari = date('Y-m-d h:i:s', strtotime('+28 day', strtotime($databayi->tgl_lahir)));
        if($pelayanan != null){
            $pel = $pelayanan;
            if($pel['suhu_tubuh'] != null){
                $suhu_tubuh = explode(',',$pel['suhu_tubuh'])[0].".".explode(',',$pel['suhu_tubuh'])[1];
                if(floatval($pel['suhu_tubuh']) >= 36.4 && floatval($pel['suhu_tubuh']) <= 37.5){
                    $capaian0 = 1;
                }else{
                    $capaian0 = 0;
                }
            }
            else{
                $capaian0 = 0;
            }
    
            if($pel['neo_pertama'] == null || $pel['neo_pertama'] == "_______" || $pel['neo_pertama'] == "t_t_t_t_t_t_t_t" ){
                $capaian1 = 0;$capaian2 = 0;$capaian3 = 0;$capaian4 = 0;
                $capaian5 = 0;$capaian6 = 0;$capaian7 = 0;$capaian8 = 0;
            }
            else{
                if(explode("_",$pel['neo_pertama'])[0] == "y"){$capaian1 = 1;}else{$capaian1 = 0;}
                if(explode("_",$pel['neo_pertama'])[1] == "y"){$capaian2 = 1;}else{$capaian2 = 0;}
                if(explode("_",$pel['neo_pertama'])[2] == "y"){$capaian3 = 1;}else{$capaian3 = 0;}
                if(explode("_",$pel['neo_pertama'])[3] == "y"){$capaian4 = 1;}else{$capaian4 = 0;}
                if(explode("_",$pel['neo_pertama'])[4] == "y"){$capaian5 = 1;}else{$capaian5 = 0;}
                if(explode("_",$pel['neo_pertama'])[5] == "y"){$capaian6 = 1;}else{$capaian6 = 0;}
                if(explode("_",$pel['neo_pertama'])[6] == "y"){$capaian7 = 1;}else{$capaian7 = 0;}
                if(explode("_",$pel['neo_pertama'])[7] == "y"){$capaian8 = 1;}else{$capaian8 = 0;}
    
            }
            
            if($pel['neo_kedua'] == null || $pel['neo_kedua'] == "_____" || $pel['neo_kedua'] == "t_t_t_t_t_t" ){
                $capaian9 = 0;$capaian10 = 0;$capaian11 = 0;
                $capaian12 = 0;$capaian13 = 0;$capaian14 = 0;
            }
            else{
                if(explode("_",$pel['neo_kedua'])[0] == "y"){$capaian9 = 1;}else{$capaian9 = 0;}
                if(explode("_",$pel['neo_kedua'])[1] == "y"){$capaian10 = 1;}else{$capaian10 = 0;}
                if(explode("_",$pel['neo_kedua'])[2] == "y"){$capaian11 = 1;}else{$capaian11 = 0;}
                if(explode("_",$pel['neo_kedua'])[3] == "y"){$capaian12 = 1;}else{$capaian12 = 0;}
                if(explode("_",$pel['neo_kedua'])[4] == "y"){$capaian13 = 1;}else{$capaian13 = 0;}
                if(explode("_",$pel['neo_kedua'])[5] == "y"){$capaian14 = 1;}else{$capaian14 = 0;}
            }
    
            if($pel['neo_fisik_apgar'] == null){
                // nilai apgar 0
                $nilai_apgar = 0;
            }else{
                if(explode("_",$pel['neo_fisik_apgar'])[4] == null){$apgar4 = 0;}
                else if(explode("_",$pel['neo_fisik_apgar'])[4] >= 100){$apgar4 = 2;}
                else if(explode("_",$pel['neo_fisik_apgar'])[4] < 100 && explode("_",$pel['neo_fisik_apgar'])[4] > 0){$apgar4 = 1;}
                else if(explode("_",$pel['neo_fisik_apgar'])[4] <= 0){$apgar4 = 0;}
                $nilai_apgar =  explode("_",$pel['neo_fisik_apgar'])[0]+explode("_",$pel['neo_fisik_apgar'])[1]+
                                explode("_",$pel['neo_fisik_apgar'])[2]+explode("_",$pel['neo_fisik_apgar'])[3]+$apgar4;                       
            }
    
            if($nilai_apgar >= 7){
                // cek geo
                if($pel['neo_fisik_geo'] != null && $pel['neo_fisik_geo'] != "__" && explode("_",$pel['neo_fisik_geo'])[0] != 0 && explode("_",$pel['neo_fisik_geo'])[1] != 0 && explode("_",$pel['neo_fisik_geo'])[2] != 0 ){
                    //cek kl
                    if($pel['neo_fisik_kl'] != null && $pel['neo_fisik_kl'] != "____" && explode("_",$pel['neo_fisik_kl'])[0] == "y" && explode("_",$pel['neo_fisik_kl'])[1] == "y" && explode("_",$pel['neo_fisik_kl'])[2] == "y" && explode("_",$pel['neo_fisik_kl'])[3] == "y" && explode("_",$pel['neo_fisik_kl'])[4] == "y"){
                        //cek mulut
                        if($pel['neo_fisik_mulut'] != null && $pel['neo_fisik_mulut'] != "_" && explode("_",$pel['neo_fisik_mulut'])[0] == "y" && explode("_",$pel['neo_fisik_mulut'])[1] == "y"){
                            //cek pk
                            if($pel['neo_fisik_pk'] != null && $pel['neo_fisik_pk'] != "__" && explode("_",$pel['neo_fisik_pk'])[0] == "y" && explode("_",$pel['neo_fisik_pk'])[1] == "y" && explode("_",$pel['neo_fisik_pk'])[2] == "y"){
                                //cek tbtk
                                if($pel['neo_fisik_tbtk'] != null && $pel['neo_fisik_tbtk'] != "__" && explode("_",$pel['neo_fisik_tbtk'])[0] == "y" && explode("_",$pel['neo_fisik_tbtk'])[1] == "y" && explode("_",$pel['neo_fisik_tbtk'])[2] == "y"){
                                    //cek tbtk
                                    $capaian15 = 1;                        
                                }else{
                                    $capaian15 = 0;
                                }
                            }else{
                                //gak perlu cek tbtk
                                $capaian15 = 0;
                            }
                        }else{
                            //gak perlu cek pk dll
                            $capaian15 = 0;
                        }
                    }else{
                        //gak perlu cek mulut dll
                        $capaian15 = 0;
                    }
                }else{
                    //gak perlu cek kl dll
                    $capaian15 = 0;
                }
            }else{
                //gak perlu cek geo dll
                $capaian15 = 0;
            }
    
            if($pel['tenaga_kerja'] != null && $pel['tenaga_kerja'] != "_" && explode("_",$pel['tenaga_kerja'])[1] != null)
            {$capaian16 = 1;}else{$capaian16 = 0;}
            if($pel['lokasi'] != null && $pel['lokasi'] != "_" && explode("_",$pel['lokasi'])[1] != null)
            {$capaian17 = 1;}else{$capaian17 = 0;}
            if($pel['skr_shk_hsd'] != null){$capaian18 = 1;}else{$capaian18 = 0;}
            
            if($pel['kie_nama'] != null){$capaian19 = 1;}else{$capaian19 = 0;}
            if($pel['kie_informasi'] != null && $pel['kie_informasi'] != "t_t_t_t_t" ){
                if(explode("_",$pel['kie_informasi'])[0] == "y"){$capaian20 = 1;}else{$capaian20 = 0;}
                if(explode("_",$pel['kie_informasi'])[1] == "y"){$capaian21 = 1;}else{$capaian21 = 0;}
                if(explode("_",$pel['kie_informasi'])[2] == "y"){$capaian22 = 1;}else{$capaian22 = 0;}
                if(explode("_",$pel['kie_informasi'])[3] == "y"){$capaian23 = 1;}else{$capaian23 = 0;}
                if(explode("_",$pel['kie_informasi'])[4] == "y"){$capaian24 = 1;}else{$capaian24 = 0;}            
            }else{
                $capaian20 = 0;$capaian21 = 0;$capaian22 = 0;
                $capaian23 = 0;$capaian24 = 0;
    
            }
            if($pel['kie_media'] != null){$capaian25 = 1;}else{$capaian25 = 0;}            
            
            if($pel['tanggal_pelayanan'] >= $batas6jam && $pel['tanggal_pelayanan'] < $batas2hari){
                $cap_neo = (($capaian0+$capaian1+$capaian2+$capaian3+
                $capaian4+$capaian5+$capaian6+$capaian7+$capaian8+$capaian15) *100) /10;
                $form = "pertama";
            }
            else if($pel['tanggal_pelayanan'] > $batas2hari && $pel['tanggal_pelayanan'] < $batas28hari){
                $cap_neo = (($capaian0+$capaian9+$capaian10+$capaian11+
                $capaian12+$capaian13+$capaian14+$capaian15) *100) /8;
                $form = "kedua";
            }
            else{
                $form ="non";
                $cap_neo = 0;
            }
            $cap_skr = (($capaian16+$capaian17+$capaian18) *100) /3;
            $cap_info = (($capaian19+$capaian20+$capaian21+$capaian22+$capaian23+$capaian24+$capaian25) *100) /7;
            $capaian = ($cap_neo+$cap_skr+$cap_info)/3;
        }
        else{
            $cap_neo = 0;
            $cap_skr = 0;
            $cap_info = 0;
            $capaian = 0;
        }
        // print "cap neo :".$cap_neo;
        // print "cap skr :".$cap_skr;
        // print "cap info :".$cap_info;
        // print "capaian :".$capaian;
        return view('spm.bayibarulahir.pelayanan.detail',compact('form','cap_neo','cap_skr','cap_info','capaian','capaian0','capaian1',
        'capaian2','capaian3','capaian4','capaian5','capaian6','capaian7','capaian8','capaian9','capaian10','capaian11',
        'capaian12','capaian13','capaian14','capaian15','capaian16','capaian17','capaian18','capaian19','capaian20',
        'capaian21','capaian22','capaian23','capaian24','capaian25','batas6jam','batas2hari','batas7hari','batas28hari',
        'databayi','bayi','pelayanan'));
        // print $form;
    }

    public function ubahPelayanan($id){
        $pelayanan = Pelayanan::where('id',$id)->first();
        $bayi = BayiBaruLahir::where('id',$pelayanan['id_spm'])->first();
        $databayi = Warga::where('nik',$bayi['nik'])->first();
        $batas6jam = date('Y-m-d h:i:s', strtotime('+6 hour', strtotime($databayi->tgl_lahir)));
        $batas2hari = date('Y-m-d h:i:s', strtotime('+2 day', strtotime($databayi->tgl_lahir)));
        $batas7hari = date('Y-m-d h:i:s', strtotime('+7 day', strtotime($databayi->tgl_lahir)));
        $batas28hari = date('Y-m-d h:i:s', strtotime('+28 day', strtotime($databayi->tgl_lahir)));
        if($pelayanan != null){

            if($pelayanan['suhu_tubuh'] != null){
                $suhu_tubuh = explode(',',$pelayanan['suhu_tubuh'])[0].".".explode(',',$pelayanan['suhu_tubuh'])[1];
                if(floatval($pelayanan['suhu_tubuh']) >= 36.4 && floatval($pelayanan['suhu_tubuh']) <= 37.5){
                    $capaian0 = 1;
                }else{
                    $capaian0 = 0;
                }
            }
            else{
                $capaian0 = 0;
            }
    
            if($pelayanan['neo_pertama'] == null || $pelayanan['neo_pertama'] == "_______" || $pelayanan['neo_pertama'] == "t_t_t_t_t_t_t_t" ){
                $capaian1 = 0;$capaian2 = 0;$capaian3 = 0;$capaian4 = 0;
                $capaian5 = 0;$capaian6 = 0;$capaian7 = 0;$capaian8 = 0;
            }
            else{
                if(explode("_",$pelayanan['neo_pertama'])[0] == "y"){$capaian1 = 1;}else{$capaian1 = 0;}
                if(explode("_",$pelayanan['neo_pertama'])[1] == "y"){$capaian2 = 1;}else{$capaian2 = 0;}
                if(explode("_",$pelayanan['neo_pertama'])[2] == "y"){$capaian3 = 1;}else{$capaian3 = 0;}
                if(explode("_",$pelayanan['neo_pertama'])[3] == "y"){$capaian4 = 1;}else{$capaian4 = 0;}
                if(explode("_",$pelayanan['neo_pertama'])[4] == "y"){$capaian5 = 1;}else{$capaian5 = 0;}
                if(explode("_",$pelayanan['neo_pertama'])[5] == "y"){$capaian6 = 1;}else{$capaian6 = 0;}
                if(explode("_",$pelayanan['neo_pertama'])[6] == "y"){$capaian7 = 1;}else{$capaian7 = 0;}
                if(explode("_",$pelayanan['neo_pertama'])[7] == "y"){$capaian8 = 1;}else{$capaian8 = 0;}
    
            }
            
            if($pelayanan['neo_kedua'] == null || $pelayanan['neo_kedua'] == "_____" || $pelayanan['neo_kedua'] == "t_t_t_t_t_t" ){
                $capaian9 = 0;$capaian10 = 0;$capaian11 = 0;
                $capaian12 = 0;$capaian13 = 0;$capaian14 = 0;
            }
            else{
                if(explode("_",$pelayanan['neo_kedua'])[0] == "y"){$capaian9 = 1;}else{$capaian9 = 0;}
                if(explode("_",$pelayanan['neo_kedua'])[1] == "y"){$capaian10 = 1;}else{$capaian10 = 0;}
                if(explode("_",$pelayanan['neo_kedua'])[2] == "y"){$capaian11 = 1;}else{$capaian11 = 0;}
                if(explode("_",$pelayanan['neo_kedua'])[3] == "y"){$capaian12 = 1;}else{$capaian12 = 0;}
                if(explode("_",$pelayanan['neo_kedua'])[4] == "y"){$capaian13 = 1;}else{$capaian13 = 0;}
                if(explode("_",$pelayanan['neo_kedua'])[5] == "y"){$capaian14 = 1;}else{$capaian14 = 0;}
            }
    
            if($pelayanan['neo_fisik_apgar'] == null){
                // nilai apgar 0
                $nilai_apgar = 0;
            }else{
                if(explode("_",$pelayanan['neo_fisik_apgar'])[4] == null){$apgar4 = 0;}
                else if(explode("_",$pelayanan['neo_fisik_apgar'])[4] >= 100){$apgar4 = 2;}
                else if(explode("_",$pelayanan['neo_fisik_apgar'])[4] < 100 && explode("_",$pelayanan['neo_fisik_apgar'])[4] > 0){$apgar4 = 1;}
                else if(explode("_",$pelayanan['neo_fisik_apgar'])[4] <= 0){$apgar4 = 0;}
                $nilai_apgar =  explode("_",$pelayanan['neo_fisik_apgar'])[0]+explode("_",$pelayanan['neo_fisik_apgar'])[1]+
                                explode("_",$pelayanan['neo_fisik_apgar'])[2]+explode("_",$pelayanan['neo_fisik_apgar'])[3]+$apgar4;                       
            }
    
            if($nilai_apgar >= 7){
                // cek geo
                if($pelayanan['neo_fisik_geo'] != null && $pelayanan['neo_fisik_geo'] != "__" && explode("_",$pelayanan['neo_fisik_geo'])[0] != 0 && explode("_",$pelayanan['neo_fisik_geo'])[1] != 0 && explode("_",$pelayanan['neo_fisik_geo'])[2] != 0 ){
                    //cek kl
                    if($pelayanan['neo_fisik_kl'] != null && $pelayanan['neo_fisik_kl'] != "____" && explode("_",$pelayanan['neo_fisik_kl'])[0] == "y" && explode("_",$pelayanan['neo_fisik_kl'])[1] == "y" && explode("_",$pelayanan['neo_fisik_kl'])[2] == "y" && explode("_",$pelayanan['neo_fisik_kl'])[3] == "y" && explode("_",$pelayanan['neo_fisik_kl'])[4] == "y"){
                        //cek mulut
                        if($pelayanan['neo_fisik_mulut'] != null && $pelayanan['neo_fisik_mulut'] != "_" && explode("_",$pelayanan['neo_fisik_mulut'])[0] == "y" && explode("_",$pelayanan['neo_fisik_mulut'])[1] == "y"){
                            //cek pk
                            if($pelayanan['neo_fisik_pk'] != null && $pelayanan['neo_fisik_pk'] != "__" && explode("_",$pelayanan['neo_fisik_pk'])[0] == "y" && explode("_",$pelayanan['neo_fisik_pk'])[1] == "y" && explode("_",$pelayanan['neo_fisik_pk'])[2] == "y"){
                                //cek tbtk
                                if($pelayanan['neo_fisik_tbtk'] != null && $pelayanan['neo_fisik_tbtk'] != "__" && explode("_",$pelayanan['neo_fisik_tbtk'])[0] == "y" && explode("_",$pelayanan['neo_fisik_tbtk'])[1] == "y" && explode("_",$pelayanan['neo_fisik_tbtk'])[2] == "y"){
                                    //cek tbtk
                                    $capaian15 = 1;                        
                                }else{
                                    $capaian15 = 0;
                                }
                            }else{
                                //gak perlu cek tbtk
                                $capaian15 = 0;
                            }
                        }else{
                            //gak perlu cek pk dll
                            $capaian15 = 0;
                        }
                    }else{
                        //gak perlu cek mulut dll
                        $capaian15 = 0;
                    }
                }else{
                    //gak perlu cek kl dll
                    $capaian15 = 0;
                }
            }else{
                //gak perlu cek geo dll
                $capaian15 = 0;
            }
    
            if($pelayanan['tenaga_kerja'] != null && $pelayanan['tenaga_kerja'] != "_" && explode("_",$pelayanan['tenaga_kerja'])[1] != null)
            {$capaian16 = 1;}else{$capaian16 = 0;}
            if($pelayanan['lokasi'] != null && $pelayanan['lokasi'] != "_" && explode("_",$pelayanan['lokasi'])[1] != null)
            {$capaian17 = 1;}else{$capaian17 = 0;}
            if($pelayanan['skr_shk_hsd'] != null){$capaian18 = 1;}else{$capaian18 = 0;}
            
            if($pelayanan['kie_nama'] != null){$capaian19 = 1;}else{$capaian19 = 0;}
            if($pelayanan['kie_informasi'] != null && $pelayanan['kie_informasi'] != "t_t_t_t_t" ){
                if(explode("_",$pelayanan['kie_informasi'])[0] == "y"){$capaian20 = 1;}else{$capaian20 = 0;}
                if(explode("_",$pelayanan['kie_informasi'])[1] == "y"){$capaian21 = 1;}else{$capaian21 = 0;}
                if(explode("_",$pelayanan['kie_informasi'])[2] == "y"){$capaian22 = 1;}else{$capaian22 = 0;}
                if(explode("_",$pelayanan['kie_informasi'])[3] == "y"){$capaian23 = 1;}else{$capaian23 = 0;}
                if(explode("_",$pelayanan['kie_informasi'])[4] == "y"){$capaian24 = 1;}else{$capaian24 = 0;}            
            }else{
                $capaian20 = 0;$capaian21 = 0;$capaian22 = 0;
                $capaian23 = 0;$capaian24 = 0;
    
            }
            if($pelayanan['kie_media'] != null){$capaian25 = 1;}else{$capaian25 = 0;}            
               
        }
    
        if($pelayanan['tanggal_pelayanan'] > $batas6jam && $pelayanan['tanggal_pelayanan'] < $batas2hari){
            $form = "pertama";               
        }
        else if($pelayanan['tanggal_pelayanan'] > $batas2hari && $pelayanan['tanggal_pelayanan'] < $batas28hari){
            $form = "kedua";
        }
        else{
            $form = "non";
        }

        $total_pelayanan =  Pelayanan::where('id_spm',$bayi['id'])->where('table_spm','bayi_baru_lahir')->get();
        // print $total_pelayanan;
        foreach($total_pelayanan as $i => $pel){  
            if($pel['tanggal_pelayanan'] > $batas6jam && $pel['tanggal_pelayanan'] < $batas2hari){
                $CapaianPelayananperiode1[$i] = BayiBaruLahirController::CekCapaian($pel['suhu_tubuh'],$pel['neo_pertama'],$pel['neo_kedua'],$pel['neo_fisik_apgar'],
                $pel['neo_fisik_geo'],$pel['neo_fisik_kl'],$pel['neo_fisik_mulut'],$pel['neo_fisik_pk'],$pel['neo_fisik_tbtk'],
                $pel['tenaga_kerja'],$pel['lokasi'],$pel['skr_shk_hsd'],$pel['kie_nama'],$pel['kie_informasi'],$pel['kie_media'],$i);
            }
            else if($pel['tanggal_pelayanan'] > $batas2hari && $pel['tanggal_pelayanan'] < $batas7hari){
                $CapaianPelayananperiode2[$i] = BayiBaruLahirController::CekCapaian($pel['suhu_tubuh'],$pel['neo_pertama'],$pel['neo_kedua'],$pel['neo_fisik_apgar'],
                $pel['neo_fisik_geo'],$pel['neo_fisik_kl'],$pel['neo_fisik_mulut'],$pel['neo_fisik_pk'],$pel['neo_fisik_tbtk'],
                $pel['tenaga_kerja'],$pel['lokasi'],$pel['skr_shk_hsd'],$pel['kie_nama'],$pel['kie_informasi'],$pel['kie_media'],$i);
            }
            else if($pel['tanggal_pelayanan'] > $batas7hari && $pel['tanggal_pelayanan'] < $batas28hari){
                $CapaianPelayananperiode3[$i] = BayiBaruLahirController::CekCapaian($pel['suhu_tubuh'],$pel['neo_pertama'],$pel['neo_kedua'],$pel['neo_fisik_apgar'],
                $pel['neo_fisik_geo'],$pel['neo_fisik_kl'],$pel['neo_fisik_mulut'],$pel['neo_fisik_pk'],$pel['neo_fisik_tbtk'],
                $pel['tenaga_kerja'],$pel['lokasi'],$pel['skr_shk_hsd'],$pel['kie_nama'],$pel['kie_informasi'],$pel['kie_media'],$i);
            }
        }

        $kri = [];
        $kri2 = [];
        $kri3 = [];

        $kri[0] = 0;$kri[1] = 0;$kri[2] = 0;$kri[3] = 0;$kri[4] = 0;
        $kri[5] = 0;
            $kri[6] = 0;
            $kri[7] = 0;
            $kri[8] = 0;
            $kri[9] = 0;
            $kri[10] = 0;
            $kri[11] = 0;
            $kri[12] = 0;
            $kri[13] = 0;
            $kri[14] = 0;
            $kri[15] = 0;
            $kri[16] = 0;
            $kri[17] = 0;
            $kri[18] = 0;
            $kri[19] = 0;
            $kri[20] = 0;
            $kri[21] = 0;
            $kri[22] = 0;
            $kri[23] = 0;
            $kri[24] = 0;
            $kri[25] = 0;
            $kri[26] = 0;

            $kri2[0] = 0;
            $kri[1] = 0;
            $kri2[2] = 0;
            $kri2[3] = 0;
            $kri2[4] = 0;
            $kri2[5] = 0;
            $kri2[6] = 0;
            $kri2[7] = 0;
            $kri2[8] = 0;
            $kri2[9] = 0;
            $kri2[10] = 0;
            $kri2[11] = 0;
            $kri2[12] = 0;
            $kri2[13] = 0;
            $kri2[14] = 0;
            $kri2[15] = 0;
            $kri2[16] = 0;
            $kri2[17] = 0;
            $kri2[18] = 0;
            $kri2[19] = 0;
            $kri2[20] = 0;
            $kri2[21] = 0;
            $kri2[22] = 0;
            $kri2[23] = 0;
            $kri2[24] = 0;
            $kri2[25] = 0;
            $kri2[26] = 0;

            $kri3[0] = 0;
            $kri3[1] = 0;
            $kri3[2] = 0;
            $kri3[3] = 0;
            $kri3[4] = 0;
            $kri3[5] = 0;
            $kri3[6] = 0;
            $kri3[7] = 0;
            $kri3[8] = 0;
            $kri3[9] = 0;
            $kri3[10] = 0;
            $kri3[11] = 0;
            $kri3[12] = 0;
            $kri3[13] = 0;
            $kri3[14] = 0;
            $kri3[15] = 0;
            $kri3[16] = 0;
            $kri3[17] = 0;
            $kri3[18] = 0;
            $kri3[19] = 0;
            $kri3[20] = 0;
            $kri3[21] = 0;
            $kri3[22] = 0;
            $kri3[23] = 0;
            $kri3[24] = 0;
            $kri3[25] = 0;
            $kri3[26] = 0; 
        for($i=0;$i<26;$i++){    
            foreach($total_pelayanan as $j => $pel){
                if($pel['tanggal_pelayanan'] > $batas6jam && $pel['tanggal_pelayanan'] < $batas2hari){
                    if($kri[$i] <1){   
                        $kri[$i]= intval($kri[$i])+intval($CapaianPelayananperiode1[$j][$i][$j]);
                    }
                                    
                }
                else if($pel['tanggal_pelayanan'] > $batas2hari && $pel['tanggal_pelayanan'] < $batas7hari){
                    if($kri2[$i] <1){   
                        $kri2[$i]= intval($kri2[$i])+intval($CapaianPelayananperiode2[$j][$i][$j]);
                    }
                    
                }
                else if($pel['tanggal_pelayanan'] > $batas7hari && $pel['tanggal_pelayanan'] < $batas28hari){
                    if($kri3[$i] <1){   
                        $kri3[$i]= intval($kri3[$i])+intval($CapaianPelayananperiode3[$j][$i][$j]);
                    }
                }
            }
        }


        if($kri != null){
            $capaian_all_pel_periode1 = (((($kri[0]+$kri[1]+$kri[2]+$kri[3]+$kri[4]+$kri[5]+$kri[6]+$kri[7]+$kri[8]+$kri[15])*100) /10)+((($kri[16]+$kri[17]+$kri[18])*100)/3)+((($kri[19]+$kri[20]+$kri[21]+$kri[22]+$kri[23]+$kri[24]+$kri[25])*100) /7) )/3;
        }
        else{
            $capaian_all_pel_periode1 = 0;
        }
        if($kri2 != null){
            $capaian_all_pel_periode2 = (((($kri2[0]+$kri2[9]+$kri2[10]+$kri2[11]+$kri2[12]+$kri2[13]+$kri2[14]+$kri2[15])*100) /8)+((($kri2[16]+$kri2[17]+$kri2[18])*100)/3)+((($kri2[19]+$kri2[20]+$kri2[21]+$kri2[22]+$kri2[23]+$kri2[24]+$kri2[25])*100) /7) )/3;
        }
        else{
            $capaian_all_pel_periode2 = 0;
        }
        if($kri3 != null){
            $capaian_all_pel_periode3 = (((($kri3[0]+$kri3[9]+$kri3[10]+$kri3[11]+$kri3[12]+$kri3[13]+$kri3[14]+$kri3[15])*100) /8)+((($kri3[16]+$kri3[17]+$kri3[18])*100)/3)+((($kri3[19]+$kri3[20]+$kri3[21]+$kri3[22]+$kri3[23]+$kri3[24]+$kri3[25])*100) /7) )/3;
        }
        else{
            $capaian_all_pel_periode3 = 0;
        }
        $total_capaian = ($capaian_all_pel_periode1+$capaian_all_pel_periode2+$capaian_all_pel_periode3)/3;

        return view('spm.bayibarulahir.pelayanan.ubah',compact('total_capaian','form','cap_neo','cap_skr','cap_info','capaian','capaian0','capaian1',
        'capaian2','capaian3','capaian4','capaian5','capaian6','capaian7','capaian8','capaian9','capaian10','capaian11',
        'capaian12','capaian13','capaian14','capaian15','capaian16','capaian17','capaian18','capaian19','capaian20',
        'capaian21','capaian22','capaian23','capaian24','capaian25','batas6jam','batas2hari','batas7hari','batas28hari',
        'databayi','bayi','pelayanan'));
    }

    public function editPelayanan(Request $request){
        $pelayanan = Pelayanan::where('id',$request->id)->first();
        $a = date('Y-m-d', strtotime($request->tanggal_pelayanan));
        $fix_tgl = $a." ".$request->waktu_pelayanan.":00";
        $neo_pertama = "t_t_t_t_t_t_t_t";
        $neo_kedua = "t_t_t_t_t_t";
        
        //Neoesensial
        if($request->form == "pertama"){
            //if pertama 6 jam kebawah
            if($request->imd != null){$imd="y";}else{$imd="t";}
            if($request->svk1 != null){$svk1="y";}else{$svk1="t";}
            if($request->ptp != null){$ptp="y";}else{$ptp="t";}
            if($request->sma != null){$sma="y";}else{$sma="t";}
            if($request->ihb0 != null){$ihb0="y";}else{$ihb0="t";}
            if($request->ptb != null){$ptb="y";}else{$ptb="t";}
            if($request->pab != null){$pab="y";}else{$pab="t";}
            if($request->pti != null){$pti="y";}else{$pti="t";}
            $neo_pertama = $imd."_".$svk1."_".$ptp."_".$sma."_".$ihb0."_".$ptb."_".$pab."_".$pti;
        }
        elseif($request->form == "kedua"){
           //if kedua 6 jam keatas
           if($request->ptpusat != null){$ptpusat="y";}else{$ptpusat="t";}
           if($request->pmk != null){$pmk="y";}else{$pmk="t";}
           if($request->psvk1p != null){$psvk1p="y";}else{$psvk1p="t";}
           if($request->imun != null){$imun="y";}else{$imun="t";}
           if($request->pbbls != null){$pbbls="y";}else{$pbbls="t";}
           if($request->pkb != null){$pkb="y";}else{$pkb="t";}
           $neo_kedua = $ptpusat."_".$pmk."_".$psvk1p."_".$imun."_".$pbbls."_".$pkb;
        }
            //fisik
                //fisik apgar
               $neo_fisik_apgar = $request->fisik_apgar_otot."_".$request->fisik_apgar_respon."_".$request->fisik_apgar_warna."_".$request->fisik_apgar_napas."_".$request->fisik_apgar_jantung;
                //fisik ges
                $neo_fisik_geo = $request->fisik_ges_berat."_".$request->fisik_ges_panjang."_".$request->fisik_ges_lingkarKepala;
                //fisik kl
                if($request->fisik_kl_kepala != null){$fisik_kl_kepala = "y";}else{$fisik_kl_kepala = "t";}
                if($request->fisik_kl_leher != null){$fisik_kl_leher = "y";}else{$fisik_kl_leher = "t";}
                if($request->fisik_kl_mata != null){$fisik_kl_mata = "y";}else{$fisik_kl_mata = "t";}
                if($request->fisik_kl_hidung != null){$fisik_kl_hidung = "y";}else{$fisik_kl_hidung = "t";}
                if($request->fisik_kl_telinga != null){$fisik_kl_telinga = "y";}else{$fisik_kl_telinga = "t";}
                $neo_fisik_kl = $fisik_kl_kepala."_".$fisik_kl_leher."_".$fisik_kl_mata."_".$fisik_kl_hidung."_".$fisik_kl_telinga;
                //fisik mulut
                if($request->fisik_mulut_gusi != null){$fisik_mulut_gusi = "y";}else{$fisik_mulut_gusi = "t";}
                if($request->fisik_mulut_langit != null){$fisik_mulut_langit = "y";}else{$fisik_mulut_langit = "t";}
                $neo_fisik_mulut = $fisik_mulut_gusi."_".$fisik_mulut_langit;
                //fisik pk
                if($request->fisik_pk_bentukPerut != null){$fisik_pk_bentukPerut = "y";}else{$fisik_pk_bentukPerut = "t";}
                if($request->fisik_pk_lingkarPerut != null){$fisik_pk_lingkarPerut = "y";}else{$fisik_pk_lingkarPerut = "t";}
                if($request->fisik_pk_organPerut != null){$fisik_pk_organPerut = "y";}else{$fisik_pk_organPerut = "t";}
                if($request->fisik_pk_organKelamin != null){$fisik_pk_organKelamin = "y";}else{$fisik_pk_organKelamin = "t";}
                $neo_fisik_pk = $fisik_pk_bentukPerut."_".$fisik_pk_lingkarPerut."_".$fisik_pk_organPerut."_".$fisik_pk_organKelamin;
                //fisik tbtk
                if($request->fisik_tbtk_tulangbel != null){$fisik_tbtk_tulangbel = "y";}else{$fisik_tbtk_tulangbel = "t";}
                if($request->fisik_tbtk_tangan != null){$fisik_tbtk_tangan = "y";}else{$fisik_tbtk_tangan = "t";}
                if($request->fisik_tbtk_kaki != null){$fisik_tbtk_kaki = "y";}else{$fisik_tbtk_kaki = "t";}
                $neo_fisik_tbtk = $fisik_tbtk_tulangbel."_".$fisik_tbtk_tangan."_".$fisik_tbtk_kaki;
        // Skrining
            //shk
                if($request->s_shk_hsd != null){$s_shk_hsd = "y";}else{$s_shk_hsd = "t";}
        //Pemberian Komunikasi, Informasi, Edukasi
        if($request->kie_perawatan != null){$kie_perawatan = "y";}else{$kie_perawatan = "t";}   
        if($request->kie_skrining != null){$kie_skrining = "y";}else{$kie_skrining = "t";}   
        if($request->kie_bahaya != null){$kie_bahaya = "y";}else{$kie_bahaya = "t";}   
        if($request->kie_pelayanan != null){$kie_pelayanan = "y";}else{$kie_pelayanan = "t";}   
        if($request->kie_asi != null){$kie_asi = "y";}else{$kie_asi = "t";}   
        $kie_informasi = $kie_perawatan."_".$kie_skrining."_".$kie_bahaya."_".$kie_pelayanan."_".$kie_asi;  
        if($request->kie_media != null){$kie_media = "y";}else{$kie_media = "t";}

        $pelayanan['tanggal_pelayanan'] = $fix_tgl;
        $pelayanan['tenaga_kerja'] = $request->s_tenaga_kesehatan."_".$request->s_nama_stk;
        $pelayanan['lokasi'] = $request->s_fasilitas_kesehatan."_".$request->s_lokasi_pelayanan;
        $pelayanan['suhu_tubuh'] = $request->suhu_tubuh1.",".$request->suhu_tubuh2;
        $pelayanan['neo_pertama'] = $neo_pertama;
        $pelayanan['neo_kedua'] = $neo_kedua;
        $pelayanan['neo_fisik_apgar'] = $neo_fisik_apgar;
        $pelayanan['neo_fisik_geo'] = $neo_fisik_geo;
        $pelayanan['neo_fisik_kl'] = $neo_fisik_kl;
        $pelayanan['neo_fisik_mulut'] = $neo_fisik_mulut;
        $pelayanan['neo_fisik_pk'] = $neo_fisik_pk;
        $pelayanan['neo_fisik_tbtk'] = $neo_fisik_tbtk;
        $pelayanan['skr_shk_hsd'] = $s_shk_hsd;
        $pelayanan['kie_nama'] = $request->kie_nama_pi;
        $pelayanan['kie_informasi'] = $kie_informasi;
        $pelayanan['kie_media'] = $kie_media;

        if($pelayanan->save()){
            return redirect('/pelayanan-bayi-baru-lahir')->with('alert', 'Berhasil Ubah Pelayanan Bayi Baru Lahir');
        }  
        else{
            return redirect('/catat-bayi-baru-lahir')->with('alert', 'Gagal Ubah Pelayanan Bayi Baru Lahir');
        }
    }

    public function Usia($tgl1, $tgl2){
        $tgl1 = (is_string($tgl1) ? strtotime($tgl1) : $tgl1);
        $tgl2 = (is_string($tgl2) ? strtotime($tgl2) : $tgl2);
        $diff_secs = abs($tgl1-$tgl2);
        $base_year = min(date("Y",$tgl1), date("Y",$tgl2));
        $diff = mktime(0,0,$diff_secs,1,1,$base_year);
        return array(
            "years"=>date("Y",$diff) - $base_year, 
            "months_total" => (date("Y",$diff) - $base_year) * 12 + date("n",$diff) - 1, 
            "months" => date("n",$diff) -1, 
            "days_total" => floor($diff_secs/ (3600 * 24)), 
            "days" => date("j",$diff) - 1, 
            "hours_total" => floor($diff_secs/3600), 
            "hours" => date("G", $diff), 
            "minutes_total" => floor($diff_secs/60), 
            "minutes" => (int)date("i",$diff),
            "seconds_total" => $diff_secs, 
            "seconds" => (int) date("s",$diff)
        );
    }

    public function CekCapaian($suhu_tubuh,$neo_pertama,$neo_kedua,$neo_fisik_apgar,$neo_fisik_geo,$neo_fisik_kl,
    $neo_fisik_mulut,$neo_fisik_pk,$neo_fisik_tbtk,$tenaga_kerja,$lokasi,$skr_shk_hsd,$kie_nama,$kie_informasi,$kie_media,$i){                    
        
        if($suhu_tubuh != null){
            $suhu_tubuh = explode(',',$suhu_tubuh)[0].".".explode(',',$suhu_tubuh)[1];
            if(floatval($suhu_tubuh) >= 36.4 && floatval($suhu_tubuh) <= 37.5){
                $capaian[0][$i] = 1;
            }else{
                $capaian[0][$i] = 0;
            }
        }
        else{
            $capaian[0][$i] = 0;
        }

        if($neo_pertama == null || $neo_pertama == "_______" || $neo_pertama == "t_t_t_t_t_t_t_t" ){
            $capaian[1][$i] = 0;$capaian[2][$i] = 0;$capaian[3][$i] = 0;$capaian[4][$i] = 0;
            $capaian[5][$i] = 0;$capaian[6][$i] = 0;$capaian[7][$i] = 0;$capaian[8][$i] = 0;
        }
        else{
            if(explode("_",$neo_pertama)[0] == "y"){$capaian[1][$i] = 1;}else{$capaian[1][$i] = 0;}
            if(explode("_",$neo_pertama)[1] == "y"){$capaian[2][$i] = 1;}else{$capaian[2][$i] = 0;}
            if(explode("_",$neo_pertama)[2] == "y"){$capaian[3][$i] = 1;}else{$capaian[3][$i] = 0;}
            if(explode("_",$neo_pertama)[3] == "y"){$capaian[4][$i] = 1;}else{$capaian[4][$i] = 0;}
            if(explode("_",$neo_pertama)[4] == "y"){$capaian[5][$i] = 1;}else{$capaian[5][$i] = 0;}
            if(explode("_",$neo_pertama)[5] == "y"){$capaian[6][$i] = 1;}else{$capaian[6][$i] = 0;}
            if(explode("_",$neo_pertama)[6] == "y"){$capaian[7][$i] = 1;}else{$capaian[7][$i] = 0;}
            if(explode("_",$neo_pertama)[7] == "y"){$capaian[8][$i] = 1;}else{$capaian[8][$i] = 0;}

        }
        
        if($neo_kedua == null || $neo_kedua == "_____" || $neo_kedua == "t_t_t_t_t_t" ){
            $capaian[9][$i] = 0;$capaian[10][$i] = 0;$capaian[11][$i] = 0;
            $capaian[12][$i] = 0;$capaian[13][$i] = 0;$capaian[14][$i] = 0;
        }
        else{
            if(explode("_",$neo_kedua)[0] == "y"){$capaian[9][$i] = 1;}else{$capaian[9][$i] = 0;}
            if(explode("_",$neo_kedua)[1] == "y"){$capaian[10][$i] = 1;}else{$capaian[10][$i] = 0;}
            if(explode("_",$neo_kedua)[2] == "y"){$capaian[11][$i] = 1;}else{$capaian[11][$i] = 0;}
            if(explode("_",$neo_kedua)[3] == "y"){$capaian[12][$i] = 1;}else{$capaian[12][$i] = 0;}
            if(explode("_",$neo_kedua)[4] == "y"){$capaian[13][$i] = 1;}else{$capaian[13][$i] = 0;}
            if(explode("_",$neo_kedua)[5] == "y"){$capaian[14][$i] = 1;}else{$capaian[14][$i] = 0;}
        }

        if($neo_fisik_apgar == null){
            // nilai apgar 0
            $nilai_apgar = 0;
        }else{
            if(explode("_",$neo_fisik_apgar)[4] == null){$apgar4 = 0;}
            else if(explode("_",$neo_fisik_apgar)[4] >= 100){$apgar4 = 2;}
            else if(explode("_",$neo_fisik_apgar)[4] < 100 && explode("_",$neo_fisik_apgar)[4] > 0){$apgar4 = 1;}
            else if(explode("_",$neo_fisik_apgar)[4] <= 0){$apgar4 = 0;}
            $nilai_apgar =  explode("_",$neo_fisik_apgar)[0]+explode("_",$neo_fisik_apgar)[1]+
                            explode("_",$neo_fisik_apgar)[2]+explode("_",$neo_fisik_apgar)[3]+$apgar4;
        }

        if($nilai_apgar >= 7){
            // cek geo
            if($neo_fisik_geo != null && $neo_fisik_geo != "__" && explode("_",$neo_fisik_geo)[0] != 0 && explode("_",$neo_fisik_geo)[1] != 0 && explode("_",$neo_fisik_geo)[2] != 0 ){
                //cek kl
                if($neo_fisik_kl != null && $neo_fisik_kl != "____" && explode("_",$neo_fisik_kl)[0] == "y" && explode("_",$neo_fisik_kl)[1] == "y" && explode("_",$neo_fisik_kl)[2] == "y" && explode("_",$neo_fisik_kl)[3] == "y" && explode("_",$neo_fisik_kl)[4] == "y"){
                    //cek mulut
                    if($neo_fisik_mulut != null && $neo_fisik_mulut != "_" && explode("_",$neo_fisik_mulut)[0] == "y" && explode("_",$neo_fisik_mulut)[1] == "y"){
                        //cek pk
                        if($neo_fisik_pk != null && $neo_fisik_pk != "__" && explode("_",$neo_fisik_pk)[0] == "y" && explode("_",$neo_fisik_pk)[1] == "y" && explode("_",$neo_fisik_pk)[2] == "y"){
                            //cek tbtk
                            if($neo_fisik_tbtk != null && $neo_fisik_tbtk != "__" && explode("_",$neo_fisik_tbtk)[0] == "y" && explode("_",$neo_fisik_tbtk)[1] == "y" && explode("_",$neo_fisik_tbtk)[2] == "y"){
                                //cek tbtk
                                $capaian[15][$i] = 1;                        
                            }else{
                                $capaian[15][$i] = 0;
                            }
                        }else{
                            //gak perlu cek tbtk
                            $capaian[15][$i] = 0;
                        }
                    }else{
                        //gak perlu cek pk dll
                        $capaian[15][$i] = 0;
                    }
                }else{
                    //gak perlu cek mulut dll
                    $capaian[15][$i] = 0;
                }
            }else{
                //gak perlu cek kl dll
                $capaian[15][$i] = 0;
            }
        }else{
            //gak perlu cek geo dll
            $capaian[15][$i] = 0;
        }

        if($tenaga_kerja != null && $tenaga_kerja != "_" && explode("_",$tenaga_kerja)[1] != null)
        {$capaian[16][$i] = 1;}else{$capaian[16][$i] = 0;}
        if($lokasi != null && $lokasi != "_" && explode("_",$lokasi)[1] != null)
        {$capaian[17][$i] = 1;}else{$capaian[17][$i] = 0;}
        if($skr_shk_hsd != null){$capaian[18][$i] = 1;}else{$capaian[18][$i] = 0;}
        
        if($kie_nama != null){$capaian[19][$i] = 1;}else{$capaian[19][$i] = 0;}
        if($kie_informasi != null && $kie_informasi != "t_t_t_t_t" ){
            if(explode("_",$kie_informasi)[0] == "y"){$capaian[20][$i] = 1;}else{$capaian[20][$i] = 0;}
            if(explode("_",$kie_informasi)[1] == "y"){$capaian[21][$i] = 1;}else{$capaian[21][$i] = 0;}
            if(explode("_",$kie_informasi)[2] == "y"){$capaian[22][$i] = 1;}else{$capaian[22][$i] = 0;}
            if(explode("_",$kie_informasi)[3] == "y"){$capaian[23][$i] = 1;}else{$capaian[23][$i] = 0;}
            if(explode("_",$kie_informasi)[4] == "y"){$capaian[24][$i] = 1;}else{$capaian[24][$i] = 0;}            
        }else{
            $capaian[20][$i] = 0;$capaian[21][$i] = 0;$capaian[22][$i] = 0;
            $capaian[23][$i] = 0;$capaian[24][$i] = 0;

        }
        if($kie_media != null){$capaian[25][$i] = 1;}else{$capaian[25][$i] = 0;}
    
        return $capaian;
    }
}
