<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Requests;
use Illuminate\Support\Facades\Session;
use Redirect;

use App\DM;
use App\Warga;
use App\Pelayanan;

class DMController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }    

    public function index(){
        return view('spm.dm.index');
    }  
    public function data(){
        date_default_timezone_set('Asia/Jakarta');
        $total_penderita = 0;
        $total_sesuai_spm = 0;
        $total_tak_sesuai_spm = 0;
        $total_tahap_pelayanan = 0;
        $dm_all = DM::all();
        foreach($dm_all as $i => $dm){
            $data_nik[$i] = Warga::where('nik',$dm['nik'])->first();
            $usia[$i] = DMController::Usia($data_nik[$i]['tgl_lahir'],date("Y/m/d/h:i:s"));
            $total_penderita++;
            if($dm->status == "Tahap Pelayanan"){
                $total_tahap_pelayanan++;
            }
        }
        return view('spm.dm.data',compact('dm_all','data_nik','usia','total_penderita','total_sesuai_spm','total_tak_sesuai_spm','total_tahap_pelayanan'));
    }  
    public function tambah(){
        date_default_timezone_set('Asia/Jakarta');
        $warga_all = Warga::all();
        foreach($warga_all as $index =>$warga){
            $data = DM::where('nik',$warga['nik'])->where('status','tahap pelayanan')->first();
            if(!$data){
                $list_nik[$index] = $warga['nik'];
                $list_name[$index] = $warga['nama'];
                $list_ttl[$index] = $warga['tgl_lahir']; 
            }
        }
        return view('spm.dm.tambah',compact('list_nik','list_name','list_ttl'));
    }

    public function save(Request $request){
        $a = date('Y-m-d', strtotime($request->tanggal_menderita));
        $data = DM::create([
            'nik' => $request->nik,
            'tanggal_menderita' => $a,
            'status' => "Tahap Pelayanan"
        ]);
        if($data->save()){
            return Redirect::route('data-penderita-dm')->with('success','Berhasil Tambah Penderita Diabetes Melitus');
        }  
        else{
            return Redirect::route('tambah-penderita-dm')->with('danger','Gagal Tambah Penderita Diabetes Melitus');
        }
    }

    public function detail($id){
        date_default_timezone_set('Asia/Jakarta');
        $dm = DM::where('id',$id)->first();
        $data_nik = Warga::where('nik',$dm['nik'])->first();
        $pelayanan_all = Pelayanan::where('table_spm','diabetes_melitus')->where('id_spm',$dm['id'])->get();
        foreach($pelayanan_all as $i => $pel){
            $usiapel[$i] = DMController::Usia($data_nik['tgl_lahir'],date("Y/m/d/h:i:s"));

        }
        return view('spm.dm.detail',compact('dm','data_nik','usiapel','pelayanan_all'));  
    }

    public function ubah($id){
        $data = DM::where('id',$id)->first();
        $nik = $data->nik;
        $warga = Warga::where('nik', $data->nik)->first();
        $nama = $warga->nama;
        $status_kawin = $warga->status_perkawinan;
        $tgl_lahir = $warga->tgl_lahir;
        $gab = $nik."_".$nama."_".$tgl_lahir."_".$status_kawin;

        $kriteria = [];
        $nikca = [];
        $tglLahirca = [];
        $statusperkawinan = [];
        $warga_all = Warga::all();
        foreach($warga_all as $i => $val){
            $usia = DMController::Usia($val['tgl_lahir'],date("Y/m/d/h:i:s"));
            if($usia["years"] >= 15){
                $datacek = DM::where('nik',$val['nik'])->where('status','tahap pelayanan')->first();
                if(!$datacek){
                    $tglLahirca[$i] = $val['tgl_lahir'];
                    $statusperkawinan[$i] = $val['status_perkawinan'];
                    $kriteria[$i] = $val['nama'];
                    $nikca[$i] = $val['nik'];
                    $gabungan[$i] = $nikca[$i]."_".$kriteria[$i]."_".$tglLahirca[$i]."_".$statusperkawinan[$i];
                }
            }
        }
        return view('spm.dm.ubah', compact('gab','gabungan','tgl_lahir','status_kawin','tglLahirca','statusperkawinan','data','nik','nama','totalcapaian','kriteria','nikca'));
    }

    public function edit(Request $request){
        if($request->status == "Tahap Pelayanan"){
            $request->status= $request->status;
        }
        else{
            if($request->totalcapaian == 100){
                $request->status= "Sesuai SPM";
            }
            else{
                $request->status= "Tidak Sesuai SPM";
            }
        }

        $dm = DM::where('id',$request->id)->first();
        $dm['nik'] = $request->nik;
        $dm['status'] = $request->status;
        $dm['tanggal_menderita'] = $request->tanggal_menderita;

        if($dm->save()){
            return Redirect::route('data-penderita-dm')->with('success','Berhasil Ubah Penderita Diabetes Melitus');
        }
        else{
            return Redirect::route('ubah-penderita-dm',$dm->id)->with('danger','Gagal Ubah Penderita Diabetes Melitus');
        }
    }

    public function delete($id){
        $dm=DM::where('id',$id)->first();
        $pelayanan_all = Pelayanan::all();
        foreach($pelayanan_all as $pel){
            if($pel['table_spm'] == "diabetes_melitus" && $pel['id_spm'] == $dm['id']){
                $deletepel = $pel->delete();
            }
        }
        $delete = $dm->delete();
        
        if($delete){
            return Redirect::route('data-penderita-dm')->with('success','Berhasil Delete Penderita Diabtes Melitus');
        }  
        else{
            return Redirect::route('data-penderita-dm')->with('danger','Gagal Delete Penderita DIabetes Melitus');
        }
    }

    public function inputnikPelayanan(){
        if(!empty(Session::get('withnik8'))){
            Session::forget('withnik8');
            return view('spm.dm.pelayanan.masukan_nik');  
        }
        else{
            return view('spm.dm.pelayanan.masukan_nik');  
        }
    }

    public function inputPelayanan(Request $request){
        Session::put('withnik',$request->nik);
        $nik = Session::get('withnik');
        $datanik = Warga::where('nik', $nik)->first();
        if(count($datanik) > 0 ){
            $data = DM::where('nik', $nik)->where('status','Tahap Pelayanan')->first();
            if(count($data) > 0){
                return view('spm.dm.pelayanan.tambah');
            }else{
                return view('spm.dm.pelayanan.masukan_nik')->with('alert','NIK belum terdaftar sebagai Penderita Hipertensi');
            }

        }else{
            return view('spm.dm.pelayanan.masukan_nik')->with('alert','NIK tidak terdaftar');
        }
    }

    public function Usia($tgl1, $tgl2){
        $tgl1 = (is_string($tgl1) ? strtotime($tgl1) : $tgl1);
        $tgl2 = (is_string($tgl2) ? strtotime($tgl2) : $tgl2);
        $diff_secs = abs($tgl1-$tgl2);
        $base_year = min(date("Y",$tgl1), date("Y",$tgl2));
        $diff = mktime(0,0,$diff_secs,1,1,$base_year);
        return array(
            "years"=>date("Y",$diff) - $base_year, 
            "months_total" => (date("Y",$diff) - $base_year) * 12 + date("n",$diff) - 1, 
            "months" => date("n",$diff) -1, 
            "days_total" => floor($diff_secs/ (3600 * 24)), 
            "days" => date("j",$diff) - 1, 
            "hours_total" => floor($diff_secs/3600), 
            "hours" => date("G", $diff), 
            "minutes_total" => floor($diff_secs/60), 
            "minutes" => (int)date("i",$diff),
            "seconds_total" => $diff_secs, 
            "seconds" => (int) date("s",$diff)
        );
    }
}
