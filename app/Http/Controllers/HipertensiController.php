<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Requests;
use Illuminate\Support\Facades\Session;
use Redirect;

use App\Hipertensi;
use App\Warga;
use App\Pelayanan;

class HipertensiController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('spm.hipertensi.index');
    }  

    public function data(){
        date_default_timezone_set('Asia/Jakarta');
        $total_penderita = 0;
        $total_sesuai_spm = 0;
        $total_tak_sesuai_spm = 0;
        $total_tahap_pelayanan = 0;
        $hipertensi_all = Hipertensi::all();
        foreach($hipertensi_all as $i => $hip){
            $data_nik[$i] = Warga::where('nik',$hip['nik'])->first();
            $usia_hip[$i] = HipertensiController::Usia($data_nik[$i]['tgl_lahir'],date("Y/m/d/h:i:s"));
            $total_penderita++;
            if($hip['status'] == "Tahap Pelayanan"){
                $total_tahap_pelayanan++;
            }else{
                if($hip['status'] == "Sesuai SPM"){
                    $total_sesuai_spm++;
                }else if($hip['status'] == "Tidak Sesuai SPM"){
                    $total_tak_sesuai_spm++;
                }
            }
        }
        return view('spm.hipertensi.data',compact('hipertensi_all','data_nik','usia_hip','total_penderita','total_sesuai_spm','total_tak_sesuai_spm','total_tahap_pelayanan'));
    }  

    public function tambah(){
        date_default_timezone_set('Asia/Jakarta');
        $warga_all = Warga::all();
        foreach($warga_all as $index =>$warga){
            $usia = HipertensiController::Usia($warga['tgl_lahir'],date("Y/m/d/h:i:s"));
            if($usia["years"] >= 15){
                $data = Hipertensi::where('nik',$warga['nik'])->where('status','tahap pelayanan')->first();
                if(!$data){
                    $list_nik[$index] = $warga['nik'];
                    $list_name[$index] = $warga['nama'];
                    $list_ttl[$index] = $warga['tgl_lahir']; 
                }
            }
        }
        return view('spm.hipertensi.tambah',compact('list_nik','list_name','list_ttl'));
    }

    public function save(Request $request){
        $a = date('Y-m-d', strtotime($request->tanggal_menderita));
        $data = Hipertensi::create([
            'nik' => $request->nik,
            'tanggal_menderita' => $a,
            'status' => "Tahap Pelayanan"
        ]);
        if($data->save()){
            return Redirect::route('data-penderita-hipertensi')->with('success','Berhasil Tambah Penderita Hipertensi');
        }  
        else{
            return Redirect::route('tambah-penderita-hipertensi')->with('danger','Gagal Tambah Penderita Hipertensi');
        }
    }

    public function detail($id){
        date_default_timezone_set('Asia/Jakarta');
        $hipertensi = Hipertensi::where('id',$id)->first();
        $data_nik = Warga::where('nik',$hipertensi['nik'])->first();
        $pelayanan_all = Pelayanan::where('table_spm','hipertensi')->where('id_spm',$hipertensi['id'])->get();
        foreach($pelayanan_all as $i => $pel){
            $usiapel[$i] = HipertensiController::Usia($data_nik['tgl_lahir'],date("Y/m/d/h:i:s"));

        }

        return view('spm.hipertensi.detail',compact('hipertensi','data_nik','usiapel','pelayanan_all'));

    }

    public function ubah($id){
        $data = Hipertensi::where('id',$id)->first();
        $nik = $data->nik;
        $warga = Warga::where('nik', $data->nik)->first();
        $nama = $warga->nama;
        $status_kawin = $warga->status_perkawinan;
        $tgl_lahir = $warga->tgl_lahir;
        $gab = $nik."_".$nama."_".$tgl_lahir."_".$status_kawin;

        $kriteria = [];
        $nikca = [];
        $tglLahirca = [];
        $statusperkawinan = [];
        $warga_all = Warga::all();
        foreach($warga_all as $i => $val){
            $usia = HipertensiController::Usia($val['tgl_lahir'],date("Y/m/d/h:i:s"));
            if($usia["years"] >= 15){
                $datacek = Hipertensi::where('nik',$val['nik'])->where('status','tahap pelayanan')->first();
                if(!$datacek){
                    $tglLahirca[$i] = $val['tgl_lahir'];
                    $statusperkawinan[$i] = $val['status_perkawinan'];
                    $kriteria[$i] = $val['nama'];
                    $nikca[$i] = $val['nik'];
                    $gabungan[$i] = $nikca[$i]."_".$kriteria[$i]."_".$tglLahirca[$i]."_".$statusperkawinan[$i];
                }
            }
        }
        return view('spm.hipertensi.ubah', compact('gab','gabungan','tgl_lahir','status_kawin','tglLahirca','statusperkawinan','data','nik','nama','totalcapaian','kriteria','nikca'));
    }

    public function edit(Request $request){
        if($request->status == "Tahap Pelayanan"){
            $request->status= $request->status;
        }
        else{
            if($request->totalcapaian == 100){
                $request->status= "Sesuai SPM";
            }
            else{
                $request->status= "Tidak Sesuai SPM";
            }
        }

        $hipertensi = Hipertensi::where('id',$request->id)->first();
        $hipertensi['nik'] = $request->nik;
        $hipertensi['status'] = $request->status;
        $hipertensi['tanggal_menderita'] = $request->tanggal_menderita;

        if($hipertensi->save()){
            return Redirect::route('data-penderita-hipertensi')->with('success','Berhasil Ubah Penderita Hipertensi');
        }
        else{
            return Redirect::route('ubah-penderita-hipertensi',$hipertensi->id)->with('danger','Gagal Ubah Penderita Hipertensi');
        }
    }

    public function delete($id){
        $hipertensi=Hipertensi::where('id',$id)->first();
        $pelayanan_all = Pelayanan::all();
        foreach($pelayanan_all as $pel){
            if($pel['table_spm'] == "hipertensi" && $pel['id_spm'] == $hipertensi['id']){
                $deletepel = $pel->delete();
            }
        }
        $delete = $hipertensi->delete();
        
        if($delete){
            return Redirect::route('data-penderita-hipertensi')->with('success','Berhasil Delete Penderita Hipertensi');
        }  
        else{
            return Redirect::route('data-penderita-hipertensi')->with('danger','Gagal Delete Penderita Hipertensi');
        }
    }

    public function inputnikPelayanan(){
        if(!empty(Session::get('withnik7'))){
            Session::forget('withnik7');
            return view('spm.hipertensi.pelayanan.masukan_nik');  
        }
        else{
            return view('spm.hipertensi.pelayanan.masukan_nik');  
        }
    }

    public function inputPelayanan(Request $request){
        Session::put('withnik',$request->nik);
        $nik = Session::get('withnik');
        $datanik = Warga::where('nik', $nik)->first();
        if(count($datanik) > 0 ){
            $data = Hipertensi::where('nik', $nik)->where('status','Tahap Pelayanan')->first();
            if(count($data) > 0){
                return view('spm.hipertensi.pelayanan.tambah');
            }else{
                return view('spm.hipertensi.pelayanan.masukan_nik')->with('alert','NIK belum terdaftar sebagai Penderita Hipertensi');
            }

        }else{
            return view('spm.hipertensi.pelayanan.masukan_nik')->with('alert','NIK tidak terdaftar');
        }
    }

    public function Usia($tgl1, $tgl2){
        $tgl1 = (is_string($tgl1) ? strtotime($tgl1) : $tgl1);
        $tgl2 = (is_string($tgl2) ? strtotime($tgl2) : $tgl2);
        $diff_secs = abs($tgl1-$tgl2);
        $base_year = min(date("Y",$tgl1), date("Y",$tgl2));
        $diff = mktime(0,0,$diff_secs,1,1,$base_year);
        return array(
            "years"=>date("Y",$diff) - $base_year, 
            "months_total" => (date("Y",$diff) - $base_year) * 12 + date("n",$diff) - 1, 
            "months" => date("n",$diff) -1, 
            "days_total" => floor($diff_secs/ (3600 * 24)), 
            "days" => date("j",$diff) - 1, 
            "hours_total" => floor($diff_secs/3600), 
            "hours" => date("G", $diff), 
            "minutes_total" => floor($diff_secs/60), 
            "minutes" => (int)date("i",$diff),
            "seconds_total" => $diff_secs, 
            "seconds" => (int) date("s",$diff)
        );
    }

}
