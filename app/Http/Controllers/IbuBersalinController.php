<?php

namespace App\Http\Controllers;
use DB;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Redirect;

use App\IbuHamil;
use App\IbuBersalin;
use App\Warga;
use App\BayiBaruLahir;
use App\Pelayanan;

class IbuBersalinController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        return view('spm.ibubersalin.index');
    }
    
    public function data(){
        $total_ibu_bersalin =0;
        $total_dpt_layanan =0;
        $total_blm_dpt_layanan =0;
        $nilai = [];
        $status_layanan = [];
        $ibubersalin = IbuBersalin::all();
        $total_belum_bersalin = Ibuhamil::where('status_kehamilan',"Tahap Pelayanan")->count(); 
        foreach($ibubersalin as $index => $value){
            $nilai[$index] =0 ; 
            $total_ibu_bersalin++;
            $warga[$index] = Warga::where('nik', $value->nik)->first();
            $nama_ibu_bersalin[$index] = $warga[$index]['nama']; 
            $nik_ibu_bersalin[$index] = $warga[$index]['nik']; 
            
            if($value['fasilitas_kesehatan'] != "Lainnya"){
                $nilai[$index] = $nilai[$index] + 50;
            }
            if($value['tenaga_kesehatan'] != "Lainnya"){
                $nilai[$index] = $nilai[$index] + 50;
            }

            if($nilai[$index] == 100){
                $total_dpt_layanan++;
                $status_layanan[$index] = "Mendapat Layanan";
            }else{
                $total_blm_dpt_layanan++;
                $status_layanan[$index] = "Belum Mendapat Layanan";
            }

        }
        return view('spm.ibubersalin.data',compact('status_layanan','ibubersalin','total_ibu_bersalin','total_dpt_layanan','total_blm_dpt_layanan','total_belum_bersalin','nama_ibu_bersalin','nilai','nik_ibu_bersalin'));
    }

    public function tambah(){
        $ibuhamil = Ibuhamil::where('status_kehamilan',"Tahap Pelayanan")->get();
        foreach($ibuhamil as $i => $val){
            $warga[$i] = Warga::where('nik', $val['nik'])->first();
            $list_name[$i] = $warga[$i]['nama'];
            $list_nik[$i] = $warga[$i]['nik'];
            $list_id[$i] = $val['id'];
        }
        return view('spm.ibubersalin.tambah', compact('list_name','list_nik','list_id'));
    }

    public function getHpht(Request $request){
        $ibuhamil = IbuHamil::where('id',$request->id)->first();
        $tgl_hppt = $ibuhamil->tanggal_hamil;
        return response()->json(['message' =>'success','data' => $tgl_hppt]);
    }

    public function getUsiaKehamilan(Request $request){
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d', strtotime($request->tgl));
        if($tgl == "1970-01-01"){
            $tgl = "0000-00-00";
        }
        if($request->wkt == null){
            $tgl = $tgl." "."00:00:00";
        }
        else{
            $tgl = $tgl." ".$request->wkt.":00";
        }
        $hpht = "0000-00-00";
        if($request->id != null){
            $ibuhamil = IbuHamil::where('id',$request->id)->first();
            $hpht = $ibuhamil->tanggal_hamil;
        }
        $data = IbuBersalinController::Usia(date('Y-m-d h:i:s',strtotime($tgl)), date('Y-m-d h:i:s',strtotime($hpht)));
        return response()->json(['message' =>'success','data' => $data,'hpht' => $hpht]);
    }
    
    public function save(Request $request){
        $this->validate($request,[
            'image1' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            'image2' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);
        if($request->file('image1') != null){
            $image1 = $request->file('image1');
            $new_name1 = rand().".".$image1->getClientOriginalExtension();
            $image1->move(public_path("images_partograf"),$new_name1);
        }else{$new_name1 = '';}
        if($request->file('image2') != null){
            $image2 = $request->file('image2');
            $new_name2 = rand().".".$image2->getClientOriginalExtension();
            $image2->move(public_path("images_partograf"),$new_name2);    
        }else{$new_name2 = '';}
        
        $request->tanggal_bersalin = date('Y-m-d', strtotime($request->tanggal_bersalin));
        $request->tanggal_bersalin = $request->tanggal_bersalin." ".$request->waktu_bersalin.":00";
        $total_capaian_ibuhamil = IbuBersalinController::TotalCapaianIbuHamil($request->id);
        $ibuhamil = IbuHamil::where('id',$request->id)->first();
        $warga = Warga::where('nik',$ibuhamil['nik'])->first();
        if($total_capaian_ibuhamil == 100){
            $ibuhamil->status_kehamilan= "Sesuai SPM";
        }
        else{
            $ibuhamil->status_kehamilan= "Tidak Sesuai SPM";
        }
        $ibuhamil->tanggal_bersalin = $request->tanggal_bersalin;
        $ibuhamil->updated_by = $request->created_by;
        
        $new_ibu_bersalin = New IbuBersalin();
        $new_ibu_bersalin['nik'] = $ibuhamil['nik'];
        $new_ibu_bersalin['tanggal_hamil'] = $ibuhamil['tanggal_hamil'];
        $new_ibu_bersalin['tanggal_bersalin'] = $request->tanggal_bersalin; 
        $new_ibu_bersalin['fasilitas_kesehatan'] = $request->fas;
        $new_ibu_bersalin['lokasi_fasilitas'] = $request->lokasi_pelayanan;
        $new_ibu_bersalin['tenaga_kesehatan'] = $request->tenaga_kesehatan;
        $new_ibu_bersalin['tenkes'] = $request->tenkes;
        $new_ibu_bersalin['nama_tenaga'] = $request->nama_stk;
        $new_ibu_bersalin['nama_tenkes'] = $request->nama_tenkes;
        $new_ibu_bersalin['tindakan'] = $request->tindakan;
        $new_ibu_bersalin['link_gambar_partograf1'] = $new_name1;
        $new_ibu_bersalin['link_gambar_partograf2'] = $new_name2;
        $new_ibu_bersalin['created_by'] = $request->created_by;
        $new_ibu_bersalin['status_persalinan'] = $request->st_persalinan;
        $new_ibu_bersalin['hasil_persalinan'] = $request->hasil_persalinan;

        if($request->hasil_persalinan != "Meninggal" && $request->hasil_persalinan != NULL ){
            $new_bayi = New BayiBaruLahir();
            $new_bayi['nik_ibu']= $ibuhamil['nik'];
            $new_bayi['nama']= "Bayi Ny.".$warga['nama'];
            $new_bayi['tgl_lahir']= $request->tanggal_bersalin;
            $new_bayi['status_kelahiran']= "Tahap Pelayanan";
            $new_bayi['created_by']= $request->created_by;
        }
        
        if($ibuhamil->save() && $new_ibu_bersalin->save() && $new_bayi->save()){
            return Redirect::route('data-ibu-bersalin')->with('success','Berhasil Tambah Ibu Bersalin');
        }
        else{
            return Redirect::route('tambah-ibu-bersalin')->with('danger','Gagal Tambah Ibu Bersalin');
        }
    }

    public function edit(Request $request){
        // $ibu_hamil = IbuHamil::where('id',$request->id_ibu_hamil)->first();
        // $ibu_bersalin = IbuBersalin::where('id',$request->id_ibubersalin)->first();

        // $temp_nik_bumil = $ibu_hamil->nik;
        // $temp_nik_bulin = $ibu_bersalin->nik;

        // $ibu_hamil->nik = $temp_nik_bulin;
        // $ibu_bersalin->nik = $temp_nik_bumil;

        $ibu_bersalin = IbuBersalin::where('id',$request->id_ibubersalin)->first();
        $ibu_bersalin['tanggal_bersalin'] = $request->tanggal_bersalin;
        $ibu_bersalin['fasilitas_kesehatan'] = $request->fas;
        $ibu_bersalin['lokasi_fasilitas'] = $request->lokasi_fas;
        $ibu_bersalin['tenaga_kesehatan'] = $request->tenkes;
        $ibu_bersalin['nama_tenaga'] = $request->nama_tenkes;
        $ibu_bersalin['status_persalinan'] = $request->st_persalinan;
        $ibu_bersalin['hasil_persalinan'] = $request->hs_persalinan;

        if($ibu_bersalin->save() ){
            return redirect('/data-ibu-bersalin')->with('alert', 'Berhasil Ubah Ibu Bersalin');        
        }  
        else{

        }
    }

    public function delete($id){
        $ibubersalin=IbuBersalin::where('id',$id)->first();
        $delete = $ibubersalin->delete();
        
        if($delete){
            return Redirect::route('data-ibu-bersalin')->with('success','Berhasil Hapus Ibu Bersalin');
        }  
        else{
            return Redirect::route('data-ibu-bersalin')->with('danger','Gagal Hapus Ibu Bersalin');
        }
    }

    public function detail($id){
        $ibubersalin = IbuBersalin::where('id',$id)->first();
        $ibu_bersalin = Warga::where('nik',$ibubersalin->nik)->first();
        $nama_ibu_bersalin = $ibu_bersalin['nama'];
        return view('spm.ibubersalin.detail',compact('ibubersalin','nama_ibu_bersalin'));
    }

    public function ubah($id){
        $ibubersalin = IbuBersalin::where('id',$id)->first();
        $data_nik_ibubersalin = Warga::where('nik',$ibubersalin->nik)->first();

        $ibu_hamil = IbuHamil::where('status_kehamilan','!=',"Tahap Pelayanan")->where('nik',$ibubersalin->nik)->first();
        $id_ibuhamil = $ibu_hamil['id'];

        $ibuhamil = IbuHamil::where('status_kehamilan',"Tahap Pelayanan")->get();
        foreach($ibuhamil as $i => $val){
            $warga[$i] = Warga::where('nik', $val['nik'])->first();
            $list_name[$i] = $warga[$i]['nama'];
            $list_nik[$i] = $warga[$i]['nik'];
            $list_id[$i] = $val['id'];
        }

        return view('spm.ibubersalin.ubah',compact('ibubersalin','data_nik_ibubersalin','list_name','list_nik','list_id','id_ibuhamil'));
    }

    public function Cek_Capaian($berat_tinggi,$tekanan_darah, $lila,$tinggi_puncak_rahim, $presentasi_djj,$imunisasi_tetanus_tt, $tablet_tambah_darah, $tes_laboratorium, $tatalaksana,$temu_wicara, $i){
        // Berat Tinggi
        if($berat_tinggi != null){
            $bt = explode('_',$berat_tinggi);
            if($bt[0] >1 && $bt[1] > 1){
                $capaian[0][$i] = "x";
            }
            elseif($bt[0] >1){
                $capaian[0][$i] = "y";
            }            
            elseif($bt[1] >1){
                $capaian[0][$i] = "z";
            }
            else{
                $capaian[0][$i] = 0;
            }            
        }
        else{
            $capaian[0][$i] = 0;
        }

        //Tekanan Darah
        if($tekanan_darah != null){
            $td = explode('/',$tekanan_darah);
            if($td[0] >1 && $td[1] > 1){
                $capaian[1][$i] = "x";
            }
            else{
                $capaian[1][$i] = 0;
            }            
        }
        else{
            $capaian[1][$i] = 0;
        }

        //Lila
        if($lila != null){
            $capaian[2][$i] = "x";            
        }
        else{
            $capaian[2][$i] = 0;
        }

        //Tinggi Puncak Rahim
        if($tinggi_puncak_rahim != null){
            $capaian[3][$i] = "x";            
        }
        else{
            $capaian[3][$i] = 0;
        }

        //Presentasi DJJ
        if($presentasi_djj != null){
            $pdjj = explode('_',$presentasi_djj);
            if($pdjj[0] != null && $pdjj[1] >1){
                $capaian[4][$i] = "x";            
            }
            elseif($pdjj[0] != null){
                $capaian[4][$i] = "y";
            }
            elseif($pdjj[1] > 1){
                $capaian[4][$i] = "z";
            }
            else{
                $capaian[4][$i] = 0;
            }
        }
        else{
            $capaian[4][$i] = 0;
        }

        //Imunisasi Tetanus TT
        if($imunisasi_tetanus_tt != null){
            $itt = explode('_',$imunisasi_tetanus_tt);
            if($itt[0] == "y" && $itt[1] == "y"){
                $capaian[5][$i] = "x";            
            }
            elseif($itt[0] == "y"){
                $capaian[5][$i] = "y";
            }
            elseif($itt[1] == "y"){
                $capaian[5][$i] = "z";
            }
            else{
                $capaian[5][$i] = 0;
            }
        }
        else{
            $capaian[5][$i] = 0;
        }
        
        //Tablet Tambah Darah
        if($tablet_tambah_darah != null){
            $capaian[6][$i] = $tablet_tambah_darah;            
        }
        else{
            $capaian[6][$i] = 0;
        }

        //Tes Laboratorium
        if($tes_laboratorium != null){
            $tl = explode('_',$tes_laboratorium);
            if($tl[0] == "y" && $tl[1] == "y" && $tl[2] == "y" && $tl[3] == "y"){
                $capaian[7][$i] = "x";            
            }
            // else if($tl[0] == "y" && $tl[1] != "y" && $tl[2] != "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "a";            
            // }
            // else if($tl[0] != "y" && $tl[1] == "y" && $tl[2] != "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "b";            
            // }
            // else if($tl[0] != "y" && $tl[1] != "y" && $tl[2] == "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "c";            
            // }
            // else if($tl[0] != "y" && $tl[1] != "y" && $tl[2] != "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "d";            
            // }
            // else if($tl[0] == "y" && $tl[1] == "y" && $tl[2] != "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "e";            
            // }
            // else if($tl[0] == "y" && $tl[1] != "y" && $tl[2] == "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "f";            
            // }
            // else if($tl[0] == "y" && $tl[1] != "y" && $tl[2] != "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "g";            
            // }
            // else if($tl[0] != "y" && $tl[1] == "y" && $tl[2] == "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "h";            
            // }
            // else if($tl[0] != "y" && $tl[1] == "y" && $tl[2] != "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "i";            
            // }
            // else if($tl[0] != "y" && $tl[1] != "y" && $tl[2] == "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "j";            
            // }
            // else if($tl[0] == "y" && $tl[1] == "y" && $tl[2] == "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "k";            
            // }
            // else if($tl[0] == "y" && $tl[1] == "y" && $tl[2] != "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "l";            
            // }
            // else if($tl[0] == "y" && $tl[1] != "y" && $tl[2] == "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "m";            
            // }
            // else if($tl[0] != "y" && $tl[1] == "y" && $tl[2] == "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "n";            
            // }
            else{
                $capaian[7][$i] = 0;
            }
        }
        else{
            $capaian[7][$i] = 0;
        }

        //Tatalaksana
        if($tatalaksana == "y"){
            $capaian[8][$i] = "x";            
        }
        else{
            $capaian[8][$i] = 0;
        }

        //Temu Wicara
        if($temu_wicara == "y"){
            $capaian[9][$i] = "x";            
        }
        else{
            $capaian[9][$i] = 0;
        }

        return $capaian;
    }

    public function Umur($lahir){
        $pisah_lahir = explode('-',$lahir);
        $thn_lahir = intval($pisah_lahir[0]);
        $bln_lahir = intval($pisah_lahir[1]);
        $tgl_lahir = intval($pisah_lahir[2]);
        $tanggal = intval(date('d'));
        $bulan = intval(date('m'));
        $tahun = intval(date('Y'));
        $usia = $tahun - $thn_lahir;
        if($bulan < $bln_lahir || ($bulan == $bln_lahir && $tanggal < $tgl_lahir)){
            $usia -= 1;
        }

        return $usia;
    }

    public function Usia($tgl1, $tgl2)
    {
        $tgl1 = (is_string($tgl1) ? strtotime($tgl1) : $tgl1);
        $tgl2 = (is_string($tgl2) ? strtotime($tgl2) : $tgl2);
        $diff_secs = abs($tgl1-$tgl2);
        $base_year = min(date("Y",$tgl1), date("Y",$tgl2));
        $diff = mktime(0,0,$diff_secs,1,1,$base_year);
        return array("years"=>date("Y",$diff) - $base_year, "months_total" => (date("Y",$diff) - $base_year) * 12 + date("n",$diff) - 1, 
        "months" => date("n",$diff) -1, "days_total" => floor($diff_secs/ (3600 * 24)), "days" => date("j",$diff) - 1, 
        "hours_total" => floor($diff_secs/3600), "hours" => date("G", $diff), "minutes_total" => floor($diff_secs/60), "minutes" => (int)date("i",$diff),
        "seconds_total" => $diff_secs, "seconds" => (int) date("s",$diff));


        // $pisah_lahir = explode('-',$lahir);
        // $thn_lahir = intval($pisah_lahir[0]);
        // $bln_lahir = intval($pisah_lahir[1]);
        // $tgl_lahir = intval($pisah_lahir[2]);
        // $tanggal = intval(date('d'));
        // $bulan = intval(date('m'));
        // $tahun = intval(date('Y'));
        // $usia = $tahun - $thn_lahir;
        // if($bulan < $bln_lahir || ($bulan == $bln_lahir && $tanggal < $tgl_lahir)){
        //     $usia -= 1;
        // }

        // return $usia;
    }

    public function TotalCapaianIbuHamil($id){
        $ibuhamil = IbuHamil::where('id',$id)->first();
        $pelayanan = Pelayanan::where('table_spm', 'ibu_hamil')->where('id_spm',$ibuhamil->id)->get();
        $hayo3bulanlagi = date('Y-m-d', strtotime('+3 month', strtotime($ibuhamil->tanggal_hamil)));
        $hayo6bulanlagi = date('Y-m-d', strtotime('+6 month', strtotime($ibuhamil->tanggal_hamil)));
        $hayo9bulanlagi = date('Y-m-d', strtotime('+9 month', strtotime($ibuhamil->tanggal_hamil)));
        
        $capaian_berat_periode_1 =0;$capaian_berat_periode_2 =0;$capaian_berat_periode_3 =0;
        $capaian_tinggi_periode_1 =0;$capaian_tinggi_periode_2 =0;$capaian_tinggi_periode_3 =0;
        $capaian_tekanan_darah_periode_1 =0;$capaian_tekanan_darah_periode_2 =0;$capaian_tekanan_darah_periode_3 =0;
        $capaian_lila_periode_1 =0;$capaian_lila_periode_2 =0;$capaian_lila_periode_3 =0;
        $capaian_tinggi_puncak_rahim_periode_1 =0;$capaian_tinggi_puncak_rahim_periode_2 =0;$capaian_tinggi_puncak_rahim_periode_3 =0;
        $capaian_presensi_periode_1 =0;$capaian_presensi_periode_2 =0;$capaian_presensi_periode_3 =0;
        $capaian_djj_periode_1 =0;$capaian_djj_periode_2 =0;$capaian_djj_periode_3 =0;
        $capaian_skrining_periode_1 =0;$capaian_skrining_periode_2 =0;$capaian_skrining_periode_3 =0;
        $capaian_imun_tt_periode_1 =0;$capaian_imun_tt_periode_2 =0;$capaian_imun_tt_periode_3 =0;
        $capaian_tablet =0;
        $capaian_tes_kehamilan_periode_1 = 0;$capaian_tes_kehamilan_periode_2 =0;$capaian_tes_kehamilan_periode_3 =0;
        $capaian_tes_hb_periode_1 = 0;$capaian_tes_hb_periode_2= 0;$capaian_tes_hb_periode_3 = 0;
        $capaian_tes_gol_dar_periode_1 = 0;$capaian_tes_gol_dar_periode_2 = 0;$capaian_tes_gol_dar_periode_3 = 0;
        $capaian_tes_urin_periode_1 = 0;$capaian_tes_urin_periode_2 = 0;$capaian_tes_urin_periode_3 = 0;        
        $capaian_tatalaksana_periode_1 =0;$capaian_tatalaksana_periode_2 =0;$capaian_tatalaksana_periode_3 =0;
        $capaian_temu_periode_1 =0;$capaian_temu_periode_2 =0;$capaian_temu_periode_3 =0;        
            // Inisialisai Nilai Kriteria per Pelayanan dan Hitung Pencapaian Pelayanan per Pelayanan
            $pel_by_id = [];
            $pel_by_id2 = [];
            $pel_by_id3 = [];
            foreach($pelayanan as $i => $pel){
                $capaian_berat[$i] = 0;$capaian_tinggi[$i] = 0;
                $capaian_tekanan_darah[$i] = 0;
                $capaian_lila[$i] = 0;
                $capaian_tinggi_puncak_rahim[$i] = 0;
                $capaian_presensi[$i] = 0;
                $capaian_djj[$i] = 0;
                $capaian_skrining[$i] = 0;
                $capaian_imun_tt[$i] = 0;
                $capaian_tes_kehamilan[$i] = 0;
                $capaian_tes_hb[$i] = 0;
                $capaian_tes_gol_dar[$i] = 0;
                $capaian_tes_urin[$i] = 0;
                $capaian_tatalaksana[$i] = 0;
                $capaian_temu[$i] = 0;

                //Berat Tinggi
                if($pel['berat_tinggi'] != null){
                    $bt = explode('_',$pel['berat_tinggi']);
                    if($bt[0] > 45 && $bt[1] > 100){
                        $capaian_berat[$i] = 0.5;
                        $capaian_tinggi[$i] = 0.5;
                    }else{
                    if($bt[0] >45){
                        $capaian_berat[$i] = 0.5;
                    }            
                    if($bt[1] >100){
                        $capaian_tinggi[$i] = 0.5;
                    }}         
                }
                
                // Tekanan Darah
                if($pel['tekanan_darah'] != null){
                    $td = explode('/',$pel['tekanan_darah']);
                    if($td[0] > 90 && $td[1] > 60){
                        $capaian_tekanan_darah[$i] = 1;
                    }            
                }
        
                //Lila
                if($pel['lila'] != null && $pel['lila'] > 2.35 ){
                    $capaian_lila[$i] = 1;            
                }
        
                //Tinggi Puncak Rahim
                if($pel['tinggi_puncak_rahim'] != null){
                    $capaian_tinggi_puncak_rahim[$i] = 1;            
                }
        
                //Presentasi DJJ
                if($pel['presentasi_djj']!= null){
                    $pdjj = explode('_',$pel['presentasi_djj']);
                    if($pdjj[0] != null && $pdjj[1] >120){
                        $capaian_presensi[$i] = 0.5;            
                        $capaian_djj[$i] = 0.5;            
                    }else{
                    if($pdjj[0] != null){
                        $capaian_presensi[$i] = 0.5;
                    }
                    if($pdjj[1] > 120){
                        $capaian_djj[$i] = 0.5;
                    }}
                }
        
                //Imunisasi Tetanus TT
                if($pel['imunisasi_tetanus_tt'] != null){
                    $itt = explode('_',$pel['imunisasi_tetanus_tt']);
                    if($itt[0] == "y" && $itt[1] == "y"){
                        $capaian_skrining[$i] = 0.5;            
                        $capaian_imun_tt[$i] = 0.5;            
                    }else{
                    if($itt[0] == "y"){
                        $capaian_skrining[$i] = 0.5;
                    }
                    if($itt[1] == "y"){
                        $capaian_imun_tt[$i] = 0.5;
                    }}
                }
                
                //Tablet Tambah Darah
                if($pel['tablet_tambah_darah'] != null){
                    $capaian_tablet = $pel['tablet_tambah_darah'];            
                }
        
                //Tes Laboratorium
                if($pel['tes_laboratorium'] != null){
                    $tl = explode('_',$pel['tes_laboratorium']);
                    
                    if($tl[0] == "y"){
                        $capaian_tes_kehamilan[$i] = 0.25;            
                    }
                    if($tl[1] == "y"){
                        $capaian_tes_hb[$i] = 0.25;            
                    }
                    if($tl[2] == "y"){
                        $capaian_tes_gol_dar[$i] = 0.25;            
                    }
                    if($tl[3] == "y"){
                        $capaian_tes_urin[$i] = 0.25;            
                    }
                }
        
                //Tatalaksana
                if($pel['tatalaksana'] == "y"){
                    $capaian_tatalaksana[$i] = 1;            
                }
        
                //Temu Wicara
                if($pel['temu_wicara'] == "y"){
                    $capaian_temu[$i] = 1;            
                }

                if($capaian_tablet >=90){
                    $capaian_tablet_all_periode = 1;
                }
                else{
                    $capaian_tablet_all_periode = 0;
                }
                //Perhitungan per Pelayanan
                if($pel['tanggal_pelayanan'] <= $hayo3bulanlagi && $pel['tanggal_pelayanan'] > $ibuhamil->tanggal_hamil || $pel['tanggal_pelayanan'] == $ibuhamil->tanggal_hamil ){
                    $pel_by_id[$pel['id']] = $capaian_berat[$i]+$capaian_tinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                    $capaian_tinggi_puncak_rahim[$i]+$capaian_presensi[$i]+$capaian_djj[$i]+$capaian_skrining[$i]+$capaian_imun_tt[$i]+
                    $capaian_tes_kehamilan[$i]+$capaian_tes_hb[$i]+$capaian_tes_gol_dar[$i]+$capaian_tes_urin[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet_all_periode;
                    // $pel_by_id2[$pel['id']] = 0;
                    // $pel_by_id3[$pel['id']] = 0;
                }else if($pel['tanggal_pelayanan'] <= $hayo6bulanlagi && $pel['tanggal_pelayanan'] > $hayo3bulanlagi){
                    $pel_by_id2[$pel['id']] = $capaian_berat[$i]+$capaian_tinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                    $capaian_tinggi_puncak_rahim[$i]+$capaian_presensi[$i]+$capaian_djj[$i]+$capaian_skrining[$i]+$capaian_imun_tt[$i]+
                    $capaian_tes_kehamilan[$i]+$capaian_tes_hb[$i]+$capaian_tes_gol_dar[$i]+$capaian_tes_urin[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet_all_periode;
                    // $pel_by_id1[$pel['id']] = 0;
                    // $pel_by_id3[$pel['id']] = 0;
                }else if($pel['tanggal_pelayanan'] <= $hayo9bulanlagi && $pel['tanggal_pelayanan'] > $hayo6bulanlagi){
                    $pel_by_id3[$pel['id']] = $capaian_berat[$i]+$capaian_tinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                    $capaian_tinggi_puncak_rahim[$i]+$capaian_presensi[$i]+$capaian_djj[$i]+$capaian_skrining[$i]+$capaian_imun_tt[$i]+
                    $capaian_tes_kehamilan[$i]+$capaian_tes_hb[$i]+$capaian_tes_gol_dar[$i]+$capaian_tes_urin[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet_all_periode;
                    // $pel_by_id2[$pel['id']] = 0;
                    // $pel_by_id1[$pel['id']] = 0;
                }
    
                //End Perhitungan per Pelayanan
            }
            // End Inisialisai Nilai Kriteria per Pelayanan dan Hitung Pencapaian Pelayanan per Pelayanan
            // Inisialisai Nilai Kriteria per Periode
            foreach($pelayanan as $i => $pel){
                if($pel['tanggal_pelayanan'] <= $hayo3bulanlagi && $pel['tanggal_pelayanan'] > $ibuhamil->tanggal_hamil || $pel['tanggal_pelayanan'] == $ibuhamil->tanggal_hamil ){
                    if($capaian_berat_periode_1 < 0.5){$capaian_berat_periode_1 = $capaian_berat_periode_1 + $capaian_berat[$i];}
                    if($capaian_tinggi_periode_1 < 0.5){$capaian_tinggi_periode_1 = $capaian_tinggi_periode_1 + $capaian_tinggi[$i];}
                    if($capaian_tekanan_darah_periode_1 < 1){$capaian_tekanan_darah_periode_1 = $capaian_tekanan_darah_periode_1 + $capaian_tekanan_darah[$i];}
                    if($capaian_lila_periode_1 < 1){$capaian_lila_periode_1 = $capaian_lila_periode_1 + $capaian_lila[$i];}
                    if($capaian_tinggi_puncak_rahim_periode_1 < 1){$capaian_tinggi_puncak_rahim_periode_1 = $capaian_tinggi_puncak_rahim_periode_1 + $capaian_tinggi_puncak_rahim[$i];}
                    if($capaian_presensi_periode_1 < 0.5){$capaian_presensi_periode_1 = $capaian_presensi_periode_1 + $capaian_presensi[$i];}
                    if($capaian_djj_periode_1 < 0.5){$capaian_djj_periode_1 = $capaian_djj_periode_1 + $capaian_djj[$i];}
                    if($capaian_imun_tt_periode_1 < 0.5){$capaian_imun_tt_periode_1 = $capaian_imun_tt_periode_1 + $capaian_imun_tt[$i];}
                    if($capaian_skrining_periode_1 < 0.5){$capaian_skrining_periode_1 = $capaian_skrining_periode_1 + $capaian_skrining[$i];}
                    if($capaian_tes_kehamilan_periode_1 < 0.25){$capaian_tes_kehamilan_periode_1 = $capaian_tes_kehamilan_periode_1 + $capaian_tes_kehamilan[$i];}
                    if($capaian_tes_hb_periode_1 < 0.25){$capaian_tes_hb_periode_1 = $capaian_tes_hb_periode_1 + $capaian_tes_hb[$i];}
                    if($capaian_tes_gol_dar_periode_1 < 0.25){$capaian_tes_gol_dar_periode_1 = $capaian_tes_gol_dar_periode_1 + $capaian_tes_gol_dar[$i];}
                    if($capaian_tes_urin_periode_1 < 0.25){$capaian_tes_urin_periode_1 = $capaian_tes_urin_periode_1 + $capaian_tes_urin[$i];}
                    if($capaian_tatalaksana_periode_1 < 1){$capaian_tatalaksana_periode_1 = $capaian_tatalaksana_periode_1 + $capaian_tatalaksana[$i];}
                    if($capaian_temu_periode_1 < 1){$capaian_temu_periode_1 = $capaian_temu_periode_1 + $capaian_temu[$i];}
                }else if($pel['tanggal_pelayanan'] <= $hayo6bulanlagi && $pel['tanggal_pelayanan'] > $hayo3bulanlagi){
                    if($capaian_berat_periode_2 < 0.5){$capaian_berat_periode_2 = $capaian_berat_periode_2 + $capaian_berat[$i];}
                    if($capaian_tinggi_periode_2 < 0.5){$capaian_tinggi_periode_2 = $capaian_tinggi_periode_2 + $capaian_tinggi[$i];}
                    if($capaian_tekanan_darah_periode_2 < 1){$capaian_tekanan_darah_periode_2 = $capaian_tekanan_darah_periode_2 + $capaian_tekanan_darah[$i];}
                    if($capaian_lila_periode_2 < 1){$capaian_lila_periode_2 = $capaian_lila_periode_2 + $capaian_lila[$i];}
                    if($capaian_tinggi_puncak_rahim_periode_2 < 1){$capaian_tinggi_puncak_rahim_periode_2 = $capaian_tinggi_puncak_rahim_periode_2 + $capaian_tinggi_puncak_rahim[$i];}
                    if($capaian_presensi_periode_2 < 0.5){$capaian_presensi_periode_2 = $capaian_presensi_periode_2 + $capaian_presensi[$i];}
                    if($capaian_djj_periode_2 < 0.5){$capaian_djj_periode_2 = $capaian_djj_periode_2 + $capaian_djj[$i];}
                    if($capaian_imun_tt_periode_2 < 0.5){$capaian_imun_tt_periode_2 = $capaian_imun_tt_periode_2 + $capaian_imun_tt[$i];}
                    if($capaian_skrining_periode_2 < 0.5){$capaian_skrining_periode_2 = $capaian_skrining_periode_2 + $capaian_skrining[$i];}
                    if($capaian_tes_kehamilan_periode_2 < 0.25){$capaian_tes_kehamilan_periode_2 = $capaian_tes_kehamilan_periode_2 + $capaian_tes_kehamilan[$i];}
                    if($capaian_tes_hb_periode_2 < 0.25){$capaian_tes_hb_periode_2 = $capaian_tes_hb_periode_2 + $capaian_tes_hb[$i];}
                    if($capaian_tes_gol_dar_periode_2 < 0.25){$capaian_tes_gol_dar_periode_2 = $capaian_tes_gol_dar_periode_2 + $capaian_tes_gol_dar[$i];}
                    if($capaian_tes_urin_periode_2 < 0.25){$capaian_tes_urin_periode_2 = $capaian_tes_urin_periode_2 + $capaian_tes_urin[$i];}
                    if($capaian_tatalaksana_periode_2 < 1){$capaian_tatalaksana_periode_2 = $capaian_tatalaksana_periode_2 + $capaian_tatalaksana[$i];}
                    if($capaian_temu_periode_2 < 1){$capaian_temu_periode_2 = $capaian_temu_periode_2 + $capaian_temu[$i];}
                }else if($pel['tanggal_pelayanan'] <= $hayo9bulanlagi && $pel['tanggal_pelayanan'] > $hayo6bulanlagi){
                    if($capaian_berat_periode_3 < 1){$capaian_berat_periode_3 = $capaian_berat_periode_3 + $capaian_berat[$i];}
                    if($capaian_tinggi_periode_3 < 1){$capaian_tinggi_periode_3 = $capaian_tinggi_periode_3 + $capaian_tinggi[$i];}
                    if($capaian_tekanan_darah_periode_3 < 2){$capaian_tekanan_darah_periode_3 = $capaian_tekanan_darah_periode_3 + $capaian_tekanan_darah[$i];}
                    if($capaian_lila_periode_3 < 2){$capaian_lila_periode_3 = $capaian_lila_periode_3 + $capaian_lila[$i];}
                    if($capaian_tinggi_puncak_rahim_periode_3 < 2){$capaian_tinggi_puncak_rahim_periode_3 = $capaian_tinggi_puncak_rahim_periode_3 + $capaian_tinggi_puncak_rahim[$i];}
                    if($capaian_presensi_periode_3 < 1){$capaian_presensi_periode_3 = $capaian_presensi_periode_3 + $capaian_presensi[$i];}
                    if($capaian_djj_periode_3 < 1){$capaian_djj_periode_3 = $capaian_djj_periode_3 + $capaian_djj[$i];}
                    if($capaian_imun_tt_periode_3 < 1){$capaian_imun_tt_periode_3 = $capaian_imun_tt_periode_3 + $capaian_imun_tt[$i];}
                    if($capaian_skrining_periode_3 < 1){$capaian_skrining_periode_3 = $capaian_skrining_periode_3 + $capaian_skrining[$i];}
                    if($capaian_tes_kehamilan_periode_3 < 0.5){$capaian_tes_kehamilan_periode_3 = $capaian_tes_kehamilan_periode_3 + $capaian_tes_kehamilan[$i];}
                    if($capaian_tes_hb_periode_3 < 0.5){$capaian_tes_hb_periode_3 = $capaian_tes_hb_periode_3 + $capaian_tes_hb[$i];}
                    if($capaian_tes_gol_dar_periode_3 < 0.5){$capaian_tes_gol_dar_periode_3 = $capaian_tes_gol_dar_periode_3 + $capaian_tes_gol_dar[$i];}
                    if($capaian_tes_urin_periode_3 < 0.5){$capaian_tes_urin_periode_3 = $capaian_tes_urin_periode_3 + $capaian_tes_urin[$i];}
                    if($capaian_tatalaksana_periode_3 < 2){$capaian_tatalaksana_periode_3 = $capaian_tatalaksana_periode_3 + $capaian_tatalaksana[$i];}
                    if($capaian_temu_periode_3 < 2){$capaian_temu_periode_3 = $capaian_temu_periode_3 + $capaian_temu[$i];}
                }
                // print "capaian berat tinggi ";
                // print" pelayanan ke";
                // print $i+1;
                // print " periode ";
                // print " ".$pel['jenis_tw']." : ";
                // print $capaian_berat_tinggi[$i]."<br>";   
            }
            if($capaian_tablet >=90){
                $capaian_tablet_all_periode = 1;
            }
            else{
                $capaian_tablet_all_periode = 0;
            }
            // End Inisialisai Nilai Kriteria per Periode

            //Hitung Pencapaian Pelayanan per Periode
            $tw1capaian =   (($capaian_berat_periode_1+$capaian_tinggi_periode_1+$capaian_tekanan_darah_periode_1+$capaian_lila_periode_1+
                            $capaian_tinggi_puncak_rahim_periode_1+$capaian_presensi_periode_1+$capaian_djj_periode_1+$capaian_skrining_periode_1+$capaian_imun_tt_periode_1+
                            $capaian_tes_kehamilan_periode_1+$capaian_tes_hb_periode_1+$capaian_tes_gol_dar_periode_1+$capaian_tes_urin_periode_1+$capaian_tatalaksana_periode_1+
                            $capaian_temu_periode_1+$capaian_tablet_all_periode) * 100 ) / 10;
            $tw2capaian =   (($capaian_berat_periode_2+$capaian_tinggi_periode_2+$capaian_tekanan_darah_periode_2+$capaian_lila_periode_2+
                            $capaian_tinggi_puncak_rahim_periode_2+$capaian_presensi_periode_2+$capaian_djj_periode_2+$capaian_skrining_periode_2+$capaian_imun_tt_periode_2+
                            $capaian_tes_kehamilan_periode_2+$capaian_tes_hb_periode_2+$capaian_tes_gol_dar_periode_2+$capaian_tes_urin_periode_2+$capaian_tatalaksana_periode_2+
                            $capaian_temu_periode_2+$capaian_tablet_all_periode) * 100 ) / 10;
            $tw3capaian =   (($capaian_berat_periode_3+$capaian_tinggi_periode_3+$capaian_tekanan_darah_periode_3+$capaian_lila_periode_3+
                            $capaian_tinggi_puncak_rahim_periode_3+$capaian_presensi_periode_3+$capaian_djj_periode_3+$capaian_skrining_periode_3+$capaian_imun_tt_periode_3+
                            $capaian_tes_kehamilan_periode_3+$capaian_tes_hb_periode_3+$capaian_tes_gol_dar_periode_3+$capaian_tes_urin_periode_3+$capaian_tatalaksana_periode_3+
                            $capaian_temu_periode_3+$capaian_tablet_all_periode) * 100 ) / 20;
            //End Hitung Pencapaian Pelayanan per Periode

            //Hitung Pencapaian Pelayanan per Total
            $totalcapaian = ($tw1capaian+$tw2capaian+$tw3capaian)/3;
        // $super_pel_total = $pel_total + $pel_total2 + $pel_total3;
        // $totalcapaian = ($super_pel_total * 100) / 40; 
            return $totalcapaian;
    }
}
