<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Requests;
use Illuminate\Support\Facades\Session;
use Redirect;

use App\IbuHamil;
use App\Pelayanan;
use App\Warga;
use App\MitraKesehatan;
use App\TenagaKesehatan;
use App\Spesialis;


class IbuHamilController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        return view('spm.ibuhamil.index');
    }

    public function data(){
        $total_ibu_hamil = 0;
        $total_sesuai_spm = 0;
        $total_tsesuai_spm = 0; 
        $total_belum_melahirkan = 0;
        $data = IbuHamil::all();

        //Looping Semua Ibu Hamil
        foreach($data as $index =>$ibuhamil){
            $warga[$index] = Warga::where('nik',$ibuhamil['nik'])->first();
            $total_ibu_hamil++;
            $hayo3bulanlagi = date('Y-m-d', strtotime('+3 month', strtotime($ibuhamil->tanggal_hamil)));
            $hayo6bulanlagi = date('Y-m-d', strtotime('+6 month', strtotime($ibuhamil->tanggal_hamil)));
            $hayo9bulanlagi = date('Y-m-d', strtotime('+9 month', strtotime($ibuhamil->tanggal_hamil)));
            if($ibuhamil->status_kehamilan == "Tahap Pelayanan"){
                $total_belum_melahirkan++;
            }
            if($ibuhamil->status_kehamilan == "Sesuai SPM"){
                $total_sesuai_spm++;
            }
            if($ibuhamil->status_kehamilan == "Tidak Sesuai SPM"){
                $total_tsesuai_spm++;
            }
            
            $pelayanan[$index] = Pelayanan::where('table_spm', 'ibu_hamil')->where('id_spm',$ibuhamil->id)->get();
            
            $capaian_berat_periode_1 =0;$capaian_berat_periode_2 =0;$capaian_berat_periode_3 =0;
            $capaian_tinggi_periode_1 =0;$capaian_tinggi_periode_2 =0;$capaian_tinggi_periode_3 =0;
            $capaian_berattinggi_periode_1 =0;$capaian_berattinggi_periode_2 =0;$capaian_berattinggi_periode_3 =0;
            $capaian_tekanan_darah_periode_1 =0;$capaian_tekanan_darah_periode_2 =0;$capaian_tekanan_darah_periode_3 =0;
            $capaian_lila_periode_1 =0;$capaian_lila_periode_2 =0;$capaian_lila_periode_3 =0;
            $capaian_tinggi_puncak_rahim_periode_1 =0;$capaian_tinggi_puncak_rahim_periode_2 =0;$capaian_tinggi_puncak_rahim_periode_3 =0;
            $capaian_presensi_periode_1 =0;$capaian_presensi_periode_2 =0;$capaian_presensi_periode_3 =0;
            $capaian_djj_periode_1 =0;$capaian_djj_periode_2 =0;$capaian_djj_periode_3 =0;
            $capaian_presensidjj_periode_1 =0;$capaian_presensidjj_periode_2 =0;$capaian_presensidjj_periode_3 =0;
            $capaian_skrining_periode_1 =0;$capaian_skrining_periode_2 =0;$capaian_skrining_periode_3 =0;
            $capaian_imun_tt_periode_1 =0;$capaian_imun_tt_periode_2 =0;$capaian_imun_tt_periode_3 =0;
            $capaian_imunisasi_periode_1 =0;$capaian_imunisasi_periode_2 =0;$capaian_imunisasi_periode_3 =0;
            $capaian_tes_kehamilan_periode_1 = 0;$capaian_tes_kehamilan_periode_2 =0;$capaian_tes_kehamilan_periode_3 =0;
            $capaian_tes_hb_periode_1 = 0;$capaian_tes_hb_periode_2= 0;$capaian_tes_hb_periode_3 = 0;
            $capaian_tes_gol_dar_periode_1 = 0;$capaian_tes_gol_dar_periode_2 = 0;$capaian_tes_gol_dar_periode_3 = 0;
            $capaian_tes_urin_periode_1 = 0;$capaian_tes_urin_periode_2 = 0;$capaian_tes_urin_periode_3 = 0;        
            $capaian_teslabo_periode_1 = 0;$capaian_teslabo_periode_2 = 0;$capaian_teslabo_periode_3 = 0;        
            $capaian_tatalaksana_periode_1 =0;$capaian_tatalaksana_periode_2 =0;$capaian_tatalaksana_periode_3 =0;
            $capaian_temu_periode_1 =0;$capaian_temu_periode_2 =0;$capaian_temu_periode_3 =0;        
            $capaian_tablet_all_periode =0;        
                $pel_by_id = [];
                $pel_by_id2 = [];
                $pel_by_id3 = [];   
            // Inisialisai Nilai Kriteria per Pelayanan dan Hitung Pencapaian Pelayanan per Pelayanan
            foreach($pelayanan[$index] as $i => $pel){
                $capaian_berat[$i] = 0;$capaian_tinggi[$i] = 0;
                $capaian_berattinggi[$i] = 0;
                $capaian_tekanan_darah[$i] = 0;
                $capaian_lila[$i] = 0;
                $capaian_tinggi_puncak_rahim[$i] = 0;
                $capaian_presensi[$i] = 0;
                $capaian_djj[$i] = 0;
                $capaian_presensidjj[$i] = 0;
                $capaian_skrining[$i] = 0;
                $capaian_imun_tt[$i] = 0;
                $capaian_imunisasi[$i] = 0;
                $capaian_tes_kehamilan[$i] = 0;
                $capaian_tes_hb[$i] = 0;
                $capaian_tes_gol_dar[$i] = 0;
                $capaian_tes_urin[$i] = 0;
                $capaian_teslabo[$i] = 0;
                $capaian_tatalaksana[$i] = 0;
                $capaian_temu[$i] = 0;
                $capaian_tablet[$i] =0;

                //Berat Tinggi
                if($pel['berat_tinggi'] != null){
                    $bt = explode('_',$pel['berat_tinggi']);
                    if($bt[0] > 45 && $bt[1] > 100){
                        $capaian_berattinggi[$i] = 1;
                    }         
                }
                
                // Tekanan Darah
                if($pel['tekanan_darah'] != null){
                    $td = explode('/',$pel['tekanan_darah']);
                    if($td[0] > 90 && $td[1] > 60){
                        $capaian_tekanan_darah[$i] = 1;
                    }            
                }
        
                //Lila
                if($pel['lila'] != null && $pel['lila'] > 2.35 ){
                    $capaian_lila[$i] = 1;            
                }
        
                //Tinggi Puncak Rahim
                if($pel['tinggi_puncak_rahim'] != null){
                    $capaian_tinggi_puncak_rahim[$i] = 1;            
                }
        
                //Presentasi DJJ
                if($pel['presentasi_djj']!= null){
                    $pdjj = explode('_',$pel['presentasi_djj']);
                    if($pdjj[0] != null && $pdjj[1] >120){
                        $capaian_presensidjj[$i] = 1;            
                    }
                }
        
                //Imunisasi Tetanus TT
                if($pel['imunisasi_tetanus_tt'] != null){
                    $itt = explode('_',$pel['imunisasi_tetanus_tt']);
                    if($itt[0] == "y" && $itt[1] == "y"){  
                        $capaian_imunisasi[$i] = 1;          
                    }
                }
                
                //Tablet Tambah Darah
                if($pel['tablet_tambah_darah'] != null){
                    $capaian_tablet[$i] = 1;
                    $capaian_tablet_all_periode = $capaian_tablet_all_periode + $pel['tablet_tambah_darah'];          
                }
                else{
                    $capaian_tablet_all_periode = $capaian_tablet_all_periode + 0;            
                }
        
                //Tes Laboratorium
                if($pel['tes_laboratorium'] != null){
                    $tl = explode('_',$pel['tes_laboratorium']);
                    
                    if($tl[0] == "y" && $tl[1] == "y" && $tl[2] == "y" && $tl[3] == "y"){
                        $capaian_teslabo[$i] = 1;            
                    }
                }
        
                //Tatalaksana
                if($pel['tatalaksana'] == "y"){
                    $capaian_tatalaksana[$i] = 1;            
                }
        
                //Temu Wicara
                if($pel['temu_wicara'] == "y"){
                    $capaian_temu[$i] = 1;            
                }

                //Perhitungan per Pelayanan
                if($pel['tanggal_pelayanan'] <= $hayo3bulanlagi && $pel['tanggal_pelayanan'] > $ibuhamil->tanggal_hamil || $pel['tanggal_pelayanan'] == $ibuhamil->tanggal_hamil ){
                    $pel_by_id[$pel['id']] = $capaian_berattinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                    $capaian_tinggi_puncak_rahim[$i]+$capaian_presensidjj[$i]+$capaian_imunisasi[$i]+
                    $capaian_teslabo[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet[$i];
                    // $pel_by_id2[$pel['id']] = 0;
                    // $pel_by_id3[$pel['id']] = 0;
                }else if($pel['tanggal_pelayanan'] <= $hayo6bulanlagi && $pel['tanggal_pelayanan'] > $hayo3bulanlagi){
                    $pel_by_id2[$pel['id']] = $capaian_berattinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                    $capaian_tinggi_puncak_rahim[$i]+$capaian_presensidjj[$i]+$capaian_imunisasi[$i]+
                    $capaian_teslabo[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet[$i];
                    // $pel_by_id1[$pel['id']] = 0;
                    // $pel_by_id3[$pel['id']] = 0;
                }else if($pel['tanggal_pelayanan'] <= $hayo9bulanlagi && $pel['tanggal_pelayanan'] > $hayo6bulanlagi){
                    $pel_by_id3[$pel['id']] = $capaian_berattinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                    $capaian_tinggi_puncak_rahim[$i]+$capaian_presensidjj[$i]+$capaian_imunisasi[$i]+
                    $capaian_teslabo[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet[$i];
                    // $pel_by_id2[$pel['id']] = 0;
                    // $pel_by_id1[$pel['id']] = 0;
                }
                //End Perhitungan per Pelayanan
            }
            // End Inisialisai Nilai Kriteria per Pelayanan dan Hitung Pencapaian Pelayanan per Pelayanan
            // Inisialisai Nilai Kriteria per Periode
            foreach($pelayanan[$index] as $i => $pel){
                if($pel['tanggal_pelayanan'] <= $hayo3bulanlagi && $pel['tanggal_pelayanan'] > $ibuhamil->tanggal_hamil || $pel['tanggal_pelayanan'] == $ibuhamil->tanggal_hamil ){
                    if($capaian_berattinggi_periode_1 < 1){$capaian_berattinggi_periode_1 = $capaian_berattinggi_periode_1 + $capaian_berattinggi[$i];}
                    if($capaian_tekanan_darah_periode_1 < 1){$capaian_tekanan_darah_periode_1 = $capaian_tekanan_darah_periode_1 + $capaian_tekanan_darah[$i];}
                    if($capaian_lila_periode_1 < 1){$capaian_lila_periode_1 = $capaian_lila_periode_1 + $capaian_lila[$i];}
                    if($capaian_tinggi_puncak_rahim_periode_1 < 1){$capaian_tinggi_puncak_rahim_periode_1 = $capaian_tinggi_puncak_rahim_periode_1 + $capaian_tinggi_puncak_rahim[$i];}
                    if($capaian_presensidjj_periode_1 < 1){$capaian_presensidjj_periode_1 = $capaian_presensidjj_periode_1 + $capaian_presensidjj[$i];}
                    if($capaian_imunisasi_periode_1 < 1){$capaian_imunisasi_periode_1 = $capaian_imunisasi_periode_1 + $capaian_imunisasi[$i];}
                    if($capaian_teslabo_periode_1 < 1){$capaian_teslabo_periode_1 = $capaian_teslabo_periode_1 + $capaian_teslabo[$i];}
                    if($capaian_tatalaksana_periode_1 < 1){$capaian_tatalaksana_periode_1 = $capaian_tatalaksana_periode_1 + $capaian_tatalaksana[$i];}
                    if($capaian_temu_periode_1 < 1){$capaian_temu_periode_1 = $capaian_temu_periode_1 + $capaian_temu[$i];}
                }else if($pel['tanggal_pelayanan'] <= $hayo6bulanlagi && $pel['tanggal_pelayanan'] > $hayo3bulanlagi){
                    if($capaian_berattinggi_periode_2 < 1){$capaian_berattinggi_periode_2 = $capaian_berattinggi_periode_2 + $capaian_berattinggi[$i];}
                    if($capaian_tekanan_darah_periode_2 < 1){$capaian_tekanan_darah_periode_2 = $capaian_tekanan_darah_periode_2 + $capaian_tekanan_darah[$i];}
                    if($capaian_lila_periode_2 < 1){$capaian_lila_periode_2 = $capaian_lila_periode_2 + $capaian_lila[$i];}
                    if($capaian_tinggi_puncak_rahim_periode_2 < 1){$capaian_tinggi_puncak_rahim_periode_2 = $capaian_tinggi_puncak_rahim_periode_2 + $capaian_tinggi_puncak_rahim[$i];}
                    if($capaian_presensidjj_periode_2 < 1){$capaian_presensidjj_periode_2 = $capaian_presensidjj_periode_2 + $capaian_presensidjj[$i];}
                    if($capaian_imunisasi_periode_2 < 1){$capaian_imunisasi_periode_2 = $capaian_imunisasi_periode_2 + $capaian_imunisasi[$i];}
                    if($capaian_teslabo_periode_2 < 1){$capaian_teslabo_periode_2 = $capaian_teslabo_periode_2 + $capaian_teslabo[$i];}
                    if($capaian_tatalaksana_periode_2 < 1){$capaian_tatalaksana_periode_2 = $capaian_tatalaksana_periode_2 + $capaian_tatalaksana[$i];}
                    if($capaian_temu_periode_2 < 1){$capaian_temu_periode_2 = $capaian_temu_periode_2 + $capaian_temu[$i];}
                }else if($pel['tanggal_pelayanan'] <= $hayo9bulanlagi && $pel['tanggal_pelayanan'] > $hayo6bulanlagi){
                    if($capaian_berattinggi_periode_3 < 2){$capaian_berattinggi_periode_3 = $capaian_berattinggi_periode_3 + $capaian_berattinggi[$i];}
                    if($capaian_tekanan_darah_periode_3 < 2){$capaian_tekanan_darah_periode_3 = $capaian_tekanan_darah_periode_3 + $capaian_tekanan_darah[$i];}
                    if($capaian_lila_periode_3 < 2){$capaian_lila_periode_3 = $capaian_lila_periode_3 + $capaian_lila[$i];}
                    if($capaian_tinggi_puncak_rahim_periode_3 < 2){$capaian_tinggi_puncak_rahim_periode_3 = $capaian_tinggi_puncak_rahim_periode_3 + $capaian_tinggi_puncak_rahim[$i];}
                    if($capaian_presensidjj_periode_3 < 2){$capaian_presensidjj_periode_3 = $capaian_presensidjj_periode_3 + $capaian_presensidjj[$i];}
                    if($capaian_imunisasi_periode_3 < 2){$capaian_imunisasi_periode_3 = $capaian_imunisasi_periode_3 + $capaian_imunisasi[$i];}
                    if($capaian_teslabo_periode_3 < 2){$capaian_teslabo_periode_3 = $capaian_teslabo_periode_3 + $capaian_teslabo[$i];}
                    if($capaian_tatalaksana_periode_3 < 2){$capaian_tatalaksana_periode_3 = $capaian_tatalaksana_periode_3 + $capaian_tatalaksana[$i];}
                    if($capaian_temu_periode_3 < 2){$capaian_temu_periode_3 = $capaian_temu_periode_3 + $capaian_temu[$i];}
                }
                // print "capaian berat tinggi ";
                // print" pelayanan ke";
                // print $i+1;
                // print " periode ";
                // print " ".$pel['jenis_tw']." : ";
                // print $capaian_berat_tinggi[$i]."<br>";   
            }
            if($capaian_tablet_all_periode >=90){
                $capaian_tablet_all_periode = 1;
            }
            else{
                $capaian_tablet_all_periode = 0;
            }
            // End Inisialisai Nilai Kriteria per Periode

            // Coba Test Data Ibu Hamil
            // print "capaian berat tinggi periode 1 : ".$capaian_berat_tinggi_periode_1."<br>";
            // print "capaian berat tinggi periode 2 : ".$capaian_berat_tinggi_periode_2."<br>";
            // print "capaian berat tinggi periode 3 : ".$capaian_berat_tinggi_periode_3."<br>";

            // print $total."<br><br>";
            //End Coba Test Data Ibu Hamil
            //Hitung Pencapaian Pelayanan per Periode
            $capaian_periode1[$index] = (($capaian_berattinggi_periode_1+$capaian_tekanan_darah_periode_1+$capaian_lila_periode_1+
                                        $capaian_tinggi_puncak_rahim_periode_1+$capaian_presensidjj_periode_1+$capaian_imunisasi_periode_1+
                                        $capaian_teslabo_periode_1+$capaian_tatalaksana_periode_1+$capaian_temu_periode_1+$capaian_tablet_all_periode) * 100 ) / 10;
            $capaian_periode2[$index] = (($capaian_berattinggi_periode_2+$capaian_tekanan_darah_periode_2+$capaian_lila_periode_2+
                                        $capaian_tinggi_puncak_rahim_periode_2+$capaian_presensidjj_periode_2+$capaian_imunisasi_periode_2+
                                        $capaian_teslabo_periode_2+$capaian_tatalaksana_periode_2+$capaian_temu_periode_2+$capaian_tablet_all_periode) * 100 ) / 10;
            $capaian_periode3[$index] = (($capaian_berattinggi_periode_3+$capaian_tekanan_darah_periode_3+$capaian_lila_periode_3+
                                        $capaian_tinggi_puncak_rahim_periode_3+$capaian_presensidjj_periode_3+$capaian_imunisasi_periode_3+
                                        $capaian_teslabo_periode_3+$capaian_tatalaksana_periode_3+$capaian_temu_periode_3+$capaian_tablet_all_periode) * 100 ) / 20;
            //End Hitung Pencapaian Pelayanan per Periode

            //Hitung Pencapaian Pelayanan per Total
            $super_pel_total = $capaian_berattinggi_periode_1+$capaian_tekanan_darah_periode_1+$capaian_lila_periode_1+
            $capaian_tinggi_puncak_rahim_periode_1+$capaian_presensidjj_periode_1+$capaian_imunisasi_periode_1+
            $capaian_teslabo_periode_1+$capaian_tatalaksana_periode_1+$capaian_temu_periode_1+$capaian_tablet_all_periode + 
            $capaian_berattinggi_periode_2+$capaian_tekanan_darah_periode_2+$capaian_lila_periode_2+
            $capaian_tinggi_puncak_rahim_periode_2+$capaian_presensidjj_periode_2+$capaian_imunisasi_periode_2+
            $capaian_teslabo_periode_2+$capaian_tatalaksana_periode_2+$capaian_temu_periode_2+$capaian_tablet_all_periode+
            $capaian_berattinggi_periode_3+$capaian_tekanan_darah_periode_3+$capaian_lila_periode_3+
            $capaian_tinggi_puncak_rahim_periode_3+$capaian_presensidjj_periode_3+$capaian_imunisasi_periode_3+
            $capaian_teslabo_periode_3+$capaian_tatalaksana_periode_3+$capaian_temu_periode_3+$capaian_tablet_all_periode;

            $totalcapaian[$index] = ($capaian_periode1[$index]+$capaian_periode2[$index]+$capaian_periode3[$index])/3;
            //End Hitung Pencapaian Pelayanan per Total

        }
        return view('spm.ibuhamil.data',compact(
            'data',
            'warga',
            'totalcapaian',
            'total_ibu_hamil',
            'total_sesuai_spm',
            'total_tsesuai_spm', 
            'total_belum_melahirkan'));
    }

    public function detail($id){
        $data = IbuHamil::where('id', $id)->first();
        $warga = Warga::where('nik', $data['nik'])->first();
        $nama = $warga['nama'];
        Session::put('withnik',$warga['nik']);
        $pelayanan = Pelayanan::where('table_spm', 'ibu_hamil')->where('id_spm',$data['id'])->get();
        
        $hayo3bulanlagi = date('Y-m-d', strtotime('+3 month', strtotime($data['tanggal_hamil'])));
        $hayo6bulanlagi = date('Y-m-d', strtotime('+6 month', strtotime($data['tanggal_hamil'])));
        $hayo9bulanlagi = date('Y-m-d', strtotime('+9 month', strtotime($data['tanggal_hamil'])));
        
            $capaian_berat_periode_1 =0;$capaian_berat_periode_2 =0;$capaian_berat_periode_3 =0;
            $capaian_tinggi_periode_1 =0;$capaian_tinggi_periode_2 =0;$capaian_tinggi_periode_3 =0;
            $capaian_berattinggi_periode_1 =0;$capaian_berattinggi_periode_2 =0;$capaian_berattinggi_periode_3 =0;
            $capaian_tekanan_darah_periode_1 =0;$capaian_tekanan_darah_periode_2 =0;$capaian_tekanan_darah_periode_3 =0;
            $capaian_lila_periode_1 =0;$capaian_lila_periode_2 =0;$capaian_lila_periode_3 =0;
            $capaian_tinggi_puncak_rahim_periode_1 =0;$capaian_tinggi_puncak_rahim_periode_2 =0;$capaian_tinggi_puncak_rahim_periode_3 =0;
            $capaian_presensi_periode_1 =0;$capaian_presensi_periode_2 =0;$capaian_presensi_periode_3 =0;
            $capaian_djj_periode_1 =0;$capaian_djj_periode_2 =0;$capaian_djj_periode_3 =0;
            $capaian_presensidjj_periode_1 =0;$capaian_presensidjj_periode_2 =0;$capaian_presensidjj_periode_3 =0;
            $capaian_skrining_periode_1 =0;$capaian_skrining_periode_2 =0;$capaian_skrining_periode_3 =0;
            $capaian_imun_tt_periode_1 =0;$capaian_imun_tt_periode_2 =0;$capaian_imun_tt_periode_3 =0;
            $capaian_imunisasi_periode_1 =0;$capaian_imunisasi_periode_2 =0;$capaian_imunisasi_periode_3 =0;
            $capaian_tes_kehamilan_periode_1 = 0;$capaian_tes_kehamilan_periode_2 =0;$capaian_tes_kehamilan_periode_3 =0;
            $capaian_tes_hb_periode_1 = 0;$capaian_tes_hb_periode_2= 0;$capaian_tes_hb_periode_3 = 0;
            $capaian_tes_gol_dar_periode_1 = 0;$capaian_tes_gol_dar_periode_2 = 0;$capaian_tes_gol_dar_periode_3 = 0;
            $capaian_tes_urin_periode_1 = 0;$capaian_tes_urin_periode_2 = 0;$capaian_tes_urin_periode_3 = 0;        
            $capaian_teslabo_periode_1 = 0;$capaian_teslabo_periode_2 = 0;$capaian_teslabo_periode_3 = 0;        
            $capaian_tatalaksana_periode_1 =0;$capaian_tatalaksana_periode_2 =0;$capaian_tatalaksana_periode_3 =0;
            $capaian_temu_periode_1 =0;$capaian_temu_periode_2 =0;$capaian_temu_periode_3 =0;        
            $capaian_tablet_all_periode =0;        
            $pel_by_id = [];
            $pel_by_id2 = [];
            $pel_by_id3 = [];   
            // Inisialisai Nilai Kriteria per Pelayanan dan Hitung Pencapaian Pelayanan per Pelayanan
            foreach($pelayanan as $i => $pel){
                $capaian_berat[$i] = 0;$capaian_tinggi[$i] = 0;
                $capaian_berattinggi[$i] = 0;
                $capaian_tekanan_darah[$i] = 0;
                $capaian_lila[$i] = 0;
                $capaian_tinggi_puncak_rahim[$i] = 0;
                $capaian_presensi[$i] = 0;
                $capaian_djj[$i] = 0;
                $capaian_presensidjj[$i] = 0;
                $capaian_skrining[$i] = 0;
                $capaian_imun_tt[$i] = 0;
                $capaian_imunisasi[$i] = 0;
                $capaian_tes_kehamilan[$i] = 0;
                $capaian_tes_hb[$i] = 0;
                $capaian_tes_gol_dar[$i] = 0;
                $capaian_tes_urin[$i] = 0;
                $capaian_teslabo[$i] = 0;
                $capaian_tatalaksana[$i] = 0;
                $capaian_temu[$i] = 0;
                $capaian_tablet[$i] =0;

                //Berat Tinggi
                if($pel['berat_tinggi'] != null){
                    $bt = explode('_',$pel['berat_tinggi']);
                    if($bt[0] > 45 && $bt[1] > 100){
                        $capaian_berattinggi[$i] = 1;
                    }         
                }
                
                // Tekanan Darah
                if($pel['tekanan_darah'] != null){
                    $td = explode('/',$pel['tekanan_darah']);
                    if($td[0] > 90 && $td[1] > 60){
                        $capaian_tekanan_darah[$i] = 1;
                    }            
                }
        
                //Lila
                if($pel['lila'] != null && $pel['lila'] > 2.35 ){
                    $capaian_lila[$i] = 1;            
                }
        
                //Tinggi Puncak Rahim
                if($pel['tinggi_puncak_rahim'] != null){
                    $capaian_tinggi_puncak_rahim[$i] = 1;            
                }
        
                //Presentasi DJJ
                if($pel['presentasi_djj']!= null){
                    $pdjj = explode('_',$pel['presentasi_djj']);
                    if($pdjj[0] != null && $pdjj[1] >120){
                        $capaian_presensidjj[$i] = 1;            
                    }
                }
        
                //Imunisasi Tetanus TT
                if($pel['imunisasi_tetanus_tt'] != null){
                    $itt = explode('_',$pel['imunisasi_tetanus_tt']);
                    if($itt[0] == "y" && $itt[1] == "y"){  
                        $capaian_imunisasi[$i] = 1;          
                    }
                }
                
                //Tablet Tambah Darah
                if($pel['tablet_tambah_darah'] != null){
                    $capaian_tablet[$i] = 1;
                    $capaian_tablet_all_periode = $capaian_tablet_all_periode + $pel['tablet_tambah_darah'];          
                }
                else{
                    $capaian_tablet_all_periode = $capaian_tablet_all_periode + 0;            
                }
        
                //Tes Laboratorium
                if($pel['tes_laboratorium'] != null){
                    $tl = explode('_',$pel['tes_laboratorium']);
                    
                    if($tl[0] == "y" && $tl[1] == "y" && $tl[2] == "y" && $tl[3] == "y"){
                        $capaian_teslabo[$i] = 1;            
                    }
                }
        
                //Tatalaksana
                if($pel['tatalaksana'] == "y"){
                    $capaian_tatalaksana[$i] = 1;            
                }
        
                //Temu Wicara
                if($pel['temu_wicara'] == "y"){
                    $capaian_temu[$i] = 1;            
                }

                //Perhitungan per Pelayanan
                if($pel['tanggal_pelayanan'] <= $hayo3bulanlagi && $pel['tanggal_pelayanan'] > $data->tanggal_hamil || $pel['tanggal_pelayanan'] == $data->tanggal_hamil ){
                    $pel_by_id[$pel['id']] = $capaian_berattinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                    $capaian_tinggi_puncak_rahim[$i]+$capaian_presensidjj[$i]+$capaian_imunisasi[$i]+
                    $capaian_teslabo[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet[$i];
                    // $pel_by_id2[$pel['id']] = 0;
                    // $pel_by_id3[$pel['id']] = 0;
                }else if($pel['tanggal_pelayanan'] <= $hayo6bulanlagi && $pel['tanggal_pelayanan'] > $hayo3bulanlagi){
                    $pel_by_id2[$pel['id']] = $capaian_berattinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                    $capaian_tinggi_puncak_rahim[$i]+$capaian_presensidjj[$i]+$capaian_imunisasi[$i]+
                    $capaian_teslabo[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet[$i];
                    // $pel_by_id1[$pel['id']] = 0;
                    // $pel_by_id3[$pel['id']] = 0;
                }else if($pel['tanggal_pelayanan'] <= $hayo9bulanlagi && $pel['tanggal_pelayanan'] > $hayo6bulanlagi){
                    $pel_by_id3[$pel['id']] = $capaian_berattinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                    $capaian_tinggi_puncak_rahim[$i]+$capaian_presensidjj[$i]+$capaian_imunisasi[$i]+
                    $capaian_teslabo[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet[$i];
                    // $pel_by_id2[$pel['id']] = 0;
                    // $pel_by_id1[$pel['id']] = 0;
                }
                //End Perhitungan per Pelayanan
            }
            // End Inisialisai Nilai Kriteria per Pelayanan dan Hitung Pencapaian Pelayanan per Pelayanan
            // Inisialisai Nilai Kriteria per Periode
            foreach($pelayanan as $i => $pel){
                if($pel['tanggal_pelayanan'] <= $hayo3bulanlagi && $pel['tanggal_pelayanan'] > $data->tanggal_hamil || $pel['tanggal_pelayanan'] == $data->tanggal_hamil ){
                    if($capaian_berattinggi_periode_1 < 1){$capaian_berattinggi_periode_1 = $capaian_berattinggi_periode_1 + $capaian_berattinggi[$i];}
                    if($capaian_tekanan_darah_periode_1 < 1){$capaian_tekanan_darah_periode_1 = $capaian_tekanan_darah_periode_1 + $capaian_tekanan_darah[$i];}
                    if($capaian_lila_periode_1 < 1){$capaian_lila_periode_1 = $capaian_lila_periode_1 + $capaian_lila[$i];}
                    if($capaian_tinggi_puncak_rahim_periode_1 < 1){$capaian_tinggi_puncak_rahim_periode_1 = $capaian_tinggi_puncak_rahim_periode_1 + $capaian_tinggi_puncak_rahim[$i];}
                    if($capaian_presensidjj_periode_1 < 1){$capaian_presensidjj_periode_1 = $capaian_presensidjj_periode_1 + $capaian_presensidjj[$i];}
                    if($capaian_imunisasi_periode_1 < 1){$capaian_imunisasi_periode_1 = $capaian_imunisasi_periode_1 + $capaian_imunisasi[$i];}
                    if($capaian_teslabo_periode_1 < 1){$capaian_teslabo_periode_1 = $capaian_teslabo_periode_1 + $capaian_teslabo[$i];}
                    if($capaian_tatalaksana_periode_1 < 1){$capaian_tatalaksana_periode_1 = $capaian_tatalaksana_periode_1 + $capaian_tatalaksana[$i];}
                    if($capaian_temu_periode_1 < 1){$capaian_temu_periode_1 = $capaian_temu_periode_1 + $capaian_temu[$i];}
                }else if($pel['tanggal_pelayanan'] <= $hayo6bulanlagi && $pel['tanggal_pelayanan'] > $hayo3bulanlagi){
                    if($capaian_berattinggi_periode_2 < 1){$capaian_berattinggi_periode_2 = $capaian_berattinggi_periode_2 + $capaian_berattinggi[$i];}
                    if($capaian_tekanan_darah_periode_2 < 1){$capaian_tekanan_darah_periode_2 = $capaian_tekanan_darah_periode_2 + $capaian_tekanan_darah[$i];}
                    if($capaian_lila_periode_2 < 1){$capaian_lila_periode_2 = $capaian_lila_periode_2 + $capaian_lila[$i];}
                    if($capaian_tinggi_puncak_rahim_periode_2 < 1){$capaian_tinggi_puncak_rahim_periode_2 = $capaian_tinggi_puncak_rahim_periode_2 + $capaian_tinggi_puncak_rahim[$i];}
                    if($capaian_presensidjj_periode_2 < 1){$capaian_presensidjj_periode_2 = $capaian_presensidjj_periode_2 + $capaian_presensidjj[$i];}
                    if($capaian_imunisasi_periode_2 < 1){$capaian_imunisasi_periode_2 = $capaian_imunisasi_periode_2 + $capaian_imunisasi[$i];}
                    if($capaian_teslabo_periode_2 < 1){$capaian_teslabo_periode_2 = $capaian_teslabo_periode_2 + $capaian_teslabo[$i];}
                    if($capaian_tatalaksana_periode_2 < 1){$capaian_tatalaksana_periode_2 = $capaian_tatalaksana_periode_2 + $capaian_tatalaksana[$i];}
                    if($capaian_temu_periode_2 < 1){$capaian_temu_periode_2 = $capaian_temu_periode_2 + $capaian_temu[$i];}
                }else if($pel['tanggal_pelayanan'] <= $hayo9bulanlagi && $pel['tanggal_pelayanan'] > $hayo6bulanlagi){
                    if($capaian_berattinggi_periode_3 < 2){$capaian_berattinggi_periode_3 = $capaian_berattinggi_periode_3 + $capaian_berattinggi[$i];}
                    if($capaian_tekanan_darah_periode_3 < 2){$capaian_tekanan_darah_periode_3 = $capaian_tekanan_darah_periode_3 + $capaian_tekanan_darah[$i];}
                    if($capaian_lila_periode_3 < 2){$capaian_lila_periode_3 = $capaian_lila_periode_3 + $capaian_lila[$i];}
                    if($capaian_tinggi_puncak_rahim_periode_3 < 2){$capaian_tinggi_puncak_rahim_periode_3 = $capaian_tinggi_puncak_rahim_periode_3 + $capaian_tinggi_puncak_rahim[$i];}
                    if($capaian_presensidjj_periode_3 < 2){$capaian_presensidjj_periode_3 = $capaian_presensidjj_periode_3 + $capaian_presensidjj[$i];}
                    if($capaian_imunisasi_periode_3 < 2){$capaian_imunisasi_periode_3 = $capaian_imunisasi_periode_3 + $capaian_imunisasi[$i];}
                    if($capaian_teslabo_periode_3 < 2){$capaian_teslabo_periode_3 = $capaian_teslabo_periode_3 + $capaian_teslabo[$i];}
                    if($capaian_tatalaksana_periode_3 < 2){$capaian_tatalaksana_periode_3 = $capaian_tatalaksana_periode_3 + $capaian_tatalaksana[$i];}
                    if($capaian_temu_periode_3 < 2){$capaian_temu_periode_3 = $capaian_temu_periode_3 + $capaian_temu[$i];}
                }
            }
            if($capaian_tablet_all_periode >=90){
                $capaian_tablet_all_periode = 1;
            }
            else{
                $capaian_tablet_all_periode = 0;
            }
            // End Inisialisai Nilai Kriteria per Periode

            //End Coba Test Data Ibu Hamil
            //Hitung Pencapaian Pelayanan per Periode
            $tw1capaian = (($capaian_berattinggi_periode_1+$capaian_tekanan_darah_periode_1+$capaian_lila_periode_1+
                                        $capaian_tinggi_puncak_rahim_periode_1+$capaian_presensidjj_periode_1+$capaian_imunisasi_periode_1+
                                        $capaian_teslabo_periode_1+$capaian_tatalaksana_periode_1+$capaian_temu_periode_1+$capaian_tablet_all_periode) * 100 ) / 10;
            $tw2capaian = (($capaian_berattinggi_periode_2+$capaian_tekanan_darah_periode_2+$capaian_lila_periode_2+
                                        $capaian_tinggi_puncak_rahim_periode_2+$capaian_presensidjj_periode_2+$capaian_imunisasi_periode_2+
                                        $capaian_teslabo_periode_2+$capaian_tatalaksana_periode_2+$capaian_temu_periode_2+$capaian_tablet_all_periode) * 100 ) / 10;
            $tw3capaian = (($capaian_berattinggi_periode_3+$capaian_tekanan_darah_periode_3+$capaian_lila_periode_3+
                                        $capaian_tinggi_puncak_rahim_periode_3+$capaian_presensidjj_periode_3+$capaian_imunisasi_periode_3+
                                        $capaian_teslabo_periode_3+$capaian_tatalaksana_periode_3+$capaian_temu_periode_3+$capaian_tablet_all_periode) * 100 ) / 20;
            //End Hitung Pencapaian Pelayanan per Periode

            //Hitung Pencapaian Pelayanan per Total
            $super_pel_total = $capaian_berattinggi_periode_1+$capaian_tekanan_darah_periode_1+$capaian_lila_periode_1+
            $capaian_tinggi_puncak_rahim_periode_1+$capaian_presensidjj_periode_1+$capaian_imunisasi_periode_1+
            $capaian_teslabo_periode_1+$capaian_tatalaksana_periode_1+$capaian_temu_periode_1+$capaian_tablet_all_periode + 
            $capaian_berattinggi_periode_2+$capaian_tekanan_darah_periode_2+$capaian_lila_periode_2+
            $capaian_tinggi_puncak_rahim_periode_2+$capaian_presensidjj_periode_2+$capaian_imunisasi_periode_2+
            $capaian_teslabo_periode_2+$capaian_tatalaksana_periode_2+$capaian_temu_periode_2+$capaian_tablet_all_periode+
            $capaian_berattinggi_periode_3+$capaian_tekanan_darah_periode_3+$capaian_lila_periode_3+
            $capaian_tinggi_puncak_rahim_periode_3+$capaian_presensidjj_periode_3+$capaian_imunisasi_periode_3+
            $capaian_teslabo_periode_3+$capaian_tatalaksana_periode_3+$capaian_temu_periode_3+$capaian_tablet_all_periode;

            $totalcapaian = ($tw1capaian+$tw2capaian+$tw3capaian)/3;
            //End Hitung Pencapaian Pelayanan per Total

        return view('spm.ibuhamil.detail', compact('data',
        'nama','tw1capaian','tw2capaian','tw3capaian','totalcapaian','hayo3bulanlagi','hayo6bulanlagi','hayo9bulanlagi',
        'super_pel_total','pelayanan', 'pel_by_id', 'pel_by_id2', 'pel_by_id3'));        
    }

    public function ubah($id){
        $data = IbuHamil::where('id',$id)->first();
        $pelayanan = Pelayanan::where('table_spm', 'ibu_hamil')->where('id_spm',$id)->get();
        $hayo3bulanlagi = date('Y-m-d', strtotime('+3 month', strtotime($data->tanggal_hamil)));
        $hayo6bulanlagi = date('Y-m-d', strtotime('+6 month', strtotime($data->tanggal_hamil)));
        $hayo9bulanlagi = date('Y-m-d', strtotime('+9 month', strtotime($data->tanggal_hamil)));
        
        $capaian_berat_periode_1 =0;$capaian_berat_periode_2 =0;$capaian_berat_periode_3 =0;
        $capaian_tinggi_periode_1 =0;$capaian_tinggi_periode_2 =0;$capaian_tinggi_periode_3 =0;
        $capaian_tekanan_darah_periode_1 =0;$capaian_tekanan_darah_periode_2 =0;$capaian_tekanan_darah_periode_3 =0;
        $capaian_lila_periode_1 =0;$capaian_lila_periode_2 =0;$capaian_lila_periode_3 =0;
        $capaian_tinggi_puncak_rahim_periode_1 =0;$capaian_tinggi_puncak_rahim_periode_2 =0;$capaian_tinggi_puncak_rahim_periode_3 =0;
        $capaian_presensi_periode_1 =0;$capaian_presensi_periode_2 =0;$capaian_presensi_periode_3 =0;
        $capaian_djj_periode_1 =0;$capaian_djj_periode_2 =0;$capaian_djj_periode_3 =0;
        $capaian_skrining_periode_1 =0;$capaian_skrining_periode_2 =0;$capaian_skrining_periode_3 =0;
        $capaian_imun_tt_periode_1 =0;$capaian_imun_tt_periode_2 =0;$capaian_imun_tt_periode_3 =0;
        $capaian_tablet =0;
        $capaian_tes_kehamilan_periode_1 = 0;$capaian_tes_kehamilan_periode_2 =0;$capaian_tes_kehamilan_periode_3 =0;
        $capaian_tes_hb_periode_1 = 0;$capaian_tes_hb_periode_2= 0;$capaian_tes_hb_periode_3 = 0;
        $capaian_tes_gol_dar_periode_1 = 0;$capaian_tes_gol_dar_periode_2 = 0;$capaian_tes_gol_dar_periode_3 = 0;
        $capaian_tes_urin_periode_1 = 0;$capaian_tes_urin_periode_2 = 0;$capaian_tes_urin_periode_3 = 0;        
        $capaian_tatalaksana_periode_1 =0;$capaian_tatalaksana_periode_2 =0;$capaian_tatalaksana_periode_3 =0;
        $capaian_temu_periode_1 =0;$capaian_temu_periode_2 =0;$capaian_temu_periode_3 =0;        
            // Inisialisai Nilai Kriteria per Pelayanan dan Hitung Pencapaian Pelayanan per Pelayanan
            $pel_by_id = [];
            $pel_by_id2 = [];
            $pel_by_id3 = [];
            foreach($pelayanan as $i => $pel){
                $capaian_berat[$i] = 0;$capaian_tinggi[$i] = 0;
                $capaian_tekanan_darah[$i] = 0;
                $capaian_lila[$i] = 0;
                $capaian_tinggi_puncak_rahim[$i] = 0;
                $capaian_presensi[$i] = 0;
                $capaian_djj[$i] = 0;
                $capaian_skrining[$i] = 0;
                $capaian_imun_tt[$i] = 0;
                $capaian_tes_kehamilan[$i] = 0;
                $capaian_tes_hb[$i] = 0;
                $capaian_tes_gol_dar[$i] = 0;
                $capaian_tes_urin[$i] = 0;
                $capaian_tatalaksana[$i] = 0;
                $capaian_temu[$i] = 0;

                //Berat Tinggi
                if($pel['berat_tinggi'] != null){
                    $bt = explode('_',$pel['berat_tinggi']);
                    if($bt[0] > 45 && $bt[1] > 100){
                        $capaian_berat[$i] = 0.5;
                        $capaian_tinggi[$i] = 0.5;
                    }else{
                    if($bt[0] >45){
                        $capaian_berat[$i] = 0.5;
                    }            
                    if($bt[1] >100){
                        $capaian_tinggi[$i] = 0.5;
                    }}         
                }
                
                // Tekanan Darah
                if($pel['tekanan_darah'] != null){
                    $td = explode('/',$pel['tekanan_darah']);
                    if($td[0] > 90 && $td[1] > 60){
                        $capaian_tekanan_darah[$i] = 1;
                    }            
                }
        
                //Lila
                if($pel['lila'] != null && $pel['lila'] > 2.35 ){
                    $capaian_lila[$i] = 1;            
                }
        
                //Tinggi Puncak Rahim
                if($pel['tinggi_puncak_rahim'] != null){
                    $capaian_tinggi_puncak_rahim[$i] = 1;            
                }
        
                //Presentasi DJJ
                if($pel['presentasi_djj']!= null){
                    $pdjj = explode('_',$pel['presentasi_djj']);
                    if($pdjj[0] != null && $pdjj[1] >120){
                        $capaian_presensi[$i] = 0.5;            
                        $capaian_djj[$i] = 0.5;            
                    }else{
                    if($pdjj[0] != null){
                        $capaian_presensi[$i] = 0.5;
                    }
                    if($pdjj[1] > 120){
                        $capaian_djj[$i] = 0.5;
                    }}
                }
        
                //Imunisasi Tetanus TT
                if($pel['imunisasi_tetanus_tt'] != null){
                    $itt = explode('_',$pel['imunisasi_tetanus_tt']);
                    if($itt[0] == "y" && $itt[1] == "y"){
                        $capaian_skrining[$i] = 0.5;            
                        $capaian_imun_tt[$i] = 0.5;            
                    }else{
                    if($itt[0] == "y"){
                        $capaian_skrining[$i] = 0.5;
                    }
                    if($itt[1] == "y"){
                        $capaian_imun_tt[$i] = 0.5;
                    }}
                }
                
                //Tablet Tambah Darah
                if($pel['tablet_tambah_darah'] != null){
                    $capaian_tablet = $pel['tablet_tambah_darah'];            
                }
        
                //Tes Laboratorium
                if($pel['tes_laboratorium'] != null){
                    $tl = explode('_',$pel['tes_laboratorium']);
                    
                    if($tl[0] == "y"){
                        $capaian_tes_kehamilan[$i] = 0.25;            
                    }
                    if($tl[1] == "y"){
                        $capaian_tes_hb[$i] = 0.25;            
                    }
                    if($tl[2] == "y"){
                        $capaian_tes_gol_dar[$i] = 0.25;            
                    }
                    if($tl[3] == "y"){
                        $capaian_tes_urin[$i] = 0.25;            
                    }
                }
        
                //Tatalaksana
                if($pel['tatalaksana'] == "y"){
                    $capaian_tatalaksana[$i] = 1;            
                }
        
                //Temu Wicara
                if($pel['temu_wicara'] == "y"){
                    $capaian_temu[$i] = 1;            
                }

                if($capaian_tablet >=90){
                    $capaian_tablet_all_periode = 1;
                }
                else{
                    $capaian_tablet_all_periode = 0;
                }
                //Perhitungan per Pelayanan
                if($pel['tanggal_pelayanan'] <= $hayo3bulanlagi && $pel['tanggal_pelayanan'] > $data->tanggal_hamil || $pel['tanggal_pelayanan'] == $data->tanggal_hamil ){
                    $pel_by_id[$pel['id']] = $capaian_berat[$i]+$capaian_tinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                    $capaian_tinggi_puncak_rahim[$i]+$capaian_presensi[$i]+$capaian_djj[$i]+$capaian_skrining[$i]+$capaian_imun_tt[$i]+
                    $capaian_tes_kehamilan[$i]+$capaian_tes_hb[$i]+$capaian_tes_gol_dar[$i]+$capaian_tes_urin[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet_all_periode;
                    // $pel_by_id2[$pel['id']] = 0;
                    // $pel_by_id3[$pel['id']] = 0;
                }else if($pel['tanggal_pelayanan'] <= $hayo6bulanlagi && $pel['tanggal_pelayanan'] > $hayo3bulanlagi){
                    $pel_by_id2[$pel['id']] = $capaian_berat[$i]+$capaian_tinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                    $capaian_tinggi_puncak_rahim[$i]+$capaian_presensi[$i]+$capaian_djj[$i]+$capaian_skrining[$i]+$capaian_imun_tt[$i]+
                    $capaian_tes_kehamilan[$i]+$capaian_tes_hb[$i]+$capaian_tes_gol_dar[$i]+$capaian_tes_urin[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet_all_periode;
                    // $pel_by_id1[$pel['id']] = 0;
                    // $pel_by_id3[$pel['id']] = 0;
                }else if($pel['tanggal_pelayanan'] <= $hayo9bulanlagi && $pel['tanggal_pelayanan'] > $hayo6bulanlagi){
                    $pel_by_id3[$pel['id']] = $capaian_berat[$i]+$capaian_tinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                    $capaian_tinggi_puncak_rahim[$i]+$capaian_presensi[$i]+$capaian_djj[$i]+$capaian_skrining[$i]+$capaian_imun_tt[$i]+
                    $capaian_tes_kehamilan[$i]+$capaian_tes_hb[$i]+$capaian_tes_gol_dar[$i]+$capaian_tes_urin[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet_all_periode;
                    // $pel_by_id2[$pel['id']] = 0;
                    // $pel_by_id1[$pel['id']] = 0;
                }
    
                //End Perhitungan per Pelayanan
            }
            // End Inisialisai Nilai Kriteria per Pelayanan dan Hitung Pencapaian Pelayanan per Pelayanan
            // Inisialisai Nilai Kriteria per Periode
            foreach($pelayanan as $i => $pel){
                if($pel['tanggal_pelayanan'] <= $hayo3bulanlagi && $pel['tanggal_pelayanan'] > $data->tanggal_hamil || $pel['tanggal_pelayanan'] == $data->tanggal_hamil ){
                    if($capaian_berat_periode_1 < 0.5){$capaian_berat_periode_1 = $capaian_berat_periode_1 + $capaian_berat[$i];}
                    if($capaian_tinggi_periode_1 < 0.5){$capaian_tinggi_periode_1 = $capaian_tinggi_periode_1 + $capaian_tinggi[$i];}
                    if($capaian_tekanan_darah_periode_1 < 1){$capaian_tekanan_darah_periode_1 = $capaian_tekanan_darah_periode_1 + $capaian_tekanan_darah[$i];}
                    if($capaian_lila_periode_1 < 1){$capaian_lila_periode_1 = $capaian_lila_periode_1 + $capaian_lila[$i];}
                    if($capaian_tinggi_puncak_rahim_periode_1 < 1){$capaian_tinggi_puncak_rahim_periode_1 = $capaian_tinggi_puncak_rahim_periode_1 + $capaian_tinggi_puncak_rahim[$i];}
                    if($capaian_presensi_periode_1 < 0.5){$capaian_presensi_periode_1 = $capaian_presensi_periode_1 + $capaian_presensi[$i];}
                    if($capaian_djj_periode_1 < 0.5){$capaian_djj_periode_1 = $capaian_djj_periode_1 + $capaian_djj[$i];}
                    if($capaian_imun_tt_periode_1 < 0.5){$capaian_imun_tt_periode_1 = $capaian_imun_tt_periode_1 + $capaian_imun_tt[$i];}
                    if($capaian_skrining_periode_1 < 0.5){$capaian_skrining_periode_1 = $capaian_skrining_periode_1 + $capaian_skrining[$i];}
                    if($capaian_tes_kehamilan_periode_1 < 0.25){$capaian_tes_kehamilan_periode_1 = $capaian_tes_kehamilan_periode_1 + $capaian_tes_kehamilan[$i];}
                    if($capaian_tes_hb_periode_1 < 0.25){$capaian_tes_hb_periode_1 = $capaian_tes_hb_periode_1 + $capaian_tes_hb[$i];}
                    if($capaian_tes_gol_dar_periode_1 < 0.25){$capaian_tes_gol_dar_periode_1 = $capaian_tes_gol_dar_periode_1 + $capaian_tes_gol_dar[$i];}
                    if($capaian_tes_urin_periode_1 < 0.25){$capaian_tes_urin_periode_1 = $capaian_tes_urin_periode_1 + $capaian_tes_urin[$i];}
                    if($capaian_tatalaksana_periode_1 < 1){$capaian_tatalaksana_periode_1 = $capaian_tatalaksana_periode_1 + $capaian_tatalaksana[$i];}
                    if($capaian_temu_periode_1 < 1){$capaian_temu_periode_1 = $capaian_temu_periode_1 + $capaian_temu[$i];}
                }else if($pel['tanggal_pelayanan'] <= $hayo6bulanlagi && $pel['tanggal_pelayanan'] > $hayo3bulanlagi){
                    if($capaian_berat_periode_2 < 0.5){$capaian_berat_periode_2 = $capaian_berat_periode_2 + $capaian_berat[$i];}
                    if($capaian_tinggi_periode_2 < 0.5){$capaian_tinggi_periode_2 = $capaian_tinggi_periode_2 + $capaian_tinggi[$i];}
                    if($capaian_tekanan_darah_periode_2 < 1){$capaian_tekanan_darah_periode_2 = $capaian_tekanan_darah_periode_2 + $capaian_tekanan_darah[$i];}
                    if($capaian_lila_periode_2 < 1){$capaian_lila_periode_2 = $capaian_lila_periode_2 + $capaian_lila[$i];}
                    if($capaian_tinggi_puncak_rahim_periode_2 < 1){$capaian_tinggi_puncak_rahim_periode_2 = $capaian_tinggi_puncak_rahim_periode_2 + $capaian_tinggi_puncak_rahim[$i];}
                    if($capaian_presensi_periode_2 < 0.5){$capaian_presensi_periode_2 = $capaian_presensi_periode_2 + $capaian_presensi[$i];}
                    if($capaian_djj_periode_2 < 0.5){$capaian_djj_periode_2 = $capaian_djj_periode_2 + $capaian_djj[$i];}
                    if($capaian_imun_tt_periode_2 < 0.5){$capaian_imun_tt_periode_2 = $capaian_imun_tt_periode_2 + $capaian_imun_tt[$i];}
                    if($capaian_skrining_periode_2 < 0.5){$capaian_skrining_periode_2 = $capaian_skrining_periode_2 + $capaian_skrining[$i];}
                    if($capaian_tes_kehamilan_periode_2 < 0.25){$capaian_tes_kehamilan_periode_2 = $capaian_tes_kehamilan_periode_2 + $capaian_tes_kehamilan[$i];}
                    if($capaian_tes_hb_periode_2 < 0.25){$capaian_tes_hb_periode_2 = $capaian_tes_hb_periode_2 + $capaian_tes_hb[$i];}
                    if($capaian_tes_gol_dar_periode_2 < 0.25){$capaian_tes_gol_dar_periode_2 = $capaian_tes_gol_dar_periode_2 + $capaian_tes_gol_dar[$i];}
                    if($capaian_tes_urin_periode_2 < 0.25){$capaian_tes_urin_periode_2 = $capaian_tes_urin_periode_2 + $capaian_tes_urin[$i];}
                    if($capaian_tatalaksana_periode_2 < 1){$capaian_tatalaksana_periode_2 = $capaian_tatalaksana_periode_2 + $capaian_tatalaksana[$i];}
                    if($capaian_temu_periode_2 < 1){$capaian_temu_periode_2 = $capaian_temu_periode_2 + $capaian_temu[$i];}
                }else if($pel['tanggal_pelayanan'] <= $hayo9bulanlagi && $pel['tanggal_pelayanan'] > $hayo6bulanlagi){
                    if($capaian_berat_periode_3 < 1){$capaian_berat_periode_3 = $capaian_berat_periode_3 + $capaian_berat[$i];}
                    if($capaian_tinggi_periode_3 < 1){$capaian_tinggi_periode_3 = $capaian_tinggi_periode_3 + $capaian_tinggi[$i];}
                    if($capaian_tekanan_darah_periode_3 < 2){$capaian_tekanan_darah_periode_3 = $capaian_tekanan_darah_periode_3 + $capaian_tekanan_darah[$i];}
                    if($capaian_lila_periode_3 < 2){$capaian_lila_periode_3 = $capaian_lila_periode_3 + $capaian_lila[$i];}
                    if($capaian_tinggi_puncak_rahim_periode_3 < 2){$capaian_tinggi_puncak_rahim_periode_3 = $capaian_tinggi_puncak_rahim_periode_3 + $capaian_tinggi_puncak_rahim[$i];}
                    if($capaian_presensi_periode_3 < 1){$capaian_presensi_periode_3 = $capaian_presensi_periode_3 + $capaian_presensi[$i];}
                    if($capaian_djj_periode_3 < 1){$capaian_djj_periode_3 = $capaian_djj_periode_3 + $capaian_djj[$i];}
                    if($capaian_imun_tt_periode_3 < 1){$capaian_imun_tt_periode_3 = $capaian_imun_tt_periode_3 + $capaian_imun_tt[$i];}
                    if($capaian_skrining_periode_3 < 1){$capaian_skrining_periode_3 = $capaian_skrining_periode_3 + $capaian_skrining[$i];}
                    if($capaian_tes_kehamilan_periode_3 < 0.5){$capaian_tes_kehamilan_periode_3 = $capaian_tes_kehamilan_periode_3 + $capaian_tes_kehamilan[$i];}
                    if($capaian_tes_hb_periode_3 < 0.5){$capaian_tes_hb_periode_3 = $capaian_tes_hb_periode_3 + $capaian_tes_hb[$i];}
                    if($capaian_tes_gol_dar_periode_3 < 0.5){$capaian_tes_gol_dar_periode_3 = $capaian_tes_gol_dar_periode_3 + $capaian_tes_gol_dar[$i];}
                    if($capaian_tes_urin_periode_3 < 0.5){$capaian_tes_urin_periode_3 = $capaian_tes_urin_periode_3 + $capaian_tes_urin[$i];}
                    if($capaian_tatalaksana_periode_3 < 2){$capaian_tatalaksana_periode_3 = $capaian_tatalaksana_periode_3 + $capaian_tatalaksana[$i];}
                    if($capaian_temu_periode_3 < 2){$capaian_temu_periode_3 = $capaian_temu_periode_3 + $capaian_temu[$i];}
                }
                // print "capaian berat tinggi ";
                // print" pelayanan ke";
                // print $i+1;
                // print " periode ";
                // print " ".$pel['jenis_tw']." : ";
                // print $capaian_berat_tinggi[$i]."<br>";   
            }
            if($capaian_tablet >=90){
                $capaian_tablet_all_periode = 1;
            }
            else{
                $capaian_tablet_all_periode = 0;
            }
            // End Inisialisai Nilai Kriteria per Periode

            //Hitung Pencapaian Pelayanan per Periode
            $tw1capaian =   (($capaian_berat_periode_1+$capaian_tinggi_periode_1+$capaian_tekanan_darah_periode_1+$capaian_lila_periode_1+
                            $capaian_tinggi_puncak_rahim_periode_1+$capaian_presensi_periode_1+$capaian_djj_periode_1+$capaian_skrining_periode_1+$capaian_imun_tt_periode_1+
                            $capaian_tes_kehamilan_periode_1+$capaian_tes_hb_periode_1+$capaian_tes_gol_dar_periode_1+$capaian_tes_urin_periode_1+$capaian_tatalaksana_periode_1+
                            $capaian_temu_periode_1+$capaian_tablet_all_periode) * 100 ) / 10;
            $tw2capaian =   (($capaian_berat_periode_2+$capaian_tinggi_periode_2+$capaian_tekanan_darah_periode_2+$capaian_lila_periode_2+
                            $capaian_tinggi_puncak_rahim_periode_2+$capaian_presensi_periode_2+$capaian_djj_periode_2+$capaian_skrining_periode_2+$capaian_imun_tt_periode_2+
                            $capaian_tes_kehamilan_periode_2+$capaian_tes_hb_periode_2+$capaian_tes_gol_dar_periode_2+$capaian_tes_urin_periode_2+$capaian_tatalaksana_periode_2+
                            $capaian_temu_periode_2+$capaian_tablet_all_periode) * 100 ) / 10;
            $tw3capaian =   (($capaian_berat_periode_3+$capaian_tinggi_periode_3+$capaian_tekanan_darah_periode_3+$capaian_lila_periode_3+
                            $capaian_tinggi_puncak_rahim_periode_3+$capaian_presensi_periode_3+$capaian_djj_periode_3+$capaian_skrining_periode_3+$capaian_imun_tt_periode_3+
                            $capaian_tes_kehamilan_periode_3+$capaian_tes_hb_periode_3+$capaian_tes_gol_dar_periode_3+$capaian_tes_urin_periode_3+$capaian_tatalaksana_periode_3+
                            $capaian_temu_periode_3+$capaian_tablet_all_periode) * 100 ) / 20;
            //End Hitung Pencapaian Pelayanan per Periode

            //Hitung Pencapaian Pelayanan per Total
            $super_pel_total = ($capaian_berat_periode_1+$capaian_tinggi_periode_1+$capaian_tekanan_darah_periode_1+$capaian_lila_periode_1+
            $capaian_tinggi_puncak_rahim_periode_1+$capaian_presensi_periode_1+$capaian_djj_periode_1+$capaian_skrining_periode_1+$capaian_imun_tt_periode_1+
            $capaian_tes_kehamilan_periode_1+$capaian_tes_hb_periode_1+$capaian_tes_gol_dar_periode_1+$capaian_tes_urin_periode_1+$capaian_tatalaksana_periode_1+
            $capaian_temu_periode_1+$capaian_tablet_all_periode) +  
            ($capaian_berat_periode_2+$capaian_tinggi_periode_2+$capaian_tekanan_darah_periode_2+$capaian_lila_periode_2+
            $capaian_tinggi_puncak_rahim_periode_2+$capaian_presensi_periode_2+$capaian_djj_periode_2+$capaian_skrining_periode_2+$capaian_imun_tt_periode_2+
            $capaian_tes_kehamilan_periode_2+$capaian_tes_hb_periode_2+$capaian_tes_gol_dar_periode_2+$capaian_tes_urin_periode_2+$capaian_tatalaksana_periode_2+
            $capaian_temu_periode_2+$capaian_tablet_all_periode) +
            ($capaian_berat_periode_3+$capaian_tinggi_periode_3+$capaian_tekanan_darah_periode_3+$capaian_lila_periode_3+
            $capaian_tinggi_puncak_rahim_periode_3+$capaian_presensi_periode_3+$capaian_djj_periode_3+$capaian_skrining_periode_3+$capaian_imun_tt_periode_3+
            $capaian_tes_kehamilan_periode_3+$capaian_tes_hb_periode_3+$capaian_tes_gol_dar_periode_3+$capaian_tes_urin_periode_3+$capaian_tatalaksana_periode_3+
            $capaian_temu_periode_3+$capaian_tablet_all_periode);
            $totalcapaian = ($tw1capaian+$tw2capaian+$tw3capaian)/3;
            // $totalcapaian[$index] = "(".$capaian_periode1[$index]."+".$capaian_periode2[$index]."+".$capaian_periode3[$index].")/3";
            // $totalcapaian[$index] = $capaian_periode1[$index];
            //End Hitung Pencapaian Pelayanan per Total

        
        // print "Pel Total : ".$pel_total;
        // print "<br><br>";
        // print "Pel Total2 : ".$pel_total2;
        // print "<br><br>";
        // print "Pel Total3 : ".$pel_total3;
        // print "<br><br>";
        // print "Total Capaian : ".$totalcapaian;
        // print "<br><br>";


        $kriteriaibuhamil = [];
        $nikcaibuhamil = [];
        $warga_all = Warga::all();
        foreach($warga_all as $i => $val){
            if($val['jenis_kelamin'] == "P"){
                $usia = IbuHamilController::Umur($val['tgl_lahir']);
                if($usia >= 22){
                    $data = IbuHamil::where('nik', $val['nik'])->where('status_kehamilan','Tahap Pelayanan')->first();
                    if(!$data){
                        $tglLahircaibuhamil[$i] = $val['tgl_lahir'];
                        $statusperkawinan[$i] = $val['status_perkawinan'];
                        $kriteriaibuhamil[$i] = $val['nama'];
                        $nikcaibuhamil[$i] = $val['nik'];
                        $gabungan[$i] = $nikcaibuhamil[$i]."_".$kriteriaibuhamil[$i]."_".$tglLahircaibuhamil[$i]."_".$statusperkawinan[$i];
                    }
                }
            }
        }
        
        $data = IbuHamil::where('id',$id)->first();
        $nik = $data->nik;
        $warga = Warga::where('nik', $data->nik)->first();
        $nama = $warga->nama;
        $status_kawin = $warga->status_perkawinan;
        $tgl_lahir = $warga->tgl_lahir;
        $gab = $nik."_".$nama."_".$tgl_lahir."_".$status_kawin;
        return view('spm.ibuhamil.ubah', compact('gab','gabungan','$tgl_lahir','status_kawin','tglLahircaibuhamil','statusperkawinan','data','nik','nama','totalcapaian','kriteriaibuhamil','nikcaibuhamil'));        
    }

    public function tambah(){
        date_default_timezone_set('Asia/Jakarta');
        $kriteriaibuhamil = [];
        $nikcaibuhamil = [];
        $list_nik_ca_suami = [];
        $list_nama_ca_suami = [];
        $warga = Warga::all();
        $urutan = 0;
        foreach($warga as $i => $val){
            if($val['jenis_kelamin'] == "P"){
                $usia = IbuHamilController::Umur($val['tgl_lahir']);
                if($usia >= 22){
                    $data = IbuHamil::where('nik', $val['nik'])->first();
                    if($data){
                        if($data['status_kehamilan'] != "Tahap Pelayanan"){
                            $ibuhamil = IbuHamil::where('nik', $val['nik'])->get();
                            foreach($ibuhamil as $i => $ih){
                                $selisihmelahirkan = IbuHamilController::Umur2($ih['tanggal_bersalin'],date("Y/m/d/h:i:s"));                                
                            }
                            if($selisihmelahirkan['months'] >= 7){
                                $kriteriaibuhamil[$urutan] = $val['nama'];
                                $nikcaibuhamil[$urutan] = $val['nik'];
                                $list_no_telp[$urutan] = $val['no_telp'];
                                $list_minG[$urutan] = count($ibuhamil)+1;
                                $urutan++;
                            }
                        }
                    }
                    else{
                        $kriteriaibuhamil[$urutan] = $val['nama'];
                        $nikcaibuhamil[$urutan] = $val['nik'];
                        $list_no_telp[$urutan] = $val['no_telp'];
                        $list_minG[$urutan] = 1;
                        $urutan++;
                    }
                }
            }
            // else if($val['jenis_kelamin'] == "L"){
            //     $usia = IbuHamilController::Umur($val['tgl_lahir']);
            //     if($usia >= 22){
            //         $list_nik_ca_suami[$i] = $val['nik'];
            //         $list_nama_ca_suami[$i] = $val['nama'];
            //     }
            // }
        }
        return view('spm.ibuhamil.tambah',compact('list_minG','kriteriaibuhamil','nikcaibuhamil','list_nik_ca_suami','list_nama_ca_suami','list_no_telp'));
    }

    public function save(Request $request){
        $a = date('Y-m-d', strtotime($request->tanggal_hamil));
        $a = $a." ".$request->waktu_hpht.":00";
        $data = New IbuHamil();
        $data->nik = explode('_',$request->nik)[2];
        $data->tanggal_hamil = $a;
        $data->status_kehamilan = "Tahap Pelayanan";
        $data->created_by = $request->created_by;
        $data->nama_suami = $request->nama_suami;
        $data->kode_riwayat_kehamilan = "G".$request->g."P".$request->p."A".$request->a;
        $warga = Warga::where('nik',explode('_',$request->nik)[2])->first();
        $warga->no_telp = $request->no_telp;
        if($data->save()){
            if($warga->save()){
                return Redirect::route('data-ibu-hamil')->with('success','Berhasil Daftar Ibu Hamil');
            }
            else{
                return Redirect::route('data-ibu-hamil')->with('success','Berhasil Daftar Ibu Hamil namun No Telp Gagal Diperbaharui');
            }
        }
        else{
            return Redirect::route('tambah-ibu-hamil')->with('danger','Gagal Daftar Ibu Hamil');
        }
    }

    public function edit(Request $request){
        if($request->status_hamil == "Tahap Pelayanan"){
            $request->status_hamil= $request->status_hamil;
        }
        else{
            if($request->totalcapaian == 100){
                $request->status_hamil= "Sesuai SPM";
            }
            else{
                $request->status_hamil= "Tidak Sesuai SPM";
            }
        }
        $ibuhamil = IbuHamil::where('id',$request->id)->first();
        $ibuhamil->nik = $request->nik;
        $ibuhamil->status_kehamilan = $request->status_hamil;
        
        if($request->tanggal_hamil == $ibuhamil->tanggal_hamil){
            // print $ibuhamil; 
            if($ibuhamil->save()){
                return Redirect::route('data-ibu-hamil')->with('success','Berhasil Ubah Ibu Hamil');
            }
            else{
                return Redirect::route('ubah-ibu-hamil',$ibuhamil->id)->with('danger','Gagal Ubah Ibu Hamil');
            }
        }
        else{
            $pelayanan = Pelayanan::where('table_spm', 'ibu_hamil')->where('id_spm',$ibuhamil->id)->get();
            
            $hayo3bulanlagi = date('Y-m-d', strtotime('+3 month', strtotime($request->tanggal_hamil)));
            $hayo6bulanlagi = date('Y-m-d', strtotime('+6 month', strtotime($request->tanggal_hamil)));
            $hayo9bulanlagi = date('Y-m-d', strtotime('+9 month', strtotime($request->tanggal_hamil)));
        
            // print "Ibu Hamil : ".$ibuhamil."<br>"; 
            // print "Tgl Hamil : ".$request->tanggal_hamil."<br>"; 

            // print $pelayanan;
            foreach($pelayanan as $i => $val){
                // print "<br>";
                if($val['tanggal_pelayanan'] <= $hayo3bulanlagi && $val['tanggal_pelayanan'] > $request->tanggal_hamil || $val['tanggal_pelayanan'] == $request->tanggal_hamil ){
                    $val['jenis_tw'] = 1;
                }else if($val['tanggal_pelayanan'] <= $hayo6bulanlagi && $val['tanggal_pelayanan'] > $hayo3bulanlagi){
                    $val['jenis_tw'] = 2;
                }else if($val['tanggal_pelayanan'] <= $hayo9bulanlagi && $val['tanggal_pelayanan'] > $hayo6bulanlagi){
                    $val['jenis_tw'] = 3;
                }else if($val['tanggal_pelayanan'] <= $request->tanggal_hamil){
                    $val['jenis_tw'] = 0;
                }else if($val['tanggal_pelayanan'] > $hayo9bulanlagi){
                    $val['jenis_tw'] = 3;
                }    
                // print "Pelayanan ".$i." : <br> Tanggal Pelayanan : ".$val['tanggal_pelayanan']."<br> Jenis tw :".$val['jenis_tw']."<br>";
                $val->save();
            }

            $ibuhamil->tanggal_hamil = $request->tanggal_hamil;

            if($ibuhamil->save()){
                return Redirect::route('data-ibu-hamil')->with('success','Berhasil Ubah Ibu Hamil');
            }
            else{
                return Redirect::route('ubah-ibu-hamil')->with('danger','Gagal Ubah Ibu Hamil');
            }
        }
    }

    public function indexPelayanan(){
        $pelayanan = Pelayanan::where('table_spm', 'ibu_hamil')->get();
        $pel = [];$pel2 = [];$pel3 =[];
        foreach($pelayanan as $i => $data_pelayanan){
            if($data_pelayanan['jenis_tw'] == 1){
                $cek[$i]= IbuHamilController::Cek_Capaian($data_pelayanan['berat_tinggi'],
                $data_pelayanan['tekanan_darah'], $data_pelayanan['lila'],
                $data_pelayanan['tinggi_puncak_rahim'], $data_pelayanan['presentasi_djj'],
                $data_pelayanan['imunisasi_tetanus_tt'], $data_pelayanan['tablet_tambah_darah'],
                $data_pelayanan['tes_laboratorium'], $data_pelayanan['tatalaksana'],
                $data_pelayanan['temu_wicara'], $i);
            }
            elseif($data_pelayanan['jenis_tw'] == 2){
                $cek2[$i] = IbuHamilController::Cek_Capaian($data_pelayanan['berat_tinggi'],
                $data_pelayanan['tekanan_darah'], $data_pelayanan['lila'],
                $data_pelayanan['tinggi_puncak_rahim'], $data_pelayanan['presentasi_djj'],
                $data_pelayanan['imunisasi_tetanus_tt'], $data_pelayanan['tablet_tambah_darah'],
                $data_pelayanan['tes_laboratorium'], $data_pelayanan['tatalaksana'],
                $data_pelayanan['temu_wicara'], $i);

            }
            elseif($data_pelayanan['jenis_tw'] == 3){
                $cek3[$i] = IbuHamilController::Cek_Capaian($data_pelayanan['berat_tinggi'],
                $data_pelayanan['tekanan_darah'], $data_pelayanan['lila'],
                $data_pelayanan['tinggi_puncak_rahim'], $data_pelayanan['presentasi_djj'],
                $data_pelayanan['imunisasi_tetanus_tt'], $data_pelayanan['tablet_tambah_darah'],
                $data_pelayanan['tes_laboratorium'], $data_pelayanan['tatalaksana'],
                $data_pelayanan['temu_wicara'], $i);
            }
            $pel[$i] =0;
            $pel2[$i] =0;
            $pel3[$i] =0;
        }
        for($i=0;$i<10;$i++){
            $hitung = ""; $hitung2 = ""; $hitung3 = "";                                                
            $tablet =0;
            $tablet2 =0;
            $tablet3 =0;
            foreach($pelayanan as $j => $data_pelayanan){
                if($data_pelayanan['jenis_tw'] == 1){
                    $hitung .= $cek[$j][$i][$j];
                    // print $cek[$j][$i][$j];
                }
                if($data_pelayanan['jenis_tw'] == 2){
                    $hitung2 .= $cek2[$j][$i][$j]; 
                    // print  $cek2[$j][$i][$j]; 
                }
                if($data_pelayanan['jenis_tw'] == 3){
                    $hitung3 .= $cek3[$j][$i][$j];
                    // print  $cek3[$j][$i][$j];
                }
            }
            $pos = strpos($hitung, "x");
            if($pos === false){
                $pos2 = strpos($hitung, "y");
                if($pos2 === false){
                }else{
                    $pos3 = strpos($hitung, "z");
                    if($pos3 === false){
                    }
                    else{
                        if($pos2 > $pos3){
                            $pel[$pos2]++;  
                        }
                        else{
                            $pel[$pos3]++;
                        }
                    }
                }
            }else{
                $pel[$pos]++;
         
            }
            $pos = strpos($hitung2, "x");
            if($pos === false){
                $pos2 = strpos($hitung2, "y");
                if($pos2 === false){
                }else{
                    $pos3 = strpos($hitung2, "z");
                    if($pos3 === false){
                    }
                    else{
                        if($pos2 > $pos3){
                            $pel2[$pos2]++; 
                        }
                        else{
                            $pel2[$pos3]++;
                        }
                    }
                }
            }else{
                $pel2[$pos]++;
            }
            $pos = strpos($hitung3, "x");
            if($pos === false){
                $pos2 = strpos($hitung3, "y");
                if($pos2 === false){
                }else{
                    $pos3 = strpos($hitung3, "z");
                    if($pos3 === false){
                    }
                    else{
                        if($pos2 > $pos3){
                            $pel3[$pos2]++; 
                        }
                        else{
                            $pel3[$pos3]++;
                        }
                    }
                }
            }else{
                $pel3[$pos]++;
            }              
        }
        $pel_total =0;
        $pel_total2 =0;
        $pel_total3 =0;
        $pel_by_id = [];
        $pel_by_id2 = [];
        $pel_by_id3 = [];
        foreach($pelayanan as $i => $data_pelayanan){
            if($data_pelayanan['jenis_tw'] == 1){
                if(intval($cek[$i][6][$i]) > 0 ){
                    $tablet += intval($cek[$i][6][$i]);
                    if($tablet == 90){
                        $pel[$i]++;
                    }
                }
                $pel_total += $pel[$i];
                $pel_by_id[$data_pelayanan->id] = $pel[$i];
            }
            else if($data_pelayanan['jenis_tw'] == 2){
                if(intval($cek2[$i][6][$i]) > 0 ){
                    $tablet2 += intval($cek2[$i][6][$i]);
                    if($tablet2 == 90){
                        $pel2[$i]++;
                    }
                }
                $pel_total2 += $pel2[$i];
                $pel_by_id2[$data_pelayanan->id] = $pel2[$i];
            }
            else if($data_pelayanan['jenis_tw'] == 3){
                if(intval($cek3[$i][6][$i]) > 0 ){
                    $tablet3 += intval($cek3[$i][6][$i]);
                    if($tablet3 == 90){
                        $pel3[$i]++;
                    }
                }
                $pel_total3 += $pel3[$i];
                $pel_by_id3[$data_pelayanan->id] = $pel3[$i];
            }
        }

        foreach($pelayanan as $index => $data_pelayanan){
            $temp_ibuhamil = IbuHamil::where('id',$data_pelayanan['id_spm'])->first();
            $ibuhamil[$index] = Warga::where('nik',$temp_ibuhamil['nik'])->first();
            // print "Pelayanan: <br>";
            // print "Kode Pelayanan : ".$data_pelayanan->id_pelayanan."<br>";
            // print "Nama Ibu Hamil : ".$ibuhamil->nama."<br>";
            // print "Tanggal Pelayanan : ".$data_pelayanan->tanggal_pelayanan."<br>";
            // print "Fasilitas Kesehatan : ".explode('_',$data_pelayanan->lokasi)[0]."<br>";
            // print "Informasi Fasilitas : ".explode('_',$data_pelayanan->lokasi)[1]."<br>";
            // print "Tenaga Kesehatan : ".explode('_',$data_pelayanan->tenaga_kerja)[0]."<br>";
            // print "Nama Tenaga Kesehatan : ".explode('_',$data_pelayanan->tenaga_kerja)[1]."<br>";
            // if($data_pelayanan['jenis_tw'] == 1){
            //     print "Capaian : ".$pel_by_id[$data_pelayanan->id_pelayanan]."<br>";    
            // }
            // if($data_pelayanan['jenis_tw'] == 2){
            //     print "Capaian : ".$pel_by_id2[$data_pelayanan->id_pelayanan]."<br>";    
            // }
            // if($data_pelayanan['jenis_tw'] == 3){
            //     print "Capaian : ".$pel_by_id3[$data_pelayanan->id_pelayanan]."<br>";    
            // }   
        }
        return view('spm.ibuhamil.pelayanan.data',compact('pelayanan', 'ibuhamil','pel_by_id','pel_by_id2','pel_by_id3'));
    }

    public function detailPelayanan($id){
        $pelayanan = Pelayanan::where('id', $id)->first();
        $ibuhamil = IbuHamil::where('id',$pelayanan['id_spm'])->first();
        $data_nik = Warga::where('nik',$ibuhamil['nik'])->first();
        // print $pelayanan;
        if($pelayanan->lokasi == null){$fasilitas = "-";$lokasi = "-";}else{
        if(explode('_',$pelayanan->lokasi)[0] != null){$fasilitas = explode('_',$pelayanan->lokasi)[0];} else{$fasilitas = "-";}
        if(explode('_',$pelayanan->lokasi)[1] != null){$lokasi = explode('_',$pelayanan->lokasi)[1];} else{$lokasi = "-";}}

        if($pelayanan->tenaga_kerja == null){$tenaga_kerja = "-";$nama_stk = "-";}else{
        if(explode('_',$pelayanan->tenaga_kerja)[0] != null){$tenaga_kerja = explode('_',$pelayanan->tenaga_kerja)[0];}else{$tenaga_kerja = "-";}
        if(explode('_',$pelayanan->tenaga_kerja)[1] != null){$nama_stk = explode('_',$pelayanan->tenaga_kerja)[1];}else{$nama_stk = "-";}}
        
        if($pelayanan->berat_tinggi == null){$berat = "-";$tinggi = "-";}else{
        if(explode('_',$pelayanan->berat_tinggi)[0] != null){$berat = explode('_',$pelayanan->berat_tinggi)[0];}else{$berat = "-";}
        if(explode('_',$pelayanan->berat_tinggi)[1] != null){$tinggi = explode('_',$pelayanan->berat_tinggi)[1];}else{$tinggi = "-";}}
        
        if($pelayanan->tekanan_darah == null){$sistole = "-";$diastole = "-";}else{
        if(explode('/',$pelayanan->tekanan_darah)[0] != null){$sistole = explode('/',$pelayanan->tekanan_darah)[0];}else{$sistole = "-";}
        if(explode('/',$pelayanan->tekanan_darah)[1] != null){$diastole = explode('/',$pelayanan->tekanan_darah)[1];}else{$diastole = "-";}}
        
        if($pelayanan->lila == null){$lila = "-";$komalila = "-";}else{
        if(explode(',',$pelayanan->lila)[0] != null){$lila = explode(',',$pelayanan->lila)[0];}else{$lila = "-";}        
        if(explode(',',$pelayanan->lila)[1] != null){$komalila = explode(',',$pelayanan->lila)[1];}else{$komalila = "-";}}
        
        if($pelayanan->tinggi_puncak_rahim == null){$tprahim = "-";$satuantprahim = "-";}else{
        if(explode(' ',$pelayanan->tinggi_puncak_rahim)[0] != null){$tprahim = explode(' ',$pelayanan->tinggi_puncak_rahim)[0];}else{$tprahim = "-";}
        if(explode(' ',$pelayanan->tinggi_puncak_rahim)[1] != null){$satuantprahim = explode(' ',$pelayanan->tinggi_puncak_rahim)[1];}else{$satuantprahim = "-";}}
        
        if($pelayanan->presentasi_djj == null){$presensi = "-";$djj = "-";}else{
        if(explode('_',$pelayanan->presentasi_djj)[0] != null){$presensi = explode('_',$pelayanan->presentasi_djj)[0];}else{$presensi = "-";}
        if(explode('_',$pelayanan->presentasi_djj)[1] != null){$djj = explode('_',$pelayanan->presentasi_djj)[1];}else{$djj = "-";}}
        
        if($pelayanan->imunisasi_tetanus_tt == null){$imun = "-";$toksoid = "-";}else{
        if(explode('_',$pelayanan->imunisasi_tetanus_tt)[0] != null){$imun = explode('_',$pelayanan->imunisasi_tetanus_tt)[0];}else{$imun = "-";}
        if(explode('_',$pelayanan->imunisasi_tetanus_tt)[1] != null){$toksoid = explode('_',$pelayanan->imunisasi_tetanus_tt)[1];}else{$toksoid = "-";}}
        
        if($pelayanan->tes_laboratorium == null){$tes_hamil = "-";$tes_hb = "-";$tes_urin = "-";$tes_gd = "-";$tes_hiv = "-";$tes_hbsag = "-";}else{
        if(explode('_',$pelayanan->tes_laboratorium)[0] != null){$tes_hamil = explode('_',$pelayanan->tes_laboratorium)[0];}else{$tes_hamil = "-";}
        if(explode('_',$pelayanan->tes_laboratorium)[1] != null){$tes_hb = explode('_',$pelayanan->tes_laboratorium)[1];}else{$tes_hb = "-";}
        if(explode('_',$pelayanan->tes_laboratorium)[2] != null){$tes_gd = explode('_',$pelayanan->tes_laboratorium)[2];}else{$tes_gd = "-";}
        if(explode('_',$pelayanan->tes_laboratorium)[3] != null){$tes_urin = explode('_',$pelayanan->tes_laboratorium)[3];}else{$tes_urin = "-";}
        if(explode('_',$pelayanan->tes_laboratorium)[4] != null){$tes_hiv = explode('_',$pelayanan->tes_laboratorium)[4];}else{$tes_hiv = "-";}
        if(explode('_',$pelayanan->tes_laboratorium)[5] != null){$tes_hbsag = explode('_',$pelayanan->tes_laboratorium)[5];}else{$tes_hbsag = "-";}}

        if($pelayanan->tablet_tambah_darah == null){$tablet = "-";}else{$tablet = $pelayanan->tablet_tambah_darah;}

        if($pelayanan->tatalaksana == null){$tatalaksana = "-";}else{$tatalaksana = $pelayanan->tatalaksana;}
        if($pelayanan->temu_wicara == null){$temu_wicara = "-";}else{$temu_wicara = $pelayanan->temu_wicara;}

        return view('spm.ibuhamil.pelayanan.detail',compact('ibuhamil','data_nik','pelayanan','fasilitas','lokasi', 
        'tenaga_kerja','nama_stk','berat','tinggi','sistole','diastole','lila','komalila','tprahim',
        'satuantprahim','presensi','djj','imun','toksoid','tes_hamil','tes_hb','tes_gd','tes_urin','tes_hiv','tes_hbsag','tablet','tatalaksana','temu_wicara')); 
    }

    public function ubahPelayanan($id){
        $pelayanan = Pelayanan::where('id', $id)->first();
        $ibuhamil = IbuHamil::where('id',$pelayanan->id_spm)->first();
        $pelayananallibuhamil = $pelayanan::where('table_spm','ibu_hamil')->where('id_spm',$ibuhamil->id)->get();
        $tgl_pel = [];
        foreach($pelayananallibuhamil as $j => $pel){
            $tgl_pel[$j] = $pel['tanggal_pelayanan'];
        }
        $data_nik = Warga::where('nik',$ibuhamil->nik)->first();
        // print $pelayanan;
        if($pelayanan->lokasi == null){$fasilitas = "-";$lokasi = "-";}else{
        if(explode('_',$pelayanan->lokasi)[0] != null){$fasilitas = explode('_',$pelayanan->lokasi)[0];} else{$fasilitas = "-";}
        if(explode('_',$pelayanan->lokasi)[1] != null){$lokasi = explode('_',$pelayanan->lokasi)[1];} else{$lokasi = "-";}}

        if($pelayanan->tenaga_kerja == null){$tenaga_kerja = "-";$nama_stk = "-";}else{
        if(explode('_',$pelayanan->tenaga_kerja)[0] != null){$tenaga_kerja = explode('_',$pelayanan->tenaga_kerja)[0];}else{$tenaga_kerja = "-";}
        if(explode('_',$pelayanan->tenaga_kerja)[1] != null){$nama_stk = explode('_',$pelayanan->tenaga_kerja)[1];}else{$nama_stk = "-";}}
        
        if($pelayanan->berat_tinggi == null){$berat = "-";$tinggi = "-";}else{
        if(explode('_',$pelayanan->berat_tinggi)[0] != null){$berat = explode('_',$pelayanan->berat_tinggi)[0];}else{$berat = "-";}
        if(explode('_',$pelayanan->berat_tinggi)[1] != null){$tinggi = explode('_',$pelayanan->berat_tinggi)[1];}else{$tinggi = "-";}}
        
        if($pelayanan->tekanan_darah == null){$sistole = "-";$diastole = "-";}else{
        if(explode('/',$pelayanan->tekanan_darah)[0] != null){$sistole = explode('/',$pelayanan->tekanan_darah)[0];}else{$sistole = "-";}
        if(explode('/',$pelayanan->tekanan_darah)[1] != null){$diastole = explode('/',$pelayanan->tekanan_darah)[1];}else{$diastole = "-";}}
        
        if($pelayanan->lila == null){$lila = "-";$komalila = "-";}else{
        if(explode(',',$pelayanan->lila)[0] != null){$lila = explode(',',$pelayanan->lila)[0];}else{$lila = "-";}        
        if(explode(',',$pelayanan->lila)[1] != null){$komalila = explode(',',$pelayanan->lila)[1];}else{$komalila = "-";}}
        
        if($pelayanan->tinggi_puncak_rahim == null){$tprahim = "-";$satuantprahim = "-";}else{
        if(explode(' ',$pelayanan->tinggi_puncak_rahim)[0] != null){$tprahim = explode(' ',$pelayanan->tinggi_puncak_rahim)[0];}else{$tprahim = "-";}
        if(explode(' ',$pelayanan->tinggi_puncak_rahim)[1] != null){$satuantprahim = explode(' ',$pelayanan->tinggi_puncak_rahim)[1];}else{$satuantprahim = "-";}}
        
        if($pelayanan->presentasi_djj == null){$presensi = "-";$djj = "-";}else{
        if(explode('_',$pelayanan->presentasi_djj)[0] != null){$presensi = explode('_',$pelayanan->presentasi_djj)[0];}else{$presensi = "-";}
        if(explode('_',$pelayanan->presentasi_djj)[1] != null){$djj = explode('_',$pelayanan->presentasi_djj)[1];}else{$djj = "-";}}
        
        if($pelayanan->imunisasi_tetanus_tt == null){$imun = "-";$toksoid = "-";}else{
        if(explode('_',$pelayanan->imunisasi_tetanus_tt)[0] != null){$imun = explode('_',$pelayanan->imunisasi_tetanus_tt)[0];}else{$imun = "-";}
        if(explode('_',$pelayanan->imunisasi_tetanus_tt)[1] != null){$toksoid = explode('_',$pelayanan->imunisasi_tetanus_tt)[1];}else{$toksoid = "-";}}
        
        if($pelayanan->tes_laboratorium == null){$tes_hamil = "-";$tes_hb = "-";$tes_urin = "-";$tes_gd = "-";$tes_hiv = "-";$tes_hbsag = "-";}else{
        if(explode('_',$pelayanan->tes_laboratorium)[0] != null){$tes_hamil = explode('_',$pelayanan->tes_laboratorium)[0];}else{$tes_hamil = "-";}
        if(explode('_',$pelayanan->tes_laboratorium)[1] != null){$tes_hb = explode('_',$pelayanan->tes_laboratorium)[1];}else{$tes_hb = "-";}
        if(explode('_',$pelayanan->tes_laboratorium)[2] != null){$tes_gd = explode('_',$pelayanan->tes_laboratorium)[2];}else{$tes_gd = "-";}
        if(explode('_',$pelayanan->tes_laboratorium)[3] != null){$tes_urin = explode('_',$pelayanan->tes_laboratorium)[3];}else{$tes_urin = "-";}
        if(explode('_',$pelayanan->tes_laboratorium)[4] != null){$tes_hiv = explode('_',$pelayanan->tes_laboratorium)[4];}else{$tes_hiv = "-";}
        if(explode('_',$pelayanan->tes_laboratorium)[5] != null){$tes_hbsag = explode('_',$pelayanan->tes_laboratorium)[5];}else{$tes_hbsag = "-";}}

        if($pelayanan->tablet_tambah_darah == null){$tablet = "-";}else{$tablet = $pelayanan->tablet_tambah_darah;}

        if($pelayanan->tatalaksana == null){$tatalaksana = "-";}else{$tatalaksana = $pelayanan->tatalaksana;}
        if($pelayanan->temu_wicara == null){$temu_wicara = "-";}else{$temu_wicara = $pelayanan->temu_wicara;}

        return view('spm.ibuhamil.pelayanan.ubah',compact('ibuhamil','data_nik','pelayanan','fasilitas','lokasi', 
        'tenaga_kerja','nama_stk','berat','tinggi','sistole','diastole','lila','komalila','tprahim',
        'satuantprahim','presensi','djj','imun','toksoid','tes_hamil','tes_hb','tes_gd','tes_urin','tes_hiv','tes_hbsag','tablet','tatalaksana','temu_wicara','tgl_pel')); 
    }
    public function inputnikPelayanan(){
        if(!empty(Session::get('withnik'))){
            Session::forget('withnik');
            return view('spm.ibuhamil.pelayanan.masukan_nik');  
        }
        else{
            return view('spm.ibuhamil.pelayanan.masukan_nik');  
        }
    }
    
    public function inputPelayanan(Request $request){
        $mitra_all = MitraKesehatan::all();
        $tenaga_all = TenagaKesehatan::all();
        $spesialis_all = Spesialis::all();
        $tgl_pel = [];
        Session::put('withnik',$request->nik);
        $nik = Session::get('withnik');
        $datanik = Warga::where('nik', $nik)->first();
        if(count($datanik) > 0 ){
            if($datanik['jenis_kelamin'] == "P"){
                $usia = IbuHamilController::Umur($datanik['tgl_lahir']);
                if($usia >= 22){
                    $data = IbuHamil::where('nik', $nik)->where('status_kehamilan','Tahap Pelayanan')->first();
                    if(count($data) > 0){
                        $pelayanan = Pelayanan::where('table_spm', 'ibu_hamil')->where('id_spm',$data->id)->get();
        
                        $hayo3bulanlagi = date('Y-m-d', strtotime('+3 month', strtotime($data->tanggal_hamil)));
                        $hayo6bulanlagi = date('Y-m-d', strtotime('+6 month', strtotime($data->tanggal_hamil)));
                        $hayo9bulanlagi = date('Y-m-d', strtotime('+9 month', strtotime($data->tanggal_hamil)));
                        
                        $capaian_berat_periode_1 =0;$capaian_berat_periode_2 =0;$capaian_berat_periode_3 =0;
                        $capaian_tinggi_periode_1 =0;$capaian_tinggi_periode_2 =0;$capaian_tinggi_periode_3 =0;
                        $capaian_berattinggi_periode_1 =0;$capaian_berattinggi_periode_2 =0;$capaian_berattinggi_periode_3 =0;
                        $capaian_tekanan_darah_periode_1 =0;$capaian_tekanan_darah_periode_2 =0;$capaian_tekanan_darah_periode_3 =0;
                        $capaian_lila_periode_1 =0;$capaian_lila_periode_2 =0;$capaian_lila_periode_3 =0;
                        $capaian_tinggi_puncak_rahim_periode_1 =0;$capaian_tinggi_puncak_rahim_periode_2 =0;$capaian_tinggi_puncak_rahim_periode_3 =0;
                        $capaian_presensi_periode_1 =0;$capaian_presensi_periode_2 =0;$capaian_presensi_periode_3 =0;
                        $capaian_djj_periode_1 =0;$capaian_djj_periode_2 =0;$capaian_djj_periode_3 =0;
                        $capaian_presensidjj_periode_1 =0;$capaian_presensidjj_periode_2 =0;$capaian_presensidjj_periode_3 =0;
                        $capaian_skrining_periode_1 =0;$capaian_skrining_periode_2 =0;$capaian_skrining_periode_3 =0;
                        $capaian_imun_tt_periode_1 =0;$capaian_imun_tt_periode_2 =0;$capaian_imun_tt_periode_3 =0;
                        $capaian_imunisasi_periode_1 =0;$capaian_imunisasi_periode_2 =0;$capaian_imunisasi_periode_3 =0;
                        $capaian_tes_kehamilan_periode_1 = 0;$capaian_tes_kehamilan_periode_2 =0;$capaian_tes_kehamilan_periode_3 =0;
                        $capaian_tes_hb_periode_1 = 0;$capaian_tes_hb_periode_2= 0;$capaian_tes_hb_periode_3 = 0;
                        $capaian_tes_gol_dar_periode_1 = 0;$capaian_tes_gol_dar_periode_2 = 0;$capaian_tes_gol_dar_periode_3 = 0;
                        $capaian_tes_urin_periode_1 = 0;$capaian_tes_urin_periode_2 = 0;$capaian_tes_urin_periode_3 = 0;        
                        $capaian_teslabo_periode_1 = 0;$capaian_teslabo_periode_2 = 0;$capaian_teslabo_periode_3 = 0;        
                        $capaian_tatalaksana_periode_1 =0;$capaian_tatalaksana_periode_2 =0;$capaian_tatalaksana_periode_3 =0;
                        $capaian_temu_periode_1 =0;$capaian_temu_periode_2 =0;$capaian_temu_periode_3 =0;        
                        $capaian_tablet_all_periode =0;        
                        $pel_by_id = [];
                        $pel_by_id2 = [];
                        $pel_by_id3 = [];   
                        // Inisialisai Nilai Kriteria per Pelayanan dan Hitung Pencapaian Pelayanan per Pelayanan
                        foreach($pelayanan as $i => $pel){
                            $tgl_pel[$i] = $pel['tanggal_pelayanan']; 
                            $capaian_berat[$i] = 0;$capaian_tinggi[$i] = 0;
                            $capaian_berattinggi[$i] = 0;
                            $capaian_tekanan_darah[$i] = 0;
                            $capaian_lila[$i] = 0;
                            $capaian_tinggi_puncak_rahim[$i] = 0;
                            $capaian_presensi[$i] = 0;
                            $capaian_djj[$i] = 0;
                            $capaian_presensidjj[$i] = 0;
                            $capaian_skrining[$i] = 0;
                            $capaian_imun_tt[$i] = 0;
                            $capaian_imunisasi[$i] = 0;
                            $capaian_tes_kehamilan[$i] = 0;
                            $capaian_tes_hb[$i] = 0;
                            $capaian_tes_gol_dar[$i] = 0;
                            $capaian_tes_urin[$i] = 0;
                            $capaian_teslabo[$i] = 0;
                            $capaian_tatalaksana[$i] = 0;
                            $capaian_temu[$i] = 0;
                            $capaian_tablet[$i] =0;
            
                            //Berat Tinggi
                            if($pel['berat_tinggi'] != null){
                                $bt = explode('_',$pel['berat_tinggi']);
                                if($bt[0] > 45 && $bt[1] > 100){
                                    $capaian_berattinggi[$i] = 1;
                                }         
                            }
                            
                            // Tekanan Darah
                            if($pel['tekanan_darah'] != null){
                                $td = explode('/',$pel['tekanan_darah']);
                                if($td[0] > 90 && $td[1] > 60){
                                    $capaian_tekanan_darah[$i] = 1;
                                }            
                            }
                    
                            //Lila
                            if($pel['lila'] != null && $pel['lila'] > 2.35 ){
                                $capaian_lila[$i] = 1;            
                            }
                    
                            //Tinggi Puncak Rahim
                            if($pel['tinggi_puncak_rahim'] != null){
                                $capaian_tinggi_puncak_rahim[$i] = 1;            
                            }
                    
                            //Presentasi DJJ
                            if($pel['presentasi_djj']!= null){
                                $pdjj = explode('_',$pel['presentasi_djj']);
                                if($pdjj[0] != null && $pdjj[1] >120){
                                    $capaian_presensidjj[$i] = 1;            
                                }
                            }
                    
                            //Imunisasi Tetanus TT
                            if($pel['imunisasi_tetanus_tt'] != null){
                                $itt = explode('_',$pel['imunisasi_tetanus_tt']);
                                if($itt[0] == "y" && $itt[1] == "y"){  
                                    $capaian_imunisasi[$i] = 1;          
                                }
                            }
                            
                            //Tablet Tambah Darah
                            if($pel['tablet_tambah_darah'] != null){
                                $capaian_tablet[$i] = 1;
                                $capaian_tablet_all_periode = $capaian_tablet_all_periode + $pel['tablet_tambah_darah'];          
                            }
                            else{
                                $capaian_tablet_all_periode = $capaian_tablet_all_periode + 0;            
                            }
                    
                            //Tes Laboratorium
                            if($pel['tes_laboratorium'] != null){
                                $tl = explode('_',$pel['tes_laboratorium']);
                                
                                if($tl[0] == "y" && $tl[1] == "y" && $tl[2] == "y" && $tl[3] == "y"){
                                    $capaian_teslabo[$i] = 1;            
                                }
                            }
                    
                            //Tatalaksana
                            if($pel['tatalaksana'] == "y"){
                                $capaian_tatalaksana[$i] = 1;            
                            }
                    
                            //Temu Wicara
                            if($pel['temu_wicara'] == "y"){
                                $capaian_temu[$i] = 1;            
                            }
            
                            //Perhitungan per Pelayanan
                            if($pel['tanggal_pelayanan'] <= $hayo3bulanlagi && $pel['tanggal_pelayanan'] > $data->tanggal_hamil || $pel['tanggal_pelayanan'] == $data->tanggal_hamil ){
                                $pel_by_id[$pel['id']] = $capaian_berattinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                                $capaian_tinggi_puncak_rahim[$i]+$capaian_presensidjj[$i]+$capaian_imunisasi[$i]+
                                $capaian_teslabo[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet[$i];
                                // $pel_by_id2[$pel['id']] = 0;
                                // $pel_by_id3[$pel['id']] = 0;
                            }else if($pel['tanggal_pelayanan'] <= $hayo6bulanlagi && $pel['tanggal_pelayanan'] > $hayo3bulanlagi){
                                $pel_by_id2[$pel['id']] = $capaian_berattinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                                $capaian_tinggi_puncak_rahim[$i]+$capaian_presensidjj[$i]+$capaian_imunisasi[$i]+
                                $capaian_teslabo[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet[$i];
                                // $pel_by_id1[$pel['id']] = 0;
                                // $pel_by_id3[$pel['id']] = 0;
                            }else if($pel['tanggal_pelayanan'] <= $hayo9bulanlagi && $pel['tanggal_pelayanan'] > $hayo6bulanlagi){
                                $pel_by_id3[$pel['id']] = $capaian_berattinggi[$i]+$capaian_tekanan_darah[$i]+$capaian_lila[$i]+
                                $capaian_tinggi_puncak_rahim[$i]+$capaian_presensidjj[$i]+$capaian_imunisasi[$i]+
                                $capaian_teslabo[$i]+$capaian_tatalaksana[$i]+$capaian_temu[$i]+$capaian_tablet[$i];
                                // $pel_by_id2[$pel['id']] = 0;
                                // $pel_by_id1[$pel['id']] = 0;
                            }
                            //End Perhitungan per Pelayanan
                        }
                        // End Inisialisai Nilai Kriteria per Pelayanan dan Hitung Pencapaian Pelayanan per Pelayanan
                        // Inisialisai Nilai Kriteria per Periode
                        foreach($pelayanan as $i => $pel){
                            if($pel['tanggal_pelayanan'] <= $hayo3bulanlagi && $pel['tanggal_pelayanan'] > $data->tanggal_hamil || $pel['tanggal_pelayanan'] == $data->tanggal_hamil ){
                                if($capaian_berattinggi_periode_1 < 1){$capaian_berattinggi_periode_1 = $capaian_berattinggi_periode_1 + $capaian_berattinggi[$i];}
                                if($capaian_tekanan_darah_periode_1 < 1){$capaian_tekanan_darah_periode_1 = $capaian_tekanan_darah_periode_1 + $capaian_tekanan_darah[$i];}
                                if($capaian_lila_periode_1 < 1){$capaian_lila_periode_1 = $capaian_lila_periode_1 + $capaian_lila[$i];}
                                if($capaian_tinggi_puncak_rahim_periode_1 < 1){$capaian_tinggi_puncak_rahim_periode_1 = $capaian_tinggi_puncak_rahim_periode_1 + $capaian_tinggi_puncak_rahim[$i];}
                                if($capaian_presensidjj_periode_1 < 1){$capaian_presensidjj_periode_1 = $capaian_presensidjj_periode_1 + $capaian_presensidjj[$i];}
                                if($capaian_imunisasi_periode_1 < 1){$capaian_imunisasi_periode_1 = $capaian_imunisasi_periode_1 + $capaian_imunisasi[$i];}
                                if($capaian_teslabo_periode_1 < 1){$capaian_teslabo_periode_1 = $capaian_teslabo_periode_1 + $capaian_teslabo[$i];}
                                if($capaian_tatalaksana_periode_1 < 1){$capaian_tatalaksana_periode_1 = $capaian_tatalaksana_periode_1 + $capaian_tatalaksana[$i];}
                                if($capaian_temu_periode_1 < 1){$capaian_temu_periode_1 = $capaian_temu_periode_1 + $capaian_temu[$i];}
                            }else if($pel['tanggal_pelayanan'] <= $hayo6bulanlagi && $pel['tanggal_pelayanan'] > $hayo3bulanlagi){
                                if($capaian_berattinggi_periode_2 < 1){$capaian_berattinggi_periode_2 = $capaian_berattinggi_periode_2 + $capaian_berattinggi[$i];}
                                if($capaian_tekanan_darah_periode_2 < 1){$capaian_tekanan_darah_periode_2 = $capaian_tekanan_darah_periode_2 + $capaian_tekanan_darah[$i];}
                                if($capaian_lila_periode_2 < 1){$capaian_lila_periode_2 = $capaian_lila_periode_2 + $capaian_lila[$i];}
                                if($capaian_tinggi_puncak_rahim_periode_2 < 1){$capaian_tinggi_puncak_rahim_periode_2 = $capaian_tinggi_puncak_rahim_periode_2 + $capaian_tinggi_puncak_rahim[$i];}
                                if($capaian_presensidjj_periode_2 < 1){$capaian_presensidjj_periode_2 = $capaian_presensidjj_periode_2 + $capaian_presensidjj[$i];}
                                if($capaian_imunisasi_periode_2 < 1){$capaian_imunisasi_periode_2 = $capaian_imunisasi_periode_2 + $capaian_imunisasi[$i];}
                                if($capaian_teslabo_periode_2 < 1){$capaian_teslabo_periode_2 = $capaian_teslabo_periode_2 + $capaian_teslabo[$i];}
                                if($capaian_tatalaksana_periode_2 < 1){$capaian_tatalaksana_periode_2 = $capaian_tatalaksana_periode_2 + $capaian_tatalaksana[$i];}
                                if($capaian_temu_periode_2 < 1){$capaian_temu_periode_2 = $capaian_temu_periode_2 + $capaian_temu[$i];}
                            }else if($pel['tanggal_pelayanan'] <= $hayo9bulanlagi && $pel['tanggal_pelayanan'] > $hayo6bulanlagi){
                                if($capaian_berattinggi_periode_3 < 2){$capaian_berattinggi_periode_3 = $capaian_berattinggi_periode_3 + $capaian_berattinggi[$i];}
                                if($capaian_tekanan_darah_periode_3 < 2){$capaian_tekanan_darah_periode_3 = $capaian_tekanan_darah_periode_3 + $capaian_tekanan_darah[$i];}
                                if($capaian_lila_periode_3 < 2){$capaian_lila_periode_3 = $capaian_lila_periode_3 + $capaian_lila[$i];}
                                if($capaian_tinggi_puncak_rahim_periode_3 < 2){$capaian_tinggi_puncak_rahim_periode_3 = $capaian_tinggi_puncak_rahim_periode_3 + $capaian_tinggi_puncak_rahim[$i];}
                                if($capaian_presensidjj_periode_3 < 2){$capaian_presensidjj_periode_3 = $capaian_presensidjj_periode_3 + $capaian_presensidjj[$i];}
                                if($capaian_imunisasi_periode_3 < 2){$capaian_imunisasi_periode_3 = $capaian_imunisasi_periode_3 + $capaian_imunisasi[$i];}
                                if($capaian_teslabo_periode_3 < 2){$capaian_teslabo_periode_3 = $capaian_teslabo_periode_3 + $capaian_teslabo[$i];}
                                if($capaian_tatalaksana_periode_3 < 2){$capaian_tatalaksana_periode_3 = $capaian_tatalaksana_periode_3 + $capaian_tatalaksana[$i];}
                                if($capaian_temu_periode_3 < 2){$capaian_temu_periode_3 = $capaian_temu_periode_3 + $capaian_temu[$i];}
                            }
                        }
                        if($capaian_tablet_all_periode >=90){
                            $capaian_tablet_all_periode = 1;
                        }
                        else{
                            $capaian_tablet_all_periode = 0;
                        }
                        // End Inisialisai Nilai Kriteria per Periode
                        
                        //Hitung Pencapaian Pelayanan per Periode
                        $tw1capaian = (($capaian_berattinggi_periode_1+$capaian_tekanan_darah_periode_1+$capaian_lila_periode_1+
                        $capaian_tinggi_puncak_rahim_periode_1+$capaian_presensidjj_periode_1+$capaian_imunisasi_periode_1+
                        $capaian_teslabo_periode_1+$capaian_tatalaksana_periode_1+$capaian_temu_periode_1+$capaian_tablet_all_periode) * 100 ) / 10;
                        $tw2capaian = (($capaian_berattinggi_periode_2+$capaian_tekanan_darah_periode_2+$capaian_lila_periode_2+
                        $capaian_tinggi_puncak_rahim_periode_2+$capaian_presensidjj_periode_2+$capaian_imunisasi_periode_2+
                        $capaian_teslabo_periode_2+$capaian_tatalaksana_periode_2+$capaian_temu_periode_2+$capaian_tablet_all_periode) * 100 ) / 10;
                        $tw3capaian = (($capaian_berattinggi_periode_3+$capaian_tekanan_darah_periode_3+$capaian_lila_periode_3+
                        $capaian_tinggi_puncak_rahim_periode_3+$capaian_presensidjj_periode_3+$capaian_imunisasi_periode_3+
                        $capaian_teslabo_periode_3+$capaian_tatalaksana_periode_3+$capaian_temu_periode_3+$capaian_tablet_all_periode) * 100 ) / 20;
                        //End Hitung Pencapaian Pelayanan per Periode
                        
                
                        //Hitung Pencapaian Pelayanan per Total
                        $super_pel_total = $capaian_berattinggi_periode_1+$capaian_tekanan_darah_periode_1+$capaian_lila_periode_1+
                        $capaian_tinggi_puncak_rahim_periode_1+$capaian_presensidjj_periode_1+$capaian_imunisasi_periode_1+
                        $capaian_teslabo_periode_1+$capaian_tatalaksana_periode_1+$capaian_temu_periode_1+$capaian_tablet_all_periode + 
                        $capaian_berattinggi_periode_2+$capaian_tekanan_darah_periode_2+$capaian_lila_periode_2+
                        $capaian_tinggi_puncak_rahim_periode_2+$capaian_presensidjj_periode_2+$capaian_imunisasi_periode_2+
                        $capaian_teslabo_periode_2+$capaian_tatalaksana_periode_2+$capaian_temu_periode_2+$capaian_tablet_all_periode+
                        $capaian_berattinggi_periode_3+$capaian_tekanan_darah_periode_3+$capaian_lila_periode_3+
                        $capaian_tinggi_puncak_rahim_periode_3+$capaian_presensidjj_periode_3+$capaian_imunisasi_periode_3+
                        $capaian_teslabo_periode_3+$capaian_tatalaksana_periode_3+$capaian_temu_periode_3+$capaian_tablet_all_periode;
                        
                        $totalcapaian = ($tw1capaian+$tw2capaian+$tw3capaian)/3;

                        return view('spm.ibuhamil.pelayanan.tambah', compact('mitra_all','tenaga_all','spesialis_all','nik','data','datanik','totalcapaian','super_pel_total','tgl_pel'));
                    }
                    else{
                        return view('spm.ibuhamil.pelayanan.masukan_nik')->with('alert','NIK belum terdaftar sebagai ibu hamil');                                            
                    }
                }
                else{
                    return view('spm.ibuhamil.pelayanan.masukan_nik')->with('alert','NIK belum berusia 22 tahun');                    
                }
            }
            else{
                return view('spm.ibuhamil.pelayanan.masukan_nik')->with('alert','NIK bukan beridentitas perempuan');
            }
        }
        else{
            return view('spm.ibuhamil.pelayanan.masukan_nik')->with('alert','NIK tidak terdaftar');
        }
    }

    public function addPelayanan(Request $request){
        $request->tanggal_pelayanan = date('Y-m-d', strtotime($request->tanggal_pelayanan));
        $request->tanggal_pelayanan = $request->tanggal_pelayanan." ".$request->waktu_pelayanan.":00";

        $hayo3bulanlagi = date('Y-m-d', strtotime('+3 month', strtotime($request->tanggal_hamil)));
        $hayo6bulanlagi = date('Y-m-d', strtotime('+6 month', strtotime($request->tanggal_hamil)));
        $hayo9bulanlagi = date('Y-m-d', strtotime('+9 month', strtotime($request->tanggal_hamil)));
        
        if($request->tanggal_pelayanan <= $hayo3bulanlagi && $request->tanggal_pelayanan > $request->tanggal_hamil || $request->tanggal_pelayanan == $request->tanggal_hamil ){
            $jenis_tw = 1;
        }else if($request->tanggal_pelayanan <= $hayo6bulanlagi && $request->tanggal_pelayanan > $hayo3bulanlagi){
            $jenis_tw = 2;
        }else if($request->tanggal_pelayanan <= $hayo9bulanlagi && $request->tanggal_pelayanan > $hayo6bulanlagi){
            $jenis_tw = 3;
        }else if($request->tanggal_pelayanan < $request->tanggal_hamil){
            $jenis_tw= 1;
        }else if($request->tanggal_pelayanan > $hayo9bulanlagi){
            $jenis_tw = 3;
        }

        if($request->skrining_imunisasi ==  "on"){$si = "y";}else{$si="t";}
        if($request->imunisasi_tt ==  "on"){$itt = "y";}else{$itt ="t";}
        if($request->tes_kehamilan ==  "on"){$tk = "y";}else{$tk="t";}
        if($request->tes_hb ==  "on"){$thb = "y";}else{$thb="t";}
        if($request->tes_gd ==  "on"){$tgd = "y";}else{$tgd="t";}
        if($request->tes_protein_urin ==  "on"){$tpu = "y";}else{$tpu="t";}
        if($request->tes_hiv ==  "on"){$thiv = "y";}else{$thiv="t";}
        if($request->tes_hbsag ==  "on"){$thbsag = "y";}else{$thbsag="t";}
        if($request->tatalaksana ==  "on"){$tatalaksana = "y";}else{$tatalaksana="t";}
        if($request->temu_wicara ==  "on"){$tw = "y";}else{$tw="t";}
        
        $data = New Pelayanan();
        $data['tanggal_pelayanan'] = $request->tanggal_pelayanan;
        $data['id_spm'] =$request->id;
        $data['table_spm'] = 'ibu_hamil';
        $data['tenaga_kerja'] = $request->tenaga_kesehatan."_".$request->nama_stk;
        $data['lokasi'] = $request->fasilitas_kesehatan."_".$request->lokasi_pelayanan;
        $data['jenis_tw'] = $jenis_tw;
        $data['berat_tinggi'] = $request->berat."_".$request->tinggi;
        $data['tekanan_darah'] = $request->tekanan_darah1."/".$request->tekanan_darah2;
        $data['lila'] = $request->lila1.",".$request->lila2;
        $data['tinggi_puncak_rahim'] = $request->tprahim." ".$request->satuantprahim;
        $data['presentasi_djj'] = $request->presensijanin."_".$request->djj;
        $data['imunisasi_tetanus_tt'] = $si."_".$itt;
        $data['tablet_tambah_darah'] = $request->tablet_tambah_darah;
        $data['tes_laboratorium'] = $tk."_".$thb."_".$tgd."_".$tpu."_".$thiv."_".$thbsag;
        $data['tatalaksana'] = $tatalaksana;
        $data['temu_wicara'] = $tw;
        $data['created_by'] = $request->created_by;

        if($data->save()){
                return Redirect::route('detail-ibu-hamil',[$request->id])->with('success','Berhasil Tambah Pelayanan Ibu Hamil');
            
        }  
        else{
            return Redirect::route('catat-ibu-hamil')->with('danger','Gagal Tambah Pelayanan Ibu Hamil');
        }

    } 

    public function editPelayanan(Request $request){
        $request->tanggal_pelayanan = date('Y-m-d', strtotime($request->tanggal_pelayanan));
        $request->tanggal_pelayanan = $request->tanggal_pelayanan." ".$request->waktu_pelayanan.":00";

        $hayo3bulanlagi = date('Y-m-d', strtotime('+3 month', strtotime($request->tanggal_hamil)));
        $hayo6bulanlagi = date('Y-m-d', strtotime('+6 month', strtotime($request->tanggal_hamil)));
        $hayo9bulanlagi = date('Y-m-d', strtotime('+9 month', strtotime($request->tanggal_hamil)));
        
        if($request->tanggal_pelayanan <= $hayo3bulanlagi && $request->tanggal_pelayanan > $request->tanggal_hamil || $request->tanggal_pelayanan == $request->tanggal_hamil ){
            $jenis_tw = 1;
        }else if($request->tanggal_pelayanan <= $hayo6bulanlagi && $request->tanggal_pelayanan > $hayo3bulanlagi){
            $jenis_tw = 2;
        }else if($request->tanggal_pelayanan <= $hayo9bulanlagi && $request->tanggal_pelayanan > $hayo6bulanlagi){
            $jenis_tw = 3;
        }else if($request->tanggal_pelayanan < $request->tanggal_hamil){
            $jenis_tw= 1;
        }else if($request->tanggal_pelayanan > $hayo9bulanlagi){
            $jenis_tw = 3;
        }

        if($request->skrining_imunisasi ==  "on"){$si = "y";}else{$si="t";}
        if($request->imunisasi_tt ==  "on"){$itt = "y";}else{$itt ="t";}
        if($request->tes_kehamilan ==  "on"){$tk = "y";}else{$tk="t";}
        if($request->tes_hb ==  "on"){$thb = "y";}else{$thb="t";}
        if($request->tes_gd ==  "on"){$tgd = "y";}else{$tgd="t";}
        if($request->tes_protein_urin ==  "on"){$tpu = "y";}else{$tpu="t";}
        if($request->tes_hiv ==  "on"){$thiv = "y";}else{$thiv="t";}
        if($request->tes_hbsag ==  "on"){$thbsag = "y";}else{$thbsag="t";}
        if($request->tatalaksana ==  "on"){$tatalaksana = "y";}else{$tatalaksana="t";}
        if($request->temu_wicara ==  "on"){$tw = "y";}else{$tw="t";}

        $data = Pelayanan::where('id',$request->id)->first();
        $ibuhamil = IbuHamil::where('id',$data->id_spm)->first();

        $data['tanggal_pelayanan'] = $request->tanggal_pelayanan;
        $data['tenaga_kerja'] = $request->tenaga_kesehatan."_".$request->nama_stk;
        $data['lokasi'] = $request->fasilitas_kesehatan."_".$request->lokasi_pelayanan;
        $data['jenis_tw'] = $jenis_tw;
        $data['berat_tinggi'] = $request->berat."_".$request->tinggi;
        $data['tekanan_darah'] = $request->tekanan_darah1."/".$request->tekanan_darah2;
        $data['lila'] = $request->lila1.",".$request->lila2;
        $data['tinggi_puncak_rahim'] = $request->tprahim." ".$request->satuantprahim;
        $data['presentasi_djj'] = $request->presensijanin."_".$request->djj;
        $data['imunisasi_tetanus_tt'] = $si."_".$itt;
        $data['tablet_tambah_darah'] = $request->tablet_tambah_darah;
        $data['tes_laboratorium'] = $tk."_".$thb."_".$tgd."_".$tpu."_".$thiv."_".$thbsag;
        $data['tatalaksana'] = $tatalaksana;
        $data['temu_wicara'] = $tw;
        $data['updated_by'] = $request->updated_by;

        if($data->save()){
            return Redirect::route('detail-ibu-hamil',[$ibuhamil->id])->with('success','Berhasil Ubah Pelayanan Ibu Hamil');
        }  
        else{
            return Redirect::route('catat-ibu-hamil')->with('danger','Gagal Tambah Pelayanan Ibu Hamil');
        }
    }

    public function getUsiaSaatPelayanan(Request $request){
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d', strtotime($request->tgl));
        if($tgl == "1970-01-01"){
            $tgl = "0000-00-00";
        }
        if($request->wkt == null){
            $tgl = $tgl." "."00:00:00";
        }
        else{
            $tgl = $tgl." ".$request->wkt.":00";
        }

        if(explode(' ',$tgl)[0] == "0000-00-00"){
            $data = array("years"=>0, "months_total" => 0, "months" => 0, "days_total" => 0, "days" => 0, 
            "hours_total" => 0, "hours" => 0, "minutes_total" => 0, "minutes" => 0,
            "seconds_total" => 0, "seconds" => 0);
            $data2 = array("years"=>0, "months_total" => 0, "months" => 0, "days_total" => 0, "days" => 0, 
            "hours_total" => 0, "hours" => 0, "minutes_total" => 0, "minutes" => 0,
            "seconds_total" => 0, "seconds" => 0);
        }
        else{
            $data = IbuHamilController::Umur2(date('Y-m-d h:i:s',strtotime($tgl)), date('Y-m-d h:i:s',strtotime($request->tgl_hml)));
            $data2 = IbuHamilController::Umur2(date('Y-m-d h:i:s',strtotime($tgl)), date('Y-m-d h:i:s',strtotime($request->tgl_lhr)));
        }
        return response()->json([
            'message' =>'success',
            'data' => $data,
            'data2' => $data2,
        ]);
    }

    public function findmitra(Request $request){
        $mitra = MitraKesehatan::where('jenis_mitra',$request->id)->get();
        return response()->json(['message' =>'success','data' => $mitra]);
        // return response()->json(['message' =>'success']);

    }

    public function findtenaga(Request $request){
        $namastk = TenagaKesehatan::where('id_mitra',$request->id)->get();
        $tenaga = "";
        foreach($namastk as $stk){
            $explode = explode('_',$stk->spesialis);
            
                foreach($explode as $exp){
                    if($tenaga == null){
                        $tenaga = $stk->spesialis;
                    }
                    else{
                        if(strpos($tenaga,$exp) !== false){
                            //nothing
                        }
                        else{
                            $tenaga = $tenaga."_".$exp;
                        }
                    }
                }
            
                if($tenaga == null){
                    $tenaga = $stk->spesialis;
                }
                else{
                    if(strpos($tenaga,$stk->spesialis) !== false){
                        //nothing
                    }
                    else{
                        $tenaga = $tenaga."_".$stk->spesialis;
                    }
                }
        }
        return response()->json(['message' =>'success','data' => $tenaga]);
        // return response()->json(['message' =>'success']);
    }
    public function findnamastk(Request $request){
        $tenaga = $request->tenaga;
        $tenaga_all = TenagaKesehatan::where('id_mitra',$request->mitra)->get();
        $nama = "";
        foreach($tenaga_all as $i => $tng){
            $explode = explode("_",$tng->spesialis);
            foreach($explode as $exp){
                if(strpos($tenaga,$exp) !== false){
                    if($nama == null){
                        $nama = $tng->nama;
                    }
                    else{
                        if(strpos($nama,$tng->nama) !== false){
                        //nothing                            
                        }
                        else{
                            $nama = $nama."_".$tng->nama;
                        }
                    }
                }
            }
        }
        return response()->json(['message' =>'success','data' => $nama]);
        // return response()->json(['message' =>'success']);
    }

    public function deletePelayanan($id){
        $forid=Pelayanan::where('id',$id)->first();        
        $idredicrect = $forid['id_spm'];
        $delete=Pelayanan::where('id',$id)->delete();
        
        if($delete){
            return Redirect::route('detail-ibu-hamil',[$idredicrect])->with('success','Berhasil Hapus Pelayanan');
        }  
        else{
            return Redirect::route('detail-ibu-hamil',[$idredicrect])->with('danger','Gagal Hapus Pelayanan');
        }
    }


    public function rohimam(){
        $nama = "aku";
        print $nama;
        // print $tenaga_all;
        // $nama = "";
        // foreach($tenaga_all as $i => $tng){
        //     $explode = explode('_',$tng->spesialis);
        //     foreach($explode as $exp){
        //         if($exp)
        //     }
        // }
        // $tenaga = TenagaKesehatan::where('spesialis',$request->tenaga)->where('id_mitra',$request->mitra)->get();
        // return response()->json(['message' =>'success','data' => $tenaga]);
        // return response()->json(['message' =>'success']);

    }

    public function delete($id){
        $ibuhamil=IbuHamil::where('id',$id)->first();
        $pelayanan_all = Pelayanan::all();
        foreach($pelayanan_all as $pel){
            if($pel['table_spm'] == "ibu_hamil" && $pel['id_spm'] == $ibuhamil->id){
                $deletepel = $pel->delete();
            }
        }
        $delete = $ibuhamil->delete();
        
        if($delete){
            return Redirect::route('data-ibu-hamil')->with('success','Berhasil Hapus Ibu Hamil');
        }  
        else{
            return Redirect::route('data-ibu-hamil')->with('danger','Gagal Hapus Ibu Hamil');
        }
    }

    public function Cek_Capaian($berat_tinggi,$tekanan_darah, $lila,$tinggi_puncak_rahim, $presentasi_djj,$imunisasi_tetanus_tt, $tablet_tambah_darah, $tes_laboratorium, $tatalaksana,$temu_wicara, $i)
    {
        // Berat Tinggi
        if($berat_tinggi != null){
            $bt = explode('_',$berat_tinggi);
            if($bt[0] >1 && $bt[1] > 1){
                $capaian[0][$i] = "x";
            }
            elseif($bt[0] >1){
                $capaian[0][$i] = "y";
            }            
            elseif($bt[1] >1){
                $capaian[0][$i] = "z";
            }
            else{
                $capaian[0][$i] = 0;
            }            
        }
        else{
            $capaian[0][$i] = 0;
        }

        //Tekanan Darah
        if($tekanan_darah != null){
            $td = explode('/',$tekanan_darah);
            if($td[0] >1 && $td[1] > 1){
                $capaian[1][$i] = "x";
            }
            else{
                $capaian[1][$i] = 0;
            }            
        }
        else{
            $capaian[1][$i] = 0;
        }

        //Lila
        if($lila != null){
            $capaian[2][$i] = "x";            
        }
        else{
            $capaian[2][$i] = 0;
        }

        //Tinggi Puncak Rahim
        if($tinggi_puncak_rahim != null){
            $capaian[3][$i] = "x";            
        }
        else{
            $capaian[3][$i] = 0;
        }

        //Presentasi DJJ
        if($presentasi_djj != null){
            $pdjj = explode('_',$presentasi_djj);
            if($pdjj[0] != null && $pdjj[1] >1){
                $capaian[4][$i] = "x";            
            }
            elseif($pdjj[0] != null){
                $capaian[4][$i] = "y";
            }
            elseif($pdjj[1] > 1){
                $capaian[4][$i] = "z";
            }
            else{
                $capaian[4][$i] = 0;
            }
        }
        else{
            $capaian[4][$i] = 0;
        }

        //Imunisasi Tetanus TT
        if($imunisasi_tetanus_tt != null){
            $itt = explode('_',$imunisasi_tetanus_tt);
            if($itt[0] == "y" && $itt[1] == "y"){
                $capaian[5][$i] = "x";            
            }
            elseif($itt[0] == "y"){
                $capaian[5][$i] = "y";
            }
            elseif($itt[1] == "y"){
                $capaian[5][$i] = "z";
            }
            else{
                $capaian[5][$i] = 0;
            }
        }
        else{
            $capaian[5][$i] = 0;
        }
        
        //Tablet Tambah Darah
        if($tablet_tambah_darah != null){
            $capaian[6][$i] = $tablet_tambah_darah;            
        }
        else{
            $capaian[6][$i] = 0;
        }

        //Tes Laboratorium
        if($tes_laboratorium != null){
            $tl = explode('_',$tes_laboratorium);
            if($tl[0] == "y" && $tl[1] == "y" && $tl[2] == "y" && $tl[3] == "y"){
                $capaian[7][$i] = "x";            
            }
            // else if($tl[0] == "y" && $tl[1] != "y" && $tl[2] != "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "a";            
            // }
            // else if($tl[0] != "y" && $tl[1] == "y" && $tl[2] != "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "b";            
            // }
            // else if($tl[0] != "y" && $tl[1] != "y" && $tl[2] == "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "c";            
            // }
            // else if($tl[0] != "y" && $tl[1] != "y" && $tl[2] != "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "d";            
            // }
            // else if($tl[0] == "y" && $tl[1] == "y" && $tl[2] != "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "e";            
            // }
            // else if($tl[0] == "y" && $tl[1] != "y" && $tl[2] == "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "f";            
            // }
            // else if($tl[0] == "y" && $tl[1] != "y" && $tl[2] != "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "g";            
            // }
            // else if($tl[0] != "y" && $tl[1] == "y" && $tl[2] == "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "h";            
            // }
            // else if($tl[0] != "y" && $tl[1] == "y" && $tl[2] != "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "i";            
            // }
            // else if($tl[0] != "y" && $tl[1] != "y" && $tl[2] == "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "j";            
            // }
            // else if($tl[0] == "y" && $tl[1] == "y" && $tl[2] == "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "k";            
            // }
            // else if($tl[0] == "y" && $tl[1] == "y" && $tl[2] != "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "l";            
            // }
            // else if($tl[0] == "y" && $tl[1] != "y" && $tl[2] == "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "m";            
            // }
            // else if($tl[0] != "y" && $tl[1] == "y" && $tl[2] == "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "n";            
            // }
            else{
                $capaian[7][$i] = 0;
            }
        }
        else{
            $capaian[7][$i] = 0;
        }

        //Tatalaksana
        if($tatalaksana == "y"){
            $capaian[8][$i] = "x";            
        }
        else{
            $capaian[8][$i] = 0;
        }

        //Temu Wicara
        if($temu_wicara == "y"){
            $capaian[9][$i] = "x";            
        }
        else{
            $capaian[9][$i] = 0;
        }

        return $capaian;
    }

    public function Umur($lahir)
    {        
        $pisah_lahir = explode('-',$lahir);
        $thn_lahir = intval($pisah_lahir[0]);
        $bln_lahir = intval($pisah_lahir[1]);
        $tgl_lahir = intval($pisah_lahir[2]);
        $tanggal = intval(date('d'));
        $bulan = intval(date('m'));
        $tahun = intval(date('Y'));
        $usia = $tahun - $thn_lahir;
        if($bulan < $bln_lahir || ($bulan == $bln_lahir && $tanggal < $tgl_lahir)){
            $usia -= 1;
        }

        return $usia;
    }
    public function Umur2($tgl1, $tgl2)
    {
        $tgl1 = (is_string($tgl1) ? strtotime($tgl1) : $tgl1);
        $tgl2 = (is_string($tgl2) ? strtotime($tgl2) : $tgl2);
        $diff_secs = abs($tgl1-$tgl2);
        $base_year = min(date("Y",$tgl1), date("Y",$tgl2));
        $diff = mktime(0,0,$diff_secs,1,1,$base_year);
        return array("years"=>date("Y",$diff) - $base_year, "months_total" => (date("Y",$diff) - $base_year) * 12 + date("n",$diff) - 1, 
        "months" => date("n",$diff) -1, "days_total" => floor($diff_secs/ (3600 * 24)), "days" => date("j",$diff) - 1, 
        "hours_total" => floor($diff_secs/3600), "hours" => date("G", $diff), "minutes_total" => floor($diff_secs/60), "minutes" => (int)date("i",$diff),
        "seconds_total" => $diff_secs, "seconds" => (int) date("s",$diff));


        // $pisah_lahir = explode('-',$lahir);
        // $thn_lahir = intval($pisah_lahir[0]);
        // $bln_lahir = intval($pisah_lahir[1]);
        // $tgl_lahir = intval($pisah_lahir[2]);
        // $tanggal = intval(date('d'));
        // $bulan = intval(date('m'));
        // $tahun = intval(date('Y'));
        // $usia = $tahun - $thn_lahir;
        // if($bulan < $bln_lahir || ($bulan == $bln_lahir && $tanggal < $tgl_lahir)){
        //     $usia -= 1;
        // }

        // return $usia;
    }

}
