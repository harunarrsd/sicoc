<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Requests;

use App\Pelayanan;

class PelayananController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }


    // public function index()
    // {
    //     return view('home');
    // }
    public function load_pelayanan($kodpel)
    {
        return Pelayanan::where('id_pelayanan', $kodpel)->first();
    }
}
