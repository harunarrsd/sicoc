<?php

namespace App\Http\Controllers;
use DB;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\IbuHamil;
use App\Warga;
use App\Pelayanan;

class SPMController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function CheckNIK_IbuHamil($nik)
    {
        return IbuHamil::where('nik', $nik)->first();
    }

    public function RiwayatIbuHamilbyNIK($nik)
    {        
        $ibuhamil = DB::table('spm_ibu_hamil')
        ->join('warga','spm_ibu_hamil.nik','=','warga.nik')
        ->select('spm_ibu_hamil.*','warga.*')
        ->where('spm_ibu_hamil.nik',$nik)
        ->get();

        $data = IbuHamil::where('nik', $nik)->first();
        $kodepel = $data['kode_pelayanan'];
        $pkodpel = explode(',', $kodepel);
        $pel = []; $pel2 = []; $pel3 = [];                        
        $hitung = []; $hitung2 = []; $hitung3 = [];                        
        // $hitung = ""; $hitung2 = ""; $hitung3 = "";                        
        foreach($pkodpel as $i =>$val){
            $data_pelayanan[$i] = Pelayanan::where('id_pelayanan', $val)->first();
            if($data_pelayanan[$i]['jenis_tw'] == 1){
                $cek[$i] = SPMController::Cek_Capaian($data_pelayanan[$i]['berat_tinggi'],
                $data_pelayanan[$i]['tekanan_darah'], $data_pelayanan[$i]['lila'],
                $data_pelayanan[$i]['tinggi_puncak_rahim'], $data_pelayanan[$i]['presentasi_djj'],
                $data_pelayanan[$i]['imunisasi_tetanus_tt'], $data_pelayanan[$i]['tablet_tambah_darah'],
                $data_pelayanan[$i]['tes_laboratorium'], $data_pelayanan[$i]['tatalaksana'],
                $data_pelayanan[$i]['temu_wicara'], $i);
                // $pel[$i] = 0;
            }
            elseif($data_pelayanan[$i]['jenis_tw'] == 2){
                $cek2[$i] = SPMController::Cek_Capaian($data_pelayanan[$i]['berat_tinggi'],
                $data_pelayanan[$i]['tekanan_darah'], $data_pelayanan[$i]['lila'],
                $data_pelayanan[$i]['tinggi_puncak_rahim'], $data_pelayanan[$i]['presentasi_djj'],
                $data_pelayanan[$i]['imunisasi_tetanus_tt'], $data_pelayanan[$i]['tablet_tambah_darah'],
                $data_pelayanan[$i]['tes_laboratorium'], $data_pelayanan[$i]['tatalaksana'],
                $data_pelayanan[$i]['temu_wicara'], $i);
                // $pel2[$i] = 0;
            }
            elseif($data_pelayanan[$i]['jenis_tw'] == 3){
                $cek3[$i] = SPMController::Cek_Capaian($data_pelayanan[$i]['berat_tinggi'],
                $data_pelayanan[$i]['tekanan_darah'], $data_pelayanan[$i]['lila'],
                $data_pelayanan[$i]['tinggi_puncak_rahim'], $data_pelayanan[$i]['presentasi_djj'],
                $data_pelayanan[$i]['imunisasi_tetanus_tt'], $data_pelayanan[$i]['tablet_tambah_darah'],
                $data_pelayanan[$i]['tes_laboratorium'], $data_pelayanan[$i]['tatalaksana'],
                $data_pelayanan[$i]['temu_wicara'], $i);
                // $pel3[$i] = 0;
            }
            $pel[$i] =0;
            $pel2[$i] =0;
            $pel3[$i] =0;
        }
        for($i=0;$i<10;$i++){
            $tablet =0;
            $tablet2 =0;
            $tablet3 =0;
            foreach($pkodpel as $j =>$val){
                $data_pelayanan[$j] = Pelayanan::where('id_pelayanan', $val)->first();
                if($data_pelayanan[$j]['jenis_tw'] == 1){
                    $hitung[$j]= $cek[$j][$i][$j];
                    // print $cek[$j][$i][$j];                                                                                     
                }
                if($data_pelayanan[$j]['jenis_tw'] == 2){
                    $hitung2[$j]= $cek2[$j][$i][$j]; 
                    // print $cek2[$j][$i][$j];                                                                                     
                }
                if($data_pelayanan[$j]['jenis_tw'] == 3){
                    $hitung3[$j]= $cek3[$j][$i][$j]; 
                    // print $cek3[$j][$i][$j];                                                                                     
                }
            }
            // print "..."; print_r($hitung); print "VVVVVVV";
            // print "..."; print_r($hitung2); print "VVVVVVV";
            // print "..."; print_r($hitung3); print "VVVVVVV";
            $indexxhitung = array_search("x", $hitung);
            if($indexxhitung != null){
                $pel[$indexxhitung]++;
                // print "NNNNNNN indexxhitung : ".$indexxhitung;
            }
            else{
                $indexyhitung = array_search("y", $hitung);
                if($indexyhitung != null){
                    $indexzhitung = array_search("z", $hitung);
                    if($indexzhitung != null){
                        if($indexyhitung > $indexzhitung){
                            $pel[$indexyhitung]++;
                            // print "NNNNNNN indexyhitung : ".$indexyhitung;                                    
                        }
                        else{
                            $pel[$indexzhitung]++;
                            // print "NNNNNNN indexzhitung : ".$indexzhitung;
                        }
                    }
                }
            }
            $indexxhitung2 = array_search("x", $hitung2);
            if($indexxhitung2 != null){
                $pel2[$indexxhitung2]++;
                // print "NNNNNNNN indexxhitung2 : ".$indexxhitung2;
            }
            else{
                $indexyhitung2 = array_search("y", $hitung2);
                if($indexyhitung2 != null){
                    $indexzhitung2 = array_search("z", $hitung2);
                    if($indexzhitung2 != null){
                        if($indexyhitung2 > $indexzhitung2){
                            $pel2[$indexyhitung2]++;
                            // print "NNNNNNN indexyhitung2 : ".$indexyhitung2;                                    
                        }
                        else{
                            $pel2[$indexzhitung2]++;
                            // print "NNNNNNN indexzhitung2 : ".$indexzhitung2;
                        }
                    }
                }
            }
            $indexxhitung3 = array_search("x", $hitung3);
            if($indexxhitung3 != null){
                $pel3[$indexxhitung3]++;
                // print "NNNNNNNN indexxhitung3 : ".$indexxhitung3;
            }
            else{
                $indexyhitung3 = array_search("y", $hitung3);
                if($indexyhitung3 != null){
                    $indexzhitung3 = array_search("z", $hitung3);
                    if($indexzhitung3 != null){
                        if($indexyhitung3 > $indexzhitung3){
                            $pel3[$indexyhitung3]++;
                            // print "NNNNNNN indexyhitung3 : ".$indexyhitung3;                                    
                        }
                        else{
                            $pel3[$indexzhitung3]++;
                            // print "NNNNNNN indexzhitung3 : ".$indexzhitung3;
                        }
                    }
                }
            }                        
            // print "<br>";       
        }
        // print "<br>----------<br>";
        // print_r($pel);print "<br>";
        // print_r($pel2);print "<br>";
        // print_r($pel3);print "<br>";
        // print "<br>----------<br>";
        $pel_total =0;
        $pel_total2 =0;
        $pel_total3 =0;
        foreach($pkodpel as $i =>$val){
            $data_pelayanan[$i] = Pelayanan::where('id_pelayanan', $val)->first();
            if($data_pelayanan[$i]['jenis_tw'] == 1){
                 
                if(intval($cek[$i][6][$i]) > 0 ){
                    $tablet += intval($cek[$i][6][$i]);
                    if($tablet == 30){
                        $pel[$i]++;
                    }
                }
                // print $pel[$i];
                $pel_total += $pel[$i];
            }
            else if($data_pelayanan[$i]['jenis_tw'] == 2){
                if(intval($cek2[$i][6][$i]) > 0 ){
                    $tablet2 += intval($cek2[$i][6][$i]);
                    if($tablet2 == 30){
                        $pel2[$i]++;
                    }
                }
                
                // print $pel2[$i];
                $pel_total2 += $pel2[$i];
            }
            else if($data_pelayanan[$i]['jenis_tw'] == 3){
                if(intval($cek3[$i][6][$i]) > 0 ){
                    $tablet3 += intval($cek3[$i][6][$i]);
                    if($tablet3 == 30){
                        $pel3[$i]++;
                    }
                }
                // print $pel3[$i];
                $pel_total3 += $pel3[$i];
            }
        }
        // print "<br><br>".$pel_total."-------".$tablet;
        // print "<br><br>".$pel_total2."-------".$tablet2;
        // print "<br><br>".$pel_total3."-------".$tablet3;
        // print "<br><br><br><br>";
        // print "<br><br>";
        $super_pel_total = $pel_total + $pel_total2 + $pel_total3;
        $totalcapaian = ($super_pel_total * 100) / 40; 

        $tw1capaian = ($pel_total * 100) / 10;
        $tw2capaian = ($pel_total2 * 100) / 10;
        $tw3capaian = ($pel_total3 * 100) / 20;
        // print $super_pel_total."<br><br><br><br><br><br><br><br>".$totalcapaian."<br>".$tw1capaian."<br>".$tw2capaian."<br>".$tw3capaian;
        return view('spm.ibuhamil.detail_user', compact('ibuhamil','tw1capaian','tw2capaian','tw3capaian','totalcapaian','super_pel_total','data_pelayanan', 'pel', 'pel2', 'pel3'));
    }

    public function MasukanNIK()
    {   
        if(!empty(Session::get('withnik'))){
            Session::forget('withnik');
            return view('spm.ibuhamil.pelayanan.masukan_nik');  
        }
        else{
            return view('spm.ibuhamil.pelayanan.masukan_nik');  
        }
    }

    public function CatatIbuHamil(Request $request)
    {
        Session::put('withnik',$request->nik);
        $nik = Session::get('withnik');
        $datanik = Warga::where('nik', $nik)->first();
        if(count($datanik) > 0 ){
            if($datanik['jenis_kelamin'] == "P"){
                $usia = SPMController::Umur($datanik['tgl_lahir']);

                if($usia >= 22){
                    $data = IbuHamil::where('nik', $nik)->where('status_kehamilan','belum melahirkan')->first();
                    if(count($data) > 0){
                        $kodepel = $data['kode_pelayanan'];
                        $pkodpel = explode(',', $kodepel);
                        $pel = []; $pel2 = []; $pel3 = [];                        
                        foreach($pkodpel as $i =>$val){
                            $data_pelayanan[$i] = Pelayanan::where('id_pelayanan', $val)->first();
                            if($data_pelayanan[$i]['jenis_tw'] == 1){
                                $cek[$i] = SPMController::Cek_Capaian($data_pelayanan[$i]['berat_tinggi'],
                                $data_pelayanan[$i]['tekanan_darah'], $data_pelayanan[$i]['lila'],
                                $data_pelayanan[$i]['tinggi_puncak_rahim'], $data_pelayanan[$i]['presentasi_djj'],
                                $data_pelayanan[$i]['imunisasi_tetanus_tt'], $data_pelayanan[$i]['tablet_tambah_darah'],
                                $data_pelayanan[$i]['tes_laboratorium'], $data_pelayanan[$i]['tatalaksana'],
                                $data_pelayanan[$i]['temu_wicara'], $i);
                                // $pel[$i] = 0;
                            }
                            elseif($data_pelayanan[$i]['jenis_tw'] == 2){
                                $cek2[$i] = SPMController::Cek_Capaian($data_pelayanan[$i]['berat_tinggi'],
                                $data_pelayanan[$i]['tekanan_darah'], $data_pelayanan[$i]['lila'],
                                $data_pelayanan[$i]['tinggi_puncak_rahim'], $data_pelayanan[$i]['presentasi_djj'],
                                $data_pelayanan[$i]['imunisasi_tetanus_tt'], $data_pelayanan[$i]['tablet_tambah_darah'],
                                $data_pelayanan[$i]['tes_laboratorium'], $data_pelayanan[$i]['tatalaksana'],
                                $data_pelayanan[$i]['temu_wicara'], $i);
                                // $pel2[$i] = 0;
                            }
                            elseif($data_pelayanan[$i]['jenis_tw'] == 3){
                                $cek3[$i] = SPMController::Cek_Capaian($data_pelayanan[$i]['berat_tinggi'],
                                $data_pelayanan[$i]['tekanan_darah'], $data_pelayanan[$i]['lila'],
                                $data_pelayanan[$i]['tinggi_puncak_rahim'], $data_pelayanan[$i]['presentasi_djj'],
                                $data_pelayanan[$i]['imunisasi_tetanus_tt'], $data_pelayanan[$i]['tablet_tambah_darah'],
                                $data_pelayanan[$i]['tes_laboratorium'], $data_pelayanan[$i]['tatalaksana'],
                                $data_pelayanan[$i]['temu_wicara'], $i);
                                // $pel3[$i] = 0;
                            }
                            $pel[$i] =0;
                            $pel2[$i] =0;
                            $pel3[$i] =0;
                        }
                        for($i=0;$i<10;$i++){                        
                            $hitung = ""; $hitung2 = ""; $hitung3 = "";                                                
                            $tablet =0;
                            $tablet2 =0;
                            $tablet3 =0;
                            foreach($pkodpel as $j =>$val){
                                $data_pelayanan[$j] = Pelayanan::where('id_pelayanan', $val)->first();
                                if($data_pelayanan[$j]['jenis_tw'] == 1){
                                    $hitung .= $cek[$j][$i][$j];
                                    // print "<button>".$cek[$j][$i][$j]."</button>";                                                                                     
                                }
                                if($data_pelayanan[$j]['jenis_tw'] == 2){
                                    $hitung2 .= $cek2[$j][$i][$j]; 
                                    // print "<b>".$cek2[$j][$i][$j]."</b>";                                                                                     
                                }
                                if($data_pelayanan[$j]['jenis_tw'] == 3){
                                    $hitung3 .= $cek3[$j][$i][$j]; 
                                    // print $cek3[$j][$i][$j];                                                                                     
                                }
                            }
                            $pos = strpos($hitung, "x");
                            if($pos === false){
                                $pos2 = strpos($hitung, "y");
                                if($pos2 === false){
                                    //0 nilai nya
                                }else{
                                    $pos3 = strpos($hitung, "z");
                                    if($pos3 === false){
                                        // Nilai 0
                                    }
                                    else{
                                        if($pos2 > $pos3){
                                            $pel[$pos2]++;                                    
                                        }
                                        else{
                                            $pel[$pos3]++;
                                        }
                                    }
                                }
                            }else{
                                $pel[$pos]++;
                            }
                            // print "<br>";
                            $pos = strpos($hitung2, "x");
                            if($pos === false){
                                $pos2 = strpos($hitung2, "y");
                                if($pos2 === false){
                                    //0 nilai nya
                                }else{
                                    $pos3 = strpos($hitung2, "z");
                                    if($pos3 === false){
                                        // Nilai 0
                                    }
                                    else{
                                        if($pos2 > $pos3){
                                            $pel2[$pos2]++;                                    
                                        }
                                        else{
                                            $pel2[$pos3]++;
                                        }
                                    }
                                }
                            }else{
                                $pel2[$pos]++;
                            }
                            // print "<br>";
                            $pos = strpos($hitung3, "x");
                            if($pos === false){
                                $pos2 = strpos($hitung3, "y");
                                if($pos2 === false){
                                    //0 nilai nya
                                }else{
                                    $pos3 = strpos($hitung3, "z");
                                    if($pos3 === false){
                                        // Nilai 0
                                    }
                                    else{
                                        if($pos2 > $pos3){
                                            $pel3[$pos2]++;                                    
                                        }
                                        else{
                                            $pel3[$pos3]++;
                                        }
                                    }
                                }
                            }else{
                                $pel3[$pos]++;
                            }
                            // print "<br>"; 
                            // $indexxhitung = array_search("x", $hitung);
                            // if($indexxhitung != null){
                            //     $pel[$indexxhitung]++;
                            // }
                            // else{
                            //     $indexyhitung = array_search("y", $hitung);
                            //     if($indexyhitung != null){
                            //         $indexzhitung = array_search("z", $hitung);
                            //         if($indexzhitung != null){
                            //             if($indexyhitung > $indexzhitung){
                            //                 $pel[$indexyhitung]++;
                            //             }
                            //             else{
                            //                 $pel[$indexzhitung]++;
                            //             }
                            //         }
                            //     }
                            // }
                            // $indexxhitung2 = array_search("x", $hitung2);
                            // if($indexxhitung2 != null){
                            //     $pel2[$indexxhitung2]++;
                            // }
                            // else{
                            //     $indexyhitung2 = array_search("y", $hitung2);
                            //     if($indexyhitung2 != null){
                            //         $indexzhitung2 = array_search("z", $hitung2);
                            //         if($indexzhitung2 != null){
                            //             if($indexyhitung2 > $indexzhitung2){
                            //                 $pel2[$indexyhitung2]++;
                            //             }
                            //             else{
                            //                 $pel2[$indexzhitung2]++;
                            //             }
                            //         }
                            //     }
                            // }
                            // $indexxhitung3 = array_search("x", $hitung3);
                            // if($indexxhitung3 != null){
                            //     $pel3[$indexxhitung3]++;
                            // }
                            // else{
                            //     $indexyhitung3 = array_search("y", $hitung3);
                            //     if($indexyhitung3 != null){
                            //         $indexzhitung3 = array_search("z", $hitung3);
                            //         if($indexzhitung3 != null){
                            //             if($indexyhitung3 > $indexzhitung3){
                            //                 $pel3[$indexyhitung3]++;
                            //             }
                            //             else{
                            //                 $pel3[$indexzhitung3]++;
                            //             }
                            //         }
                            //     }
                            // }      
                        }
                        $pel_total =0;
                        $pel_total2 =0;
                        $pel_total3 =0;
                        foreach($pkodpel as $i =>$val){
                            $data_pelayanan[$i] = Pelayanan::where('id_pelayanan', $val)->first();
                            if($data_pelayanan[$i]['jenis_tw'] == 1){
                                 
                                if(intval($cek[$i][6][$i]) > 0 ){
                                    $tablet += intval($cek[$i][6][$i]);
                                    if($tablet == 30){
                                        $pel[$i]++;
                                    }
                                }
                                // print $pel[$i];
                                $pel_total += $pel[$i];
                            }
                            else if($data_pelayanan[$i]['jenis_tw'] == 2){
                                if(intval($cek2[$i][6][$i]) > 0 ){
                                    $tablet2 += intval($cek2[$i][6][$i]);
                                    if($tablet2 == 30){
                                        $pel2[$i]++;
                                    }
                                }
                                
                                // print $pel2[$i];
                                $pel_total2 += $pel2[$i];
                            }
                            else if($data_pelayanan[$i]['jenis_tw'] == 3){
                                if(intval($cek3[$i][6][$i]) > 0 ){
                                    $tablet3 += intval($cek3[$i][6][$i]);
                                    if($tablet3 == 30){
                                        $pel3[$i]++;
                                    }
                                }
                                // print $pel3[$i];
                                $pel_total3 += $pel3[$i];
                            }
                        }
                        // print "<br><br>".$pel_total."-------".$tablet;
                        // print "<br><br>".$pel_total2."-------".$tablet2;
                        // print "<br><br>".$pel_total3."-------".$tablet3;
                        // print "<br><br><br><br>";
                        // print "<br><br>";
                        $super_pel_total = $pel_total + $pel_total2 + $pel_total3;
                        $totalcapaian = ($super_pel_total * 100) / 40; 

                        $tw1capaian = ($pel_total * 100) / 10;
                        $tw2capaian = ($pel_total2 * 100) / 10;
                        $tw3capaian = ($pel_total3 * 100) / 20;
                        // print $super_pel_total."<br><br><br><br><br><br><br><br>".$totalcapaian."<br>".$tw1capaian."<br>".$tw2capaian."<br>".$tw3capaian;
                        return view('spm.ibuhamil.pelayanan.tambah', compact('nik','data','datanik','totalcapaian','super_pel_total'));
                    }
                    else{
                        return view('spm.ibuhamil.pelayanan.masukan_nik')->with('alert','NIK belum terdaftar kehamilannya ');
                    }
                }
                else{
                    return view('spm.ibuhamil.pelayanan.masukan_nik')->with('alert','NIK belum berusia 22 tahun');                    
                }
            }
            else{
                return view('spm.ibuhamil.pelayanan.masukan_nik')->with('alert','NIK bukan beridentitas perempuan');
            }
        }
        else{
            return view('spm.ibuhamil.masukan_nik')->with('alert','NIK tidak terdaftar');
        }
    }

    public function SimpanCatatIbuHamil(Request $request)
    {
        $hayo3bulanlagi = date('Y-m-d', strtotime('+3 month', strtotime($request->tanggal_hamil)));
        $hayo6bulanlagi = date('Y-m-d', strtotime('+6 month', strtotime($request->tanggal_hamil)));
        $hayo9bulanlagi = date('Y-m-d', strtotime('+9 month', strtotime($request->tanggal_hamil)));

        if($request->tanggal_pelayanan <= $hayo3bulanlagi && $request->tanggal_pelayanan > $request->tanggal_hamil){
            $jenis_tw = 1;
        }else if($request->tanggal_pelayanan <= $hayo6bulanlagi && $request->tanggal_pelayanan > $hayo3bulanlagi){
            $jenis_tw = 2;
        }else if($request->tanggal_pelayanan <= $hayo9bulanlagi && $request->tanggal_pelayanan > $hayo6bulanlagi){
            $jenis_tw = 3;
        }else if($request->tanggal_pelayanan <= $request->tanggal_hamil){
            $jenis_tw= 0;
        }else if($request->tanggal_pelayanan > $hayo9bulanlagi){
            $jenis_tw = 3;
        }
        if($request->imunisasi_tetanus_tt ==  "on"){$itt = "y";}else{$itt ="t";}
        if($request->skrining_imunisasi ==  "on"){$si = "y";}else{$si="t";}
        if($request->tes_kehamilan ==  "on"){$tk = "y";}else{$tk="t";}
        if($request->tes_hb ==  "on"){$thb = "y";}else{$thb="t";}
        if($request->tes_gd ==  "on"){$tgd = "y";}else{$tgd="t";}
        if($request->tes_protein_urin ==  "on"){$tpu = "y";}else{$tpu="t";}
        if($request->tatalaksana ==  "on"){$tatalaksana = "y";}else{$tatalaksana="t";}
        if($request->temu_wicara ==  "on"){$tw = "y";}else{$tw="t";}
        
        $data = Pelayanan::create([
            'tanggal_pelayanan' => $request->tanggal_pelayanan,
            'tenaga_kerja' => $request->tenaga_kesehatan."_".$request->nama_stk,
            'lokasi' => $request->fasilitas_kesehatan."_".$request->lokasi_pelayanan,
            'jenis_tw' => $jenis_tw,
            'id_spm' => 1,
            'berat_tinggi' => $request->berat."_".$request->tinggi,
            'tekanan_darah' => $request->tekanan_darah1."/".$request->tekanan_darah2,
            'lila' => $request->lila1.",".$request->lila2,
            'tinggi_puncak_rahim' => $request->tprahim1.",".$request->tprahim2,
            'presentasi_djj' => $request->presensijanin."_".$request->djj,
            'imunisasi_tetanus_tt' => $itt."_".$si,
            'tablet_tambah_darah' => $request->tablet_tambah_darah,
            'tes_laboratorium' => $tk."_".$thb."_".$tgd."_".$tpu,
            'tatalaksana' => $tatalaksana,
            'temu_wicara' => $tw
        ]);
        // print $data->id;
        $ibuhamil = IbuHamil::find($request->id);
        // print $request->id;
        $kodepel = $ibuhamil['kode_pelayanan'].",".$data->id;
        $ibuhamil->kode_pelayanan = $kodepel;
        if($ibuhamil->save()){
            return redirect('/pelayanan-ibu-hamil')->with('alert', 'Berhasil Tambah Pelayanan Ibu Hamil');
        }  
        else{
            return redirect('/catat-ibu-hamil')->with('alert', 'Gagal Tambah Pelayanan Ibu Hamil');
        }
    }
    // public function DataIbuHamil()
    // {
    //     $data = DB::table('spm_ibu_hamil')
    //     ->join('warga','spm_ibu_hamil.nik','=','warga.nik')
    //     ->select('spm_ibu_hamil.*','warga.*')
    //     ->get();

    //     $bmelahirkan = DB::table('spm_ibu_hamil')
    //     ->join('warga','spm_ibu_hamil.nik','=','warga.nik')
    //     ->select('spm_ibu_hamil.*','warga.*')
    //     ->where('status_kehamilan','=','belum melahirkan')
    //     ->get();

    //     $sesuai = DB::table('spm_ibu_hamil')
    //     ->join('warga','spm_ibu_hamil.nik','=','warga.nik')
    //     ->select('spm_ibu_hamil.*','warga.*')
    //     ->where('status_kehamilan','=','sesuai spm')
    //     ->get();

    //     $tidaksesuai = DB::table('spm_ibu_hamil')
    //     ->join('warga','spm_ibu_hamil.nik','=','warga.nik')
    //     ->select('spm_ibu_hamil.*','warga.*')
    //     ->where('status_kehamilan','=','tidak sesuai spm')
    //     ->get();

    //     $total_belum_melahirkan = $bmelahirkan->count();
    //     $total_sesuai_spm = $sesuai->count();
    //     $total_tsesuai_spm = $tidaksesuai->count();
    //     $total_ibu_hamil = $data->count();

    //     foreach($data as $index =>$ibuhamil){
    //         $kodepel = $ibuhamil->kode_pelayanan;
    //         $pkodpel = explode(',', $kodepel);
    //         $pel = []; $pel2 = []; $pel3 = [];                        
    //         foreach($pkodpel as $i =>$val){
    //             $data_pelayanan[$i] = Pelayanan::where('id_pelayanan', $val)->first();
    //             if($data_pelayanan[$i]['jenis_tw'] == 1){
    //                 $cek[$i] = SPMController::Cek_Capaian($data_pelayanan[$i]['berat_tinggi'],
    //                 $data_pelayanan[$i]['tekanan_darah'], $data_pelayanan[$i]['lila'],
    //                 $data_pelayanan[$i]['tinggi_puncak_rahim'], $data_pelayanan[$i]['presentasi_djj'],
    //                 $data_pelayanan[$i]['imunisasi_tetanus_tt'], $data_pelayanan[$i]['tablet_tambah_darah'],
    //                 $data_pelayanan[$i]['tes_laboratorium'], $data_pelayanan[$i]['tatalaksana'],
    //                 $data_pelayanan[$i]['temu_wicara'], $i);
    //             }
    //             elseif($data_pelayanan[$i]['jenis_tw'] == 2){
    //                 $cek2[$i] = SPMController::Cek_Capaian($data_pelayanan[$i]['berat_tinggi'],
    //                 $data_pelayanan[$i]['tekanan_darah'], $data_pelayanan[$i]['lila'],
    //                 $data_pelayanan[$i]['tinggi_puncak_rahim'], $data_pelayanan[$i]['presentasi_djj'],
    //                 $data_pelayanan[$i]['imunisasi_tetanus_tt'], $data_pelayanan[$i]['tablet_tambah_darah'],
    //                 $data_pelayanan[$i]['tes_laboratorium'], $data_pelayanan[$i]['tatalaksana'],
    //                 $data_pelayanan[$i]['temu_wicara'], $i);
    //             }
    //             elseif($data_pelayanan[$i]['jenis_tw'] == 3){
    //                 $cek3[$i] = SPMController::Cek_Capaian($data_pelayanan[$i]['berat_tinggi'],
    //                 $data_pelayanan[$i]['tekanan_darah'], $data_pelayanan[$i]['lila'],
    //                 $data_pelayanan[$i]['tinggi_puncak_rahim'], $data_pelayanan[$i]['presentasi_djj'],
    //                 $data_pelayanan[$i]['imunisasi_tetanus_tt'], $data_pelayanan[$i]['tablet_tambah_darah'],
    //                 $data_pelayanan[$i]['tes_laboratorium'], $data_pelayanan[$i]['tatalaksana'],
    //                 $data_pelayanan[$i]['temu_wicara'], $i);
    //             }
    //             $pel[$i] =0;
    //             $pel2[$i] =0;
    //             $pel3[$i] =0;
    //         }
    //         for($i=0;$i<10;$i++){
    //             $hitung = ""; $hitung2 = ""; $hitung3 = "";                                                
    //             $tablet =0;
    //             $tablet2 =0;
    //             $tablet3 =0;
    //             foreach($pkodpel as $j =>$val){
    //                 $data_pelayanan[$j] = Pelayanan::where('id_pelayanan', $val)->first();
    //                 if($data_pelayanan[$j]['jenis_tw'] == 1){
    //                     $hitung .= $cek[$j][$i][$j];
    //                 }
    //                 if($data_pelayanan[$j]['jenis_tw'] == 2){
    //                     $hitung2 .= $cek2[$j][$i][$j]; 
    //                 }
    //                 if($data_pelayanan[$j]['jenis_tw'] == 3){
    //                     $hitung3 .= $cek3[$j][$i][$j]; 
    //                 }
    //             }
    //             $pos = strpos($hitung, "x");
    //             if($pos === false){
    //                 $pos2 = strpos($hitung, "y");
    //                 if($pos2 === false){
    //                 }else{
    //                     $pos3 = strpos($hitung, "z");
    //                     if($pos3 === false){
    //                     }
    //                     else{
    //                         if($pos2 > $pos3){
    //                             $pel[$pos2]++;                                    
    //                         }
    //                         else{
    //                             $pel[$pos3]++;
    //                         }
    //                     }
    //                 }
    //             }else{
    //                 $pel[$pos]++;
    //             }
    //             $pos = strpos($hitung2, "x");
    //             if($pos === false){
    //                 $pos2 = strpos($hitung2, "y");
    //                 if($pos2 === false){
    //                 }else{
    //                     $pos3 = strpos($hitung2, "z");
    //                     if($pos3 === false){
    //                     }
    //                     else{
    //                         if($pos2 > $pos3){
    //                             $pel2[$pos2]++;                                    
    //                         }
    //                         else{
    //                             $pel2[$pos3]++;
    //                         }
    //                     }
    //                 }
    //             }else{
    //                 $pel2[$pos]++;
    //             }
    //             $pos = strpos($hitung3, "x");
    //             if($pos === false){
    //                 $pos2 = strpos($hitung3, "y");
    //                 if($pos2 === false){
    //                 }else{
    //                     $pos3 = strpos($hitung3, "z");
    //                     if($pos3 === false){
    //                     }
    //                     else{
    //                         if($pos2 > $pos3){
    //                             $pel3[$pos2]++;                                    
    //                         }
    //                         else{
    //                             $pel3[$pos3]++;
    //                         }
    //                     }
    //                 }
    //             }else{
    //                 $pel3[$pos]++;
    //             }                        
    //         }
    //         $pel_total =0;
    //         $pel_total2 =0;
    //         $pel_total3 =0;
    //         foreach($pkodpel as $i =>$val){
    //             $data_pelayanan[$i] = Pelayanan::where('id_pelayanan', $val)->first();
    //             if($data_pelayanan[$i]['jenis_tw'] == 1){
    //                 if(intval($cek[$i][6][$i]) > 0 ){
    //                     $tablet += intval($cek[$i][6][$i]);
    //                     if($tablet == 30){
    //                         $pel[$i]++;
    //                     }
    //                 }
    //                 $pel_total += $pel[$i];
    //             }
    //             else if($data_pelayanan[$i]['jenis_tw'] == 2){
    //                 if(intval($cek2[$i][6][$i]) > 0 ){
    //                     $tablet2 += intval($cek2[$i][6][$i]);
    //                     if($tablet2 == 30){
    //                         $pel2[$i]++;
    //                     }
    //                 }
    //                 $pel_total2 += $pel2[$i];
    //             }
    //             else if($data_pelayanan[$i]['jenis_tw'] == 3){
    //                 if(intval($cek3[$i][6][$i]) > 0 ){
    //                     $tablet3 += intval($cek3[$i][6][$i]);
    //                     if($tablet3 == 30){
    //                         $pel3[$i]++;
    //                     }
    //                 }
    //                 $pel_total3 += $pel3[$i];
    //             }
    //         }
    //         $super_pel_total[$index] = $pel_total + $pel_total2 + $pel_total3;
    //         $totalcapaian[$index] = ($super_pel_total[$index] * 100) / 40; 
    
    //         $tw1capaian[$index] = ($pel_total * 100) / 10;
    //         $tw2capaian[$index] = ($pel_total2 * 100) / 10;
    //         $tw3capaian[$index] = ($pel_total3 * 100) / 20;
    //     }
    //     return view('spm.ibuhamil.data',compact('data','totalcapaian','total_ibu_hamil','total_sesuai_spm','total_tsesuai_spm', 'total_belum_melahirkan'));
    // }
    
    public function PelayananIbuHamil()
    {
        $spm_ibu_hamil = IbuHamil::all();

        $pel_by_id = [];
        $pel_by_id2 = [];
        $pel_by_id3 = [];
            
        foreach($spm_ibu_hamil as $urutan => $val){
            $spm_ibu_hamil[$urutan] = Warga::where('nik', $val->nik)->first();
            $nikspm_ibu_hamil[$urutan] = $val->nik;
            $kodpel[$urutan] =  explode(',',$val->kode_pelayanan);
            // print_r($kodpel[$i]);
            $pel = []; $pel2 = []; $pel3 = [];
            foreach($kodpel[$urutan] as $index => $value){
                // print "......".$value."......";
                $data[$urutan][$index] = Pelayanan::where('id_pelayanan', $value)->first();
                // print $data[$i][$index];
                if($data[$urutan][$index]['jenis_tw'] == 1){
                    $cek[$index] = SPMController::Cek_Capaian($data[$urutan][$index]['berat_tinggi'],
                    $data[$urutan][$index]['tekanan_darah'], $data[$urutan][$index]['lila'],
                    $data[$urutan][$index]['tinggi_puncak_rahim'], $data[$urutan][$index]['presentasi_djj'],
                    $data[$urutan][$index]['imunisasi_tetanus_tt'], $data[$urutan][$index]['tablet_tambah_darah'],
                    $data[$urutan][$index]['tes_laboratorium'], $data[$urutan][$index]['tatalaksana'],
                    $data[$urutan][$index]['temu_wicara'], $index);
                }
                if($data[$urutan][$index]['jenis_tw'] == 2){
                    $cek2[$index] = SPMController::Cek_Capaian($data[$urutan][$index]['berat_tinggi'],
                    $data[$urutan][$index]['tekanan_darah'], $data[$urutan][$index]['lila'],
                    $data[$urutan][$index]['tinggi_puncak_rahim'], $data[$urutan][$index]['presentasi_djj'],
                    $data[$urutan][$index]['imunisasi_tetanus_tt'], $data[$urutan][$index]['tablet_tambah_darah'],
                    $data[$urutan][$index]['tes_laboratorium'], $data[$urutan][$index]['tatalaksana'],
                    $data[$urutan][$index]['temu_wicara'], $index);
                }
                if($data[$urutan][$index]['jenis_tw'] == 3){
                    $cek3[$index] = SPMController::Cek_Capaian($data[$urutan][$index]['berat_tinggi'],
                    $data[$urutan][$index]['tekanan_darah'], $data[$urutan][$index]['lila'],
                    $data[$urutan][$index]['tinggi_puncak_rahim'], $data[$urutan][$index]['presentasi_djj'],
                    $data[$urutan][$index]['imunisasi_tetanus_tt'], $data[$urutan][$index]['tablet_tambah_darah'],
                    $data[$urutan][$index]['tes_laboratorium'], $data[$urutan][$index]['tatalaksana'],
                    $data[$urutan][$index]['temu_wicara'], $index);
                }
                $pel[$index] =0;
                $pel2[$index] =0;
                $pel3[$index] =0;
            }
            for($i=0;$i<10;$i++){
                $hitung = ""; $hitung2 = ""; $hitung3 = "";
                $tablet =0;
                $tablet2 =0;
                $tablet3 =0;
                foreach($kodpel[$urutan] as $j => $value){
                    $data[$urutan][$j] = Pelayanan::where('id_pelayanan', $value)->first();
                    if($data[$urutan][$j]['jenis_tw'] == 1){
                        $hitung .= $cek[$j][$i][$j];
                    }
                    if($data[$urutan][$j]['jenis_tw'] == 2){
                        $hitung2 .= $cek2[$j][$i][$j]; 
                    }
                    if($data[$urutan][$j]['jenis_tw'] == 3){
                        $hitung3 .= $cek3[$j][$i][$j]; 
                    }
                }
                $pos = strpos($hitung, "x");
                if($pos === false){
                    $pos2 = strpos($hitung, "y");
                    if($pos2 === false){
                    }else{
                        $pos3 = strpos($hitung, "z");
                        if($pos3 === false){
                        }
                        else{
                            if($pos2 > $pos3){
                                $pel[$pos2]++;                                    
                            }
                            else{
                                $pel[$pos3]++;
                            }
                        }
                    }
                }else{
                    $pel[$pos]++;
                }
                $pos = strpos($hitung2, "x");
                if($pos === false){
                    $pos2 = strpos($hitung2, "y");
                    if($pos2 === false){
                    }else{
                        $pos3 = strpos($hitung2, "z");
                        if($pos3 === false){
                        }
                        else{
                            if($pos2 > $pos3){
                                $pel2[$pos2]++;                                    
                            }
                            else{
                                $pel2[$pos3]++;
                            }
                        }
                    }
                }else{
                    $pel2[$pos]++;
                }
                $pos = strpos($hitung3, "x");
                if($pos === false){
                    $pos2 = strpos($hitung3, "y");
                    if($pos2 === false){
                    }else{
                        $pos3 = strpos($hitung3, "z");
                        if($pos3 === false){
                        }
                        else{
                            if($pos2 > $pos3){
                                $pel3[$pos2]++;                                    
                            }
                            else{
                                $pel3[$pos3]++;
                            }
                        }
                    }
                }else{
                    $pel3[$pos]++;
                }
            }
            $pel_total[$urutan] =0;
            $pel_total2[$urutan] =0;
            $pel_total3[$urutan] =0;
            foreach($kodpel[$urutan] as $j => $value){
                $data[$urutan][$j] = Pelayanan::where('id_pelayanan', $value)->first();
                if($data[$urutan][$j]['jenis_tw'] == 1){
                    if(intval($cek[$j][6][$j]) > 0 ){
                        $tablet += intval($cek[$j][6][$j]);
                        if($tablet == 30){
                            $pel[$j]++;
                        }
                    }
                    $pel_total[$urutan] += $pel[$j];
                    $pel_by_id[$urutan][$value] = $pel[$j];
                }
                else if($data[$urutan][$j]['jenis_tw'] == 2){
                    if(intval($cek2[$j][6][$j]) > 0 ){
                        $tablet2 += intval($cek2[$j][6][$j]);
                        if($tablet2 == 30){
                            $pel2[$j]++;
                        }
                    }
                    $pel_total2[$urutan] += $pel2[$j];
                    $pel_by_id2[$urutan][$value] = $pel2[$j];
                }
                else if($data[$urutan][$j]['jenis_tw'] == 3){
                    if(intval($cek3[$j][6][$j]) > 0 ){
                        $tablet3 += intval($cek3[$j][6][$j]);
                        if($tablet3 == 30){
                            $pel3[$j]++;
                        }
                    }
                    $pel_total3[$urutan] += $pel3[$j];
                    $pel_by_id3[$urutan][$value] = $pel3[$j];
                }
            }
            // print_r($pel_by_id);
            $super_pel_total[$urutan] = $pel_total[$urutan] + $pel_total2[$urutan] + $pel_total3[$urutan];
            $totalcapaian[$urutan] = ($super_pel_total[$urutan] * 100) / 40; 
    
            $tw1capaian[$urutan] = ($pel_total[$urutan] * 100) / 10;
            $tw2capaian[$urutan] = ($pel_total2[$urutan] * 100) / 10;
            $tw3capaian[$urutan] = ($pel_total3[$urutan] * 100) / 20;
            // print_r($data);
        }
        // print_r($data);
        // print_r($spm_ibu_hamil);
        return view('spm.ibuhamil.pelayanan.data',compact('data', 'spm_ibu_hamil', 'nikspm_ibu_hamil','totalcapaian','pel_total','pel_total2','pel_total3','super_pel_total','pel','pel2','pel3','pel_by_id','pel_by_id2','pel_by_id3'));
        // return view('spm.ibuhamil.pelayanan.data',compact('data', 'datanik','totalcapaian','pel_total','pel_total2','pel_total3','super_pel_total','pel','pel2','pel3'));
    }

    public function TambahIbuHamil()
    {
        $kriteriaibuhamil = [];
        $nikcaibuhamil = [];
        $warga = Warga::all();
        foreach($warga as $i => $val){
            if($val['jenis_kelamin'] == "P"){
                $usia = SPMController::Umur($val['tgl_lahir']);
                if($usia >= 22){
                    $data = IbuHamil::where('nik', $val['nik'])->where('status_kehamilan','belum melahirkan')->first();
                    if(!$data){
                        $kriteriaibuhamil[$i] = $val['nama'];
                        $nikcaibuhamil[$i] = $val['nik'];
                    }
                }
            }
        }
        return view('spm.ibuhamil.tambah',compact('kriteriaibuhamil','nikcaibuhamil'));
    }

    public function SimpanIbuHamilBaru(Request $request)
    {
        print $request->nik;
        print "<br>";
        print $request->tanggal_hamil;

        $data = New IbuHamil();
        $data->nik = $request->nik;
        $data->tanggal_hamil = $request->tanggal_hamil;
        $data->status_kehamilan = "Belum Melahirkan";
        if($data->save()){
            return redirect('/data-ibu-hamil')->with('alert', 'Berhasil Daftar Ibu Hamil');
        }
        else{
            return redirect('/tambah-ibu-hamil')->with('alert', 'Gagal Daftar Ibu Hamil');
        }
    }

    public function DetailIbuHamil($nik)
    {        
        // print $nik;
        $ibuhamil = DB::table('spm_ibu_hamil')
        ->join('warga','spm_ibu_hamil.nik','=','warga.nik')
        ->select('spm_ibu_hamil.*','warga.*')
        ->where('spm_ibu_hamil.nik',$nik)
        ->get();

        $data = IbuHamil::where('nik', $nik)->first();
        $kodepel = $data['kode_pelayanan'];
        $pkodpel = explode(',', $kodepel);
        $pel = []; $pel2 = []; $pel3 = [];                                    
        foreach($pkodpel as $i =>$val){
            $data_pelayanan[$i] = Pelayanan::where('id_pelayanan', $val)->first();
            if($data_pelayanan[$i]['jenis_tw'] == 1){
                $cek[$i] = SPMController::Cek_Capaian($data_pelayanan[$i]['berat_tinggi'],
                $data_pelayanan[$i]['tekanan_darah'], $data_pelayanan[$i]['lila'],
                $data_pelayanan[$i]['tinggi_puncak_rahim'], $data_pelayanan[$i]['presentasi_djj'],
                $data_pelayanan[$i]['imunisasi_tetanus_tt'], $data_pelayanan[$i]['tablet_tambah_darah'],
                $data_pelayanan[$i]['tes_laboratorium'], $data_pelayanan[$i]['tatalaksana'],
                $data_pelayanan[$i]['temu_wicara'], $i);
                // $pel[$i] = 0;
            }
            elseif($data_pelayanan[$i]['jenis_tw'] == 2){
                $cek2[$i] = SPMController::Cek_Capaian($data_pelayanan[$i]['berat_tinggi'],
                $data_pelayanan[$i]['tekanan_darah'], $data_pelayanan[$i]['lila'],
                $data_pelayanan[$i]['tinggi_puncak_rahim'], $data_pelayanan[$i]['presentasi_djj'],
                $data_pelayanan[$i]['imunisasi_tetanus_tt'], $data_pelayanan[$i]['tablet_tambah_darah'],
                $data_pelayanan[$i]['tes_laboratorium'], $data_pelayanan[$i]['tatalaksana'],
                $data_pelayanan[$i]['temu_wicara'], $i);
                // $pel2[$i] = 0;
            }
            elseif($data_pelayanan[$i]['jenis_tw'] == 3){
                $cek3[$i] = SPMController::Cek_Capaian($data_pelayanan[$i]['berat_tinggi'],
                $data_pelayanan[$i]['tekanan_darah'], $data_pelayanan[$i]['lila'],
                $data_pelayanan[$i]['tinggi_puncak_rahim'], $data_pelayanan[$i]['presentasi_djj'],
                $data_pelayanan[$i]['imunisasi_tetanus_tt'], $data_pelayanan[$i]['tablet_tambah_darah'],
                $data_pelayanan[$i]['tes_laboratorium'], $data_pelayanan[$i]['tatalaksana'],
                $data_pelayanan[$i]['temu_wicara'], $i);
                // $pel3[$i] = 0;
            }
            $pel[$i] =0;
            $pel2[$i] =0;
            $pel3[$i] =0;
        }
        for($i=0;$i<10;$i++){
            $hitung = ""; $hitung2 = ""; $hitung3 = "";                                                
            $tablet =0;
            $tablet2 =0;
            $tablet3 =0;
            foreach($pkodpel as $j =>$val){
                $data_pelayanan[$j] = Pelayanan::where('id_pelayanan', $val)->first();
                if($data_pelayanan[$j]['jenis_tw'] == 1){
                    $hitung .= $cek[$j][$i][$j];
                }
                if($data_pelayanan[$j]['jenis_tw'] == 2){
                    $hitung2 .= $cek2[$j][$i][$j]; 
                }
                if($data_pelayanan[$j]['jenis_tw'] == 3){
                    $hitung3 .= $cek3[$j][$i][$j]; 
                }
            }
            $pos = strpos($hitung, "x");
            if($pos === false){
                $pos2 = strpos($hitung, "y");
                if($pos2 === false){
                }else{
                    $pos3 = strpos($hitung, "z");
                    if($pos3 === false){
                    }
                    else{
                        if($pos2 > $pos3){
                            $pel[$pos2]++;                                    
                        }
                        else{
                            $pel[$pos3]++;
                        }
                    }
                }
            }else{
                $pel[$pos]++;
            }
            $pos = strpos($hitung2, "x");
            if($pos === false){
                $pos2 = strpos($hitung2, "y");
                if($pos2 === false){
                }else{
                    $pos3 = strpos($hitung2, "z");
                    if($pos3 === false){
                    }
                    else{
                        if($pos2 > $pos3){
                            $pel2[$pos2]++;                                    
                        }
                        else{
                            $pel2[$pos3]++;
                        }
                    }
                }
            }else{
                $pel2[$pos]++;
            }
            $pos = strpos($hitung3, "x");
            if($pos === false){
                $pos2 = strpos($hitung3, "y");
                if($pos2 === false){
                }else{
                    $pos3 = strpos($hitung3, "z");
                    if($pos3 === false){
                    }
                    else{
                        if($pos2 > $pos3){
                            $pel3[$pos2]++;                                    
                        }
                        else{
                            $pel3[$pos3]++;
                        }
                    }
                }
            }else{
                $pel3[$pos]++;
            }       
        }
        // print "<br>----------<br>";
        // print_r($pel);print "<br>";
        // print_r($pel2);print "<br>";
        // print_r($pel3);print "<br>";
        // print "<br>----------<br>";
        $pel_total =0;
        $pel_total2 =0;
        $pel_total3 =0;
        foreach($pkodpel as $i =>$val){
            $data_pelayanan[$i] = Pelayanan::where('id_pelayanan', $val)->first();
            if($data_pelayanan[$i]['jenis_tw'] == 1){
                 
                if(intval($cek[$i][6][$i]) > 0 ){
                    $tablet += intval($cek[$i][6][$i]);
                    if($tablet == 30){
                        $pel[$i]++;
                    }
                }
                // print $pel[$i];
                $pel_total += $pel[$i];
            }
            else if($data_pelayanan[$i]['jenis_tw'] == 2){
                if(intval($cek2[$i][6][$i]) > 0 ){
                    $tablet2 += intval($cek2[$i][6][$i]);
                    if($tablet2 == 30){
                        $pel2[$i]++;
                    }
                }
                
                // print $pel2[$i];
                $pel_total2 += $pel2[$i];
            }
            else if($data_pelayanan[$i]['jenis_tw'] == 3){
                if(intval($cek3[$i][6][$i]) > 0 ){
                    $tablet3 += intval($cek3[$i][6][$i]);
                    if($tablet3 == 30){
                        $pel3[$i]++;
                    }
                }
                // print $pel3[$i];
                $pel_total3 += $pel3[$i];
            }
        }
        // print "<br><br>".$pel_total."-------".$tablet;
        // print "<br><br>".$pel_total2."-------".$tablet2;
        // print "<br><br>".$pel_total3."-------".$tablet3;
        // print "<br><br><br><br>";
        // print "<br><br>";
        $super_pel_total = $pel_total + $pel_total2 + $pel_total3;
        $totalcapaian = ($super_pel_total * 100) / 40; 

        $tw1capaian = ($pel_total * 100) / 10;
        $tw2capaian = ($pel_total2 * 100) / 10;
        $tw3capaian = ($pel_total3 * 100) / 20;
        // print $super_pel_total."<br><br><br><br><br><br><br><br>".$totalcapaian."<br>".$tw1capaian."<br>".$tw2capaian."<br>".$tw3capaian;
        return view('spm.ibuhamil.detail', compact('ibuhamil','tw1capaian','tw2capaian','tw3capaian','totalcapaian','super_pel_total','data_pelayanan', 'pel', 'pel2', 'pel3'));        
    }
    
    public function UbahIbuHamil($id)
    {
        $kriteriaibuhamil = [];
        $nikcaibuhamil = [];
        $warga = Warga::all();
        foreach($warga as $i => $val){
            if($val['jenis_kelamin'] == "P"){
                $usia = SPMController::Umur($val['tgl_lahir']);
                if($usia >= 22){
                    $data = IbuHamil::where('nik', $val['nik'])->where('status_kehamilan','belum melahirkan')->first();
                    if(!$data){
                        $kriteriaibuhamil[$i] = $val['nama'];
                        $nikcaibuhamil[$i] = $val['nik'];
                    }
                }
            }
        }
        
        $data = IbuHamil::where('id', $id)->first();
        $nik = $data->nik;
        $datanik = Warga::where('nik', $nik)->first();
        $nama = $datanik->nama;
        $kodepel = $data['kode_pelayanan'];
        $pkodpel = explode(',', $kodepel);
        $pel = []; $pel2 = []; $pel3 = [];                        
        $hitung = []; $hitung2 = []; $hitung3 = [];                        
        // $hitung = ""; $hitung2 = ""; $hitung3 = "";                        
        foreach($pkodpel as $i =>$val){
            $data_pelayanan[$i] = Pelayanan::where('id_pelayanan', $val)->first();
            if($data_pelayanan[$i]['jenis_tw'] == 1){
                $cek[$i] = SPMController::Cek_Capaian($data_pelayanan[$i]['berat_tinggi'],
                $data_pelayanan[$i]['tekanan_darah'], $data_pelayanan[$i]['lila'],
                $data_pelayanan[$i]['tinggi_puncak_rahim'], $data_pelayanan[$i]['presentasi_djj'],
                $data_pelayanan[$i]['imunisasi_tetanus_tt'], $data_pelayanan[$i]['tablet_tambah_darah'],
                $data_pelayanan[$i]['tes_laboratorium'], $data_pelayanan[$i]['tatalaksana'],
                $data_pelayanan[$i]['temu_wicara'], $i);
                // $pel[$i] = 0;
            }
            elseif($data_pelayanan[$i]['jenis_tw'] == 2){
                $cek2[$i] = SPMController::Cek_Capaian($data_pelayanan[$i]['berat_tinggi'],
                $data_pelayanan[$i]['tekanan_darah'], $data_pelayanan[$i]['lila'],
                $data_pelayanan[$i]['tinggi_puncak_rahim'], $data_pelayanan[$i]['presentasi_djj'],
                $data_pelayanan[$i]['imunisasi_tetanus_tt'], $data_pelayanan[$i]['tablet_tambah_darah'],
                $data_pelayanan[$i]['tes_laboratorium'], $data_pelayanan[$i]['tatalaksana'],
                $data_pelayanan[$i]['temu_wicara'], $i);
                // $pel2[$i] = 0;
            }
            elseif($data_pelayanan[$i]['jenis_tw'] == 3){
                $cek3[$i] = SPMController::Cek_Capaian($data_pelayanan[$i]['berat_tinggi'],
                $data_pelayanan[$i]['tekanan_darah'], $data_pelayanan[$i]['lila'],
                $data_pelayanan[$i]['tinggi_puncak_rahim'], $data_pelayanan[$i]['presentasi_djj'],
                $data_pelayanan[$i]['imunisasi_tetanus_tt'], $data_pelayanan[$i]['tablet_tambah_darah'],
                $data_pelayanan[$i]['tes_laboratorium'], $data_pelayanan[$i]['tatalaksana'],
                $data_pelayanan[$i]['temu_wicara'], $i);
                // $pel3[$i] = 0;
            }
            $pel[$i] =0;
            $pel2[$i] =0;
            $pel3[$i] =0;
        }
        for($i=0;$i<10;$i++){
            $tablet =0;
            $tablet2 =0;
            $tablet3 =0;
            foreach($pkodpel as $j =>$val){
                $data_pelayanan[$j] = Pelayanan::where('id_pelayanan', $val)->first();
                if($data_pelayanan[$j]['jenis_tw'] == 1){
                    $hitung[$j]= $cek[$j][$i][$j];
                    // print $cek[$j][$i][$j];                                                                                     
                }
                if($data_pelayanan[$j]['jenis_tw'] == 2){
                    $hitung2[$j]= $cek2[$j][$i][$j]; 
                    // print $cek2[$j][$i][$j];                                                                                     
                }
                if($data_pelayanan[$j]['jenis_tw'] == 3){
                    $hitung3[$j]= $cek3[$j][$i][$j]; 
                    // print $cek3[$j][$i][$j];                                                                                     
                }
            }
            // print "..."; print_r($hitung); print "VVVVVVV";
            // print "..."; print_r($hitung2); print "VVVVVVV";
            // print "..."; print_r($hitung3); print "VVVVVVV";
            $indexxhitung = array_search("x", $hitung);
            if($indexxhitung != null){
                $pel[$indexxhitung]++;
                // print "NNNNNNN indexxhitung : ".$indexxhitung;
            }
            else{
                $indexyhitung = array_search("y", $hitung);
                if($indexyhitung != null){
                    $indexzhitung = array_search("z", $hitung);
                    if($indexzhitung != null){
                        if($indexyhitung > $indexzhitung){
                            $pel[$indexyhitung]++;
                            // print "NNNNNNN indexyhitung : ".$indexyhitung;                                    
                        }
                        else{
                            $pel[$indexzhitung]++;
                            // print "NNNNNNN indexzhitung : ".$indexzhitung;
                        }
                    }
                }
            }
            $indexxhitung2 = array_search("x", $hitung2);
            if($indexxhitung2 != null){
                $pel2[$indexxhitung2]++;
                // print "NNNNNNNN indexxhitung2 : ".$indexxhitung2;
            }
            else{
                $indexyhitung2 = array_search("y", $hitung2);
                if($indexyhitung2 != null){
                    $indexzhitung2 = array_search("z", $hitung2);
                    if($indexzhitung2 != null){
                        if($indexyhitung2 > $indexzhitung2){
                            $pel2[$indexyhitung2]++;
                            // print "NNNNNNN indexyhitung2 : ".$indexyhitung2;                                    
                        }
                        else{
                            $pel2[$indexzhitung2]++;
                            // print "NNNNNNN indexzhitung2 : ".$indexzhitung2;
                        }
                    }
                }
            }
            $indexxhitung3 = array_search("x", $hitung3);
            if($indexxhitung3 != null){
                $pel3[$indexxhitung3]++;
                // print "NNNNNNNN indexxhitung3 : ".$indexxhitung3;
            }
            else{
                $indexyhitung3 = array_search("y", $hitung3);
                if($indexyhitung3 != null){
                    $indexzhitung3 = array_search("z", $hitung3);
                    if($indexzhitung3 != null){
                        if($indexyhitung3 > $indexzhitung3){
                            $pel3[$indexyhitung3]++;
                            // print "NNNNNNN indexyhitung3 : ".$indexyhitung3;                                    
                        }
                        else{
                            $pel3[$indexzhitung3]++;
                            // print "NNNNNNN indexzhitung3 : ".$indexzhitung3;
                        }
                    }
                }
            }                        
            // print "<br>";       
        }
        // print "<br>----------<br>";
        // print_r($pel);print "<br>";
        // print_r($pel2);print "<br>";
        // print_r($pel3);print "<br>";
        // print "<br>----------<br>";
        $pel_total =0;
        $pel_total2 =0;
        $pel_total3 =0;
        foreach($pkodpel as $i =>$val){
            $data_pelayanan[$i] = Pelayanan::where('id_pelayanan', $val)->first();
            if($data_pelayanan[$i]['jenis_tw'] == 1){
                 
                if(intval($cek[$i][6][$i]) > 0 ){
                    $tablet += intval($cek[$i][6][$i]);
                    if($tablet == 30){
                        $pel[$i]++;
                    }
                }
                // print $pel[$i];
                $pel_total += $pel[$i];
            }
            else if($data_pelayanan[$i]['jenis_tw'] == 2){
                if(intval($cek2[$i][6][$i]) > 0 ){
                    $tablet2 += intval($cek2[$i][6][$i]);
                    if($tablet2 == 30){
                        $pel2[$i]++;
                    }
                }
                
                // print $pel2[$i];
                $pel_total2 += $pel2[$i];
            }
            else if($data_pelayanan[$i]['jenis_tw'] == 3){
                if(intval($cek3[$i][6][$i]) > 0 ){
                    $tablet3 += intval($cek3[$i][6][$i]);
                    if($tablet3 == 30){
                        $pel3[$i]++;
                    }
                }
                // print $pel3[$i];
                $pel_total3 += $pel3[$i];
            }
        }
        // print "<br><br>".$pel_total."-------".$tablet;
        // print "<br><br>".$pel_total2."-------".$tablet2;
        // print "<br><br>".$pel_total3."-------".$tablet3;
        // print "<br><br><br><br>";
        // print "<br><br>";
        $super_pel_total = $pel_total + $pel_total2 + $pel_total3;
        $totalcapaian = ($super_pel_total * 100) / 40; 

        $tw1capaian = ($pel_total * 100) / 10;
        $tw2capaian = ($pel_total2 * 100) / 10;
        $tw3capaian = ($pel_total3 * 100) / 20;
        return view('spm.ibuhamil.ubah', compact('data','nik','nama','totalcapaian','kriteriaibuhamil','nikcaibuhamil'));        
    }
    
    public function DetailPelayananIbuHamil($id)
    {
        $pelayanan = Pelayanan::where('id_pelayanan', $id)->first();
        // print $pelayanan;
        if(explode('_',$pelayanan->lokasi)[1] == null){
            $fasilitas = $pelayanan->$lokasi;
            $lokasi = "Tidak diisi";
        } 
        else{
            $fasilitas = explode('_',$pelayanan->lokasi)[0];
            $lokasi = explode('_',$pelayanan->lokasi)[1];
        }
        if(explode('_',$pelayanan->tenaga_kerja)[1] == null){
            $tenaga_kerja = $pelayanan->$tenaga_kerja;
            $nama_stk = "Tidak diisi";
        } 
        else{
            $tenaga_kerja = explode('_',$pelayanan->tenaga_kerja)[0];
            $nama_stk = explode('_',$pelayanan->tenaga_kerja)[1];
        }

        if($pelayanan->berat_tinggi == null || explode('_',$pelayanan->berat_tinggi)[1] == null && explode('_',$pelayanan->berat_tinggi)[0] == null){
            $berat = 0;
            $tinggi = 0;
        }
        else if(explode('_',$pelayanan->berat_tinggi)[1] == null){
            $berat = explode('_',$pelayanan->berat_tinggi)[0];
            $tinggi = 0;
        } 
        else if(explode('_',$pelayanan->berat_tinggi)[0] == null){
            $berat = 0;
            $tinggi = explode('_',$pelayanan->berat_tinggi)[1];
        }
        else{
            $berat = explode('_',$pelayanan->berat_tinggi)[0];
            $tinggi = explode('_',$pelayanan->berat_tinggi)[1];
        }

        if($pelayanan->tekanan_darah == null || explode('/',$pelayanan->tekanan_darah)[1] == null && explode('/',$pelayanan->tekanan_darah)[0] == null){
            $tekanan_darah = 0;
            $koma_tekanan_darah = 0;
        }
        else if(explode('/',$pelayanan->tekanan_darah)[0] != null && explode('/',$pelayanan->tekanan_darah)[1] != null){
            $tekanan_darah = explode('/',$pelayanan->tekanan_darah)[0];
            $koma_tekanan_darah = explode('/',$pelayanan->tekanan_darah)[1];
        }   
        else if(explode('/',$pelayanan->tekanan_darah)[1] == null){
            $tekanan_darah = explode('/',$pelayanan->tekanan_darah)[0];
            $koma_tekanan_darah = 0;
        } 
        else if(explode('/',$pelayanan->tekanan_darah)[0] == null){
            $tekanan_darah = 0;
            $koma_tekanan_darah = explode('/',$pelayanan->tekanan_darah)[1];
        }

        if($pelayanan->lila == null){
            $lila = 0;
            $koma_lila = 0;
        }
        else{
            $komalila = strpos($pelayanan->lila,",");
            if($komalila === false){
                $lila = $pelayanan->lila;
                $koma_lila = 0;
            }
            else{
                $lila = explode(',',$pelayanan->lila)[0];
                $koma_lila = explode(',',$pelayanan->lila)[1];
            }
        }

        if($pelayanan->tinggi_puncak_rahim == null){
            $tinggi_puncak_rahim = 0;
            $koma_tinggi_puncak_rahim = 0;
        }
        else{
            $komatprahim = strpos($pelayanan->tinggi_puncak_rahim,",");
            if($komatprahim === false){
                $tinggi_puncak_rahim = $pelayanan->tinggi_puncak_rahim;
                $koma_tinggi_puncak_rahim = 0;
            }
            else{
                $tinggi_puncak_rahim = explode(',',$pelayanan->tinggi_puncak_rahim)[0];
                $koma_tinggi_puncak_rahim = explode(',',$pelayanan->tinggi_puncak_rahim)[1];
            }
        }

        if($pelayanan->presentasi_djj == null || explode('_',$pelayanan->presentasi_djj)[1] == null && explode('_',$pelayanan->presentasi_djj)[0] == null){
            $presentasi = "Tidak diisi";
            $djj = 0;
        }
        else if(explode('_',$pelayanan->presentasi_djj)[0] != null && explode('_',$pelayanan->presentasi_djj)[1] != null){
            $presentasi = explode('_',$pelayanan->presentasi_djj)[0];
            $djj = explode('_',$pelayanan->presentasi_djj)[1];
        }   
        else if(explode('_',$pelayanan->presentasi_djj)[1] == null){
            $presentasi = explode('/',$pelayanan->presentasi_djj)[0];
            $djj = 0;
        } 
        else if(explode('_',$pelayanan->presentasi_djj)[0] == null){
            $presentasi = "Tidak diisi";
            $djj = explode('/',$pelayanan->presentasi_djj)[1];
        }

        if($pelayanan->imunisasi_tetanus_tt == null || explode('_',$pelayanan->imunisasi_tetanus_tt)[1] == null && explode('_',$pelayanan->imunisasi_tetanus_tt)[0] == null){
            $imunisasi_tetanus_tt = "t_t";
        }
        else if(explode('_',$pelayanan->imunisasi_tetanus_tt)[0] == "y" && explode('_',$pelayanan->imunisasi_tetanus_tt)[1] == "y"){
            $imunisasi_tetanus_tt = "y_y";
        }   
        else if(explode('_',$pelayanan->imunisasi_tetanus_tt)[1] == "t"){
            $imunisasi_tetanus_tt = "y_t";
        } 
        else if(explode('_',$pelayanan->imunisasi_tetanus_tt)[0] == "t"){
            $imunisasi_tetanus_tt = "t_y";
        } 

        if($pelayanan->tes_laboratorium == null){
            $tes_laboratorium = "t_t_t_t";
        }
        else{
            $tes_laboratorium = $pelayanan->tes_laboratorium;
        }
        
        if($pelayanan->tatalaksana == null){
            $tatalaksana = "t";
        }
        else{
            $tatalaksana = $pelayanan->tatalaksana;
        }
        
        if($pelayanan->temu_wicara == null){
            $temu_wicara = "t";
        }
        else{
            $temu_wicara = $pelayanan->temu_wicara;
        }

        return view('spm.ibuhamil.pelayanan.detail',compact('pelayanan','fasilitas','lokasi', 'tenaga_kerja','nama_stk','berat','tinggi','tekanan_darah','koma_tekanan_darah','lila','koma_lila','tinggi_puncak_rahim','koma_tinggi_puncak_rahim','presentasi','djj','imunisasi_tetanus_tt','tes_laboratorium','tatalaksana','temu_wicara')); 
    }
    
    public function UbahPelayananIbuHamil($id)
    {
        return view('spm.ibuhamil.pelayanan.ubah');
    }

    public function SimpanUbahCatatIbuHamil()
    {
        return redirect('/pelayanan-ibu-hamil')->with('alert', 'Berhasil Tambah Pelayanan Ibu Hamil');
    }

    public function SimpanUbahIbuHamil(Request $request)
    {
        if($request->status_hamil == "Belum Melahirkan"){
            $request->status_hamil= $request->status_hamil;
        }
        else{
            if($request->totalcapaian == 100){
                $request->status_hamil= "Sesuai SPM";
            }
            else{
                $request->status_hamil= "Tidak Sesuai SPM";
            }
        }
        $ibuhamil = IbuHamil::where('id',$request->id)->first();
        $ibuhamil->tanggal_hamil = $request->tanggal_hamil;
        $ibuhamil->nik = $request->nik;
        $ibuhamil->status_kehamilan = $request->status_hamil;
        if($ibuhamil->save()){
            return redirect('/data-ibu-hamil')->with('alert', 'Berhasil Ubah Ibu Hamil');
        }
        else{
            return redirect('/ubah-ibu-hamil')->with('alert', 'Gagal Ubah Ibu Hamil');
        }
    }

    public function Cek_Capaian($berat_tinggi,$tekanan_darah, $lila,$tinggi_puncak_rahim, $presentasi_djj,$imunisasi_tetanus_tt, $tablet_tambah_darah, $tes_laboratorium, $tatalaksana,$temu_wicara, $i)
    {
        // Berat Tinggi
        if($berat_tinggi != null){
            $bt = explode('_',$berat_tinggi);
            if($bt[0] >1 && $bt[1] > 1){
                $capaian[0][$i] = "x";
            }
            elseif($bt[0] >1){
                $capaian[0][$i] = "y";
            }            
            elseif($bt[1] >1){
                $capaian[0][$i] = "z";
            }
            else{
                $capaian[0][$i] = 0;
            }            
        }
        else{
            $capaian[0][$i] = 0;
        }

        //Tekanan Darah
        if($tekanan_darah != null){
            $td = explode('/',$tekanan_darah);
            if($td[0] >1 && $td[1] > 1){
                $capaian[1][$i] = "x";
            }
            else{
                $capaian[1][$i] = 0;
            }            
        }
        else{
            $capaian[1][$i] = 0;
        }

        //Lila
        if($lila != null){
            $capaian[2][$i] = "x";            
        }
        else{
            $capaian[2][$i] = 0;
        }

        //Tinggi Puncak Rahim
        if($tinggi_puncak_rahim != null){
            $capaian[3][$i] = "x";            
        }
        else{
            $capaian[3][$i] = 0;
        }

        //Presentasi DJJ
        if($presentasi_djj != null){
            $pdjj = explode('_',$presentasi_djj);
            if($pdjj[0] != null && $pdjj[1] >1){
                $capaian[4][$i] = "x";            
            }
            elseif($pdjj[0] != null){
                $capaian[4][$i] = "y";
            }
            elseif($pdjj[1] > 1){
                $capaian[4][$i] = "z";
            }
            else{
                $capaian[4][$i] = 0;
            }
        }
        else{
            $capaian[4][$i] = 0;
        }

        //Imunisasi Tetanus TT
        if($imunisasi_tetanus_tt != null){
            $itt = explode('_',$imunisasi_tetanus_tt);
            if($itt[0] == "y" && $itt[1] == "y"){
                $capaian[5][$i] = "x";            
            }
            elseif($itt[0] == "y"){
                $capaian[5][$i] = "y";
            }
            elseif($itt[1] == "y"){
                $capaian[5][$i] = "z";
            }
            else{
                $capaian[5][$i] = 0;
            }
        }
        else{
            $capaian[5][$i] = 0;
        }
        
        //Tablet Tambah Darah
        if($tablet_tambah_darah != null){
            $capaian[6][$i] = $tablet_tambah_darah;            
        }
        else{
            $capaian[6][$i] = 0;
        }

        //Tes Laboratorium
        if($tes_laboratorium != null){
            $tl = explode('_',$tes_laboratorium);
            if($tl[0] == "y" && $tl[1] == "y" && $tl[2] == "y" && $tl[3] == "y"){
                $capaian[7][$i] = "x";            
            }
            // else if($tl[0] == "y" && $tl[1] != "y" && $tl[2] != "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "a";            
            // }
            // else if($tl[0] != "y" && $tl[1] == "y" && $tl[2] != "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "b";            
            // }
            // else if($tl[0] != "y" && $tl[1] != "y" && $tl[2] == "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "c";            
            // }
            // else if($tl[0] != "y" && $tl[1] != "y" && $tl[2] != "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "d";            
            // }
            // else if($tl[0] == "y" && $tl[1] == "y" && $tl[2] != "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "e";            
            // }
            // else if($tl[0] == "y" && $tl[1] != "y" && $tl[2] == "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "f";            
            // }
            // else if($tl[0] == "y" && $tl[1] != "y" && $tl[2] != "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "g";            
            // }
            // else if($tl[0] != "y" && $tl[1] == "y" && $tl[2] == "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "h";            
            // }
            // else if($tl[0] != "y" && $tl[1] == "y" && $tl[2] != "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "i";            
            // }
            // else if($tl[0] != "y" && $tl[1] != "y" && $tl[2] == "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "j";            
            // }
            // else if($tl[0] == "y" && $tl[1] == "y" && $tl[2] == "y" && $tl[3] != "y"){
            //     $capaian[7][$i] = "k";            
            // }
            // else if($tl[0] == "y" && $tl[1] == "y" && $tl[2] != "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "l";            
            // }
            // else if($tl[0] == "y" && $tl[1] != "y" && $tl[2] == "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "m";            
            // }
            // else if($tl[0] != "y" && $tl[1] == "y" && $tl[2] == "y" && $tl[3] == "y"){
            //     $capaian[7][$i] = "n";            
            // }
            else{
                $capaian[7][$i] = 0;
            }
        }
        else{
            $capaian[7][$i] = 0;
        }

        //Tatalaksana
        if($tatalaksana == "y"){
            $capaian[8][$i] = "x";            
        }
        else{
            $capaian[8][$i] = 0;
        }

        //Temu Wicara
        if($temu_wicara == "y"){
            $capaian[9][$i] = "x";            
        }
        else{
            $capaian[9][$i] = 0;
        }

        return $capaian;
    }

    public function Umur($lahir)
    {
        $pisah_lahir = explode('-',$lahir);
        $thn_lahir = intval($pisah_lahir[0]);
        $bln_lahir = intval($pisah_lahir[1]);
        $tgl_lahir = intval($pisah_lahir[2]);
        $tanggal = intval(date('d'));
        $bulan = intval(date('m'));
        $tahun = intval(date('Y'));
        $usia = $tahun - $thn_lahir;
        if($bulan < $bln_lahir || ($bulan == $bln_lahir && $tanggal < $tgl_lahir)){
            $usia -= 1;
        }

        return $usia;
    }
}
