<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Requests;
use DB;
use Illuminate\Support\Facades\Session;
use Redirect;

use App\User;
use App\Warga;

use Auth;
use Hash;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $user_all = User::all();
        $total_super_admin = 0;
        $total_admin = 0;
        $total_tenaga_kerja =0;
        $total_masyarakat =0;
        $total_user =0;
        foreach($user_all as $i => $user){
            $total_user++;
            if($user['level'] == 1){
                $total_super_admin++;
            }
            else if($user['level'] == 2){
                $total_admin++;
            }
            else if($user['level'] == 3){
                $total_tenaga_kerja++;
            }
            else if($user['level'] == 4){
                $total_masyarakat++;
            }
        }
        
        return view('user.data',compact('user_all','total_super_admin','total_admin','total_tenaga_kerja','total_masyarakat','total_user'));
    }

    public function tambah(){
        $all_warga = Warga::all();
        $list_nik = [];
        $list_name = [];
        foreach($all_warga as $index => $warga){
            $list_nik[$index] = $warga['nik'];
            $list_name[$index] = $warga['nama'];    
        }
        return view('user.tambah',compact('list_nik','list_name'));
    }

    public function save(Request $request){
        // print "Name : ".$request->name."<br>";
        // print "Email : ".$request->email."<br>";
        // print "Level : ".$request->level."<br>";
        // print "Instansi : ".$request->instansi."<br>";
        // print "Username : ".$request->username."<br>";
        // print "Usernamewarga : ".$request->usernamewarga."<br>";
        // print "Password : ".$request->password."<br>";
        // print "Confirm Password : ".$request->confirm_password."<br>";

        if($request->usernamewarga != null ){
            if($request->usernamewarga == "Bukan Masyarakat"){
                $data = User::create([
                    'name' => $request->name,
                    'username' => $request->username,
                    'email' => $request->email,
                    'level' => $request->level,
                    'instansi' => $request->instansi,
                    'password' => bcrypt($request->password)
                ]);
                if($data->save()){
                    return redirect('/users')->with('alert', 'Berhasil Tambah Users');
                }  
                else{
                    return redirect('/tambah-user')->with('alert', 'Gagal Tambah Users');
                }
            }
            else{
                $data = User::create([
                    'name' => $request->name,
                    'username' => explode("_",$request->usernamewarga)[0],
                    'email' => $request->email,
                    'level' => $request->level,
                    'instansi' => $request->instansi,
                    'password' => bcrypt($request->password)
                ]);
                if($data->save()){
                    return Redirect::route('data-user')->with('success','Berhasil Tambah User');
                }  
                else{
                    return Redirect::route('tambah-user')->with('danger','Gagal Tambah User');
                }
            }
        }
        else{
            return Redirect::route('tambah-user')->with('danger','Gagal Tambah User');
        }
    }

    public function detail($id){
        $user = User::where('id',$id)->first();
        return view('user.detail',compact('user'));
    }

    public function ubah($id){
        $user = User::where('id',$id)->first();
        $all_warga = Warga::all();
        $list_nik = [];
        $list_name = [];
        foreach($all_warga as $index => $warga){
            $list_nik[$index] = $warga['nik'];
            $list_name[$index] = $warga['nama'];    
        }
        return view('user.ubah',compact('user','list_nik','list_name'));
    }
    
    public function delete($id){
        $delete=User::where('id',$id)->delete();
        if($delete){
            return Redirect::route('data-user')->with('success','Berhasil Hapus User');
        }  
        else{
            return Redirect::route('data-user')->with('danger','Gagal Hapus User');
        }
    }

    public function edit(Request $request){
        $data = User::where('id',$request->id)->first();
        if($request->level == 4){
            $data['name'] = $request->name;
            $data['username'] = explode("_",$request->usernamewarga)[0];
            $data['email'] = $request->email;
            $data['level'] = $request->level;
            $data['instansi'] = $request->instansi;
            $data['bio'] = $request->bio;

            if($request->gantipassword == 1 ){
                $data['password'] = bcrypt($request->password);
            }

            if($data->save()){
                return Redirect::route('data-user')->with('success','Berhasil Ubah User');
            }  
            else{
                return Redirect::route('ubah-user')->with('danger','Gagal Ubah User');
            }
        }
        else{
            $data['name'] = $request->name;
            $data['username'] = $request->username;
            $data['email'] = $request->email;
            $data['level'] = $request->level;
            $data['instansi'] = $request->instansi;
            $data['bio'] = $request->bio;

            if($request->gantipassword == 1 ){
                $data['password'] = bcrypt($request->password);
            }
            if($data->save()){
                return Redirect::route('data-user')->with('success','Berhasil Ubah User');
            }  
            else{
                return Redirect::route('ubah-user')->with('danger','Gagal Ubah User');
            }
        }
    }

    public function findPass(Request $request){
        $passbcrip = User::where('id',$request->id)->first();
        if(Hash::check($request->lama,$passbcrip->password)){
            return response()->json(['message' =>'success']);
        }
        else{
            return response()->json(['message' =>'error']);            
        }

    }

    
}
