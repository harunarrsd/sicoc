<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Requests;
use Illuminate\Support\Facades\Session;
use Redirect;

use App\Pelayanan;
use App\Warga;
use App\UsiaLanjut;


class UsiaLanjutController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        return view('spm.usialanjut.index');
    }

    public function data(){
        $usialanjut_all = UsiaLanjut::all();
        $total_spm = 0;
        $total_tidak_spm = 0;
        $total_tahap_pelayanan = 0;
        $total_usialanjut = 0;
        foreach($usialanjut_all as $index => $usialanjut){
            $total_usialanjut++;
            $datawarga[$index] = Warga::where('nik',$usialanjut['nik'])->first();
            $usia_warga[$index] = UsiaLanjutController::Usia($datawarga[$index]['tgl_lahir'],date("Y/m/d/h:i:s"));
            $pelayanan_all[$index] = Pelayanan::where('table_spm','usia_lanjut')->where('id_spm',$usialanjut['id'])->get();
            
            if($usialanjut['status'] == "Sesuai SPM"){
                $total_spm++;
            }
            else if($usialanjut['status'] == "Tidak Sesuai SPM"){
                $total_tidak_spm ++;
            }
            else if($usialanjut['status'] == "Tahap Pelayanan"){
                $total_tahap_pelayanan++;
            }
            
            $kumpulan_usia[$index] = [];
            foreach($pelayanan_all[$index] as $i => $pel){
                $usiapel = UsiaLanjutController::Usia($datawarga[$index]['tgl_lahir'],$pel['tanggal_pelayanan']);
                $usia = $usiapel['years'];$cap_tek_darah_all_pel[$usia] = 0;$cap_gula_darah_all_pel[$usia] = 0;
                $kumpulan_usia[$index][$i] = $usia;
                $cap_kepikunan_all_pel[$usia] = 0;$cap_emo_all_pel[$usia] = 0;$cap_perilaku_all_pel[$usia] = 0;$cap_kolesterol_all_pel[$usia] = 0;
                if($pel['tekanan_darah'] != null){$cap_tekanan_darah[$i] =1;}else{$cap_tekanan_darah[$i] =0;}
                if($pel['kadar_gula_darah'] != null){$cap_kadar_gula_darah[$i] =1;}else{$cap_kadar_gula_darah[$i] =0;}
                if($pel['cara_deteksi_kepikunan'] != null){$cap_kepikunan[$i] =1;}else{$cap_kepikunan[$i] =0;}
                if($pel['tes_gangguan_emosional'] == "y"){$cap_emo[$i] =1;}else{$cap_emo[$i] =0;}
                if($pel['tes_gangguan_perilaku'] == "y"){$cap_perilaku[$i] =1;}else{$cap_perilaku[$i] =0;}
                if($pel['tes_kolesterol_darah'] == "y"){$cap_kolesterol_darah[$i] =1;}else{$cap_kolesterol_darah[$i] =0;}
            }
    
            // Perhitungan 
            foreach($pelayanan_all[$index] as $i => $pel){
                $usiapel = UsiaLanjutController::Usia($datawarga[$index]['tgl_lahir'],$pel['tanggal_pelayanan']);
                $usia = $usiapel['years'];
                if($cap_tek_darah_all_pel[$usia] < 1){$cap_tek_darah_all_pel[$usia] = $cap_tek_darah_all_pel[$usia]+$cap_tekanan_darah[$i];}
                if($cap_gula_darah_all_pel[$usia] < 1){$cap_gula_darah_all_pel[$usia] = $cap_gula_darah_all_pel[$usia]+$cap_kadar_gula_darah[$i];}
                if($cap_kepikunan_all_pel[$usia] < 1){$cap_kepikunan_all_pel[$usia] = $cap_kepikunan_all_pel[$usia]+$cap_kepikunan[$i];}
                if($cap_emo_all_pel[$usia] < 1){$cap_emo_all_pel[$usia] = $cap_emo_all_pel[$usia]+$cap_emo[$i];}
                if($cap_perilaku_all_pel[$usia] < 1){$cap_perilaku_all_pel[$usia] = $cap_perilaku_all_pel[$usia]+$cap_perilaku[$i];}
                if($cap_kolesterol_all_pel[$usia] < 1){$cap_kolesterol_all_pel[$usia] = $cap_kolesterol_all_pel[$usia]+$cap_kolesterol_darah[$i];}
            }
    
            $notdouble = "";
            $capaian =0;
            $max =0; $min=60;
            foreach($kumpulan_usia[$index] as $i => $usia){
                $cap_pel[$usia] = (($cap_tek_darah_all_pel[$usia]*100)+($cap_gula_darah_all_pel[$usia]*100)+($cap_kolesterol_all_pel[$usia]*100)+
                ((($cap_emo_all_pel[$usia]+$cap_perilaku_all_pel[$usia]+$cap_kepikunan_all_pel[$usia])*100)/3))/4;
                $notdouble = $notdouble."_";
                $pos = strpos($notdouble, "_".$usia);
                if($pos === false){
                    $capaian = $capaian + $cap_pel[$usia];
                    $notdouble = $notdouble.$usia;
                }
                if($usia > $max){
                    $max = $usia;
                }
            }
    
            $selisih = ($max-$min)+1;
            $capaian_fix[$index] = $capaian/$selisih;
        }
        return view('spm.usialanjut.data',compact('capaian_fix','usia_warga','usialanjut_all','total_spm','total_tidak_spm','total_tahap_pelayanan','total_usialanjut','datawarga'));
    }

    public function tambah(){
        date_default_timezone_set('Asia/Jakarta');
        $all_warga = Warga::all();
        $list_ca_lanjut = [];
        foreach($all_warga as $index => $warga){
            $data = UsiaLanjut::where('nik', $warga['nik'])->where('status','tahap pelayanan')->first();
                if(!$data){
                    $usia = UsiaLanjutController::Usia($warga['tgl_lahir'],date("Y/m/d/h:i:s"));
                    if($usia["years"] >= 60){
                        $list_ca_lanjut[$index] = $warga;
                        $list_nik[$index] = $warga['nik'];
                        $list_name[$index] = $warga['nama'];
                        $list_ttl[$index] = $warga['tgl_lahir'];                            
                    }
             }
        }
        return view('spm.usialanjut.tambah',compact('list_nik','list_name','list_ttl','list_ca_lanjut'));
    }

    public function save(Request $request){
        $warga = Warga::where('id',$request->select)->first();
        $data = New UsiaLanjut();
        $data['nik'] = $warga->nik;
        $data['status'] = "Tahap Pelayanan";
        if($data->save()){
            return Redirect::route('data-usia-lanjut')->with('success','Berhasil Tambah Usia Lanjut');
        }  
        else{
            return Redirect::route('tambah-usia-lanjut')->with('danger','Gagal Tambah Usia Lanjut');
        }
    }

    public function edit(Request $request){
        $usialanjut = UsiaLanjut::where('id',$request->id)->first();
        $usialanjut['nik'] = $request->nik;
        if($usialanjut->save()){
            return redirect('/data-usia-lanjut')->with('alert', 'Berhasil Ubah NIK Usia Lanjut');
        }  
        else{
            return redirect('/data-usia-lanjut')->with('alert', 'Gagal Ubah NIK Usia Lanjut');
        }
    }

    public function delete($id){
        $UsiaLanjut=UsiaLanjut::where('id',$id)->first();
        $delete = $UsiaLanjut->delete();
        
        if($delete){
            return Redirect::route('data-usia-lanjut')->with('success','Berhasil Usia Lanjut');
        }  
        else{
            return Redirect::route('data-usia-lanjut')->with('danger','Gagal Usia Lanjut');
        }
    }

    public function delete_pelayanan($id){
        $pelayanan=Pelayanan::where('id',$id)->first();
        $delete = $pelayanan->delete();
        
        if($delete){
            return Redirect::route('data-usia-lanjut')->with('success','Berhasil Hapus Pelayanan');
        }  
        else{
            return Redirect::route('data-usia-lanjut')->with('danger','Gagal Hapus Pelayanan');
        }
    }

    public function detail($id){
        $usialanjut = UsiaLanjut::where('id',$id)->first();
        date_default_timezone_set('Asia/Jakarta');
        $datawarga = Warga::where('nik',$usialanjut['nik'])->first();
        Session::put('withnik6',$datawarga['nik']);
        $usia_usialanjut =  UsiaLanjutController::Usia($datawarga['tgl_lahir'],date("Y/m/d/h:i:s"));
        $pelayanan_all = Pelayanan::where('table_spm','usia_lanjut')->where('id_spm',$usialanjut['id'])->get();

        //Inisialisasi
        $kumpulan_usia = [];
        foreach($pelayanan_all as $i => $pel){
            $usiapel[$i] = UsiaLanjutController::Usia($datawarga['tgl_lahir'],$pel['tanggal_pelayanan']);
            $usia = $usiapel[$i]['years'];$cap_tek_darah_all_pel[$usia] = 0;$cap_gula_darah_all_pel[$usia] = 0;
            $kumpulan_usia[$i] = $usia;
            $cap_kepikunan_all_pel[$usia] = 0;$cap_emo_all_pel[$usia] = 0;$cap_perilaku_all_pel[$usia] = 0;$cap_kolesterol_all_pel[$usia] = 0;
            if($pel['tekanan_darah'] != null){$cap_tekanan_darah[$i] =1;}else{$cap_tekanan_darah[$i] =0;}
            if($pel['kadar_gula_darah'] != null){$cap_kadar_gula_darah[$i] =1;}else{$cap_kadar_gula_darah[$i] =0;}
            if($pel['cara_deteksi_kepikunan'] != null){$cap_kepikunan[$i] =1;}else{$cap_kepikunan[$i] =0;}
            if($pel['tes_gangguan_emosional'] == "y"){$cap_emo[$i] =1;}else{$cap_emo[$i] =0;}
            if($pel['tes_gangguan_perilaku'] == "y"){$cap_perilaku[$i] =1;}else{$cap_perilaku[$i] =0;}
            if($pel['tes_kolesterol_darah'] == "y"){$cap_kolesterol_darah[$i] =1;}else{$cap_kolesterol_darah[$i] =0;}

            $capaian_pelayanan[$i] = (($cap_tekanan_darah[$i]*100)+($cap_kadar_gula_darah[$i]*100)+($cap_kolesterol_darah[$i]*100)+
            ((($cap_emo[$i]+$cap_perilaku[$i]+$cap_kepikunan[$i])*100)/3))/4;
        }

        // Perhitungan 
        foreach($pelayanan_all as $i => $pel){
            $usiapel[$i] = UsiaLanjutController::Usia($datawarga['tgl_lahir'],$pel['tanggal_pelayanan']);
            $usia = $usiapel[$i]['years'];
            if($cap_tek_darah_all_pel[$usia] < 1){$cap_tek_darah_all_pel[$usia] = $cap_tek_darah_all_pel[$usia]+$cap_tekanan_darah[$i];}
            if($cap_gula_darah_all_pel[$usia] < 1){$cap_gula_darah_all_pel[$usia] = $cap_gula_darah_all_pel[$usia]+$cap_kadar_gula_darah[$i];}
            if($cap_kepikunan_all_pel[$usia] < 1){$cap_kepikunan_all_pel[$usia] = $cap_kepikunan_all_pel[$usia]+$cap_kepikunan[$i];}
            if($cap_emo_all_pel[$usia] < 1){$cap_emo_all_pel[$usia] = $cap_emo_all_pel[$usia]+$cap_emo[$i];}
            if($cap_perilaku_all_pel[$usia] < 1){$cap_perilaku_all_pel[$usia] = $cap_perilaku_all_pel[$usia]+$cap_perilaku[$i];}
            if($cap_kolesterol_all_pel[$usia] < 1){$cap_kolesterol_all_pel[$usia] = $cap_kolesterol_all_pel[$usia]+$cap_kolesterol_darah[$i];}
        }

        $notdouble = "";
        $capaian =0;
        $max =0; $min=60;
        foreach($kumpulan_usia as $i => $usia){
            $cap_pel[$usia] = (($cap_tek_darah_all_pel[$usia]*100)+($cap_gula_darah_all_pel[$usia]*100)+($cap_kolesterol_all_pel[$usia]*100)+
            ((($cap_emo_all_pel[$usia]+$cap_perilaku_all_pel[$usia]+$cap_kepikunan_all_pel[$usia])*100)/3))/4;
            $notdouble = $notdouble."_";
            $pos = strpos($notdouble, "_".$usia);
            if($pos === false){
                $capaian = $capaian + $cap_pel[$usia];
                $notdouble = $notdouble.$usia;
            }
            if($usia > $max){
                $max = $usia;
            }
        }

        $selisih = ($max-$min)+1;
        $capaian_fix = $capaian/$selisih;


        return view('spm.usialanjut.detail',compact('datawarga','usia_usialanjut','pelayanan_all','capaian_fix','capaian_pelayanan','usiapel','usialanjut'));
    }

    public function ubah($id){
        date_default_timezone_set('Asia/Jakarta');
        $usialanjut = UsiaLanjut::where('id',$id)->first();
        $datawarga = Warga::where('nik',$usialanjut['nik'])->first();
        $warga_all = Warga::all();
        $list_nik = [];
        $list_name = [];
        $list_ttl = [];
        foreach($warga_all as $indexwarga => $warga){
            $data = UsiaLanjut::where('nik', $warga['nik'])->where('status','tahap pelayanan')->first();
            if(!$data){
                $usia[$indexwarga] = UsiaLanjutController::Usia($warga['tgl_lahir'],date("Y/m/d/h:i:s"));
                if($usia[$indexwarga]["years"] > 59){
                    $list_nik[$indexwarga] = $warga['nik'];
                    $list_name[$indexwarga] = $warga['nama'];
                    $list_ttl[$indexwarga] = $warga['tgl_lahir'];
                }
            }  
        }

        $pelayanan_all = Pelayanan::where('table_spm','usia_lanjut')->where('id_spm',$usialanjut['id'])->get();

        //Inisialisasi
        $kumpulan_usia = [];
        foreach($pelayanan_all as $i => $pel){
            $usiapel[$i] = UsiaLanjutController::Usia($datawarga['tgl_lahir'],$pel['tanggal_pelayanan']);
            $usia = $usiapel[$i]['years'];$cap_tek_darah_all_pel[$usia] = 0;$cap_gula_darah_all_pel[$usia] = 0;
            $kumpulan_usia[$i] = $usia;
            $cap_kepikunan_all_pel[$usia] = 0;$cap_emo_all_pel[$usia] = 0;$cap_perilaku_all_pel[$usia] = 0;$cap_kolesterol_all_pel[$usia] = 0;
            if($pel['tekanan_darah'] != null){$cap_tekanan_darah[$i] =1;}else{$cap_tekanan_darah[$i] =0;}
            if($pel['kadar_gula_darah'] != null){$cap_kadar_gula_darah[$i] =1;}else{$cap_kadar_gula_darah[$i] =0;}
            if($pel['cara_deteksi_kepikunan'] != null){$cap_kepikunan[$i] =1;}else{$cap_kepikunan[$i] =0;}
            if($pel['tes_gangguan_emosional'] == "y"){$cap_emo[$i] =1;}else{$cap_emo[$i] =0;}
            if($pel['tes_gangguan_perilaku'] == "y"){$cap_perilaku[$i] =1;}else{$cap_perilaku[$i] =0;}
            if($pel['tes_kolesterol_darah'] == "y"){$cap_kolesterol_darah[$i] =1;}else{$cap_kolesterol_darah[$i] =0;}

            $capaian_pelayanan[$i] = (($cap_tekanan_darah[$i]*100)+($cap_kadar_gula_darah[$i]*100)+($cap_kolesterol_darah[$i]*100)+
            ((($cap_emo[$i]+$cap_perilaku[$i]+$cap_kepikunan[$i])*100)/3))/4;
        }

        // Perhitungan 
        foreach($pelayanan_all as $i => $pel){
            $usiapel[$i] = UsiaLanjutController::Usia($datawarga['tgl_lahir'],$pel['tanggal_pelayanan']);
            $usia = $usiapel[$i]['years'];
            if($cap_tek_darah_all_pel[$usia] < 1){$cap_tek_darah_all_pel[$usia] = $cap_tek_darah_all_pel[$usia]+$cap_tekanan_darah[$i];}
            if($cap_gula_darah_all_pel[$usia] < 1){$cap_gula_darah_all_pel[$usia] = $cap_gula_darah_all_pel[$usia]+$cap_kadar_gula_darah[$i];}
            if($cap_kepikunan_all_pel[$usia] < 1){$cap_kepikunan_all_pel[$usia] = $cap_kepikunan_all_pel[$usia]+$cap_kepikunan[$i];}
            if($cap_emo_all_pel[$usia] < 1){$cap_emo_all_pel[$usia] = $cap_emo_all_pel[$usia]+$cap_emo[$i];}
            if($cap_perilaku_all_pel[$usia] < 1){$cap_perilaku_all_pel[$usia] = $cap_perilaku_all_pel[$usia]+$cap_perilaku[$i];}
            if($cap_kolesterol_all_pel[$usia] < 1){$cap_kolesterol_all_pel[$usia] = $cap_kolesterol_all_pel[$usia]+$cap_kolesterol_darah[$i];}
        }

        $notdouble = "";
        $capaian =0;
        $max =0; $min=60;
        foreach($kumpulan_usia as $i => $usia){
            $cap_pel[$usia] = (($cap_tek_darah_all_pel[$usia]*100)+($cap_gula_darah_all_pel[$usia]*100)+($cap_kolesterol_all_pel[$usia]*100)+
            ((($cap_emo_all_pel[$usia]+$cap_perilaku_all_pel[$usia]+$cap_kepikunan_all_pel[$usia])*100)/3))/4;
            $notdouble = $notdouble."_";
            $pos = strpos($notdouble, "_".$usia);
            if($pos === false){
                $capaian = $capaian + $cap_pel[$usia];
                $notdouble = $notdouble.$usia;
            }
            if($usia > $max){
                $max = $usia;
            }
        }

        $selisih = ($max-$min)+1;
        $capaian_fix = $capaian/$selisih;
        return view('spm.usialanjut.ubah',compact('datawarga','capaian_fix','list_nik','list_name','list_ttl','usialanjut'));
    }

    public function indexPelayanan(){
        $pelayanan_usia_lanjut = Pelayanan::where('table_spm','usia_lanjut')->get();
        foreach($pelayanan_usia_lanjut as $i => $pel){
            $data_usialanjut[$i] = UsiaLanjut::where('id',$pel['id_spm'])->first();  
            $datawarga[$i] = Warga::where('nik',$data_usialanjut[$i]['nik'])->first();
            $usiapel[$i] = UsiaLanjutController::Usia($datawarga[$i]['tgl_lahir'],$pel['tanggal_pelayanan']);
            if($pel['tekanan_darah'] != null){$cap_tekanan_darah =1;}else{$cap_tekanan_darah =0;}
            if($pel['kadar_gula_darah'] != null){$cap_kadar_gula_darah =1;}else{$cap_kadar_gula_darah =0;}
            if($pel['cara_deteksi_kepikunan'] != null){$cap_kepikunan =1;}else{$cap_kepikunan =0;}
            if($pel['tes_gangguan_emosional'] == "y"){$cap_emo =1;}else{$cap_emo =0;}
            if($pel['tes_gangguan_perilaku'] == "y"){$cap_perilaku =1;}else{$cap_perilaku =0;}
            if($pel['tes_kolesterol_darah'] == "y"){$cap_kolesterol_darah =1;}else{$cap_kolesterol_darah =0;}

            $cap_pel[$i] = (($cap_tekanan_darah*100)+($cap_kadar_gula_darah*100)+($cap_kolesterol_darah*100)+((($cap_emo+$cap_perilaku+$cap_kepikunan)*100)/3))/4;
        }
        return view('spm.usialanjut.pelayanan.data',compact('pelayanan_usia_lanjut','data_usialanjut','datawarga','usiapel','cap_pel',
        'cap_tekanan_darah','cap_kadar_gula_darah','cap_kolesterol_darah','cap_emo','cap_perilaku','cap_kepikunan'));
    }
    public function detailPelayanan($id){
        $pelayanan = Pelayanan::where('id',$id)->first();
        $usialanjut = UsiaLanjut::where('id',$pelayanan['id_spm'])->first();
        $datawarga = Warga::where('nik',$usialanjut['nik'])->first();
        $pelayanan_all = Pelayanan::where('table_spm','usia_lanjut')->where('id_spm',$usialanjut['id'])->get();

        //Inisialisasi
        $kumpulan_usia = [];
        foreach($pelayanan_all as $i => $pel){
            $usiapel = UsiaLanjutController::Usia($datawarga['tgl_lahir'],$pel['tanggal_pelayanan']);
            $usia = $usiapel['years'];$cap_tek_darah_all_pel[$usia] = 0;$cap_gula_darah_all_pel[$usia] = 0;
            $kumpulan_usia[$i] = $usia;
            $cap_kepikunan_all_pel[$usia] = 0;$cap_emo_all_pel[$usia] = 0;$cap_perilaku_all_pel[$usia] = 0;$cap_kolesterol_all_pel[$usia] = 0;
            if($pel['tekanan_darah'] != null){$cap_tekanan_darah[$i] =1;}else{$cap_tekanan_darah[$i] =0;}
            if($pel['kadar_gula_darah'] != null){$cap_kadar_gula_darah[$i] =1;}else{$cap_kadar_gula_darah[$i] =0;}
            if($pel['cara_deteksi_kepikunan'] != null){$cap_kepikunan[$i] =1;}else{$cap_kepikunan[$i] =0;}
            if($pel['tes_gangguan_emosional'] == "y"){$cap_emo[$i] =1;}else{$cap_emo[$i] =0;}
            if($pel['tes_gangguan_perilaku'] == "y"){$cap_perilaku[$i] =1;}else{$cap_perilaku[$i] =0;}
            if($pel['tes_kolesterol_darah'] == "y"){$cap_kolesterol_darah[$i] =1;}else{$cap_kolesterol_darah[$i] =0;}
        }

        // Perhitungan 
        foreach($pelayanan_all as $i => $pel){
            $usiapel = UsiaLanjutController::Usia($datawarga['tgl_lahir'],$pel['tanggal_pelayanan']);
            $usia = $usiapel['years'];
            if($cap_tek_darah_all_pel[$usia] < 1){$cap_tek_darah_all_pel[$usia] = $cap_tek_darah_all_pel[$usia]+$cap_tekanan_darah[$i];}
            if($cap_gula_darah_all_pel[$usia] < 1){$cap_gula_darah_all_pel[$usia] = $cap_gula_darah_all_pel[$usia]+$cap_kadar_gula_darah[$i];}
            if($cap_kepikunan_all_pel[$usia] < 1){$cap_kepikunan_all_pel[$usia] = $cap_kepikunan_all_pel[$usia]+$cap_kepikunan[$i];}
            if($cap_emo_all_pel[$usia] < 1){$cap_emo_all_pel[$usia] = $cap_emo_all_pel[$usia]+$cap_emo[$i];}
            if($cap_perilaku_all_pel[$usia] < 1){$cap_perilaku_all_pel[$usia] = $cap_perilaku_all_pel[$usia]+$cap_perilaku[$i];}
            if($cap_kolesterol_all_pel[$usia] < 1){$cap_kolesterol_all_pel[$usia] = $cap_kolesterol_all_pel[$usia]+$cap_kolesterol_darah[$i];}
        }

        $notdouble = "";
        $capaian =0;
        $max =0; $min=60;
        foreach($kumpulan_usia as $i => $usia){
            $cap_pel[$usia] = (($cap_tek_darah_all_pel[$usia]*100)+($cap_gula_darah_all_pel[$usia]*100)+($cap_kolesterol_all_pel[$usia]*100)+
            ((($cap_emo_all_pel[$usia]+$cap_perilaku_all_pel[$usia]+$cap_kepikunan_all_pel[$usia])*100)/3))/4;
            $notdouble = $notdouble."_";
            $pos = strpos($notdouble, "_".$usia);
            if($pos === false){
                $capaian = $capaian + $cap_pel[$usia];
                $notdouble = $notdouble.$usia;
            }
            if($usia > $max){
                $max = $usia;
            }
        }

        $selisih = ($max-$min)+1;
        $capaian_fix = $capaian/$selisih;
        return view('spm.usialanjut.pelayanan.detail',compact('pelayanan','datawarga','capaian_fix'));
    }
    public function ubahPelayanan($id){
        $pelayanan = Pelayanan::where('id',$id)->first();
        $usialanjut = UsiaLanjut::where('id',$pelayanan['id_spm'])->first();
        $datawarga = Warga::where('nik',$usialanjut['nik'])->first();
        $pelayanan_all = Pelayanan::where('table_spm','usia_lanjut')->where('id_spm',$usialanjut['id'])->get();

        //Inisialisasi
        $kumpulan_usia = [];
        foreach($pelayanan_all as $i => $pel){
            $usiapel = UsiaLanjutController::Usia($datawarga['tgl_lahir'],$pel['tanggal_pelayanan']);
            $usia = $usiapel['years'];$cap_tek_darah_all_pel[$usia] = 0;$cap_gula_darah_all_pel[$usia] = 0;
            $kumpulan_usia[$i] = $usia;
            $cap_kepikunan_all_pel[$usia] = 0;$cap_emo_all_pel[$usia] = 0;$cap_perilaku_all_pel[$usia] = 0;$cap_kolesterol_all_pel[$usia] = 0;
            if($pel['tekanan_darah'] != null){$cap_tekanan_darah[$i] =1;}else{$cap_tekanan_darah[$i] =0;}
            if($pel['kadar_gula_darah'] != null){$cap_kadar_gula_darah[$i] =1;}else{$cap_kadar_gula_darah[$i] =0;}
            if($pel['cara_deteksi_kepikunan'] != null){$cap_kepikunan[$i] =1;}else{$cap_kepikunan[$i] =0;}
            if($pel['tes_gangguan_emosional'] == "y"){$cap_emo[$i] =1;}else{$cap_emo[$i] =0;}
            if($pel['tes_gangguan_perilaku'] == "y"){$cap_perilaku[$i] =1;}else{$cap_perilaku[$i] =0;}
            if($pel['tes_kolesterol_darah'] == "y"){$cap_kolesterol_darah[$i] =1;}else{$cap_kolesterol_darah[$i] =0;}
        }

        // Perhitungan 
        foreach($pelayanan_all as $i => $pel){
            $usiapel = UsiaLanjutController::Usia($datawarga['tgl_lahir'],$pel['tanggal_pelayanan']);
            $usia = $usiapel['years'];
            if($cap_tek_darah_all_pel[$usia] < 1){$cap_tek_darah_all_pel[$usia] = $cap_tek_darah_all_pel[$usia]+$cap_tekanan_darah[$i];}
            if($cap_gula_darah_all_pel[$usia] < 1){$cap_gula_darah_all_pel[$usia] = $cap_gula_darah_all_pel[$usia]+$cap_kadar_gula_darah[$i];}
            if($cap_kepikunan_all_pel[$usia] < 1){$cap_kepikunan_all_pel[$usia] = $cap_kepikunan_all_pel[$usia]+$cap_kepikunan[$i];}
            if($cap_emo_all_pel[$usia] < 1){$cap_emo_all_pel[$usia] = $cap_emo_all_pel[$usia]+$cap_emo[$i];}
            if($cap_perilaku_all_pel[$usia] < 1){$cap_perilaku_all_pel[$usia] = $cap_perilaku_all_pel[$usia]+$cap_perilaku[$i];}
            if($cap_kolesterol_all_pel[$usia] < 1){$cap_kolesterol_all_pel[$usia] = $cap_kolesterol_all_pel[$usia]+$cap_kolesterol_darah[$i];}
        }

        $notdouble = "";
        $capaian =0;
        $max =0; $min=60;
        foreach($kumpulan_usia as $i => $usia){
            $cap_pel[$usia] = (($cap_tek_darah_all_pel[$usia]*100)+($cap_gula_darah_all_pel[$usia]*100)+($cap_kolesterol_all_pel[$usia]*100)+
            ((($cap_emo_all_pel[$usia]+$cap_perilaku_all_pel[$usia]+$cap_kepikunan_all_pel[$usia])*100)/3))/4;
            $notdouble = $notdouble."_";
            $pos = strpos($notdouble, "_".$usia);
            if($pos === false){
                $capaian = $capaian + $cap_pel[$usia];
                $notdouble = $notdouble.$usia;
            }
            if($usia > $max){
                $max = $usia;
            }
        }

        $selisih = ($max-$min)+1;
        $capaian_fix = $capaian/$selisih;
        return view('spm.usialanjut.pelayanan.ubah',compact('pelayanan','datawarga','capaian_fix'));
    }

    public function inputnikPelayanan(){
        if(!empty(Session::get('withnik6'))){
            Session::forget('withnik6');
            return view('spm.usialanjut.pelayanan.masukan_nik');  
        }
        else{
            return view('spm.usialanjut.pelayanan.masukan_nik');  
        }
    }

    public function inputPelayanan(Request $request){
        date_default_timezone_set('Asia/Jakarta');
        Session::put('withnik6',$request->nik);
        $nik = Session::get('withnik6');
        $datanik = Warga::where('nik', $nik)->first();
        if(count($datanik) > 0 ){
            $usia = UsiaLanjutController::Usia($datanik['tgl_lahir'],date("Y/m/d/h:i:s"));
            if($usia["years"] > 59){
                $data = UsiaLanjut::where('nik', $nik)->where('status','tahap pelayanan')->first();
                if(count($data) > 0 ){
                    return view('spm.usialanjut.pelayanan.tambah',compact('data'));
                }else{
                    return view('spm.usialanjut.pelayanan.masukan_nik')->with('alert','NIK tidak terdaftar sebagai Usia Lanjut');                
                }
            }
            else{
                return view('spm.usialanjut.pelayanan.masukan_nik')->with('alert','NIK bukan beridentitas Usia Lanjut');                
            }
        }
        else{
            return view('spm.usialanjut.pelayanan.masukan_nik')->with('alert','NIK tidak terdaftar');
        }
    }

    public function savePelayanan(Request $request){
        $a = date('Y-m-d', strtotime($request->tanggal_pelayanan));

        if($request->tes_emo != null){$tes_gangguan_emosional = "y";}else{$tes_gangguan_emosional = "t";}
        if($request->tes_perilaku != null){$tes_gangguan_perilaku = "y";}else{$tes_gangguan_perilaku = "t";}
        if($request->tes_kolesterol != null){$tes_kolesterol_darah = "y";}else{$tes_kolesterol_darah = "t";}

        $data = Pelayanan::create([
            'tanggal_pelayanan' => $a,
            'id_spm' => $request->id,
            'table_spm' => 'usia_lanjut',
            'tenaga_kerja' => $request->tenaga_kesehatan."_".$request->nama_stk,
            'lokasi' => $request->fasilitas_kesehatan."_".$request->lokasi_pelayanan,
            'tekanan_darah' => $request->tekanan_darah,
            'kadar_gula_darah' => $request->gula_darah,
            'tes_kolesterol_darah' => $tes_kolesterol_darah,
            'tes_gangguan_emosional' => $tes_gangguan_emosional,
            'tes_gangguan_perilaku' => $tes_gangguan_perilaku,
            'cara_deteksi_kepikunan' => $request->kepikunan,
            'created_by' => $request->created_by
        ]);

        if($data->save()){
            return Redirect::route('detail-usia-lanjut',[$request->id])->with('success','Berhasil Tambah Pelayanan Usia Lanjut');        
        }  
        else{
            return Redirect::route('catat-usia-lanjut')->with('danger','Gagal Tambah Pelayanan Usia Lanjut');
        }
    }

    public function editPelayanan(Request $request){
        $a = date('Y-m-d', strtotime($request->tanggal_pelayanan));

        if($request->tes_emo != null){$tes_gangguan_emosional = "y";}else{$tes_gangguan_emosional = "t";}
        if($request->tes_perilaku != null){$tes_gangguan_perilaku = "y";}else{$tes_gangguan_perilaku = "t";}
        if($request->tes_kolesterol != null){$tes_kolesterol_darah = "y";}else{$tes_kolesterol_darah = "t";}

        $data = Pelayanan::where('id',$request->id)->first();
        $data['tanggal_pelayanan'] = $a;
        $data['tenaga_kerja'] = $request->tenaga_kesehatan."_".$request->nama_stk;
        $data['lokasi'] = $request->fasilitas_kesehatan."_".$request->lokasi_pelayanan;
        $data['tekanan_darah'] = $request->tekanan_darah;
        $data['kadar_gula_darah'] = $request->gula_darah;
        $data['tes_kolesterol_darah'] = $tes_kolesterol_darah;
        $data['tes_gangguan_emosional'] = $tes_gangguan_emosional;
        $data['tes_gangguan_perilaku'] = $tes_gangguan_perilaku;
        $data['cara_deteksi_kepikunan'] = $request->kepikunan;

        if($data->save()){
            return redirect('/pelayanan-usia-lanjut')->with('alert', 'Berhasil Ubah Pelayanan Usia Lanjut');
        }  
        else{
            return redirect('/catat-usia-lanjut')->with('alert', 'Gagal Ubah Pelayanan Usia Lanjut');
        }
    }

    public function Usia($tgl1, $tgl2){
        $tgl1 = (is_string($tgl1) ? strtotime($tgl1) : $tgl1);
        $tgl2 = (is_string($tgl2) ? strtotime($tgl2) : $tgl2);
        $diff_secs = abs($tgl1-$tgl2);
        $base_year = min(date("Y",$tgl1), date("Y",$tgl2));
        $diff = mktime(0,0,$diff_secs,1,1,$base_year);
        return array(
            "years"=>date("Y",$diff) - $base_year, 
            "months_total" => (date("Y",$diff) - $base_year) * 12 + date("n",$diff) - 1, 
            "months" => date("n",$diff) -1, 
            "days_total" => floor($diff_secs/ (3600 * 24)), 
            "days" => date("j",$diff) - 1, 
            "hours_total" => floor($diff_secs/3600), 
            "hours" => date("G", $diff), 
            "minutes_total" => floor($diff_secs/60), 
            "minutes" => (int)date("i",$diff),
            "seconds_total" => $diff_secs, 
            "seconds" => (int) date("s",$diff)
        );
    }
}
