<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Requests;
use Illuminate\Support\Facades\Session;
use Redirect;

use App\Pelayanan;
use App\Warga;
use App\UsiaPendDasar;

class UsiaPendidikanDasarController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        return view('spm.usiapenddasar.index');
    }

    public function data(){
        $usiapenddasar_all = UsiaPendDasar::all();
        $total_spm = 0;
        $total_tidak_spm = 0;
        $total_tahap_pelayanan = 0;
        $total_penddasar = 0;
        foreach($usiapenddasar_all as $i => $usiapenddasar){
            date_default_timezone_set('Asia/Jakarta');
            $datawarga[$i] = Warga::where('nik',$usiapenddasar['nik'])->first();
            $usia[$i] = UsiaPendidikanDasarController::Usia($datawarga[$i]['tgl_lahir'],date("Y/m/d/h:i:s"));
            $total_penddasar++;
            if($usiapenddasar['status_usia_pend_dasar'] == "Sesuai SPM"){
                $total_spm++;
            }
            else if($usiapenddasar['status_usia_pend_dasar'] == "Tidak Sesuai SPM"){
                $total_tidak_spm ++;
            }
            else if($usiapenddasar['status_usia_pend_dasar'] == "Tahap Pelayanan"){
                $total_tahap_pelayanan++;
            }
            $cap_berat_total1[$i] =0; $cap_tinggi_total1[$i] = 0; $cap_anemia_total1[$i] =0; $cap_tek_darah_total1[$i] = 0; $cap_nadi_napas_total1[$i] =0; $cap_gigi_total1[$i] = 0; $cap_mulut_total1[$i] =0; $cap_mata_total1[$i] =0; $cap_telinga_total1[$i] =0;
            $cap_berat_total7[$i] =0; $cap_tinggi_total7[$i] = 0; $cap_anemia_total7[$i] =0; $cap_tek_darah_total7[$i] = 0; $cap_nadi_napas_total7[$i] =0; $cap_gigi_total7[$i] = 0; $cap_mulut_total7[$i] =0; $cap_mata_total7[$i] =0; $cap_telinga_total7[$i] =0;
            $pelayanan[$i] = Pelayanan::where('table_spm','usia_pend_dasar')->where('id_spm',$usiapenddasar['id'])->get();
            foreach($pelayanan[$i] as $j => $pel){
                if($pel['kelas'] == 1){
                    if($pel['status_gizi'] != null && $pel['status_gizi'] != "__"){
                        if(explode('_',$pel['status_gizi'])[0] != null && explode('_',$pel['status_gizi'])[0] > 0){
                            $cap_berat1[$i][$j] = 1;   
                        }
                        else{
                            $cap_berat1[$i][$j] = 0;
                        }
                        if(explode('_',$pel['status_gizi'])[1] != null && explode('_',$pel['status_gizi'])[1] > 0){
                            $cap_tinggi1[$i][$j] = 1;   
                        }
                        else{
                            $cap_tinggi1[$i][$j] = 0;
                        }
                        if(explode('_',$pel['status_gizi'])[2] != null && explode('_',$pel['status_gizi'])[2] == "y"){
                            $cap_anemia1[$i][$j] = 1;   
                        }
                        else{
                            $cap_anemia1[$i][$j] = 0;
                        }
                    }else{
                        $cap_berat1[$i][$j] = 0; $cap_tinggi1[$i][$j] =0; $cap_anemia1[$i][$j]=0;
                    }
                    if($pel['tanda_vital'] != null){
                        if(explode('_',$pel['tanda_vital'])[0] != null && explode('_',$pel['tanda_vital'])[0] > 0){
                            $cap_tek_darah1[$i][$j] = 1;   
                        }
                        else{
                            $cap_tek_darah1[$i][$j] = 0;
                        }
                        if(explode('_',$pel['tanda_vital'])[1] != null && explode('_',$pel['tanda_vital'])[1] > 0){
                            $cap_nadi_napas1[$i][$j] = 1;   
                        }
                        else{
                            $cap_nadi_napas1[$i][$j] = 0;
                        }
                    }else{
                        $cap_tek_darah1[$i][$j] = 0; $cap_nadi_napas1[$i][$j] =0;
                    }            
                    if($pel['gigi_mulut'] != null){
                        if(explode('_',$pel['gigi_mulut'])[0] != null && explode('_',$pel['gigi_mulut'])[0] == "y"){
                            $cap_gigi1[$i][$j] = 1;   
                        }
                        else{
                            $cap_gigi1[$i][$j] = 0;
                        }
                        if(explode('_',$pel['gigi_mulut'])[1] != null && explode('_',$pel['gigi_mulut'])[1] == "y"){
                            $cap_mulut1[$i][$j] = 1;   
                        }
                        else{
                            $cap_mulut1[$i][$j] = 0;
                        }
                    }else{
                        $cap_gigi1[$i][$j] = 0; $cap_mulut1[$i][$j] =0;
                    }
                    if($pel['ketajaman_indera'] != null){
                        if(explode('_',$pel['ketajaman_indera'])[0] != null && explode('_',$pel['ketajaman_indera'])[0] == "y"){
                            $cap_mata1[$i][$j] = 1;   
                        }
                        else{
                            $cap_mata1[$i][$j] = 0;
                        }
                        if(explode('_',$pel['ketajaman_indera'])[1] != null && explode('_',$pel['ketajaman_indera'])[1] == "y"){
                            $cap_telinga1[$i][$j] = 1;   
                        }
                        else{
                            $cap_telinga1[$i][$j] = 0;
                        }
                    }else{
                        $cap_mata1[$i][$j] = 0; $cap_telinga1[$i][$j] =0;
                    }   
                }
                else if($pel['kelas'] == 7){
                    if($pel['status_gizi'] != null && $pel['status_gizi'] != "__"){
                        if(explode('_',$pel['status_gizi'])[0] != null && explode('_',$pel['status_gizi'])[0] > 0){
                            $cap_berat7[$i][$j] = 1;   
                        }
                        else{
                            $cap_berat7[$i][$j] = 0;
                        }
                        if(explode('_',$pel['status_gizi'])[1] != null && explode('_',$pel['status_gizi'])[1] > 0){
                            $cap_tinggi7[$i][$j] = 1;   
                        }
                        else{
                            $cap_tinggi7[$i][$j] = 0;
                        }
                        if(explode('_',$pel['status_gizi'])[2] != null && explode('_',$pel['status_gizi'])[2] == "y"){
                            $cap_anemia7[$i][$j] = 1;   
                        }
                        else{
                            $cap_anemia7[$i][$j] = 0;
                        }
                    }else{
                        $cap_berat7[$i][$j] = 0; $cap_tinggi7[$i][$j] =0; $cap_anemia7[$i][$j]=0;
                    }
                    if($pel['tanda_vital'] != null){
                        if(explode('_',$pel['tanda_vital'])[0] != null && explode('_',$pel['tanda_vital'])[0] > 0){
                            $cap_tek_darah7[$i][$j] = 1;   
                        }
                        else{
                            $cap_tek_darah7[$i][$j] = 0;
                        }
                        if(explode('_',$pel['tanda_vital'])[1] != null && explode('_',$pel['tanda_vital'])[1] > 0){
                            $cap_nadi_napas7[$i][$j] = 1;   
                        }
                        else{
                            $cap_nadi_napas7[$i][$j] = 0;
                        }
                    }else{
                        $cap_tek_darah7[$i][$j] = 0; $cap_nadi_napas7[$i][$j] =0;
                    }            
                    if($pel['gigi_mulut'] != null){
                        if(explode('_',$pel['gigi_mulut'])[0] != null && explode('_',$pel['gigi_mulut'])[0] == "y"){
                            $cap_gigi7[$i][$j] = 1;   
                        }
                        else{
                            $cap_gigi7[$i][$j] = 0;
                        }
                        if(explode('_',$pel['gigi_mulut'])[1] != null && explode('_',$pel['gigi_mulut'])[1] == "y"){
                            $cap_mulut7[$i][$j] = 1;   
                        }
                        else{
                            $cap_mulut7[$i][$j] = 0;
                        }
                    }else{
                        $cap_gigi7[$i][$j] = 0; $cap_mulut7[$i][$j] =0;
                    }
                    if($pel['ketajaman_indera'] != null){
                        if(explode('_',$pel['ketajaman_indera'])[0] != null && explode('_',$pel['ketajaman_indera'])[0] == "y"){
                            $cap_mata7[$i][$j] = 1;   
                        }
                        else{
                            $cap_mata7[$i][$j] = 0;
                        }
                        if(explode('_',$pel['ketajaman_indera'])[1] != null && explode('_',$pel['ketajaman_indera'])[1] == "y"){
                            $cap_telinga7[$i][$j] = 1;   
                        }
                        else{
                            $cap_telinga7[$i][$j] = 0;
                        }
                    }else{
                        $cap_mata7[$i][$j] = 0; $cap_telinga7[$i][$j] =0;
                    }
                }
            }
        }
        foreach($usiapenddasar_all as $i => $usiapenddasar){
            foreach($pelayanan[$i] as $j => $pel){
                if($pel['kelas'] == 1){
                    if($cap_berat_total1[$i] == 0){$cap_berat_total1[$i] = $cap_berat_total1[$i] + $cap_berat1[$i][$j];}
                    if($cap_tinggi_total1[$i] == 0){$cap_tinggi_total1[$i] = $cap_tinggi_total1[$i] + $cap_tinggi1[$i][$j];}
                    if($cap_anemia_total1[$i] == 0){$cap_anemia_total1[$i] = $cap_anemia_total1[$i] + $cap_anemia1[$i][$j];}
                    if($cap_tek_darah_total1[$i] == 0){$cap_tek_darah_total1[$i] = $cap_tek_darah_total1[$i] + $cap_tek_darah1[$i][$j];}
                    if($cap_nadi_napas_total1[$i] == 0){$cap_nadi_napas_total1[$i] = $cap_nadi_napas_total1[$i] + $cap_nadi_napas1[$i][$j];}
                    if($cap_gigi_total1[$i] == 0){$cap_gigi_total1[$i] = $cap_gigi_total1[$i] + $cap_gigi1[$i][$j];}
                    if($cap_mulut_total1[$i] == 0){$cap_mulut_total1[$i] = $cap_mulut_total1[$i] + $cap_mulut1[$i][$j];}
                    if($cap_mata_total1[$i] == 0){$cap_mata_total1[$i] = $cap_mata_total1[$i] + $cap_mata1[$i][$j];}
                    if($cap_telinga_total1[$i] == 0){$cap_telinga_total1[$i] = $cap_telinga_total1[$i] + $cap_telinga1[$i][$j];}
                }
                else if($pel['kelas'] == 7){
                    if($cap_berat_total7[$i] == 0){$cap_berat_total7[$i] = $cap_berat_total7[$i] + $cap_berat7[$i][$j];}
                    if($cap_tinggi_total7[$i] == 0){$cap_tinggi_total7[$i] = $cap_tinggi_total7[$i] + $cap_tinggi7[$i][$j];}
                    if($cap_anemia_total7[$i] == 0){$cap_anemia_total7[$i] = $cap_anemia_total7[$i] + $cap_anemia7[$i][$j];}
                    if($cap_tek_darah_total7[$i] == 0){$cap_tek_darah_total7[$i] = $cap_tek_darah_total7[$i] + $cap_tek_darah7[$i][$j];}
                    if($cap_nadi_napas_total7[$i] == 0){$cap_nadi_napas_total7[$i] = $cap_nadi_napas_total7[$i] + $cap_nadi_napas7[$i][$j];}
                    if($cap_gigi_total7[$i] == 0){$cap_gigi_total7[$i] = $cap_gigi_total7[$i] + $cap_gigi7[$i][$j];}
                    if($cap_mulut_total7[$i] == 0){$cap_mulut_total7[$i] = $cap_mulut_total7[$i] + $cap_mulut7[$i][$j];}
                    if($cap_mata_total7[$i] == 0){$cap_mata_total7[$i] = $cap_mata_total7[$i] + $cap_mata7[$i][$j];}
                    if($cap_telinga_total7[$i] == 0){$cap_telinga_total7[$i] = $cap_telinga_total7[$i] + $cap_telinga7[$i][$j];}
                }
            }
            $capaian_super_total1[$i] = (((($cap_berat_total1[$i]+$cap_tinggi_total1[$i]+$cap_anemia_total1[$i])*100)/3)+
            ((($cap_tek_darah_total1[$i]+$cap_nadi_napas_total1[$i])*100)/2)+((($cap_gigi_total1[$i]+$cap_mulut_total1[$i])*100)/2)+
            ((($cap_mata_total1[$i]+$cap_telinga_total1[$i])*100)/2))/4;
            $capaian_super_total7[$i] = (((($cap_berat_total7[$i]+$cap_tinggi_total7[$i]+$cap_anemia_total7[$i])*100)/3)+
            ((($cap_tek_darah_total7[$i]+$cap_nadi_napas_total7[$i])*100)/2)+((($cap_gigi_total7[$i]+$cap_mulut_total7[$i])*100)/2)+
            ((($cap_mata_total7[$i]+$cap_telinga_total7[$i])*100)/2))/4;
            $capaian_super_total[$i] = ($capaian_super_total1[$i]+$capaian_super_total7[$i])/2;
        }
        return view('spm.usiapenddasar.data',compact('usiapenddasar_all','pelayanan','capaian_super_total',
        'total_spm','total_tidak_spm','total_tahap_pelayanan','total_penddasar','capaian_super_total','datawarga','usia'));
    }

    public function delete($id){
        $UsiaPendDasar=UsiaPendDasar::where('id',$id)->first();
        $delete = $UsiaPendDasar->delete();
        
        if($delete){
            return Redirect::route('data-usia-pend-dasar')->with('success','Berhasil Hapus Usia Pend. Dasar');
        }  
        else{
            return Redirect::route('data-usia-pend-dasar')->with('danger','Gagal Hapus Usia Pend. Dasar');
        }
    }

    public function delete_pelayanan($id){
        $pelayanan=Pelayanan::where('id',$id)->first();
        $delete = $pelayanan->delete();
        
        if($delete){
            return Redirect::route('data-usia-pend-dasar')->with('success','Berhasil Hapus Pelayanan');
        }  
        else{
            return Redirect::route('data-usia-pend-dasar')->with('danger','Gagal Hapus Pelayanan');
        }
    }
    
    public function detail($id){
        $usiapenddasar = UsiaPendDasar::where('id',$id)->first();
        $datawarga = Warga::where('nik',$usiapenddasar['nik'])->first();
        Session::put('withnik4',$datawarga['nik']);
        date_default_timezone_set('Asia/Jakarta');
        $usia_penddasar =  UsiaPendidikanDasarController::Usia($datawarga['tgl_lahir'],date("Y/m/d/h:i:s"));
        $pelayanan = Pelayanan::where('table_spm','usia_pend_dasar')->where('id_spm',$usiapenddasar['id'])->get();
        $cap_berat_total1 =0; $cap_tinggi_total1 = 0; $cap_anemia_total1 =0; $cap_tek_darah_total1 = 0; $cap_nadi_napas_total1 =0; $cap_gigi_total1 = 0; $cap_mulut_total1 =0; $cap_mata_total1 =0; $cap_telinga_total1 =0;
        $cap_berat_total7 =0; $cap_tinggi_total7 = 0; $cap_anemia_total7 =0; $cap_tek_darah_total7 = 0; $cap_nadi_napas_total7 =0; $cap_gigi_total7 = 0; $cap_mulut_total7 =0; $cap_mata_total7 =0; $cap_telinga_total7 =0;
        foreach($pelayanan as $j => $pel){
                if($pel['kelas'] == 1){
                    if($pel['status_gizi'] != null && $pel['status_gizi'] != "__"){
                        if(explode('_',$pel['status_gizi'])[0] != null && explode('_',$pel['status_gizi'])[0] > 0){
                            $cap_berat1[$j] = 1;   
                        }
                        else{
                            $cap_berat1[$j] = 0;
                        }
                        if(explode('_',$pel['status_gizi'])[1] != null && explode('_',$pel['status_gizi'])[1] > 0){
                            $cap_tinggi1[$j] = 1;   
                        }
                        else{
                            $cap_tinggi1[$j] = 0;
                        }
                        if(explode('_',$pel['status_gizi'])[2] != null && explode('_',$pel['status_gizi'])[2] == "y"){
                            $cap_anemia1[$j] = 1;   
                        }
                        else{
                            $cap_anemia1[$j] = 0;
                        }
                    }else{
                        $cap_berat1[$j] = 0; $cap_tinggi1[$j] =0; $cap_anemia1[$j]=0;
                    }
                    if($pel['tanda_vital'] != null){
                        if(explode('_',$pel['tanda_vital'])[0] != null && explode('_',$pel['tanda_vital'])[0] > 0){
                            $cap_tek_darah1[$j] = 1;   
                        }
                        else{
                            $cap_tek_darah1[$j] = 0;
                        }
                        if(explode('_',$pel['tanda_vital'])[1] != null && explode('_',$pel['tanda_vital'])[1] > 0){
                            $cap_nadi_napas1[$j] = 1;   
                        }
                        else{
                            $cap_nadi_napas1[$j] = 0;
                        }
                    }else{
                        $cap_tek_darah1[$j] = 0; $cap_nadi_napas1[$j] =0;
                    }            
                    if($pel['gigi_mulut'] != null){
                        if(explode('_',$pel['gigi_mulut'])[0] != null && explode('_',$pel['gigi_mulut'])[0] == "y"){
                            $cap_gigi1[$j] = 1;   
                        }
                        else{
                            $cap_gigi1[$j] = 0;
                        }
                        if(explode('_',$pel['gigi_mulut'])[1] != null && explode('_',$pel['gigi_mulut'])[1] == "y"){
                            $cap_mulut1[$j] = 1;   
                        }
                        else{
                            $cap_mulut1[$j] = 0;
                        }
                    }else{
                        $cap_gigi1[$j] = 0; $cap_mulut1[$j] =0;
                    }
                    if($pel['ketajaman_indera'] != null){
                        if(explode('_',$pel['ketajaman_indera'])[0] != null && explode('_',$pel['ketajaman_indera'])[0] == "y"){
                            $cap_mata1[$j] = 1;   
                        }
                        else{
                            $cap_mata1[$j] = 0;
                        }
                        if(explode('_',$pel['ketajaman_indera'])[1] != null && explode('_',$pel['ketajaman_indera'])[1] == "y"){
                            $cap_telinga1[$j] = 1;   
                        }
                        else{
                            $cap_telinga1[$j] = 0;
                        }
                    }else{
                        $cap_mata1[$j] = 0; $cap_telinga1[$j] =0;
                    }
                    if($cap_berat_total1 == 0){$cap_berat_total1 = $cap_berat_total1 + $cap_berat1[$j];}
                    if($cap_tinggi_total1 == 0){$cap_tinggi_total1 = $cap_tinggi_total1 + $cap_tinggi1[$j];}
                    if($cap_anemia_total1 == 0){$cap_anemia_total1 = $cap_anemia_total1 + $cap_anemia1[$j];}
                    if($cap_tek_darah_total1 == 0){$cap_tek_darah_total1 = $cap_tek_darah_total1 + $cap_tek_darah1[$j];}
                    if($cap_nadi_napas_total1 == 0){$cap_nadi_napas_total1 = $cap_nadi_napas_total1 + $cap_nadi_napas1[$j];}
                    if($cap_gigi_total1 == 0){$cap_gigi_total1 = $cap_gigi_total1 + $cap_gigi1[$j];}
                    if($cap_mulut_total1 == 0){$cap_mulut_total1 = $cap_mulut_total1 + $cap_mulut1[$j];}
                    if($cap_mata_total1 == 0){$cap_mata_total1 = $cap_mata_total1 + $cap_mata1[$j];}
                    if($cap_telinga_total1 == 0){$cap_telinga_total1 = $cap_telinga_total1 + $cap_telinga1[$j];}  
                    $cap_pel[$j] = (((($cap_berat1[$j]+$cap_tinggi1[$j]+$cap_anemia1[$j])*100)/3)+
                    ((($cap_tek_darah1[$j]+$cap_nadi_napas1[$j])*100)/2)+((($cap_gigi1[$j]+$cap_mulut1[$j])*100)/2)+
                    ((($cap_mata1[$j]+$cap_telinga1[$j])*100)/2))/4;
                }
                else if($pel['kelas'] == 7){
                    if($pel['status_gizi'] != null && $pel['status_gizi'] != "__"){
                        if(explode('_',$pel['status_gizi'])[0] != null && explode('_',$pel['status_gizi'])[0] > 0){
                            $cap_berat7[$j] = 1;   
                        }
                        else{
                            $cap_berat7[$j] = 0;
                        }
                        if(explode('_',$pel['status_gizi'])[1] != null && explode('_',$pel['status_gizi'])[1] > 0){
                            $cap_tinggi7[$j] = 1;   
                        }
                        else{
                            $cap_tinggi7[$j] = 0;
                        }
                        if(explode('_',$pel['status_gizi'])[2] != null && explode('_',$pel['status_gizi'])[2] == "y"){
                            $cap_anemia7[$j] = 1;   
                        }
                        else{
                            $cap_anemia7[$j] = 0;
                        }
                    }else{
                        $cap_berat7[$j] = 0; $cap_tinggi7[$j] =0; $cap_anemia7[$j]=0;
                    }
                    if($pel['tanda_vital'] != null){
                        if(explode('_',$pel['tanda_vital'])[0] != null && explode('_',$pel['tanda_vital'])[0] > 0){
                            $cap_tek_darah7[$j] = 1;   
                        }
                        else{
                            $cap_tek_darah7[$j] = 0;
                        }
                        if(explode('_',$pel['tanda_vital'])[1] != null && explode('_',$pel['tanda_vital'])[1] > 0){
                            $cap_nadi_napas7[$j] = 1;   
                        }
                        else{
                            $cap_nadi_napas7[$j] = 0;
                        }
                    }else{
                        $cap_tek_darah7[$j] = 0; $cap_nadi_napas7[$j] =0;
                    }            
                    if($pel['gigi_mulut'] != null){
                        if(explode('_',$pel['gigi_mulut'])[0] != null && explode('_',$pel['gigi_mulut'])[0] == "y"){
                            $cap_gigi7[$j] = 1;   
                        }
                        else{
                            $cap_gigi7[$j] = 0;
                        }
                        if(explode('_',$pel['gigi_mulut'])[1] != null && explode('_',$pel['gigi_mulut'])[1] == "y"){
                            $cap_mulut7[$j] = 1;   
                        }
                        else{
                            $cap_mulut7[$j] = 0;
                        }
                    }else{
                        $cap_gigi7[$j] = 0; $cap_mulut7[$j] =0;
                    }
                    if($pel['ketajaman_indera'] != null){
                        if(explode('_',$pel['ketajaman_indera'])[0] != null && explode('_',$pel['ketajaman_indera'])[0] == "y"){
                            $cap_mata7[$j] = 1;   
                        }
                        else{
                            $cap_mata7[$j] = 0;
                        }
                        if(explode('_',$pel['ketajaman_indera'])[1] != null && explode('_',$pel['ketajaman_indera'])[1] == "y"){
                            $cap_telinga7[$j] = 1;   
                        }
                        else{
                            $cap_telinga7[$j] = 0;
                        }
                    }else{
                        $cap_mata7[$j] = 0; $cap_telinga7[$j] =0;
                    }
                    if($cap_berat_total7 == 0){$cap_berat_total7 = $cap_berat_total7 + $cap_berat7[$j];}
                    if($cap_tinggi_total7 == 0){$cap_tinggi_total7 = $cap_tinggi_total7 + $cap_tinggi7[$j];}
                    if($cap_anemia_total7 == 0){$cap_anemia_total7 = $cap_anemia_total7 + $cap_anemia7[$j];}
                    if($cap_tek_darah_total7 == 0){$cap_tek_darah_total7 = $cap_tek_darah_total7 + $cap_tek_darah7[$j];}
                    if($cap_nadi_napas_total7 == 0){$cap_nadi_napas_total7 = $cap_nadi_napas_total7 + $cap_nadi_napas7[$j];}
                    if($cap_gigi_total7 == 0){$cap_gigi_total7 = $cap_gigi_total7 + $cap_gigi7[$j];}
                    if($cap_mulut_total7 == 0){$cap_mulut_total7 = $cap_mulut_total7 + $cap_mulut7[$j];}
                    if($cap_mata_total7 == 0){$cap_mata_total7 = $cap_mata_total7 + $cap_mata7[$j];}
                    if($cap_telinga_total7 == 0){$cap_telinga_total7 = $cap_telinga_total7 + $cap_telinga7[$j];}
                    $cap_pel[$j] = (((($cap_berat7[$j]+$cap_tinggi7[$j]+$cap_anemia7[$j])*100)/3)+
                    ((($cap_tek_darah7[$j]+$cap_nadi_napas7[$j])*100)/2)+((($cap_gigi7[$j]+$cap_mulut7[$j])*100)/2)+
                    ((($cap_mata7[$j]+$cap_telinga7[$j])*100)/2))/4;
                }
        }
        $capaian_super_total1 = (((($cap_berat_total1+$cap_tinggi_total1+$cap_anemia_total1)*100)/3)+
        ((($cap_tek_darah_total1+$cap_nadi_napas_total1)*100)/2)+((($cap_gigi_total1+$cap_mulut_total1)*100)/2)+
        ((($cap_mata_total1+$cap_telinga_total1)*100)/2))/4;
        $capaian_super_total7 = (((($cap_berat_total7+$cap_tinggi_total7+$cap_anemia_total7)*100)/3)+
        ((($cap_tek_darah_total7+$cap_nadi_napas_total7)*100)/2)+((($cap_gigi_total7+$cap_mulut_total7)*100)/2)+
        ((($cap_mata_total7+$cap_telinga_total7)*100)/2))/4;
        $capaian_super_total = ($capaian_super_total1+$capaian_super_total7)/2;
        return view('spm.usiapenddasar.detail',compact('usia_penddasar','cap_pel','capaian_super_total1','capaian_super_total7','capaian_super_total','pelayanan','datawarga','usiapenddasar'));
    }
    
    public function ubah($id){
        $usiapenddasar = UsiaPendDasar::where('id',$id)->first();
        $cap_berat_total1 =0; $cap_tinggi_total1 = 0; $cap_anemia_total1 =0; $cap_tek_darah_total1 = 0; $cap_nadi_napas_total1 =0; $cap_gigi_total1 = 0; $cap_mulut_total1 =0; $cap_mata_total1 =0; $cap_telinga_total1 =0;
        $cap_berat_total7 =0; $cap_tinggi_total7 = 0; $cap_anemia_total7 =0; $cap_tek_darah_total7 = 0; $cap_nadi_napas_total7 =0; $cap_gigi_total7 = 0; $cap_mulut_total7 =0; $cap_mata_total7 =0; $cap_telinga_total7 =0;
        $pelayanan_all = Pelayanan::where('table_spm','usia_pend_dasar')->where('id_spm',$usiapenddasar['id'])->get();
        foreach($pelayanan_all as $j => $pel){
            if($pel['kelas'] == 1){
                if($pel['status_gizi'] != null && $pel['status_gizi'] != "__"){
                    if(explode('_',$pel['status_gizi'])[0] != null && explode('_',$pel['status_gizi'])[0] > 0){
                        $cap_berat1[$j] = 1;   
                    }
                    else{
                        $cap_berat1[$j] = 0;
                    }
                    if(explode('_',$pel['status_gizi'])[1] != null && explode('_',$pel['status_gizi'])[1] > 0){
                        $cap_tinggi1[$j] = 1;   
                    }
                    else{
                        $cap_tinggi1[$j] = 0;
                    }
                    if(explode('_',$pel['status_gizi'])[2] != null && explode('_',$pel['status_gizi'])[2] == "y"){
                        $cap_anemia1[$j] = 1;   
                    }
                    else{
                        $cap_anemia1[$j] = 0;
                    }
                }else{
                    $cap_berat1[$j] = 0; $cap_tinggi1[$j] =0; $cap_anemia1[$j]=0;
                }
                if($pel['tanda_vital'] != null){
                    if(explode('_',$pel['tanda_vital'])[0] != null && explode('_',$pel['tanda_vital'])[0] > 0){
                        $cap_tek_darah1[$j] = 1;   
                    }
                    else{
                        $cap_tek_darah1[$j] = 0;
                    }
                    if(explode('_',$pel['tanda_vital'])[1] != null && explode('_',$pel['tanda_vital'])[1] > 0){
                        $cap_nadi_napas1[$j] = 1;   
                    }
                    else{
                        $cap_nadi_napas1[$j] = 0;
                    }
                }else{
                    $cap_tek_darah1[$j] = 0; $cap_nadi_napas1[$j] =0;
                }            
                if($pel['gigi_mulut'] != null){
                    if(explode('_',$pel['gigi_mulut'])[0] != null && explode('_',$pel['gigi_mulut'])[0] == "y"){
                        $cap_gigi1[$j] = 1;   
                    }
                    else{
                        $cap_gigi1[$j] = 0;
                    }
                    if(explode('_',$pel['gigi_mulut'])[1] != null && explode('_',$pel['gigi_mulut'])[1] == "y"){
                        $cap_mulut1[$j] = 1;   
                    }
                    else{
                        $cap_mulut1[$j] = 0;
                    }
                }else{
                    $cap_gigi1[$j] = 0; $cap_mulut1[$j] =0;
                }
                if($pel['ketajaman_indera'] != null){
                    if(explode('_',$pel['ketajaman_indera'])[0] != null && explode('_',$pel['ketajaman_indera'])[0] == "y"){
                        $cap_mata1[$j] = 1;   
                    }
                    else{
                        $cap_mata1[$j] = 0;
                    }
                    if(explode('_',$pel['ketajaman_indera'])[1] != null && explode('_',$pel['ketajaman_indera'])[1] == "y"){
                        $cap_telinga1[$j] = 1;   
                    }
                    else{
                        $cap_telinga1[$j] = 0;
                    }
                }else{
                    $cap_mata1[$j] = 0; $cap_telinga1[$j] =0;
                }
                if($cap_berat_total1 == 0){$cap_berat_total1 = $cap_berat_total1 + $cap_berat1[$j];}
                if($cap_tinggi_total1 == 0){$cap_tinggi_total1 = $cap_tinggi_total1 + $cap_tinggi1[$j];}
                if($cap_anemia_total1 == 0){$cap_anemia_total1 = $cap_anemia_total1 + $cap_anemia1[$j];}
                if($cap_tek_darah_total1 == 0){$cap_tek_darah_total1 = $cap_tek_darah_total1 + $cap_tek_darah1[$j];}
                if($cap_nadi_napas_total1 == 0){$cap_nadi_napas_total1 = $cap_nadi_napas_total1 + $cap_nadi_napas1[$j];}
                if($cap_gigi_total1 == 0){$cap_gigi_total1 = $cap_gigi_total1 + $cap_gigi1[$j];}
                if($cap_mulut_total1 == 0){$cap_mulut_total1 = $cap_mulut_total1 + $cap_mulut1[$j];}
                if($cap_mata_total1 == 0){$cap_mata_total1 = $cap_mata_total1 + $cap_mata1[$j];}
                if($cap_telinga_total1 == 0){$cap_telinga_total1 = $cap_telinga_total1 + $cap_telinga1[$j];}  
                $cap_pel[$j] = (((($cap_berat1[$j]+$cap_tinggi1[$j]+$cap_anemia1[$j])*100)/3)+
                ((($cap_tek_darah1[$j]+$cap_nadi_napas1[$j])*100)/2)+((($cap_gigi1[$j]+$cap_mulut1[$j])*100)/2)+
                ((($cap_mata1[$j]+$cap_telinga1[$j])*100)/2))/4;
            }
            else if($pel['kelas'] == 7){
                if($pel['status_gizi'] != null && $pel['status_gizi'] != "__"){
                    if(explode('_',$pel['status_gizi'])[0] != null && explode('_',$pel['status_gizi'])[0] > 0){
                        $cap_berat7[$j] = 1;   
                    }
                    else{
                        $cap_berat7[$j] = 0;
                    }
                    if(explode('_',$pel['status_gizi'])[1] != null && explode('_',$pel['status_gizi'])[1] > 0){
                        $cap_tinggi7[$j] = 1;   
                    }
                    else{
                        $cap_tinggi7[$j] = 0;
                    }
                    if(explode('_',$pel['status_gizi'])[2] != null && explode('_',$pel['status_gizi'])[2] == "y"){
                        $cap_anemia7[$j] = 1;   
                    }
                    else{
                        $cap_anemia7[$j] = 0;
                    }
                }else{
                    $cap_berat7[$j] = 0; $cap_tinggi7[$j] =0; $cap_anemia7[$j]=0;
                }
                if($pel['tanda_vital'] != null){
                    if(explode('_',$pel['tanda_vital'])[0] != null && explode('_',$pel['tanda_vital'])[0] > 0){
                        $cap_tek_darah7[$j] = 1;   
                    }
                    else{
                        $cap_tek_darah7[$j] = 0;
                    }
                    if(explode('_',$pel['tanda_vital'])[1] != null && explode('_',$pel['tanda_vital'])[1] > 0){
                        $cap_nadi_napas7[$j] = 1;   
                    }
                    else{
                        $cap_nadi_napas7[$j] = 0;
                    }
                }else{
                    $cap_tek_darah7[$j] = 0; $cap_nadi_napas7[$j] =0;
                }            
                if($pel['gigi_mulut'] != null){
                    if(explode('_',$pel['gigi_mulut'])[0] != null && explode('_',$pel['gigi_mulut'])[0] == "y"){
                        $cap_gigi7[$j] = 1;   
                    }
                    else{
                        $cap_gigi7[$j] = 0;
                    }
                    if(explode('_',$pel['gigi_mulut'])[1] != null && explode('_',$pel['gigi_mulut'])[1] == "y"){
                        $cap_mulut7[$j] = 1;   
                    }
                    else{
                        $cap_mulut7[$j] = 0;
                    }
                }else{
                    $cap_gigi7[$j] = 0; $cap_mulut7[$j] =0;
                }
                if($pel['ketajaman_indera'] != null){
                    if(explode('_',$pel['ketajaman_indera'])[0] != null && explode('_',$pel['ketajaman_indera'])[0] == "y"){
                        $cap_mata7[$j] = 1;   
                    }
                    else{
                        $cap_mata7[$j] = 0;
                    }
                    if(explode('_',$pel['ketajaman_indera'])[1] != null && explode('_',$pel['ketajaman_indera'])[1] == "y"){
                        $cap_telinga7[$j] = 1;   
                    }
                    else{
                        $cap_telinga7[$j] = 0;
                    }
                }else{
                    $cap_mata7[$j] = 0; $cap_telinga7[$j] =0;
                }
                if($cap_berat_total7 == 0){$cap_berat_total7 = $cap_berat_total7 + $cap_berat7[$j];}
                if($cap_tinggi_total7 == 0){$cap_tinggi_total7 = $cap_tinggi_total7 + $cap_tinggi7[$j];}
                if($cap_anemia_total7 == 0){$cap_anemia_total7 = $cap_anemia_total7 + $cap_anemia7[$j];}
                if($cap_tek_darah_total7 == 0){$cap_tek_darah_total7 = $cap_tek_darah_total7 + $cap_tek_darah7[$j];}
                if($cap_nadi_napas_total7 == 0){$cap_nadi_napas_total7 = $cap_nadi_napas_total7 + $cap_nadi_napas7[$j];}
                if($cap_gigi_total7 == 0){$cap_gigi_total7 = $cap_gigi_total7 + $cap_gigi7[$j];}
                if($cap_mulut_total7 == 0){$cap_mulut_total7 = $cap_mulut_total7 + $cap_mulut7[$j];}
                if($cap_mata_total7 == 0){$cap_mata_total7 = $cap_mata_total7 + $cap_mata7[$j];}
                if($cap_telinga_total7 == 0){$cap_telinga_total7 = $cap_telinga_total7 + $cap_telinga7[$j];}
                $cap_pel[$j] = (((($cap_berat7[$j]+$cap_tinggi7[$j]+$cap_anemia7[$j])*100)/3)+
                ((($cap_tek_darah7[$j]+$cap_nadi_napas7[$j])*100)/2)+((($cap_gigi7[$j]+$cap_mulut7[$j])*100)/2)+
                ((($cap_mata7[$j]+$cap_telinga7[$j])*100)/2))/4;
        }
        }
        $capaian_super_total1 = (((($cap_berat_total1+$cap_tinggi_total1+$cap_anemia_total1)*100)/3)+
        ((($cap_tek_darah_total1+$cap_nadi_napas_total1)*100)/2)+((($cap_gigi_total1+$cap_mulut_total1)*100)/2)+
        ((($cap_mata_total1+$cap_telinga_total1)*100)/2))/4;
        $capaian_super_total7 = (((($cap_berat_total7+$cap_tinggi_total7+$cap_anemia_total7)*100)/3)+
        ((($cap_tek_darah_total7+$cap_nadi_napas_total7)*100)/2)+((($cap_gigi_total7+$cap_mulut_total7)*100)/2)+
        ((($cap_mata_total7+$cap_telinga_total7)*100)/2))/4;
        $capaian_super_total = ($capaian_super_total1+$capaian_super_total7)/2;

        date_default_timezone_set('Asia/Jakarta');
        $all_warga = Warga::all();
        foreach($all_warga as $index => $warga){
            $usia = UsiaPendidikanDasarController::Usia($warga['tgl_lahir'],date("Y/m/d/h:i:s"));
            if($usia["years"] >= 5 && $usia["years"] <= 15){
                $data = UsiaPendDasar::where('nik', $warga['nik'])->where('status_usia_pend_dasar','tahap pelayanan')->first();
                if(!$data){
                    $list_nik[$index] = $warga['nik'];
                    $list_name[$index] = $warga['nama'];
                    $list_ttl[$index] = $warga['tgl_lahir'];    
                }    
            }
        }
        return view('spm.usiapenddasar.ubah',compact('capaian_super_total','list_nik','list_name','list_ttl','usiapenddasar'));
    }

    public function tambah(){
        date_default_timezone_set('Asia/Jakarta');
        $all_warga = Warga::all();
        $list_ca_penddasar = [];
        foreach($all_warga as $index => $warga){
            $data = UsiaPendDasar::where('nik', $warga['nik'])->where('status_usia_pend_dasar','tahap pelayanan')->first();
            if(!$data){
                $usia = UsiaPendidikanDasarController::Usia($warga['tgl_lahir'],date("Y/m/d/h:i:s"));
                if($usia["years"] >= 5 && $usia["years"] <= 15){
                    $list_ca_penddasar[$index] = $warga;
                    $list_nik[$index] = $warga['nik'];
                    $list_name[$index] = $warga['nama'];
                    $list_ttl[$index] = $warga['tgl_lahir'];                        
                }
    
            }
        }
        return view('spm.usiapenddasar.tambah',compact('list_nik','list_name','list_ttl','list_ca_penddasar'));
    }

    public function save(Request $request){
        $warga = Warga::where('id',$request->select)->first();
        $data = New UsiaPendDasar();
        $data['nik'] = $warga->nik;
        $data['status_usia_pend_dasar'] = "Tahap Pelayanan";
        if($data->save()){
            return Redirect::route('data-usia-pend-dasar')->with('success','Berhasil Tambah Usia Pendidikan Dasar');
        }  
        else{
            return Redirect::route('tambah-usia-pend-dasar')->with('danger','Gagal Tambah Usia Pendidikan Dasar');
        }
    }

    public function edit(Request $request){
        $data = UsiaPendDasar::where('id',$request->id)->first();
        $data['nik'] = $request->nik;
        if($data->save()){
            return redirect('/data-usia-pend-dasar')->with('alert', 'Berhasil Ubah Usia Pendidikan Dasar');
        }  
        else{
            return redirect('/ubah-usia-pend-dasar')->with('alert', 'Gagal Ubah Usia Pendidikan Dasar');
        }
    }

    public function indexPelayanan(){
        $pelayanan_all = Pelayanan::where('table_spm','usia_pend_dasar')->get();
        $cekpelayanan =0;
        foreach($pelayanan_all as $i => $pel){
            $usiapenddasar[$i] = UsiaPendDasar::where('id',$pel['id_spm'])->first();
            $datawarga[$i] = Warga::where('nik',$usiapenddasar[$i]['nik'])->first();
            $usia[$i] = UsiaPendidikanDasarController::Usia($datawarga[$i]['tgl_lahir'],date("Y/m/d/h:i:s"));
            if($pel['status_gizi'] != null && $pel['status_gizi'] != "__"){
                if(explode('_',$pel['status_gizi'])[0] != null && explode('_',$pel['status_gizi'])[0] > 0){
                    $cap_berat = 1;   
                }
                else{
                    $cap_berat = 0;
                }
                if(explode('_',$pel['status_gizi'])[1] != null && explode('_',$pel['status_gizi'])[1] > 0){
                    $cap_tinggi = 1;   
                }
                else{
                    $cap_tinggi = 0;
                }
                if(explode('_',$pel['status_gizi'])[2] != null && explode('_',$pel['status_gizi'])[2] == "y"){
                    $cap_anemia = 1;   
                }
                else{
                    $cap_anemia = 0;
                }
            }else{
                $cap_berat = 0; $cap_tinggi =0; $cap_anemia=0;
            }
            if($pel['tanda_vital'] != null){
                if(explode('_',$pel['tanda_vital'])[0] != null && explode('_',$pel['tanda_vital'])[0] > 0){
                    $cap_tek_darah = 1;   
                }
                else{
                    $cap_tek_darah = 0;
                }
                if(explode('_',$pel['tanda_vital'])[1] != null && explode('_',$pel['tanda_vital'])[1] > 0){
                    $cap_nadi_napas = 1;   
                }
                else{
                    $cap_nadi_napas = 0;
                }
            }else{
                $cap_tek_darah = 0; $cap_nadi_napas =0;
            }            
            if($pel['gigi_mulut'] != null){
                if(explode('_',$pel['gigi_mulut'])[0] != null && explode('_',$pel['gigi_mulut'])[0] == "y"){
                    $cap_gigi = 1;   
                }
                else{
                    $cap_gigi = 0;
                }
                if(explode('_',$pel['gigi_mulut'])[1] != null && explode('_',$pel['gigi_mulut'])[1] == "y"){
                    $cap_mulut = 1;   
                }
                else{
                    $cap_mulut = 0;
                }
            }else{
                $cap_gigi = 0; $cap_mulut =0;
            }
            if($pel['ketajaman_indera'] != null){
                if(explode('_',$pel['ketajaman_indera'])[0] != null && explode('_',$pel['ketajaman_indera'])[0] == "y"){
                    $cap_mata = 1;   
                }
                else{
                    $cap_mata = 0;
                }
                if(explode('_',$pel['ketajaman_indera'])[1] != null && explode('_',$pel['ketajaman_indera'])[1] == "y"){
                    $cap_telinga = 1;   
                }
                else{
                    $cap_telinga = 0;
                }
            }else{
                $cap_mata = 0; $cap_telinga =0;
            }
            $capaian_pel[$i] = (((($cap_berat+$cap_tinggi+$cap_anemia)*100)/3)+((($cap_tek_darah+$cap_nadi_napas)*100)/2)+((($cap_gigi+$cap_mulut)*100)/2)+
            ((($cap_mata+$cap_telinga)*100)/2))/4;
        }
        return view('spm.usiapenddasar.pelayanan.data',compact('cap_berat','cap_tinggi','cap_anemia',
        'cap_tek_darah','cap_nadi_napas','cap_gigi','cap_mulut','cap_mata','cap_telinga','capaian_pel','usiapenddasar','datawarga','pelayanan_all','usia'));
    }
    
    public function detailPelayanan($id){
        $pelayanan = Pelayanan::where('id',$id)->first();
        $usiapenddasar = UsiaPendDasar::where('id',$pelayanan['id_spm'])->first();
        $datawarga = Warga::where('nik',$usiapenddasar['nik'])->first();
        $cap_berat_total1 =0; $cap_tinggi_total1 = 0; $cap_anemia_total1 =0; $cap_tek_darah_total1 = 0; $cap_nadi_napas_total1 =0; $cap_gigi_total1 = 0; $cap_mulut_total1 =0; $cap_mata_total1 =0; $cap_telinga_total1 =0;
        $cap_berat_total7 =0; $cap_tinggi_total7 = 0; $cap_anemia_total7 =0; $cap_tek_darah_total7 = 0; $cap_nadi_napas_total7 =0; $cap_gigi_total7 = 0; $cap_mulut_total7 =0; $cap_mata_total7 =0; $cap_telinga_total7 =0;
        $pelayanan_all = Pelayanan::where('table_spm','usia_pend_dasar')->where('id_spm',$usiapenddasar['id'])->get();
        foreach($pelayanan_all as $j => $pel){
                if($pel['kelas'] == 1){
                    if($pel['status_gizi'] != null && $pel['status_gizi'] != "__"){
                        if(explode('_',$pel['status_gizi'])[0] != null && explode('_',$pel['status_gizi'])[0] > 0){
                            $cap_berat1[$j] = 1;   
                        }
                        else{
                            $cap_berat1[$j] = 0;
                        }
                        if(explode('_',$pel['status_gizi'])[1] != null && explode('_',$pel['status_gizi'])[1] > 0){
                            $cap_tinggi1[$j] = 1;   
                        }
                        else{
                            $cap_tinggi1[$j] = 0;
                        }
                        if(explode('_',$pel['status_gizi'])[2] != null && explode('_',$pel['status_gizi'])[2] == "y"){
                            $cap_anemia1[$j] = 1;   
                        }
                        else{
                            $cap_anemia1[$j] = 0;
                        }
                    }else{
                        $cap_berat1[$j] = 0; $cap_tinggi1[$j] =0; $cap_anemia1[$j]=0;
                    }
                    if($pel['tanda_vital'] != null){
                        if(explode('_',$pel['tanda_vital'])[0] != null && explode('_',$pel['tanda_vital'])[0] > 0){
                            $cap_tek_darah1[$j] = 1;   
                        }
                        else{
                            $cap_tek_darah1[$j] = 0;
                        }
                        if(explode('_',$pel['tanda_vital'])[1] != null && explode('_',$pel['tanda_vital'])[1] > 0){
                            $cap_nadi_napas1[$j] = 1;   
                        }
                        else{
                            $cap_nadi_napas1[$j] = 0;
                        }
                    }else{
                        $cap_tek_darah1[$j] = 0; $cap_nadi_napas1[$j] =0;
                    }            
                    if($pel['gigi_mulut'] != null){
                        if(explode('_',$pel['gigi_mulut'])[0] != null && explode('_',$pel['gigi_mulut'])[0] == "y"){
                            $cap_gigi1[$j] = 1;   
                        }
                        else{
                            $cap_gigi1[$j] = 0;
                        }
                        if(explode('_',$pel['gigi_mulut'])[1] != null && explode('_',$pel['gigi_mulut'])[1] == "y"){
                            $cap_mulut1[$j] = 1;   
                        }
                        else{
                            $cap_mulut1[$j] = 0;
                        }
                    }else{
                        $cap_gigi1[$j] = 0; $cap_mulut1[$j] =0;
                    }
                    if($pel['ketajaman_indera'] != null){
                        if(explode('_',$pel['ketajaman_indera'])[0] != null && explode('_',$pel['ketajaman_indera'])[0] == "y"){
                            $cap_mata1[$j] = 1;   
                        }
                        else{
                            $cap_mata1[$j] = 0;
                        }
                        if(explode('_',$pel['ketajaman_indera'])[1] != null && explode('_',$pel['ketajaman_indera'])[1] == "y"){
                            $cap_telinga1[$j] = 1;   
                        }
                        else{
                            $cap_telinga1[$j] = 0;
                        }
                    }else{
                        $cap_mata1[$j] = 0; $cap_telinga1[$j] =0;
                    }
                    if($cap_berat_total1 == 0){$cap_berat_total1 = $cap_berat_total1 + $cap_berat1[$j];}
                    if($cap_tinggi_total1 == 0){$cap_tinggi_total1 = $cap_tinggi_total1 + $cap_tinggi1[$j];}
                    if($cap_anemia_total1 == 0){$cap_anemia_total1 = $cap_anemia_total1 + $cap_anemia1[$j];}
                    if($cap_tek_darah_total1 == 0){$cap_tek_darah_total1 = $cap_tek_darah_total1 + $cap_tek_darah1[$j];}
                    if($cap_nadi_napas_total1 == 0){$cap_nadi_napas_total1 = $cap_nadi_napas_total1 + $cap_nadi_napas1[$j];}
                    if($cap_gigi_total1 == 0){$cap_gigi_total1 = $cap_gigi_total1 + $cap_gigi1[$j];}
                    if($cap_mulut_total1 == 0){$cap_mulut_total1 = $cap_mulut_total1 + $cap_mulut1[$j];}
                    if($cap_mata_total1 == 0){$cap_mata_total1 = $cap_mata_total1 + $cap_mata1[$j];}
                    if($cap_telinga_total1 == 0){$cap_telinga_total1 = $cap_telinga_total1 + $cap_telinga1[$j];}  
                    $cap_pel[$j] = (((($cap_berat1[$j]+$cap_tinggi1[$j]+$cap_anemia1[$j])*100)/3)+
                    ((($cap_tek_darah1[$j]+$cap_nadi_napas1[$j])*100)/2)+((($cap_gigi1[$j]+$cap_mulut1[$j])*100)/2)+
                    ((($cap_mata1[$j]+$cap_telinga1[$j])*100)/2))/4;
                }
                else if($pel['kelas'] == 7){
                    if($pel['status_gizi'] != null && $pel['status_gizi'] != "__"){
                        if(explode('_',$pel['status_gizi'])[0] != null && explode('_',$pel['status_gizi'])[0] > 0){
                            $cap_berat7[$j] = 1;   
                        }
                        else{
                            $cap_berat7[$j] = 0;
                        }
                        if(explode('_',$pel['status_gizi'])[1] != null && explode('_',$pel['status_gizi'])[1] > 0){
                            $cap_tinggi7[$j] = 1;   
                        }
                        else{
                            $cap_tinggi7[$j] = 0;
                        }
                        if(explode('_',$pel['status_gizi'])[2] != null && explode('_',$pel['status_gizi'])[2] == "y"){
                            $cap_anemia7[$j] = 1;   
                        }
                        else{
                            $cap_anemia7[$j] = 0;
                        }
                    }else{
                        $cap_berat7[$j] = 0; $cap_tinggi7[$j] =0; $cap_anemia7[$j]=0;
                    }
                    if($pel['tanda_vital'] != null){
                        if(explode('_',$pel['tanda_vital'])[0] != null && explode('_',$pel['tanda_vital'])[0] > 0){
                            $cap_tek_darah7[$j] = 1;   
                        }
                        else{
                            $cap_tek_darah7[$j] = 0;
                        }
                        if(explode('_',$pel['tanda_vital'])[1] != null && explode('_',$pel['tanda_vital'])[1] > 0){
                            $cap_nadi_napas7[$j] = 1;   
                        }
                        else{
                            $cap_nadi_napas7[$j] = 0;
                        }
                    }else{
                        $cap_tek_darah7[$j] = 0; $cap_nadi_napas7[$j] =0;
                    }            
                    if($pel['gigi_mulut'] != null){
                        if(explode('_',$pel['gigi_mulut'])[0] != null && explode('_',$pel['gigi_mulut'])[0] == "y"){
                            $cap_gigi7[$j] = 1;   
                        }
                        else{
                            $cap_gigi7[$j] = 0;
                        }
                        if(explode('_',$pel['gigi_mulut'])[1] != null && explode('_',$pel['gigi_mulut'])[1] == "y"){
                            $cap_mulut7[$j] = 1;   
                        }
                        else{
                            $cap_mulut7[$j] = 0;
                        }
                    }else{
                        $cap_gigi7[$j] = 0; $cap_mulut7[$j] =0;
                    }
                    if($pel['ketajaman_indera'] != null){
                        if(explode('_',$pel['ketajaman_indera'])[0] != null && explode('_',$pel['ketajaman_indera'])[0] == "y"){
                            $cap_mata7[$j] = 1;   
                        }
                        else{
                            $cap_mata7[$j] = 0;
                        }
                        if(explode('_',$pel['ketajaman_indera'])[1] != null && explode('_',$pel['ketajaman_indera'])[1] == "y"){
                            $cap_telinga7[$j] = 1;   
                        }
                        else{
                            $cap_telinga7[$j] = 0;
                        }
                    }else{
                        $cap_mata7[$j] = 0; $cap_telinga7[$j] =0;
                    }
                    if($cap_berat_total7 == 0){$cap_berat_total7 = $cap_berat_total7 + $cap_berat7[$j];}
                    if($cap_tinggi_total7 == 0){$cap_tinggi_total7 = $cap_tinggi_total7 + $cap_tinggi7[$j];}
                    if($cap_anemia_total7 == 0){$cap_anemia_total7 = $cap_anemia_total7 + $cap_anemia7[$j];}
                    if($cap_tek_darah_total7 == 0){$cap_tek_darah_total7 = $cap_tek_darah_total7 + $cap_tek_darah7[$j];}
                    if($cap_nadi_napas_total7 == 0){$cap_nadi_napas_total7 = $cap_nadi_napas_total7 + $cap_nadi_napas7[$j];}
                    if($cap_gigi_total7 == 0){$cap_gigi_total7 = $cap_gigi_total7 + $cap_gigi7[$j];}
                    if($cap_mulut_total7 == 0){$cap_mulut_total7 = $cap_mulut_total7 + $cap_mulut7[$j];}
                    if($cap_mata_total7 == 0){$cap_mata_total7 = $cap_mata_total7 + $cap_mata7[$j];}
                    if($cap_telinga_total7 == 0){$cap_telinga_total7 = $cap_telinga_total7 + $cap_telinga7[$j];}
                    $cap_pel[$j] = (((($cap_berat7[$j]+$cap_tinggi7[$j]+$cap_anemia7[$j])*100)/3)+
                    ((($cap_tek_darah7[$j]+$cap_nadi_napas7[$j])*100)/2)+((($cap_gigi7[$j]+$cap_mulut7[$j])*100)/2)+
                    ((($cap_mata7[$j]+$cap_telinga7[$j])*100)/2))/4;
                }
        }
        $capaian_super_total1 = (((($cap_berat_total1+$cap_tinggi_total1+$cap_anemia_total1)*100)/3)+
        ((($cap_tek_darah_total1+$cap_nadi_napas_total1)*100)/2)+((($cap_gigi_total1+$cap_mulut_total1)*100)/2)+
        ((($cap_mata_total1+$cap_telinga_total1)*100)/2))/4;
        $capaian_super_total7 = (((($cap_berat_total7+$cap_tinggi_total7+$cap_anemia_total7)*100)/3)+
        ((($cap_tek_darah_total7+$cap_nadi_napas_total7)*100)/2)+((($cap_gigi_total7+$cap_mulut_total7)*100)/2)+
        ((($cap_mata_total7+$cap_telinga_total7)*100)/2))/4;
        $capaian_super_total = ($capaian_super_total1+$capaian_super_total7)/2;
        return view('spm.usiapenddasar.pelayanan.detail',compact('pelayanan','usiapenddasar','datawarga','capaian_super_total'));
    }
    
    public function ubahPelayanan($id){
        $pelayanan = Pelayanan::where('id',$id)->first();
        $usiapenddasar = UsiaPendDasar::where('id',$pelayanan['id_spm'])->first();
        $datawarga = Warga::where('nik',$usiapenddasar['nik'])->first();
        $cap_berat_total1 =0; $cap_tinggi_total1 = 0; $cap_anemia_total1 =0; $cap_tek_darah_total1 = 0; $cap_nadi_napas_total1 =0; $cap_gigi_total1 = 0; $cap_mulut_total1 =0; $cap_mata_total1 =0; $cap_telinga_total1 =0;
        $cap_berat_total7 =0; $cap_tinggi_total7 = 0; $cap_anemia_total7 =0; $cap_tek_darah_total7 = 0; $cap_nadi_napas_total7 =0; $cap_gigi_total7 = 0; $cap_mulut_total7 =0; $cap_mata_total7 =0; $cap_telinga_total7 =0;
        $pelayanan_all = Pelayanan::where('table_spm','usia_pend_dasar')->where('id_spm',$usiapenddasar['id'])->get();
        foreach($pelayanan_all as $j => $pel){
                if($pel['kelas'] == 1){
                    if($pel['status_gizi'] != null && $pel['status_gizi'] != "__"){
                        if(explode('_',$pel['status_gizi'])[0] != null && explode('_',$pel['status_gizi'])[0] > 0){
                            $cap_berat1[$j] = 1;   
                        }
                        else{
                            $cap_berat1[$j] = 0;
                        }
                        if(explode('_',$pel['status_gizi'])[1] != null && explode('_',$pel['status_gizi'])[1] > 0){
                            $cap_tinggi1[$j] = 1;   
                        }
                        else{
                            $cap_tinggi1[$j] = 0;
                        }
                        if(explode('_',$pel['status_gizi'])[2] != null && explode('_',$pel['status_gizi'])[2] == "y"){
                            $cap_anemia1[$j] = 1;   
                        }
                        else{
                            $cap_anemia1[$j] = 0;
                        }
                    }else{
                        $cap_berat1[$j] = 0; $cap_tinggi1[$j] =0; $cap_anemia1[$j]=0;
                    }
                    if($pel['tanda_vital'] != null){
                        if(explode('_',$pel['tanda_vital'])[0] != null && explode('_',$pel['tanda_vital'])[0] > 0){
                            $cap_tek_darah1[$j] = 1;   
                        }
                        else{
                            $cap_tek_darah1[$j] = 0;
                        }
                        if(explode('_',$pel['tanda_vital'])[1] != null && explode('_',$pel['tanda_vital'])[1] > 0){
                            $cap_nadi_napas1[$j] = 1;   
                        }
                        else{
                            $cap_nadi_napas1[$j] = 0;
                        }
                    }else{
                        $cap_tek_darah1[$j] = 0; $cap_nadi_napas1[$j] =0;
                    }            
                    if($pel['gigi_mulut'] != null){
                        if(explode('_',$pel['gigi_mulut'])[0] != null && explode('_',$pel['gigi_mulut'])[0] == "y"){
                            $cap_gigi1[$j] = 1;   
                        }
                        else{
                            $cap_gigi1[$j] = 0;
                        }
                        if(explode('_',$pel['gigi_mulut'])[1] != null && explode('_',$pel['gigi_mulut'])[1] == "y"){
                            $cap_mulut1[$j] = 1;   
                        }
                        else{
                            $cap_mulut1[$j] = 0;
                        }
                    }else{
                        $cap_gigi1[$j] = 0; $cap_mulut1[$j] =0;
                    }
                    if($pel['ketajaman_indera'] != null){
                        if(explode('_',$pel['ketajaman_indera'])[0] != null && explode('_',$pel['ketajaman_indera'])[0] == "y"){
                            $cap_mata1[$j] = 1;   
                        }
                        else{
                            $cap_mata1[$j] = 0;
                        }
                        if(explode('_',$pel['ketajaman_indera'])[1] != null && explode('_',$pel['ketajaman_indera'])[1] == "y"){
                            $cap_telinga1[$j] = 1;   
                        }
                        else{
                            $cap_telinga1[$j] = 0;
                        }
                    }else{
                        $cap_mata1[$j] = 0; $cap_telinga1[$j] =0;
                    }
                    if($cap_berat_total1 == 0){$cap_berat_total1 = $cap_berat_total1 + $cap_berat1[$j];}
                    if($cap_tinggi_total1 == 0){$cap_tinggi_total1 = $cap_tinggi_total1 + $cap_tinggi1[$j];}
                    if($cap_anemia_total1 == 0){$cap_anemia_total1 = $cap_anemia_total1 + $cap_anemia1[$j];}
                    if($cap_tek_darah_total1 == 0){$cap_tek_darah_total1 = $cap_tek_darah_total1 + $cap_tek_darah1[$j];}
                    if($cap_nadi_napas_total1 == 0){$cap_nadi_napas_total1 = $cap_nadi_napas_total1 + $cap_nadi_napas1[$j];}
                    if($cap_gigi_total1 == 0){$cap_gigi_total1 = $cap_gigi_total1 + $cap_gigi1[$j];}
                    if($cap_mulut_total1 == 0){$cap_mulut_total1 = $cap_mulut_total1 + $cap_mulut1[$j];}
                    if($cap_mata_total1 == 0){$cap_mata_total1 = $cap_mata_total1 + $cap_mata1[$j];}
                    if($cap_telinga_total1 == 0){$cap_telinga_total1 = $cap_telinga_total1 + $cap_telinga1[$j];}  
                    $cap_pel[$j] = (((($cap_berat1[$j]+$cap_tinggi1[$j]+$cap_anemia1[$j])*100)/3)+
                    ((($cap_tek_darah1[$j]+$cap_nadi_napas1[$j])*100)/2)+((($cap_gigi1[$j]+$cap_mulut1[$j])*100)/2)+
                    ((($cap_mata1[$j]+$cap_telinga1[$j])*100)/2))/4;
                }
                else if($pel['kelas'] == 7){
                    if($pel['status_gizi'] != null && $pel['status_gizi'] != "__"){
                        if(explode('_',$pel['status_gizi'])[0] != null && explode('_',$pel['status_gizi'])[0] > 0){
                            $cap_berat7[$j] = 1;   
                        }
                        else{
                            $cap_berat7[$j] = 0;
                        }
                        if(explode('_',$pel['status_gizi'])[1] != null && explode('_',$pel['status_gizi'])[1] > 0){
                            $cap_tinggi7[$j] = 1;   
                        }
                        else{
                            $cap_tinggi7[$j] = 0;
                        }
                        if(explode('_',$pel['status_gizi'])[2] != null && explode('_',$pel['status_gizi'])[2] == "y"){
                            $cap_anemia7[$j] = 1;   
                        }
                        else{
                            $cap_anemia7[$j] = 0;
                        }
                    }else{
                        $cap_berat7[$j] = 0; $cap_tinggi7[$j] =0; $cap_anemia7[$j]=0;
                    }
                    if($pel['tanda_vital'] != null){
                        if(explode('_',$pel['tanda_vital'])[0] != null && explode('_',$pel['tanda_vital'])[0] > 0){
                            $cap_tek_darah7[$j] = 1;   
                        }
                        else{
                            $cap_tek_darah7[$j] = 0;
                        }
                        if(explode('_',$pel['tanda_vital'])[1] != null && explode('_',$pel['tanda_vital'])[1] > 0){
                            $cap_nadi_napas7[$j] = 1;   
                        }
                        else{
                            $cap_nadi_napas7[$j] = 0;
                        }
                    }else{
                        $cap_tek_darah7[$j] = 0; $cap_nadi_napas7[$j] =0;
                    }            
                    if($pel['gigi_mulut'] != null){
                        if(explode('_',$pel['gigi_mulut'])[0] != null && explode('_',$pel['gigi_mulut'])[0] == "y"){
                            $cap_gigi7[$j] = 1;   
                        }
                        else{
                            $cap_gigi7[$j] = 0;
                        }
                        if(explode('_',$pel['gigi_mulut'])[1] != null && explode('_',$pel['gigi_mulut'])[1] == "y"){
                            $cap_mulut7[$j] = 1;   
                        }
                        else{
                            $cap_mulut7[$j] = 0;
                        }
                    }else{
                        $cap_gigi7[$j] = 0; $cap_mulut7[$j] =0;
                    }
                    if($pel['ketajaman_indera'] != null){
                        if(explode('_',$pel['ketajaman_indera'])[0] != null && explode('_',$pel['ketajaman_indera'])[0] == "y"){
                            $cap_mata7[$j] = 1;   
                        }
                        else{
                            $cap_mata7[$j] = 0;
                        }
                        if(explode('_',$pel['ketajaman_indera'])[1] != null && explode('_',$pel['ketajaman_indera'])[1] == "y"){
                            $cap_telinga7[$j] = 1;   
                        }
                        else{
                            $cap_telinga7[$j] = 0;
                        }
                    }else{
                        $cap_mata7[$j] = 0; $cap_telinga7[$j] =0;
                    }
                    if($cap_berat_total7 == 0){$cap_berat_total7 = $cap_berat_total7 + $cap_berat7[$j];}
                    if($cap_tinggi_total7 == 0){$cap_tinggi_total7 = $cap_tinggi_total7 + $cap_tinggi7[$j];}
                    if($cap_anemia_total7 == 0){$cap_anemia_total7 = $cap_anemia_total7 + $cap_anemia7[$j];}
                    if($cap_tek_darah_total7 == 0){$cap_tek_darah_total7 = $cap_tek_darah_total7 + $cap_tek_darah7[$j];}
                    if($cap_nadi_napas_total7 == 0){$cap_nadi_napas_total7 = $cap_nadi_napas_total7 + $cap_nadi_napas7[$j];}
                    if($cap_gigi_total7 == 0){$cap_gigi_total7 = $cap_gigi_total7 + $cap_gigi7[$j];}
                    if($cap_mulut_total7 == 0){$cap_mulut_total7 = $cap_mulut_total7 + $cap_mulut7[$j];}
                    if($cap_mata_total7 == 0){$cap_mata_total7 = $cap_mata_total7 + $cap_mata7[$j];}
                    if($cap_telinga_total7 == 0){$cap_telinga_total7 = $cap_telinga_total7 + $cap_telinga7[$j];}
                    $cap_pel[$j] = (((($cap_berat7[$j]+$cap_tinggi7[$j]+$cap_anemia7[$j])*100)/3)+
                    ((($cap_tek_darah7[$j]+$cap_nadi_napas7[$j])*100)/2)+((($cap_gigi7[$j]+$cap_mulut7[$j])*100)/2)+
                    ((($cap_mata7[$j]+$cap_telinga7[$j])*100)/2))/4;
                }
        }
        $capaian_super_total1 = (((($cap_berat_total1+$cap_tinggi_total1+$cap_anemia_total1)*100)/3)+
        ((($cap_tek_darah_total1+$cap_nadi_napas_total1)*100)/2)+((($cap_gigi_total1+$cap_mulut_total1)*100)/2)+
        ((($cap_mata_total1+$cap_telinga_total1)*100)/2))/4;
        $capaian_super_total7 = (((($cap_berat_total7+$cap_tinggi_total7+$cap_anemia_total7)*100)/3)+
        ((($cap_tek_darah_total7+$cap_nadi_napas_total7)*100)/2)+((($cap_gigi_total7+$cap_mulut_total7)*100)/2)+
        ((($cap_mata_total7+$cap_telinga_total7)*100)/2))/4;
        $capaian_super_total = ($capaian_super_total1+$capaian_super_total7)/2;
        return view('spm.usiapenddasar.pelayanan.ubah',compact('pelayanan','usiapenddasar','datawarga','capaian_super_total'));
    }

    public function editPelayanan(Request $request){
        if($request->anemia != null){$anemia = "y";}else{$anemia = "t";}
        if($request->gigi != null){$gigi = "y";}else{$gigi = "t";}
        if($request->mulut != null){$mulut = "y";}else{$mulut = "t";}
        if($request->mata != null){$mata = "y";}else{$mata = "t";}
        if($request->telinga != null){$telinga = "y";}else{$telinga = "t";}

        $a = date('Y-m-d', strtotime($request->tanggal_pelayanan));
        
        $data = Pelayanan::where('id',$request->id)->first();
        $data['tanggal_pelayanan']=$a;
        $data['kelas']= $request->kelas;
        $data['tenaga_kerja']= $request->tenaga_kesehatan."_".$request->nama_stk;
        $data['lokasi']= "Puskesmas_".$request->lokasi_pelayanan;
        $data['status_gizi']= $request->berat."_".$request->tinggi."_".$anemia;
        $data['tanda_vital']= $request->tekanan_darah."_".$request->nadi_napas;
        $data['gigi_mulut']= $gigi."_".$mulut;
        $data['ketajaman_indera']= $mata."_".$telinga;
        if($data->save()){
            return redirect('/pelayanan-usia-pend-dasar')->with('alert', 'Berhasil Ubah Pelayanan Usia Pendidikan Dasar');
        }  
        else{
            return redirect('/catat-usia-pend-dasar')->with('alert', 'Gagal Ubah Pelayanan Usia Pendidikan Dasar');
        }
    }

    public function inputnikPelayanan(){
        if(!empty(Session::get('withnik4'))){
            Session::forget('withnik4');
            return view('spm.usiapenddasar.pelayanan.masukan_nik');  
        }
        else{
            return view('spm.usiapenddasar.pelayanan.masukan_nik');  
        }
    }

    public function inputPelayanan(Request $request){
        date_default_timezone_set('Asia/Jakarta');
        Session::put('withnik4',$request->nik);
        $nik = Session::get('withnik4');
        $datawarga = Warga::where('nik', $nik)->first();
        if(count($datawarga) > 0 ){
            $usia = UsiaPendidikanDasarController::Usia($datawarga['tgl_lahir'],date("Y/m/d/h:i:s"));
            if($usia["years"] >= 5 && $usia["years"] <= 15){
                $usiapenddasar = UsiaPendDasar::where('nik', $nik)->where('status_usia_pend_dasar','tahap pelayanan')->first();
                if(count($usiapenddasar) > 0 ){
                    $pelayanan = Pelayanan::where('id_spm',$usiapenddasar['id'])->where('table_spm','usia_pend_dasar')->get();
                    
                    return view('spm.usiapenddasar.pelayanan.tambah', compact('datawarga','usiapenddasar'));
                }
                else{
                    return view('spm.usiapenddasar.pelayanan.masukan_nik')->with('alert','NIK belum terdaftar sebagai Bayi Baru Lahir');
                }
            }
            else{
                return view('spm.usiapenddasar.pelayanan.masukan_nik')->with('alert','NIK bukan beridentitas Usia Pendidikan Dasar');                
            }
        }
        else{
            return view('spm.usiapenddasar.pelayanan.masukan_nik')->with('alert','NIK tidak terdaftar');
        }
    }

    public function savePelayanan(Request $request){        
        if($request->anemia != null){$anemia = "y";}else{$anemia = "t";}
        if($request->gigi != null){$gigi = "y";}else{$gigi = "t";}
        if($request->mulut != null){$mulut = "y";}else{$mulut = "t";}
        if($request->mata != null){$mata = "y";}else{$mata = "t";}
        if($request->telinga != null){$telinga = "y";}else{$telinga = "t";}

        $a = date('Y-m-d', strtotime($request->tanggal_pelayanan));
        
        $data = Pelayanan::create([
            'tanggal_pelayanan' => $a,
            'table_spm' => 'usia_pend_dasar',
            'id_spm' => $request->id,
            'kelas' => $request->kelas,
            'tenaga_kerja' => $request->tenaga_kesehatan."_".$request->nama_stk,
            'lokasi' => "Puskesmas_".$request->lokasi_pelayanan,
            'status_gizi' => $request->berat."_".$request->tinggi."_".$anemia,
            'tanda_vital' => $request->tekanan_darah."_".$request->nadi_napas,
            'gigi_mulut' => $gigi."_".$mulut,
            'ketajaman_indera' => $mata."_".$telinga,
            'created_by' => $request->created_by
        ]);
        if($data->save()){
            return Redirect::route('detail-usia-pend-dasar',[$request->id])->with('success','Berhasil Tambah Pelayanan Usia Pendidikan Dasar');        
        }  
        else{
            return Redirect::route('catat-usia-pend-dasar')->with('danger','Gagal Tambah Pelayanan Usia Pendidikan Dasar');
        }
    }

    public function Usia($tgl1, $tgl2){
        $tgl1 = (is_string($tgl1) ? strtotime($tgl1) : $tgl1);
        $tgl2 = (is_string($tgl2) ? strtotime($tgl2) : $tgl2);
        $diff_secs = abs($tgl1-$tgl2);
        $base_year = min(date("Y",$tgl1), date("Y",$tgl2));
        $diff = mktime(0,0,$diff_secs,1,1,$base_year);
        return array(
            "years"=>date("Y",$diff) - $base_year, 
            "months_total" => (date("Y",$diff) - $base_year) * 12 + date("n",$diff) - 1, 
            "months" => date("n",$diff) -1, 
            "days_total" => floor($diff_secs/ (3600 * 24)), 
            "days" => date("j",$diff) - 1, 
            "hours_total" => floor($diff_secs/3600), 
            "hours" => date("G", $diff), 
            "minutes_total" => floor($diff_secs/60), 
            "minutes" => (int)date("i",$diff),
            "seconds_total" => $diff_secs, 
            "seconds" => (int) date("s",$diff)
        );
    }

}
