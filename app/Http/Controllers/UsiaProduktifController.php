<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Requests;
use Illuminate\Support\Facades\Session;
use Redirect;

use App\Pelayanan;
use App\Warga;
use App\UsiaProduktif;


class UsiaProduktifController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        return view('spm.usiaprod.index');
    }

    public function data(){
        $usiaprod_all = UsiaProduktif::all();
        $total_spm = 0;
        $total_tidak_spm = 0;
        $total_tahap_pelayanan = 0;
        $total_prod = 0;
        foreach($usiaprod_all as $index => $usiaprod){
            $total_prod++;
            if($usiaprod->status == "Tahap Pelayanan"){
                $total_tahap_pelayanan++;
            }
            if($usiaprod->status == "Sesuai SPM"){
                $total_spm++;
            }
            if($usiaprod->status == "Tidak Sesuai SPM"){
                $total_tidak_spm++;
            }
            $datawarga[$index] = Warga::where('nik',$usiaprod['nik'])->first();
            $usia_warga[$index] = UsiaProduktifController::Usia($datawarga[$index]['tgl_lahir'],date("Y/m/d/h:i:s"));
            $pelayanan_all[$index] = Pelayanan::where('table_spm','usia_prod')->where('id_spm',$usiaprod['id'])->get();
            for($i=15;$i<60;$i++){
                $cap_berat[$index][$i] = 0;$cap_tinggi[$index][$i] = 0;$cap_perut[$index][$i] = 0;$cap_tek_darah[$index][$i] = 0;$cap_gula_darah[$index][$i] = 0;
                $cap_emosional[$index][$i] = 0;$cap_peri[$index][$i] = 0;$cap_penglihatan[$index][$i] = 0;$cap_pendengaran[$index][$i] = 0;
                $cap_iva[$index][$i] =0; $cap_klinis[$index][$i] = 0;
            }
            foreach($pelayanan_all[$index] as $i => $pel){
                $usiapel = UsiaProduktifController::Usia($datawarga[$index]['tgl_lahir'],$pel['tanggal_pelayanan']);
                if($pel['berat_tinggi'] != null){
                    if(explode('_',$pel['berat_tinggi'])[0] != null){$cap_pel_berat[$index][$i] = 1;}else{$cap_pel_berat[$index][$i] = 0;}
                    if(explode('_',$pel['berat_tinggi'])[1] != null){$cap_pel_tinggi[$index][$i] = 1;}else{$cap_pel_tinggi[$index][$i] = 0;}
                }else{$cap_pel_berat[$index][$i] = 0; $cap_pel_tinggi[$index][$i]=0;}
                if($pel['lingkar_perut'] != null){$cap_pel_perut[$index][$i] =1;}else{$cap_pel_perut[$index][$i] =0;}
                if($pel['tekanan_darah'] != null){$cap_pel_tekanan_darah[$index][$i] =1;}else{$cap_pel_tekanan_darah[$index][$i] =0;}
                if($pel['tes_cepat_gula_darah'] == "y"){$cap_pel_tes_gula_darah[$index][$i] =1;}else{$cap_pel_tes_gula_darah[$index][$i] =0;}
                if($pel['tes_gangguan_emosional'] == "y"){$cap_pel_emo[$index][$i] =1;}else{$cap_pel_emo[$index][$i] =0;}
                if($pel['tes_gangguan_perilaku'] == "y"){$cap_pel_perilaku[$index][$i] =1;}else{$cap_pel_perilaku[$index][$i] =0;}
                if($pel['periksa_indra_penglihatan'] == "y"){$cap_pel_mata[$index][$i] =1;}else{$cap_pel_mata[$index][$i] =0;}
                if($pel['periksa_indra_pendengaran'] == "y"){$cap_pel_telinga[$index][$i] =1;}else{$cap_pel_telinga[$index][$i] =0;}
                if($datawarga[$index]['jenis_kelamin'] == "P"){
                    if($usiapel['years'] >= 30 && $usiapel['years'] <= 59 ){
                        if($pel['pemeriksaan_payudara_klinis'] == "y"){$cap_pel_payudara_klinis[$index][$i] =1;}else{$cap_pel_payudara_klinis[$index][$i] =0;}
                        if($pel['pemeriksaan_iva'] == "y"){$cap_pel_iva[$index][$i] =1;}else{$cap_pel_iva[$index][$i] =0;}
                    }
                }
            }
            foreach($pelayanan_all[$index] as $i => $pel){
                $usiapel = UsiaProduktifController::Usia($datawarga[$index]['tgl_lahir'],$pel['tanggal_pelayanan']);
                $usia = $usiapel['years'];
                if($cap_berat[$index][$usia] < 1){$cap_berat[$index][$usia] = $cap_berat[$index][$usia]+$cap_pel_berat[$index][$i];}
                if($cap_tinggi[$index][$usia] < 1){$cap_tinggi[$index][$usia] = $cap_tinggi[$index][$usia]+$cap_pel_tinggi[$index][$i];}
                if($cap_perut[$index][$usia] < 1){$cap_perut[$index][$usia] = $cap_perut[$index][$usia]+$cap_pel_perut[$index][$i];}
                if($cap_tek_darah[$index][$usia] < 1){$cap_tek_darah[$index][$usia] = $cap_tek_darah[$index][$usia]+$cap_pel_tekanan_darah[$index][$i];}
                if($cap_gula_darah[$index][$usia] < 1){$cap_gula_darah[$index][$usia] = $cap_gula_darah[$index][$usia]+$cap_pel_tes_gula_darah[$index][$i];}
                if($cap_emosional[$index][$usia] < 1){$cap_emosional[$index][$usia] = $cap_emosional[$index][$usia]+$cap_pel_emo[$index][$i];}
                if($cap_peri[$index][$usia] < 1){$cap_peri[$index][$usia] = $cap_peri[$index][$usia]+$cap_pel_perilaku[$index][$i];}
                if($cap_penglihatan[$index][$usia] < 1){$cap_penglihatan[$index][$usia] = $cap_penglihatan[$index][$usia]+$cap_pel_mata[$index][$i];}
                if($cap_pendengaran[$index][$usia] < 1){$cap_pendengaran[$index][$usia] = $cap_pendengaran[$index][$usia]+$cap_pel_telinga[$index][$i];}
                if($datawarga[$index]['jenis_kelamin'] == "P"){
                    if($usiapel['years'] >= 30 && $usiapel['years'] <= 59 ){
                        if($cap_klinis[$index][$usia] < 1){$cap_klinis[$index][$usia] = $cap_klinis[$index][$usia]+$cap_pel_payudara_klinis[$index][$i];}
                        if($cap_iva[$index][$usia] < 1){$cap_iva[$index][$usia] = $cap_iva[$index][$usia]+$cap_pel_iva[$index][$i];}
                    }
                }
            }
            $capaian[$index]=0;
            for($i=15;$i<60;$i++){
                if($datawarga[$index]['jenis_kelamin'] == "P"){
                    if($i >= 30 && $i <= 59 ){
                        $cap_pel_usia[$index][$i] =(((($cap_berat[$index][$i]+$cap_tinggi[$index][$i]+$cap_perut[$index][$i])*100)/3)+($cap_tek_darah[$index][$i]*100)+($cap_gula_darah[$index][$i]*100)+
                        ((($cap_emosional[$index][$i]+$cap_peri[$index][$i])*100)/2)+($cap_penglihatan[$index][$i]*100)+($cap_pendengaran[$index][$i]*100)+((($cap_klinis[$index][$i]+$cap_iva[$index][$i])*100)/2)/7);
                    }
                    else{
                        $cap_pel_usia[$index][$i] =(((($cap_berat[$index][$i]+$cap_tinggi[$index][$i]+$cap_perut[$index][$i])*100)/3)+($cap_tek_darah[$index][$i]*100)+($cap_gula_darah[$index][$i]*100)+
                        ((($cap_emosional[$index][$i]+$cap_peri[$index][$i])*100)/2)+($cap_penglihatan[$index][$i]*100)+($cap_pendengaran[$index][$i]*100))/6;
                    }
                }else{
                    $cap_pel_usia[$index][$i] =(((($cap_berat[$index][$i]+$cap_tinggi[$index][$i]+$cap_perut[$index][$i])*100)/3)+($cap_tek_darah[$index][$i]*100)+($cap_gula_darah[$index][$i]*100)+
                    ((($cap_emosional[$index][$i]+$cap_peri[$index][$i])*100)/2)+($cap_penglihatan[$index][$i]*100)+($cap_pendengaran[$index][$i]*100))/6;
                }
    
                $capaian[$index] = $capaian[$index] + $cap_pel_usia[$index][$i];
            }
            $selisih[$index] = (60-15)+1;
            $capaian_fix[$index] = $capaian[$index]/$selisih[$index];
        }
        return view('spm.usiaprod.data',compact('usiaprod_all','total_spm','total_tidak_spm','total_tahap_pelayanan','total_prod','datawarga','usia_warga','capaian_fix'));
    }

    public function tambah(){
        date_default_timezone_set('Asia/Jakarta');
        $all_warga = Warga::all();
        $list_ca_prod = [];
        foreach($all_warga as $index => $warga){
            $data = UsiaProduktif::where('nik', $warga['nik'])->where('status','tahap pelayanan')->first();
                if(!$data){
                    $usia = UsiaProduktifController::Usia($warga['tgl_lahir'],date("Y/m/d/h:i:s"));
                    if($usia["years"] >= 15 && $usia["years"] <= 59){
                        $list_ca_prod[$index] = $warga;
                        $list_nik[$index] = $warga['nik'];
                        $list_name[$index] = $warga['nama'];
                        $list_ttl[$index] = $warga['tgl_lahir'];                            
                    }
                }
        }
        return view('spm.usiaprod.tambah',compact('list_nik','list_name','list_ttl','list_ca_prod'));
    }
    
    public function save(Request $request){
        $warga = Warga::where('id',$request->select)->first();
        $data = New UsiaProduktif();
        $data['nik'] = $warga->nik;
        $data['status'] = "Tahap Pelayanan";
        if($data->save()){
            return Redirect::route('data-usia-prod')->with('success','Berhasil Tambah Usia Produktif');
        }  
        else{
            return Redirect::route('tambah-usia-prod')->with('danger','Gagal Tambah Usia Produktif');
        }
    }

    public function edit(Request $request){
        $usiaprod = UsiaProduktif::where('id',$request->id)->first();
        $usiaprod['nik'] = $request->nik;
        if($usiaprod->save()){
            return redirect('/data-usia-prod')->with('alert', 'Berhasil Ubah NIK Usia Produktif');
        }  
        else{
            return redirect('/data-usia-prod')->with('alert', 'Gagal Ubah NIK Usia Produktif');
        }
    }

    public function delete($id){
        $UsiaProduktif=UsiaProduktif::where('id',$id)->first();
        $delete = $UsiaProduktif->delete();
        
        if($delete){
            return Redirect::route('data-usia-prod')->with('success','Berhasil Hapus Usia Produktif');
        }  
        else{
            return Redirect::route('data-usia-prod')->with('danger','Gagal Hapus Usia Produktif');
        }
    }

    public function delete_pelayanan($id){
        $pelayanan=Pelayanan::where('id',$id)->first();
        $delete = $pelayanan->delete();
        
        if($delete){
            return Redirect::route('data-usia-prod')->with('success','Berhasil Hapus Pelayanan');
        }  
        else{
            return Redirect::route('data-usia-prod')->with('danger','Gagal Hapus Pelayanan');
        }
    }

    public function detail($id){
        $usiaprod = UsiaProduktif::where('id',$id)->first();
        $datawarga = Warga::where('nik',$usiaprod['nik'])->first();
        Session::put('withnik5',$datawarga['nik']);
        date_default_timezone_set('Asia/Jakarta');
        $usia_warga = UsiaProduktifController::Usia($datawarga['tgl_lahir'],date("Y/m/d/h:i:s"));
        $pelayanan_all = Pelayanan::where('table_spm','usia_prod')->where('id_spm',$usiaprod['id'])->get();

        for($i=15;$i<60;$i++){
            $cap_berat[$i] = 0;$cap_tinggi[$i] = 0;$cap_perut[$i] = 0;$cap_tek_darah[$i] = 0;$cap_gula_darah[$i] = 0;
            $cap_emosional[$i] = 0;$cap_peri[$i] = 0;$cap_penglihatan[$i] = 0;$cap_pendengaran[$i] = 0;
            $cap_iva[$i] =0; $cap_klinis[$i] = 0;
        }

        foreach($pelayanan_all as $i => $pel){
            $usiapel = UsiaProduktifController::Usia($datawarga['tgl_lahir'],$pel['tanggal_pelayanan']);
            $usia = $usiapel['years'];
            if($pel['berat_tinggi'] != null){
                if(explode('_',$pel['berat_tinggi'])[0] != null){$cap_pel_berat[$i] = 1;}else{$cap_pel_berat[$i] = 0;}
                if(explode('_',$pel['berat_tinggi'])[1] != null){$cap_pel_tinggi[$i] = 1;}else{$cap_pel_tinggi[$i] = 0;}
            }else{$cap_pel_berat[$i] = 0; $cap_pel_tinggi[$i]=0;}
            if($pel['lingkar_perut'] != null){$cap_pel_perut[$i] =1;}else{$cap_pel_perut[$i] =0;}
            if($pel['tekanan_darah'] != null){$cap_pel_tekanan_darah[$i] =1;}else{$cap_pel_tekanan_darah[$i] =0;}
            if($pel['tes_cepat_gula_darah'] == "y"){$cap_pel_tes_gula_darah[$i] =1;}else{$cap_pel_tes_gula_darah[$i] =0;}
            if($pel['tes_gangguan_emosional'] == "y"){$cap_pel_emo[$i] =1;}else{$cap_pel_emo[$i] =0;}
            if($pel['tes_gangguan_perilaku'] == "y"){$cap_pel_perilaku[$i] =1;}else{$cap_pel_perilaku[$i] =0;}
            if($pel['periksa_indra_penglihatan'] == "y"){$cap_pel_mata[$i] =1;}else{$cap_pel_mata[$i] =0;}
            if($pel['periksa_indra_pendengaran'] == "y"){$cap_pel_telinga[$i] =1;}else{$cap_pel_telinga[$i] =0;}
            if($datawarga['jenis_kelamin'] == "P"){
                if($usiapel['years'] >= 30 && $usiapel['years'] <= 59 ){
                    if($pel['pemeriksaan_payudara_klinis'] == "y"){$cap_pel_payudara_klinis[$i] =1;}else{$cap_pel_payudara_klinis[$i] =0;}
                    if($pel['pemeriksaan_iva'] == "y"){$cap_pel_iva[$i] =1;}else{$cap_pel_iva[$i] =0;}
                    $cap_pel[$i] =(((($cap_pel_berat[$i]+$cap_pel_tinggi[$i]+$cap_pel_perut[$i])*100)/3)+($cap_pel_tekanan_darah[$i]*100)+($cap_pel_tes_gula_darah[$i]*100)+
                    ((($cap_pel_emo[$i]+$cap_pel_perilaku[$i])*100)/2)+($cap_pel_mata[$i]*100)+($cap_pel_telinga[$i]*100)+((($cap_pel_payudara_klinis[$i]+$cap_pel_iva[$i])*100)/2)/7);
                }
                else{
                    $cap_pel[$i] =(((($cap_pel_berat[$i]+$cap_pel_tinggi[$i]+$cap_pel_perut[$i])*100)/3)+($cap_pel_tekanan_darah[$i]*100)+($cap_pel_tes_gula_darah[$i]*100)+
                    ((($cap_pel_emo[$i]+$cap_pel_perilaku[$i])*100)/2)+($cap_pel_mata[$i]*100)+($cap_pel_telinga[$i]*100))/6;
                }
            }else{
                $cap_pel[$i] =(((($cap_pel_berat[$i]+$cap_pel_tinggi[$i]+$cap_pel_perut[$i])*100)/3)+($cap_pel_tekanan_darah[$i]*100)+($cap_pel_tes_gula_darah[$i]*100)+
                ((($cap_pel_emo[$i]+$cap_pel_perilaku[$i])*100)/2)+($cap_pel_mata[$i]*100)+($cap_pel_telinga[$i]*100))/6;
            }
        }

        foreach($pelayanan_all as $i => $pel){
            $usiapel = UsiaProduktifController::Usia($datawarga['tgl_lahir'],$pel['tanggal_pelayanan']);
            $usia = $usiapel['years'];
            if($cap_berat[$usia] < 1){$cap_berat[$usia] = $cap_berat[$usia]+$cap_pel_berat[$i];}
            if($cap_tinggi[$usia] < 1){$cap_tinggi[$usia] = $cap_tinggi[$usia]+$cap_pel_tinggi[$i];}
            if($cap_perut[$usia] < 1){$cap_perut[$usia] = $cap_perut[$usia]+$cap_pel_perut[$i];}
            if($cap_tek_darah[$usia] < 1){$cap_tek_darah[$usia] = $cap_tek_darah[$usia]+$cap_pel_tekanan_darah[$i];}
            if($cap_gula_darah[$usia] < 1){$cap_gula_darah[$usia] = $cap_gula_darah[$usia]+$cap_pel_tes_gula_darah[$i];}
            if($cap_emosional[$usia] < 1){$cap_emosional[$usia] = $cap_emosional[$usia]+$cap_pel_emo[$i];}
            if($cap_peri[$usia] < 1){$cap_peri[$usia] = $cap_peri[$usia]+$cap_pel_perilaku[$i];}
            if($cap_penglihatan[$usia] < 1){$cap_penglihatan[$usia] = $cap_penglihatan[$usia]+$cap_pel_mata[$i];}
            if($cap_pendengaran[$usia] < 1){$cap_pendengaran[$usia] = $cap_pendengaran[$usia]+$cap_pel_telinga[$i];}
            if($datawarga['jenis_kelamin'] == "P"){
                if($usiapel['years'] >= 30 && $usiapel['years'] <= 59 ){
                    if($cap_klinis[$usia] < 1){$cap_klinis[$usia] = $cap_klinis[$usia]+$cap_pel_payudara_klinis[$i];}
                    if($cap_iva[$usia] < 1){$cap_iva[$usia] = $cap_iva[$usia]+$cap_pel_iva[$i];}
                }
            }
        }

        $capaian =0;
        for($i=15;$i<60;$i++){
            if($datawarga['jenis_kelamin'] == "P"){
                if($i >= 30 && $i <= 59 ){
                    $cap_pel_usia[$i] =(((($cap_berat[$i]+$cap_tinggi[$i]+$cap_perut[$i])*100)/3)+($cap_tek_darah[$i]*100)+($cap_gula_darah[$i]*100)+
                    ((($cap_emosional[$i]+$cap_peri[$i])*100)/2)+($cap_penglihatan[$i]*100)+($cap_pendengaran[$i]*100)+((($cap_klinis[$i]+$cap_iva[$i])*100)/2)/7);
                }
                else{
                    $cap_pel_usia[$i] =(((($cap_berat[$i]+$cap_tinggi[$i]+$cap_perut[$i])*100)/3)+($cap_tek_darah[$i]*100)+($cap_gula_darah[$i]*100)+
                    ((($cap_emosional[$i]+$cap_peri[$i])*100)/2)+($cap_penglihatan[$i]*100)+($cap_pendengaran[$i]*100))/6;
                }
            }else{
                $cap_pel_usia[$i] =(((($cap_berat[$i]+$cap_tinggi[$i]+$cap_perut[$i])*100)/3)+($cap_tek_darah[$i]*100)+($cap_gula_darah[$i]*100)+
                ((($cap_emosional[$i]+$cap_peri[$i])*100)/2)+($cap_penglihatan[$i]*100)+($cap_pendengaran[$i]*100))/6;
            }

            $capaian = $capaian + $cap_pel_usia[$i];
        }

        $selisih = (60-15)+1;
        $capaian_fix = $capaian/$selisih;
        return view('spm.usiaprod.detail',compact('usiaprod','capaian_fix','datawarga','pelayanan_all','usia_warga','cap_pel'));
    }

    public function ubah($id){
        date_default_timezone_set('Asia/Jakarta');
        $usiaprod = UsiaProduktif::where('id',$id)->first();
        $datawarga = Warga::where('nik',$usiaprod['nik'])->first();
        $warga_all = Warga::all();
        $list_nik = [];
        $list_name = [];
        $list_ttl = [];
        foreach($warga_all as $indexwarga => $warga){
            $data = UsiaProduktif::where('nik', $warga['nik'])->where('status','tahap pelayanan')->first();
            if(!$data){
                $usia[$indexwarga] = UsiaProduktifController::Usia($warga['tgl_lahir'],date("Y/m/d/h:i:s"));
                if($usia[$indexwarga]["years"] >= 15 && $usia[$indexwarga]["years"] <= 59){
                    $list_nik[$indexwarga] = $warga['nik'];
                    $list_name[$indexwarga] = $warga['nama'];
                    $list_ttl[$indexwarga] = $warga['tgl_lahir'];
                }
            }  
        }

        $pelayanan_all = Pelayanan::where('table_spm','usia_prod')->where('id_spm',$usiaprod['id'])->get();

        for($i=15;$i<60;$i++){
            $cap_berat[$i] = 0;$cap_tinggi[$i] = 0;$cap_perut[$i] = 0;$cap_tek_darah[$i] = 0;$cap_gula_darah[$i] = 0;
            $cap_emosional[$i] = 0;$cap_peri[$i] = 0;$cap_penglihatan[$i] = 0;$cap_pendengaran[$i] = 0;
            $cap_iva[$i] =0; $cap_klinis[$i] = 0;
        }

        foreach($pelayanan_all as $i => $pel){
            $usiapel = UsiaProduktifController::Usia($datawarga['tgl_lahir'],$pel['tanggal_pelayanan']);
            $usia = $usiapel['years'];
            if($pel['berat_tinggi'] != null){
                if(explode('_',$pel['berat_tinggi'])[0] != null){$cap_pel_berat[$i] = 1;}else{$cap_pel_berat[$i] = 0;}
                if(explode('_',$pel['berat_tinggi'])[1] != null){$cap_pel_tinggi[$i] = 1;}else{$cap_pel_tinggi[$i] = 0;}
            }else{$cap_pel_berat[$i] = 0; $cap_pel_tinggi[$i]=0;}
            if($pel['lingkar_perut'] != null){$cap_pel_perut[$i] =1;}else{$cap_pel_perut[$i] =0;}
            if($pel['tekanan_darah'] != null){$cap_pel_tekanan_darah[$i] =1;}else{$cap_pel_tekanan_darah[$i] =0;}
            if($pel['tes_cepat_gula_darah'] == "y"){$cap_pel_tes_gula_darah[$i] =1;}else{$cap_pel_tes_gula_darah[$i] =0;}
            if($pel['tes_gangguan_emosional'] == "y"){$cap_pel_emo[$i] =1;}else{$cap_pel_emo[$i] =0;}
            if($pel['tes_gangguan_perilaku'] == "y"){$cap_pel_perilaku[$i] =1;}else{$cap_pel_perilaku[$i] =0;}
            if($pel['periksa_indra_penglihatan'] == "y"){$cap_pel_mata[$i] =1;}else{$cap_pel_mata[$i] =0;}
            if($pel['periksa_indra_pendengaran'] == "y"){$cap_pel_telinga[$i] =1;}else{$cap_pel_telinga[$i] =0;}
            if($datawarga['jenis_kelamin'] == "P"){
                if($usiapel['years'] >= 30 && $usiapel['years'] <= 59 ){
                    if($pel['pemeriksaan_payudara_klinis'] == "y"){$cap_pel_payudara_klinis[$i] =1;}else{$cap_pel_payudara_klinis[$i] =0;}
                    if($pel['pemeriksaan_iva'] == "y"){$cap_pel_iva[$i] =1;}else{$cap_pel_iva[$i] =0;}
                }
            }
        }

        foreach($pelayanan_all as $i => $pel){
            $usiapel = UsiaProduktifController::Usia($datawarga['tgl_lahir'],$pel['tanggal_pelayanan']);
            $usia = $usiapel['years'];
            if($cap_berat[$usia] < 1){$cap_berat[$usia] = $cap_berat[$usia]+$cap_pel_berat[$i];}
            if($cap_tinggi[$usia] < 1){$cap_tinggi[$usia] = $cap_tinggi[$usia]+$cap_pel_tinggi[$i];}
            if($cap_perut[$usia] < 1){$cap_perut[$usia] = $cap_perut[$usia]+$cap_pel_perut[$i];}
            if($cap_tek_darah[$usia] < 1){$cap_tek_darah[$usia] = $cap_tek_darah[$usia]+$cap_pel_tekanan_darah[$i];}
            if($cap_gula_darah[$usia] < 1){$cap_gula_darah[$usia] = $cap_gula_darah[$usia]+$cap_pel_tes_gula_darah[$i];}
            if($cap_emosional[$usia] < 1){$cap_emosional[$usia] = $cap_emosional[$usia]+$cap_pel_emo[$i];}
            if($cap_peri[$usia] < 1){$cap_peri[$usia] = $cap_peri[$usia]+$cap_pel_perilaku[$i];}
            if($cap_penglihatan[$usia] < 1){$cap_penglihatan[$usia] = $cap_penglihatan[$usia]+$cap_pel_mata[$i];}
            if($cap_pendengaran[$usia] < 1){$cap_pendengaran[$usia] = $cap_pendengaran[$usia]+$cap_pel_telinga[$i];}
            if($datawarga['jenis_kelamin'] == "P"){
                if($usiapel['years'] >= 30 && $usiapel['years'] <= 59 ){
                    if($cap_klinis[$usia] < 1){$cap_klinis[$usia] = $cap_klinis[$usia]+$cap_pel_payudara_klinis[$i];}
                    if($cap_iva[$usia] < 1){$cap_iva[$usia] = $cap_iva[$usia]+$cap_pel_iva[$i];}
                }
            }
        }

        $capaian =0;
        for($i=15;$i<60;$i++){
            if($datawarga['jenis_kelamin'] == "P"){
                if($i >= 30 && $i <= 59 ){
                    $cap_pel_usia[$i] =(((($cap_berat[$i]+$cap_tinggi[$i]+$cap_perut[$i])*100)/3)+($cap_tek_darah[$i]*100)+($cap_gula_darah[$i]*100)+
                    ((($cap_emosional[$i]+$cap_peri[$i])*100)/2)+($cap_penglihatan[$i]*100)+($cap_pendengaran[$i]*100)+((($cap_klinis[$i]+$cap_iva[$i])*100)/2)/7);
                }
                else{
                    $cap_pel_usia[$i] =(((($cap_berat[$i]+$cap_tinggi[$i]+$cap_perut[$i])*100)/3)+($cap_tek_darah[$i]*100)+($cap_gula_darah[$i]*100)+
                    ((($cap_emosional[$i]+$cap_peri[$i])*100)/2)+($cap_penglihatan[$i]*100)+($cap_pendengaran[$i]*100))/6;
                }
            }else{
                $cap_pel_usia[$i] =(((($cap_berat[$i]+$cap_tinggi[$i]+$cap_perut[$i])*100)/3)+($cap_tek_darah[$i]*100)+($cap_gula_darah[$i]*100)+
                ((($cap_emosional[$i]+$cap_peri[$i])*100)/2)+($cap_penglihatan[$i]*100)+($cap_pendengaran[$i]*100))/6;
            }

            $capaian = $capaian + $cap_pel_usia[$i];
        }

        $selisih = (60-15)+1;
        $capaian_fix = $capaian/$selisih;

        return view('spm.usiaprod.ubah',compact('list_nik','list_name','list_ttl','usiaprod','data_warga','capaian_fix','datawarga'));
    }

    public function indexPelayanan(){
        $pelayanan_usia_prod = Pelayanan::where('table_spm','usia_prod')->get(); 
        foreach($pelayanan_usia_prod as $i => $pel){
            $data_usiaprod[$i] = UsiaProduktif::where('id',$pel['id_spm'])->first();  
            $datawarga[$i] = Warga::where('nik',$data_usiaprod[$i]['nik'])->first();
            $usiapel[$i] = UsiaProduktifController::Usia($datawarga[$i]['tgl_lahir'],$pel['tanggal_pelayanan']);
            if($pel['berat_tinggi'] != null){
                if(explode('_',$pel['berat_tinggi'])[0] != null){$cap_pel_berat = 1;}else{$cap_pel_berat = 0;}
                if(explode('_',$pel['berat_tinggi'])[1] != null){$cap_pel_tinggi = 1;}else{$cap_pel_tinggi = 0;}
            }else{$cap_pel_berat = 0; $cap_pel_tinggi=0;}
            if($pel['lingkar_perut'] != null){$cap_pel_perut =1;}else{$cap_pel_perut =0;}
            if($pel['tekanan_darah'] != null){$cap_tekanan_darah =1;}else{$cap_tekanan_darah =0;}
            if($pel['tes_cepat_gula_darah'] == "y"){$cap_tes_gula_darah =1;}else{$cap_tes_gula_darah =0;}
            if($pel['tes_gangguan_emosional'] == "y"){$cap_emo =1;}else{$cap_emo =0;}
            if($pel['tes_gangguan_perilaku'] == "y"){$cap_perilaku =1;}else{$cap_perilaku =0;}
            if($pel['periksa_indra_penglihatan'] == "y"){$cap_mata =1;}else{$cap_mata =0;}
            if($pel['periksa_indra_pendengaran'] == "y"){$cap_telinga =1;}else{$cap_telinga =0;}
            if($datawarga[$i]['jenis_kelamin'] == "P"){
                if($usiapel[$i]['years'] >= 30 && $usiapel[$i]['years'] <= 59 ){
                    if($pel['pemeriksaan_payudara_klinis'] == "y"){$cap_payudara_klinis =1;}else{$cap_payudara_klinis =0;}
                    if($pel['pemeriksaan_iva'] == "y"){$cap_iva =1;}else{$cap_iva =0;}
                    $cap_pel[$i] =(((($cap_pel_berat+$cap_pel_tinggi+$cap_pel_perut)*100)/3)+($cap_tekanan_darah*100)+($cap_tes_gula_darah*100)+
                    ((($cap_emo+$cap_perilaku)*100)/2)+($cap_mata*100)+($cap_telinga*100)+((($cap_payudara_klinis+$cap_iva)*100)/2)/7);
                }
                else{
                    $cap_pel[$i] =(((($cap_pel_berat+$cap_pel_tinggi+$cap_pel_perut)*100)/3)+($cap_tekanan_darah*100)+($cap_tes_gula_darah*100)+
                    ((($cap_emo+$cap_perilaku)*100)/2)+($cap_mata*100)+($cap_telinga*100))/6;
                }
            }else{
                $cap_pel[$i] =(((($cap_pel_berat+$cap_pel_tinggi+$cap_pel_perut)*100)/3)+($cap_tekanan_darah*100)+($cap_tes_gula_darah*100)+
                ((($cap_emo+$cap_perilaku)*100)/2)+($cap_mata*100)+($cap_telinga*100))/6;
            }
        }
        return view('spm.usiaprod.pelayanan.data',compact('pelayanan_usia_prod','data_usiaprod','datawarga','usiapel','cap_pel'));
    }
    public function detailPelayanan($id){
        $pelayanan = Pelayanan::where('id',$id)->first();
        $usiaprod = UsiaProduktif::where('id',$pelayanan['id_spm'])->first();
        $datawarga = Warga::where('nik',$usiaprod['nik'])->first();
        $pelayanan_all = Pelayanan::where('table_spm','usia_prod')->where('id_spm',$usiaprod['id'])->get();

        for($i=15;$i<60;$i++){
            $cap_berat[$i] = 0;$cap_tinggi[$i] = 0;$cap_perut[$i] = 0;$cap_tek_darah[$i] = 0;$cap_gula_darah[$i] = 0;
            $cap_emosional[$i] = 0;$cap_peri[$i] = 0;$cap_penglihatan[$i] = 0;$cap_pendengaran[$i] = 0;
            $cap_iva[$i] =0; $cap_klinis[$i] = 0;
        }

        foreach($pelayanan_all as $i => $pel){
            $usiapel = UsiaProduktifController::Usia($datawarga['tgl_lahir'],$pel['tanggal_pelayanan']);
            $usia = $usiapel['years'];
            if($pel['berat_tinggi'] != null){
                if(explode('_',$pel['berat_tinggi'])[0] != null){$cap_pel_berat[$i] = 1;}else{$cap_pel_berat[$i] = 0;}
                if(explode('_',$pel['berat_tinggi'])[1] != null){$cap_pel_tinggi[$i] = 1;}else{$cap_pel_tinggi[$i] = 0;}
            }else{$cap_pel_berat[$i] = 0; $cap_pel_tinggi[$i]=0;}
            if($pel['lingkar_perut'] != null){$cap_pel_perut[$i] =1;}else{$cap_pel_perut[$i] =0;}
            if($pel['tekanan_darah'] != null){$cap_pel_tekanan_darah[$i] =1;}else{$cap_pel_tekanan_darah[$i] =0;}
            if($pel['tes_cepat_gula_darah'] == "y"){$cap_pel_tes_gula_darah[$i] =1;}else{$cap_pel_tes_gula_darah[$i] =0;}
            if($pel['tes_gangguan_emosional'] == "y"){$cap_pel_emo[$i] =1;}else{$cap_pel_emo[$i] =0;}
            if($pel['tes_gangguan_perilaku'] == "y"){$cap_pel_perilaku[$i] =1;}else{$cap_pel_perilaku[$i] =0;}
            if($pel['periksa_indra_penglihatan'] == "y"){$cap_pel_mata[$i] =1;}else{$cap_pel_mata[$i] =0;}
            if($pel['periksa_indra_pendengaran'] == "y"){$cap_pel_telinga[$i] =1;}else{$cap_pel_telinga[$i] =0;}
            if($datawarga['jenis_kelamin'] == "P"){
                if($usiapel['years'] >= 30 && $usiapel['years'] <= 59 ){
                    if($pel['pemeriksaan_payudara_klinis'] == "y"){$cap_pel_payudara_klinis[$i] =1;}else{$cap_pel_payudara_klinis[$i] =0;}
                    if($pel['pemeriksaan_iva'] == "y"){$cap_pel_iva[$i] =1;}else{$cap_pel_iva[$i] =0;}
                }
            }
        }

        foreach($pelayanan_all as $i => $pel){
            $usiapel = UsiaProduktifController::Usia($datawarga['tgl_lahir'],$pel['tanggal_pelayanan']);
            $usia = $usiapel['years'];
            if($cap_berat[$usia] < 1){$cap_berat[$usia] = $cap_berat[$usia]+$cap_pel_berat[$i];}
            if($cap_tinggi[$usia] < 1){$cap_tinggi[$usia] = $cap_tinggi[$usia]+$cap_pel_tinggi[$i];}
            if($cap_perut[$usia] < 1){$cap_perut[$usia] = $cap_perut[$usia]+$cap_pel_perut[$i];}
            if($cap_tek_darah[$usia] < 1){$cap_tek_darah[$usia] = $cap_tek_darah[$usia]+$cap_pel_tekanan_darah[$i];}
            if($cap_gula_darah[$usia] < 1){$cap_gula_darah[$usia] = $cap_gula_darah[$usia]+$cap_pel_tes_gula_darah[$i];}
            if($cap_emosional[$usia] < 1){$cap_emosional[$usia] = $cap_emosional[$usia]+$cap_pel_emo[$i];}
            if($cap_peri[$usia] < 1){$cap_peri[$usia] = $cap_peri[$usia]+$cap_pel_perilaku[$i];}
            if($cap_penglihatan[$usia] < 1){$cap_penglihatan[$usia] = $cap_penglihatan[$usia]+$cap_pel_mata[$i];}
            if($cap_pendengaran[$usia] < 1){$cap_pendengaran[$usia] = $cap_pendengaran[$usia]+$cap_pel_telinga[$i];}
            if($datawarga['jenis_kelamin'] == "P"){
                if($usiapel['years'] >= 30 && $usiapel['years'] <= 59 ){
                    if($cap_klinis[$usia] < 1){$cap_klinis[$usia] = $cap_klinis[$usia]+$cap_pel_payudara_klinis[$i];}
                    if($cap_iva[$usia] < 1){$cap_iva[$usia] = $cap_iva[$usia]+$cap_pel_iva[$i];}
                }
            }
        }

        $capaian =0;
        for($i=15;$i<60;$i++){
            if($datawarga['jenis_kelamin'] == "P"){
                if($i >= 30 && $i <= 59 ){
                    $cap_pel_usia[$i] =(((($cap_berat[$i]+$cap_tinggi[$i]+$cap_perut[$i])*100)/3)+($cap_tek_darah[$i]*100)+($cap_gula_darah[$i]*100)+
                    ((($cap_emosional[$i]+$cap_peri[$i])*100)/2)+($cap_penglihatan[$i]*100)+($cap_pendengaran[$i]*100)+((($cap_klinis[$i]+$cap_iva[$i])*100)/2)/7);
                }
                else{
                    $cap_pel_usia[$i] =(((($cap_berat[$i]+$cap_tinggi[$i]+$cap_perut[$i])*100)/3)+($cap_tek_darah[$i]*100)+($cap_gula_darah[$i]*100)+
                    ((($cap_emosional[$i]+$cap_peri[$i])*100)/2)+($cap_penglihatan[$i]*100)+($cap_pendengaran[$i]*100))/6;
                }
            }else{
                $cap_pel_usia[$i] =(((($cap_berat[$i]+$cap_tinggi[$i]+$cap_perut[$i])*100)/3)+($cap_tek_darah[$i]*100)+($cap_gula_darah[$i]*100)+
                ((($cap_emosional[$i]+$cap_peri[$i])*100)/2)+($cap_penglihatan[$i]*100)+($cap_pendengaran[$i]*100))/6;
            }

            $capaian = $capaian + $cap_pel_usia[$i];
        }

        $selisih = (60-15)+1;
        $capaian_fix = $capaian/$selisih;

        return view('spm.usiaprod.pelayanan.detail',compact('pelayanan','usiaprod','datawarga','capaian_fix'));
    }
    public function ubahPelayanan($id){
        $pelayanan = Pelayanan::where('id',$id)->first();
        $usiaprod = UsiaProduktif::where('id',$pelayanan['id_spm'])->first();
        $datawarga = Warga::where('nik',$usiaprod['nik'])->first();
        $pelayanan_all = Pelayanan::where('table_spm','usia_prod')->where('id_spm',$usiaprod['id'])->get();

        for($i=15;$i<60;$i++){
            $cap_berat[$i] = 0;$cap_tinggi[$i] = 0;$cap_perut[$i] = 0;$cap_tek_darah[$i] = 0;$cap_gula_darah[$i] = 0;
            $cap_emosional[$i] = 0;$cap_peri[$i] = 0;$cap_penglihatan[$i] = 0;$cap_pendengaran[$i] = 0;
            $cap_iva[$i] =0; $cap_klinis[$i] = 0;
        }

        foreach($pelayanan_all as $i => $pel){
            $usiapel = UsiaProduktifController::Usia($datawarga['tgl_lahir'],$pel['tanggal_pelayanan']);
            $usia = $usiapel['years'];
            if($pel['berat_tinggi'] != null){
                if(explode('_',$pel['berat_tinggi'])[0] != null){$cap_pel_berat[$i] = 1;}else{$cap_pel_berat[$i] = 0;}
                if(explode('_',$pel['berat_tinggi'])[1] != null){$cap_pel_tinggi[$i] = 1;}else{$cap_pel_tinggi[$i] = 0;}
            }else{$cap_pel_berat[$i] = 0; $cap_pel_tinggi[$i]=0;}
            if($pel['lingkar_perut'] != null){$cap_pel_perut[$i] =1;}else{$cap_pel_perut[$i] =0;}
            if($pel['tekanan_darah'] != null){$cap_pel_tekanan_darah[$i] =1;}else{$cap_pel_tekanan_darah[$i] =0;}
            if($pel['tes_cepat_gula_darah'] == "y"){$cap_pel_tes_gula_darah[$i] =1;}else{$cap_pel_tes_gula_darah[$i] =0;}
            if($pel['tes_gangguan_emosional'] == "y"){$cap_pel_emo[$i] =1;}else{$cap_pel_emo[$i] =0;}
            if($pel['tes_gangguan_perilaku'] == "y"){$cap_pel_perilaku[$i] =1;}else{$cap_pel_perilaku[$i] =0;}
            if($pel['periksa_indra_penglihatan'] == "y"){$cap_pel_mata[$i] =1;}else{$cap_pel_mata[$i] =0;}
            if($pel['periksa_indra_pendengaran'] == "y"){$cap_pel_telinga[$i] =1;}else{$cap_pel_telinga[$i] =0;}
            if($datawarga['jenis_kelamin'] == "P"){
                if($usiapel['years'] >= 30 && $usiapel['years'] <= 59 ){
                    if($pel['pemeriksaan_payudara_klinis'] == "y"){$cap_pel_payudara_klinis[$i] =1;}else{$cap_pel_payudara_klinis[$i] =0;}
                    if($pel['pemeriksaan_iva'] == "y"){$cap_pel_iva[$i] =1;}else{$cap_pel_iva[$i] =0;}
                }
            }
        }

        foreach($pelayanan_all as $i => $pel){
            $usiapel = UsiaProduktifController::Usia($datawarga['tgl_lahir'],$pel['tanggal_pelayanan']);
            $usia = $usiapel['years'];
            if($cap_berat[$usia] < 1){$cap_berat[$usia] = $cap_berat[$usia]+$cap_pel_berat[$i];}
            if($cap_tinggi[$usia] < 1){$cap_tinggi[$usia] = $cap_tinggi[$usia]+$cap_pel_tinggi[$i];}
            if($cap_perut[$usia] < 1){$cap_perut[$usia] = $cap_perut[$usia]+$cap_pel_perut[$i];}
            if($cap_tek_darah[$usia] < 1){$cap_tek_darah[$usia] = $cap_tek_darah[$usia]+$cap_pel_tekanan_darah[$i];}
            if($cap_gula_darah[$usia] < 1){$cap_gula_darah[$usia] = $cap_gula_darah[$usia]+$cap_pel_tes_gula_darah[$i];}
            if($cap_emosional[$usia] < 1){$cap_emosional[$usia] = $cap_emosional[$usia]+$cap_pel_emo[$i];}
            if($cap_peri[$usia] < 1){$cap_peri[$usia] = $cap_peri[$usia]+$cap_pel_perilaku[$i];}
            if($cap_penglihatan[$usia] < 1){$cap_penglihatan[$usia] = $cap_penglihatan[$usia]+$cap_pel_mata[$i];}
            if($cap_pendengaran[$usia] < 1){$cap_pendengaran[$usia] = $cap_pendengaran[$usia]+$cap_pel_telinga[$i];}
            if($datawarga['jenis_kelamin'] == "P"){
                if($usiapel['years'] >= 30 && $usiapel['years'] <= 59 ){
                    if($cap_klinis[$usia] < 1){$cap_klinis[$usia] = $cap_klinis[$usia]+$cap_pel_payudara_klinis[$i];}
                    if($cap_iva[$usia] < 1){$cap_iva[$usia] = $cap_iva[$usia]+$cap_pel_iva[$i];}
                }
            }
        }

        $capaian =0;
        for($i=15;$i<60;$i++){
            if($datawarga['jenis_kelamin'] == "P"){
                if($i >= 30 && $i <= 59 ){
                    $cap_pel_usia[$i] =(((($cap_berat[$i]+$cap_tinggi[$i]+$cap_perut[$i])*100)/3)+($cap_tek_darah[$i]*100)+($cap_gula_darah[$i]*100)+
                    ((($cap_emosional[$i]+$cap_peri[$i])*100)/2)+($cap_penglihatan[$i]*100)+($cap_pendengaran[$i]*100)+((($cap_klinis[$i]+$cap_iva[$i])*100)/2)/7);
                }
                else{
                    $cap_pel_usia[$i] =(((($cap_berat[$i]+$cap_tinggi[$i]+$cap_perut[$i])*100)/3)+($cap_tek_darah[$i]*100)+($cap_gula_darah[$i]*100)+
                    ((($cap_emosional[$i]+$cap_peri[$i])*100)/2)+($cap_penglihatan[$i]*100)+($cap_pendengaran[$i]*100))/6;
                }
            }else{
                $cap_pel_usia[$i] =(((($cap_berat[$i]+$cap_tinggi[$i]+$cap_perut[$i])*100)/3)+($cap_tek_darah[$i]*100)+($cap_gula_darah[$i]*100)+
                ((($cap_emosional[$i]+$cap_peri[$i])*100)/2)+($cap_penglihatan[$i]*100)+($cap_pendengaran[$i]*100))/6;
            }

            $capaian = $capaian + $cap_pel_usia[$i];
        }

        $selisih = (60-15)+1;
        $capaian_fix = $capaian/$selisih;

        return view('spm.usiaprod.pelayanan.ubah',compact('pelayanan','usiaprod','datawarga','capaian_fix'));
    }

    public function inputnikPelayanan(){
        if(!empty(Session::get('withnik5'))){
            Session::forget('withnik5');
            return view('spm.usiaprod.pelayanan.masukan_nik');  
        }
        else{
            return view('spm.usiaprod.pelayanan.masukan_nik');  
        }
    }

    public function inputPelayanan(Request $request){
        date_default_timezone_set('Asia/Jakarta');
        Session::put('withnik5',$request->nik);
        $nik = Session::get('withnik5');
        $datanik = Warga::where('nik', $nik)->first();
        if(count($datanik) > 0 ){
            $usia = UsiaProduktifController::Usia($datanik['tgl_lahir'],date("Y/m/d/h:i:s"));
            if($usia["years"] <= 59 && $usia["years"] >= 15){
                $data = UsiaProduktif::where('nik', $nik)->where('status','tahap pelayanan')->first();
                if(count($data) > 0 ){
                    return view('spm.usiaprod.pelayanan.tambah',compact('data'));
                }
                else{
                    return view('spm.usiaprod.pelayanan.masukan_nik')->with('alert','NIK tidak terdaftar sebagai Usia Produktif');                
                }
            }
            else{
                return view('spm.usiaprod.pelayanan.masukan_nik')->with('alert','NIK bukan beridentitas Usia Produktif');                
            }
        }
        else{
            return view('spm.usiaprod.pelayanan.masukan_nik')->with('alert','NIK tidak terdaftar');
        }
    }

    public function savePelayanan(Request $request){
        $a = date('Y-m-d', strtotime($request->tanggal_pelayanan));

        if($request->tes_gula_darah != null){$tes_gula_darah = "y";}else{$tes_gula_darah = "t";}
        if($request->tes_emo != null){$tes_gangguan_emosional = "y";}else{$tes_gangguan_emosional = "t";}
        if($request->tes_perilaku != null){$tes_gangguan_perilaku = "y";}else{$tes_gangguan_perilaku = "t";}
        if($request->mata != null){$periksa_indra_penglihatan = "y";}else{$periksa_indra_penglihatan = "t";}
        if($request->telinga != null){$periksa_indra_pendengaran = "y";}else{$periksa_indra_pendengaran = "t";}
        if($request->payudara_klinis != null){$pemeriksaan_payudara_klinis = "y";}else{$pemeriksaan_payudara_klinis = "t";}
        if($request->iva != null){$pemeriksaan_iva = "y";}else{$pemeriksaan_iva = "t";}

        $data = Pelayanan::create([
            'tanggal_pelayanan' => $a,
            'id_spm' => $request->id,
            'table_spm' => 'usia_prod',
            'tenaga_kerja' => $request->tenaga_kesehatan."_".$request->nama_stk,
            'lokasi' => $request->fasilitas_kesehatan."_".$request->lokasi_pelayanan,
            'berat_tinggi' => $request->berat."_".$request->tinggi,
            'lingkar_perut' => $request->perut,
            'tekanan_darah' => $request->tekanan_darah,
            'tes_cepat_gula_darah' => $tes_gula_darah,
            'tes_gangguan_emosional' => $tes_gangguan_emosional,
            'tes_gangguan_perilaku' => $tes_gangguan_perilaku,
            'periksa_indra_penglihatan' => $periksa_indra_penglihatan,
            'periksa_indra_pendengaran' => $tes_gangguan_perilaku,
            'pemeriksaan_payudara_klinis' => $pemeriksaan_payudara_klinis,
            'pemeriksaan_iva' => $pemeriksaan_iva,
            'created_by' => $request->created_by
        ]);
        if($data->save()){
            return Redirect::route('detail-usia-prod',[$request->id])->with('success','Berhasil Tambah Pelayanan Usia Produktif');        
        }  
        else{
            return Redirect::route('catat-usia-prod')->with('danger','Gagal Tambah Pelayanan Usia Produktif');
        }
    }

    public function editPelayanan(Request $request){
        $a = date('Y-m-d', strtotime($request->tanggal_pelayanan));

        if($request->tes_gula_darah != null){$tes_gula_darah = "y";}else{$tes_gula_darah = "t";}
        if($request->tes_emo != null){$tes_gangguan_emosional = "y";}else{$tes_gangguan_emosional = "t";}
        if($request->tes_perilaku != null){$tes_gangguan_perilaku = "y";}else{$tes_gangguan_perilaku = "t";}
        if($request->mata != null){$periksa_indra_penglihatan = "y";}else{$periksa_indra_penglihatan = "t";}
        if($request->telinga != null){$periksa_indra_pendengaran = "y";}else{$periksa_indra_pendengaran = "t";}
        if($request->payudara_klinis != null){$pemeriksaan_payudara_klinis = "y";}else{$pemeriksaan_payudara_klinis = "t";}
        if($request->iva != null){$pemeriksaan_iva = "y";}else{$pemeriksaan_iva = "t";}

        $data = Pelayanan::where('id',$request->id)->first();
        $data['tanggal_pelayanan'] = $a;
        $data['tenaga_kerja'] = $request->tenaga_kesehatan."_".$request->nama_stk;
        $data['lokasi'] = $request->fasilitas_kesehatan."_".$request->lokasi_pelayanan;
        $data['berat_tinggi'] = $request->berat."_".$request->tinggi;
        $data['lingkar_perut'] = $request->perut;
        $data['tekanan_darah'] = $request->tekanan_darah;
        $data['tes_cepat_gula_darah'] = $tes_gula_darah;
        $data['tes_gangguan_emosional'] = $tes_gangguan_emosional;
        $data['tes_gangguan_perilaku'] = $tes_gangguan_perilaku;
        $data['periksa_indra_penglihatan'] = $periksa_indra_penglihatan;
        $data['periksa_indra_pendengaran'] = $tes_gangguan_perilaku;
        $data['pemeriksaan_payudara_klinis'] = $pemeriksaan_payudara_klinis;
        $data['pemeriksaan_iva'] = $pemeriksaan_iva;

        if($data->save()){
            return redirect('/pelayanan-usia-prod')->with('alert', 'Berhasil Ubah Pelayanan Usia Produktif');
        }  
        else{
            return redirect('/catat-usia-prod')->with('alert', 'Gagal Ubah Pelayanan Usia Produktif');
        }
    }

    public function Usia($tgl1, $tgl2){
        $tgl1 = (is_string($tgl1) ? strtotime($tgl1) : $tgl1);
        $tgl2 = (is_string($tgl2) ? strtotime($tgl2) : $tgl2);
        $diff_secs = abs($tgl1-$tgl2);
        $base_year = min(date("Y",$tgl1), date("Y",$tgl2));
        $diff = mktime(0,0,$diff_secs,1,1,$base_year);
        return array(
            "years"=>date("Y",$diff) - $base_year, 
            "months_total" => (date("Y",$diff) - $base_year) * 12 + date("n",$diff) - 1, 
            "months" => date("n",$diff) -1, 
            "days_total" => floor($diff_secs/ (3600 * 24)), 
            "days" => date("j",$diff) - 1, 
            "hours_total" => floor($diff_secs/3600), 
            "hours" => date("G", $diff), 
            "minutes_total" => floor($diff_secs/60), 
            "minutes" => (int)date("i",$diff),
            "seconds_total" => $diff_secs, 
            "seconds" => (int) date("s",$diff)
        );
    }


}
