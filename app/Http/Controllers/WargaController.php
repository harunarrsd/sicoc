<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Requests;
use DB;
use Illuminate\Support\Facades\Session;
use Redirect;

use App\Warga;
use App\IbuHamil;
use App\IbuBersalin;
use App\BayiBaruLahir;
use App\Balita;
use App\UsiaPendDasar;
use App\UsiaProduktif;
use App\UsiaLanjut;
use App\Hipertensi;
use App\DM;
use App\ODGJ;
use App\TB;
use App\HIV;
use App\Kecamatan;
use App\Kelurahan;

class WargaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){        
        date_default_timezone_set('Asia/Jakarta');
        $warga_all = Warga::all();
        
        $warga_total_nik = 0;$warga_spm = 0;$warga_belum_spm = 0;
        $ibuhamil_total_nik = IbuHamil::distinct('nik')->count('nik'); 
        $ibubersalin_total_nik = IbuBersalin::distinct('nik')->count('nik'); 
        $bayibarulahir_total_nik = BayiBaruLahir::distinct('nik_ibu')->count('nik_ibu'); 
        $balita_total_nik = Balita::distinct('nik')->count('nik'); 
        $usiapenddasar_total_nik = UsiaPendDasar::distinct('nik')->count('nik'); 
        $usiaprod_total_nik = UsiaProduktif::distinct('nik')->count('nik'); 
        $usialanjut_total_nik = UsiaLanjut::distinct('nik')->count('nik'); 
        $hipertensi_total_nik = Hipertensi::distinct('nik')->count('nik'); 
        $dm_total_nik = DM::distinct('nik')->count('nik'); 
        $odgj_total_nik = ODGJ::distinct('nik')->count('nik'); 
        $tb_total_nik = TB::distinct('nik')->count('nik'); 
        $hiv_total_nik = HIV::distinct('nik')->count('nik'); 
        $ibuhamil_total = count(IbuHamil::all()); $ibubersalin_total = count(IbuBersalin::all()); 
        $bayibarulahir_total = count(BayiBaruLahir::all()); $balita_total = count(Balita::all()); 
        $usiapenddasar_total = count(UsiaPendDasar::all()); $usiaprod_total = count(UsiaProduktif::all()); 
        $usialanjut_total = count(UsiaLanjut::all()); $hipertensi_total = count(Hipertensi::all()); 
        $dm_total = count(DM::all()); $odgj_total = count(ODGJ::all()); $tb_total = count(TB::all()); 
        $hiv_total = count(HIV::all()); 
        foreach($warga_all as $i => $warga){
            $layanan_spm[$i] = "";
            $warga_total_nik++;
            $usia[$i] = WargaController::Usia($warga['tgl_lahir'],date("Y/m/d/h:i:s"));
            $nik = $warga['nik'];
            if(
                count(IbuHamil::where('nik',$nik)->get()) > 0 || 
                count(IbuBersalin::where('nik',$nik)->get()) > 0 || 
                count(BayiBaruLahir::where('nik_ibu',$nik)->get()) > 0 || 
                count(Balita::where('nik',$nik)->get()) > 0 || 
                count(UsiaPendDasar::where('nik',$nik)->get()) > 0 || 
                count(UsiaProduktif::where('nik',$nik)->get()) > 0 || 
                count(UsiaLanjut::where('nik',$nik)->get()) > 0 ||
                count(Hipertensi::where('nik',$nik)->get()) > 0 ||
                count(DM::where('nik',$nik)->get()) > 0 ||
                count(ODGJ::where('nik',$nik)->get()) > 0 ||
                count(TB::where('nik',$nik)->get()) > 0 ||
                count(HIV::where('nik',$nik)->get()) > 0
            ){
                if(count(IbuHamil::where('nik',$nik)->get()) > 0){
                    $layanan_spm[$i] = $layanan_spm[$i]."_Ibu Hamil"; 
                }
                if(count(IbuBersalin::where('nik',$nik)->get()) > 0){
                    $layanan_spm[$i] = $layanan_spm[$i]."_Ibu Bersalin";
                }
                if(count(BayiBaruLahir::where('nik_ibu',$nik)->get()) > 0){
                    $layanan_spm[$i] = $layanan_spm[$i]."_Bayi Baru Lahir";
                }
                if(count(Balita::where('nik',$nik)->get()) > 0){
                    $layanan_spm[$i] = $layanan_spm[$i]."_Balita";
                }
                if(count(UsiaPendDasar::where('nik',$nik)->get()) > 0){
                    $layanan_spm[$i] = $layanan_spm[$i]."_Usia Pendidikan Dasar";
                }
                if(count(UsiaProduktif::where('nik',$nik)->get()) > 0){
                    $layanan_spm[$i] = $layanan_spm[$i]."_Usia Produktif";
                }
                if(count(UsiaLanjut::where('nik',$nik)->get()) > 0){
                    $layanan_spm[$i] = $layanan_spm[$i]."_Usia Lanjut";
                }
                if(count(Hipertensi::where('nik',$nik)->get()) > 0){
                    $layanan_spm[$i] = $layanan_spm[$i]."_Hipertensi";
                }
                if(count(DM::where('nik',$nik)->get()) > 0){
                    $layanan_spm[$i] = $layanan_spm[$i]."_Diabetes Melitus";
                }
                if(count(ODGJ::where('nik',$nik)->get()) > 0){
                    $layanan_spm[$i] = $layanan_spm[$i]."_ODGJ";
                }
                if(count(TB::where('nik',$nik)->get()) > 0){
                    $layanan_spm[$i] = $layanan_spm[$i]."_Tuberkulosis";
                }
                if(count(HIV::where('nik',$nik)->get()) > 0){
                    $layanan_spm[$i] = $layanan_spm[$i]."_Risiko Terinfeksi HIV";
                }
                $warga_spm++;
            }else{
                $warga_belum_spm++;
            }
        }
        return view('warga.data',compact('tb_total','hiv_total','hipertensi_total','dm_total','odgj_total','layanan_spm','usia','warga_all','warga_spm','warga_belum_spm','warga_total_nik','ibuhamil_total','ibubersalin_total',
        'bayibarulahir_total','balita_total','usiapenddasar_total','usiaprod_total','usialanjut_total','ibuhamil_total_nik','ibubersalin_total_nik',
        'bayibarulahir_total_nik','balita_total_nik','usiapenddasar_total_nik','usiaprod_total_nik','usialanjut_total_nik','hipertensi_total_nik','dm_total_nik','odgj_total_nik','tb_total_nik','hiv_total_nik'));
    }

    public function getDataWarga(Request $request){
        $data = Warga::where('id',$request->id)->first();
        return response()->json([
            'message' =>'success',
            'data' => $data,
        ]);
    }

    public function tambah(){
        $list_nik = [];
        $all_warga = Warga::all();
        foreach($all_warga as $i =>$warga){
            $list_nik[$i] = $warga['nik']; 
        }
        return view('warga.tambah',compact('list_nik'));
    }

    public function save(Request $request){
        // print $request->kecamatan;
        $request->tgl_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
        $request->tgl_lahir = $request->tgl_lahir." ".$request->waktu_lahir.":00";
        $data = New Warga();
        $data['nik'] = $request->nik;
        $data['no_kk'] = $request->kk;
        $data['nama'] = $request->nama;
        $data['tempat_lahir'] = $request->tempat_lahir;
        $data['tgl_lahir'] = $request->tgl_lahir;
        $data['jenis_kelamin'] = $request->jenis_kelamin;
        $data['gol_darah'] = $request->gol_darah;
        $data['alamat'] = $request->alamat;
        $data['kelurahan'] = $request->kelurahan;
        $data['kode_pos'] = $request->kode_pos;
        $data['kecamatan'] = $request->kecamatan;
        $data['kota'] = "Depok";
        $data['provinsi'] = "Jawa Barat";
        $data['agama'] = $request->agama;
        $data['no_telp'] = $request->no_telp;
        $data['status_perkawinan'] = $request->status_perkawinan;
        $data['pekerjaan'] = $request->pekerjaan;
        $data['kewarganegaraan'] = $request->kewarganegaraan;
        $data['id_created_by'] = $request->id_created_by;
        $data['created_by'] = $request->created_by;
        if($data->save()){
            return Redirect::route('data-warga')->with('success','Berhasil Tambah Masyarakat');
        }
        else{
            return Redirect::route('tambah-warga')->with('danger','Gagal Tambah Masyarakat');
        }
    }

    public function detail($id){
        $warga = Warga::where('id',$id)->first();
        return view('warga.detail',compact('warga'));
    }

    public function ubah($id){
        $warga = Warga::where('id',$id)->first();
        $list_nik = [];
        $all_warga = Warga::all();
        foreach($all_warga as $i =>$wargaku){
            $list_nik[$i] = $wargaku['nik']; 
        }
        return view('warga.ubah',compact('warga','list_nik'));
    }

    public function edit(Request $request){
        // print $request->kecamatan;
        $request->tgl_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
        $request->tgl_lahir = $request->tgl_lahir." ".$request->waktu_lahir.":00";
        $data = Warga::where('id',$request->id)->first();
        $data->no_kk = $request->kk;
        $data->nama = $request->nama;
        $data->tempat_lahir = $request->tempat_lahir;
        $data->tgl_lahir = $request->tgl_lahir;
        $data->jenis_kelamin = $request->jenis_kelamin;
        $data->gol_darah = $request->gol_darah;
        $data->alamat = $request->alamat;
        $data->kelurahan = $request->kelurahan;
        $data->kode_pos = $request->kode_pos;
        $data->kecamatan = $request->kecamatan;
        $data->kota = "Depok";
        $data->provinsi = "Jawa Barat";
        $data->agama = $request->agama;
        $data->no_telp = $request->no_telp;
        $data->status_perkawinan = $request->status_perkawinan;
        $data->pekerjaan = $request->pekerjaan;
        $data->kewarganegaraan = $request->kewarganegaraan;
        $data->id_updated_by = $request->id_updated_by;
        $data->updated_by = $request->updated_by;
        if($data->save()){
            return Redirect::route('data-warga')->with('success','Berhasil Ubah Masyarakat');
        }
        else{
            return Redirect::route('ubah-warga')->with('danger','Gagal Ubah Masyarakat');
        }
    }

    public function delete($id){
        $delete=Warga::where('id',$id)->delete();
        
        if($delete){
            return Redirect::route('data-warga')->with('success','Berhasil Hapus Masyarakat');
        }  
        else{
            return Redirect::route('data-warga')->with('danger','Gagal Hapus Masyarakat');
        }
    } 

    public function getKelurahan(Request $request){
        $kel = Kelurahan::where('kecamatan',$request->kec)->get();
        return response()->json(['message' =>'success','data' => $kel]);
    }

    public function Usia($tgl1, $tgl2){
        $tgl1 = (is_string($tgl1) ? strtotime($tgl1) : $tgl1);
        $tgl2 = (is_string($tgl2) ? strtotime($tgl2) : $tgl2);
        $diff_secs = abs($tgl1-$tgl2);
        $base_year = min(date("Y",$tgl1), date("Y",$tgl2));
        $diff = mktime(0,0,$diff_secs,1,1,$base_year);
        return array(
            "years"=>date("Y",$diff) - $base_year, 
            "months_total" => (date("Y",$diff) - $base_year) * 12 + date("n",$diff) - 1, 
            "months" => date("n",$diff) -1, 
            "days_total" => floor($diff_secs/ (3600 * 24)), 
            "days" => date("j",$diff) - 1, 
            "hours_total" => floor($diff_secs/3600), 
            "hours" => date("G", $diff), 
            "minutes_total" => floor($diff_secs/60), 
            "minutes" => (int)date("i",$diff),
            "seconds_total" => $diff_secs, 
            "seconds" => (int) date("s",$diff)
        );
    }
}
