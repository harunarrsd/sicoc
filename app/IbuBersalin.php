<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IbuBersalin extends Model
{
    protected $table = 'spm_ibu_bersalin';

    protected $fillable = [
        'id',
        'nik',
        'tanggal_hamil',
        'tanggal_bersalin',
        'fasilitas_kesehatan',
        'lokasi_fasilitas',
        'tenaga_kesehatan',
        'nama_tenaga',
        'status_persalinan',
        'hasil_persalinan'];
}