<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IbuHamil extends Model
{
    protected $table = 'spm_ibu_hamil';
    protected $fillable = [
        'id',
        'nik',
        'tanggal_hamil',
        'tanggal_bersalin',
        'kode_pelayanan',
        'status_kehamilan'];
}
