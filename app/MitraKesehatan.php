<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MitraKesehatan extends Model
{
    protected $table = 'mitra_kesehatan';
    protected $fillable = [
        'id', 'nama', 'jenis_mitra', 'tahun_berdiri', 'alamat_kantor_pusat', 'kode_pos',
        'telepon', 'email'
    ];
}
