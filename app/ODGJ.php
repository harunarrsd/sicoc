<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ODGJ extends Model
{
    protected $table = 'spm_odgj';
    protected $fillable = [
        'id', 'nik','status','tanggal_menderita'
    ];
}
