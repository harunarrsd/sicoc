<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelayanan extends Model
{
    protected $table = 'catatan_pelayanan';
    protected $fillable = [
        'tanggal_pelayanan',
        'tenaga_kerja',
        'lokasi',
        'jenis_tw',
        'table_spm',
        'id_spm',
        'berat_tinggi',
        'tekanan_darah',
        'lila',
        'tinggi_puncak_rahim',
        'presentasi_djj',
        'imunisasi_tetanus_tt',
        'tablet_tambah_darah',
        'tes_laboratorium',
        'tatalaksana',
        'temu_wicara',
        'waktu_pelayanan', 'suhu_tubuh', 'neo_pertama', 'neo_kedua', 'neo_fisik_apgar',
        'neo_fisik_geo', 'neo_fisik_kl', 'neo_fisik_mulut', 'neo_fisikpk', 'neo_fisik_tbtk',
        'skr_faskes_lokasi', 'skr_tenkes_namastk', 'skr_shk_hsd', 'kie_nama', 'kie_informasi', 'kie_media',
        'kapsul_vit_a','imunisasi_d_lengkap',
        'kelas','status_gizi','tanda_vital','gigi_mulut','ketajaman_indera',
        'lingkar_perut','tes_cepat_gula_darah','tes_gangguan_emosional','tes_gangguan_perilaku','periksa_indra_penglihatan', 'periksa_indra_pendengaran','pemeriksaan_payudara_klinis','pemeriksaan_iva',
        'kadar_gula_darah','tes_kolesterol_darah','cara_deteksi_kepikunan', 'created_by'

    ];
}