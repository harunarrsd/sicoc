<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TB extends Model
{
    protected $table = 'spm_tuberkulosis';
    protected $fillable = [
        'id', 'nik','status','tanggal_menderita'
    ];
}
