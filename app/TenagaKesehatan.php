<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TenagaKesehatan extends Model
{
    protected $table = 'tenaga_kesehatan';
    protected $fillable = [
        'id', 'id_mitra', 'nik', 'nama', 'tgl_lahir', 'tahun_berkarir',
        'spesialis', 'email'
    ];
}
