<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsiaLanjut extends Model
{
    protected $table = 'spm_usia_lanjut';
    protected $fillable = [
        'id',
        'nik',
        'status'];
}
