<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsiaPendDasar extends Model
{
    protected $table = 'spm_usia_pend_dasar';
    protected $fillable = [
        'id',
        'nik',
        'status_usia_pend_dasar'];
}
