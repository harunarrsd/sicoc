<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsiaProduktif extends Model
{
    protected $table = 'spm_usia_prod';
    protected $fillable = [
        'id',
        'nik',
        'status'];
}
