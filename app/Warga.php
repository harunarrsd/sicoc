<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warga extends Model
{
    protected $table = 'warga';
    protected $fillable = [
        'nik', 'nama', 'tempat_lahir', 'tgl_lahir', 'jenis_kelamin', 'gol_darah',
        'alamat', 'kelurahan', 'kecamatan', 'kota', 'provinsi', 'agama', 'no_telp',
        'status_perkawinan', 'pekerjaan', 'kewarganegaraan', 'kode_pos', 'created_by',
        'updated_by','id_created_by','id_updated_by','no_kk'
    ];
}
