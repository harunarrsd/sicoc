-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 02 Okt 2019 pada 08.57
-- Versi server: 10.2.27-MariaDB
-- Versi PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gumilang_sicoc`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `catatan_pelayanan`
--

CREATE TABLE `catatan_pelayanan` (
  `id` bigint(100) NOT NULL,
  `tanggal_pelayanan` datetime NOT NULL,
  `tenaga_kerja` varchar(100) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `jenis_tw` int(1) DEFAULT NULL,
  `table_spm` varchar(100) NOT NULL,
  `id_spm` int(11) NOT NULL,
  `berat_tinggi` varchar(100) DEFAULT NULL,
  `tekanan_darah` varchar(100) DEFAULT NULL,
  `lila` varchar(100) DEFAULT NULL,
  `tinggi_puncak_rahim` varchar(100) DEFAULT NULL,
  `presentasi_djj` varchar(100) DEFAULT NULL,
  `imunisasi_tetanus_tt` varchar(100) DEFAULT NULL,
  `tablet_tambah_darah` varchar(100) DEFAULT NULL,
  `tes_laboratorium` varchar(100) DEFAULT NULL,
  `tatalaksana` varchar(100) DEFAULT NULL,
  `temu_wicara` varchar(100) DEFAULT NULL,
  `waktu_pelayanan` varchar(5) DEFAULT NULL,
  `suhu_tubuh` varchar(5) DEFAULT NULL,
  `neo_pertama` varchar(20) DEFAULT NULL,
  `neo_kedua` varchar(20) DEFAULT NULL,
  `neo_fisik_apgar` varchar(30) DEFAULT NULL,
  `neo_fisik_geo` varchar(10) DEFAULT NULL,
  `neo_fisik_kl` varchar(10) DEFAULT NULL,
  `neo_fisik_mulut` varchar(10) DEFAULT NULL,
  `neo_fisik_pk` varchar(10) DEFAULT NULL,
  `neo_fisik_tbtk` varchar(10) DEFAULT NULL,
  `skr_faskes_lokasi` varchar(100) DEFAULT NULL,
  `skr_tenkes_namastk` varchar(100) DEFAULT NULL,
  `skr_shk_hsd` varchar(20) DEFAULT NULL,
  `kie_nama` varchar(50) DEFAULT NULL,
  `kie_informasi` varchar(10) DEFAULT NULL,
  `kie_media` varchar(50) DEFAULT NULL,
  `kapsul_vit_a` varchar(5) DEFAULT NULL,
  `imunisasi_d_lengkap` varchar(5) DEFAULT NULL,
  `kelas` int(2) DEFAULT NULL,
  `status_gizi` varchar(9) DEFAULT NULL,
  `tanda_vital` varchar(7) DEFAULT NULL,
  `gigi_mulut` varchar(5) DEFAULT NULL,
  `ketajaman_indera` varchar(5) DEFAULT NULL,
  `lingkar_perut` int(10) DEFAULT NULL,
  `tes_cepat_gula_darah` varchar(5) DEFAULT NULL,
  `tes_gangguan_emosional` varchar(5) DEFAULT NULL,
  `tes_gangguan_perilaku` varchar(5) DEFAULT NULL,
  `periksa_indra_penglihatan` varchar(5) DEFAULT NULL,
  `periksa_indra_pendengaran` varchar(5) DEFAULT NULL,
  `pemeriksaan_payudara_klinis` varchar(5) DEFAULT NULL,
  `pemeriksaan_iva` varchar(5) DEFAULT NULL,
  `kadar_gula_darah` int(10) DEFAULT NULL,
  `tes_kolesterol_darah` varchar(5) DEFAULT NULL,
  `cara_deteksi_kepikunan` varchar(200) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `catatan_pelayanan`
--

INSERT INTO `catatan_pelayanan` (`id`, `tanggal_pelayanan`, `tenaga_kerja`, `lokasi`, `jenis_tw`, `table_spm`, `id_spm`, `berat_tinggi`, `tekanan_darah`, `lila`, `tinggi_puncak_rahim`, `presentasi_djj`, `imunisasi_tetanus_tt`, `tablet_tambah_darah`, `tes_laboratorium`, `tatalaksana`, `temu_wicara`, `waktu_pelayanan`, `suhu_tubuh`, `neo_pertama`, `neo_kedua`, `neo_fisik_apgar`, `neo_fisik_geo`, `neo_fisik_kl`, `neo_fisik_mulut`, `neo_fisik_pk`, `neo_fisik_tbtk`, `skr_faskes_lokasi`, `skr_tenkes_namastk`, `skr_shk_hsd`, `kie_nama`, `kie_informasi`, `kie_media`, `kapsul_vit_a`, `imunisasi_d_lengkap`, `kelas`, `status_gizi`, `tanda_vital`, `gigi_mulut`, `ketajaman_indera`, `lingkar_perut`, `tes_cepat_gula_darah`, `tes_gangguan_emosional`, `tes_gangguan_perilaku`, `periksa_indra_penglihatan`, `periksa_indra_pendengaran`, `pemeriksaan_payudara_klinis`, `pemeriksaan_iva`, `kadar_gula_darah`, `tes_kolesterol_darah`, `cara_deteksi_kepikunan`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, '2019-03-04 00:00:00', '_', 'Puskesmas_', 1, 'ibu_hamil', 1, '76_171', '120/80', '24,08', '2 jari atas siymphisis', 'Kepala_101', 'y_y', '1', 'y_y_y_y_y_y', 'y', 'y', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:33:43', '2019-08-28 00:21:25'),
(4, '2019-03-05 00:00:00', 'Bidan_dr. Ricky Firmansyah', 'Puskesmas_Puskesmas Pancoran Mas', 1, 'ibu_hamil', 2, '76_165', '120/80', '24,65', '37,09 cm', 'Bokong_117', 't_y', '9', 't_t_t_t_t_t', 'y', 'y', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:33:52', '2019-07-21 21:05:59'),
(5, '2019-03-06 00:00:00', 'Dokter_dr. Ahmad Subakrie', 'Puskesmas_Puskesmas Depok Jaya', 1, 'ibu_hamil', 2, '76_165', '120/80', '23,09', '35,08 cm', 'Lain-lain_119', 'y_t', '7', 't_t_t_t_t_t', 't', 't', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:33:58', '2019-07-21 21:09:20'),
(6, '2019-03-07 00:00:00', 'Dokter_dr. Ahmad Subakrie', 'Puskesmas_Puskesmas Depok Jaya', 1, 'ibu_hamil', 2, '76_164', '120/80', '0,0', '0,0 cm', '_', 't_t', NULL, 't_t_t_t_t_t', 't', 't', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:34:03', '2019-07-21 21:08:55'),
(7, '2019-03-08 00:00:00', 'Dokter_dr. Ahmad Subakrie', 'Puskesmas_Puskesmas Depok Jaya', 1, 'ibu_hamil', 2, '0_0', '120/80', '0,0', '0,0 cm', 'Tidak diisi_0', 't_t', '7', 't_t_t_t_t_t', 't', 't', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:34:09', '2019-07-21 21:12:34'),
(8, '2019-03-09 00:00:00', 'Bidan_dr. Ricky Firmansyah', 'Puskesmas_Puskesmas Pancoran Mas', 1, 'ibu_hamil', 2, '77_160', '0/0', ',', ',', 'Tidak diisi_0', 'y_t', NULL, 't_t_t_t_t_t', 't', 't', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:34:14', '2019-07-21 21:07:31'),
(9, '2019-03-10 00:00:00', 'Bidan_dr. Ricky Firmansyah', 'Puskesmas_Puskesmas Pancoran Mas', 1, 'ibu_hamil', 2, '77_160', '0/0', ',', ',', '_', 'y_t', NULL, 't_t_t_t_t_t', 't', 't', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:34:20', '2019-07-21 21:09:46'),
(11, '2019-03-12 00:00:00', 'Bidan_dr. Ricky Firmansyah', 'Puskesmas_Puskesmas Pancoran Mas', 1, 'ibu_hamil', 2, '77_160', '0/0', ',', ',', '_', 'y_t', NULL, 't_t_t_t_t_t', 't', 't', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:34:29', '2019-07-21 21:10:38'),
(12, '2019-03-13 00:00:00', 'Dokter_dr. Ahmad Subakrie', 'Puskesmas_Puskesmas Depok Jaya', 1, 'ibu_hamil', 2, '77_160', '0/0', ',', ',', '_', 'y_t', NULL, 't_t_t_t_t_t', 't', 't', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:34:35', '2019-07-21 21:11:12'),
(18, '2019-03-19 00:00:00', '_', 'Rumah Sakit_', 1, 'ibu_hamil', 1, '76_165', '100/80', '24,00', '3 jari bawah pusar', 'Kepala_101', 'y_y', '0', 'y_y_y_y_y_y', 'y', 'y', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:34:40', '2019-08-28 00:10:00'),
(19, '2019-03-20 00:00:00', 'Dokter_dr. Ahmad Riza Syauqi', 'Rumah Sakit_Rumah Sakit Hermina Depok', 1, 'ibu_hamil', 1, '0_0', '0/0', ',', ',', '_', 'y_t', NULL, 't_t_t_t_t_t', 't', 't', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:34:46', '2019-07-21 20:57:55'),
(20, '2019-03-21 00:00:00', 'Bidan_dr. Ricky Firmansyah', 'Puskesmas_Puskesmas Pancoran Mas', 1, 'ibu_hamil', 1, '0_0', '0/0', ',', ',', '_', 'y_t', NULL, 't_t_t_t_t_t', 't', 't', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:34:54', '2019-07-21 20:58:33'),
(21, '2019-03-22 00:00:00', 'Dokter Spesialis Kebidanan_dr. Mahmud Rizki Abdullah', 'Rumah Sakit_Rumah Sakit Mitra Keluarga', 1, 'ibu_hamil', 1, '68_166', '120/80', '24,00', '36,00 cm', 'Bokong_120', 't_y', '10', 'y_y_y_y_t_t', 'y', 'y', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:35:04', '2019-07-21 20:59:16'),
(22, '2019-03-23 00:00:00', 'Dokter_dr. Ahmad Subakrie', 'Puskesmas_Puskesmas Depok Jaya', 1, 'ibu_hamil', 2, '75_166', '120/80', '25,01', '34,09 cm', 'Kepala_119', 't_y', '10', 'y_t_y_t_t_t', 't', 't', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:35:28', '2019-07-21 21:13:34'),
(23, '2019-03-24 00:00:00', 'Dokter_dr. Ahmad Subakrie', 'Puskesmas_Puskesmas Depok Jaya', 1, 'ibu_hamil', 2, '75_166', '120/80', '25,01', '34,09 cm', 'Lain-lain_119', 't_y', '10', 'y_t_y_t_t_t', 't', 't', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:35:32', '2019-07-21 21:13:51'),
(24, '2019-03-25 00:00:00', 'Dokter_dr. Ahmad Riza Syauqi', 'Rumah Sakit_Rumah Sakit Hermina Depok', 1, 'ibu_hamil', 2, '76_166', '120/80', '24,00', '35,00 cm', 'Lain-lain_119', 't_y', '12', 'y_y_y_y_t_t', 'y', 'y', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:35:36', '2019-07-21 21:14:11'),
(26, '2019-04-22 00:00:00', 'Dokter_dr. Ahmad Riza Syauqi', 'Rumah Sakit_Rumah Sakit Hermina Depok', 2, 'ibu_hamil', 1, '76_166', '119/78', '24,01', ',', 'Kepala_0', 'y_t', NULL, 't_t_t_t_t_t', 't', 't', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:35:40', '2019-07-21 20:58:54'),
(27, '2019-04-22 00:00:00', 'Dokter Spesialis Kebidanan_dr. Mahmud Rizki Abdullah', 'Rumah Sakit_Rumah Sakit Mitra Keluarga', 1, 'ibu_hamil', 2, '76_165', '119/78', '24,00', '36,01 cm', 'Kepala_119', 't_y', '3', 'y_y_y_y_t_t', 'y', 'y', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:35:49', '2019-07-21 21:15:25'),
(28, '2019-06-21 00:00:00', 'Dokter_dr. Ahmad Riza Syauqi', 'Rumah Sakit_Rumah Sakit Hermina Depok', 2, 'ibu_hamil', 1, '76_165', '120/80', '24,09', '36,00 cm', 'Bokong_119', 'y_y', '1', 'y_y_y_y_t_t', 'y', 'y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:54:15', '2019-07-21 20:59:37'),
(31, '2019-07-11 09:34:00', 'Dokter/DLP_dr. Ahmad Riza', 'Rumah Sakit_RSIA Tumbuh Kembang', NULL, 'balita', 1, '9_14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'y', 'y', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:54:30', '2019-06-11 08:58:45'),
(33, '2019-07-12 08:09:00', 'Dokter/DLP_dr. Ahmad Riza', 'Rumah Sakit_RSIA Tumbuh Kembang', NULL, 'balita', 1, '9_14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:54:40', '0000-00-00 00:00:00'),
(34, '2019-07-13 07:09:00', 'Dokter/DLP_dr. Ahmad Riza', 'Rumah Sakit_RSIA Tumbuh Kembang', NULL, 'balita', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'y', 'y', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:54:51', '0000-00-00 00:00:00'),
(35, '2019-06-21 13:23:00', 'Dokter/DLP_dr. Susi Wijaya', 'Rumah Sakit_RSIA Tumbuh Kembang', NULL, 'balita', 1, '3_45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'y', 'y', 0, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 00:59:53', '2019-06-20 23:24:18'),
(36, '2019-06-24 00:00:00', 'Dokter/DLP_dr. Ahmad Riza', 'Puskesmas_Puskesmas Beji', NULL, 'usia_pend_dasar', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '40_17_y', '76_165', 't_t', 't_t', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 01:00:00', '2019-06-24 21:13:17'),
(37, '2019-06-28 00:00:00', 'Nutrisionis/Tenaga Gizi_dr. Hirsadi', 'Puskesmas_Puskesmas Beji', NULL, 'usia_prod', 1, '45_167', '76', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, 'y', 'y', 't', 'y', 't', 't', 't', NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 01:00:07', '2019-06-30 19:29:14'),
(38, '2019-06-27 00:00:00', 'Petugas Pelaksana Posbindu PTM terlatih_dr. Maula Gilang', 'Posbindu PTM_Posbindu PTM 1 Beji', NULL, 'usia_prod', 1, '44_160', '77', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, 'y', 't', 't', 'y', 't', 't', 't', NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 01:00:15', '2019-06-30 19:29:43'),
(39, '2019-06-26 00:00:00', 'Perawat_dr. Rini Anggraini', 'Klinik_Klinik Utama Sejatera Beji', NULL, 'usia_prod', 1, '45_165', '76', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, 'y', 'y', 't', 't', 't', 't', 't', NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 01:00:22', '2019-06-30 19:30:09'),
(40, '2019-06-25 00:00:00', 'Nutrisionis/Tenaga Gizi_dr. Hirsadi', 'Puskesmas_Puskesmas Beji', NULL, 'usia_prod', 1, '45_167', '75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, 't', 't', 't', 'y', 't', 't', 't', NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 01:00:28', '2019-06-30 19:32:13'),
(41, '2019-06-24 00:00:00', 'Petugas Pelaksana Posbindu PTM terlatih_dr. Maula Gilang', 'Posbindu PTM_Posbindu PTM 1 Beji', NULL, 'usia_prod', 1, '45_166', '76', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 31, 't', 't', 't', 't', 't', 't', 't', NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 01:00:40', '2019-06-30 19:31:04'),
(42, '2019-06-28 00:00:00', 'Kader Posyandu lansia/Posbindu_dr. Ahmad Riza', 'Kelompok Lansia_Kelompok Lansia Beji', NULL, 'usia_lanjut', 1, NULL, '78', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'y', 'y', NULL, NULL, NULL, NULL, 78, 'y', 'MMSE', 'Rohimam', 'Rohimam', '2019-08-30 01:00:55', '2019-07-02 20:59:38'),
(43, '2019-06-27 00:00:00', 'Dokter_dr. Ridwansyah', 'Rumah Sakit_RSIA Tumbuh Kembang', NULL, 'usia_lanjut', 1, NULL, '77', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'y', 'y', NULL, NULL, NULL, NULL, 109, NULL, 'MMSE', 'Rohimam', 'Rohimam', '2019-08-30 01:01:09', '2019-06-27 19:36:55'),
(44, '2019-06-26 00:00:00', 'Dokter_dr. Ridwansyah', 'Rumah Sakit_RSIA Tumbuh Kembang', NULL, 'usia_lanjut', 1, NULL, '75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'y', 'y', NULL, NULL, NULL, NULL, 110, NULL, 'MMSE', 'Rohimam', 'Rohimam', '2019-08-30 01:01:22', '2019-06-27 19:37:40'),
(45, '2019-06-25 00:00:00', 'Dokter_dr. Ridwansyah', 'Rumah Sakit_RSIA Tumbuh Kembang', NULL, 'usia_lanjut', 1, NULL, '77', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'y', 'y', NULL, NULL, NULL, NULL, 111, NULL, 'MMSE', 'Rohimam', 'Rohimam', '2019-08-30 01:01:39', '2019-06-27 19:38:26'),
(46, '2019-06-24 00:00:00', 'Dokter_dr. Ahmad Riza', 'Kelompok Lansia_Kelompok Lansia Beji', NULL, 'usia_lanjut', 1, NULL, '76', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'y', 'y', NULL, NULL, NULL, NULL, 110, 'y', 'MMSE', 'Rohimam', 'Rohimam', '2019-08-30 01:01:52', '2019-06-27 23:50:21'),
(49, '2019-07-21 00:00:00', 'Bidan_dr. Dita Ananda', 'Puskesmas_Puskesmas Pancoran Mas', 3, 'ibu_hamil', 1, '56_167', '120/80', '24,00', '36,00 cm', 'Atas_123', 't_t', NULL, 't_t_t_t_t_t', 't', 't', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 01:02:03', '2019-07-21 03:27:06'),
(53, '2019-07-23 00:00:00', 'Dokter_dr. Ahmad Riza Syauqi', 'Rumah Sakit_Rumah Sakit Hermina Depok', 3, 'ibu_hamil', 1, '56_156', '120/80', '24,00', '36,00 cm', 'Atas_121', 'y_y', '10', 'y_y_y_y_t_t', 'y', 'y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 01:02:17', '2019-07-22 00:11:13'),
(55, '2019-07-22 00:00:00', 'Dokter Spesialis Kebidanan_dr. Steven Stevu Denito', 'Klinik_Klinik Citra Derma', 3, 'ibu_hamil', 1, '_', '/', ',', '', '_', 't_t', NULL, 't_t_t_t_t_t', 't', 't', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 01:02:30', '2019-07-22 01:00:28'),
(56, '2019-07-22 00:00:00', 'Dokter Spesialis Kebidanan_dr. Steven Stevu Denito', 'Klinik_Klinik Citra Derma', 2, 'ibu_hamil', 2, '0_0', '0/0', ',', '', 'Atas_0', 't_t', NULL, 't_t_t_t_t_t', 't', 't', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 01:02:45', '2019-07-22 01:33:58'),
(57, '2019-07-23 00:00:00', 'Dokter Spesialis Kebidanan_dr. Mahmud Rizki Abdullah', 'Rumah Sakit_Rumah Sakit Mitra Keluarga', 3, 'ibu_hamil', 2, '_', '/', ',', '', '_', 't_t', NULL, 't_t_t_t_t_t', 't', 't', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 01:02:58', '2019-07-22 01:39:28'),
(58, '2019-07-20 00:00:00', 'Dokter_dr. Ahmad Riza Syauqi', 'Rumah Sakit_Rumah Sakit Hermina Depok', 3, 'ibu_hamil', 1, '_', '', ',', '', '_', 't_t', NULL, 't_t_t_t_t_t', 't', 't', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 01:03:09', '2019-07-22 02:16:34'),
(59, '2019-08-26 16:44:00', '_', 'Puskesmas_', 3, 'ibu_hamil', 1, '76_165', '120/80', '24,00', '', 'Bokong_120', 'y_y', '10', 'y_y_y_y_t_t', 'y', 'y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', 'Rohimam', '2019-08-30 01:03:22', '2019-08-26 02:45:59'),
(61, '2019-09-02 09:29:00', 'Dokter_dr. Iif Rifai', 'Klinik_Klinik Nirmala', 1, 'ibu_hamil', 15, '56_165', '117/89', '24,01', '4 cm', 'Lain-lain_109', 'y_y', '30', 'y_y_y_y_y_y', 'y', 'y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', NULL, '2019-08-29 19:29:58', '2019-08-29 19:29:58'),
(62, '2019-12-03 10:39:00', 'Dokter_dr. Iif Rifai', 'Klinik_Klinik Nirmala', 2, 'ibu_hamil', 15, '76_165', '110/80', '24,09', '3 jari bawah pusar', 'Bokong_110', 'y_y', '30', 'y_y_y_y_y_y', 'y', 'y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rohimam', NULL, '2019-08-29 19:33:19', '2019-08-29 19:33:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id` int(11) NOT NULL,
  `nama_kecamatan` varchar(100) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kecamatan`
--

INSERT INTO `kecamatan` (`id`, `nama_kecamatan`, `kota`, `provinsi`, `created_at`, `updated_at`) VALUES
(1, 'Beji', 'Depok', 'Jawa Barat', '2019-09-08 02:23:00', '0000-00-00 00:00:00'),
(2, 'Bojongsari', 'Depok', 'Jawa Barat', '2019-09-08 02:25:58', '0000-00-00 00:00:00'),
(3, 'Cilodong', 'Depok', 'Jawa Barat', '2019-09-08 02:25:58', '0000-00-00 00:00:00'),
(4, 'Cimanggis', 'Depok', 'Jawa Barat', '2019-09-08 02:25:58', '0000-00-00 00:00:00'),
(5, 'Cinere', 'Depok', 'Jawa Barat', '2019-09-08 02:25:58', '0000-00-00 00:00:00'),
(6, 'Cipayung', 'Depok', 'Jawa Barat', '2019-09-08 02:25:58', '0000-00-00 00:00:00'),
(7, 'Limo', 'Depok', 'Jawa Barat', '2019-09-08 02:25:58', '0000-00-00 00:00:00'),
(8, 'Pancoran Mas', 'Depok', 'Jawa Barat', '2019-09-08 02:25:58', '0000-00-00 00:00:00'),
(9, 'Sawangan', 'Depok', 'Jawa Barat', '2019-09-08 02:25:58', '0000-00-00 00:00:00'),
(10, 'Sukmajaya', 'Depok', 'Jawa Barat', '2019-09-08 02:25:58', '0000-00-00 00:00:00'),
(11, 'Tapos', 'Depok', 'Jawa Barat', '2019-09-08 02:25:58', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id` int(11) NOT NULL,
  `nama_kelurahan` varchar(100) NOT NULL,
  `kode_pos` int(5) NOT NULL,
  `kecamatan` varchar(100) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelurahan`
--

INSERT INTO `kelurahan` (`id`, `nama_kelurahan`, `kode_pos`, `kecamatan`, `kota`, `provinsi`, `created_at`, `updated_at`) VALUES
(1, 'Beji', 16421, 'Beji', 'Depok', 'Jawa Barat', '2019-09-08 02:31:50', '0000-00-00 00:00:00'),
(4, 'Beji Timur', 16422, 'Beji', 'Depok', 'Jawa Barat', '2019-09-08 03:42:17', '0000-00-00 00:00:00'),
(5, 'Kemiri Muka', 16423, 'Beji', 'Depok', 'Jawa Barat', '2019-09-08 03:42:26', '0000-00-00 00:00:00'),
(6, 'Kukusan', 16425, 'Beji', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(7, 'Pondok Cina', 16424, 'Beji', 'Depok', 'Jawa Barat', '2019-09-08 03:42:42', '0000-00-00 00:00:00'),
(8, 'Tanah Baru', 16425, 'Beji', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(9, 'Bojongsari', 16516, 'Bojongsari', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(10, 'Bojongsari Baru', 16516, 'Bojongsari', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(11, 'Curug', 16517, 'Bojongsari', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(12, 'Duren Mekar', 16518, 'Bojongsari', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(13, 'Duren Seribu', 16518, 'Bojongsari', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(14, 'Pondok Petir', 16517, 'Bojongsari', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(15, 'Serua', 16517, 'Bojongsari', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(16, 'Cilodong', 16414, 'Cilodong', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(17, 'Jatimulya', 16413, 'Cilodong', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(18, 'Kalibaru', 16414, 'Cilodong', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(19, 'Kalimulya', 16413, 'Cilodong', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(20, 'Sukamaju', 16415, 'Cilodong', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(21, 'Cisalak Pasar', 16452, 'Cimanggis', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(22, 'Curug', 16453, 'Cimanggis', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(23, 'Harjamukti', 16454, 'Cimanggis', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(24, 'Mekarsari', 16452, 'Cimanggis', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(25, 'Pasir Gunung Selatan', 16451, 'Cimanggis', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(26, 'Tugu', 16451, 'Cimanggis', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(27, 'Cinere', 16514, 'Cinere', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(28, 'Gandul', 16512, 'Cinere', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(29, 'Pangkalan Jati Baru', 16513, 'Cinere', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(30, 'Pangkalan Jati Lama', 16513, 'Cinere', 'Depok', 'Jawa Barat', '2019-09-08 02:43:07', '0000-00-00 00:00:00'),
(31, 'Bojong Pondok Terong', 16436, 'Cipayung', 'Depok', 'Jawa Barat', '2019-09-08 02:44:28', '0000-00-00 00:00:00'),
(32, 'Cipayung', 16437, 'Cipayung', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(33, 'Cipayung Jaya', 16437, 'Cipayung', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(34, 'Pondok Jaya', 16438, 'Cipayung', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(35, 'Ratu Jaya', 16439, 'Cipayung', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(36, 'Grogol', 16512, 'Limo', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(37, 'Limo', 16515, 'Limo', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(38, 'Meruyung', 16515, 'Limo', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(39, 'Krukut', 16512, 'Limo', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(40, 'Cipayung', 16437, 'Pancoran Mas', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(41, 'Depok', 16431, 'Pancoran Mas', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(42, 'Depok Jaya', 16432, 'Pancoran Mas', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(43, 'Mampang', 16433, 'Pancoran Mas', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(44, 'Pancoran Mas', 16436, 'Pancoran Mas', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(45, 'Rangkapan Jaya', 16435, 'Pancoran Mas', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(46, 'Rangkapan Jaya Baru', 16434, 'Pancoran Mas', 'Depok', 'Jawa Barat', '2019-09-08 02:55:29', '0000-00-00 00:00:00'),
(47, 'Bedahan', 16519, 'Sawangan', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(48, 'Cinangka', 16516, 'Sawangan', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(49, 'Kedaung', 16516, 'Sawangan', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(50, 'Sawangan', 16511, 'Sawangan', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(51, 'Sawangan Baru', 16511, 'Sawangan', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(52, 'Pasir Putih', 16519, 'Sawangan', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(53, 'Pengasinan', 16518, 'Sawangan', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(54, 'Abadijaya', 16417, 'Sukmajaya', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(55, 'Baktijaya', 16418, 'Sukmajaya', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(56, 'Cisalak', 16416, 'Sukmajaya', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(57, 'Mekarjaya', 16411, 'Sukmajaya', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(58, 'Sukmajaya', 16412, 'Sukmajaya', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(59, 'Tirtajaya', 16412, 'Sukmajaya', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(60, 'Cilangkap', 16458, 'Tapos', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(61, 'Cimpaeun', 16459, 'Tapos', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(62, 'Jatijajar', 16451, 'Tapos', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(63, 'Leuwinanggung', 16456, 'Tapos', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(64, 'Sukamaju Baru', 16455, 'Tapos', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(65, 'Sukatani', 16454, 'Tapos', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00'),
(66, 'Tapos', 16457, 'Tapos', 'Depok', 'Jawa Barat', '2019-09-08 02:54:54', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mitra_kesehatan`
--

CREATE TABLE `mitra_kesehatan` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jenis_mitra` varchar(50) NOT NULL,
  `tahun_berdiri` int(4) NOT NULL,
  `alamat_kantor_pusat` varchar(200) NOT NULL,
  `kode_pos` int(5) NOT NULL,
  `telepon` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mitra_kesehatan`
--

INSERT INTO `mitra_kesehatan` (`id`, `nama`, `jenis_mitra`, `tahun_berdiri`, `alamat_kantor_pusat`, `kode_pos`, `telepon`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Hermina Depok', 'Rumah Sakit', 1997, 'Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok, Jawa Barat', 16436, '0217270036', 'hermina-depok@gmail.com', '2019-07-21 03:42:20', '0000-00-00 00:00:00'),
(2, 'Permata Depok', 'Rumah Sakit', 1997, 'Jl. Raya Muchtar', 16426, '0217270036', 'permata-depok@gmail.com', '2019-07-21 03:42:20', '0000-00-00 00:00:00'),
(3, 'Mitra Keluarga', 'Rumah Sakit', 1997, 'Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok, Jawa Barat', 16436, '0217270036', 'hmitra-keluargadepok@gmail.com', '2019-07-21 03:46:19', '0000-00-00 00:00:00'),
(4, 'Depok Jaya', 'Puskesmas', 1997, 'Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok, Jawa Barat', 16436, '0217270036', 'puskesdepokjaya@gmail.com', '2019-07-21 03:46:19', '0000-00-00 00:00:00'),
(5, 'Pancoran Mas', 'Puskesmas', 1997, 'Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok, Jawa Barat', 16436, '0217270036', 'puskespancoranmas@gmail.com', '2019-07-21 03:46:19', '0000-00-00 00:00:00'),
(6, 'Kec. Beji', 'Puskesmas', 1997, 'Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok, Jawa Barat', 16436, '0217270036', 'puskeskecbeji@gmail.com', '2019-07-21 03:46:19', '0000-00-00 00:00:00'),
(7, 'Nirmala', 'Klinik', 1997, 'Jl. Margonda Raya ', 16426, '0217270036', 'nirmalaklinik@gmail.com', '2019-07-21 03:49:50', '0000-00-00 00:00:00'),
(8, 'Citra Derma', 'Klinik', 1997, 'Jl. Margonda Raya ', 16426, '0217270036', 'nirmalaklinik@gmail.com', '2019-07-21 03:50:19', '0000-00-00 00:00:00'),
(9, 'Depok 2000', 'Klinik', 1997, 'Jl. Margonda Raya ', 16426, '0217270036', 'nirmalaklinik@gmail.com', '2019-07-21 03:50:19', '0000-00-00 00:00:00'),
(10, 'Beji', 'Polindes', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:56', '0000-00-00 00:00:00'),
(11, 'Pancoran Mas', 'Polindes', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(12, 'Sawangan', 'Polindes', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(13, 'Pasir Putih', 'Poskesdes', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(14, 'Kedaung', 'Poskesdes', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(15, 'Bojongsari', 'Poskesdes', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(16, 'Sawangan', 'Klinik Pratama', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(17, 'Pasir Putih', 'Klinik Pratama', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(18, 'Kedaung', 'Klinik Pratama', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(19, 'Pengasinan', 'Klinik Utama', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(20, 'Bojongsari', 'Klinik Utama', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(21, 'Duren Seribu', 'Klinik Utama', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(22, 'Rangkapan Jaya', 'Klinik Bersalin', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(23, 'Cipayung', 'Klinik Bersalin', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(24, 'Ratu Jaya', 'Klinik Bersalin', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(25, 'Sukmajaya', 'Balai Kesehatan Ibu dan Anak', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(26, 'Abadi Jaya', 'Balai Kesehatan Ibu dan Anak', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(27, 'Bakti Jaya', 'Balai Kesehatan Ibu dan Anak', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(28, 'Pondok Sukmajaya', 'Bidan Praktek Swasta', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(29, 'Cilodong', 'Bidan Praktek Swasta', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(30, 'Villa Pertiwi', 'Bidan Praktek Swasta', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(31, 'Kalimulya', 'Puskesmas', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(32, 'Cimanggis', 'Puskesmas', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(33, 'Tugu', 'Puskesmas', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(34, 'Harjamukti', 'Puskesmas', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(35, 'Pasir Gunung Selatan', 'Puskesmas', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(36, 'Mekarsari', 'Puskesmas', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(37, 'Cisalak Pasar', 'Puskesmas', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(38, 'Tapos', 'Puskesmas', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(39, 'Sukatani', 'Puskesmas', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(40, 'Jatijajar', 'Puskesmas', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(41, 'Cilangkap', 'Puskesmas', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(42, 'Cimpaeun', 'Puskesmas', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(43, 'Sukamaju Baru', 'Puskesmas', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(44, 'Kemiri Muka', 'Puskesmas', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(45, 'Limo', 'Puskesmas', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(46, 'Cinere', 'Puskesmas', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(47, 'RSUD Depok', 'Rumah Sakit Pemerintahan', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(48, 'Bhakti Yudha', 'Rumah Sakit Swasta', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(49, 'Hasanah Graha', 'Rumah Sakit Swasta', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00'),
(50, 'Terapis Pa Toumas', 'Rumah Sakit Swasta', 1997, 'Jl. Siliwangi Jl. Siliwangi No.50, Depok, Kec. Pancoran Mas, Kota Depok', 16436, '0217270036', 'polindes@gmail.com', '2019-07-22 12:00:21', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `spesialis`
--

CREATE TABLE `spesialis` (
  `id` int(11) NOT NULL,
  `id_tenaga` int(11) NOT NULL,
  `nama_spesialis` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spesialis`
--

INSERT INTO `spesialis` (`id`, `id_tenaga`, `nama_spesialis`, `created_at`, `updated_at`) VALUES
(1, 1, 'Dokter', '2019-07-21 06:21:31', '0000-00-00 00:00:00'),
(2, 2, 'Bidan', '2019-07-21 06:21:40', '0000-00-00 00:00:00'),
(3, 3, 'Dokter Spesialis Kebidanan', '2019-07-21 06:21:46', '0000-00-00 00:00:00'),
(4, 4, 'Dokter', '2019-07-21 06:22:01', '0000-00-00 00:00:00'),
(5, 12, 'Dokter', '2019-07-21 06:25:47', '0000-00-00 00:00:00'),
(6, 7, 'Dokter', '2019-07-21 06:25:14', '0000-00-00 00:00:00'),
(7, 10, 'Dokter', '2019-07-21 06:25:14', '0000-00-00 00:00:00'),
(8, 13, 'Dokter', '2019-07-21 06:25:14', '0000-00-00 00:00:00'),
(9, 16, 'Dokter', '2019-07-21 06:25:14', '0000-00-00 00:00:00'),
(10, 5, 'Bidan', '2019-07-21 06:25:14', '0000-00-00 00:00:00'),
(11, 8, 'Bidan', '2019-07-21 06:25:14', '0000-00-00 00:00:00'),
(12, 11, 'Bidan', '2019-07-21 06:25:14', '0000-00-00 00:00:00'),
(13, 17, 'Bidan', '2019-07-21 06:25:14', '0000-00-00 00:00:00'),
(14, 6, 'Dokter Spesialis Kebidanan', '2019-07-21 06:25:14', '0000-00-00 00:00:00'),
(15, 9, 'Dokter Spesialis Kebidanan', '2019-07-21 06:25:14', '0000-00-00 00:00:00'),
(16, 15, 'Dokter Spesialis Kebidanan', '2019-07-21 06:25:14', '0000-00-00 00:00:00'),
(17, 14, 'Bidan', '2019-07-21 06:26:41', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spm_balita`
--

CREATE TABLE `spm_balita` (
  `id` bigint(100) NOT NULL,
  `nik` bigint(16) NOT NULL,
  `status_balita` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spm_balita`
--

INSERT INTO `spm_balita` (`id`, `nik`, `status_balita`, `created_at`, `updated_at`) VALUES
(1, 3278901234567120, 'Tahap Pelayanan', '2019-06-20 05:09:00', '2019-06-19 22:09:00'),
(2, 3278901234567111, 'Tahap Pelayanan', '2019-06-20 23:26:49', '2019-06-20 23:26:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spm_bayi_baru_lahir`
--

CREATE TABLE `spm_bayi_baru_lahir` (
  `id` bigint(100) NOT NULL,
  `nik_ibu` bigint(16) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `tgl_lahir` datetime NOT NULL,
  `jenis_kelamin` varchar(1) DEFAULT NULL,
  `status_kelahiran` varchar(100) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spm_bayi_baru_lahir`
--

INSERT INTO `spm_bayi_baru_lahir` (`id`, `nik_ibu`, `nama`, `tgl_lahir`, `jenis_kelamin`, `status_kelahiran`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(6, 3275041312670010, 'Bayi Ny.Riska Amelia Suhendar', '2019-09-17 03:18:00', 'P', 'Tahap Pelayanan', 'Rohimam', NULL, '2019-09-16 21:32:07', '2019-09-16 13:18:28'),
(7, 3275041110740002, 'Bayi Ny.Juminten', '2019-09-17 04:38:00', NULL, 'Tahap Pelayanan', 'Rohimam', NULL, '2019-09-16 14:38:16', '2019-09-16 14:38:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spm_diabetes_melitus`
--

CREATE TABLE `spm_diabetes_melitus` (
  `id` int(11) NOT NULL,
  `nik` bigint(16) NOT NULL,
  `tanggal_menderita` datetime NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spm_diabetes_melitus`
--

INSERT INTO `spm_diabetes_melitus` (`id`, `nik`, `tanggal_menderita`, `status`, `created_at`, `updated_at`) VALUES
(1, 3275041110740001, '2019-07-13 00:00:00', 'Tahap Pelayanan', '2019-07-23 19:36:31', '2019-07-23 12:36:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spm_hipertensi`
--

CREATE TABLE `spm_hipertensi` (
  `id` int(11) NOT NULL,
  `nik` bigint(16) NOT NULL,
  `tanggal_menderita` datetime NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spm_hipertensi`
--

INSERT INTO `spm_hipertensi` (`id`, `nik`, `tanggal_menderita`, `status`, `created_at`, `updated_at`) VALUES
(1, 3275567890933453, '2019-07-23 00:00:00', 'Tahap Pelayanan', '2019-07-22 17:43:41', '2019-07-22 17:43:41'),
(3, 3275041110740004, '2019-07-15 00:00:00', 'Tahap Pelayanan', '2019-07-23 07:23:46', '2019-07-23 07:23:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spm_ibu_bersalin`
--

CREATE TABLE `spm_ibu_bersalin` (
  `id` bigint(35) NOT NULL,
  `nik` bigint(16) NOT NULL,
  `tanggal_hamil` datetime NOT NULL,
  `tanggal_bersalin` datetime NOT NULL,
  `fasilitas_kesehatan` varchar(100) NOT NULL,
  `lokasi_fasilitas` varchar(100) NOT NULL,
  `tenaga_kesehatan` varchar(100) NOT NULL,
  `nama_tenaga` varchar(100) NOT NULL,
  `tenkes` varchar(100) DEFAULT NULL,
  `nama_tenkes` varchar(100) DEFAULT NULL,
  `tindakan` varchar(100) DEFAULT NULL,
  `link_gambar_partograf1` varchar(100) DEFAULT NULL,
  `link_gambar_partograf2` varchar(100) DEFAULT NULL,
  `status_persalinan` varchar(100) NOT NULL,
  `hasil_persalinan` varchar(100) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spm_ibu_bersalin`
--

INSERT INTO `spm_ibu_bersalin` (`id`, `nik`, `tanggal_hamil`, `tanggal_bersalin`, `fasilitas_kesehatan`, `lokasi_fasilitas`, `tenaga_kesehatan`, `nama_tenaga`, `tenkes`, `nama_tenkes`, `tindakan`, `link_gambar_partograf1`, `link_gambar_partograf2`, `status_persalinan`, `hasil_persalinan`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 3275041312670010, '2018-07-05 00:00:00', '2019-09-17 03:18:00', 'Poskesdes', 'Poskesdes Pasir Putih', 'Dokter', 'dr. Salim Santoso', 'Bidan', 'dr. Niniek Haryani', 'Dilayani', '', '', 'Normal', 'Hidup Sehat', 'Rohimam', NULL, '2019-09-16 13:18:28', '2019-09-16 13:18:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spm_ibu_hamil`
--

CREATE TABLE `spm_ibu_hamil` (
  `id` bigint(100) NOT NULL,
  `nik` bigint(16) NOT NULL,
  `tanggal_hamil` date NOT NULL,
  `nama_suami` varchar(100) NOT NULL,
  `tanggal_bersalin` date DEFAULT NULL,
  `status_kehamilan` varchar(100) NOT NULL,
  `kode_riwayat_kehamilan` varchar(9) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spm_ibu_hamil`
--

INSERT INTO `spm_ibu_hamil` (`id`, `nik`, `tanggal_hamil`, `nama_suami`, `tanggal_bersalin`, `status_kehamilan`, `kode_riwayat_kehamilan`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 3275041312670008, '2019-01-08', 'Abdullah Ali', '0000-00-00', 'Tahap Pelayanan', 'G1P0A0', 'Rohimam', '', '2019-09-16 20:12:01', '2019-09-16 12:53:58'),
(2, 3275041312670009, '2019-01-22', 'Abdul Majid', '0000-00-00', 'Tahap Pelayanan', 'G1P0A0', 'Rohimam', '', '2019-09-16 20:12:01', '2019-09-02 04:23:10'),
(3, 3275041312670010, '2018-01-31', 'Syaiful Rizky', '2018-05-21', 'Tidak Sesuai SPM', 'G1P0A0', 'Rohimam', 'Rohimam', '2019-08-22 00:50:29', '2019-05-20 18:21:03'),
(7, 3275041312670010, '2018-07-05', 'Syaiful Rizky', '2019-09-17', 'Tidak Sesuai SPM', 'G2P1A0', 'Rohimam', 'Rohimam', '2019-09-16 20:18:28', '2019-09-16 13:18:28'),
(14, 3275041110740003, '2019-08-22', 'Suprapto', NULL, 'Tahap Pelayanan', 'G1P0A0', 'Rohimam', NULL, '2019-08-21 18:49:28', '2019-08-21 18:49:28'),
(15, 3275041110740006, '2019-08-30', 'Ricky Januar Putra', NULL, 'Tahap Pelayanan', 'G1P0A0', 'Rohimam', NULL, '2019-08-29 18:13:24', '2019-08-29 18:13:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spm_odgj`
--

CREATE TABLE `spm_odgj` (
  `id` int(11) NOT NULL,
  `nik` bigint(16) NOT NULL,
  `tanggal_menderita` datetime NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spm_odgj`
--

INSERT INTO `spm_odgj` (`id`, `nik`, `tanggal_menderita`, `status`, `created_at`, `updated_at`) VALUES
(1, 3275567890933453, '2019-07-23 00:00:00', 'Tahap Pelayanan', '2019-07-22 17:47:46', '2019-07-22 17:47:46'),
(2, 3275041110740004, '2019-07-23 00:00:00', 'Tahap Pelayanan', '2019-07-22 17:52:00', '2019-07-22 17:52:00'),
(3, 3275041110740001, '2019-07-23 00:00:00', 'Tahap Pelayanan', '2019-07-22 19:24:16', '2019-07-22 19:24:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spm_risiko_hiv`
--

CREATE TABLE `spm_risiko_hiv` (
  `id` int(11) NOT NULL,
  `nik` bigint(16) NOT NULL,
  `tanggal_menderita` datetime NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spm_risiko_hiv`
--

INSERT INTO `spm_risiko_hiv` (`id`, `nik`, `tanggal_menderita`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 3275041110740001, '2019-09-07 00:00:00', 'Tahap Pelayanan', 'Rohimam', NULL, '2019-09-07 00:20:32', '2019-09-07 00:20:32'),
(2, 3275041110740002, '2019-09-09 00:00:00', 'Tahap Pelayanan', 'Rohimam', NULL, '2019-09-07 00:24:29', '2019-09-07 00:24:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spm_tuberkulosis`
--

CREATE TABLE `spm_tuberkulosis` (
  `id` int(11) NOT NULL,
  `nik` bigint(16) NOT NULL,
  `tanggal_menderita` datetime NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spm_tuberkulosis`
--

INSERT INTO `spm_tuberkulosis` (`id`, `nik`, `tanggal_menderita`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 3275041110740001, '2019-09-07 00:00:00', 'Tahap Pelayanan', 'Rohimam', NULL, '2019-09-07 00:22:13', '2019-09-07 00:22:13'),
(2, 3275041110740002, '2019-09-08 00:00:00', 'Tahap Pelayanan', 'Rohimam', NULL, '2019-09-07 00:23:48', '2019-09-07 00:23:48');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spm_usia_lanjut`
--

CREATE TABLE `spm_usia_lanjut` (
  `id` int(11) NOT NULL,
  `nik` bigint(16) NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spm_usia_lanjut`
--

INSERT INTO `spm_usia_lanjut` (`id`, `nik`, `status`, `created_at`, `updated_at`) VALUES
(1, 3275567890933403, 'Tahap Pelayanan', '2019-07-03 06:52:51', '2019-07-02 23:52:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spm_usia_pend_dasar`
--

CREATE TABLE `spm_usia_pend_dasar` (
  `id` bigint(16) NOT NULL,
  `nik` bigint(16) NOT NULL,
  `status_usia_pend_dasar` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spm_usia_pend_dasar`
--

INSERT INTO `spm_usia_pend_dasar` (`id`, `nik`, `status_usia_pend_dasar`, `created_at`, `updated_at`) VALUES
(1, 3275567890933451, 'Tahap Pelayanan', '2019-06-21 00:38:09', '2019-06-21 00:38:09'),
(2, 3275567890933453, 'Tahap Pelayanan', '2019-06-26 06:21:17', '2019-06-25 23:21:17'),
(3, 3275567890933452, 'Tahap Pelayanan', '2019-09-22 15:02:31', '2019-09-22 15:02:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spm_usia_prod`
--

CREATE TABLE `spm_usia_prod` (
  `id` int(11) NOT NULL,
  `nik` bigint(16) NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spm_usia_prod`
--

INSERT INTO `spm_usia_prod` (`id`, `nik`, `status`, `created_at`, `updated_at`) VALUES
(1, 3275041212960007, 'Tahap Pelayanan', '2019-07-02 13:27:24', '2019-07-02 06:27:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tenaga_kesehatan`
--

CREATE TABLE `tenaga_kesehatan` (
  `id` int(11) NOT NULL,
  `id_mitra` int(11) NOT NULL,
  `nik` bigint(16) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tgl_lahir` datetime NOT NULL,
  `spesialis` varchar(200) NOT NULL,
  `tahun_berkarir` int(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tenaga_kesehatan`
--

INSERT INTO `tenaga_kesehatan` (`id`, `id_mitra`, `nik`, `nama`, `tgl_lahir`, `spesialis`, `tahun_berkarir`, `created_at`, `updated_at`) VALUES
(1, 1, 3275060109870001, 'dr. Ahmad Riza Syauqi', '1987-09-01 00:00:00', 'Dokter', 2000, '2019-07-21 06:44:19', '2019-07-20 17:00:00'),
(2, 2, 3275060901870001, 'dr. Rizqa Latifah', '1987-01-09 00:00:00', 'Bidan', 2000, '2019-07-21 06:44:52', '2019-07-20 17:00:00'),
(3, 3, 3275061109870001, 'dr. Mahmud Rizki Abdullah', '1987-09-11 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-21 06:45:27', '2019-07-20 17:00:00'),
(4, 4, 3275061201870001, 'dr. Ahmad Subakrie', '1987-01-12 00:00:00', 'Dokter', 2000, '2019-07-21 06:44:25', '2019-07-20 17:00:00'),
(5, 5, 3275061901870001, 'dr. Ricky Firmansyah', '1987-01-19 00:00:00', 'Bidan', 2000, '2019-07-21 06:44:57', '2019-07-20 17:00:00'),
(6, 6, 3275060406870001, 'dr. Mutia Safari', '1987-06-04 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-21 06:45:34', '2019-07-20 17:00:00'),
(7, 7, 3275062109870001, 'dr. Geraldine Agusta', '1987-09-21 00:00:00', 'Dokter', 2000, '2019-07-21 06:44:31', '2019-07-20 17:00:00'),
(8, 8, 3275061507870001, 'dr. Muhammad Fikri', '1987-07-15 00:00:00', 'Bidan', 2000, '2019-07-21 06:45:01', '2019-07-20 17:00:00'),
(9, 9, 3275061309870001, 'dr. Prianda Dipati', '1987-09-13 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-21 06:45:39', '2019-07-20 17:00:00'),
(10, 1, 3275060901870002, 'dr. Aisyah Dewi Hasanah', '1987-01-09 00:00:00', 'Dokter', 2000, '2019-07-21 06:44:35', '2019-07-20 17:00:00'),
(11, 2, 3275060901870003, 'dr. Evelinda Siahaan', '1987-01-09 00:00:00', 'Bidan', 2000, '2019-07-21 06:45:05', '2019-07-20 17:00:00'),
(12, 3, 3275060901870004, 'dr. Agung Ibrahim', '1987-01-09 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-21 06:45:44', '2019-07-20 17:00:00'),
(13, 4, 3275060901870005, 'dr. Willy Bima Alfajri', '1987-01-09 00:00:00', 'Dokter', 2000, '2019-07-21 06:44:40', '2019-07-20 17:00:00'),
(14, 5, 3275060901870006, 'dr. Dita Ananda', '1987-01-09 00:00:00', 'Bidan', 2000, '2019-07-21 06:45:09', '2019-07-20 17:00:00'),
(15, 6, 3275060901870007, 'dr. Viranita Julianti', '1987-01-09 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-21 06:45:51', '2019-07-20 17:00:00'),
(16, 7, 3275060901870008, 'dr. Iif Rifai', '1987-01-09 00:00:00', 'Dokter', 2000, '2019-07-21 06:44:44', '2019-07-20 17:00:00'),
(17, 8, 3275060901870009, 'dr. Steven Stevu Denito', '1987-01-09 00:00:00', 'Bidan_Dokter Spesialis Kebidanan', 2000, '2019-07-21 06:45:56', '2019-07-20 17:00:00'),
(18, 10, 3278901209820001, 'dr. Setiawan Gilang', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 12:10:33', '0000-00-00 00:00:00'),
(19, 10, 3278901209820002, 'dr. Rahmat Dirga', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(20, 10, 3278901209820003, 'dr. Fitri Dea Hirman', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(21, 10, 3278901209820004, 'dr. Riska Ayu Diana', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(22, 10, 3278901209820005, 'dr. Siti Jannatul', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(23, 10, 3278901209820006, 'dr. Mawah Airi', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(24, 10, 3278901209820007, 'dr. Ridwan Ali Juniaidi', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(25, 10, 3278901209820008, 'dr. Tyan GUmilang Ricky', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(26, 10, 3278901209820009, 'dr. Hermansyah', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(27, 11, 3278901209820010, 'dr. Sandhika Ramadhan', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(28, 11, 3278901209820011, 'dr. Aini Dewi Sri', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(29, 11, 3278901209820012, 'dr. Sabillah Gina Ayu', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(30, 11, 3278901209820013, 'dr. Galang Putra', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(31, 11, 3278901209820014, 'dr. Putri Aini', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(32, 11, 3278901209820015, 'dr. Belinda Syahputri', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(33, 11, 3278901209820016, 'dr. Dameria Putri', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(34, 11, 3278901209820017, 'dr. Rosya Ayu GH', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(35, 12, 3278901209820018, 'dr. Aam Dewi Hamidah ', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(36, 12, 3278901209820019, 'dr. Abdul Hadi Ismail ', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(37, 12, 3278901209820020, 'dr. Abdul Rifai Natanegara ', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(38, 12, 3278901209820021, 'dr. Bimo Pramudyo Soekarno ', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(39, 12, 3278901209820022, 'dr. Hendry Lesmana ', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(40, 12, 3278901209820023, 'dr. Joko Himawan', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(41, 12, 3278901209820024, 'dr. Kurniawan Nurdin Karim ', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(42, 12, 3278901209820025, 'dr. Linda Tanuwiradjaja ', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(43, 13, 3278901209820026, 'dr. Mochtar Riady ', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(44, 13, 3278901209820027, 'dr. Niniek Haryani ', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(45, 13, 3278901209820028, 'dr. Pramudji Suginawan ', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(46, 13, 3278901209820029, 'dr. Rahardjo Moecharar ', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(47, 13, 3278901209820030, 'dr. Salim Santoso ', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(48, 13, 3278901209820031, 'dr. Triana Soenryo ', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(49, 13, 3278901209820032, 'dr. Winardi Thamrin ', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(50, 13, 3278901209820033, 'dr. Vivian Kent Janawati ', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(51, 14, 3278901209820034, 'dr. Wiwin Untari Andoko ', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(52, 14, 3278901209820035, 'dr. Yogie Sandi Yudhianto ', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(53, 14, 3278901209820036, 'dr. Zaini Abidin Noor ', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(54, 14, 3278901209820037, 'dr. Lusi Windayati ', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(55, 14, 3278901209820038, 'dr. Diniwati ', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(56, 14, 3278901209820039, 'dr. Fahmi Idris ', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(57, 14, 3278901209820040, 'dr. Nugraha Tirtanata ', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(58, 14, 3278901209820041, 'dr. Roswita Trismitro ', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(59, 15, 3278901209820042, 'dr. Sri Rahayu Setyawati ', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(60, 15, 3278901209820043, 'dr. Taufik Surya Dharma ', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(61, 15, 3278901209820044, 'dr. Tiara Dharani Josodirdjo ', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(62, 15, 3278901209820045, 'dr. Anita Ratnasari', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(63, 15, 3278901209820046, 'dr. Amalia Aristiningsih ', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(64, 15, 3278901209820047, 'dr. Bambang Subianto ', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(65, 15, 3278901209820048, 'dr. Candra Winoto Salim ', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(66, 15, 3278901209820049, 'dr. Danny Nugroho ', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(67, 16, 3278901209820050, 'dr. Dharmawandi Sutanto ', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(68, 16, 3278901209820051, 'dr. Doddy Agustiawan Tjahjadi ', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(69, 16, 3278901209820052, 'dr. Edo Djunaydi', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(70, 16, 3278901209820053, 'dr. Edwin Sugiarto', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(71, 16, 3278901209820054, 'dr. Edwin Mohtar', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(72, 16, 3278901209820055, 'dr. Edwin Soeryadjaya', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(73, 16, 3278901209820056, 'dr. Fida Unidjaja', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(74, 16, 3278901209820057, 'dr. Fiona Adeline Sutanto', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(75, 17, 3278901209820058, 'dr. Hendra Tjahyawati', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(76, 17, 3278901209820059, 'dr. Retno Cahyaningtyas', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(77, 17, 3278901209820060, 'dr. Rudy Cahyadi Sukandadinata', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(78, 17, 3278901209820061, 'dr. Aji Bayu Wirrotama', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(79, 17, 3278901209820062, 'dr. Awiek Lestari Rahayu', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(80, 17, 3278901209820063, 'dr. Bayu Irianto', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(81, 17, 3278901209820064, 'dr. Emily Ayu Subrata', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(82, 17, 3278901209820065, 'dr. Dian Muljani Soedarjo', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(83, 18, 3278901209820066, 'dr. Soetikno Soedarjo', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(84, 18, 3278901209820067, 'dr. Kezia Putri', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(85, 18, 3278901209820068, 'dr. Putri Rahayu Soedarjo', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(86, 18, 3278901209820069, 'dr. Aprilya Kusdianto', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(87, 18, 3278901209820070, 'dr. Myrna Putri Pribadi', '1982-09-12 00:00:00', 'Dokter', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(88, 18, 3278901209820071, 'dr. Rini Susilo Putri', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(89, 18, 3278901209820072, 'dr. Widiyanti Putri', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(90, 18, 3278901209820073, 'dr. Widiyanti Putri', '1982-09-12 00:00:00', 'Dokter Spesialis Kebidanan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00'),
(91, 19, 3278901209820074, 'dr. Atira Aksa', '1982-09-12 00:00:00', 'Bidan', 2000, '2019-07-22 14:22:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `instansi` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_profile` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `level`, `instansi`, `foto_profile`, `bio`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ega Javier Harwenda', 'egajavi', 'ega@gik.com', NULL, '$2y$10$wEC9t8//wf9KILi.J4TgW.eQ3h9dBiC.rq5BgYgOqR5cGGEo6dCam', 1, 'GIK', NULL, '</ Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you life />', NULL, '2019-04-12 10:21:34', '2019-04-12 10:21:34'),
(2, 'Farkhan Adiansyah', 'farkhanaan', 'farkhan@gik.com', NULL, '$2y$10$8U5B.EC6GgBoYV2aagjF4ug93w/oGEh0swCofeZWqHSCrauPRtEgO', 1, 'GIK', NULL, '</ Programs must be written for people to read, and only incidentally for machines to execute />', NULL, '2019-07-05 00:33:06', '2019-07-05 00:33:06'),
(3, 'Fidya Kusumaningrum', 'fidyakusuma', 'fidya@gik.com', NULL, '$2y$10$Tkf/1Gs79fIM4PAIrXNPp.KNQwPjEoo.K5f/uQeOsCXJGeEa1/yIG', 1, 'GIK', NULL, 'Hidup tidak memberi kita tujuan, namun kita memberi tujuan dalam hidup -Barry Allen', NULL, '2019-07-05 00:34:55', '2019-07-05 00:34:55'),
(4, 'Rohimam', 'rohimam', 'rohimam@gik.com', NULL, '$2y$10$29MZHC0s8x/iHk7HtAaDsuKq1qGyF66/VhjBrpK/wuCFaeVXjAe7a', 1, 'GIK', NULL, 'I am Rohimam', NULL, '2019-07-05 00:39:08', '2019-07-05 00:39:08'),
(5, 'Dyah Safitri', 'dyahsaf', 'dyahsafitri@gik.com', NULL, '$2y$10$wdsrF8c2mzzMzSp/htONRutURZLcouutkgiWELaoYMHy.S9PBK6hu', 3, 'GIK', NULL, 'Good Job', NULL, '2019-07-06 19:28:09', '2019-07-06 19:28:09'),
(6, 'Mitra Keluarga', 'mitrakeluarga', 'mitrakeluarga@gmail.com', NULL, '$2y$10$hUZqSKo0ouIot.6oG3OuUukQQ58/t6jr.PQtN3FF5OTbsdG9uAvGy', 3, 'RS. Mitra Keluarga', NULL, NULL, NULL, '2019-07-28 20:11:19', '2019-07-28 20:11:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `warga`
--

CREATE TABLE `warga` (
  `id` int(11) NOT NULL,
  `nik` bigint(16) NOT NULL,
  `no_kk` bigint(16) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tgl_lahir` datetime NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `gol_darah` varchar(10) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `kode_pos` int(5) NOT NULL,
  `kelurahan` varchar(100) NOT NULL,
  `kecamatan` varchar(100) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `agama` varchar(50) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `status_perkawinan` varchar(100) NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `kewarganegaraan` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(100) NOT NULL,
  `id_created_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(100) DEFAULT NULL,
  `id_updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `warga`
--

INSERT INTO `warga` (`id`, `nik`, `no_kk`, `nama`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `gol_darah`, `alamat`, `kode_pos`, `kelurahan`, `kecamatan`, `kota`, `provinsi`, `agama`, `no_telp`, `status_perkawinan`, `pekerjaan`, `kewarganegaraan`, `created_at`, `created_by`, `id_created_by`, `updated_at`, `updated_by`, `id_updated_by`) VALUES
(1, 3270409201800001, 9990409201800001, 'Citra Destriya', 'Depok', '2019-07-24 00:00:00', 'P', 'AB', 'Jl. Depok Indah 2 Blok C No.3', 0, 'Beji', 'Beji', 'Depok', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', 'Belum Bekerja', 'Indonesia', '2019-09-08 08:34:25', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(2, 3275041110740001, 9995041110740001, 'Markonah', 'Bekasi', '1991-07-18 00:00:00', 'P', 'O', 'Jalan H. Mahmud No. 15', 0, 'Jatiasih', 'Jatiasih', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Menikah', 'Ibu Rumah Tangga', 'Indonesia', '2019-09-08 08:34:55', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(3, 3275041110740002, 9995041110740002, 'Juminten', 'Bekasi', '1994-04-08 00:00:00', 'P', 'AB', 'Jalan Luas Gang Senggol No.17', 0, 'Pekayon Jaya', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Menikah', 'Ibu Rumah Tangga', 'Indonesia', '2019-09-08 08:34:59', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(4, 3275041110740003, 9995041110740003, 'Suniati', 'Bekasi', '1974-10-11 00:00:00', 'P', 'B', 'Jalan Pengairan', 0, 'Jakasetia', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '085719368361', 'Menikah', 'Ibu Rumah Tangga', 'Indonesia', '2019-09-08 08:35:04', 'Rohimam', 4, '2019-08-21 18:49:28', NULL, NULL),
(5, 3275041110740004, 9995041110740004, 'Siti Hurairah', 'Bekasi', '1967-04-14 00:00:00', 'P', 'O', 'Jln, Merpati', 0, 'Jatiasih', 'Jatiasih', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Menikah', 'Ibu Rumah Tangga', 'Indonesia', '2019-09-08 08:35:07', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(6, 3275041110740005, 9995041110740005, 'Aisyah Tumairah', 'Bekasi', '1967-08-14 00:00:00', 'P', 'O', 'Jln, Merpati', 0, 'Jatirasa', 'Jatiasih', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Menikah', 'Ibu Rumah Tangga', 'Indonesia', '2019-09-08 08:35:11', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(7, 3275041110740006, 9995041110740006, 'Tania Laena', 'Bekasi', '1987-04-10 00:00:00', 'P', 'AB', 'Jln. Kenanga', 0, 'Jatisari', 'Jatiasih', 'Bekasi', 'Jawa Barat', 'Islam', '082319970909', 'Menikah', 'Ibu Rumah Tangga', 'Indonesia', '2019-09-08 08:35:18', 'Rohimam', 4, '2019-08-29 18:13:24', NULL, NULL),
(8, 3275041212960007, 9995041212960007, 'Rohimam', 'Bekasi', '1996-12-12 00:00:00', 'L', 'AB', 'Jln. Pengairan', 0, 'Jakasetia', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Belum Kawin', 'Mahasiswa', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(9, 3275041312670005, 9995041312670005, 'Suprapto', 'Bekasi', '1967-12-13 00:00:00', 'L', 'A', 'Jalan Pengairan', 0, 'Jakasetia', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Menikah', 'Buruh Wiraswasta', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(10, 3275041312670007, 9995041312670007, 'Mardiyah', 'Bandung', '1978-04-12 00:00:00', 'P', 'A', 'Jln. Buah Batu', 0, 'Jakamulya', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Menikah', 'Ibu Rumah Tangga', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(11, 3275041312670008, 9995041312670008, 'Rina Ardiana Putri', 'Malang', '1978-07-12 00:00:00', 'P', 'AB', 'Jln. Sempit III No.3', 0, 'Pekayon Jaya', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Menikah', 'Ibu Rumah Tangga', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(12, 3275041312670009, 9995041312670009, 'Fitriana Rosyaputri', 'Bandung', '1988-08-08 00:00:00', 'P', 'O', 'Jln. Jaya Merdeka II No.12', 0, 'Pekayon Jaya', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Menikah', 'Ibu Rumah Tangga', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(13, 3275041312670010, 9995041312670010, 'Riska Amelia Suhendar', 'Bogor', '1968-01-12 00:00:00', 'P', 'B', 'Jln. Serdadu 9', 0, 'Pekayon Jaya', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Menikah', 'Ibu Rumah Tangga', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(14, 3275041607190001, 9995041607190001, 'Tiara Andini', 'Depok', '2019-07-17 06:20:54', 'P', 'AB', 'Jl. Pemuda No.18 Block A5', 0, 'Beji', 'Beji', 'Depok', 'Jawa Barat', 'Islam', '089601723344', 'Belum Menikah', 'Belum Bekerja', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '2019-07-16 23:20:54', NULL, NULL),
(15, 3275041607191111, 9995041607191111, 'Evelinda Siahaan', 'Prabumulih', '1997-11-07 00:00:00', 'P', 'A', 'Jalan Balongan No.19A Block R25', 0, 'Tanah Baru', 'Beji', 'Depok', 'Jawa Barat', 'Protestan', '089601723341', 'Belum Menikah', 'Mahasiswa', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '2019-07-16 21:57:09', NULL, NULL),
(16, 3275567890933401, 9995567890933401, 'Rinjani', 'Bekasi', '1956-08-01 00:00:00', 'P', 'AB', 'Jln. Jauh', 0, 'Kecapi Timur', 'Keacpi', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', 'Ibu Rumah Tangga', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(17, 3275567890933402, 9995567890933402, 'Mnja', 'Bekasi', '1958-01-12 00:00:00', 'P', 'AB', 'Jln. Jauh', 0, 'Kecapi Timur', 'Kecapi', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', 'Ibu Rumah Tangga', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(18, 3275567890933403, 9995567890933403, 'Lidya', 'Bekasi', '1957-08-01 08:09:00', 'P', 'AB', 'Jln. Jauh', 0, 'Kecapi Timur', 'Kecapi', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', 'Ibu Rumah Tangga', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(19, 3275567890933451, 9995567890933451, 'Ricky Bachtiar', 'Bekasi', '2013-12-01 00:00:00', 'L', 'AB', 'Jln. Jauh', 0, 'Kecapi Barat', 'Kecapi', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', '', '', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(20, 3275567890933452, 9995567890933452, 'Fitri Anisah', 'Bekasi', '2013-07-17 00:00:00', 'P', 'O', 'Jln. Jauh', 0, 'Kecapi Barat', 'Kecapi', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', '', '', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(21, 3275567890933453, 9995567890933453, 'Ridwansyah', 'Bekasi', '2004-03-11 00:00:00', 'L', 'B', 'Jln. Jauh', 0, 'Kecapi Barat', 'Kecapi', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', '', '', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(22, 3278901234567111, 9998901234567111, 'Ridho Alghifari', 'Bekasi', '2019-06-17 00:38:00', 'L', 'AB', 'Jln. Kenanga Semilir No.32', 0, 'Jatiasih', 'Jatiasih', 'Bekasi ', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', 'Belum Bekerja', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(23, 3278901234567112, 9998901234567112, 'Muhammad Ali Firdaus', 'Bekasi', '2019-06-17 08:54:00', 'L', 'AB', 'Jln. Merdeka Barat ', 0, 'Jakasetia', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', 'Belum Bekerja', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(24, 3278901234567113, 9998901234567113, 'Riska Ayu Ningtyas', 'Bekasi', '2019-06-18 01:02:00', 'P', 'O', 'Jln. Merdeka Timur No.13 ', 0, 'Jakasetia', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', 'Belum Bekerja', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(25, 3278901234567114, 9998901234567114, 'Ratna Dewi Hindasari', 'Bekasi', '2019-06-17 05:39:00', 'P', 'B', 'Jln. Merdeka Selatan No.13 ', 0, 'Jakasetia', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', 'Belum Bekerja', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(26, 3278901234567115, 9998901234567115, 'Wydyna Riski Putri', 'Bekasi', '2019-06-17 00:04:00', 'P', 'A', 'Jln. Merdeka Utara No.98 ', 0, 'Jakasetia', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', 'Belum Bekerja', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(27, 3278901234567116, 9998901234567116, 'Aini Ghina Umayroh', 'Bekasi', '2019-06-16 07:12:00', 'P', 'B', 'Jln. Merdeka Barat No.13 ', 0, 'Jakasetia', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', 'Belum Bekerja', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(28, 3278901234567117, 9998901234567117, 'Balqis Bella Putri', 'Bekasi', '2019-06-16 15:07:00', 'P', 'B', 'Jln. Merdeka Barat Block A1 No.54 ', 0, 'Jakasetia', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', 'Belum Bekerja', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(29, 3278901234567118, 9998901234567118, 'Catur Pangestu Dhika', 'Bekasi', '2019-06-18 22:30:00', 'L', 'AB', 'Jln. Merdeka Raya Block B2 ', 0, 'Jakasetia', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', 'Belum Bekerja', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(30, 3278901234567119, 9998901234567119, 'Dimas Whedaniputra', 'Bekasi', '2019-06-18 00:17:00', 'L', 'O', 'Jln. Merdeka Selatan No.13 ', 0, 'Jakasetia', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', 'Belum Bekerja', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL),
(31, 3278901234567120, 9998901234567120, 'Egi Herdiansyah', 'Bekasi', '2019-06-30 00:00:00', 'L', 'AB', 'Jln. Merdeka Raya Block C1', 0, 'Jakasetia', 'Bekasi Selatan', 'Bekasi', 'Jawa Barat', 'Islam', '089601723341', 'Belum Menikah', 'Belum Bekerja', 'Indonesia', '2019-09-08 08:38:01', 'Rohimam', 4, '0000-00-00 00:00:00', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `catatan_pelayanan`
--
ALTER TABLE `catatan_pelayanan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `mitra_kesehatan`
--
ALTER TABLE `mitra_kesehatan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `spesialis`
--
ALTER TABLE `spesialis`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `spm_balita`
--
ALTER TABLE `spm_balita`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `spm_bayi_baru_lahir`
--
ALTER TABLE `spm_bayi_baru_lahir`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `spm_diabetes_melitus`
--
ALTER TABLE `spm_diabetes_melitus`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `spm_hipertensi`
--
ALTER TABLE `spm_hipertensi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `spm_ibu_bersalin`
--
ALTER TABLE `spm_ibu_bersalin`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `spm_ibu_hamil`
--
ALTER TABLE `spm_ibu_hamil`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `spm_odgj`
--
ALTER TABLE `spm_odgj`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `spm_risiko_hiv`
--
ALTER TABLE `spm_risiko_hiv`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `spm_tuberkulosis`
--
ALTER TABLE `spm_tuberkulosis`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `spm_usia_lanjut`
--
ALTER TABLE `spm_usia_lanjut`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `spm_usia_pend_dasar`
--
ALTER TABLE `spm_usia_pend_dasar`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `spm_usia_prod`
--
ALTER TABLE `spm_usia_prod`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tenaga_kesehatan`
--
ALTER TABLE `tenaga_kesehatan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indeks untuk tabel `warga`
--
ALTER TABLE `warga`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nik` (`nik`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `catatan_pelayanan`
--
ALTER TABLE `catatan_pelayanan`
  MODIFY `id` bigint(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `kelurahan`
--
ALTER TABLE `kelurahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `mitra_kesehatan`
--
ALTER TABLE `mitra_kesehatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT untuk tabel `spesialis`
--
ALTER TABLE `spesialis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `spm_balita`
--
ALTER TABLE `spm_balita`
  MODIFY `id` bigint(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `spm_bayi_baru_lahir`
--
ALTER TABLE `spm_bayi_baru_lahir`
  MODIFY `id` bigint(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `spm_diabetes_melitus`
--
ALTER TABLE `spm_diabetes_melitus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `spm_hipertensi`
--
ALTER TABLE `spm_hipertensi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `spm_ibu_bersalin`
--
ALTER TABLE `spm_ibu_bersalin`
  MODIFY `id` bigint(35) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `spm_ibu_hamil`
--
ALTER TABLE `spm_ibu_hamil`
  MODIFY `id` bigint(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `spm_odgj`
--
ALTER TABLE `spm_odgj`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `spm_risiko_hiv`
--
ALTER TABLE `spm_risiko_hiv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `spm_tuberkulosis`
--
ALTER TABLE `spm_tuberkulosis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `spm_usia_lanjut`
--
ALTER TABLE `spm_usia_lanjut`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `spm_usia_pend_dasar`
--
ALTER TABLE `spm_usia_pend_dasar`
  MODIFY `id` bigint(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `spm_usia_prod`
--
ALTER TABLE `spm_usia_prod`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tenaga_kesehatan`
--
ALTER TABLE `tenaga_kesehatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `warga`
--
ALTER TABLE `warga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
