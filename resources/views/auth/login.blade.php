<!DOCTYPE html>
<html class="no-js">
<head>
  @include('content.head')@include('content.main')@include('content.js')
  @yield('head-login')
  <title>Masuk | Continuum of Care</title>
</head>
<body>
<div class="main-wrapper">
        <!-- @yield('main-preloader') -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center bg-01">
            <div class="auth-box br-7">
                <div id="loginform">
                    <div class="logo pb-20">
                        <span class="db"><img src="{{asset('adminbite-10/assets/images/sicoc-logo.png')}}" class="he-100" alt="logo" /></span>
                        <h5 class="font-medium m-b-20" style="font-size:15px;">{{ __('Sistem Informasi Continuum of Care') }}</h5>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <form class="form-horizontal m-t-20" id="loginform" method="POST" action="{{ route('login') }}"enctype="multipart/form-data">
                                @csrf
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="ti-user"></i></span>
                                    </div>
                                    <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email atau Username" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon2"><i class="ti-pencil"></i></span>
                                    </div>
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-lg" placeholder="Kata Sandi" style="font-size:15px;" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group text-center">
                                    <div class="col-xs-12 p-b-20">
                                      <button class="btn btn-block btn-lg btn-info" type="submit">Masuk</button>
                                    </div>
                                </div>

                                <!-- <div class="form-group m-b-0 m-t-10">
                                    <div class="col-sm-12 text-center">
                                        Belum memiliki akun? <a href="javascript:void(0)" id="to-recover" class="text-info m-l-5"><b>Daftar ></b></a>
                                    </div>
                                </div> -->
                            </form>
                        </div>
                    </div>
                </div>
                <div id="recoverform">
                    <div class="logo">
                        <span class="db"><img src="{{asset('adminbite-10/assets/images/sicoc-logo.png')}}" class="wi-50" alt="logo" /></span>
                        <h5 class="font-medium m-b-20">Pendaftaran Akun</h5>
                        <span>Datang ke Dinas Kesehatan dan</span>
                        <span>Hubungi admin untuk ajukan pembuatan akun!</span>
                    </div>
                    <div class="row m-t-20">
                        <form class="col-12" action="index.html">
                            <div class="form-group m-b-0 m-t-10">
                              <div class="col-sm-12 text-center">
                                <a href="javascript:void(0)" id="to-login" class="text-info m-l-5"><b>< Kembali</b></a>  
                              </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @yield('js-login')
</body>
</html>
