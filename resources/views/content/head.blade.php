@section('head-login')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="GIK">
    <meta name="author" content="GIK">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('adminbite-10/assets/images/sicoc-favicon.png')}}">    
    <link href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
    <style>
    .bg-01{
        background:rgba(0,0,0,0.1);
    }
    .br-7{
        border-radius: 7px;
    }
    .pb-20{
        padding-bottom: 20px;
    }
    .he-100{
        height:100px;
    }
    .wi-50{
        width:50px;
    }
  </style>
@endsection
@section('head-home')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="GIK">
    <meta name="author" content="GIK">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('adminbite-10/assets/images/sicoc-favicon.png')}}">
    <link href="{{asset('adminbite-10/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{asset('adminbite-10/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
    <link href="{{asset('adminbite-10/assets/libs/morris.js/morris.css')}}" rel="stylesheet">
    <link href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">

    <link href="{{asset('adminbite-10/assets/libs/toastr/build/toastr.min.css')}}" rel="stylesheet">

    <link href="{{asset('adminbite-10/assets/libs/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet">
    <style>
        .sidebar-item a{
            font-weight:600;
        }
        .tx-c{
            text-align:center;
        }
        .bg-y{
            background:yellow;
        }
        .sidebar-link .icon-Record{
            visibility: visible !important;
        }
    </style>
@endsection
@section('head-ripel01')
    <link href="{{asset('adminbite-10/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('head-preloader')
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
@endsection
@section('head-topbar')
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header">
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)">
                        <i class="ti-menu ti-close"></i>
                    </a>
                    <a class="navbar-brand" href="index.html">
                        <b class="logo-icon">
                            <img src="{{asset('adminbite-10/assets/images/sicoc-ic-01.png')}}" alt="homepage" class="dark-logo" style="width:20px;" />
                        </b>
                        <span class="logo-text">
                        <img src="{{asset('adminbite-10/assets/images/RENCANA1/sicoc-up-fb.png')}}" alt="homepage" class="dark-logo" style="width:85px;" />
                            <!-- <img src="{{asset('adminbite-10/assets/images/logo-light-text.png')}}" class="light-logo" alt="homepage" /> -->
                        </span>
                    </a>
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="ti-more"></i>
                    </a>
                </div>
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav float-left mr-auto">
                        <li class="nav-item d-none d-md-block">
                            <a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar">
                                <i class="sl-icon-menu font-20"></i>
                            </a>
                        </li>
                    </ul>                    
                    <ul class="navbar-nav float-right">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <img src="{{asset('adminbite-10/assets/images/users/user.png')}}" alt="user" class="rounded-circle" width="31"> Profil
                            </a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <span class="with-arrow">
                                    <span class="bg-primary"></span>
                                </span>
                                <div class="d-flex no-block align-items-center p-15 bg-primary text-white m-b-10">
                                    <div class="">
                                        <img src="{{asset('adminbite-10/assets/images/users/user.png')}}" alt="user" class="img-circle" width="60">
                                    </div>
                                    <div class="m-l-10">
                                        <h4 class="m-b-0">{{ Auth::user()['name'] }}</h4>
                                        <p class=" m-b-0">{{ Auth::user()['email'] }}</p>
                                    </div>
                                </div>
                                <a class="dropdown-item" href="javascript:void(0)">
                                    <i class="ti-user m-r-5 m-l-5"></i>Profil Saya</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)">
                                    <i class="ti-settings m-r-5 m-l-5"></i>Pengaturan Akun</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}"onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off m-r-5 m-l-5"></i> Keluar</a>
                                <div class="dropdown-divider"></div>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>                    
                    </ul>
                </div>
            </nav>
        </header>
@endsection
@section('head-asidebar')
        <aside class="left-sidebar">
            <div class="scroll-sidebar">
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item">
                            <a href="{{ route('home') }}" class="sidebar-link">
                                <i class="icon-Car-Wheel"></i>
                                <span class="hide-menu"> Beranda </span>
                            </a>
                        </li>
                        <li class="nav-small-cap">
                            <i class="mdi mdi-dots-horizontal"></i>
                            <span class="hide-menu">Standar Pelayanan Minimal</span>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{url('/ibu-hamil')}}" class="sidebar-link">
                                <i class="fas fa-notes-medical"></i>
                                <span class="hide-menu">Ibu Hamil </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link">
                                <i class="fas fa-notes-medical"></i>
                                <span class="hide-menu">Ibu Bersalin </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link">
                                <i class="fas fa-notes-medical"></i>
                                <span class="hide-menu">Anak baru lahir </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link">
                                <i class="fas fa-notes-medical"></i>
                                <span class="hide-menu">Balita </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link">
                                <i class="fas fa-notes-medical"></i>
                                <span class="hide-menu">Usia Pend. Dasar </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link">
                                <i class="fas fa-notes-medical"></i>
                                <span class="hide-menu">Usia Produktif </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link">
                                <i class="fas fa-notes-medical"></i>
                                <span class="hide-menu">Usia Lanjut </span>
                            </a>
                        </li>
                        @if (Auth::user()['level'] == 1 || Auth::user()['level'] == 2 || Auth::user()['level'] == 3 )
                            <li class="sidebar-item">
                                <a href="{{url('/input-spm')}}" class="sidebar-link">
                                    <i class="fas fa-notes-medical"></i>
                                    <span class="hide-menu">Input SPM </span>
                                </a>
                            </li>
                        @endif
                        <li class="nav-small-cap">
                            <i class="mdi mdi-dots-horizontal"></i>
                            <span class="hide-menu">Data User</span>
                        </li>
                        @if (Auth::user()['level'] == 1 )
                        <li class="sidebar-item">
                            <a href="{{url('/input-spm')}}" class="sidebar-link">
                                <i class="fas fa-notes-medical"></i>
                                <span class="hide-menu">Super Admin </span>
                            </a>
                        </li>
                        @endif
                        @if (Auth::user()['level'] == 1 || Auth::user()['level'] == 2)
                        <li class="sidebar-item">
                            <a href="{{url('/input-spm')}}" class="sidebar-link">
                                <i class="fas fa-notes-medical"></i>
                                <span class="hide-menu">Admin </span>
                            </a>
                        </li>
                        @endif
                        @if (Auth::user()['level'] == 1 || Auth::user()['level'] == 2 || Auth::user()['level'] == 3)
                        <li class="sidebar-item">
                            <a href="{{url('/input-spm')}}" class="sidebar-link">
                                <i class="fas fa-notes-medical"></i>
                                <span class="hide-menu">Staf Tenaga Kerja </span>
                            </a>
                        </li>
                        @endif
                        
                        <li class="sidebar-item">
                            <a href="{{url('/input-spm')}}" class="sidebar-link">
                                <i class="fas fa-notes-medical"></i>
                                <span class="hide-menu">Warga </span>
                            </a>
                        </li>
                        
                    </ul>
                </nav>
            </div>
        </aside>
@endsection
@section('footer')
<footer class="footer text-center">
    All Rights Reserved by AdminBite admin. Designed and Developed by
    <a href="https://wrappixel.com">WrapPixel</a>.
</footer>
@endsection
@section('footer-jscustomizer')
    <script src="{{asset('adminbite-10/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/app.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/app.init.light-sidebar.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/app-style-switcher.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/waves.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/sidebarmenu.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/custom.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/chartist/dist/chartist.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/extra-libs/c3/d3.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/extra-libs/c3/c3.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/raphael/raphael.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/assets/libs/morris.js/morris.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/dist/js/pages/dashboards/dashboard1.js')}}"></script>
@endsection
@section('custom-bg')
    <script>
        $(function() {
        'use strict';
        $('#main-wrapper').AdminSettings({
            Theme: false, // this can be true or false ( true means dark and false means light ),
            Layout: 'vertical',
            LogoBg: 'skin6', // You can change the Value to be skin1/skin2/skin3/skin4/skin5/skin6
            NavbarBg: 'skin6', // You can change the Value to be skin1/skin2/skin3/skin4/skin5/skin6
            SidebarType: 'full', // You can change it full / mini-sidebar / iconbar / overlay
            SidebarColor: 'skin6', // You can change the Value to be skin1/skin2/skin3/skin4/skin5/skin6
            SidebarPosition: true, // it can be true / false ( true means Fixed and false means absolute )
            HeaderPosition: true, // it can be true / false ( true means Fixed and false means absolute )
            BoxedLayout: false // it can be true / false ( true means Boxed and false means Fluid )
        });
        });
    </script>
@endsection