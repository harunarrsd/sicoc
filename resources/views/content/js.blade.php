@section('js-login')
    <script src="{{asset('adminbite-10/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script>
    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();
    $('#to-recover').on("click", function() {
        $("#loginform").slideUp();
        $("#recoverform").slideDown();
    });
    $('#to-login').on("click", function() {
        $("#recoverform").slideUp();
        $("#loginform").slideDown();
    });
    </script>
@endsection
@section('js-home')
    <script src="{{asset('adminbite-10/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/app.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/app.init.light-sidebar.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/app-style-switcher.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/waves.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/sidebarmenu.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/custom.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/chartist/dist/chartist.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/extra-libs/c3/d3.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/extra-libs/c3/c3.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/raphael/raphael.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/assets/libs/morris.js/morris.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/dist/js/pages/dashboards/dashboard1.js')}}"></script>
@endsection
@section('js-ripel01')
    <script src="{{asset('adminbite-10/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('adminbite-10/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- apps -->
    <script src="{{asset('adminbite-10/dist/js/app.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/app.init.light-sidebar.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/app-style-switcher.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('adminbite-10/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset('adminbite-10/dist/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('adminbite-10/dist/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('adminbite-10/dist/js/custom.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/extra-libs/jqbootstrapvalidation/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }(window, document, jQuery);
    </script>
    <script>
    $('[data-toggle="tooltip"]').tooltip();
    </script>
    <script src="{{asset('adminbite-10/assets/libs/toastr/build/toastr.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/extra-libs/toastr/toastr-init.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
@endsection
    