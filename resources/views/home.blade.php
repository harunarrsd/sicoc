<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    @include('content.head')@include('content.main')@include('content.js')
    @yield('head-home')
    <title>Beranda | Continuum of Care</title>
</head>
<body>
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Beranda</h4>
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Beranda</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Tampilan Awal</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    @if (Auth::user()->level == 1 || Auth::user()->level == 2 || Auth::user()->level == 3 || Auth::user()->level == 4 )
                        <div class="col-lg-12 col-xlg-9 col-md-7">
                            <div class="card">
                                <div class="card-body">
                                    <center class="m-t-30"> <img src="{{asset('adminbite-10/assets/images/users/user.png')}}" class="rounded-circle" width="150" />
                                        <h4 class="card-title m-t-10">
                                            {{ Auth::user()->name }}    
                                        </h4>
                                        <h6 class="card-subtitle">
                                            @if (Auth::user()->level == 1 )
                                                Super Admin
                                            @elseif (Auth::user()->level == 2 )
                                                Admin
                                            @elseif (Auth::user()->level == 3 )
                                                Tenaga Kerja
                                            @elseif (Auth::user()->level == 4 )
                                                Masyarakat
                                            @endif
                                        </h6>
                                        <h6 class="card-subtitle">{{ Auth::user()->email }}</h6>
                                    </center>
                                </div>
                            </div>
                            <div class="card bg-y tx-c">
                            <h1 class="card-title m-t-10">
                                @if (Auth::user()->level == 1 )
                                    Super Admin
                                @elseif (Auth::user()->level == 2 )
                                    Admin
                                @elseif (Auth::user()->level == 3 )
                                    Tenaga Kerja  
                                @elseif (Auth::user()->level == 4 )
                                    Masyarakat
                                @endif    
                            </h1>
                            <h6> Kamu berhasil Masuk sebagai <b>
                                @if (Auth::user()->level == 1 )
                                    Super Admin
                                @elseif (Auth::user()->level == 2 )
                                    Admin
                                @elseif (Auth::user()->level == 3 )
                                    Tenaga Kerja   
                                @elseif (Auth::user()->level == 4 )
                                    Masyarakat
                                @endif    
                            </b></h6>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-home')
</body>
</html>
