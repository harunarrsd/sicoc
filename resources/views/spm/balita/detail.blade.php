<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')@yield('head-ripel01')
<title>Detail Balita</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Balita</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Balita</li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Balita</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('success')}}
                            </div>
                        @endif
                        @if(session()->has('danger'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('danger')}}
                            </div>
                        @endif
                        <div class="card">
                            <div class="row">
                                <div class="card-body p-b-0 m-l-10">
                                    <h4 class="card-title">Riwayat Pelayanan </h4>
                                    <div class="d-flex no-block align-items-center m-b-10">
                                        <div class="m-r-10">
                                            <img src="{{asset('adminbite-10/assets/images/users/user.png')}}" alt="user" class="rounded-circle" width="45">
                                        </div>
                                        <div class="">
                                            <h5 class="m-b-0 font-16 font-medium">{{ $datawarga['nama'] }}</h5>
                                            <span>{{ $datawarga['nik']}}</span>
                                            <form method="POST" id="tambahpel-form" action="{{url('/mulai-catat-balita')}}" style="display: none;">{{ csrf_field() }}
                                                <input type="text" maxlength="16" name="nik" value="">
                                            </form>
                                            <h5 class="card-title m-b-0">
                                                {{ date('d M Y',strtotime($datawarga['tgl_lahir']))}}
                                                <span class="btn waves-effect waves-light btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mulai lahir sejak {{ date('d M Y',strtotime($datawarga['tgl_lahir']))}}">
                                                    Mulai Lahir
                                                </span>
                                            </h5>
                                        </div> 
                                    </div>
                                </div>
                                <div class="card-body col-lg-6 col-md-12">
                                    <h4 class="card-title m-b-0">
                                        Capaian SPM
                                        <span class="btn bg-info text-white btn-outline" data-toggle="tooltip" data-placement="top" title="" data-original-title="">
                                            {{ $capaian_total }}% 
                                        </span>
                                    </h4>                                    
                                    <div class="d-flex flex-row text-center">
                                        <div class="p-10 p-l-0 b-r">
                                            <h6 class="font-light">Periode 1<br><small>0-12 Bulan</small></h6><b>{{ $capaian_all_periode1 }}%</b></div>
                                        <div class="p-10 b-r">
                                            <h6 class="font-light">Periode 2<br><small>13-24 Bulan</small></h6><b>{{ $capaian_all_periode2 }}%</b></div>
                                        <div class="p-10">
                                            <h6 class="font-light">Periode 3<br><small>25-36 Bulan</small></h6><b>{{ $capaian_all_periode3 }}%</b></div>
                                        <div class="p-10">
                                            <h6 class="font-light">Periode 4<br><small>37-48 Bulan</small></h6><b>{{ $capaian_all_periode4 }}%</b></div>
                                        <div class="p-10">
                                            <h6 class="font-light">Periode 5<br><small>49-59 Bulan</small></h6><b>{{ $capaian_all_periode5 }}%</b></div>
                                        
                                    </div>
                                </div>                                
                            </div>
                            <div class="row">
                                <div class="card-body p-b-0 p-t-0">
                                    <div class="d-md-flex align-items-center">
                                        <div>                           
                                            <ul class="nav nav-tabs customtab m-l-10" role="tablist">
                                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home2" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Periode 1</span></a> </li>
                                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile2" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Periode 2</span></a> </li>
                                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages2" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Periode 3</span></a> </li>
                                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#periode4" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Periode 4</span></a> </li>
                                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#periode5" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Periode 5</span></a> </li>
                                            </ul>
                                        </div>
                                        <form method="POST" id="withnik-form3" action="{{url('/mulai-catat-balita')}}" style="display: none;">{{ csrf_field() }}
                                            <input type="text" maxlength="16" name="nik" value="{{ Session::get('withnik3') }}">
                                        </form>
                                        @if($usia_balita["years"] < 5 && $usia_balita["months"] < 59 || $usia_balita["months"] == 59 && $usia_balita["days"] <= 0 && $balita['status_balita'] == "Tahap Pelayanan")
                                        <div class="ml-auto">
                                            <div class="dl">
                                                <div class="btn-group m-r-10">
                                                    <a  href="{{ url('/mulai-catat-balita') }}" onclick="event.preventDefault(); document.getElementById('withnik-form3').submit();">
                                                    <button type="button" class="btn btn-info">                                                    
                                                        <i class="mdi mdi-book-open-page-variant"></i> Tambah Pelayanan
                                                    </button></a>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="home2" role="tabpanel">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="zero_config" class="table table-striped table-bordered display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Tanggal Pelayanan</th>
                                                        <th>Fasilitas Kesehatan</th>
                                                        <th>Tenaga Kesehatan</th>
                                                        <th>Capaian</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $no=1; @endphp
                                                    @foreach($pelayanan_all as $i => $pel)
                                                    @if($periode[$i] == 1)
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>
                                                            {{ date('d M Y',strtotime($pel['tanggal_pelayanan']))}}<br>
                                                            {{ date('h:i:s',strtotime($pel['tanggal_pelayanan']))}}
                                                        </td>
                                                        <td>
                                                            @if($pel['lokasi'] != null && $pel['lokasi'] != "_" && explode('_',$pel['lokasi'])[0] != null ) {{ explode('_',$pel['lokasi'])[0] }} @else - @endif <br>
                                                            <small class="text-muted"> @if($pel['lokasi'] != null && $pel['lokasi'] != "_" && explode('_',$pel['lokasi'])[1] != null ) {{ explode('_',$pel['lokasi'])[1] }} @else - @endif </small>
                                                        </td>
                                                        <td>
                                                            @if($pel['tenaga_kerja'] != null && $pel['tenaga_kerja'] != "_" && explode('_',$pel['tenaga_kerja'])[0] != null ) {{ explode('_',$pel['tenaga_kerja'])[0] }} @else - @endif <br>
                                                            {{-- <small class="text-muted"> @if($pel['tenaga_kerja'] != null && $pel['tenaga_kerja'] != "_" && explode('_',$pel['tenaga_kerja'])[1] != null ) {{ explode('_',$pel['tenaga_kerja'])[1] }} @else - @endif </small> --}}
                                                        </td>
                                                        <td><span class="label label-info">{{ $cap_pel_total[$i]}}%</span></td>
                                                        <td>                                        
                                                            <a href="{{ url('/detail-pelayanan-balita/'.$pel['id']) }}">
                                                                <button type="submit" class="btn btn-info btn-xs">
                                                                    Detail
                                                                </button>
                                                            </a>
                                                            
                                                            <a href="{{ url('/ubah-pelayanan-balita/'.$pel['id']) }}">
                                                                <button type="submit" class="btn btn-warning btn-xs">
                                                                    Ubah
                                                                </button>
                                                            </a>
                                                            
                                                            <a alt="default" class="btn btn-danger btn-xs" href="{{ url('/hapus-pelayanan-balita/'.$pel['id']) }}">
                                                                Hapus
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="profile2" role="tabpanel">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="default_order" class="table table-striped table-bordered display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Tanggal Pelayanan</th>
                                                        <th>Fasilitas Kesehatan</th>
                                                        <th>Tenaga Kesehatan</th>
                                                        <th>Capaian</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $no2=1; @endphp
                                                    @foreach($pelayanan_all as $i => $pel)
                                                    @if($periode[$i] == 2)
                                                    <tr>
                                                        <td>{{ $no2++ }}</td>
                                                        <td>
                                                            {{ date('d M Y',strtotime($pel['tanggal_pelayanan']))}}<br>
                                                            {{ date('h:i:s',strtotime($pel['tanggal_pelayanan']))}}
                                                        </td>
                                                        <td>
                                                            @if($pel['lokasi'] != null && $pel['lokasi'] != "_" && explode('_',$pel['lokasi'])[0] != null ) {{ explode('_',$pel['lokasi'])[0] }} @else - @endif <br>
                                                            <small class="text-muted"> @if($pel['lokasi'] != null && $pel['lokasi'] != "_" && explode('_',$pel['lokasi'])[1] != null ) {{ explode('_',$pel['lokasi'])[1] }} @else - @endif </small>
                                                        </td>
                                                        <td>
                                                            @if($pel['tenaga_kerja'] != null && $pel['tenaga_kerja'] != "_" && explode('_',$pel['tenaga_kerja'])[0] != null ) {{ explode('_',$pel['tenaga_kerja'])[0] }} @else - @endif <br>
                                                            <small class="text-muted"> @if($pel['tenaga_kerja'] != null && $pel['tenaga_kerja'] != "_" && explode('_',$pel['tenaga_kerja'])[1] != null ) {{ explode('_',$pel['tenaga_kerja'])[1] }} @else - @endif </small>
                                                        </td>
                                                        <td><span class="label label-info">{{ $cap_pel_total[$i]}}%</span></td>
                                                        <td>                                        
                                                            <a href="{{ url('/detail-pelayanan-balita/'.$pel['id']) }}">
                                                                <button type="submit" class="btn btn-info btn-xs">
                                                                    Detail
                                                                </button>
                                                            </a>
                                                            
                                                            <a href="{{ url('/ubah-pelayanan-balita/'.$pel['id']) }}">
                                                                <button type="submit" class="btn btn-warning btn-xs">
                                                                    Ubah
                                                                </button>
                                                            </a>
                                                            
                                                            <a alt="default" class="btn btn-danger btn-xs" href="{{ url('/hapus-pelayanan-balita/'.$pel['id']) }}">
                                                                Hapus
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="messages2" role="tabpanel">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="multi_col_order" class="table table-striped table-bordered display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Tanggal Pelayanan</th>
                                                        <th>Fasilitas Kesehatan</th>
                                                        <th>Tenaga Kesehatan</th>
                                                        <th>Capaian</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $no3=1; @endphp
                                                    @foreach($pelayanan_all as $i => $pel)
                                                    @if($periode[$i] == 3)
                                                    <tr>
                                                        <td>{{ $no3++ }}</td>
                                                        <td>
                                                            {{ date('d M Y',strtotime($pel['tanggal_pelayanan']))}}<br>
                                                            {{ date('h:i:s',strtotime($pel['tanggal_pelayanan']))}}
                                                        </td>
                                                        <td>
                                                            @if($pel['lokasi'] != null && $pel['lokasi'] != "_" && explode('_',$pel['lokasi'])[0] != null ) {{ explode('_',$pel['lokasi'])[0] }} @else - @endif <br>
                                                            <small class="text-muted"> @if($pel['lokasi'] != null && $pel['lokasi'] != "_" && explode('_',$pel['lokasi'])[1] != null ) {{ explode('_',$pel['lokasi'])[1] }} @else - @endif </small>
                                                        </td>
                                                        <td>
                                                            @if($pel['tenaga_kerja'] != null && $pel['tenaga_kerja'] != "_" && explode('_',$pel['tenaga_kerja'])[0] != null ) {{ explode('_',$pel['tenaga_kerja'])[0] }} @else - @endif <br>
                                                            <small class="text-muted"> @if($pel['tenaga_kerja'] != null && $pel['tenaga_kerja'] != "_" && explode('_',$pel['tenaga_kerja'])[1] != null ) {{ explode('_',$pel['tenaga_kerja'])[1] }} @else - @endif </small>
                                                        </td>
                                                        <td><span class="label label-info">{{ $cap_pel_total[$i]}}%</span></td>
                                                        <td>                                        
                                                            <a href="{{ url('/detail-pelayanan-balita/'.$pel['id']) }}">
                                                                <button type="submit" class="btn btn-info btn-xs">
                                                                    Detail
                                                                </button>
                                                            </a>
                                                            
                                                            <a href="{{ url('/ubah-pelayanan-balita/'.$pel['id']) }}">
                                                                <button type="submit" class="btn btn-warning btn-xs">
                                                                    Ubah
                                                                </button>
                                                            </a>
                                                            
                                                            <a alt="default" class="btn btn-danger btn-xs" href="{{ url('/hapus-pelayanan-balita/'.$pel['id']) }}">
                                                                Hapus
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="periode4" role="tabpanel">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="lang_comma_deci" class="table table-striped table-bordered display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Tanggal Pelayanan</th>
                                                        <th>Fasilitas Kesehatan</th>
                                                        <th>Tenaga Kesehatan</th>
                                                        <th>Capaian</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $no4=1; @endphp
                                                    @foreach($pelayanan_all as $i => $pel)
                                                    @if($periode[$i] == 4)
                                                    <tr>
                                                        <td>{{ $no4++ }}</td>
                                                        <td>
                                                            {{ date('d M Y',strtotime($pel['tanggal_pelayanan']))}}<br>
                                                            {{ date('h:i:s',strtotime($pel['tanggal_pelayanan']))}}
                                                        </td>
                                                        <td>
                                                            @if($pel['lokasi'] != null && $pel['lokasi'] != "_" && explode('_',$pel['lokasi'])[0] != null ) {{ explode('_',$pel['lokasi'])[0] }} @else - @endif <br>
                                                            <small class="text-muted"> @if($pel['lokasi'] != null && $pel['lokasi'] != "_" && explode('_',$pel['lokasi'])[1] != null ) {{ explode('_',$pel['lokasi'])[1] }} @else - @endif </small>
                                                        </td>
                                                        <td>
                                                            @if($pel['tenaga_kerja'] != null && $pel['tenaga_kerja'] != "_" && explode('_',$pel['tenaga_kerja'])[0] != null ) {{ explode('_',$pel['tenaga_kerja'])[0] }} @else - @endif <br>
                                                            <small class="text-muted"> @if($pel['tenaga_kerja'] != null && $pel['tenaga_kerja'] != "_" && explode('_',$pel['tenaga_kerja'])[1] != null ) {{ explode('_',$pel['tenaga_kerja'])[1] }} @else - @endif </small>
                                                        </td>
                                                        <td><span class="label label-info">{{ $cap_pel_total[$i]}}%</span></td>
                                                        <td>                                        
                                                            <a href="{{ url('/detail-pelayanan-balita/'.$pel['id']) }}">
                                                                <button type="submit" class="btn btn-info btn-xs">
                                                                    Detail
                                                                </button>
                                                            </a>
                                                            
                                                            <a href="{{ url('/ubah-pelayanan-balita/'.$pel['id']) }}">
                                                                <button type="submit" class="btn btn-warning btn-xs">
                                                                    Ubah
                                                                </button>
                                                            </a>
                                                            
                                                            <a alt="default" class="btn btn-danger btn-xs" href="{{ url('/hapus-pelayanan-balita/'.$pel['id']) }}">
                                                                Hapus
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="periode5" role="tabpanel">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="complex_header" class="table table-striped table-bordered display" style="width:100%">
                                            <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Tanggal Pelayanan</th>
                                                        <th>Fasilitas Kesehatan</th>
                                                        <th>Tenaga Kesehatan</th>
                                                        <th>Capaian</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @php $no5=1; @endphp
                                                    @foreach($pelayanan_all as $i => $pel)
                                                    @if($periode[$i] == 5)
                                                    <tr>
                                                        <td>{{ $no5++ }}</td>
                                                        <td>
                                                            {{ date('d M Y',strtotime($pel['tanggal_pelayanan']))}}<br>
                                                            {{ date('h:i:s',strtotime($pel['tanggal_pelayanan']))}}
                                                        </td>
                                                        <td>
                                                            @if($pel['lokasi'] != null && $pel['lokasi'] != "_" && explode('_',$pel['lokasi'])[0] != null ) {{ explode('_',$pel['lokasi'])[0] }} @else - @endif <br>
                                                            <small class="text-muted"> @if($pel['lokasi'] != null && $pel['lokasi'] != "_" && explode('_',$pel['lokasi'])[1] != null ) {{ explode('_',$pel['lokasi'])[1] }} @else - @endif </small>
                                                        </td>
                                                        <td>
                                                            @if($pel['tenaga_kerja'] != null && $pel['tenaga_kerja'] != "_" && explode('_',$pel['tenaga_kerja'])[0] != null ) {{ explode('_',$pel['tenaga_kerja'])[0] }} @else - @endif <br>
                                                            <small class="text-muted"> @if($pel['tenaga_kerja'] != null && $pel['tenaga_kerja'] != "_" && explode('_',$pel['tenaga_kerja'])[1] != null ) {{ explode('_',$pel['tenaga_kerja'])[1] }} @else - @endif </small>
                                                        </td>
                                                        <td><span class="label label-info">{{ $cap_pel_total[$i]}}%</span></td>
                                                        <td>                                        
                                                            <a href="{{ url('/detail-pelayanan-balita/'.$pel['id']) }}">
                                                                <button type="submit" class="btn btn-info btn-xs">
                                                                    Detail
                                                                </button>
                                                            </a>
                                                            
                                                            <a href="{{ url('/ubah-pelayanan-balita/'.$pel['id']) }}">
                                                                <button type="submit" class="btn btn-warning btn-xs">
                                                                    Ubah
                                                                </button>
                                                            </a>
                                                            
                                                            <a alt="default" class="btn btn-danger btn-xs" href="{{ url('/hapus-pelayanan-balita/'.$pel['id']) }}">
                                                                Hapus
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                Hapus
                            </div>
                            <div class="modal-body">
                                Anda yakin ingin menghapus ?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                <a class="btn btn-danger btn-ok text-white">Hapus</a>
                            </div>
                        </div>
                    </div>
                </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>