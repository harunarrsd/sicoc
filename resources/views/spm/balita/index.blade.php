<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>SPM Balita</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Balita</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active" aria-current="page">Standar Pelayanan Minimal</li>                                
                                    <li class="breadcrumb-item active" aria-current="page">Balita</li>
                                    <li class="breadcrumb-item active" aria-current="page">SPM Balita</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
                <div class="card-body bg-light">
                    <div class="d-md-flex align-items-center">
                        <div><h2>Pelayanan Balita</h2></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Penyataan Standar</h4>
                            Setiap balita mendapatkan pelayanan kesehatan sesuai standar.
                            Pemerintah Daerah Kabupaten/Kota wajib memberikan pelayanan
                            kesehatan anak balita kepada semua balita di wilayah kerjanya
                            dalam kurun waktu satu tahun.
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Pelayanan Kesehatan Balita</h4>
                            Pelayanan kesehatan balita sesuai standar adalah pelayanan
                            kesehatan yang diberikan kepada anak berusia 0-59 bulan dan
                            dilakukan oleh Bidan dan atau Perawat dan atau Dokter/DLP
                            dan atau Dokter Spesialis Anak yang memiliki Surat Tanda
                            Register (STR) dan diberikan di fasilitas kesehatan pemerintah
                            maupun swasta, dan UKBM.
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Pelayanan kesehatan, meliputi : </h4>
                            <p> a) Penimbangan minimal 8 kali setahun, pengukuran
                                panjang/tinggi badan minimal 2 kali setahun
                                b) Pemberian kapsul vitamin A 2 kali setahun.
                                c) Pemberian imunisasi dasar lengkap.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Definisi Operasional Capaian Kerja</h4>
                            Capaian Kinerja Pemerintah Daerah Kabupaten/Kota dalam
                            memberikan pelayanan kesehatan balita usia 0-59 bulan dinilai dari
                            cakupan balita yang mendapat pelayanan kesehatan balita sehat
                            sesuai standar di wilayah kerjanya dalam kurun waktu satu tahun.
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4><i class="fas fa-chevron-circle-right m-r-10 m-b-10"></i> Referensi</h4>
                    <h6><i class="fas fa-square"></i> Keputusan Menteri Kesehatan Nomor 284/MENKES/SK/III/2004 tentang Buku Kesehatan Ibu dan Anak;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 1464/X/Menkes/2010 tentang Izin dan Penyelenggaraan Praktik Bidan;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 17 Tahun 2013 tentang Praktik Keperawatan;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 42 Tahun 2013 tentang Penyelenggaraan Imunisasi;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 25 Tahun 2014 tentang Upaya Kesehatan Anak;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 56 Tahun 2014 tentang Klasifikasi dan Perizinan Rumah Sakit;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 75 Tahun 2014 tentang Pusat Kesehatan Masyarakat;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Konsil Kedokteran Indonesia Nomor 11 Tahun 2012 tentang Standar Kompetensi Dokter Indonesia;</h6>
                    <h6><i class="fas fa-square"></i> Pedoman Pelaksanaan Stimulasi, Deteksi dan Intervensi Dini Tumbuh Kembang Anak Ditingkat Pelayanan Kesehatan Dasar. </h6>                    
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>