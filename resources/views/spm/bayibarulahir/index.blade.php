<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>SPM Bayi Baru Lahir</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Bayi Baru Lahir</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active" aria-current="page">Standar Pelayanan Minimal</li>                                
                                    <li class="breadcrumb-item active" aria-current="page">Bayi Baru Lahir</li>
                                    <li class="breadcrumb-item active" aria-current="page">SPM Bayi Baru Lahir</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
                <div class="card-body bg-light">
                    <div class="d-md-flex align-items-center">
                        <div><h2>Pelayanan Bayi Baru Lahir</h2></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Penyataan Standar</h4>
                            Setiap bayi baru lahir mendapatkan pelayanan kesehatan sesuai standar.
                            Pemerintah Daerah Kabupaten/Kota wajib memberikan pelayanan
                            kesehatan bayi baru lahir kepada semua bayi di wilayah kerjanya
                            dalam kurun waktu satu tahun.
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Pelayanan Kesehatan Bayi Lahir</h4>
                            Pelayanan kesehatan bayi baru lahir sesuai standar adalah
                            pelayanan yang diberikan pada bayi usia 0-28 hari dan
                            mengacu kepada Pelayanan Neonatal Esensial sesuai yang
                            tercantum dalam Peraturan Menteri Kesehatan Nomor 25 Tahun
                            2014 tentang Upaya Kesehatan Anak, dilakukan oleh Bidan
                            dan atau perawat dan atau Dokter dan atau Dokter Spesialis
                            Anak yang memiliki Surat Tanda Register (STR).
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Fasilitas Pelayanan Kesehatan</h4>
                            <p>Pelayanan dilakukan di fasilitas pelayanan kesehatan (Polindes,
                                Poskesdes, Puskesmas, Bidan praktek swasta, klinik pratama,
                                klinik utama, klinik bersalin, balai kesehatan ibu dan anak,
                                rumah sakit pemerintah maupun swasta), Posyandu dan atau
                                kunjungan rumah.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Definisi Operasional Capaian Kerja</h4>
                            Capaian kinerja Pemerintah Daerah Kabupaten/Kota dalam
                            memberikan paket pelayanan kesehatan bayi baru lahir dinilai dari
                            persentase jumlah bayi baru lahir usia 0-28 hari yang mendapatkan
                            pelayanan kesehatan bayi baru lahir sesuai standar di wilayah
                            kabupaten/kota tersebut dalam kurun waktu satu tahun.
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4><i class="fas fa-chevron-circle-right m-r-10 m-b-10"></i> Referensi</h4>
                    <h6><i class="fas fa-square"></i> Peraturan Pemerintah Nomor 33 Tahun 2012 tentang Pemberian Air Susu Ibu Ekslusif;</h6>
                    <h6><i class="fas fa-square"></i> Keputusan Menteri Kesehatan Nomor 284/MENKES/SK/III/2004 tentang Buku Kesehatan Ibu dan Anak;</h6>
                    <h6><i class="fas fa-square"></i> Permenkes Nomor 1464/X/Menkes/2010 tentang Izin dan Penyelenggaraan Praktik Bidan;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 15 Tahun 2013 tentang Tata Cara Penyediaan Fasilitas Khusus Menyusui dan/atau Memerah Air Susu Ibu;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 17 Tahun 2013 tentang Praktik Keperawatan;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 39 Tahun 2013 tentang Susu Formula Bayi dan Produk Bayi Lainnya;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 42 Tahun 2013 tentang Penyelenggaraan Imunisasi;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 15 Tahun 2014 tentang Tata Cara Pengenaan Sanksi Administratif Bagi Tenaga Kesehatan, Penyelenggara Fasilitas Pelayanan Kesehatan, Penyelenggara Satuan Pendidikan Kesehatan, Pengurus Organisasi Profesi di Bidang Kesehatan, Serta Produsen dan Distributor Susu Formula Bayi dan/atau Produk Bayi Lainnya yang Dapat Menghambat Keberhasilan Program Pemberian Air Susu Ibu Eksklusif;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 25 Tahun 2014 tentang Upaya Kesehatan Anak;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 56 Tahun 2014 tentang Klasifikasi dan Perizinan Rumah Sakit;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 75 Tahun 2014 tentang Pusat Kesehatan Masyarakat;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Konsil Kedokteran Indonesia Nomor 11 Tahun 2012 tentang Standar Kompetensi Dokter Indonesia.</h6>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>