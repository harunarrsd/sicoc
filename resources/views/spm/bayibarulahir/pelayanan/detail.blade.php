<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')@yield('head-ripel01')
<title>Detail Pelayanan Bayi Baru Lahir</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Bayi Baru Lahir</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Bayi Baru Lahir</li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Pelayanan Bayi Baru Lahir</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-md-flex align-items-center">
                                    <div>
                                        <h4 class="card-title">Detail Pelayanan Bayi Baru Lahir</h4>
                                        <h6 class="card-subtitle">Standar Pelayanan Minimal</h6>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="dl">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive m-t-15">
                                    <table id="" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Kode Pelayanan</th>
                                                <th>Nama Bayi </th>
                                                <th>Tanggal Pelayanan</th>
                                                <th>Neonatal Esensial</th>
                                                <th>Pelayanan Skrining</th>
                                                <th>Pemberian Informasi</th>
                                                <th>Capaian Pelayanan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $pelayanan['id'] }}</td>
                                                <td>{{ $databayi['nama'] }}</td>
                                                <td>{{ date("d M Y",strtotime($databayi->tgl_lahir))  }}</td>
                                                <td>{{ number_format($cap_neo,2) }}%</td>
                                                <td>{{ number_format($cap_skr,2) }}%</td>
                                                <td>{{ number_format($cap_info,2) }}%</td>
                                                <td>
                                                    <span class="label label-info">
                                                        {{ number_format($capaian,2) }}%
                                                    </span>
                                                </td>
                                                
                                            </tr>
                                        </tbody>                                       
                                    </table>
                                </div>
                                <div class="card-body p-0 m-t-20">
                                        <h4 class="card-title">Neoesensial</h4>
                                        <div class="table-responsive">
                                            <table id="" class="table table-striped table-bordered display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>Suhu Bayi</th>
                                                        @if($form== "pertama")
                                                            <th>Inisiasi Menyusu Dini</th>
                                                            <th>Suntikan Vitamin K1</th>
                                                            <th>Perawatan Tali Pusat</th>                                                        
                                                            <th>Salep Mata Antibiotik</th>
                                                            <th>Imunisasi Hepatitis B0</th>
                                                            <th>Pemantauan Tanda Bahaya</th>
                                                            <th>Penanganan Asfiksia Bayi</th>
                                                            <th>Pemberian Tanda Identitas</th>
                                                        @elseif($form == "kedua")
                                                            <th>Perawatan Tali Pusat</th>
                                                            <th>Metode Kanguru</th>
                                                            <th>Vit. K1 Profilaksis</th>                                                        
                                                            <th>Pemeriksaan Imunisasi</th>
                                                            <th>Penanganan Bayisakit</th>
                                                            <th>Penanganan Kelainan</th>
                                                        @endif
                                                        <th>Pemeriksaan Fisik</th>
                                                    </tr> 
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>@if($pelayanan['suhu_tubuh'] != null){{ $pelayanan['suhu_tubuh']}} @else 0 @endif <sup>o</sup>C <br>  @if($capaian0 == 1) <small class="text-muted"> (Hangat) </small> @else <small class="text-muted"> (Tidak Hangat) </small> @endif</td>
                                                        @if($form == "pertama")
                                                            <td> @if($capaian1 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                            <td> @if($capaian2 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                            <td> @if($capaian3 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                            <td> @if($capaian4 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                            <td> @if($capaian5 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                            <td> @if($capaian6 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                            <td> @if($capaian7 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                            <td> @if($capaian8 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                        @elseif($form == "kedua")
                                                            <td> @if($capaian9 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                            <td> @if($capaian10 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                            <td> @if($capaian11 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                            <td> @if($capaian12 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                            <td> @if($capaian13 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                            <td> @if($capaian14 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                        @endif
                                                        <td> @if($capaian15 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>                                                        
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                </div>    
                                <div class="card-body p-0 m-t-20">
                                        <h4 class="card-title">Skrining</h4>
                                        <div class="table-responsive">
                                            <table id="" class="table table-striped table-bordered display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>Fasilitas Kesahatan</th>
                                                        <th>Tenaga Kesehatan </th>
                                                        <th>Hipotiroid Kongenital</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            @if(explode('_',$pelayanan['lokasi'])[0] != null) {{ explode('_',$pelayanan['lokasi'])[0] }} @else - @endif <br>
                                                            @if(explode('_',$pelayanan['lokasi'])[1] != null) <small class="text-muted"> {{ explode('_',$pelayanan['lokasi'])[1] }} </small> @else - @endif
                                                        </td>
                                                        <td>
                                                            @if(explode('_',$pelayanan['tenaga_kerja'])[0] != null) {{ explode('_',$pelayanan['tenaga_kerja'])[0] }} @else - @endif <br>
                                                            @if(explode('_',$pelayanan['tenaga_kerja'])[1] != null) <small class="text-muted"> {{ explode('_',$pelayanan['tenaga_kerja'])[1] }} </small> @else - @endif 
                                                        </td>
                                                        <td>
                                                            @if($pelayanan['skr_shk_hsd'] == "y") Positif <br> <small class="text-muted"> (Hasil Semple Darah) </small>  @elseif($pelayanan['skr_shk_hsd'] == "t") Negatif <br> <small class="text-muted"> (Hasil Semple Darah) </small> @else - @endif <br>
                                                             
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                </div>    
                                <div class="card-body p-0 m-t-20">
                                        <h4 class="card-title">Pemberian Informasi</h4>
                                        <div class="table-responsive">
                                            <table id="" class="table table-striped table-bordered display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>Pemberi Informasi</th>
                                                        <th>Pemberian KIE Perawatan Bayi</th>
                                                        <th>Pemberian KIE Skrining Bayi</th>
                                                        <th>Pemberian KIE Tanda Bahaya</th>
                                                        <th>Pemberian KIE Pelayanan</th>
                                                        <th>Pemberian KIE ASI Eksklusif</th>
                                                        <th>Media Informasi</th> 
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>@if($pelayanan['kie_nama'] != null) {{$pelayanan['kie_nama']}}  @else - @endif</td>
                                                        <td> @if($capaian20 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                        <td> @if($capaian21 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                        <td> @if($capaian22 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                        <td> @if($capaian23 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                        <td> @if($capaian24 == 1) <span class="label label-info"> Sudah </span> @else <span class="label label-danger"> Belum </span> @endif</td>
                                                        <td>
                                                            @if($pelayanan['kie_media'] == "y") Buku KIA <small class="text-muted"> (Standar) </small> @elseif($pelayanan['kie_media'] == "t") Lainnya @else - @endif <br>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>