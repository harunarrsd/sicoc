<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>Catat Pelayanan Bayi Baru Lahir</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.date.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.time.css')}}">
    <link type="text/css" href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
            <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Bayi Baru Lahir</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Bayi Baru Lahir</li>
                                    <li class="breadcrumb-item active" aria-current="page">Catat Pelayanan</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="col-12 p-0">
                @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('success')}}
                            </div>
                        @endif
                        @if(session()->has('danger'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('danger')}}
                            </div>
                        @endif
                    <div class="card">
                        <div class="card-body wizard-content">
                            <div class="row">
                                <div class="col-lg-7 col-xlg-7 col-md-6 col-sm-12 p-b-0">
                                    <div class="card-body m-0">
                                        <h4 class="card-title">Catat Pelayanan Bayi Baru Lahir </h4>
                                        <h6 class="card-subtitle">Pelayanan Bayi Baru Lahir sesuai SPM</h6>
                                    </div> 
                                </div>
                                <div class="col-lg-3 col-xlg-3 col-md-3 col-sm-12">
                                    <h5 class="m-b-0 font-16 font-medium">{{ $data['nama'] }}</h5>
                                    <span>NIK Ibu : {{ $data['nik_ibu'] }}</span>
                                    <h5 class="card-title m-b-0">
                                        {{ date('d M Y', strtotime($data->tgl_lahir)) }}
                                        <span class="btn waves-effect waves-light btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mulai lahir sejak {{ date('d M Y', strtotime($data->tgl_lahir)) }}">
                                            Mulai Lahir
                                        </span>
                                    </h5> 
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2 col-sm-12">
                                    <div class="card-title">    
                                        <h5>Capaian SPM</h5>
                                        <span class="btn bg-info text-white btn-outline" data-toggle="tooltip" data-placement="top" title="" data-original-title="% dan total T dari 40T">
                                            100% 
                                            <!-- {{ $umur }} -->
                                        </span>                                                           
                                    </div>
                                </div>
                            </div>
                            <form id="wizard-form" method="POST"  action="{{url('/simpan-catat-bayi-baru-lahir')}}" class="validation-wizard wizard-circle m-t-0">
                                @csrf
                                <!-- Step 0 -->
                                <h6>Informasi Pelayanan</h6>
                                <section>
                                    <div class="row">
                                        <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                            <label>Tanggal Pelayanan</label> <span class="text-danger">*</span>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                                </div>
                                                <input type="date" class="form-control required pickadate-disable" name="tanggal_pelayanan"/>
                                                <input type="text" class="form-control" name="id" value="{{ $request_id }}" style="display:none;"/>
                                                <input type="text" class="form-control" name="created_by" value="{{ Auth::user()['name'] }}" style="display:none;"/>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                            <label>Waktu Pelayanan</label> <span class="text-danger">*</span>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                                </div>
                                                <input id="timepicker" class="form-control required" name="waktu_pelayanan" placeholder="Pilih"/>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <!-- Step 1 -->
                                <h6>Neonatal Esensial</h6>
                                <section>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Suhu Tubuh Bayi -- Menjaga Bayi Hangat </label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="ti-user"></i></span>
                                                    </div>
                                                    <input type="number" name="suhu_tubuh1" class="form-control" placeholder="Co : 67">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">,</span>
                                                    </div>
                                                    <input type="number" name="suhu_tubuh2" class="form-control" placeholder="Co : 00">
                                                    <input type="text" name="form" class="form-control" value="{{ $form }}" style="display:none;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><sup>o</sup>C</span>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($form == "pertama")
                                                <span class="text-danger"> ** Pelayanan neonatal esensial 0 sampai 6 jam</span>
                                                <div class="row">
                                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                                        <label> Inisiasi Menyusu Dini </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                            <input type="checkbox" name="imd" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                                        <label> Suntikan Vitamin K1 </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                            <input type="checkbox" name="svk1" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                                        <label> Perawatan Tali Pusat </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                            <input type="checkbox" name="ptp" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                                        <label> Salep Mata Antibiotik </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                            <input type="checkbox" name="sma" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                                        <label> Imunisasi Hepatitis B0 </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                            <input type="checkbox" name="ihb0" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                                        <label> Pemantauan Tanda Bahaya </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                            <input type="checkbox" name="ptb" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                                        <label> Penanganan Asfiksia Bayi </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                            <input type="checkbox" name="pab" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                                        <label> Pemberian Tanda Identitas </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                            <input type="checkbox" name="pti" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        </div>
                                                    </div>
                                                </div>
                                            @elseif($form == "kedua")
                                                <span class="text-danger"> ** Pelayanan neonatal esensial 6 jam sampai 28 hari</span>
                                                <div class="row">
                                                    <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                        <label> Perawatan Tali Pusat </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                            <input type="checkbox" name="ptpusat" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                        <label> Perawatan Metode Kanguru </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                            <input type="checkbox" name="pmk" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                        <label> Status Vitamin K1 Profilaksis </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                            <input type="checkbox" name="psvk1p" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                        <label> Pemeriksaan Imunisasi </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                            <input type="checkbox" name="imun" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        </div>
                                                    </div>    
                                                    <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                        <label> Penanganan Bayi Baru Lahir sakit </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                            <input type="checkbox" name="pbbls" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                        <label> Penanganan Kelainan Bawaan </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                            <input type="checkbox" name="pkb" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-6">
                                            
                                        
                                        </div>
                                    </div>
                                </section>
                                <!-- Step 2 -->
                                <h6>Pemeriksaan Fisik </h6>
                                <section>
                                    <div class="row">
                                        <div class="col-6">
                                            <h5>Pemeriksaan Apgar</h5>
                                            <div class="row">
                                                <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label> Aktivitas Otot </label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ti-user"></i></span>
                                                        </div>
                                                        <select name="fisik_apgar_otot" class="form-control" aria-invalid="true">
                                                            <option value="0">Pilih</option>
                                                            <option value="2">Gerakan Aktif</option>
                                                            <option value="1">Hanya Gerakan Kaki dan Tangan</option>
                                                            <option value="0">Tidak Ada Gerakan</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label> Denyut Jantung </label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ti-user"></i></span>
                                                        </div>
                                                        <input type="number" name="fisik_apgar_jantung" class="form-control" placeholder="Co : 120">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">x/menit</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label> Respon terhadap Ransangan </label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ti-user"></i></span>
                                                        </div>
                                                        <select name="fisik_apgar_respon" class="form-control" aria-invalid="true">
                                                            <option value="0">Pilih</option>
                                                            <option value="2">Bayi Meringis dan Menarik Diri, Batuk atau Menangis</option>
                                                            <option value="1">Hanya Meringis</option>
                                                            <option value="0">Tidak Ada Respon</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label> Warna Tubuh </label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ti-user"></i></span>
                                                        </div>
                                                        <select name="fisik_apgar_warna" class="form-control" aria-invalid="true">
                                                            <option value="0">Pilih</option>
                                                            <option value="2">Seluruhnya Normal</option>
                                                            <option value="1">Warna Tangan dan atau Kaki Kebiruan</option>
                                                            <option value="0">Seluruhnya abu-abu kebiruan atau Pucat</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label> Pernapasan </label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="ti-user"></i></span>
                                                    </div>
                                                    <select name="fisik_apgar_napas" class="form-control" aria-invalid="true">
                                                        <option value="0">Pilih</option>
                                                        <option value="2">Menangis Kuat dan bernafas secara Normal</option>
                                                        <option value="1">Menangis Lemah, Terkadag Merintih dan disertai Pernapasan tidak Teratur</option>
                                                        <option value="0">Tidak Bernafas</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <h5>Pemeriksaan Gestasional</h5>
                                            <div class="row">
                                                <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                                    <label>Berat Badan</label> 
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ti-user"></i></span>
                                                        </div>
                                                        <input type="number" name="fisik_ges_berat" class="form-control" placeholder="0" max="200">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">kg</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                                    <label>Panjang Tubuh</label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" ><i class="ti-user"></i></span>
                                                        </div>
                                                        <input type="number" name="fisik_ges_panjang" class="form-control" placeholder="0" max="200">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">cm</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                                    <label>Lingkar Kepala</label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ti-user"></i></span>
                                                        </div>
                                                        <input type="number" name="fisik_ges_lingkarKepala" class="form-control" placeholder="0" max="200">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">cm</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h5>Pemeriksaan Kepala dan Leher</h5>
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Bentuk Kepala </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="fisik_kl_kepala" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Bentuk Leher </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="fisik_kl_leher" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Bentuk Mata </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="fisik_kl_mata" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Bentuk Hidung </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="fisik_kl_hidung" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Bentuk Telinga </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="fisik_kl_telinga" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <h5>Pemeriksaan Mulut</h5>
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Kondisi Gusi </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="fisik_mulut_gusi" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Kondisi Langit - langit mulut </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="fisik_mulut_langit" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    </div>
                                                </div>
                                            </div>
                                            <h5>Pemeriksaan Perut dan Kelamin</h5>
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Bentuk Perut </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="fisik_pk_bentukPerut" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Lingkar Perut </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="fisik_pk_lingkarPerut" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Organ dalam Perut </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="fisik_pk_organPerut" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Organ Kelamin </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="fisik_pk_organKelamin" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    </div>
                                                </div>
                                            </div>
                                            <h5>Pemeriksaan Tulang Belakang, Tangan, dan Kaki</h5>
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Tulang Belakang </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="fisik_tbtk_tulangbel" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Kondisi Tangan </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="fisik_tbtk_tangan" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Kondisi Kaki </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="fisik_tbtk_kaki" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <!-- Step 3 -->
                                <h6>Skrining</h6>
                                <section>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label>Fasilitas Kesahatan</label> <span class="text-danger">*</span>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon11"><i class="ti-location-pin"></i></span>
                                                        </div>
                                                        <select name="s_fasilitas_kesehatan" id="select" class="form-control required" aria-invalid="true">
                                                            <option value="">Pilih</option>
                                                            <option value="Rumah Sakit">Rumah Sakit</option>
                                                            <option value="Puskesmas">Puskesmas</option>
                                                            <option value="Kilnik">Kilnik</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label>Lokasi Tempat Pelayanan</label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon11"><i class="ti-map-alt"></i></span>
                                                        </div>
                                                        <input type="text" name="s_lokasi_pelayanan" class="form-control" placeholder="Nama RS / Nama Jalan">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label>Tenaga Kesehatan</label> <span class="text-danger">*</span>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                        </div>
                                                        <select name="s_tenaga_kesehatan" id="select" class="form-control required" aria-invalid="true">
                                                            <option value="">Pilih</option>
                                                            <option value="Dokter">Dokter</option>
                                                            <option value="Bidan">Bidan</option>
                                                            <option value="Dokter Spesialis">Dokter Spesialis</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label>Nama Dokter/Bidan</label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                        </div>
                                                        <input type="text" name="s_nama_stk" class="form-control" placeholder="Co : dr. Ahmad Riza">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <h5>Skrining Hipotiroid Kongenital</h5>
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Hasil Sampel Darah </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="s_shk_hsd" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Positif" data-off-text="Negatif">
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> 
                                                        Jika menunjukkan hasil positif, pengobatan harus dilakukan
                                                        sebelum Bayi berusia 1 bulan.  
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <!-- Step 4 -->
                                <h6>Pemberian Informasi</h6>
                                <section>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Nama Tenaga Kesehatan Pemberi Informasi</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="ti-user"></i></span>
                                                    </div>
                                                    <input type="text" name="kie_nama_pi" class="form-control" placeholder="Co : Suster Rini A.">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Informasi perawatan Bayi </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="kie_perawatan" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Diinformasikan" data-off-text="Belum">
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Informasi skrining Bayi </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="kie_skrining" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Diinformasikan" data-off-text="Belum">
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Informasi tanda bahaya pada Bayi </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="kie_bahaya" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Diinformasikan" data-off-text="Belum">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Informasi pelayanan kesehatan pada Bayi </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="kie_pelayanan" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Diinformasikan" data-off-text="Belum">
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Informasi ASI Eksklusif </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="kie_asi" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Diinformasikan" data-off-text="Belum">
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Media memberikan Informasi</label>
                                                    <div class="input-group mb-3 bt-switch">
                                                        <input type="checkbox" name="kie_media" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Buku KIA" data-off-text="Lainnya">
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>
                                </section>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <!-- Picker Date Style -->
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.date.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.time.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/legacy.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <!-- <script src="{{asset('adminbite-10/assets/libs/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/datetimepicker/datetimepicker.init.js')}}"></script> -->
    
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js')}}"></script>
    <!-- Switch Style -->
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/js/bootstrap-switch.min.js')}}"></script>
    <!-- Wizard Style -->
    <script src="{{asset('adminbite-10/assets/libs/jquery-steps/build/jquery.steps.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script>
    //Basic Example
    $("#example-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true
    });

    // Basic Example with form
    var form = $("#example-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password"
            }
        }
    });
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    });

    // Advance Example

    var form = $("#example-advanced-form").show();

    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            // Forbid next action on "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age-2").val()) < 18) {
                return false;
            }
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onStepChanged: function(event, currentIndex, priorIndex) {
            // Used to skip the "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
                form.steps("next");
            }
            // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3) {
                form.steps("previous");
            }
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password-2"
            }
        }
    });

    // Dynamic Manipulation
    $("#example-manipulation").steps({
        headerTag: "h3",
        bodyTag: "section",
        enableAllSteps: true,
        enablePagination: false
    });

    //Vertical Steps

    $("#example-vertical").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        stepsOrientation: "vertical"
    });

    //Custom design form example
    $(".tab-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onFinished: function(event, currentIndex) {
            document.getElementById('wizard-form').submit();

        }
    });


    var form = $(".validation-wizard").show();

    $(".validation-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onStepChanging: function(event, currentIndex, newIndex) {
            return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
        },
        onFinishing: function(event, currentIndex) {
            return form.validate().settings.ignore = ":disabled", form.valid()
        },
        onFinished: function(event, currentIndex) {
            document.getElementById('wizard-form').submit();
        }
    }), $(".validation-wizard").validate({
        ignore: "input[type=hidden]",
        errorClass: "text-danger",
        successClass: "text-success",
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element)
        },
        rules: {
            email: {
                email: !0
            }
        }
    })
    </script>
    <!-- Switch Style     -->
    <script>
    $(".bt-switch input[type='checkbox'], .bt-switch input[type='radio']").bootstrapSwitch();
    var radioswitch = function() {
        var bt = function() {
            $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioState")
            }), $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck")
            }), $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck", !1)
            })
        };
        return {
            init: function() {
                bt()
            }
        }
    }();
    $(document).ready(function() {
        init()
    });
    </script>
    <script>
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    $('#date-fr').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', lang: 'fr', weekStart: 1, cancelText: 'ANNULER' });
    $('#date-end').bootstrapMaterialDatePicker({ weekStart: 0 });
    $('#date-start').bootstrapMaterialDatePicker({ weekStart: 0 }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });
    </script>
    <script type="text/javascript">
        $('.pickadate-disable').pickadate({
            disable: [
                @foreach($tgl_pel as $i => $tgl)
                    [{{ explode("-",$tgl)[0] }}, {{ explode("-",$tgl)[1] }}-1, {{ explode("-",$tgl)[2] }}],
                @endforeach
                1, 7
            ]
        });
    </script>
</body>
</html>