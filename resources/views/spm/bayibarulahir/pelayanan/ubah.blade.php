<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>Ubah Pelayanan Bayi Baru Lahir</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.date.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.time.css')}}">
    <link type="text/css" href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
            <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Bayi Baru Lahir</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Bayi Baru Lahir</li>
                                    <li class="breadcrumb-item active" aria-current="page">Ubah Pelayanan</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="col-12 p-0">
                    <div class="card">
                        <div class="card-body wizard-content">
                            <div class="row">
                                <div class="col-lg-7 col-xlg-7 col-md-6 col-sm-12 p-b-0">
                                    <div class="card-body m-0">
                                        <h4 class="card-title">Ubah Pelayanan Bayi Baru Lahir </h4>
                                        <h6 class="card-subtitle">Pelayanan Bayi Baru Lahir sesuai SPM</h6>
                                    </div> 
                                </div>
                                <div class="col-lg-3 col-xlg-3 col-md-3 col-sm-12">
                                    <h5 class="m-b-0 font-16 font-medium">{{ $databayi['nama']}}</h5>
                                    <span>{{$databayi['nik']}}</span>
                                    <h5 class="card-title m-b-0">
                                        {{ date("d M Y",strtotime($databayi->tgl_lahir)) }}
                                        <span class="btn waves-effect waves-light btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mulai lahir sejak {{ date('d M Y',strtotime($databayi->tgl_lahir)) }}">
                                            Mulai Lahir
                                        </span>
                                    </h5> 
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2 col-sm-12">
                                    <div class="card-title">    
                                        <h5>Capaian SPM</h5>
                                        <span class="btn bg-info text-white btn-outline" data-toggle="tooltip" data-placement="top" title="" data-original-title="">
                                            {{ number_format($total_capaian,2) }}% 
                                        </span>                                                           
                                    </div>
                                </div>
                            </div>
                            <form id="wizard-form" method="POST"  action="{{url('/edit-pelayanan-bayi-baru-lahir')}}" class="validation-wizard wizard-circle m-t-0">
                                @csrf
                                <!-- Step 0 -->
                                <h6>Informasi Pelayanan</h6>
                                <section>
                                    <div class="row">
                                        <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                            <label>Tanggal Pelayanan</label> <span class="text-danger">*</span>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                                </div>
                                                <input type="date" class="form-control required pickadate-disable" name="tanggal_pelayanan" placeholder="{{ explode(' ',$pelayanan['tanggal_pelayanan'])[0]}}"/>
                                                <input type="text" name="id" value="{{ $pelayanan['id'] }}" style="display:none;"/>
                                                <input type="text" name="form" value="{{ $form }}" style="display:none;"/>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                            <label>Waktu Pelayanan</label> <span class="text-danger">*</span>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                                </div>
                                                <input id="timepicker" class="form-control required" placeholder="{{ explode(' ',$pelayanan['tanggal_pelayanan'])[1] }}" name="waktu_pelayanan" placeholder="Pilih"/>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <!-- Step 1 -->
                                <h6>Neonatal Esensial</h6>
                                <section>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Suhu Tubuh Bayi -- Menjaga Bayi Hangat </label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="ti-user"></i></span>
                                                    </div>
                                                    @if($pelayanan['suhu_tubuh'] != null && explode(',',$pelayanan['suhu_tubuh'])[0] != null)
                                                    <input type="number" name="suhu_tubuh1" class="form-control" placeholder="Co : 67" value="{{ explode(',',$pelayanan['suhu_tubuh'])[0] }}">
                                                    @else
                                                    <input type="number" name="suhu_tubuh1" class="form-control" placeholder="Co : 67" value="00">
                                                    @endif
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">,</span>
                                                    </div>
                                                    @if($pelayanan['suhu_tubuh'] != null && explode(',',$pelayanan['suhu_tubuh'])[1] != null)  
                                                       <input type="number" name="suhu_tubuh2" class="form-control" placeholder="Co : 00" value="{{explode(',',$pelayanan['suhu_tubuh'])[1]}}">
                                                    @else
                                                        <input type="number" name="suhu_tubuh2" class="form-control" placeholder="Co : 00" value="00">
                                                    @endif
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><sup>o</sup>C</span>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($form == "pertama")
                                                <span class="text-danger"> ** Pelayanan neonatal esensial 0 sampai 6 jam</span>
                                                <div class="row">
                                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                                        <label> Inisiasi Menyusu Dini </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                            @if($capaian1 == 1)
                                                                <input type="checkbox" name="imd" checked data-size="small"  data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                            @else
                                                                <input type="checkbox" name="imd" data-size="small"  data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                                        <label> Suntikan Vitamin K1 </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                        @if($capaian2 == 1)
                                                            <input type="checkbox" name="svk1" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @else
                                                            <input type="checkbox" name="svk1" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                                        <label> Perawatan Tali Pusat </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                        @if($capaian3 == 1)
                                                            <input type="checkbox" name="ptp" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @else
                                                            <input type="checkbox" name="ptp" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                                        <label> Salep Mata Antibiotik </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                        @if($capaian4 == 1)
                                                            <input type="checkbox" name="sma" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @else
                                                            <input type="checkbox" name="sma" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                                        <label> Imunisasi Hepatitis B0 </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                        @if($capaian5 == 1)
                                                            <input type="checkbox" name="ihb0" checked data-size="small"  data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @else
                                                            <input type="checkbox" name="ihb0" data-size="small"  data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @endif    
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                                        <label> Pemantauan Tanda Bahaya </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                        @if($capaian6 == 1)
                                                            <input type="checkbox" name="ptb" checked data-size="small"  data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @else
                                                            <input type="checkbox" name="ptb" data-size="small"  data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                                        <label> Penanganan Asfiksia Bayi </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                        @if($capaian7 == 1)
                                                            <input type="checkbox" name="pab" checked data-size="small"  data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @else
                                                            <input type="checkbox" name="pab" data-size="small"  data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                                        <label> Pemberian Tanda Identitas </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                        @if($capaian8 == 1)
                                                            <input type="checkbox" name="pti" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @else
                                                            <input type="checkbox" name="pti" data-size="small"  data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @elseif($form == "kedua")
                                                <span class="text-danger"> ** Pelayanan neonatal esensial 6 jam sampai 28 hari</span>
                                                <div class="row">
                                                    <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                        <label> Perawatan Tali Pusat </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                        @if($capaian9 == 1)
                                                            <input type="checkbox" name="ptpusat" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @else
                                                            <input type="checkbox" name="ptpusat" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                        <label> Perawatan Metode Kanguru </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                        @if($capaian10 == 1)
                                                            <input type="checkbox" name="pmk" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @else
                                                            <input type="checkbox" name="pmk" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                        <label> Status Vitamin K1 Profilaksis </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                        @if($capaian11 == 1)
                                                            <input type="checkbox" name="psvk1p" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @else
                                                            <input type="checkbox" name="psvk1p" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                        <label> Pemeriksaan Imunisasi </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                        @if($capaian12 == 1)
                                                            <input type="checkbox" name="imun" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @else
                                                            <input type="checkbox" name="imun" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @endif
    
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                        <label> Penanganan Bayi Baru Lahir sakit </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                        @if($capaian13 == 1)
                                                            <input type="checkbox" name="pbbls" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @else
                                                            <input type="checkbox" name="pbbls" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @endif 
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                        <label> Penanganan Kelainan Bawaan </label>
                                                        <div class="input-group mb-3 bt-switch">
                                                        @if($capaian14 == 1)
                                                            <input type="checkbox" name="pkb" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @else
                                                            <input type="checkbox" name="pkb" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                        @endif    
                                                        </div>
                                                    </div>
                                                </div>                                            
                                            
                                            @endif
                                        </div>
                                    </div>
                                </section>
                                <!-- Step 2 -->
                                <h6>Pemeriksaan Fisik </h6>
                                <section>
                                    <div class="row">
                                        <div class="col-6">
                                            <h5>Pemeriksaan Apgar</h5>
                                            <div class="row">
                                                <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label> Aktivitas Otot </label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ti-user"></i></span>
                                                        </div>
                                                        <select name="fisik_apgar_otot" class="form-control" aria-invalid="true">
                                                            @if($pelayanan['neo_fisik_apgar'] != null && explode('_',$pelayanan['neo_fisik_apgar'])[0] == 0 )
                                                                <option value="0">Pilih</option>
                                                                <option value="2">Gerakan Aktif</option>
                                                                <option value="1">Hanya Gerakan Kaki dan Tangan</option>
                                                                <option value="0" selected>Tidak Ada Gerakan</option>
                                                            @elseif($pelayanan['neo_fisik_apgar'] != null && explode('_',$pelayanan['neo_fisik_apgar'])[0] == 1)
                                                                <option value="0" >Pilih</option>
                                                                <option value="2">Gerakan Aktif</option>
                                                                <option value="1" selected>Hanya Gerakan Kaki dan Tangan</option>
                                                                <option value="0">Tidak Ada Gerakan</option>
                                                            @elseif($pelayanan['neo_fisik_apgar'] != null && explode('_',$pelayanan['neo_fisik_apgar'])[0] == 2)
                                                                <option value="0" >Pilih</option>
                                                                <option value="2" selected>Gerakan Aktif</option>
                                                                <option value="1">Hanya Gerakan Kaki dan Tangan</option>
                                                                <option value="0">Tidak Ada Gerakan</option>
                                                            @else
                                                                <option value="0" selected>Pilih</option>
                                                                <option value="2">Gerakan Aktif</option>
                                                                <option value="1">Hanya Gerakan Kaki dan Tangan</option>
                                                                <option value="0">Tidak Ada Gerakan</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label> Denyut Jantung </label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ti-user"></i></span>
                                                        </div>
                                                        @if($pelayanan['neo_fisik_apgar'] != null && explode('_',$pelayanan['neo_fisik_apgar'])[4] != null )
                                                            <input type="number" name="fisik_apgar_jantung" class="form-control" value="{{ explode('_',$pelayanan['neo_fisik_apgar'])[4] }}" placeholder="Co : 120">
                                                        @else
                                                            <input type="number" name="fisik_apgar_jantung" class="form-control" value="00" placeholder="Co : 120">
                                                        @endif  
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">x/menit</span>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label> Respon terhadap Ransangan </label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ti-user"></i></span>
                                                        </div>
                                                        <select name="fisik_apgar_respon" class="form-control" aria-invalid="true">
                                                            @if($pelayanan['neo_fisik_apgar'] != null && explode('_',$pelayanan['neo_fisik_apgar'])[1] == 0)
                                                                <option value="0">Pilih</option>
                                                                <option value="2">Bayi Meringis dan Menarik Diri, Batuk atau Menangis</option>
                                                                <option value="1">Hanya Meringis</option>
                                                                <option value="0" selected>Tidak Ada Respon</option>
                                                            @elseif($pelayanan['neo_fisik_apgar'] != null && explode('_',$pelayanan['neo_fisik_apgar'])[1]  == 1)
                                                                <option value="0">Pilih</option>
                                                                <option value="2">Bayi Meringis dan Menarik Diri, Batuk atau Menangis</option>
                                                                <option value="1" selected>Hanya Meringis</option>
                                                                <option value="0">Tidak Ada Respon</option>
                                                            @elseif($pelayanan['neo_fisik_apgar'] != null && explode('_',$pelayanan['neo_fisik_apgar'])[1] == 2)
                                                                <option value="0">Pilih</option>
                                                                <option value="2" selected>Bayi Meringis dan Menarik Diri, Batuk atau Menangis</option>
                                                                <option value="1">Hanya Meringis</option>
                                                                <option value="0">Tidak Ada Respon</option>
                                                            @else
                                                                <option value="0" selected>Pilih</option>
                                                                <option value="2">Bayi Meringis dan Menarik Diri, Batuk atau Menangis</option>
                                                                <option value="1">Hanya Meringis</option>
                                                                <option value="0">Tidak Ada Respon</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label> Warna Tubuh </label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ti-user"></i></span>
                                                        </div>
                                                        <select name="fisik_apgar_warna" class="form-control" aria-invalid="true">
                                                            @if($pelayanan['neo_fisik_apgar'] != null && explode('_',$pelayanan['neo_fisik_apgar'])[2] == 0)
                                                                <option value="0">Pilih</option>
                                                                <option value="2">Seluruhnya Normal</option>
                                                                <option value="1">Warna Tangan dan atau Kaki Kebiruan</option>
                                                                <option value="0" selected>Seluruhnya abu-abu kebiruan atau Pucat</option>
                                                            @elseif($pelayanan['neo_fisik_apgar'] != null && explode('_',$pelayanan['neo_fisik_apgar'])[2] == 1)
                                                                <option value="0">Pilih</option>
                                                                <option value="2">Seluruhnya Normal</option>
                                                                <option value="1" selected>Warna Tangan dan atau Kaki Kebiruan</option>
                                                                <option value="0">Seluruhnya abu-abu kebiruan atau Pucat</option>
                                                            @elseif($pelayanan['neo_fisik_apgar'] != null && explode('_',$pelayanan['neo_fisik_apgar'])[2] == 2)
                                                                <option value="0">Pilih</option>
                                                                <option value="2" selected>Seluruhnya Normal</option>
                                                                <option value="1">Warna Tangan dan atau Kaki Kebiruan</option>
                                                                <option value="0">Seluruhnya abu-abu kebiruan atau Pucat</option>
                                                            @else
                                                                <option value="0" selected>Pilih</option>
                                                                <option value="2">Seluruhnya Normal</option>
                                                                <option value="1">Warna Tangan dan atau Kaki Kebiruan</option>
                                                                <option value="0">Seluruhnya abu-abu kebiruan atau Pucat</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label> Pernapasan </label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="ti-user"></i></span>
                                                    </div>
                                                    <select name="fisik_apgar_napas" class="form-control" aria-invalid="true">
                                                    @if($pelayanan['neo_fisik_apgar'] != null && explode('_',$pelayanan['neo_fisik_apgar'])[3] == 0)
                                                        <option value="0">Pilih</option>
                                                        <option value="2">Menangis Kuat dan bernafas secara Normal</option>
                                                        <option value="1">Menangis Lemah, Terkadag Merintih dan disertai Pernapasan tidak Teratur</option>
                                                        <option value="0" selected>Tidak Bernafas</option>
                                                    @elseif($pelayanan['neo_fisik_apgar'] != null && explode('_',$pelayanan['neo_fisik_apgar'])[3] == 1)
                                                        <option value="0">Pilih</option>
                                                        <option value="2">Menangis Kuat dan bernafas secara Normal</option>
                                                        <option value="1" selected>Menangis Lemah, Terkadag Merintih dan disertai Pernapasan tidak Teratur</option>
                                                        <option value="0">Tidak Bernafas</option>
                                                    @elseif($pelayanan['neo_fisik_apgar'] != null && explode('_',$pelayanan['neo_fisik_apgar'])[3] == 2)
                                                        <option value="0">Pilih</option>
                                                        <option value="2" selected >Menangis Kuat dan bernafas secara Normal</option>
                                                        <option value="1">Menangis Lemah, Terkadag Merintih dan disertai Pernapasan tidak Teratur</option>
                                                        <option value="0">Tidak Bernafas</option>
                                                    @else
                                                        <option value="0" selected>Pilih</option>
                                                        <option value="2">Menangis Kuat dan bernafas secara Normal</option>
                                                        <option value="1">Menangis Lemah, Terkadag Merintih dan disertai Pernapasan tidak Teratur</option>
                                                        <option value="0">Tidak Bernafas</option>
                                                    @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <h5>Pemeriksaan Gestasional</h5>
                                            <div class="row">
                                                <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                                    <label>Berat Badan</label> 
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ti-user"></i></span>
                                                        </div>
                                                        <input type="number" name="fisik_ges_berat" class="form-control" value="{{ explode('_',$pelayanan['neo_fisik_geo'])[0] }}" placeholder="0" max="200">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">kg</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                                    <label>Panjang Tubuh</label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" ><i class="ti-user"></i></span>
                                                        </div>
                                                        @if($pelayanan['neo_fisik_geo'] != null && explode('_',$pelayanan['neo_fisik_geo'])[1] != null)
                                                        <input type="number" name="fisik_ges_panjang" value="{{ explode('_',$pelayanan['neo_fisik_geo'])[1] }}" class="form-control" placeholder="0" max="200">
                                                        @else
                                                        <input type="number" name="fisik_ges_panjang" class="form-control" placeholder="0" max="200">
                                                        @endif
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">cm</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                                    <label>Lingkar Kepala</label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ti-user"></i></span>
                                                        </div>
                                                        @if($pelayanan['neo_fisik_geo'] != null && explode('_',$pelayanan['neo_fisik_geo'])[2] != null)
                                                        <input type="number" name="fisik_ges_lingkarKepala" value="{{ explode('_',$pelayanan['neo_fisik_geo'])[2] }}" class="form-control" placeholder="0" max="200">                                                    
                                                        @else
                                                        <input type="number" name="fisik_ges_lingkarKepala" class="form-control" placeholder="0" max="200">
                                                        @endif
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">cm</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h5>Pemeriksaan Kepala dan Leher</h5>
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Bentuk Kepala </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if( $pelayanan['neo_fisik_kl'] != null && explode("_",$pelayanan['neo_fisik_kl'])[0] == "y")
                                                        <input type="checkbox" name="fisik_kl_kepala" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @else
                                                        <input type="checkbox" name="fisik_kl_kepala" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Bentuk Leher </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if( $pelayanan['neo_fisik_kl'] != null && explode("_",$pelayanan['neo_fisik_kl'])[1] == "y")
                                                        <input type="checkbox" name="fisik_kl_leher" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @else
                                                        <input type="checkbox" name="fisik_kl_leher" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Bentuk Mata </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if( $pelayanan['neo_fisik_kl'] != null && explode("_",$pelayanan['neo_fisik_kl'])[2] == "y")
                                                        <input type="checkbox" name="fisik_kl_mata" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @else
                                                        <input type="checkbox" name="fisik_kl_mata"  data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Bentuk Hidung </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if( $pelayanan['neo_fisik_kl'] != null && explode("_",$pelayanan['neo_fisik_kl'])[3] == "y")
                                                        <input type="checkbox" name="fisik_kl_hidung" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @else
                                                        <input type="checkbox" name="fisik_kl_hidung" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @endif    
                                                    
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Bentuk Telinga </label>
                                                    <div class="input-group mb-3 bt-switch"> 
                                                    @if( $pelayanan['neo_fisik_kl'] != null && explode("_",$pelayanan['neo_fisik_kl'])[4] == "y")
                                                        <input type="checkbox" name="fisik_kl_telinga" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">                                            
                                                    @else
                                                        <input type="checkbox" name="fisik_kl_telinga" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <h5>Pemeriksaan Mulut</h5>
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Kondisi Gusi </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if( $pelayanan['neo_fisik_mulut'] != null && explode("_",$pelayanan['neo_fisik_mulut'])[0] == "y")
                                                        <input type="checkbox" name="fisik_mulut_gusi" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @else
                                                        <input type="checkbox" name="fisik_mulut_gusi" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Kondisi Langit - langit mulut </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if( $pelayanan['neo_fisik_mulut'] != null && explode("_",$pelayanan['neo_fisik_mulut'])[1] == "y")
                                                        <input type="checkbox" name="fisik_mulut_langit" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @else
                                                        <input type="checkbox" name="fisik_mulut_langit" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <h5>Pemeriksaan Perut dan Kelamin</h5>
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Bentuk Perut </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if( $pelayanan['neo_fisik_pk'] != null && explode("_",$pelayanan['neo_fisik_pk'])[0] == "y")
                                                        <input type="checkbox" name="fisik_pk_bentukPerut"checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @else
                                                        <input type="checkbox" name="fisik_pk_bentukPerut" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Lingkar Perut </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if( $pelayanan['neo_fisik_pk'] != null && explode("_",$pelayanan['neo_fisik_pk'])[1] == "y")
                                                        <input type="checkbox" name="fisik_pk_lingkarPerut" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">                                                    
                                                    @else
                                                        <input type="checkbox" name="fisik_pk_lingkarPerut" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">                                                    
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Organ dalam Perut </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if( $pelayanan['neo_fisik_pk'] != null && explode("_",$pelayanan['neo_fisik_pk'])[2] == "y")
                                                    <input type="checkbox" name="fisik_pk_organPerut" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @else
                                                    <input type="checkbox" name="fisik_pk_organPerut" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @endif                                                    
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Organ Kelamin </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if( $pelayanan['neo_fisik_pk'] != null && explode("_",$pelayanan['neo_fisik_pk'])[3] == "y")
                                                        <input type="checkbox" name="fisik_pk_organKelamin" checked data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @else
                                                        <input type="checkbox" name="fisik_pk_organKelamin" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <h5>Pemeriksaan Tulang Belakang, Tangan, dan Kaki</h5>
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Tulang Belakang </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if( $pelayanan['neo_fisik_tbtk'] != null && explode("_",$pelayanan['neo_fisik_tbtk'])[0] == "y")
                                                    <input type="checkbox" name="fisik_tbtk_tulangbel" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @else
                                                    <input type="checkbox" name="fisik_tbtk_tulangbel" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Kondisi Tangan </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if( $pelayanan['neo_fisik_tbtk'] != null && explode("_",$pelayanan['neo_fisik_tbtk'])[1] == "y")
                                                    <input type="checkbox" name="fisik_tbtk_tangan" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @else
                                                    <input type="checkbox" name="fisik_tbtk_tangan" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Kondisi Kaki </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if( $pelayanan['neo_fisik_tbtk'] != null && explode("_",$pelayanan['neo_fisik_tbtk'])[2] == "y")
                                                    <input type="checkbox" name="fisik_tbtk_kaki" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @else
                                                    <input type="checkbox" name="fisik_tbtk_kaki" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Normal" data-off-text="Kelainan">
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <!-- Step 3 -->
                                <h6>Skrining</h6>
                                <section>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label>Fasilitas Kesahatan</label> <span class="text-danger">*</span>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon11"><i class="ti-location-pin"></i></span>
                                                        </div>
                                                        <select name="s_fasilitas_kesehatan" id="select" class="form-control required" aria-invalid="true">
                                                            @if($pelayanan['lokasi'] != null && explode("_",$pelayanan['lokasi'])[0] == "Rumah Sakit")
                                                                <option value="">Pilih</option>
                                                                <option value="Rumah Sakit" selected>Rumah Sakit</option>
                                                                <option value="Puskesmas">Puskesmas</option>
                                                                <option value="Klinik" >Klinik</option>
                                                            @elseif($pelayanan['lokasi'] != null && explode("_",$pelayanan['lokasi'])[0] == "Puskesmas" )                                                            
                                                                <option value="">Pilih</option>
                                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                                <option value="Puskesmas" selected>Puskesmas</option>
                                                                <option value="Klinik">Klinik</option>
                                                            @elseif($pelayanan['lokasi'] != null && explode("_",$pelayanan['lokasi'])[0] == "Klinik" )                                                            
                                                                <option value="">Pilih</option>
                                                                <option value="Rumah Sakit" >Rumah Sakit</option>
                                                                <option value="Puskesmas">Puskesmas</option>
                                                                <option value="Klinik" selected>Klinik</option>
                                                            @else
                                                                <option value="" selected>Pilih</option>
                                                                <option value="Rumah Sakit" >Rumah Sakit</option>
                                                                <option value="Puskesmas">Puskesmas</option>
                                                                <option value="Klinik">Klinik</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label>Lokasi Tempat Pelayanan</label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon11"><i class="ti-map-alt"></i></span>
                                                        </div>
                                                        @if($pelayanan['lokasi'] != null && explode("_",$pelayanan['lokasi'])[1] != null )
                                                        <input type="text" value="{{ explode('_',$pelayanan['lokasi'])[1] }}" name="s_lokasi_pelayanan" class="form-control" placeholder="Nama RS / Nama Jalan">
                                                        @else
                                                        <input type="text" name="s_lokasi_pelayanan" class="form-control" placeholder="Nama RS / Nama Jalan">
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label>Tenaga Kesehatan</label> <span class="text-danger">*</span>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                        </div>
                                                        <select name="s_tenaga_kesehatan" id="select" class="form-control required" aria-invalid="true">
                                                            @if($pelayanan['tenaga_kerja'] != null && explode("_",$pelayanan['tenaga_kerja'])[0] == "Dokter")
                                                            <option value="">Pilih</option>
                                                            <option value="Dokter" selected>Dokter</option>
                                                            <option value="Bidan">Bidan</option>
                                                            <option value="Dokter Spesialis">Dokter Spesialis</option>
                                                            @elseif($pelayanan['tenaga_kerja'] != null && explode("_",$pelayanan['tenaga_kerja'])[0] == "Bidan" )                                                            
                                                            <option value="">Pilih</option>
                                                            <option value="Dokter">Dokter</option>
                                                            <option value="Bidan" selected>Bidan</option>
                                                            <option value="Dokter Spesialis">Dokter Spesialis</option>
                                                            @elseif($pelayanan['tenaga_kerja'] != null && explode("_",$pelayanan['tenaga_kerja'])[0] == "Bidan Spesialis" )                                                            
                                                            <option value="">Pilih</option>
                                                            <option value="Dokter">Dokter</option>
                                                            <option value="Bidan">Bidan</option>
                                                            <option value="Dokter Spesialis" selected>Dokter Spesialis</option>
                                                            @else
                                                            <option value="" selected>Pilih</option>
                                                            <option value="Dokter">Dokter</option>
                                                            <option value="Bidan">Bidan</option>
                                                            <option value="Dokter Spesialis">Dokter Spesialis</option>
                                                            @endif
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label>Nama Dokter/Bidan</label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                        </div>
                                                        @if($pelayanan['tenaga_kerja'] != null && explode("_",$pelayanan['tenaga_kerja'])[1] != null )
                                                        <input type="text" name="s_nama_stk" value="{{ explode('_',$pelayanan['tenaga_kerja'])[1] }}" class="form-control" placeholder="Co : dr. Ahmad Riza">                                                        
                                                        @else
                                                        <input type="text" name="s_nama_stk" class="form-control" placeholder="Co : dr. Ahmad Riza">
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <h5>Skrining Hipotiroid Kongenital</h5>
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> Hasil Sampel Darah </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if($pelayanan['skr_shk_hsd'] == "y")
                                                        <input type="checkbox" name="s_shk_hsd" data-size="small"  checked data-on-color="info" data-off-color="default" data-on-text="Positif" data-off-text="Negatif">
                                                    @else
                                                        <input type="checkbox" name="s_shk_hsd" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Positif" data-off-text="Negatif">
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                    <label> 
                                                        Jika menunjukkan hasil positif, pengobatan harus dilakukan
                                                        sebelum Bayi berusia 1 bulan.  
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <!-- Step 4 -->
                                <h6>Pemberian Informasi</h6>
                                <section>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Nama Tenaga Kesehatan Pemberi Informasi</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="ti-user"></i></span>
                                                    </div>
                                                    @if($pelayanan['kie_nama'] != null)
                                                        <input type="text" value="{{$pelayanan['kie_nama']}}" name="kie_nama_pi" class="form-control" placeholder="Co : Suster Rini A.">
                                                    @else
                                                        <input type="text" name="kie_nama_pi" class="form-control" placeholder="Co : Suster Rini A.">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Informasi perawatan Bayi </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if($pelayanan['kie_informasi'] != null && explode('_',$pelayanan['kie_informasi'])[0] == "y")
                                                        <input type="checkbox" name="kie_perawatan" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Diinformasikan" data-off-text="Belum">
                                                    @else
                                                        <input type="checkbox" name="kie_perawatan" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Diinformasikan" data-off-text="Belum">
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Informasi skrining Bayi </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if($pelayanan['kie_informasi'] != null && explode('_',$pelayanan['kie_informasi'])[1] == "y")
                                                    <input type="checkbox" name="kie_skrining" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Diinformasikan" data-off-text="Belum">
                                                    @else
                                                    <input type="checkbox" name="kie_skrining" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Diinformasikan" data-off-text="Belum">
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Informasi tanda bahaya pada Bayi </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if($pelayanan['kie_informasi'] != null && explode('_',$pelayanan['kie_informasi'])[2] == "y")
                                                    <input type="checkbox" name="kie_bahaya" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Diinformasikan" data-off-text="Belum">
                                                    @else
                                                    <input type="checkbox" name="kie_bahaya" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Diinformasikan" data-off-text="Belum">
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Informasi pelayanan kesehatan pada Bayi </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if($pelayanan['kie_informasi'] != null && explode('_',$pelayanan['kie_informasi'])[3] == "y")
                                                    <input type="checkbox" name="kie_pelayanan" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Diinformasikan" data-off-text="Belum">
                                                    @else
                                                    <input type="checkbox" name="kie_pelayanan" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Diinformasikan" data-off-text="Belum">
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Informasi ASI Eksklusif </label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if($pelayanan['kie_informasi'] != null && explode('_',$pelayanan['kie_informasi'])[4] == "y")
                                                    <input type="checkbox" name="kie_asi" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Diinformasikan" data-off-text="Belum">
                                                    @else
                                                    <input type="checkbox" name="kie_asi" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Diinformasikan" data-off-text="Belum">
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="form-group m-0 col-lg-4 col-xlg-4 col-md-12">
                                                    <label> Media memberikan Informasi</label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if($pelayanan['kie_media'] != null && $pelayanan['kie_media'] == "y")
                                                    <input type="checkbox" name="kie_media" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Buku KIA" data-off-text="Lainnya">
                                                    @else
                                                    <input type="checkbox" name="kie_media" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Buku KIA" data-off-text="Lainnya">
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>
                                </section>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <!-- Picker Date Style -->
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.date.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.time.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/legacy.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <!-- <script src="{{asset('adminbite-10/assets/libs/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/datetimepicker/datetimepicker.init.js')}}"></script> -->
    
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js')}}"></script>
    <!-- Switch Style -->
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/js/bootstrap-switch.min.js')}}"></script>
    <!-- Wizard Style -->
    <script src="{{asset('adminbite-10/assets/libs/jquery-steps/build/jquery.steps.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script>
    //Basic Example
    $("#example-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true
    });

    // Basic Example with form
    var form = $("#example-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password"
            }
        }
    });
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    });

    // Advance Example

    var form = $("#example-advanced-form").show();

    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            // Forbid next action on "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age-2").val()) < 18) {
                return false;
            }
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onStepChanged: function(event, currentIndex, priorIndex) {
            // Used to skip the "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
                form.steps("next");
            }
            // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3) {
                form.steps("previous");
            }
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password-2"
            }
        }
    });

    // Dynamic Manipulation
    $("#example-manipulation").steps({
        headerTag: "h3",
        bodyTag: "section",
        enableAllSteps: true,
        enablePagination: false
    });

    //Vertical Steps

    $("#example-vertical").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        stepsOrientation: "vertical"
    });

    //Custom design form example
    $(".tab-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onFinished: function(event, currentIndex) {
            document.getElementById('wizard-form').submit();

        }
    });


    var form = $(".validation-wizard").show();

    $(".validation-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onStepChanging: function(event, currentIndex, newIndex) {
            return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
        },
        onFinishing: function(event, currentIndex) {
            return form.validate().settings.ignore = ":disabled", form.valid()
        },
        onFinished: function(event, currentIndex) {
            document.getElementById('wizard-form').submit();
        }
    }), $(".validation-wizard").validate({
        ignore: "input[type=hidden]",
        errorClass: "text-danger",
        successClass: "text-success",
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element)
        },
        rules: {
            email: {
                email: !0
            }
        }
    })
    </script>
    <!-- Switch Style     -->
    <script>
    $(".bt-switch input[type='checkbox'], .bt-switch input[type='radio']").bootstrapSwitch();
    var radioswitch = function() {
        var bt = function() {
            $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioState")
            }), $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck")
            }), $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck", !1)
            })
        };
        return {
            init: function() {
                bt()
            }
        }
    }();
    $(document).ready(function() {
        init()
    });
    </script>
    <script>
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    $('#date-fr').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', lang: 'fr', weekStart: 1, cancelText: 'ANNULER' });
    $('#date-end').bootstrapMaterialDatePicker({ weekStart: 0 });
    $('#date-start').bootstrapMaterialDatePicker({ weekStart: 0 }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });
    </script>
    <script type="text/javascript">
        $('.pickadate-disable').pickadate({
            disable: [

                1, 7
            ]
        });
    </script>
</body>
</html>