<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>SPM Penderita Diabetes Melitus</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Penderita Diabetes Melitus</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active" aria-current="page">Standar Pelayanan Minimal</li>                                
                                    <li class="breadcrumb-item active" aria-current="page">Penderita Diabetes Melitus</li>
                                    <li class="breadcrumb-item active" aria-current="page">SPM Penderita Diabetes Melitus</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
                <div class="card-body bg-light">
                    <div class="d-md-flex align-items-center">
                        <div><h2>Pelayanan Penderita Diabetes Melitus</h2></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Penyataan Standar</h4>
                            Setiap penderita diabetes melitus mendapatkan pelayanan kesehatan
                            sesuai standar.
                            Pemerintah Kabupaten/Kota mempunyai kewajiban untuk
                            memberikan pelayanan kesehatan sesuai standar kepada seluruh
                            penyandang diabetes melitus sebagai upaya pencegahan sekunder di
                            wilayah kerjanya.
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Pengertian Penderita Diabetes Melitus</h4>
                            <p> 1) Sasaran indikator ini adalah penyandang DM di wilayah kerja
                                kabupaten/kota.<br>
                                2) Penduduk yang ditemukan menderita DM atau penyandang DM
                                memperoleh pelayanan kesehatan sesuai standar dan upaya
                                promotif dan preventif di FKTP.<br>
                                3) Penduduk yang ditemukan menderita DM atau penyandang DM
                                dengan komplikasi perlu dirujuk ke fasilitas kesehatan rujukan
                                untuk penanganan selanjutnya.<br>
                                
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                        <h4 class="card-title">Pengertian Penderita Diabetes Melitus</h4>
                            <p> 
                                4) Pelayanan kesehatan penyandang DM diberikan sesuai
                                    kewenangannya oleh :
                                    a) Dokter/DLP
                                    b) Perawat
                                    c) Nutrisionis/Tenaga Gizi<br>
                                5) Pelayanan kesehatan diberikan kepada penyandang DM di FKTP
                                sesuai standar meliputi 4 (empat) pilar penatalaksanaan sebagai
                                berikut:<br>
                                a) Edukasi
                                b) Aktifitas fisik
                                c) Terapi nutrisi medis
                                d) Intervensi farmakologis<br>
                                6) Setiap penyandang DM yang mendapatkan pelayanan sesuai
                                standar termasuk pemeriksaan HbA1C.<br>
                                7) Bagi penyandang DM yang belum menjadi peserta JKN
                                diwajibkan menjadi peserta JKN.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Definisi Operasional Capaian Kerja</h4>
                            Capaian kinerja Pemerintah Kabupaten/Kota dalam memberikan
                            pelayanan kesehatan sesuai standar bagi penyandang DM dinilai dari
                            persentase penyandang DM yang mendapatkan pelayanan sesuai
                            standar di wilayah kerjanya dalam kurun waktu satu tahun.<br><br>
                            Pemerintah kabupaten/kota secara bertahap harus membuat
                            rencana aksi untuk bisa menjangkau seluruh penyandang DM di
                            wilayahnya dan mengupayakan agar semua penyandang DM tersebut
                            memperoleh akses terhadap pelayanan kesehatan sesuai standar.
                            Secara nasional saat ini baru 30 persen penyandang DM yang
                            terdiagnosis dan mendapatkan pelayanan kesehatan.
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4><i class="fas fa-chevron-circle-right m-r-10 m-b-10"></i> Referensi</h4>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 71 Tahun 2015 tentang Penanggulangan PTM;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 59 Tahun 2014 tentang standar tarif pelayanan kesehatan dalam penyelenggaraan program jaminan kesehatan;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 75 Tahun 2014 tentang Pusat Kesehatan Masyarakat;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 001 Tahun 2012 tentang Sistem Rujukan;</h6>
                    <h6><i class="fas fa-square"></i> Pedoman Nasional Pelayanan Kedokteran Diabetes Melitus;</h6>
                    <h6><i class="fas fa-square"></i> Pedoman Umum Pencegahan dan Pengendalian DM Tipe 2, Kemenkes 2016;</h6>
                    <h6><i class="fas fa-square"></i> Pedoman Umum Pengendalian DM Tipe 1, Kemenkes 2013;</h6>
                    <h6><i class="fas fa-square"></i> Pedoman Umum Pengendalian DM Gestasional, Kemenkes 2013;</h6>
                    <h6><i class="fas fa-square"></i> Panduan Penatalaksanaan DM Tipe 2, PB PERKENI, 2015;</h6>
                    <h6><i class="fas fa-square"></i> Konsensus Pengelolaan dan Pencegahan DM Tipe 2 di Indonesia, PB PERKENI 2015;</h6>
                    <h6><i class="fas fa-square"></i> Manual Peralatan Pemeriksaan Gula Darah dan A1C;</h6>
                    <h6><i class="fas fa-square"></i> Panduan Praktik Klinis Bagi Dokter di Fasilitas Pelayanan Kesehatan Primer.</h6>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>