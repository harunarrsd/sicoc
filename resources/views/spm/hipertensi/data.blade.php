<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')@yield('head-ripel01')
<title>Data Penderita Hipertensi</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Penderita Hipertensi</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Penderita Hipertensi</li>
                                    <li class="breadcrumb-item active" aria-current="page">Data Penderita Hipertensi</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @if(!empty($alert))
                            {{ $alert }}
                        @endif
                        @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('success')}}
                            </div>
                        @endif
                        @if(session()->has('danger'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('danger')}}
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <div class="d-md-flex align-items-center">
                                    <div>
                                        <h4 class="card-title">Data Penderita Hipertensi</h4>
                                        <h6 class="card-subtitle">Standar Pelayanan Minimal</h6>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="dl">
                                            <div class="btn-group">
                                            @if(Auth::user()['level'] == 1 || Auth::user()['level'] == 2)
                                                <a href="{{ url('/tambah-penderita-hipertensi')}}"><button type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Pendaftaran Penderita Hipertensi">
                                                    <i class="mdi mdi-plus"></i> Tambah Penderita Hipertensi
                                                </button></a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-t-10">
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card">
                                            <div class="box bg-info text-center">
                                                <h1 class="font-light text-white">{{ $total_penderita}}</h1>
                                                <h6 class="text-white">Total Penderita Hipertensi</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card">
                                            <div class="box bg-success text-center">
                                                <h1 class="font-light text-white">{{$total_sesuai_spm}}</h1>
                                                <h6 class="text-white">Penderita Sesuai SPM</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card">
                                            <div class="box bg-danger text-center">
                                                <h1 class="font-light text-white">{{$total_tak_sesuai_spm}}</h1>
                                                <h6 class="text-white">Penderita Tidak Sesuai SPM</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card">
                                            <div class="box bg-warning text-center">
                                                <h1 class="font-light text-white">{{ $total_tahap_pelayanan}}</h1>
                                                <h6 class="text-white">Tahap Pelayanan</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Status</th>
                                                <th>Nama Penderita</th>
                                                <th>Hipertensi Sejak</th>
                                                <th>Usia</th>
                                                <th>Capaian SPM</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @php $no=1; @endphp
                                        @foreach($hipertensi_all as $i => $hip)
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>
                                                    @if($hip['status'] == "Tahap Pelayanan")
                                                        <span class="label label-warning">{{$hip['status']}}</span>
                                                    @elseif($hip['status'] == "Sesuai SPM")
                                                        <span class="label label-info">{{$hip['status']}}</span>
                                                    @elseif($hip['status'] == "Tidak Sesuai SPM")
                                                        <span class="label label-danger">{{$hip['status']}}</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="#" class="font-bold link">{{$data_nik[$i]['nama']}}</a><br>
                                                    <small class="text-muted">{{$data_nik[$i]['nik']}}</small>
                                                </td>
                                                
                                                <td>
                                                    {{ date("d M Y",strtotime($hip['tanggal_menderita'])) }} <br>
                                                    <small class="text-muted">{{ date("H:i:s",strtotime($hip['tanggal_menderita'])) }}</small>
                                                </td>
                                                <td>
                                                    @if($usia_hip[$i]['years'] ==0)
                                                        @if($usia_hip[$i]['months'] == 0)
                                                            {{ $usia_hip[$i]['days'] }} Hari
                                                            <br> <small class="text-muted">{{ $usia_hip[$i]['hours'] }} Jam</small> 
                                                        @else
                                                            {{ $usia_hip[$i]['months'] }} Bulan <br> <small class="text-muted">{{ $usia_hip[$i]['days'] }} Hari</small> 
                                                        @endif
                                                    @else
                                                        {{ $usia_hip[$i]['years'] }} Tahun <br> <small class="text-muted"> {{ $usia_hip[$i]['months'] }} Bulan {{ $usia_hip[$i]['days'] }} Hari</small> 
                                                    @endif
                                                </td>
                                                <td><span class="label label-info">0%</span></td>
                                                <td>                                        
                                                    <a href="{{ url('/detail-penderita-hipertensi/'.$hip->id )}}">
                                                        <button type="submit" class="btn btn-info btn-xs">
                                                            Detail
                                                        </button>
                                                    </a>
                                                    @if(Auth::user()['level'] == 1 || Auth::user()['level'] == 2)
                                                    @if($hip['status'] == "Tahap Pelayanan")
                                                    <a href="{{ url('/ubah-penderita-hipertensi/'.$hip->id)}}">
                                                        <button type="submit" class="btn btn-warning btn-xs">
                                                            Ubah
                                                        </button>
                                                    </a>

                                                    <button alt="default" class="btn btn-danger btn-xs" data-href="{{ url('/hapus-penderita-hipertensi/'.$hip['id'])}}" data-toggle="modal" data-target="#confirm-delete">
                                                        Hapus
                                                    </button>
                                                    @endif
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            Hapus
                        </div>
                        <div class="modal-body">
                            Anda yakin ingin menghapus ?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <a class="btn btn-danger btn-ok text-white">Delete</a>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
    </script>
</body>
</html>