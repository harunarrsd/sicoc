<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>SPM Penderita Hipertensi</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Penderita Hipertensi</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active" aria-current="page">Standar Pelayanan Minimal</li>                                
                                    <li class="breadcrumb-item active" aria-current="page">Penderita Hipertensi</li>
                                    <li class="breadcrumb-item active" aria-current="page">SPM Penderita Hipertensi</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
                <div class="card-body bg-light">
                    <div class="d-md-flex align-items-center">
                        <div><h2>Pelayanan Penderita Hipertensi</h2></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Penyataan Standar</h4>
                            Setiap penderita hipertensi mendapatkan pelayanan kesehatan
                            sesuai standar.
                            Pemerintah Kabupaten/Kota mempunyai kewajiban untuk
                            memberikan pelayanan kesehatan sesuai standar kepada seluruh
                            penderita hipertensi sebagai upaya pencegahan sekunder di wilayah
                            kerjanya.
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title"> Pengertian Penderita Hipertensi : </h4>
                            <p> 1) Sasaran adalah penduduk usia 15 tahun ke atas <br>
                                2) Penderita hipertensi esensial atau hipertensi tanpa komplikasi
                                memperoleh pelayanan kesehatan sesuai standar; dan upaya
                                promosi kesehatan melalui modifikasi gaya hidup di Fasilitas
                                Kesehatan Tingkat Pertama (FKTP).<br>
                                3) Penderita hipertensi dengan komplikasi (jantung, stroke dan
                                penyakit ginjal kronis, diabetes melitus) perlu dirujuk ke
                                Fasilitas Kesehatan Tingkat Lanjut (FKTL) yang mempunyai
                                kompetensi untuk penanganan komplikasi.<br>
                                4) Standar pelayanan kesehatan penderita hipertensi</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title"> Standar pelayanan kesehatan penderita hipertensi adalah : </h4>
                            <p> a) Mengikuti Panduan Praktik Klinik Bagi Dokter di FKTP. <br>
                                b) Pelayanan kesehatan sesuai standar diberikan kepada
                                penderita Hipertensi di FKTP.<br>
                                c) Pelayanan kesehatan hipertensi sesuai standar meliputi:
                                pemeriksaan dan monitoring tekanan darah, edukasi,
                                pengaturan diet seimbang, aktifitas fisik, dan pengelolaan
                                farmakologis.<br>
                                d) Pelayanan kesehatan berstandar ini dilakukan untuk
                                mempertahankan tekanan darah pada < 140/90 mmHg
                                untuk usia di bawah 60 th dan < 150/90 mmHg untuk
                                penderita 60 tahun ke atas dan untuk mencegah terjadinya
                                komplikasi jantung, stroke, diabetes melitus dan penyakit
                                ginjal kronis. <br>
                                e) Selama menjalani pelayanan kesehatan sesuai standar, jika
                                tekanan darah penderita hipertensi tidak bisa
                                dipertahankan sebagaimana dimaksud pada poin
                                sebelumnya atau mengalami komplikasi, maka penderita
                                perlu dirujuk ke FKTL yang berkompeten.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Definisi Operasional Capaian Kerja</h4>
                            Capaian kinerja Pemerintah Kabupaten/Kota dalam memberikan
                            pelayanan kesehatan sesuai standar bagi penderita hipertensi, dinilai
                            dari persentase jumlah penderita hipertensi yang mendapatkan
                            pelayanan kesehatan sesuai standar di wilayah kerjanya dalam
                            kurun waktu satu tahun.
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4><i class="fas fa-chevron-circle-right m-r-10 m-b-10"></i> Referensi</h4>
                    <h6><i class="fas fa-square"></i> Undang-Undang Nomor 44 Tahun 2009 tentang Rumah Sakit;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 001 Tahun 2012 tentang Sistem Rujukan;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Presiden Nomor 12 Tahun 2013 tentang Jaminan Kesehatan Nasional;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 75 Tahun 2014 tentang Pusat Kesehatan Masyarakat;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 59 Tahun 2014 tentang Standar Tarif Pelayanan Kesehatan dalam Penyelenggaraan Program Jaminan Kesehatan;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 5 Tahun 2014 tentang Panduan Praktik Klinis Bagi Dokter di Fasilitas Pelayanan Kesehatan Primer;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 71 Tahun 2015 tentang Penanggulangan PTM dengan lampiran:</h6>
                    <h6>a) JNC-8 Hipertension Guidelines 2014</h6>
                    <h6>b) Pedoman Pengendalian Hipertensi 2015</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Konsil Kedokteran Indonesia Nomor 11 Tahun 2012 tentang Standar Kompetensi Dokter Indonesia;</h6>
                    <h6><i class="fas fa-square"></i> Pedoman Pelaksanaan Stimulasi, Deteksi dan Intervensi Dini Tumbuh Kembang Anak Ditingkat Pelayanan Kesehatan Dasar. </h6>                    
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>