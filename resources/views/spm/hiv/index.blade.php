<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>SPM Orang dengan Risiko Terinfeksi HIV</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Risiko Terinfeksi HIV</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active" aria-current="page">Standar Pelayanan Minimal</li>                                
                                    <li class="breadcrumb-item active" aria-current="page">Orang dengan Risiko Terinfeksi HIV</li>
                                    <li class="breadcrumb-item active" aria-current="page">SPM Risiko Terinfeksi HIV</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
                <div class="card-body bg-light">
                    <div class="d-md-flex align-items-center">
                        <div><h2>Pelayanan Orang dengan Risiko Terinfeksi HIV</h2></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Penyataan Standar</h4>
                            Setiap orang berisiko terinfeksi HIV (ibu hamil, pasien TB, pasien
                            IMS, waria/transgender, pengguna napza, dan warga binaan lembaga
                            pemasyarakatan) mendapatkan pemeriksaan HIV sesuai standar. 
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Pelayanan Kesehatan orang dengan risiko terinfeksi HIV </h4>
                            <p> Pelayanan Kesehatan orang dengan risiko terinfeksi HIV sesuai
                                standar adalah pelayanan kesehatan yang diberikan kepada ibu
                                hamil, pasien TB, pasien infeksi menular seksual (IMS),
                                waria/transgender, pengguna napza, dan warga binaan lembaga
                                pemasyarakatan, dilakukan oleh tenaga kesehatan sesuai
                                kewenangannya dan diberikan di FKTP (Puskesmas dan
                                Jaringannya) dan FKTL baik pemerintah maupun swasta serta di
                                lapas/rutan narkotika.
                                </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Pelayanan Kesehatan meliputi:</h4>
                            <p> 1) Upaya pencegahan pada orang yang memiliki risiko terinfeksi
                                HIV<br>
                                Pemeriksaan HIV ditawarkan secara aktif oleh petugas
                                kesehatan bagi orang yang berisiko dimulai dengan:<br>
                                - pemberian informasi terkait HIV-AIDS<br>
                                - pemeriksaan HIV menggunakan tes cepat HIV dengan
                                menggunakan alat tes sesuai standar nasional yang telah
                                ditetapkan<br>
                                - orang dengan hasil pemeriksaan HIV positif harus dirujuk
                                ke fasilitas yang mampu menangani untuk mendapatkan
                                pengobatan ARV dan konseling tentang HIV dan AIDS bagi
                                orang dengan HIV (ODHA) dan pasangannya<br>
                                - orang dengan infeksi menular seksual (IMS),
                                waria/transgender, pengguna napza, dan warga binaan
                                lembaga pemasyarakatan dengan hasil pemeriksaan HIV
                                negatif harus dilakukan pemeriksaan ulang minimal setelah
                                tiga (3) bulan, enam (6) bulan dan 12 bulan dari
                                pemeriksaan yang pertama.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Definisi Operasional Capaian Kerja</h4>
                            Capaian kinerja Pemerintah Daerah Kabupaten/Kota dalam
                            memberikan pemeriksaan HIV terhadap orang berisiko terinfeksi HIV
                            dinilai dari persentase orang berisiko terinfeksi HIV yang datang ke
                            fasyankes dan mendapatkan pemeriksaan HIV sesuai standar di
                            wilayah kerjanya dalam kurun waktu satu tahun.
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4><i class="fas fa-chevron-circle-right m-r-10 m-b-10"></i> Referensi</h4>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 75 Tahun 2014 tentang Pusat Kesehatan Masyarakat;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 5 Tahun 2014 tentang Panduan Praktik Klinis;</h6>
                    <h6><i class="fas fa-square"></i> Pedoman Pencegahan dan Pengendalian Pemasungan Orang Dengan Gangguan Jiwa (ODGJ);</h6>
                    <h6><i class="fas fa-square"></i> Keputusan Menteri Kesehatan Nomor 279/Menkes/SK/IV/2006 tentang Pedoman Penyelenggaraan Upaya Keperawatan Kesehatan Masyarakat di Puskesmas;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor HK 02.02/Menkes/148/I/2010 tentang Ijin dan Penyelenggaraan Praktik Keperawatan;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 17 Tahun 2013 tentang Perubahan Atas Peraturan Menteri Kesehatan Nomor HK 02.02/Menkes/148/I/2010 tentang Praktik Keperawatan;</h6>
                    <h6><i class="fas fa-square"></i> Buku Keperawatan Jiwa Masyarakat (Community Mental Health Nursing).</h6>

                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>