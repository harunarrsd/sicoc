<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')@yield('head-ripel01')
<title>Data Ibu Bersalin</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/sweetalert2/dist/sweetalert2.min.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Ibu Bersalin</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Ibu Bersalin</li>
                                    <li class="breadcrumb-item active" aria-current="page">Data Ibu Hamil</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                @if(!empty($alert))
                    {{ $alert }}
                @endif
                <div class="row">
                    <div class="col-12">
                        @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('success')}}
                            </div>
                        @endif
                        @if(session()->has('danger'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('danger')}}
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <div class="d-md-flex align-items-center">
                                    <div>
                                        <h4 class="card-title">Data Ibu Bersalin</h4>
                                        <h6 class="card-subtitle">Standar Pelayanan Minimal</h6>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="dl">
                                            <div class="btn-group">
                                                <a href="{{ url('/tambah-ibu-bersalin')}}"><button type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Pendaftaran Ibu Bersalin">
                                                    <i class="mdi mdi-plus"></i> Tambah Ibu Bersalin
                                                </button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-t-10">
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card">
                                            <div class="box bg-info text-center">
                                                <h1 class="font-light text-white">{{ $total_ibu_bersalin }}</h1>
                                                <h6 class="text-white" data-toggle="tooltip" data-placement="bottom" title="Total Ibu Bersalin terdaftar pada SICOC">Total Ibu Bersalin</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card">
                                            <div class="box bg-success text-center">
                                                <h1 class="font-light text-white">{{ $total_dpt_layanan }}</h1>
                                                <h6 class="text-white" data-toggle="tooltip" data-placement="bottom" title="Mendapat Layanan Sesuai SPM! Capaian SPM mencapai 100%">Mendapat Layanan</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card">
                                            <div class="box bg-danger text-center">
                                                <h1 class="font-light text-white">{{ $total_blm_dpt_layanan }}</h1>
                                                <h6 class="text-white" data-toggle="tooltip" data-placement="bottom" title="Tidak Mendapat Layanan Sesuai SPM. Dikarenakan Capaian SPM tidak mencapai 100%.">Belum Mendapat Layanan</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card">
                                            <div class="box bg-warning text-center">
                                                <h1 class="font-light text-white">{{ $total_belum_bersalin }}</h1>
                                                <h6 class="text-white" data-toggle="tooltip" data-placement="bottom" title="Total Ibu Hamil terdaftar pada SICOC">Belum Bersalin</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Status Layanan</th>
                                                <th>Nama Ibu Bersalin</th>
                                                <th>Tanggal Bersalin</th>
                                                <th>Tenaga Kesehatan</th>
                                                <th>Capaian SPM</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $no = 1; @endphp
                                            @foreach($ibubersalin as $index => $value)
                                            <tr>
                                                <td>{{$no}}</td>@php $no++; @endphp
                                                <td>
                                                    @if($status_layanan[$index] == "Mendapat Layanan")
                                                        <span class="label label-success" data-toggle="tooltip" data-placement="top" title="Mendapat Layanan Sesuai SPM! Capaian SPM mencapai 100%">Sesuai SPM</span>
                                                    @endif
                                                    @if($status_layanan[$index] == "Belum Mendapat Layanan")
                                                        <span class="label label-danger" data-toggle="tooltip" data-placement="top" title="Tidak Mendapat Layanan Sesuai SPM. Dikarenakan Capaian SPM tidak mencapai 100%.">Tidak Sesuai SPM</span>
                                                    @endif
                                                </td>
                                                <td><a href="#" class="font-bold link">{{$nama_ibu_bersalin[$index]}}</a><br>
                                                <small class="text-muted"> {{$nik_ibu_bersalin[$index]}}</small>
                                                
                                                </td> 
                                                <td>
                                                    {{ date('d M Y', strtotime($value['tanggal_bersalin'])) }}<br>
                                                    @if($value['hasil_persalinan'] == "Hidup Sehat")
                                                        <small class="text-muted"> Hasil : </small><span class="label label-info">{{$value['hasil_persalinan']}}</span>
                                                    @endif
                                                    @if($value['hasil_persalinan'] == "Meninggal")
                                                        <small class="text-muted"> Hasil : </small><span class="label label-danger">{{$value['hasil_persalinan']}}</span>
                                                    @endif
                                                    @if(explode(' ',$value['hasil_persalinan'])[0] == "Komplikasi")
                                                        <small class="text-muted"> Hasil : </small><span class="label label-warning">{{$value['hasil_persalinan']}}</span>
                                                    @endif 
                                                    <!-- @if($value['status_persalinan'] == "Normal")
                                                        span class="label label-info">{{ $value['status_persalinan'] }}</span>
                                                    @endif
                                                    @if($value['status_persalinan'] == "Komplikasi")
                                                        <span class="label label-danger">{{ $value['status_persalinan'] }}</span>
                                                    @endif -->
                                                </td>                                               
                                                <td> <span class="label label-info"> @if($value['nama_tenaga'] != null) {{$value['nama_tenaga']}} @else - @endif </span> <br> &
                                                <span class="label label-info"> @if($value['nama_tenkes'] != null) {{$value['nama_tenkes']}} @else - @endif </span> </td>
                                                <!-- <td>
                                                    @if($value['hasil_persalinan'] == "Hidup Sehat")
                                                        <span class="label label-info">{{$value['hasil_persalinan']}}</span>
                                                    @endif
                                                    @if($value['hasil_persalinan'] == "Meninggal")
                                                        <span class="label label-danger">{{$value['hasil_persalinan']}}</span>
                                                    @endif 
                                                </td> -->
                                                <td><span class="label label-info">{{$nilai[$index]}}%</span></td>  
                                                <td>                                        
                                                   <a href="{{ url('/detail-ibu-bersalin/'.$value['id'])}}">
                                                       <button type="submit" class="btn btn-info btn-xs">
                                                           Detail
                                                       </button>
                                                   </a>
                                                    
                                                <!--    <a href="{{ url('/ubah-ibu-bersalin/'.$value['id'])}}">-->
                                                <!--        <button type="submit" class="btn btn-warning btn-xs">-->
                                                <!--            Ubah-->
                                                <!--        </button>-->
                                                <!--    </a>-->

                                                   <button alt="default" class="btn btn-danger btn-xs" data-href="{{ url('/hapus-ibu-bersalin/'.$value['id'])}}" data-toggle="modal" data-target="#confirm-delete">
                                                       Hapus
                                                   </button>

                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            Hapus
                        </div>
                        <div class="modal-body">
                            Anda yakin ingin menghapus ?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            <a class="btn btn-danger btn-ok text-white">Hapus</a>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <script src="{{asset('adminbite-10/assets/libs/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/sweetalert2/sweet-alert.init.js')}}"></script>
    <script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
    </script>
</body>
</html>