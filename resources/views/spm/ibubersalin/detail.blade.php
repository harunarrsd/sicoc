<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="GIK">
<meta name="author" content="GIK">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('adminbite-10/assets/images/sicoc-favicon.png')}}">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/select2/dist/css/select2.min.css')}}">
<link href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/morris.js/morris.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/toastr/build/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet">
<style>
    .sidebar-item a{
        font-weight:600;
    }
    .tx-c{
        text-align:center;
    }
    .bg-y{
        background:yellow;
    }
    .sidebar-link .icon-Record{
        visibility: visible !important;
    }
</style>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.date.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.time.css')}}">
    <link type="text/css" href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
            <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}">
            <link href="{{asset('adminbite-10/assets/libs/sweetalert2/dist/sweetalert2.min.css')}}" rel="stylesheet">
<title>Detail Ibu Bersalin</title>
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Ibu Bersalin</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Ibu Bersalin</li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Ibu Bersalin</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
               <div class="col-12">    
                    <div class="card">
                        <div class="card-body p-b-10">
                            <div class="d-md-flex align-items-center p-b-0 m-b-0">
                                <div>
                                    <h4 class="card-title">Detail Ibu Bersalin</h4>
                                    <h6 class="card-subtitle m-b-0">Detail Pendaftaran Ibu Bersalin</h6>
                                </div>
                                <div class="ml-auto">
                                    <div class="dl">
                                        <div class="btn-group m-r-10">
                                            <!-- <a href="{{ url('/tambah-ibu-bersalin')}}" ><button type="button" class="btn btn-info m-r-5">
                                                <i class="mdi mdi-view-list font-18 "></i> Tambah Ibu Bersalin
                                            </button></a> -->
                                            <!-- <a href="{{ url('/ubah-ibu-bersalin/'.$ibubersalin['id'] )}}" ><button type="button" class="btn btn-warning">
                                                <i class="mdi mdi-view-list font-18 "></i> Ubah Ibu Bersalin
                                            </button></a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                                <div class="row">
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Nama</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="id" id="nama" disabled class="form-control bg-success text-white" style="width:92%;" aria-invalid="true">
                                                <option value="{{$ibubersalin->id}}">{{$ibubersalin->nik}} -- {{$nama_ibu_bersalin}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label for="exampleInputEmail1">Tanggal Bersalin</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                            </div>
                                            @if($ibubersalin->tanggal_bersalin != null)
                                            <input type="text" id="tanggal_bersalin" name="tanggal_bersalin" class="form-control bg-success text-white" disabled value="{{explode(' ',$ibubersalin->tanggal_bersalin)[0]}}">
                                            @else
                                            <input type="text" id="tanggal_bersalin" name="tanggal_bersalin" class="form-control bg-danger text-white" disabled value="-">
                                            @endif
                                           
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label for="exampleInputEmail1">Waktu Bersalin</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                            </div>
                                            @if($ibubersalin->tanggal_bersalin != null)
                                            <input id="timepicker" class="form-control bg-success text-white" name="waktu_bersalin" disabled placeholder="Pilih"  value="{{ explode(':',explode(' ',$ibubersalin->tanggal_bersalin)[1])[0]}}:{{ explode(':',explode(' ',$ibubersalin->tanggal_bersalin)[1])[1] }}"/>
                                            @else
                                            <input id="timepicker" class="form-control bg-danger text-white" name="waktu_bersalin" disabled placeholder="Pilih"  value="-"/>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Tanggal HPHT</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($ibubersalin->tanggal_bersalin != null)
                                            <input type="text" disabled id="tanggal_hpht" class="form-control bg-success text-white" name="tanggal_hpht" placeholder="Terisi Otomatis"/>
                                            @else
                                            <input type="text" disabled id="tanggal_hpht" class="form-control bg-danger text-white" name="tanggal_hpht" placeholder="Terisi Otomatis"/>
                                            @endif                                            
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Usia Kehamilan</label><small class="text-muted"> ketika bersalin</small>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            @if($ibubersalin->tanggal_bersalin != null)
                                            <input type="text" id="usia_kehamilan" disabled class="form-control bg-success text-white" name="usia_kehamilan" placeholder="Terisi Otomatis"/>
                                            @else
                                            <input type="text" id="usia_kehamilan" disabled class="form-control bg-danger text-white" name="usia_kehamilan" placeholder="Terisi Otomatis"/>
                                            @endif
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11">Bulan</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-6 col-md-12">
                                        <label>Fasilitas Kesehatan</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($ibubersalin->fasilitas_kesehatan != null)
                                            <select name="fas" id="selectfas" required="" aria-invalid="true" class="form-control bg-success text-white" style="width: 82%; height:36px;">
                                                <option value="">{{$ibubersalin->fasilitas_kesehatan}}</option>
                                            @else
                                            <select name="fas" id="selectfas" required="" aria-invalid="true" class="form-control bg-danger text-white" style="width: 82%; height:36px;">
                                                <option value="">-</option>
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Nama Fasilitas Pelayanan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-map-alt"></i></span>
                                            </div>
                                            @if($ibubersalin->lokasi_fasilitas != null)
                                            <select name="selectlok" id="selectlok" required="" aria-invalid="true" class="form-control bg-success text-white" style="width: 82%; height:36px;">
                                                <option value="">{{$ibubersalin->lokasi_fasilitas}}</option>
                                            @else
                                            <select name="selectlok" id="selectlok" required="" aria-invalid="true" class="form-control bg-danger text-white" style="width: 82%; height:36px;">
                                                <option value="">-</option>
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Tenaga Kesehatan 1</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            @if($ibubersalin->tenaga_kesehatan != null)
                                            <select name="selecttenaga" id="selecttenaga" required="" aria-invalid="true" class="form-control bg-success text-white" style="width: 82%; height:36px;">
                                                <option value="">{{$ibubersalin->tenaga_kesehatan}}</option>
                                            @else
                                            <select name="selecttenaga" id="selecttenaga" required="" aria-invalid="true" class="form-control bg-danger text-white" style="width: 82%; height:36px;">
                                                <option value="">-</option>
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Nama Tenaga Kesehatan 1</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            @if($ibubersalin->nama_tenaga != null)
                                            <select name="selectnamaSTK" id="selectnamaSTK" required="" aria-invalid="true" class="form-control bg-success text-white" style="width: 82%; height:36px;">
                                                <option value="">{{$ibubersalin->nama_tenaga}}</option>
                                            @else
                                            <select name="selectnamaSTK" id="selectnamaSTK" required="" aria-invalid="true" class="form-control bg-danger text-white" style="width: 82%; height:36px;">
                                                <option value="">-</option>
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Tenaga Kesehatan 2</label> 
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            @if($ibubersalin->tenkes != null)
                                            <select name="selecttenaga2" id="selecttenaga2" required="" aria-invalid="true" class="form-control bg-success text-white" style="width: 82%; height:36px;">
                                                <option value="">{{$ibubersalin->tenkes}}</option>
                                            @else
                                            <select name="selecttenaga2" id="selecttenaga2" required="" aria-invalid="true" class="form-control bg-danger text-white" style="width: 82%; height:36px;">
                                                <option value="">-</option>
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Nama Tenaga Kesehatan 2</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            @if($ibubersalin->nama_tenkes != null)
                                            <select name="selectnamaSTK2" id="selectnamaSTK2" required="" aria-invalid="true" class="form-control bg-success text-white" style="width: 82%; height:36px;">
                                                <option value="">{{$ibubersalin->nama_tenkes}}</option>
                                            @else
                                            <select name="selectnamaSTK2" id="selectnamaSTK2" required="" aria-invalid="true" class="form-control bg-danger text-white" style="width: 82%; height:36px;">
                                                <option value="">-</option>
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Tindakan</label><span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            @if($ibubersalin->tindakan != null)
                                            <select name="tindakan" id="tindakan" required="" aria-invalid="true" class="form-control bg-success text-white" style="width: 82%; height:36px;">
                                                <option value="">{{$ibubersalin->tindakan}}</option>
                                            @else
                                            <select name="tindakan" id="tindakan" required="" aria-invalid="true" class="form-control bg-danger text-white" style="width: 82%; height:36px;">
                                                <option value="">-</option>
                                            @endif
                                            </select>                                            
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Partograf 1</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <!-- <img src="images/noimg.jpg" id="blah" style="display:none;"> -->
                                            @if($ibubersalin->link_gambar_partograf1 != null)
                                            <input type="file" id="image1" name="image1" disabled class="dropzone form-control bg-success text-white" value="{{link_gambar_partograf1}}"> <div class="help-block"></div>
                                            @else
                                            <input type="file" id="image1" name="image1" disabled class="dropzone form-control bg-danger text-white"> <div class="help-block"></div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Partograf 2</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($ibubersalin->link_gambar_partograf1 != null)
                                            <input type="file" id="image2" name="image2" disabled class="dropzone form-control bg-success text-white" value="{{link_gambar_partograf1}}"> <div class="help-block"></div>
                                            @else
                                            <input type="file" id="image2" name="image2" disabled class="dropzone form-control bg-danger text-white"> <div class="help-block"></div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Status Persalinan</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($ibubersalin->status_persalinan != null)
                                            <select name="st_persalinan" id="st_persalinan" required="" aria-invalid="true" class="form-control bg-success text-white" style="width: 82%; height:36px;">
                                                <option value="">{{$ibubersalin->status_persalinan}}</option>
                                            @else
                                            <select name="st_persalinan" id="st_persalinan" required="" aria-invalid="true" class="form-control bg-danger text-white" style="width: 82%; height:36px;">
                                                <option value="">-</option>
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Hasil Persalinan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($ibubersalin->hasil_persalinan != null)
                                            <select name="hs_persalinan" id="hs_persalinan" required="" aria-invalid="true" class="form-control bg-success text-white" style="width: 82%; height:36px;">
                                                <option value="">{{$ibubersalin->hasil_persalinan}}</option>
                                            @else
                                            <select name="hs_persalinan" id="hs_persalinan" required="" aria-invalid="true" class="form-control bg-danger text-white" style="width: 82%; height:36px;">
                                                <option value="">-</option>
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    
    @yield('js-ripel01')
    <script>
            var tgl = document.getElementById("tanggal_bersalin").value;
            var wkt = document.getElementById("timepicker").value;
            var id_ibuhamil = document.getElementById("nama").value;
            if(tgl =="-" || wkt == "-"){
                document.getElementById("usia_kehamilan").value = "-";
                document.getElementById("tanggal_hpht").value = "-";
            }
            else{
                $.ajax({
                    type:'GET',
                    url:'/getUsiaKehamilan',
                    data:{tgl:tgl,wkt:wkt,id:id_ibuhamil},
                    dataType:'json',
                    success:function(data){
                        console.log(data.data.months);
                        document.getElementById("usia_kehamilan").value = data.data.months;
                        document.getElementById("tanggal_hpht").value = data.hpht;
                    }
                });
            }
    </script>
        <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/select2/select2.init.js')}}"></script>
    <!-- Picker Date Style -->
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.date.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.time.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/legacy.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <!-- <script src="{{asset('adminbite-10/assets/libs/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/datetimepicker/datetimepicker.init.js')}}"></script> -->
    
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js')}}"></script>
    <script>
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    $('#date-fr').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', lang: 'fr', weekStart: 1, cancelText: 'ANNULER' });
    $('#date-end').bootstrapMaterialDatePicker({ weekStart: 0 });
    $('#date-start').bootstrapMaterialDatePicker({ weekStart: 0 }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });
    </script>
    <script src="{{asset('adminbite-10/assets/libs/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/sweetalert2/sweet-alert.init.js')}}"></script>
    <script>
    </script>
</body>
</html>