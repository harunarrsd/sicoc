<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>SPM Ibu Bersalin</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Ibu Bersalin</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Ibu Bersalin</li>
                                    <li class="breadcrumb-item active" aria-current="page">SPM Ibu Bersalin</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
                <div class="card-body bg-light">
                    <div class="d-md-flex align-items-center">
                        <div><h2>Pelayanan Ibu Bersalin</h2></div>
                        <div class="ml-auto">
                            <div class="dl">
                                <!-- <div class="btn-group m-r-10" role="group" aria-label="Button group with nested dropdown" style="folat:right;">
                                    <a href="javascript:void(0)" id="to-pernyataan_dasar"><button type="button" class="btn btn-outline-secondary font-18" data-toggle="tooltip" data-placement="top" title="Pernyataan Standar">
                                        <i class="mdi mdi-book-open-page-variant"></i>
                                    </button></a>
                                    <div class="btn-group" role="group">
                                        <button id="btnGroupDrop1" type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="mdi mdi-view-list font-18 " data-toggle="tooltip" data-placement="top" title="Antenatal & 10T"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                            <a class="dropdown-item" href="javascript:void(0)" id="to-antenatal">Pelayanan Antenatal</a>
                                            <a class="dropdown-item" href="javascript:void(0)" id="to-k10t">Kriteria 10T</a>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0)" id="to-docp"><button type="button" class="btn btn-outline-secondary font-18 " data-toggle="tooltip" data-placement="top" title="Definisi Operasional Capaian Kinerja">
                                        <i class="mdi mdi-check-circle"></i>
                                    </button></a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Penyataan Standar</h4>
                            Setiap ibu bersalin mendapatkan pelayanan persalinan sesuai standar. 
                            Pemerintah Daerah Kabupaten/Kota wajib memberikan pelayanan 
                            kesehatan ibu Bersalin kepada semua ibu bersalin di wilayah kerjanya 
                            dalam kurun waktu satu tahun.
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Pelayanan Persalinan sesuai Standar</h4>
                            Pelayanan persalinan sesuai standar adalah persalinan yang
                            dilakukan oleh Bidan dan atau Dokter dan atau Dokter Spesialis 
                            Kebidanan yang bekerja di fasilitas pelayanan kesehatan Pemerintah 
                            maupun Swasta yang memiliki Surat Tanda Register (STR) baik 
                            persalinan normal dan atau persalinan dengan komplikasi.
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Fasilitas Pelayanan Kesehatan</h4>
                            Fasilitas pelayanan kesehatan meliputi : 
                            <h6>a) Polindes;</h6>
                            <h6>b) Poskesdes;</h6>
                            <h6>c) Puskesmas;</h6>
                            <h6>d) Bidan praktek Swasta;</h6>
                            <h6>e) Klinik Pratama;</h6>
                            <h6>f) Klinik Utama; </h6>
                            <h6>g) Klinik Bersalin;</h6>
                            <h6>h) Balai Kesehatan Ibu dan Anak;</h6>
                            <h6>i) Rumah Sakit Pemerintah maupun Swasta</h6>
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Acuan Standar Persalinan</h4>
                            <h6> Standar pelayanan persalinan <b> normal </b> mengikuti acuan asuhan 
                                persalinan normal yang tercantum dalam <b> Peraturan Menteri
                                Kesehatan Nomor 97 Tahun 2014 </b> tentang Pelayanan Kesehatan
                                Masa Sebelum Hamil, Masa Hamil, Persalinan, dan Masa Sesudah 
                                Melahirkan, Penyelenggaraan Pelayanan Kontrasepsi, serta 
                                Pelayanan Kesehatan Seksual.</h6>
                            <h6> Adapun untuk persalinan dengan <b> komplikasi </b> mengikuti acuan  
                                dari <b> Buku Saku Pelayanan Kesehatan Ibu </b> di Fasilitas Rujukan.</h6>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Definisi Operasional Capaian Kerja</h4>
                            Capaian Kinerja Pemerintah Daerah Kabupaten/Kota dalam memberikan
                            pelayanan kesehatan ibu bersalin dinilai dari cakupan pelayanan 
                            kesehatan ibu bersalin sesuai standar di wilayah kabupaten/kota 
                            tersebut dalam kurun waktu satu tahun.
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4>
                        <i class="fas fa-chevron-circle-right m-r-10 m-b-10"></i> Referensi
                    </h4>
                    <h6><i class="fas fa-square"></i> Keputusan Menteri Kesehatan Nomor 284/MENKES/SK/III/2004 tentang Buku Kesehatan Ibu dan Anak;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 1464/X/Menkes/2010 tentang Izin dan Penyelenggaraan Praktik Bidan;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 56 Tahun 2014 tentang Izin dan Klasifikasi Rumah Sakit;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 75 Tahun 2014 tentang Pusat Kesehatan Masyarakat;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 97 Tahun 2014 tentang Pelayanan Kesehatan Masa Sebelum Hamil, Masa Hamil, Persalinan, dan Masa Sesudah Melahirkan, Penyelenggaraan Pelayanan Kontrasepsi, serta Pelayanan Kesehatan Seksual;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Konsil Kedokteran Indonesia Nomor 11 Tahun 2012 tentang Standar Kompetensi Dokter Indonesia;</h6>
                    <h6><i class="fas fa-square"></i> Buku Saku Pelayanan Kesehatan Ibu di Fasilitas Kesehatan Rujukan, Kementrian Kesehatan, 2013;</h6>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <!-- <script>
    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();
    $("#antenatal").fadeOut();
    $("#k10t").fadeOut();
    $("#docp").fadeOut();
    $('#to-antenatal').on("click", function() {
        $("#pernyataan_dasar").slideUp();
        $("#k10t").slideUp();
        $("#docp").slideUp();
        $("#antenatal").slideDown();
    });
    $('#to-k10t').on("click", function() {
        $("#pernyataan_dasar").slideUp();
        $("#antenatal").slideUp();
        $("#docp").slideUp();
        $("#k10t").slideDown();
    });
    $('#to-docp').on("click", function() {
        $("#pernyataan_dasar").slideUp();
        $("#antenatal").slideUp();
        $("#k10t").slideUp();
        $("#docp").slideDown();
    });
    $('#to-pernyataan_dasar').on("click", function() {
        $("#docp").slideUp();
        $("#antenatal").slideUp();
        $("#k10t").slideUp();
        $("#pernyataan_dasar").slideDown();
    });
    </script> -->
</body>
</html>