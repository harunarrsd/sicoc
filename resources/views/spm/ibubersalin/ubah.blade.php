<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="GIK">
<meta name="author" content="GIK">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('adminbite-10/assets/images/sicoc-favicon.png')}}">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/select2/dist/css/select2.min.css')}}">
<link href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/morris.js/morris.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/toastr/build/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet">
<style>
    .sidebar-item a{
        font-weight:600;
    }
    .tx-c{
        text-align:center;
    }
    .bg-y{
        background:yellow;
    }
    .sidebar-link .icon-Record{
        visibility: visible !important;
    }
</style>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.date.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.time.css')}}">
    <link type="text/css" href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
            <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}">
            <link href="{{asset('adminbite-10/assets/libs/sweetalert2/dist/sweetalert2.min.css')}}" rel="stylesheet">
<title>Ubah Ibu Bersalin</title>
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Ibu Bersalin</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Ibu Bersalin</li>
                                    <li class="breadcrumb-item active" aria-current="page">Ubah Ibu Bersalin</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
               <div class="col-12">    
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Ubah Ibu Bersalin</h4>
                            <h6 class="card-subtitle m-b-30">Perubahan Data Ibu Bersalin</h6>
                            <form method="POST" action="{{ url('/simpanubah-ibu-bersalin') }}" enctype="multipart/form-data" >{{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Nama</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="id" id="nama" required="" class="form-control select2 custom-select" style="width:92%;" aria-invalid="true" onchange="NamaChange();">
                                                <option value="">Pilih</option>
                                                <option value="{{$ibubersalin['id']}}" selected>{{$data_nik_ibubersalin['nik']}} -- {{$data_nik_ibubersalin['nama']}}</option>
                                                @foreach($list_name as $i => $name)                    
                                                    <option value="{{ $list_id[$i] }}">{{ $list_nik[$i] }} -- {{$name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label for="exampleInputEmail1">Tanggal Bersalin</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                            </div>
                                            <input type="date" class="pickadate-disable form-control required" id="tanggal_bersalin" name="tanggal_bersalin" placeholder="Pilih" required onchange="tglbersalinChange();"/>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label for="exampleInputEmail1">Waktu Bersalin</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                            </div>
                                            <input id="timepicker" class="form-control required" name="waktu_bersalin" required placeholder="Pilih" value="{{ explode(':',explode(' ',$ibubersalin['tanggal_bersalin'])[1])[0] }}:{{ explode(':',explode(' ',$ibubersalin['tanggal_bersalin'])[1])[1] }}" onchange="tglbersalinChange();"/>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Tanggal HPHT</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="date" disabled id="tanggal_hpht" class="form-control pickadate-disable" name="tanggal_hpht" placeholder="Terisi Otomatis"/>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Usia Kehamilan</label><small class="text-muted"> ketika bersalin</small>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" id="usia_kehamilan" disabled class="form-control required" name="usia_kehamilan" placeholder="Terisi Otomatis"/>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11">Bulan</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-6 col-md-12">
                                        <label>Fasilitas Kesehatan</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="fas" id="selectfas" required="" aria-invalid="true" class="form-control select2 custom-select" style="width: 82%; height:36px;" onchange="getMitra()">
                                                @if($ibubersalin['fasilitas_kesehatan'] == "Rumah Sakit")
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit" selected>Rumah Sakit</option>
                                                <option value="Polindes">Polindes</option>
                                                <option value="Poskesdes">Poskesdes</option>
                                                <option value="Puskesmas">Puskesmas</option>
                                                <option value="Klinik">Kilnik</option>
                                                <option value="Posyandu">Posyandu</option>
                                                <option value="BPM">BPM</option>
                                                <option value="Luar Wilayah">Luar Wilayah</option>
                                                <option value="Lainnya">Lainnya</option>
                                                @elseif($ibubersalin['fasilitas_kesehatan'] == "Polindes")
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                <option value="Polindes" selected>Polindes</option>
                                                <option value="Poskesdes">Poskesdes</option>
                                                <option value="Puskesmas">Puskesmas</option>
                                                <option value="Klinik">Kilnik</option>
                                                <option value="Posyandu">Posyandu</option>
                                                <option value="BPM">BPM</option>
                                                <option value="Luar Wilayah">Luar Wilayah</option>
                                                <option value="Lainnya">Lainnya</option>
                                                @elseif($ibubersalin['fasilitas_kesehatan'] == "Poskesdes")
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                <option value="Polindes">Polindes</option>
                                                <option value="Poskesdes" selected>Poskesdes</option>
                                                <option value="Puskesmas">Puskesmas</option>
                                                <option value="Klinik">Kilnik</option>
                                                <option value="Posyandu">Posyandu</option>
                                                <option value="BPM">BPM</option>
                                                <option value="Luar Wilayah">Luar Wilayah</option>
                                                <option value="Lainnya">Lainnya</option>
                                                @elseif($ibubersalin['fasilitas_kesehatan'] == "Puskesmas")
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                <option value="Polindes">Polindes</option>
                                                <option value="Poskesdes">Poskesdes</option>
                                                <option value="Puskesmas" selected>Puskesmas</option>
                                                <option value="Klinik">Kilnik</option>
                                                <option value="Posyandu">Posyandu</option>
                                                <option value="BPM">BPM</option>
                                                <option value="Luar Wilayah">Luar Wilayah</option>
                                                <option value="Lainnya">Lainnya</option>
                                                @elseif($ibubersalin['fasilitas_kesehatan'] == "Kilnik")
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                <option value="Polindes">Polindes</option>
                                                <option value="Poskesdes">Poskesdes</option>
                                                <option value="Puskesmas">Puskesmas</option>
                                                <option value="Klinik" selected>Kilnik</option>
                                                <option value="Posyandu">Posyandu</option>
                                                <option value="BPM">BPM</option>
                                                <option value="Luar Wilayah">Luar Wilayah</option>
                                                <option value="Lainnya">Lainnya</option>
                                                @elseif($ibubersalin['fasilitas_kesehatan'] == "Posyandu")
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                <option value="Polindes">Polindes</option>
                                                <option value="Poskesdes">Poskesdes</option>
                                                <option value="Puskesmas">Puskesmas</option>
                                                <option value="Klinik">Kilnik</option>
                                                <option value="Posyandu" selected>Posyandu</option>
                                                <option value="BPM">BPM</option>
                                                <option value="Luar Wilayah">Luar Wilayah</option>
                                                <option value="Lainnya">Lainnya</option>
                                                @elseif($ibubersalin['fasilitas_kesehatan'] == "BPM")
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                <option value="Polindes">Polindes</option>
                                                <option value="Poskesdes">Poskesdes</option>
                                                <option value="Puskesmas">Puskesmas</option>
                                                <option value="Klinik">Kilnik</option>
                                                <option value="Posyandu">Posyandu</option>
                                                <option value="BPM" selected>BPM</option>
                                                <option value="Luar Wilayah">Luar Wilayah</option>
                                                <option value="Lainnya">Lainnya</option>
                                                @elseif($ibubersalin['fasilitas_kesehatan'] == "Luar Wilayah")
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                <option value="Polindes">Polindes</option>
                                                <option value="Poskesdes">Poskesdes</option>
                                                <option value="Puskesmas">Puskesmas</option>
                                                <option value="Klinik">Kilnik</option>
                                                <option value="Posyandu">Posyandu</option>
                                                <option value="BPM">BPM</option>
                                                <option value="Luar Wilayah" selected>Luar Wilayah</option>
                                                <option value="Lainnya">Lainnya</option>
                                                @elseif($ibubersalin['fasilitas_kesehatan'] == "Lainnya")
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                <option value="Polindes">Polindes</option>
                                                <option value="Poskesdes">Poskesdes</option>
                                                <option value="Puskesmas">Puskesmas</option>
                                                <option value="Klinik">Kilnik</option>
                                                <option value="Posyandu">Posyandu</option>
                                                <option value="BPM">BPM</option>
                                                <option value="Luar Wilayah">Luar Wilayah</option>
                                                <option value="Lainnya" selected>Lainnya</option>
                                                @else
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                <option value="Polindes">Polindes</option>
                                                <option value="Poskesdes">Poskesdes</option>
                                                <option value="Puskesmas">Puskesmas</option>
                                                <option value="Klinik">Kilnik</option>
                                                <option value="Posyandu">Posyandu</option>
                                                <option value="BPM">BPM</option>
                                                <option value="Luar Wilayah">Luar Wilayah</option>
                                                <option value="Lainnya">Lainnya</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Nama Fasilitas Pelayanan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-map-alt"></i></span>
                                            </div>
                                            <select name="selectlok" id="selectlok" aria-invalid="true" class="form-control select2 custom-select" style="width: 82%; height:36px;" onchange="getTenaga()">
                                                <option value="">Pilih</option> 
                                                 
                                            </select>
                                            <input type="text" name="lokasi_pelayanan" id="lokasi_pelayanan"  value="{{ $ibubersalin['lokasi_fasilitas']}}" class="form-control" style="display:none;" >
                                            <input type="text" name="created_by" id="created_by" class="form-control" value="{{ Auth::user()['name'] }}" style="display:none;" >
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Tenaga Kesehatan 1</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="selecttenaga" id="selecttenaga" class="form-control select2 custom-select" aria-invalid="true" style="width: 82%; height:36px;" onchange="getNamaStk()">
                                                <option value="">Pilih</option>
                                            </select>
                                            <input type="text" name="tenaga_kesehatan" id="tenaga_kesehatan" value="{{$ibubersalin['tenaga_kesehatan']}}" class="form-control" style="display:none;" >
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Nama Tenaga Kesehatan 1</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="selectnamaSTK" id="selectnamaSTK" aria-invalid="true" class="form-control select2 custom-select" style="width: 82%; height:36px;" onchange="getNamaStk2()">
                                                <option value="">Pilih</option>                                                    
                                            </select>
                                            <input type="text" id="nama_stk" name="nama_stk" value="{{ $ibubersalin['nama_tenaga']}}" class="form-control"  style="display:none;">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Tenaga Kesehatan 2</label> 
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="selecttenaga2" id="selecttenaga2" class="form-control select2 custom-select" aria-invalid="true" style="width: 82%; height:36px;" onchange="getNamaStkk()">
                                                <option value="">Pilih</option>
                                                
                                            </select>
                                            <input type="text" name="tenkes" id="tenkes" value="{{$ibubersalin['tenkes']}}" class="form-control"  style="display:none;">
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Nama Tenaga Kesehatan 2</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="selectnamaSTK2" id="selectnamaSTK2" aria-invalid="true" class="form-control select2 custom-select" style="width: 82%; height:36px;" onchange="getNamaStkk2()">
                                                <option value="">Pilih</option>                                                    
                                            </select>
                                            <input type="text" id="nama_tenkes" name="nama_tenkes" class="form-control" value="{{$ibubersalin['nama_tenkes']}}" style="display:none;">
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Tindakan</label><span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="tindakan" id="tindakan" required aria-invalid="true" class="form-control select2 custom-select" style="width: 92%; height:36px;">
                                                @if($ibubersalin['tindakan'] == "Dilayani")
                                                <option value="">Pilih</option>                                                    
                                                <option value="Dilayani" selected>Ditangani/Dilayani</option>                                                    
                                                <option value="Dirujuk">Dirujuk</option>    
                                                @elseif($ibubersalin['tindakan'] == "Dirujuk")
                                                <option value="">Pilih</option>                                                    
                                                <option value="Dilayani">Ditangani/Dilayani</option>                                                    
                                                <option value="Dirujuk" selected>Dirujuk</option>
                                                @else
                                                <option value="">Pilih</option>                                                    
                                                <option value="Dilayani">Ditangani/Dilayani</option>                                                    
                                                <option value="Dirujuk">Dirujuk</option>
                                                @endif
                                            </select>                                            
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Partograf 1</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <!-- <img src="images/noimg.jpg" id="blah" style="display:none;"> -->
                                            <input type="file" id="image1" name="image1" class="dropzone form-control" value="{{$ibubersalin['link_gambar_partograf1']}}" onchange="return ValidateFileUpload();"> <div class="help-block"></div>                                           
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Partograf 2</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="file" name="image2" id="image2" class="dropzone form-control" value="{{$ibubersalin['link_gambar_partograf2']}}" onchange="return ValidateFileUpload2();"> <div class="help-block"></div>                                           
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Status Persalinan</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="st_persalinan" id="st_persalinan" required="" class="form-control select2 custom-select"  style="width:92%;" aria-invalid="true" onchange="stChange();">
                                                <option value="">Pilih</option>
                                                <option value="Normal">Normal</option>
                                                <option value="Komplikasi">Komplikasi</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Hasil Persalinan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="hs_persalinan" id="hs_persalinan" class="form-control select2 custom-select"  style="width:92%;" aria-invalid="true" onchange="hsChange();">
                                                <option value="">Pilih</option>
                                                <option value="Meninggal">Meninggal</option>
                                                <option value="Hidup Sehat">Hidup Sehat</option>
                                                <option value="Komplikasi Cs">Komplikasi Cs</option>
                                                <option value="Komplikasi V">Komplikasi V</option>
                                                <option value="Komplikasi F">Komplikasi F</option>
                                            </select>
                                            <input type="text" id="hasil_persalinan" name="hasil_persalinan" class="form-control" style="display:none;">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                    <button type="submit" class="btn btn-info">Kirimkan ></button> 
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <script type="text/javascript">
        var st = "{{ $ibubersalin->status_persalinan}}";
        document.getElementById("st_persalinan").value = st;

        var hs = document.getElementById("hs_persalinan").value;

        if(st == "Normal"){
            $('#hs_persalinan').find('option').not(':first').remove();   
            var option = "<option value='Meninggal'> Meninggal </option> <option value='Hidup Sehat'> Hidup Sehat </option>";
            $("#hs_persalinan").append(option); 
            document.getElementById("hs_persalinan").value = "{{$ibubersalin->hasil_persalinan}}";
        }
        else if(st == "Komplikasi"){
            $('#hs_persalinan').find('option').not(':first').remove();
            var option = "<option value='Komplikasi Cs'> Komplikasi Cs </option> <option value='Komplikasi V'> Komplikasi V </option> <option value='Komplikasi F'> Komplikasi F </option>";
            $("#hs_persalinan").append(option);
            document.getElementById("hs_persalinan").value = "{{$ibubersalin->hasil_persalinan}}";
        }
        else{
            $('#hs_persalinan').find('option').not(':first').remove();
            var option = "<option value='Meninggal'> Meninggal </option> <option value='Hidup Sehat'> Hidup Sehat </option> <option value='Komplikasi Cs'> Komplikasi Cs </option> <option value='Komplikasi V'> Komplikasi V </option> <option value='Komplikasi F'> Komplikasi F </option>";
            $("#hs_persalinan").append(option);
        }

        var fas = document.getElementById("selectfas").value;

        $('#selectlok').find('option').not(':first').remove();
        $.ajax({
            type:'GET',
            url:'/findmitra',
            data:{id:fas},
            dataType:'json',
            success:function(data){
                var len = 0;
                if(data['data'] != null){
                    len = data['data'].length;
                }
                var idgetmitra = 0;
                for(var i=0;i<len;i++){
                    var id = data['data'][i]['id'];
                    var jenis = data['data'][i]['jenis_mitra'];
                    var nama = data['data'][i]['nama'];
                    var lok = document.getElementById("lokasi_pelayanan").value;
                    if(jenis+" "+nama == lok){
                        var option = "<option value='"+id+"_"+jenis+" "+nama+"' selected>"+jenis+" "+nama+"</option>";
                        idgetmitra = id+"_"+jenis+" "+nama;
                    }else{
                        var option = "<option value='"+id+"_"+jenis+" "+nama+"'>"+jenis+" "+nama+"</option>";
                    }
                    $("#selectlok").append(option);
                }

                var lok_ok = idgetmitra.split("_");
                $.ajax({
                    type:'GET',
                    url:'/findtenaga',
                    data:{id:lok_ok[0]},
                    dataType:'json',
                    success:function(data){
                        var idgettenaga = 0;
                        var tenaga = data['data'].split("_");
                        for(var i=0;i<tenaga.length;i++){
                            var tenaga_kes = document.getElementById("tenaga_kesehatan").value;
                            if(tenaga[i] == tenaga_kes){
                                var option = "<option value='"+tenaga[i]+"' selected>"+tenaga[i]+"</option>";                                    
                                idgettenaga = tenaga[i];
                            }else{
                                var option = "<option value='"+tenaga[i]+"'>"+tenaga[i]+"</option>";
                            }
                            $("#selecttenaga").append(option);
                        }
                        var idgettenaga2 = 0;
                        var tenaga2 = data['data'].split("_");
                        for(var i=0;i<tenaga2.length;i++){
                            var tenaga_kes2 = document.getElementById("tenkes").value;
                            if(tenaga[i] == tenaga_kes2){
                                var option = "<option value='"+tenaga[i]+"' selected>"+tenaga[i]+"</option>";                                    
                                idgettenaga2 = tenaga[i];
                            }else{
                                var option = "<option value='"+tenaga[i]+"'>"+tenaga[i]+"</option>";
                            }
                            $("#selecttenaga2").append(option);
                        }
                        $.ajax({
                            type:'GET',
                            url:'/findnamastk',
                            data:{tenaga:idgettenaga,mitra:idgetmitra},
                            dataType:'json',
                            success:function(data){
                                var nama = data['data'].split("_");
                                for(var i=0;i<nama.length;i++){
                                    var nama_stk = document.getElementById("nama_stk").value;
                                    if(nama[i] == nama_stk){
                                        var option = "<option value='"+nama[i]+"' selected>"+nama[i]+"</option>";                                            
                                    }else{
                                        var option = "<option value='"+nama[i]+"'>"+nama[i]+"</option>";
                                    }
                                    $("#selectnamaSTK").append(option);
                                }
                            }
                        });
                        $.ajax({
                            type:'GET',
                            url:'/findnamastk',
                            data:{tenaga:idgettenaga2,mitra:idgetmitra},
                            dataType:'json',
                            success:function(data){
                                var nama2 = data['data'].split("_");
                                for(var i=0;i<nama2.length;i++){
                                    var nama_tenkes = document.getElementById("nama_tenkes").value;
                                    if(nama2[i] == nama_tenkes){
                                        var option = "<option value='"+nama2[i]+"' selected>"+nama2[i]+"</option>";                                            
                                    }else{
                                        var option = "<option value='"+nama2[i]+"'>"+nama2[i]+"</option>"; 
                                    }
                                    $("#selectnamaSTK2").append(option);
                                }
                            }
                        });
                    } 
                });
            }
        });



    </script>
    <script type="text/javascript">
        function hsChange(){
            var hs_persalinan = document.getElementById("hs_persalinan").value;
            document.getElementById("hasil_persalinan").value = hs_persalinan;
        }
        function ValidateFileUpload() {
            var fuData = document.getElementById('image1');
            var FileUploadPath = fuData.value;

            //To check if user upload any file
            if (FileUploadPath == '') {
                swal("Unggah gambar");
                // alert("Please upload an image");

            }else {
                var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
                //The file uploaded is an image
                if (Extension == "jpeg" || Extension == "png" || Extension == "jpg" || Extension == "gif") {
                    // To Display
                    if (fuData.files && fuData.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#blah').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(fuData.files[0]);
                    }
                } 
                //The file upload is NOT an image
                else {
                    swal("Foto hanya memungkinkan jenis file GIF, PNG, JPG, dan JPEG.");
                    // alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
                    document.getElementById('image1').value = '';
                }
            }
        }
        function ValidateFileUpload2() {
            var fuData = document.getElementById('image2');
            var FileUploadPath = fuData.value;

            //To check if user upload any file
            if (FileUploadPath == '') {
                swal("Unggah gambar");
                // alert("Please upload an image");

            }else {
                var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
                //The file uploaded is an image
                if (Extension == "gif" || Extension == "png" || Extension == "jpeg" || Extension == "jpg") {
                    // To Display
                    if (fuData.files && fuData.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#blah').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(fuData.files[0]);
                    }
                } 
                //The file upload is NOT an image
                else {
                    swal("Foto hanya memungkinkan jenis file GIF, PNG, JPG, dan JPEG.");
                    // alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
                    document.getElementById('image2').value = '';
                }
            }
        }
    </script>
    <script>
        function hsChange(){
            var hs_persalinan = document.getElementById("hs_persalinan").value;
            document.getElementById("hasil_persalinan").value = hs_persalinan;
        }
        function stChange(){
            var st = document.getElementById("st_persalinan").value;
            var hs = document.getElementById("hs_persalinan").value;

            if(st == "Normal"){
                $('#hs_persalinan').find('option').not(':first').remove();   
                var option = "<option value='Meninggal'> Meninggal </option> <option value='Hidup Sehat'> Hidup Sehat </option>";
                $("#hs_persalinan").append(option); 
            }
            else if(st == "Komplikasi"){
                $('#hs_persalinan').find('option').not(':first').remove();
                var option = "<option value='Komplikasi Cs'> Komplikasi Cs </option> <option value='Komplikasi V'> Komplikasi V </option> <option value='Komplikasi F'> Komplikasi F </option>";
                $("#hs_persalinan").append(option);
            }
            else{
                $('#hs_persalinan').find('option').not(':first').remove();
                var option = "<option value='Meninggal'> Meninggal </option> <option value='Hidup Sehat'> Hidup Sehat </option> <option value='Komplikasi Cs'> Komplikasi Cs </option> <option value='Komplikasi V'> Komplikasi V </option> <option value='Komplikasi F'> Komplikasi F </option>";
                $("#hs_persalinan").append(option);
            }            
        }
        function NamaChange(){
            var tgl = document.getElementById("tanggal_bersalin").value;
            var wkt = document.getElementById("timepicker").value;
            var id_ibuhamil = document.getElementById("nama").value;
            $.ajax({
                type:'GET',
                url:'/getUsiaKehamilan',
                data:{tgl:tgl,wkt:wkt,id:id_ibuhamil},
                dataType:'json',
                success:function(data){
                    var split = data.hpht.split("-");
                    var JakartaTime = new Date(split[0],split[1]-1,split[2]).toLocaleString("en-US", {timeZone: "Asia/Jakarta"});
                    if(JakartaTime == "11/30/1899, 12:25:08 AM"){
                        JakartaTime = "00/00/000, 12:25:08 AM";
                    }
                    var date = new Date(JakartaTime);
                    document.getElementById("usia_kehamilan").value = data.data.months;
                    var picker = $('#tanggal_hpht').pickadate('picker');
                    picker.set('select', date);
                }
            });
        }
        function tglbersalinChange(){
            var tgl = document.getElementById("tanggal_bersalin").value;
            var wkt = document.getElementById("timepicker").value;
            var id_ibuhamil = document.getElementById("nama").value;
            $.ajax({
                type:'GET',
                url:'/getUsiaKehamilan',
                data:{tgl:tgl,wkt:wkt,id:id_ibuhamil},
                dataType:'json',
                success:function(data){
                    var split = data.hpht.split("-");
                    var JakartaTime = new Date(split[0],split[1]-1,split[2]).toLocaleString("en-US", {timeZone: "Asia/Jakarta"});
                    if(JakartaTime == "11/30/1899, 12:25:08 AM"){
                        JakartaTime = "00/00/000, 12:25:08 AM";
                    }
                    var date = new Date(JakartaTime);
                    document.getElementById("usia_kehamilan").value = data.data.months;
                    var picker = $('#tanggal_hpht').pickadate('picker');
                    picker.set('select', date);
                }
            });
        }
        function getMitra(){
            var fas = document.getElementById("selectfas").value;

            $('#selectlok').find('option').not(':first').remove();
            $('#selecttenaga').find('option').not(':first').remove();
            $('#selecttenaga2').find('option').not(':first').remove();
            $('#selectnamaSTK').find('option').not(':first').remove();
            $('#selectnamaSTK2').find('option').not(':first').remove();
            $.ajax({
                type:'GET',
                url:'/findmitra',
                data:{id:fas},
                dataType:'json',
                success:function(data){
                    var len = 0;
                    if(data['data'] != null){
                        len = data['data'].length;
                    }
                    for(var i=0;i<len;i++){
                        var id = data['data'][i]['id'];
                        var jenis = data['data'][i]['jenis_mitra'];
                        var nama = data['data'][i]['nama'];

                        var option = "<option value='"+id+"_"+jenis+" "+nama+"'>"+jenis+" "+nama+"</option>";
                        $("#selectlok").append(option);
                    }
                    
                }
            });
        }
        function getTenaga(){
            var lok = document.getElementById("selectlok").value;
            var lok_ok = lok.split("_");
            $('#selectnamaSTK').find('option').not(':first').remove();
            $('#selectnamaSTK2').find('option').not(':first').remove();
            $('#selecttenaga').find('option').not(':first').remove();
            $('#selecttenaga2').find('option').not(':first').remove();
            $.ajax({
                type:'GET',
                url:'/findtenaga',
                data:{id:lok_ok[0]},
                dataType:'json',
                success:function(data){
                    var tenaga = data['data'].split("_");
                    for(var i=0;i<tenaga.length;i++){
                        var option = "<option value='"+tenaga[i]+"'>"+tenaga[i]+"</option>";
                        $("#selecttenaga").append(option);
                        $("#selecttenaga2").append(option);
                    }
                }
            });
            document.getElementById("lokasi_pelayanan").value = lok_ok[1];
        }
        function getNamaStk(){
            var tenaga = document.getElementById("selecttenaga").value;
            var lok = document.getElementById("selectlok").value;
            var lok_ok = lok.split("_");
            $('#selectnamaSTK').find('option').not(':first').remove();
            $.ajax({
                type:'GET',
                url:'/findnamastk',
                data:{tenaga:tenaga,mitra:lok_ok[0]},
                dataType:'json',
                success:function(data){
                    var nama = data['data'].split("_");
                    for(var i=0;i<nama.length;i++){
                        var option = "<option value='"+nama[i]+"'>"+nama[i]+"</option>";
                        $("#selectnamaSTK").append(option);
                    }
                }
            });
            document.getElementById("tenaga_kesehatan").value = tenaga;
        }
        function getNamaStkk(){
            var tenaga = document.getElementById("selecttenaga2").value;
            var lok = document.getElementById("selectlok").value;
            var lok_ok = lok.split("_");
            $('#selectnamaSTK2').find('option').not(':first').remove();
            $.ajax({
                type:'GET',
                url:'/findnamastk',
                data:{tenaga:tenaga,mitra:lok_ok[0]},
                dataType:'json',
                success:function(data){
                    var nama = data['data'].split("_");
                    for(var i=0;i<nama.length;i++){
                        var option = "<option value='"+nama[i]+"'>"+nama[i]+"</option>";
                        $("#selectnamaSTK2").append(option);
                    }
                }
            });
            document.getElementById("tenkes").value = tenaga;
        }
        function getNamaStk2(){
            var selectnamaSTK = document.getElementById("selectnamaSTK").value;
            document.getElementById("nama_stk").value = selectnamaSTK;
        }
        function getNamaStkk2(){
            var selectnamaSTK = document.getElementById("selectnamaSTK2").value;
            document.getElementById("nama_tenkes").value = selectnamaSTK;
        }
    </script>
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/select2/select2.init.js')}}"></script>
    <!-- Picker Date Style -->
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.date.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.time.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/legacy.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <!-- <script src="{{asset('adminbite-10/assets/libs/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/datetimepicker/datetimepicker.init.js')}}"></script> -->
    
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js')}}"></script>
    <script>
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    $('#date-fr').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', lang: 'fr', weekStart: 1, cancelText: 'ANNULER' });
    $('#date-end').bootstrapMaterialDatePicker({ weekStart: 0 });
    $('#date-start').bootstrapMaterialDatePicker({ weekStart: 0 }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });
    </script>
    <script type="text/javascript">
        var date = new Date({{ explode("-",$ibubersalin['tanggal_bersalin'])[0] }},{{ explode("-",$ibubersalin['tanggal_bersalin'])[1] }}-1,
        {{ explode(" ",explode("-",$ibubersalin['tanggal_bersalin'])[2])[0] }});
        
        $('.pickadate-disable').pickadate({
            disabled:[]
        });
        var picker = $('#tanggal_bersalin').pickadate('picker');
        picker.set('select', date);

        // document.getElementById("timepicker").value = {{ explode(":",explode(" ",$ibubersalin['tanggal_bersalin'])[1])[0] }}+":"+
        // {{ explode(":",explode(" ",$ibubersalin['tanggal_bersalin'])[1])[1] }};

    </script>
    <script src="{{asset('adminbite-10/assets/libs/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/sweetalert2/sweet-alert.init.js')}}"></script>
</body>
</html>