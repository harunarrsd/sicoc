<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')@yield('head-ripel01')

<title>Data Ibu Hamil</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/sweetalert2/dist/sweetalert2.min.css')}}" rel="stylesheet">
<body>
    <!-- @yield('main-preloader') -->
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Ibu Hamil</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <span>Standar Pelayanan Minimal</span>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Ibu Hamil</li>
                                    <li class="breadcrumb-item active" aria-current="page">Data Ibu Hamil</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('success')}}
                            </div>
                        @endif
                        @if(session()->has('danger'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('danger')}}
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <div class="d-md-flex align-items-center">
                                    <div>
                                        <h4 class="card-title">Data Ibu Hamil</h4>
                                        <h6 class="card-subtitle">Standar Pelayanan Minimal</h6>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="dl">
                                            <div class="btn-group">
                                                @if(Auth::user()['level'] == 1 ||Auth::user()['level'] == 2 )
                                                <a href="{{url('/tambah-ibu-hamil')}}"><button type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Pendaftaran Ibu Hamil">
                                                    <i class="mdi mdi-plus"></i> Tambah Ibu Hamil
                                                </button></a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-t-10">
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card">
                                            <div class="box bg-info text-center">
                                                <h1 class="font-light text-white">{{ $total_ibu_hamil }}</h1>
                                                <h6 class="text-white" data-toggle="tooltip" data-placement="bottom" title="Total Ibu Hamil terdaftar pada SICOC">Total Ibu Hamil terdaftar</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card">
                                            <div class="box bg-success text-center">
                                                <h1 class="font-light text-white">{{ $total_sesuai_spm}}</h1>
                                                <h6 class="text-white" data-toggle="tooltip" data-placement="bottom" title="Mendapat Layanan Sesuai SPM! Capaian SPM mencapai 100%">Mendapat Layanan</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card">
                                            <div class="box bg-danger text-center">
                                                <h1 class="font-light text-white">{{ $total_tsesuai_spm}}</h1>
                                                <h6 class="text-white" data-toggle="tooltip" data-placement="bottom" title="Tidak Mendapat Layanan Sesuai SPM. Dikarenakan Capaian SPM tidak mencapai 100%.">Tidak Mendapat Layanan</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card">
                                            <div class="box bg-warning text-center">
                                                <h1 class="font-light text-white">{{ $total_belum_melahirkan}}</h1>
                                                <h6 class="text-white" data-toggle="tooltip" data-placement="bottom" title="Tahap Menuju Mendapat Layanan Sesuai SPM">Tahap Pelayanan</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Status Layanan</th>
                                                <!-- <th>Kode Riwayat</th> -->
                                                <th>Ibu Hamil</th>
                                                <!-- <th>NIK Ibu Hamil</th> -->
                                                <th>Tanggal HPHT</th>
                                                <!-- <th>No. Telepon</th>
                                                <th>Nama Suami</th> -->
                                                <th>Capaian SPM</th>
                                                <!-- @if(Auth::user()['name'] != 3 || Auth::user()['name'] != 4)
                                                    <th> Dibuat Oleh</th>
                                                    <th> Diubah Oleh</th>
                                                @endif -->
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $no = 1; @endphp
                                            @foreach($data as $index => $ibuhamil)
                                            <tr>
                                                <td>{{$no}}</td>@php $no++; @endphp
                                                <td>
                                                @if($ibuhamil->status_kehamilan == "Tahap Pelayanan")
                                                    <span class="label label-warning" data-toggle="tooltip" data-placement="top" title="Tahap Menuju Mendapat Layanan Sesuai SPM">
                                                @elseif($ibuhamil->status_kehamilan == "Tidak Sesuai SPM")
                                                    <span class="label label-danger" data-toggle="tooltip" data-placement="top" title="Tidak Mendapat Layanan Sesuai SPM. Dikarenakan Capaian SPM tidak mencapai 100%."> 
                                                @elseif($ibuhamil->status_kehamilan == "Sesuai SPM")
                                                    <span class="label label-info" data-toggle="tooltip" data-placement="top" title="Mendapat Layanan Sesuai SPM! Capaian SPM mencapai 100%">
                                                @endif
                                                {{ $ibuhamil->status_kehamilan }}</span>
                                                </td>
                                                <!-- <td>{{$ibuhamil->kode_riwayat_kehamilan}}</td> -->
                                                <td><span class="font-bold link">{{ $warga[$index]['nama'] }}</span><br>
                                                <small class="text-muted">{{ $warga[$index]['nik'] }}</small></td>
                                                <!-- <td>{{ $warga[$index]['nik'] }}</td> -->
                                                <td>{{ date('d M Y',strtotime($ibuhamil->tanggal_hamil)) }}<br>
                                                <small class="text-muted">{{$ibuhamil->kode_riwayat_kehamilan}}</small></td>
                                                <!-- <td>{{$warga[$index]['no_telp']}}</td>
                                                <td>{{ $ibuhamil->nama_suami }}</td> -->
                                                <td><span class="label label-info">{{ number_format($totalcapaian[$index],2) }}%</span></td>
                                                <!-- @if(Auth::user()['name'] != 3 || Auth::user()['name'] != 4)
                                                    <td>
                                                        {{$ibuhamil->created_by}}
                                                        <br><small class="text-muted">Tanggal : {{ $ibuhamil->created_at }}</small>
                                                    </td>
                                                    <td>
                                                        @if($ibuhamil->updated_by == null) - @else {{$ibuhamil->updated_by}} @endif
                                                        <br><small class="text-muted"> Tanggal : @if($ibuhamil->updated_at == "0000-00-00 00:00:00") - @else {{ $ibuhamil->updated_at }} @endif </small>
                                                    </td>
                                                @endif -->
                                                <td>                                                    
                                                    <a href="{{url('/detail-ibu-hamil/'.$ibuhamil->id)}}">
                                                        <button type="submit" class="btn btn-info btn-xs">
                                                            Detail
                                                        </button>
                                                    </a>
                                                    @if(Auth::user()['level'] == 1 ||Auth::user()['level'] == 2 )
                                                    @if($ibuhamil->status_kehamilan == "Tahap Pelayanan")
                                                    <a href="{{url('/ubah-ibu-hamil/'.$ibuhamil->id)}}">
                                                        <button type="submit" class="btn btn-warning btn-xs">
                                                            Ubah
                                                        </button>
                                                    </a>
                                                    <button alt="default" class="btn btn-danger btn-xs" data-href="{{ url('/hapus-ibu-hamil/'.$ibuhamil->id)}}" data-toggle="modal" data-target="#confirm-delete">
                                                        Hapus
                                                    </button>
                                                    @endif
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            Hapus
                        </div>
                        <div class="modal-body">
                            Anda yakin ingin menghapus ?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            <a class="btn btn-danger btn-ok text-white">Hapus</a>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <script src="{{asset('adminbite-10/assets/libs/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/sweetalert2/sweet-alert.init.js')}}"></script>
    <script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
    </script>
</body>
</html>