<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')@yield('head-ripel01')
<title>Detail Ibu Hamil</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Ibu Hamil</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Ibu Hamil</li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Ibu Hamil</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('success')}}
                            </div>
                        @endif
                        @if(session()->has('danger'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('danger')}}
                            </div>
                        @endif
                        <div class="card">
                            <div class="row">
                                <div class="card-body p-b-0 m-l-10">
                                    <h4 class="card-title">Riwayat Pelayanan </h4>
                                    <div class="d-flex no-block align-items-center m-b-10">
                                        <div class="m-r-10">
                                            <img src="{{asset('adminbite-10/assets/images/users/user.png')}}" alt="user" class="rounded-circle" width="45">
                                        </div>
                                        <div class="">
                                            <h5 class="m-b-0 font-16 font-medium">{{ $nama }}</h5>
                                            <span>{{ $data['nik'] }}</span>
                                            <form method="POST" id="tambahpel-form" action="{{url('/mulai-catat-ibu-hamil')}}" style="display: none;">{{ csrf_field() }}
                                                <input type="text" maxlength="16" name="nik" value="{{$data['nik']}}">
                                            </form>
                                            <h5 class="card-title m-b-0">
                                                {{ date('d M Y', strtotime($data['tanggal_hamil'])) }}
                                                <span class="btn waves-effect waves-light btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mulai hamil sejak {{ date('d M Y', strtotime($data['tanggal_hamil'])) }}">
                                                    Mulai Hamil
                                                </span>
                                            </h5>
                                        </div> 
                                    </div>
                                </div>
                                <div class="card-body col-lg-4 col-md-12">
                                    <h4 class="card-title m-b-0">
                                        Capaian SPM
                                        <span class="btn bg-info text-white btn-outline" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{ number_format($totalcapaian,2) }}% dan total {{ $super_pel_total }}T dari 40T">
                                            {{ number_format($totalcapaian,2) }}%
                                        </span>
                                    </h4>                                    
                                    <div class="d-flex flex-row">
                                        <div class="p-10 p-l-0 b-r">
                                            <h6 class="font-light">Trimester 1</h6><b>{{ $tw1capaian }}%</b></div>
                                        <div class="p-10 b-r">
                                            <h6 class="font-light">Trimester 2</h6><b>{{ $tw2capaian }}%</b>
                                        </div>
                                        <div class="p-10">
                                            <h6 class="font-light">Trimester 3</h6><b>{{ $tw3capaian }}%</b>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="row">
                                <div class="card-body p-b-0 p-t-0">
                                    <div class="d-md-flex align-items-center">
                                        <div>                           
                                            <ul class="nav nav-tabs customtab m-l-10" role="tablist">
                                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home2" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Trimester 1</span></a> </li>
                                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile2" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Trimester 2</span></a> </li>
                                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages2" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Trimester 3</span></a> </li>
                                            </ul>
                                        </div>
                                        <div class="ml-auto">
                                            <div class="dl">
                                                <div class="btn-group m-r-10">
                                                    <form method="POST" id="withnik-form" action="{{url('/mulai-catat-ibu-hamil')}}" style="display: none;">{{ csrf_field() }}
                                                        <input type="text" maxlength="16" name="nik" value="{{ Session::get('withnik') }}">
                                                    </form>
                                                    @if($data['status_kehamilan'] == "Tahap Pelayanan")
                                                    <a  href="{{ url('/mulai-catat-ibu-hamil') }}" onclick="event.preventDefault(); document.getElementById('withnik-form').submit();">
                                                    <button type="button" class="btn btn-info">                                                    
                                                        <i class="mdi mdi-book-open-page-variant"></i> Tambah Pelayanan
                                                    </button></a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="home2" role="tabpanel">
                                <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="zero_config" class="table table-striped table-bordered display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Tanggal Pelayanan</th>
                                                        <th>Fasilitas Kesehatan</th>
                                                        <th>Tenaga Kesehatan</th>
                                                        <th>Capaian</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $no = 1; @endphp
                                                    @foreach($pelayanan as $i => $pel)
                                                    @if($pel['tanggal_pelayanan'] <= $hayo3bulanlagi && $pel['tanggal_pelayanan'] > $data['tanggal_hamil'] || $pel['tanggal_pelayanan'] == $data['tanggal_hamil'] )
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ date('d M Y', strtotime($pel['tanggal_pelayanan'])) }}</td>
                                                        <td>
                                                            @if(explode('_',$pel['lokasi'])[0] != null){{{ explode('_',$pel['lokasi'])[0] }}}@else - @endif <br>
                                                            <small class="text-muted"> @if(explode('_',$pel['lokasi'])[1] != null){{{ explode('_',$pel['lokasi'])[1] }}}@else - @endif </small>
                                                        </td>
                                                        <td>
                                                            @if(explode('_',$pel['tenaga_kerja'])[0] != null){{{ explode('_',$pel['tenaga_kerja'])[0] }}}@else - @endif <br>
                                                            <small class="text-muted"> @if(explode('_',$pel['tenaga_kerja'])[1] != null){{{ explode('_',$pel['tenaga_kerja'])[1] }}}@else - @endif </small>
                                                        </td>
                                                        <td>{{ $pel_by_id[$pel['id']] }} dari 10T</td>
                                                        <td>
                                                            <a href="{{ url('/detail-pelayanan-ibu-hamil/'.$pel['id'])}}" class="text-info"><button class="btn btn-xs btn-info" type="button"> Detail </button></a>
                                                            <a href="{{ url('/ubah-pelayanan-ibu-hamil/'.$pel['id'])}}" class="text-info"><button class="btn btn-xs btn-warning" type="button"> Ubah </button></a>
                                                            <button alt="default" class="btn btn-danger btn-xs" data-href="{{ url('/hapus-pelayanan-ibu-hamil/'.$pel['id'])}}" data-toggle="modal" data-target="#confirm-delete">
                                                                Hapus
                                                            </button>
                                                            
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="profile2" role="tabpanel">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="default_order" class="table table-striped table-bordered display" style="width:100%">
                                            <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Tanggal Pelayanan</th>
                                                        <th>Fasilitas Kesehatan</th>
                                                        <th>Tenaga Kesehatan</th>
                                                        <th>Capaian</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $no = 1; @endphp
                                                    @foreach($pelayanan as $i => $pel)
                                                    @if($pel['tanggal_pelayanan'] <= $hayo6bulanlagi && $pel['tanggal_pelayanan'] > $hayo3bulanlagi)
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ date('d M Y', strtotime($pel['tanggal_pelayanan'])) }}</td>
                                                        <td>
                                                            @if(explode('_',$pel['lokasi'])[0] != null){{{ explode('_',$pel['lokasi'])[0] }}}@else - @endif <br>
                                                            <small class="text-muted"> @if(explode('_',$pel['lokasi'])[1] != null){{{ explode('_',$pel['lokasi'])[1] }}}@else - @endif </small>
                                                        </td>
                                                        <td>
                                                            @if(explode('_',$pel['tenaga_kerja'])[0] != null){{{ explode('_',$pel['tenaga_kerja'])[0] }}}@else - @endif <br>
                                                            <small class="text-muted"> @if(explode('_',$pel['tenaga_kerja'])[1] != null){{{ explode('_',$pel['tenaga_kerja'])[1] }}}@else - @endif </small>
                                                        </td>
                                                        <td>{{ $pel_by_id2[$pel['id']] }} dari 10T</td>
                                                        <td>
                                                            <a href="{{ url('/detail-pelayanan-ibu-hamil/'.$pel['id'])}}" class="text-info"><button class="btn btn-xs btn-info" type="button"> Detail </button></a>
                                                            <a href="{{ url('/ubah-pelayanan-ibu-hamil/'.$pel['id'])}}" class="text-info"><button class="btn btn-xs btn-warning" type="button"> Ubah </button></a>
                                                            <button alt="default" class="btn btn-danger btn-xs" data-href="{{ url('/hapus-pelayanan-ibu-hamil/'.$pel['id'])}}" data-toggle="modal" data-target="#confirm-delete">
                                                                Hapus
                                                            </button>
                                                            
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="messages2" role="tabpanel">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="multi_col_order" class="table table-striped table-bordered display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Tanggal Pelayanan</th>
                                                        <th>Fasilitas Kesehatan</th>
                                                        <th>Tenaga Kesehatan</th>
                                                        <th>Capaian</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $no = 1; @endphp
                                                    @foreach($pelayanan as $i => $pel)
                                                    @if($pel['tanggal_pelayanan'] <= $hayo9bulanlagi && $pel['tanggal_pelayanan'] > $hayo6bulanlagi)
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ date('d M Y', strtotime($pel['tanggal_pelayanan'])) }}</td>
                                                        <td>
                                                            @if(explode('_',$pel['lokasi'])[0] != null){{{ explode('_',$pel['lokasi'])[0] }}}@else - @endif <br>
                                                            <small class="text-muted"> @if(explode('_',$pel['lokasi'])[1] != null){{{ explode('_',$pel['lokasi'])[1] }}}@else - @endif </small>
                                                        </td>
                                                        <td>
                                                            @if(explode('_',$pel['tenaga_kerja'])[0] != null){{{ explode('_',$pel['tenaga_kerja'])[0] }}}@else - @endif <br>
                                                            <small class="text-muted"> @if(explode('_',$pel['tenaga_kerja'])[1] != null){{{ explode('_',$pel['tenaga_kerja'])[1] }}}@else - @endif </small>
                                                        </td>
                                                        <td>{{ $pel_by_id3[$pel['id']] }} dari 10T</td>                                                        
                                                        <td>
                                                            <a href="{{ url('/detail-pelayanan-ibu-hamil/'.$pel['id'])}}" class="text-info"><button class="btn btn-xs btn-info" type="button"> Detail </button></a>
                                                            <a href="{{ url('/ubah-pelayanan-ibu-hamil/'.$pel['id'])}}" class="text-info"><button class="btn btn-xs btn-warning" type="button"> Ubah </button></a>
                                                            <button alt="default" class="btn btn-danger btn-xs" data-href="{{ url('/hapus-pelayanan-ibu-hamil/'.$pel['id'])}}" data-toggle="modal" data-target="#confirm-delete">
                                                                Hapus
                                                            </button>
                                                            
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Hapus
            </div>
            <div class="modal-body">
                Anda yakin ingin menghapus ?
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <a class="btn btn-danger btn-ok text-white">Hapus</a>
                    </div>
            </div>
        </div>
    </div>
    @yield('js-ripel01')
    <script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
    </script>
</body>
</html>