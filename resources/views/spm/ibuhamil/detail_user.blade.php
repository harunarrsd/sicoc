<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')@yield('head-ripel01')
<title>Riwayat Pelayanan Ibu Hamil</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Ibu Hamil</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Ibu Hamil</li>
                                    <li class="breadcrumb-item active" aria-current="page">Riwayat Pelayanan</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="row">
                                <div class="card-body p-b-0 m-l-10">
                                    <h4 class="card-title">Riwayat Pelayanan </h4>
                                    <div class="d-flex no-block align-items-center m-b-10">
                                        <div class="m-r-10">
                                            <img src="{{asset('adminbite-10/assets/images/users/user.png')}}" alt="user" class="rounded-circle" width="45">
                                        </div>
                                        <div class="">
                                            @foreach($ibuhamil as $ibuhamils)
                                            <h5 class="m-b-0 font-16 font-medium">{{ $ibuhamils->nama }}</h5>
                                            <span>{{ $ibuhamils->nik }}</span>
                                            <h5 class="card-title m-b-0">
                                                {{ date('d M Y', strtotime($ibuhamils->tanggal_hamil)) }}
                                                <span class="btn waves-effect waves-light btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mulai hamil sejak {{ date('d M Y', strtotime($ibuhamils->tanggal_hamil)) }}">
                                                    Mulai Hamil
                                                </span>
                                            </h5>
                                            @endforeach
                                        </div> 
                                    </div>
                                </div>
                                <div class="card-body col-lg-4 col-md-12">
                                    <h4 class="card-title m-b-0">
                                        Capaian SPM
                                        <span class="btn bg-info text-white btn-outline" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{ $totalcapaian }}% dan total {{ $super_pel_total }}T dari 40T">
                                            {{ $totalcapaian }}%
                                        </span>
                                    </h4>                                    
                                    <div class="d-flex flex-row">
                                        <div class="p-10 p-l-0 b-r">
                                            <h6 class="font-light">Triwulan 1</h6><b>{{ $tw1capaian }}%</b></div>
                                        <div class="p-10 b-r">
                                            <h6 class="font-light">Triwulan 2</h6><b>{{ $tw2capaian }}%</b>
                                        </div>
                                        <div class="p-10">
                                            <h6 class="font-light">Triwulan 3</h6><b>{{ $tw3capaian }}%</b>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="row">
                                <a href="javascript:void(0)" id="step2-right-to-step1" class="text-info m-l-30"><button class="btn btn-success" type="button">< Masukkan NIK lain</button></a>
                                <ul class="nav nav-tabs customtab m-l-10" role="tablist">
                                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home2" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Triwulan 1</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile2" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Triwulan 2</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages2" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Triwulan 3</span></a> </li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="home2" role="tabpanel">
                                <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="zero_config" class="table table-striped table-bordered display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Kode Pelayanan</th>
                                                        <th>Tanggal Pelayanan</th>
                                                        <th>Capaian</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $no = 1; @endphp
                                                    @foreach($data_pelayanan as $i=>$pelayanan)
                                                    @if($pelayanan['jenis_tw'] == 1)
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ $pelayanan['id_pelayanan'] }}</td>
                                                        <td>{{ date('d M Y', strtotime($pelayanan['tanggal_pelayanan'])) }}</td>
                                                        <td>{{ $pel[$i] }} dari 10T</td>
                                                        <td><a href="javascript:void(0)" id="step2-right-to-step3-add" class="text-info m-l-10"><button class="btn btn-info" type="button"> Detail </button></a></td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="profile2" role="tabpanel">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="default_order" class="table table-striped table-bordered display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Kode Pelayanan</th>
                                                        <th>Tanggal Pelayanan</th>
                                                        <th>Capaian</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $no = 1; @endphp
                                                    @foreach($data_pelayanan as $i=>$pelayanan)
                                                    @if($pelayanan['jenis_tw'] == 2)
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ $pelayanan['id_pelayanan'] }}</td>
                                                        <td>{{ date('d M Y', strtotime($pelayanan['tanggal_pelayanan'])) }}</td>
                                                        <td>{{ $pel2[$i] }} dari 10T</td>
                                                        <td><a href="javascript:void(0)" id="step2-right-to-step3-add" class="text-info m-l-10"><button class="btn btn-info" type="button"> Detail </button></a></td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="messages2" role="tabpanel">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="multi_col_order" class="table table-striped table-bordered display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Kode Pelayanan</th>
                                                        <th>Tanggal Pelayanan</th>
                                                        <th>Capaian</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $no = 1; @endphp
                                                    @foreach($data_pelayanan as $i=>$pelayanan)
                                                    @if($pelayanan['jenis_tw'] == 3)
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ $pelayanan['id_pelayanan'] }}</td>
                                                        <td>{{ date('d M Y', strtotime($pelayanan['tanggal_pelayanan'])) }}</td>
                                                        <td>{{ $pel3[$i] }} dari 10T</td>
                                                        <td><a href="javascript:void(0)" id="step2-right-to-step3-add" class="text-info m-l-10"><button class="btn btn-info" type="button"> Detail </button></a></td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <script>
        function cek_capaian($berat_tinggi, $tekanan_darah, $lila, $tinggi_puncak_rahim, $presentasi_djj, $imuniasi_tetanus_tt, $tablet_tambah_darah, $tes_laboratorium, $tatalaksana, $temu_wicara){
            var datbt;
            var bt = $berat_tinggi.split("_");
            if($berat_tinggi != "" && bt[0] != 0 && bt[1] != 0){
                datbt += "1";
            }
            var td = $tekanan_darah.split("/");
            if($tekanan_darah != "" && td[0] != 0 && td[1] != 0){
                
            }
            if($lila != "" && $lila != "0"){
                
            }
            if($tinggi_puncak_rahim != "" && $tinggi_puncak_rahim != "0"){
                
            }
            var pdjj = $presentasi_djj.split("_");
            if($presentasi_djj != "" && pdjj[0] != "" && pdjj[1] != ""){
                
            }
            var it_tt = $imuniasi_tetanus_tt.split("_");
            if($imuniasi_tetanus_tt != "" && it_tt[0] != "t" && it_tt[1] != "t"){
                
            }
            var tl = $tes_laboratorium.split("_");
            if($tes_laboratorium != "" && tl[0] != "t" && tl[1] != "t" && tl[2] != "t" && tl[3] != "t"){
                
            }
            if($tatalaksana != "" && $tatalaksana != "t"){
                
            }
            if($temu_wicara != "" && $temu_wicara != "t"){
                
            }

            //Cek
            if(datbt)
        }
        </script>
</body>
</html>