<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>SPM Ibu Hamil</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Ibu Hamil</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <!-- <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li> -->
                                    <li class="breadcrumb-item active" aria-current="page">Standar Pelayanan Minimal</li>
                                    <li class="breadcrumb-item active" aria-current="page">Ibu Hamil</li>
                                    <li class="breadcrumb-item active" aria-current="page">Catat Pelayanan</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
                <div class="card-body bg-light">
                    <div class="d-md-flex align-items-center">
                        <div><h2>Pelayanan Ibu Hamil</h2></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Penyataan Standar</h4>
                            Setiap ibu hamil mendapatkan pelayanan antenatal sesuai standar. 
                            Pemerintah Daerah Kabupaten/Kota wajib memberikan pelayanan 
                            kesehatan ibu hamil kepada semua ibu hamil di wilayah kabupaten/kota 
                            tersebut dalam kurun waktu kehamilan.
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Pelayanan Antenatal</h4>
                            Pelayanan antenatal sesuai standar adalah pelayanan yang
                            diberikan kepada ibu hamil minimal 4 kali selama kehamilan
                            dengan jadwal satu kali pada trimester pertama, satu kali pada
                            trimester kedua dan dua kali pada trimester ketiga yang
                            dilakukan oleh Bidan dan atau Dokter dan atau Dokter Spesialis
                            Kebidanan baik yang bekerja di fasilitas pelayanan kesehatan
                            pemerintah maupun swasta yang memiliki Surat Tanda Register
                            (STR).
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Kriteria 10T</h4>
                            Yang disebut dengan standar pelayanan antenatal adalah
                            pelayanan yang dilakukan kepada ibu hamil dengan memenuhi
                            kriteria 10 T yaitu :
                            <h6>a) Timbang berat badan dan ukur tinggi badan;</h6>
                            <h6>b) Ukur tekanan darah;</h6>
                            <h6>c) Nilai status gizi (Ukur Lingkar Lengan Atas/LILA);</h6>
                            <h6>d) Ukur tinggi puncak rahim (fundus uteri);</h6>
                            <h6>e) Tentukan presentasi janin dan Denyut Jantung Janin(DJJ);</h6>
                            <h6>f) Skrining status imunisasi tetanus dan berikan imunisasi Tetanus Toksoid (TT) bila diperlukan; </h6>
                            <h6>g) Pemberian tablet tambah darah minimal 90 tablet selama kehamilan;</h6>
                            <h6>h) Tes laboratorium: tes kehamilan, pemeriksaan hemoglobin darah (Hb), pemeriksaan golongan darah 
                                (bila belum pernah dilakukan sebelumnya), pemeriksaan protein urin (bila ada indikasi); 
                                yang pemberian pelayanannya disesuaikan dengan trimester kehamilan;</h6>
                            <h6>i) Tatalaksana/penanganan kasus sesuai kewenangan;</h6>
                            <h6>j) Temu wicara (konseling)</h6>
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Definisi Operasional Capaian Kinerja</h4>
                            Capaian kinerja Pemerintah Daerah Kabupaten/Kota dalam
                            memberikan pelayanan kesehatan ibu hamil dinilai dari cakupan
                            Pelayanan Kesehatan Ibu Hamil (K4) sesuai standar di wilayah
                            kabupaten/kota tersebut dalam kurun waktu satu tahun.
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4>
                        <i class="fas fa-chevron-circle-right m-r-10 m-b-10"></i> Referensi
                    </h4>
                    <h6><i class="fas fa-square"></i> Keputusan Menteri Kesehatan Nomor 284/MENKES/SK/III/2004 tentang Buku Kesehatan Ibu dan Anak;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 1464/X/Menkes/2010 tentang Izin dan Penyelenggaraan Praktik Bidan;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 75 Tahun 2014 tentang Pusat Kesehatan Masyarakat;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 97 Tahun 2014 tentang Pelayanan Kesehatan Masa Sebelum Hamil, Masa Hamil, Persalinan, dan Masa Sesudah Melahirkan, Penyelenggaraan Pelayanan Kontrasepsi, serta Pelayanan Kesehatan Seksual;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Konsil Kedokteran Indonesia Nomor 11 Tahun 2012 tentang Standar Kompetensi Dokter Indonesia;</h6>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <script>
    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();
    $("#antenatal").fadeOut();
    $("#k10t").fadeOut();
    $("#docp").fadeOut();
    $('#to-antenatal').on("click", function() {
        $("#pernyataan_dasar").slideUp();
        $("#k10t").slideUp();
        $("#docp").slideUp();
        $("#antenatal").slideDown();
    });
    $('#to-k10t').on("click", function() {
        $("#pernyataan_dasar").slideUp();
        $("#antenatal").slideUp();
        $("#docp").slideUp();
        $("#k10t").slideDown();
    });
    $('#to-docp').on("click", function() {
        $("#pernyataan_dasar").slideUp();
        $("#antenatal").slideUp();
        $("#k10t").slideUp();
        $("#docp").slideDown();
    });
    $('#to-pernyataan_dasar').on("click", function() {
        $("#docp").slideUp();
        $("#antenatal").slideUp();
        $("#k10t").slideUp();
        $("#pernyataan_dasar").slideDown();
    });
    </script>
</body>
</html>