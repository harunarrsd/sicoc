<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')@yield('head-ripel01')
<title>Data Pelayanan Ibu Hamil</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Ibu Hamil</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Ibu Hamil</li>
                                    <li class="breadcrumb-item active" aria-current="page">Data Pelayanan Ibu Hamil</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-md-flex align-items-center">
                                    <div>
                                        <h4 class="card-title">Data Pelayanan Ibu Hamil</h4>
                                        <h6 class="card-subtitle">Standar Pelayanan Minimal</h6>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="dl">
                                            <div class="btn-group">
                                                <a href="{{url('/catat-ibu-hamil')}}"><button type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Catat Pelayanan Ibu Hamil">
                                                    <i class="mdi mdi-plus"></i> Tambah Pelayanan Ibu Hamil
                                                </button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="row m-t-10">
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card card-hover">
                                            <div class="box bg-info text-center">
                                                <h1 class="font-light text-white">5678</h1>
                                                <h6 class="text-white">Total Ibu Hamil</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card card-hover">
                                            <div class="box bg-primary text-center">
                                                <h1 class="font-light text-white">0182932</h1>
                                                <h6 class="text-white">Melahirkan Sesuai SPM</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card card-hover">
                                            <div class="box bg-success text-center">
                                                <h1 class="font-light text-white">7638</h1>
                                                <h6 class="text-white">Melahirkan Tidak Sesuai SPM</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card card-hover">
                                            <div class="box bg-dark text-center">
                                                <h1 class="font-light text-white">27328</h1>
                                                <h6 class="text-white">Belum Melahirkan</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="table-responsive m-t-15">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Kode Pelayanan</th>
                                                <th>Nama Ibu Hamil</th>
                                                <th>Tanggal Pelayanan</th>
                                                <th>Fasilitas Kesehatan</th>
                                                <th>Informasi Fasilitas</th>
                                                <th>Tenaga Kesehatan</th>
                                                <th>Nama Tenaga Kesehatan</th>
                                                <th>Capaian 10T</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($pelayanan as $index => $data_pelayanan)
                                            <tr>
                                                <td>{{ $data_pelayanan['id'] }}</td>
                                                <td><a href="#" class="font-bold link">{{ $ibuhamil[$index]['nama'] }}</a>  
                                                <td>{{ date('d M Y', strtotime($data_pelayanan->tanggal_pelayanan)) }}</td>                                                  </td>
                                                <td>{{ explode('_',$data_pelayanan->lokasi)[0] }}</td>                                                  </td>
                                                <td>{{ explode('_',$data_pelayanan->lokasi)[1] }}</td>                                                  </td>
                                                <td>{{ explode('_',$data_pelayanan->tenaga_kerja)[0] }}</td>                                                  </td>
                                                <td>{{ explode('_',$data_pelayanan->tenaga_kerja)[1] }}</td>
                                                @if($data_pelayanan['jenis_tw'] == 1)
                                                    <td> {{ $pel_by_id[$data_pelayanan->id] }}T dari 10T </td>
                                                @elseif($data_pelayanan['jenis_tw'] == 2)
                                                    <td> {{ $pel_by_id2[$data_pelayanan->id] }}T dari 10T </td>
                                                @elseif($data_pelayanan['jenis_tw'] == 3)
                                                    <td> {{ $pel_by_id3[$data_pelayanan->id] }}T dari 20T</td>                                                
                                                @endif
                                                <td>
                                                    <a href="{{ url('/detail-pelayanan-ibu-hamil/'.$data_pelayanan->id)}}"><button class="btn btn-xs btn-info" type="button"> Detail </button></a>
                                                    <a href="{{ url('/ubah-pelayanan-ibu-hamil/'.$data_pelayanan->id)}}"><button class="btn btn-xs btn-warning" type="button"> Ubah </button></a>    
                                                </td>                                    
                                            </tr>
                                            @endforeach
                                        </tbody>                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>