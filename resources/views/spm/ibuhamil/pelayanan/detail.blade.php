<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="GIK">
<meta name="author" content="GIK">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('adminbite-10/assets/images/sicoc-favicon.png')}}">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/select2/dist/css/select2.min.css')}}">
<link href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/morris.js/morris.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/toastr/build/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet">
<style>
    .sidebar-item a{
        font-weight:600;
    }
    .tx-c{
        text-align:center;
    }
    .bg-y{
        background:yellow;
    }
    .sidebar-link .icon-Record{
        visibility: visible !important;
    }
</style>
<title>Detail Pelayanan Ibu Hamil</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.date.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.time.css')}}">
    <link type="text/css" href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
            <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Ibu Hamil</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Ibu Hamil</li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Pelayanan</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="card" id="step3-add">
                    <div class="row">
                        <div class="col-lg-12 col-xlg-12 col-md-12">
                            <div class="card-body p-b-0 m-b-30">
                                <div class="d-md-flex align-items-center">
                                    <div>                           
                                        <h4 class="card-title">Detail Pelayanan Ibu Hamil </h4>
                                        <h6 class="card-subtitle">Detail 10T Pelayanan Ibu Hamil sesuai SPM</h6>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="dl">
                                            @if($pelayanan['status_kehamilan'] == "Tahap Pelayanan")
                                            <a href="{{url('/ubah-pelayanan-ibu-hamil/'.$pelayanan->id)}}">
                                                <button type="button" class="btn btn-warning">
                                                    <i class="mdi mdi-book-open-page-variant"></i> Ubah Pelayanan
                                                </button>
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form method="POST" action="{{url('/simpan-catat-ibu-hamil')}}">{{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-6 col-xlg-6 col-md-12">
                            <div class="card-body p-t-0">
                                <div class="row">
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Tanggal Pelayanan</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                            </div>
                                            <input type="text" class="form-control bg-success text-white" disabled value="{{ date('d F, Y',strtotime(explode(' ',$pelayanan['tanggal_pelayanan'])[0])) }}" id="tanggal_pelayanan"/>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label for="exampleInputEmail1">Waktu Pelayanan</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                            </div>
                                            <input id="timepicker" class="form-control bg-success text-white" disabled name="waktu_pelayanan" value="{{ explode(':',explode(' ',$pelayanan['tanggal_pelayanan'])[1])[0] }}:{{ explode(':',explode(' ',$pelayanan['tanggal_pelayanan'])[1])[1] }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Usia Ibu Hamil</label><small class="text-muted"> ketika pelayanan</small>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" id="usia_ibu_saat_pelayanan" class="form-control bg-success text-white" disabled name="usia_ibu_saat_pelayanan" placeholder="Terisi Otomatis"/>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11">Tahun</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Usia Kehamilan</label><small class="text-muted"> ketika pelayanan</small>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" id="usia_saat_pelayanan" class="form-control bg-success text-white" disabled name="usia_saat_pelayanan" placeholder="Terisi Otomatis"/>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11">Bulan</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Fasilitas Kesahatan</label> <span class="text-danger">*</span>    
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($fasilitas == "-") 
                                            <select name="fasilitas_kesehatan" id="selectfas" aria-invalid="true" class="form-control bg-danger text-white" disabled>
                                            @else
                                            <select name="fasilitas_kesehatan" id="selectfas" aria-invalid="true" class="form-control bg-success text-white" disabled>
                                            @endif
                                                <option value="">{{$fasilitas}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                        <label>Nama Fasilitas Pelayanan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-map-alt"></i></span>
                                            </div>
                                            @if($lokasi == "-") 
                                            <select name="selectlok" id="selectlok"class="form-control bg-danger text-white" disabled>
                                            @else
                                            <select name="selectlok" id="selectlok"class="form-control bg-success text-white" disabled>
                                            @endif
                                                <option value="">{{$lokasi}}</option> 
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                        <label>Tenaga Kesehatan</label> 
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($tenaga_kerja == "-") 
                                            <select name="selecttenaga" id="selecttenaga" class="form-control bg-danger text-white" disabled>
                                            @else
                                            <select name="selecttenaga" id="selecttenaga" class="form-control bg-success text-white" disabled>
                                            @endif
                                                <option value="">{{$tenaga_kerja}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                        <label>Nama Dokter/Bidan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            @if($nama_stk == "-") 
                                            <select name="selectnamaSTK" id="selectnamaSTK" class="form-control bg-danger text-white" disabled>
                                            @else
                                            <select name="selectnamaSTK" id="selectnamaSTK" class="form-control bg-success text-white" disabled>
                                            @endif
                                                <option value="">{{$nama_stk}}</option>                                                    
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                        <label>Berat Badan</label> 
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            @if($berat == "-" || intval($berat) < 45) 
                                            <input type="text" name="berat" class="form-control bg-danger text-white" disabled value={{$berat}}>
                                            @else
                                            <input type="text" name="berat" class="form-control bg-success text-white" disabled value={{$berat}}>
                                            @endif
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">kg</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                        <label>Tinggi Badan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            @if($tinggi == "-" || intval($tinggi) < 100) 
                                            <input type="text" name="tinggi" class="form-control bg-danger text-white" disabled value={{$tinggi}}>
                                            @else
                                            <input type="text" name="tinggi" class="form-control bg-success text-white" disabled value={{$tinggi}}>
                                            @endif
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">cm</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-8 col-xlg-8 col-md-12 m-0">
                                        <label>Tekanan Darah</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            @if($sistole == "-" || intval($sistole) < 90) 
                                            <input type="text" id="sistole" name="tekanan_darah1" class="form-control bg-danger text-white" disabled value={{$sistole}}>
                                            @else
                                            <input type="text" id="sistole" name="tekanan_darah1" class="form-control bg-success text-white" disabled value={{$sistole}}>
                                            @endif
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">/</span>
                                            </div>
                                            @if($diastole == "-" || intval($diastole) < 60) 
                                            <input type="text" id="diastole" name="tekanan_darah2" class="form-control bg-danger text-white" disabled value={{$diastole}}>
                                            @else
                                            <input type="text" id="diastole" name="tekanan_darah2" class="form-control bg-success text-white" disabled value={{$diastole}}>
                                            @endif
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">mmHG</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                        <label>Ket. Tekanan Darah</label>
                                        <div class="input-group mb-3">
                                        @if($sistole > 120 || $diastole > 80)
                                            <input type="text" id="ket_tek_darah" name="ket_tek_darah" disabled class="form-control bg-danger text-white" disabled value="Tinggi">
                                        @elseif($sistole == "-" || $diastole == "-")
                                            <input type="text" id="ket_tek_darah" name="ket_tek_darah" disabled class="form-control bg-danger text-white" disabled value="-">
                                        @elseif($sistole < 90 || $diastole < 60 )
                                        <input type="text" id="ket_tek_darah" name="ket_tek_darah" disabled class="form-control bg-danger text-white" disabled value="Rendah">
                                        @elseif($sistole > 90 && $sistole < 120 && $diastole > 60 && $diastole < 80 )
                                            <input type="text" id="ket_tek_darah" name="ket_tek_darah" disabled class="form-control bg-success text-white" disabled value="Normal">
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-8 col-xlg-8 col-md-12 m-0">
                                        <label>Lingkar Lengan Atas/LiLA</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            @if($lila == "-") 
                                            <input type="text" id="lila" name="lila1" class="form-control bg-danger text-white" disabled value={{$lila}}>
                                            @else
                                            <input type="text" id="lila" name="lila1" class="form-control bg-success text-white" disabled value={{$lila}}>
                                            @endif
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">,</span>
                                            </div>
                                            @if($komalila == "-") 
                                            <input type="text" id="komalila" name="lila2" class="form-control bg-danger text-white" disabled value={{$komalila}}>
                                            @else
                                            <input type="text" id="komalila" name="lila2" class="form-control bg-success text-white" disabled value={{$komalila}}>
                                            @endif
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">cm</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                        <label>Nilai Status Gizi</label>
                                        <div class="input-group mb-3">
                                        @if($lila == "-" || $komalila == "-")
                                        <input type="text" id="ket_lila" name="ket_lila" class="form-control bg-danger text-white" disabled value="-">
                                        @elseif( floatval($lila.$komalila) <= 23.5)
                                        <input type="text" id="ket_lila" name="ket_lila" class="form-control bg-danger text-white" disabled value="Kekurangan Energi Kronis (KEK)">
                                        @else
                                        <input type="text" id="ket_lila" name="ket_lila" class="form-control bg-success text-white" disabled value="Normal">
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <input type="text" id="tanggal_lahir" name="tanggal_lahir" value="{{$data_nik['tgl_lahir']}}" style="display:none;">
                        <input type="text" id="tanggal_hamil" name="tanggal_hamil" value="{{$ibuhamil['tanggal_hamil']}}" style="display:none;">
                        <div class="col-lg-6 col-xlg-6 col-md-12">
                            <div class="card-body p-t-0 p-l-0">
                                <div class="form-group">
                                    <label>Tinggi Puncak Rahim (Fundus Uteril)</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ti-user"></i></span>
                                        </div>
                                        @if($tprahim == "-" || $satuantprahim == "-")
                                        <input type="text" name="tprahim" class="form-control bg-danger text-white" disabled value="-">
                                        @else
                                        <input type="text" name="tprahim" class="form-control bg-success text-white" disabled value="{{$tprahim.' '.$satuantprahim}}">
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                        <label>Presensi Janin</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($presensi == "-")                                        
                                            <input type="text" name="presensijanin" class="form-control bg-danger text-white" disabled value="{{$presensi}}">
                                            @else
                                            <input type="text" name="presensijanin" class="form-control bg-success text-white" disabled value="{{$presensi}}">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-5 col-xlg-5 col-md-12 m-0">
                                        <label>Denyut Jantung Janin</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($djj != "-" && intval($djj) > 100 && intval($djj) < 160 )
                                            <input type="text" id="djj" name="djj" class="form-control bg-success text-white" disabled value="{{$djj}}">
                                            @else
                                            <input type="text" id="djj" name="djj" class="form-control bg-danger text-white" disabled value="{{$djj}}">
                                            @endif
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">x/menit</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-3 col-xlg-3 col-md-12 m-0">
                                        <label>Ket. DJJ</label>
                                        <div class="input-group mb-3">
                                        @if($djj != "-" && intval($djj) > 100 && intval($djj) < 160 )
                                            <input type="text" disabled id="ket_djj" name="ket_djj" class="form-control bg-success text-white" disabled value="Normal">
                                        @elseif($djj == "-")
                                            <input type="text" disabled id="ket_djj" name="ket_djj" class="form-control bg-danger text-white" disabled value="-">
                                        @else
                                            <input type="text" disabled id="ket_djj" name="ket_djj" class="form-control bg-danger text-white" disabled value="Gawat Janin">
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label> Skrining Status Imunisasi Tetanus </label>
                                        <div class="input-group mb-3 bt-switch">
                                        @if($imun == "y")
                                        <div class="bootstrap-switch-on bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: 0px;">
                                                <span class="bootstrap-switch-handle-on bg-success text-white" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bootstrap-switch-danger" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @else
                                        <div class="bootstrap-switch-off bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: -58px;">
                                                <span class="bootstrap-switch-handle-on bootstrap-switch-success" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bg-danger text-white" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label> Imunisasi Tetanus Toksoid (TT) </label>
                                        <div class="input-group mb-3 bt-switch">
                                        @if($toksoid == "y")
                                        <div class="bootstrap-switch-on bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: 0px;">
                                                <span class="bootstrap-switch-handle-on bg-success text-white" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bootstrap-switch-danger" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @else
                                        <div class="bootstrap-switch-off bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: -58px;">
                                                <span class="bootstrap-switch-handle-on bootstrap-switch-success" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bg-danger text-white" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Jumlah Tablet Tambah Darah</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon22"><i class="ti-package"></i></span>
                                        </div>
                                        @if($tablet == "-" || $tablet == "0" )
                                        <input type="text" disabled class="form-control bg-danger text-white" value="{{$tablet}}" name="tablet_tambah_darah">
                                        @else
                                        <input type="text" disabled class="form-control bg-success text-white" value="{{$tablet}}" name="tablet_tambah_darah">
                                        @endif
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Tablet</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label> Tes Kehamilan </label>
                                        <div class="input-group mb-3 bt-switch">
                                        @if($tes_hamil == "y")
                                        <div class="bootstrap-switch-on bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: 0px;">
                                                <span class="bootstrap-switch-handle-on bg-success text-white" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bootstrap-switch-danger" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @else
                                        <div class="bootstrap-switch-off bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: -58px;">
                                                <span class="bootstrap-switch-handle-on bootstrap-switch-success" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bg-danger text-white" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label> Pemeriksaan Hemoglobin Darah </label>
                                        <div class="input-group mb-3 bt-switch">
                                        @if($tes_hb == "y")
                                        <div class="bootstrap-switch-on bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: 0px;">
                                                <span class="bootstrap-switch-handle-on bg-success text-white" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bootstrap-switch-danger" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @else
                                        <div class="bootstrap-switch-off bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: -58px;">
                                                <span class="bootstrap-switch-handle-on bootstrap-switch-success" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bg-danger text-white" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @endif 
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label> Pemeriksaan Golongan Darah</label>
                                        <div class="input-group mb-3 bt-switch">
                                        @if($tes_gd == "y")
                                        <div class="bootstrap-switch-on bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: 0px;">
                                                <span class="bootstrap-switch-handle-on bg-success text-white" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bootstrap-switch-danger" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @else
                                        <div class="bootstrap-switch-off bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: -58px;">
                                                <span class="bootstrap-switch-handle-on bootstrap-switch-success" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bg-danger text-white" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @endif 
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label> Pemeriksaan Protein Urin </label>
                                        <div class="input-group mb-3 bt-switch">
                                        @if($tes_urin == "y")
                                        <div class="bootstrap-switch-on bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: 0px;">
                                                <span class="bootstrap-switch-handle-on bg-success text-white" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bootstrap-switch-danger" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @else
                                        <div class="bootstrap-switch-off bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: -58px;">
                                                <span class="bootstrap-switch-handle-on bootstrap-switch-success" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bg-danger text-white" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @endif 
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label> Pemeriksaan Tes HIV </label>
                                        <div class="input-group mb-3 bt-switch">
                                        @if($tes_hiv == "y")
                                        <div class="bootstrap-switch-on bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: 0px;">
                                                <span class="bootstrap-switch-handle-on bg-success text-white" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bootstrap-switch-danger" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @else
                                        <div class="bootstrap-switch-off bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: -58px;">
                                                <span class="bootstrap-switch-handle-on bootstrap-switch-success" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bg-danger text-white" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @endif 
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label> Pemeriksaan Tes HbsAg </label>
                                        <div class="input-group mb-3 bt-switch">
                                        @if($tes_hbsag == "y")
                                        <div class="bootstrap-switch-on bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: 0px;">
                                                <span class="bootstrap-switch-handle-on bg-success text-white" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bootstrap-switch-danger" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @else
                                        <div class="bootstrap-switch-off bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: -58px;">
                                                <span class="bootstrap-switch-handle-on bootstrap-switch-success" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bg-danger text-white" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label> Tatalaksana </label>
                                        <div class="input-group mb-3 bt-switch">
                                        @if($tatalaksana == "y")
                                        <div class="bootstrap-switch-on bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: 0px;">
                                                <span class="bootstrap-switch-handle-on bg-success text-white" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bootstrap-switch-danger" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @else
                                        <div class="bootstrap-switch-off bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: -58px;">
                                                <span class="bootstrap-switch-handle-on bootstrap-switch-success" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bg-danger text-white" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label> Temu Wicara (Konseling) </label>
                                        <div class="input-group mb-3 bt-switch">
                                        @if($temu_wicara == "y")
                                        <div class="bootstrap-switch-on bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: 0px;">
                                                <span class="bootstrap-switch-handle-on bg-success text-white" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bootstrap-switch-danger" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @else
                                        <div class="bootstrap-switch-off bootstrap-switch-small bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 118px;">
                                            <div class="bootstrap-switch-container" style="width: 174px; margin-left: -58px;">
                                                <span class="bootstrap-switch-handle-on bootstrap-switch-success" style="width: 58px;">Sudah</span>
                                                <span class="bootstrap-switch-label" style="width: 58px;">&nbsp;</span>
                                                <span class="bootstrap-switch-handle-off bg-danger text-white" style="width: 58px;">Belum</span>
                                            </div>
                                        </div>
                                        @endif
                                        </div>
                                    </div>
                                </div>   
                                <div class="form-group">
                                    <!-- <a class="btn btn-success" href="{{url('/catat-ibu-hamil')}}">< Kembali</a> -->
                                    <!-- <button type="submit" class="btn btn-info m-r-10" id="step3-add-to-send-data">Kirimkan ></button>  -->
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <script>
        var tgl = document.getElementById("tanggal_pelayanan").value;
        var wkt = document.getElementById("timepicker").value;
        var tgl_hml = document.getElementById("tanggal_hamil").value;
        var tgl_lhr = document.getElementById("tanggal_lahir").value;

        $.ajax({
            type:'GET',
            url:'/getUsiaSaatPelayanan',
            data:{tgl:tgl,wkt:wkt,tgl_hml:tgl_hml,tgl_lhr:tgl_lhr},
            dataType:'json',
            success:function(data){
                document.getElementById("usia_saat_pelayanan").value = data.data.months;
                document.getElementById("usia_ibu_saat_pelayanan").value = data.data2.years;
            }
        });  

        function djjChange(){
            var djj = document.getElementById("djj").value;
            if(djj <100 || djj > 160){
                document.getElementById("ket_djj").value = "Gawat Janin";
            }else{
                document.getElementById("ket_djj").value = "Normal";
            }
        }
    </script>

    <!-- Picker Date Style -->
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.date.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.time.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/legacy.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <!-- <script src="{{asset('adminbite-10/assets/libs/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/datetimepicker/datetimepicker.init.js')}}"></script> -->
    
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js')}}"></script>
    <!-- Switch Style -->
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/js/bootstrap-switch.min.js')}}"></script>
    <!-- Select2 Style --> 
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/select2/select2.init.js')}}"></script>
    <!-- Wizard Style -->
    <script src="{{asset('adminbite-10/assets/libs/jquery-steps/build/jquery.steps.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script>
    //Basic Example
    $("#example-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true
    });

    // Basic Example with form
    var form = $("#example-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password"
            }
        }
    });
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    });

    // Advance Example

    var form = $("#example-advanced-form").show();

    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            // Forbid next action on "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age-2").val()) < 18) {
                return false;
            }
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onStepChanged: function(event, currentIndex, priorIndex) {
            // Used to skip the "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
                form.steps("next");
            }
            // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3) {
                form.steps("previous");
            }
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password-2"
            }
        }
    });

    // Dynamic Manipulation
    $("#example-manipulation").steps({
        headerTag: "h3",
        bodyTag: "section",
        enableAllSteps: true,
        enablePagination: false
    });

    //Vertical Steps

    $("#example-vertical").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        stepsOrientation: "vertical"
    });

    //Custom design form example
    $(".tab-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onFinished: function(event, currentIndex) {
            document.getElementById('wizard-form').submit();

        }
    });


    var form = $(".validation-wizard").show();

    $(".validation-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onStepChanging: function(event, currentIndex, newIndex) {
            return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
        },
        onFinishing: function(event, currentIndex) {
            return form.validate().settings.ignore = ":disabled", form.valid()
        },
        onFinished: function(event, currentIndex) {
            document.getElementById('wizard-form').submit();
        }
    }), $(".validation-wizard").validate({
        ignore: "input[type=hidden]",
        errorClass: "text-danger",
        successClass: "text-success",
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element)
        },
        rules: {
            email: {
                email: !0
            }
        }
    })
    </script>
    <!-- Switch Style     -->
    <script>
    $(".bt-switch input[type='checkbox'], .bt-switch input[type='radio']").bootstrapSwitch();
    var radioswitch = function() {
        var bt = function() {
            $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioState")
            }), $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck")
            }), $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck", !1)
            })
        };
        return {
            init: function() {
                bt()
            }
        }
    }();
    $(document).ready(function() {
        init()
    });
    </script>
    <script>
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    $('#date-fr').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', lang: 'fr', weekStart: 1, cancelText: 'ANNULER' });
    $('#date-end').bootstrapMaterialDatePicker({ weekStart: 0 });
    $('#date-start').bootstrapMaterialDatePicker({ weekStart: 0 }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });
    </script>
    <script type="text/javascript">
        $('.pickadate-disable').pickadate({
            disable: [

            ]
        });
    </script>

</body>
</html>
