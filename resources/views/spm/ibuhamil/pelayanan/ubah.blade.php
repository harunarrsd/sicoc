<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="GIK">
<meta name="author" content="GIK">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('adminbite-10/assets/images/sicoc-favicon.png')}}">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/select2/dist/css/select2.min.css')}}">
<link href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/morris.js/morris.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/toastr/build/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet">
<style>
    .sidebar-item a{
        font-weight:600;
    }
    .tx-c{
        text-align:center;
    }
    .bg-y{
        background:yellow;
    }
    .sidebar-link .icon-Record{
        visibility: visible !important;
    }
</style>
<title>Ubah Pelayanan Ibu Hamil</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.date.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.time.css')}}">
    <link type="text/css" href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
            <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Ibu Hamil</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Ibu Hamil</li>
                                    <li class="breadcrumb-item active" aria-current="page">Ubah Pelayanan</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="card" id="step3-add">
                    <div class="row">
                        <div class="col-lg-12 col-xlg-12 col-md-12">
                            <div class="card-body p-b-0 m-l-10">
                                <div class="row">
                                    <div class="col-lg-7 col-xlg-7 col-md-6 col-sm-12 p-b-0 p-l-0">
                                        <div class="card-body m-0 p-l-0">
                                            <h4 class="card-title">Ubah Pelayanan Ibu Hamil </h4>
                                            <h6 class="card-subtitle">Ubah 10T Pelayanan Ibu Hamil sesuai SPM</h6>
                                        </div> 
                                    </div>
                                    <div class="col-lg-3 col-xlg-3 col-md-3 col-sm-12">
                                        <!-- <h5 class="m-b-0 font-16 font-medium">Rohimam</h5>
                                        <span>3275041212960007</span>
                                        <h5 class="card-title m-b-0">
                                            {{ date('d M Y', strtotime('2019-08-12')) }}
                                            <span class="btn waves-effect waves-light btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mulai hamil sejak">
                                                Mulai Hamil
                                            </span>
                                        </h5>   -->
                                    </div>
                                    <div class="col-lg-2 col-xlg-2 col-md-2 col-sm-12">
                                        <!-- <div class="card-title">    
                                            <h5>Capaian SPM</h5>
                                            <span class="btn bg-info text-white btn-outline" data-toggle="tooltip" data-placement="top" title="" data-original-title="100% dan total 40T dari 40T">
                                                100%
                                            </span>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form method="POST" action="{{url('/simpanubah-catat-ibu-hamil')}}">{{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-6 col-xlg-6 col-md-12">
                            <div class="card-body p-t-0">
                                <div class="row">
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Tanggal Pelayanan</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-timer"></i></span>
                                            </div>
                                            <input type="date" class="form-control pickadate-disable" value="{{ explode(' ',$pelayanan['tanggal_pelayanan'])[0] }}" id="tanggal_pelayanan" name="tanggal_pelayanan" placeholder="Pilih" required onchange="tglpelChange()"/>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label for="exampleInputEmail1">Waktu Pelayanan</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                            </div>
                                            <input id="timepicker" class="form-control required" name="waktu_pelayanan" required placeholder="Pilih" value="{{ explode(':',explode(' ',$pelayanan['tanggal_pelayanan'])[1])[0] }}:{{ explode(':',explode(' ',$pelayanan['tanggal_pelayanan'])[1])[1] }}" onchange="wktpelChange()"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Usia Ibu Hamil</label><small class="text-muted"> ketika pelayanan</small>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" id="usia_ibu_saat_pelayanan" disabled class="form-control required" name="usia_ibu_saat_pelayanan" placeholder="Terisi Otomatis"/>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11">Tahun</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Usia Kehamilan</label><small class="text-muted"> ketika pelayanan</small>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" id="usia_saat_pelayanan" disabled class="form-control required" name="usia_saat_pelayanan" placeholder="Terisi Otomatis"/>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11">Bulan</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Fasilitas Kesahatan</label> <span class="text-danger">*</span>    
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="fasilitas_kesehatan" id="selectfas" required="" aria-invalid="true" class="form-control select2 custom-select" style="width: 82%; height:36px;" onchange="getMitra()">
                                                @if($fasilitas == "Rumah Sakit")
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit" selected>Rumah Sakit</option>
                                                <option value="Puskesmas">Puskesmas</option>
                                                <option value="Klinik">Kilnik</option>
                                                <option value="Posyandu">Posyandu</option>
                                                <option value="BPM">BPM</option>
                                                <option value="Luar Wilayah">Luar Wilayah</option>
                                                @elseif($fasilitas == "Puskesmas")
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                <option value="Puskesmas" selected>Puskesmas</option>
                                                <option value="Klinik">Kilnik</option>
                                                <option value="Posyandu">Posyandu</option>
                                                <option value="BPM">BPM</option>
                                                <option value="Luar Wilayah">Luar Wilayah</option>
                                                @elseif($fasilitas == "Kilnik")
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                <option value="Puskesmas">Puskesmas</option>
                                                <option value="Klinik" selected>Kilnik</option>
                                                <option value="Posyandu">Posyandu</option>
                                                <option value="BPM">BPM</option>
                                                <option value="Luar Wilayah">Luar Wilayah</option>
                                                @elseif($fasilitas == "Posyandu")
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                <option value="Puskesmas">Puskesmas</option>
                                                <option value="Klinik">Kilnik</option>
                                                <option value="Posyandu" selected>Posyandu</option>
                                                <option value="BPM">BPM</option>
                                                <option value="Luar Wilayah">Luar Wilayah</option>
                                                @elseif($fasilitas == "BPM")
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                <option value="Puskesmas">Puskesmas</option>
                                                <option value="Klinik">Kilnik</option>
                                                <option value="Posyandu">Posyandu</option>
                                                <option value="BPM" selected>BPM</option>
                                                <option value="Luar Wilayah">Luar Wilayah</option>
                                                @elseif($fasilitas == "Luar Wilayah")
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                <option value="Puskesmas">Puskesmas</option>
                                                <option value="Klinik">Kilnik</option>
                                                <option value="Posyandu">Posyandu</option>
                                                <option value="BPM">BPM</option>
                                                <option value="Luar Wilayah" selected>Luar Wilayah</option>
                                                @else
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                <option value="Puskesmas">Puskesmas</option>
                                                <option value="Klinik">Kilnik</option>
                                                <option value="Posyandu">Posyandu</option>
                                                <option value="BPM">BPM</option>
                                                <option value="Luar Wilayah">Luar Wilayah</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                        <label>Nama Fasilitas Pelayanan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-map-alt"></i></span>
                                            </div>
                                            <select name="selectlok" id="selectlok" aria-invalid="true" class="form-control select2 custom-select" style="width: 82%; height:36px;" onchange="getTenaga()">
                                                <option value="">Pilih</option> 
                                                 
                                            </select>
                                            <input type="text" name="lokasi_pelayanan" id="lokasi_pelayanan" value="{{ $lokasi}}" class="form-control" style="display:none;" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                        <label>Tenaga Kesehatan</label> 
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="selecttenaga" id="selecttenaga" class="form-control select2 custom-select" aria-invalid="true" style="width: 82%; height:36px;" onchange="getNamaStk()">
                                                <option value="">Pilih</option>
                                                
                                            </select>
                                            <input type="text" name="tenaga_kesehatan" id="tenaga_kesehatan" value="{{$tenaga_kerja}}" class="form-control" style="display:none;">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                        <label>Nama Dokter/Bidan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="selectnamaSTK" id="selectnamaSTK" aria-invalid="true" class="form-control select2 custom-select" style="width: 82%; height:36px;" onchange="getNamaStk2()">
                                                <option value="">Pilih</option>                                                    
                                            </select>
                                            <input type="text" id="nama_stk" name="nama_stk" value="{{ $nama_stk}}" class="form-control" style="display:none;" >
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                        <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                            <label>Berat Badan</label> 
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                </div>
                                                <input type="number" name="berat" class="form-control" placeholder="Co : 76" min="10" max="200" value={{$berat}}>
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">kg</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                            <label>Tinggi Badan</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                </div>
                                                <input type="number" name="tinggi" class="form-control" placeholder="Co : 165" min="50" max="200" value={{$tinggi}}>
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">cm</span>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-8 col-xlg-8 col-md-12 m-0">
                                        <label>Tekanan Darah</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="number" id="sistole" name="tekanan_darah1" class="form-control" min="0" max="999" placeholder="Co:120" onchange="sisdiaChange()" value="{{$sistole}}">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">/</span>
                                            </div>
                                            <input type="number" id="diastole" name="tekanan_darah2" class="form-control"min="0" max="999" placeholder="Co:80" onchange="sisdiaChange()" value="{{$diastole}}">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">mmHG</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                        <label>Ket. Tekanan Darah</label>
                                        <div class="input-group mb-3">
                                            <input type="text" id="ket_tek_darah" name="ket_tek_darah" disabled class="form-control" placeholder="Terisi Otomatis">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-8 col-xlg-8 col-md-12 m-0">
                                        <label>Lingkar Lengan Atas/LiLA</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="number" id="lila" name="lila1" class="form-control" min="0" placeholder="Co : 24" onchange="lilaChange()" value="{{$lila}}">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">,</span>
                                            </div>
                                            <input type="number" id="komalila" name="lila2" class="form-control" value="00" placeholder="00" min="0" max="99" onchange="lilaChange()" value="{{$komalila}}">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">cm</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                        <label>Nilai Status Gizi </label>
                                        <div class="input-group mb-3">
                                            <input type="text" id="ket_lila" disabled name="ket_lila" class="form-control" placeholder="Terisi Otomatis">
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <input type="text" id="tanggal_lahir" name="tanggal_lahir" value="{{$data_nik->tgl_lahir}}" style="display:none;">
                        <input type="text" id="tanggal_hamil" name="tanggal_hamil" value="{{$ibuhamil->tanggal_hamil}}" style="display:none;">
                        <input type="text" id="updated_by" name="updated_by" value="{{ Auth::user()['name'] }}" style="display:none;">
                        <input type="text" name="id" value="{{ $pelayanan['id'] }}" style="display:none;">
                        <div class="col-lg-6 col-xlg-6 col-md-12">
                            <div class="card-body p-t-0 p-l-0">
                                    <div class="form-group">
                                        <label>Tinggi Puncak Rahim (Fundus Uteril)</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="number" name="tprahim" class="form-control" placeholder="" value="{{$tprahim}}">
                                            <div class="input-group-prepend">
                                                <select name="satuantprahim" aria-invalid="true" class="input-group-text">
                                                    @if($satuantprahim == "jari bawah pusar")
                                                    <option value="cm">cm</option>
                                                    <option value="jari bawah pusar" selected>jari bawah pusar</option>
                                                    <option value="jari atas siymphisis">jari atas siymphisis</option>
                                                    @elseif($satuantprahim == "jari atas siymphisis")
                                                    <option value="cm">cm</option>
                                                    <option value="jari bawah pusar">jari bawah pusar</option>
                                                    <option value="jari atas siymphisis" selected>jari atas siymphisis</option>
                                                    @else
                                                    <option value="cm">cm</option>
                                                    <option value="jari bawah pusar">jari bawah pusar</option>
                                                    <option value="jari atas siymphisis">jari atas siymphisis</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                            <label>Presensi Janin</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                </div>
                                                <select name="presensijanin" aria-invalid="true" class="form-control">
                                                    @if($presensi == "Bokong")
                                                    <option value="">Pilih</option>
                                                    <option value="Bokong" selected>Bokong</option>
                                                    <option value="Kepala">Kepala</option>
                                                    <option value="Lain-lain">Lain-lain</option>
                                                    @elseif($presensi == "Kepala")
                                                    <option value="">Pilih</option>
                                                    <option value="Bokong">Bokong</option>
                                                    <option value="Kepala" selected>Kepala</option>
                                                    <option value="Lain-lain">Lain-lain</option>
                                                    @elseif($presensi == "Lain-lain")
                                                    <option value="">Pilih</option>
                                                    <option value="Bokong">Bokong</option>
                                                    <option value="Kepala">Kepala</option>
                                                    <option value="Lain-lain" selected>Lain-lain</option>
                                                    @else
                                                    <option value="">Pilih</option>
                                                    <option value="Bokong">Bokong</option>
                                                    <option value="Kepala">Kepala</option>
                                                    <option value="Lain-lain">Lain-lain</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-5 col-xlg-5 col-md-12 m-0">
                                            <label>Denyut Jantung Janin</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                </div>
                                                <input type="number" id="djj" name="djj" class="form-control" placeholder="Co:120" onchange="djjChange()" value="{{$djj}}">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">x/menit</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-3 col-xlg-3 col-md-12 m-0">
                                            <label>Ket. DJJ</label>
                                            <div class="input-group mb-3">
                                                <input type="text" disabled id="ket_djj" name="ket_djj" class="form-control" placeholder="Terisi Otomatis">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                            <label> Skrining Status Imunisasi Tetanus </label>
                                            <div class="input-group mb-3 bt-switch">
                                            @if($imun == "y")
                                            <input type="checkbox" name="skrining_imunisasi" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @else
                                                <input type="checkbox" name="skrining_imunisasi" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @endif
                                            </div>
                                        </div>
                                        <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                            <label> Imunisasi Tetanus Toksoid (TT) </label>
                                            <div class="input-group mb-3 bt-switch">
                                            @if($toksoid == "y")
                                            <input type="checkbox" name="imunisasi_tt" data-size="small"  checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @else
                                            <input type="checkbox" name="imunisasi_tt" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Jumlah Tablet Tambah Darah</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon22"><i class="ti-package"></i></span>
                                            </div>
                                            <input type="number" class="form-control" placeholder="Co : 12" name="tablet_tambah_darah" value="{{$tablet}}">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Tablet</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                            <label> Tes Kehamilan </label>
                                            <div class="input-group mb-3 bt-switch">
                                            @if($tes_hamil == "y")
                                            <input type="checkbox" name="tes_kehamilan" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @else
                                            <input type="checkbox" name="tes_kehamilan" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @endif
                                            </div>
                                        </div>
                                        <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                            <label> Pemeriksaan Hemoglobin Darah </label>
                                            <div class="input-group mb-3 bt-switch">
                                            @if($tes_hb == "y")
                                            <input type="checkbox" name="tes_hb" data-size="small"  checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @else
                                            <input type="checkbox" name="tes_hb" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @endif

                                            </div>
                                        </div>
                                        <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                            <label> Pemeriksaan Golongan Darah</label>
                                            <div class="input-group mb-3 bt-switch">
                                            @if($tes_gd == "y")
                                            <input type="checkbox" name="tes_gd" data-size="small"  checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @else
                                            <input type="checkbox" name="tes_gd" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @endif
                                            </div>
                                        </div>
                                        <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                            <label> Pemeriksaan Protein Urin </label>
                                            <div class="input-group mb-3 bt-switch">
                                            @if($tes_urin == "y")
                                            <input type="checkbox" name="tes_protein_urin" data-size="small"  checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @else
                                            <input type="checkbox" name="tes_protein_urin" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @endif
                                            </div>
                                        </div>
                                        <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                            <label> Pemeriksaan Tes HIV </label>
                                            <div class="input-group mb-3 bt-switch">
                                            @if($tes_hiv == "y")
                                            <input type="checkbox" name="tes_hiv" data-size="small"  checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @else
                                            <input type="checkbox" name="tes_hiv" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @endif
                                            </div>
                                        </div>
                                        <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                            <label> Pemeriksaan Tes HbsAg </label>
                                            <div class="input-group mb-3 bt-switch">
                                            @if($tes_hbsag == "y")
                                            <input type="checkbox" name="tes_hbsag" data-size="small"  checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @else
                                            <input type="checkbox" name="tes_hbsag" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                            <label> Tatalaksana </label>
                                            <div class="input-group mb-3 bt-switch">
                                            @if($tatalaksana == "y")
                                            <input type="checkbox" name="tatalaksana" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @else
                                            <input type="checkbox" name="tatalaksana" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @endif

                                            </div>
                                        </div>
                                        <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                            <label> Temu Wicara (Konseling) </label>
                                            <div class="input-group mb-3 bt-switch">
                                            @if($tes_urin == "y")
                                            <input type="checkbox" name="temu_wicara" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @else
                                            <input type="checkbox" name="temu_wicara" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                            @endif
                                            </div>
                                        </div>
                                    </div>   
                                    <div class="form-group">
                                        <!-- <a class="btn btn-success" href="{{url('/catat-ibu-hamil')}}">< Kembali</a> -->
                                        <button type="submit" class="btn btn-info m-r-10">Kirimkan Perubahan ></button> 
                                    </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <script>
        window.onload = function () {
            var djj = document.getElementById("djj").value;
            if(djj <100 || djj > 160){
                document.getElementById("ket_djj").value = "Gawat Janin";
            }else{
                document.getElementById("ket_djj").value = "Normal";
            }

            var lila = document.getElementById("lila").value;
            var komalila = document.getElementById("komalila").value;

            lila = lila+"."+komalila;
            lila = parseFloat(lila);

            if(lila <= 23.5){
                document.getElementById("ket_lila").value = "Kekurangan Energi Kronis (KEK)";
            }else{
                document.getElementById("ket_lila").value = "Normal";
            }

            var sis = document.getElementById("sistole").value;
            var dia = document.getElementById("diastole").value;

            if(parseInt(sis) > 120 || parseInt(dia) > 80){
                document.getElementById("ket_tek_darah").value = "Tinggi";
            } 
            else if(parseInt(sis) < 90 || parseInt(dia) < 60){
                document.getElementById("ket_tek_darah").value = "Rendah";                
            }
            else if(parseInt(sis) < 120 &&  parseInt(sis) >= 90 && parseInt(dia) < 80 && parseInt(dia) >= 60 ){
                document.getElementById("ket_tek_darah").value = "Normal";
            }

            var tgl = document.getElementById("tanggal_pelayanan").value;
            var wkt = document.getElementById("timepicker").value;
            var tgl_hml = document.getElementById("tanggal_hamil").value;
            var tgl_lhr = document.getElementById("tanggal_lahir").value;

            $.ajax({
                type:'GET',
                url:'/getUsiaSaatPelayanan',
                data:{tgl:tgl,wkt:wkt,tgl_hml:tgl_hml,tgl_lhr:tgl_lhr},
                dataType:'json',
                success:function(data){
                    console.log(data);
                    document.getElementById("usia_saat_pelayanan").value = data.data.months;
                    document.getElementById("usia_ibu_saat_pelayanan").value = data.data2.years;
                }
            });

            var fas = document.getElementById("selectfas").value;
            var lok = document.getElementById("lokasi_pelayanan").value;
            var tenaga = document.getElementById("tenaga_kesehatan").value;
            var nama_stk = document.getElementById("nama_stk").value;

            $('#selectlok').find('option').not(':first').remove();
            $('#selecttenaga').find('option').not(':first').remove();
            $('#selectnamaSTK').find('option').not(':first').remove();
            $.ajax({
                type:'GET',
                url:'/findmitra',
                data:{id:fas},
                dataType:'json',
                success:function(data){
                    var len = 0;
                    if(data['data'] != null){
                        len = data['data'].length;
                    }
                    var idku = 0;
                    for(var i=0;i<len;i++){
                        
                        var id = data['data'][i]['id'];
                        var jenis = data['data'][i]['jenis_mitra'];
                        var nama = data['data'][i]['nama'];
                        var lok = document.getElementById("lokasi_pelayanan").value;
                        if(jenis+" "+nama == lok){
                            var option = "<option value='"+id+"_"+jenis+" "+nama+"' selected>"+jenis+" "+nama+"</option>";
                            idku = id+"_"+jenis+" "+nama;
                        }else{
                            var option = "<option value='"+id+"_"+jenis+" "+nama+"'>"+jenis+" "+nama+"</option>";
                        }
                        $("#selectlok").append(option);
                    }

                    var lok_ok = idku.split("_");
                    $.ajax({
                        type:'GET',
                        url:'/findtenaga',
                        data:{id:lok_ok[0]},
                        dataType:'json',
                        success:function(data){
                            var idgettenaga = 0;
                            var tenaga = data['data'].split("_");
                            for(var i=0;i<tenaga.length;i++){
                                var tenaga_kes = document.getElementById("tenaga_kesehatan").value;
                                if(tenaga[i] == tenaga_kes){
                                    var option = "<option value='"+tenaga[i]+"' selected>"+tenaga[i]+"</option>";                                    
                                    idgettenaga = tenaga[i];
                                }else{
                                    var option = "<option value='"+tenaga[i]+"'>"+tenaga[i]+"</option>";
                                }
                                $("#selecttenaga").append(option);
                            }

                            $.ajax({
                                type:'GET',
                                url:'/findnamastk',
                                data:{tenaga:idgettenaga,mitra:idku},
                                dataType:'json',
                                success:function(data){
                                    var nama = data['data'].split("_");
                                    for(var i=0;i<nama.length;i++){
                                        var nama_stk = document.getElementById("nama_stk").value;
                                        if(nama[i] == nama_stk){
                                            var option = "<option value='"+nama[i]+"' selected>"+nama[i]+"</option>";                                            
                                        }else{
                                            var option = "<option value='"+nama[i]+"'>"+nama[i]+"</option>";
                                        }
                                        $("#selectnamaSTK").append(option);
                                    }
                                }
                            });

                        }
                    });
                }
            });

        }
        
    </script>
    <script>
        function djjChange(){
            var djj = document.getElementById("djj").value;
            if(djj <100 || djj > 160){
                document.getElementById("ket_djj").value = "Gawat Janin";
            }else{
                document.getElementById("ket_djj").value = "Normal";
            }
        }


        function lilaChange(){
            var lila = document.getElementById("lila").value;
            var komalila = document.getElementById("komalila").value;

            lila = lila+"."+komalila;
            lila = parseFloat(lila);

            if(lila <= 23.5){
                document.getElementById("ket_lila").value = "Kekurangan Energi Kronis (KEK)";
            }else{
                document.getElementById("ket_lila").value = "Normal";
            }

        }        

        function sisdiaChange(){
            var sis = document.getElementById("sistole").value;
            var dia = document.getElementById("diastole").value;

            if(parseInt(sis) > 120 || parseInt(dia) > 80){
                document.getElementById("ket_tek_darah").value = "Tinggi";
            } 
            else{
                document.getElementById("ket_tek_darah").value = "Normal";
            }
        }

        function tglpelChange(){
            var tgl = document.getElementById("tanggal_pelayanan").value;
            var wkt = document.getElementById("timepicker").value;
            var tgl_hml = document.getElementById("tanggal_hamil").value;
            var tgl_lhr = document.getElementById("tanggal_lahir").value;

            $.ajax({
                type:'GET',
                url:'/getUsiaSaatPelayanan',
                data:{tgl:tgl,wkt:wkt,tgl_hml:tgl_hml,tgl_lhr:tgl_lhr},
                dataType:'json',
                success:function(data){
                    console.log(data);
                    document.getElementById("usia_saat_pelayanan").value = data.data.months;
                    document.getElementById("usia_ibu_saat_pelayanan").value = data.data2.years;
                }
            });            
        }

        function wktpelChange(){
            var tgl = document.getElementById("tanggal_pelayanan").value;
            var wkt = document.getElementById("timepicker").value;
            var tgl_hml = document.getElementById("tanggal_hamil").value;
            var tgl_lhr = document.getElementById("tanggal_lahir").value;
            
            $.ajax({
                type:'GET',
                url:'/getUsiaSaatPelayanan',
                data:{tgl:tgl,wkt:wkt,tgl_hml:tgl_hml,tgl_lhr:tgl_lhr},
                dataType:'json',
                success:function(data){
                    console.log(data);
                    document.getElementById("usia_saat_pelayanan").value = data.data.months;
                    document.getElementById("usia_ibu_saat_pelayanan").value = data.data2.years;
                }
            });            
        }


        function getMitra(){
            var fas = document.getElementById("selectfas").value;
            $('#selectlok').find('option').not(':first').remove();
            $('#selecttenaga').find('option').not(':first').remove();
            $('#selectnamaSTK').find('option').not(':first').remove();
            $.ajax({
                type:'GET',
                url:'/findmitra',
                data:{id:fas},
                dataType:'json',
                success:function(data){
                    var len = 0;
                    if(data['data'] != null){
                        len = data['data'].length;
                    }
                    for(var i=0;i<len;i++){
                        var id = data['data'][i]['id'];
                        var jenis = data['data'][i]['jenis_mitra'];
                        var nama = data['data'][i]['nama'];

                        var option = "<option value='"+id+"_"+jenis+" "+nama+"'>"+jenis+" "+nama+"</option>";
                        $("#selectlok").append(option);
                    }
                    
                }
            });
        }
        function getTenaga(){
            var lok = document.getElementById("selectlok").value;
            var lok_ok = lok.split("_");
            $('#selectnamaSTK').find('option').not(':first').remove();
            $('#selecttenaga').find('option').not(':first').remove();
            $.ajax({
                type:'GET',
                url:'/findtenaga',
                data:{id:lok_ok[0]},
                dataType:'json',
                success:function(data){
                    var tenaga = data['data'].split("_");
                    for(var i=0;i<tenaga.length;i++){
                        var option = "<option value='"+tenaga[i]+"'>"+tenaga[i]+"</option>";
                        $("#selecttenaga").append(option);
                    }
                }
            });
            document.getElementById("lokasi_pelayanan").value = lok_ok[1];
        }
        function getNamaStk(){
            var nama_stk = document.getElementById("nama_stk").value;

            // if(nama_stk != null){
            //     document.getElementById("nama_stk").value = null;
            // }

            var tenaga = document.getElementById("selecttenaga").value;
            var lok = document.getElementById("selectlok").value;
            var lok_ok = lok.split("_");
            $('#selectnamaSTK').find('option').not(':first').remove();
            $.ajax({
                type:'GET',
                url:'/findnamastk',
                data:{tenaga:tenaga,mitra:lok_ok[0]},
                dataType:'json',
                success:function(data){
                    var nama = data['data'].split("_");
                    for(var i=0;i<nama.length;i++){
                        var option = "<option value='"+nama[i]+"'>"+nama[i]+"</option>";
                        $("#selectnamaSTK").append(option);
                    }
                }
            });
            document.getElementById("tenaga_kesehatan").value = tenaga;
        }
        function getNamaStk2(){
            var selectnamaSTK = document.getElementById("selectnamaSTK").value;
            document.getElementById("nama_stk").value = selectnamaSTK;
        }
    </script>

    <!-- Picker Date Style -->
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.date.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.time.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/legacy.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <!-- <script src="{{asset('adminbite-10/assets/libs/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/datetimepicker/datetimepicker.init.js')}}"></script> -->
    
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js')}}"></script>
    <!-- Switch Style -->
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/js/bootstrap-switch.min.js')}}"></script>
    <!-- Select2 Style --> 
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/select2/select2.init.js')}}"></script>
    <!-- Wizard Style -->
    <script src="{{asset('adminbite-10/assets/libs/jquery-steps/build/jquery.steps.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script>
    //Basic Example
    $("#example-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true
    });

    // Basic Example with form
    var form = $("#example-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password"
            }
        }
    });
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    });

    // Advance Example

    var form = $("#example-advanced-form").show();

    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            // Forbid next action on "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age-2").val()) < 18) {
                return false;
            }
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onStepChanged: function(event, currentIndex, priorIndex) {
            // Used to skip the "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
                form.steps("next");
            }
            // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3) {
                form.steps("previous");
            }
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password-2"
            }
        }
    });

    // Dynamic Manipulation
    $("#example-manipulation").steps({
        headerTag: "h3",
        bodyTag: "section",
        enableAllSteps: true,
        enablePagination: false
    });

    //Vertical Steps

    $("#example-vertical").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        stepsOrientation: "vertical"
    });

    //Custom design form example
    $(".tab-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onFinished: function(event, currentIndex) {
            document.getElementById('wizard-form').submit();

        }
    });


    var form = $(".validation-wizard").show();

    $(".validation-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onStepChanging: function(event, currentIndex, newIndex) {
            return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
        },
        onFinishing: function(event, currentIndex) {
            return form.validate().settings.ignore = ":disabled", form.valid()
        },
        onFinished: function(event, currentIndex) {
            document.getElementById('wizard-form').submit();
        }
    }), $(".validation-wizard").validate({
        ignore: "input[type=hidden]",
        errorClass: "text-danger",
        successClass: "text-success",
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element)
        },
        rules: {
            email: {
                email: !0
            }
        }
    })
    </script>
    <!-- Switch Style     -->
    <script>
    $(".bt-switch input[type='checkbox'], .bt-switch input[type='radio']").bootstrapSwitch();
    var radioswitch = function() {
        var bt = function() {
            $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioState")
            }), $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck")
            }), $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck", !1)
            })
        };
        return {
            init: function() {
                bt()
            }
        }
    }();
    $(document).ready(function() {
        init()
    });
    </script>
    <script>
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    $('#date-fr').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', lang: 'fr', weekStart: 1, cancelText: 'ANNULER' });
    $('#date-end').bootstrapMaterialDatePicker({ weekStart: 0 });
    $('#date-start').bootstrapMaterialDatePicker({ weekStart: 0 }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });
    </script>
    <script type="text/javascript">
        $('.pickadate-disable').pickadate({
            disable: [
                @foreach($tgl_pel as $i => $tgl)
                    [{{ explode("-",$tgl)[0] }}, {{ explode("-",$tgl)[1] }}-1, {{ explode("-",explode(" ",$tgl)[0])[2] }}],
                @endforeach
            ]
        });
    </script>

</body>
</html>