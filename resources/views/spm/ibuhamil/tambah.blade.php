<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="GIK">
<meta name="author" content="GIK">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('adminbite-10/assets/images/sicoc-favicon.png')}}">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/select2/dist/css/select2.min.css')}}">
<link href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/morris.js/morris.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/toastr/build/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet">
<style>
    .sidebar-item a{
        font-weight:600;
    }
    .tx-c{
        text-align:center;
    }
    .bg-y{
        background:yellow;
    }
    .sidebar-link .icon-Record{
        visibility: visible !important;
    }
</style>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.date.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.time.css')}}">
    <link type="text/css" href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
            <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}">
<title>Tambah Data Ibu Hamil</title>   
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Ibu Hamil</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Ibu Hamil</li>
                                    <li class="breadcrumb-item active" aria-current="page">Pendaftaran Ibu Hamil</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('success')}}
                            </div>
                        @endif
                        @if(session()->has('danger'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('danger')}}
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Pendaftaran Ibu Hamil</h4>
                                <h6 class="card-subtitle">Pendaftaran Ibu Hamil pada SICOC</h6>
                                <form method="POST" action="{{url('/simpan-ibu-hamil')}}">{{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>NIK/Nama Ibu Hamil</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="nik" required="" id="select" aria-invalid="true" class="form-control select2 custom-select" style="width: 90%; height:10px;" onchange="myFunction()">
                                                <option value="">Pilih</option>
                                                @foreach($kriteriaibuhamil as $i => $item)                    
                                                    <option value="{{ $list_no_telp[$i] }}_{{$list_minG[$i]}}_{{ $nikcaibuhamil[$i] }}">{{$nikcaibuhamil[$i]}} - {{$item}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                        <label>Nama Suami</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" name="nama_suami" required="" aria-invalid="true" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-3 col-xlg-3 col-md-12 m-0">
                                        <label>Hari Pertama Haid Terakhir</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                            </div>
                                            <input type="date" class="form-control pickadate-disable" name="tanggal_hamil" required/>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-3 col-xlg-3 col-md-12 m-0">
                                        <label>Waktu HPHT</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                            </div>
                                            <input id="timepicker" class="form-control required" name="waktu_hpht" placeholder="Pilih"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                        <label>Kode Riwayat Pelayanan</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11">G</span>
                                            </div>
                                            <input type="number" id ="g" name="g" class="form-control required" min="1" max="99" placeholder="G" onchange="gChange();">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11">P</span>
                                            </div>
                                            <input type="number" id ="p" name="p" class="form-control required" min="0" max="99" placeholder="P" onchange="pChange();">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11">A</span>
                                            </div>
                                            <input type="number" id ="a" name="a" class="form-control required" min="0" max="99" placeholder="A" onchange="aChange();">
                                            <input type="text" id ="created_by" name="created_by" value="{{ Auth::user()['name'] }}" class="form-control required" style="display:none;">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-2 col-xlg-2 col-md-12 m-0">
                                        <label>No. Telepon</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-mobile"></i></span>
                                            </div>
                                            <input type="text" id="no_telp" name="no_telp" class="form-control required" placeholder="Pilih NIK Ibu Hamil">
                                        </div>
                                    </div>
                                    <div class="form-group p-l-10 col-lg-4 col-md-6 col-sm-12">
                                        <button type="submit" class="btn btn-info m-r-10">Kirimkan ></button> 
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/select2/select2.init.js')}}"></script>
    <!-- Picker Date Style -->
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.date.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.time.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/legacy.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <!-- <script src="{{asset('adminbite-10/assets/libs/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/datetimepicker/datetimepicker.init.js')}}"></script> -->
    
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js')}}"></script>
    <script>
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    $('#date-fr').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', lang: 'fr', weekStart: 1, cancelText: 'ANNULER' });
    $('#date-end').bootstrapMaterialDatePicker({ weekStart: 0 });
    $('#date-start').bootstrapMaterialDatePicker({ weekStart: 0 }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });
    </script>
    <script>
        function myFunction() {
        var x = document.getElementById("select").value;

        var element = x.split("_");
        document.getElementById("g").value = element[1];
        document.getElementById("p").value = element[1]-1;
        document.getElementById("a").value = 0;
        $("#g").attr({"min" : element[1]});
        $("#p").attr({"max" : element[1]-1});
        $("#a").attr({"max" : element[1]-1});
        }
        function gChange(){
            var g = document.getElementById("g").value;
            var p = document.getElementById("p").value;
            var a = document.getElementById("a").value;
            $("#p").attr({"max" : g-1});
            $("#a").attr({"max" : g-1});
        }
        function pChange(){
            var g = document.getElementById("g").value;
            var p = document.getElementById("p").value;
            document.getElementById("a").value = (g-1)-p;
        }
        function aChange(){
            var g = document.getElementById("g").value;
            var a = document.getElementById("a").value;
            document.getElementById("p").value = (g-1)-a;
        }
    </script>
    <script type="text/javascript">
        $('.pickadate-disable').pickadate({
            disable: [

            ]
        });
    </script>
</body>
</html>