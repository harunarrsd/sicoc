<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')
@yield('head-adminlte')
<title>Input SPM SICOC</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('head-preloader')
    <div id="main-wrapper">
        @yield('head-topbar')
        @yield('head-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Standar Pelayanan Minimal</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">SPM</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Input Pelayanan</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                <div class="col-md-12">
                        <div class="card">
                            <div class="card-body p-b-0">
                                <h4 class="card-title">Customtab Tab</h4>
                                <h6 class="card-subtitle">Use default tab with class <code>customtab</code></h6> </div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs customtab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home2" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Home</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile2" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Profile</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages2" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Messages</span></a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="home2" role="tabpanel">
                                    <div class="p-20">
                                        <h3>Best Clean Tab ever</h3>
                                        <h4>you can use it with the small code</h4>
                                        <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                                    </div>
                                </div>
                                <div class="tab-pane  p-20" id="profile2" role="tabpanel">2</div>
                                <div class="tab-pane p-20" id="messages2" role="tabpanel">3</div>
                            </div>
                            <div class="card-body p-t-0">
                                <pre class="language-html scrollable">
                                    <code>
                                        &lt;!-- Nav tabs --&gt;
                                        &lt;ul class=&quot;nav nav-tabs customtab&quot; role=&quot;tablist&quot;&gt;
                                            &lt;li class=&quot;nav-item&quot;&gt; &lt;a class=&quot;nav-link active&quot; data-toggle=&quot;tab&quot; href=&quot;#home2&quot; role=&quot;tab&quot;&gt;&lt;span class=&quot;hidden-sm-up&quot;&gt;&lt;i class=&quot;ti-home&quot;&gt;&lt;/i&gt;&lt;/span&gt; &lt;span class=&quot;hidden-xs-down&quot;&gt;Home&lt;/span&gt;&lt;/a&gt; &lt;/li&gt;
                                            &lt;li class=&quot;nav-item&quot;&gt; &lt;a class=&quot;nav-link&quot; data-toggle=&quot;tab&quot; href=&quot;#profile2&quot; role=&quot;tab&quot;&gt;&lt;span class=&quot;hidden-sm-up&quot;&gt;&lt;i class=&quot;ti-user&quot;&gt;&lt;/i&gt;&lt;/span&gt; &lt;span class=&quot;hidden-xs-down&quot;&gt;Profile&lt;/span&gt;&lt;/a&gt; &lt;/li&gt;
                                            &lt;li class=&quot;nav-item&quot;&gt; &lt;a class=&quot;nav-link&quot; data-toggle=&quot;tab&quot; href=&quot;#messages2&quot; role=&quot;tab&quot;&gt;&lt;span class=&quot;hidden-sm-up&quot;&gt;&lt;i class=&quot;ti-email&quot;&gt;&lt;/i&gt;&lt;/span&gt; &lt;span class=&quot;hidden-xs-down&quot;&gt;Messages&lt;/span&gt;&lt;/a&gt; &lt;/li&gt;
                                        &lt;/ul&gt;
                                        &lt;!-- Tab panes --&gt;
                                        &lt;div class=&quot;tab-content&quot;&gt;
                                            &lt;div class=&quot;tab-pane active&quot; id=&quot;home2&quot; role=&quot;tabpanel&quot;&gt;
                                                &lt;div class=&quot;p-20&quot;&gt;
                                                    &lt;h3&gt;Best Clean Tab ever&lt;/h3&gt;
                                                    &lt;h4&gt;you can use it with the small code&lt;/h4&gt;
                                                    &lt;p&gt;Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.&lt;/p&gt;
                                                &lt;/div&gt;
                                            &lt;/div&gt;
                                            &lt;div class=&quot;tab-pane  p-20&quot; id=&quot;profile2&quot; role=&quot;tabpanel&quot;&gt;2&lt;/div&gt;
                                            &lt;div class=&quot;tab-pane p-20&quot; id=&quot;messages2&quot; role=&quot;tabpanel&quot;&gt;3&lt;/div&gt;
                                        &lt;/div&gt;                                        
                                    </code>
                                </pre>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="loginform">
                    <div class="col-lg-12 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body wizard-content">
                                <div class="form-group">
                                    <h5>Masukkan Nomor Induk Kependudukan <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <div class="input-group">
                                        <input type="text" id="nik" maxlength="16" name="noChar" class="form-control" required data-validation-required-message="Tidak boleh kosong" required data-validation-containsnumber-regex="(\d)+" data-validation-containsnumber-message="Mohon Masukkan NIK dengan benar, hanya berupa angka">
                                        <a href="javascript:void(0)" id="to-recover" class="text-info m-l-5"><button class="btn btn-info" type="button">Lanjutkan ></button></a> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="row" id="rra">
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                            <div class="card">
                                <div class="card-body">
                                    <center class="m-t-30"> <img src="{{asset('adminbite-10/assets/images/users/user.png')}}" class="rounded-circle" width="150" />
                                        <h4 class="card-title m-t-10" id="mynama">Nama Warga</h4>
                                        <h6 class="card-subtitle" id="mynik">Nomor Induk Kependudukan</h6>
                                        <div class="row text-center justify-content-md-center">
                                            <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i> <font class="font-medium">Jumlah Pelayanan</font></a></div>
                                            <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-picture"></i> <font class="font-medium">Presentase Pelayanan</font></a></div>
                                        </div>
                                    </center>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-4 col-xlg-3 col-md-5"> 
                            <div class="card">
                                <div class="card-body">
                                <small class="text-muted">NIK </small>
                                <h6 id="mynik1">327504121296000*7</h6> 
                                
                                <small class="text-muted p-t-30 db">Nama Lengkap</small>
                                <h6 id="mynama1">Warga Teladan</h6>                                 
                                <small class="text-muted p-t-30 db">Jenis Kelamin</small>
                                <h6 id="myjk">Laki-laki</h6>
                                <small class="text-muted p-t-30 db">Agama</small>
                                <h6 id="myagama">Islam</h6>
                                <small class="text-muted p-t-30 db">Gol. Darah</small>
                                <h6 id="mygd">AB</h6>
                                <small class="text-muted p-t-30 db">Kewarganegaraan</small>
                                <h6 id="mykwn">Indonesia</h6>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-4 col-xlg-3 col-md-5"> 
                            <div class="card">
                                <div class="card-body">
                                    <small class="text-muted p-t-30 db">Alamat</small>
                                    <h6 id="myal"></h6>
                                </div>
                                <div class="map-box">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d470029.1604841957!2d72.29955005258641!3d23.019996818380896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e848aba5bd449%3A0x4fcedd11614f6516!2sAhmedabad%2C+Gujarat!5e0!3m2!1sen!2sin!4v1493204785508" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div> 
                                <div class="card-body">
                                    <a href="javascript:void(0)" id="to-login2" class="text-info m-l-5"><button class="btn btn-success" type="button">< Masukkan NIK lain</button></a>
                                    <a href="javascript:void(0)" id="to-layanan" class="text-info m-l-5"><button class="btn btn-info" type="button">Lanjutkan ></button></a>
                                </div>
                            </div>
                    </div>
                </div> -->
                <div class="row" id="salahnik">
                    <div class="col-lg-12 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <center>
                                    <h5>Mohon Maaf NIK yang anda masukkan tidak terdaftar</h5>
                                    <div class="controls">
                                        <center>
                                            <a href="javascript:void(0)" id="to-login" class="text-info m-l-5"><button class="btn btn-success" type="button">< Masukkan NIK lain</button></a>
                                            <a href="javascript:void(0)" id="to-daftar" class="text-info m-l-5"><button class="btn btn-info" type="button">Daftarkan ></button></a>
                                        </center>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="recoverform">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Riwayat Pelayanan</h4>
                                <div class="d-flex no-block align-items-center m-b-10">
                                    <div class="m-r-10">
                                        <img src="{{asset('adminbite-10/assets/images/users/user.png')}}" alt="user" class="rounded-circle" width="45">
                                    </div>
                                    <div class="">
                                        <h5 class="m-b-0 font-16 font-medium" id="namawarga">Nama Warga </h5>
                                        <span id="nikwarga">3275040309890001</span>
                                    </div>
                                </div>
                                <!-- <h6 class="card-subtitle">On a per-column basis (i.e. order by a specific column and then a secondary column if the data in the first column is identical), through the <code> columns.orderData</code> option.</h6> -->
                                <div class="table-responsive">
                                    <table id="multi_col_order" class="table table-striped table-bordered display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>First name</th>
                                                <th>Last name</th>
                                                <th>Position</th>
                                                <th>Office</th>
                                                <th>Salary</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Tiger</td>
                                                <td>Nixon</td>
                                                <td>System Architect</td>
                                                <td>Edinburgh</td>
                                                <td>$320,800</td>
                                            </tr>
                                            <tr>
                                                <td>Garrett</td>
                                                <td>Winters</td>
                                                <td>Accountant</td>
                                                <td>Tokyo</td>
                                                <td>$170,750</td>
                                            </tr>
                                            <tr>
                                                <td>Ashton</td>
                                                <td>Cox</td>
                                                <td>Junior Technical Author</td>
                                                <td>San Francisco</td>
                                                <td>$86,000</td>
                                            </tr>
                                            <tr>
                                                <td>Cedric</td>
                                                <td>Kelly</td>
                                                <td>Senior Javascript Developer</td>
                                                <td>Edinburgh</td>
                                                <td>$433,060</td>
                                            </tr>
                                            <tr>
                                                <td>Airi</td>
                                                <td>Satou</td>
                                                <td>Accountant</td>
                                                <td>Tokyo</td>
                                                <td>$162,700</td>
                                            </tr>
                                            <tr>
                                                <td>Brielle</td>
                                                <td>Williamson</td>
                                                <td>Integration Specialist</td>
                                                <td>New York</td>
                                                <td>$372,000</td>
                                            </tr>
                                            <tr>
                                                <td>Herrod</td>
                                                <td>Chandler</td>
                                                <td>Sales Assistant</td>
                                                <td>San Francisco</td>
                                                <td>$137,500</td>
                                            </tr>
                                            <tr>
                                                <td>Rhona</td>
                                                <td>Davidson</td>
                                                <td>Integration Specialist</td>
                                                <td>Tokyo</td>
                                                <td>$327,900</td>
                                            </tr>
                                            <tr>
                                                <td>Colleen</td>
                                                <td>Hurst</td>
                                                <td>Javascript Developer</td>
                                                <td>San Francisco</td>
                                                <td>$205,500</td>
                                            </tr>
                                            <tr>
                                                <td>Sonya</td>
                                                <td>Frost</td>
                                                <td>Software Engineer</td>
                                                <td>Edinburgh</td>
                                                <td>$103,600</td>
                                            </tr>
                                            <tr>
                                                <td>Jena</td>
                                                <td>Gaines</td>
                                                <td>Office Manager</td>
                                                <td>London</td>
                                                <td>$90,560</td>
                                            </tr>
                                            <tr>
                                                <td>Quinn</td>
                                                <td>Flynn</td>
                                                <td>Support Lead</td>
                                                <td>Edinburgh</td>
                                                <td>$342,000</td>
                                            </tr>
                                            <tr>
                                                <td>Charde</td>
                                                <td>Marshall</td>
                                                <td>Regional Director</td>
                                                <td>San Francisco</td>
                                                <td>$470,600</td>
                                            </tr>
                                            <tr>
                                                <td>Haley</td>
                                                <td>Kennedy</td>
                                                <td>Senior Marketing Designer</td>
                                                <td>London</td>
                                                <td>$313,500</td>
                                            </tr>
                                            <tr>
                                                <td>Tatyana</td>
                                                <td>Fitzpatrick</td>
                                                <td>Regional Director</td>
                                                <td>London</td>
                                                <td>$385,750</td>
                                            </tr>
                                            <tr>
                                                <td>Michael</td>
                                                <td>Silva</td>
                                                <td>Marketing Designer</td>
                                                <td>London</td>
                                                <td>$198,500</td>
                                            </tr>
                                            <tr>
                                                <td>Paul</td>
                                                <td>Byrd</td>
                                                <td>Chief Financial Officer (CFO)</td>
                                                <td>New York</td>
                                                <td>$725,000</td>
                                            </tr>
                                            <tr>
                                                <td>Gloria</td>
                                                <td>Little</td>
                                                <td>Systems Administrator</td>
                                                <td>New York</td>
                                                <td>$237,500</td>
                                            </tr>
                                            <tr>
                                                <td>Bradley</td>
                                                <td>Greer</td>
                                                <td>Software Engineer</td>
                                                <td>London</td>
                                                <td>$132,000</td>
                                            </tr>
                                            <tr>
                                                <td>Dai</td>
                                                <td>Rios</td>
                                                <td>Personnel Lead</td>
                                                <td>Edinburgh</td>
                                                <td>$217,500</td>
                                            </tr>
                                            <tr>
                                                <td>Jenette</td>
                                                <td>Caldwell</td>
                                                <td>Development Lead</td>
                                                <td>New York</td>
                                                <td>$345,000</td>
                                            </tr>
                                            <tr>
                                                <td>Yuri</td>
                                                <td>Berry</td>
                                                <td>Chief Marketing Officer (CMO)</td>
                                                <td>New York</td>
                                                <td>$675,000</td>
                                            </tr>
                                            <tr>
                                                <td>Caesar</td>
                                                <td>Vance</td>
                                                <td>Pre-Sales Support</td>
                                                <td>New York</td>
                                                <td>$106,450</td>
                                            </tr>
                                            <tr>
                                                <td>Doris</td>
                                                <td>Wilder</td>
                                                <td>Sales Assistant</td>
                                                <td>Sidney</td>
                                                <td>$85,600</td>
                                            </tr>
                                            <tr>
                                                <td>Angelica</td>
                                                <td>Ramos</td>
                                                <td>Chief Executive Officer (CEO)</td>
                                                <td>London</td>
                                                <td>$1,200,000</td>
                                            </tr>
                                            <tr>
                                                <td>Gavin</td>
                                                <td>Joyce</td>
                                                <td>Developer</td>
                                                <td>Edinburgh</td>
                                                <td>$92,575</td>
                                            </tr>
                                            <tr>
                                                <td>Jennifer</td>
                                                <td>Chang</td>
                                                <td>Regional Director</td>
                                                <td>Singapore</td>
                                                <td>$357,650</td>
                                            </tr>
                                            <tr>
                                                <td>Brenden</td>
                                                <td>Wagner</td>
                                                <td>Software Engineer</td>
                                                <td>San Francisco</td>
                                                <td>$206,850</td>
                                            </tr>
                                            <tr>
                                                <td>Fiona</td>
                                                <td>Green</td>
                                                <td>Chief Operating Officer (COO)</td>
                                                <td>San Francisco</td>
                                                <td>$850,000</td>
                                            </tr>
                                            <tr>
                                                <td>Shou</td>
                                                <td>Itou</td>
                                                <td>Regional Marketing</td>
                                                <td>Tokyo</td>
                                                <td>$163,000</td>
                                            </tr>
                                            <tr>
                                                <td>Michelle</td>
                                                <td>House</td>
                                                <td>Integration Specialist</td>
                                                <td>Sidney</td>
                                                <td>$95,400</td>
                                            </tr>
                                            <tr>
                                                <td>Suki</td>
                                                <td>Burks</td>
                                                <td>Developer</td>
                                                <td>London</td>
                                                <td>$114,500</td>
                                            </tr>
                                            <tr>
                                                <td>Prescott</td>
                                                <td>Bartlett</td>
                                                <td>Technical Author</td>
                                                <td>London</td>
                                                <td>$145,000</td>
                                            </tr>
                                            <tr>
                                                <td>Gavin</td>
                                                <td>Cortez</td>
                                                <td>Team Leader</td>
                                                <td>San Francisco</td>
                                                <td>$235,500</td>
                                            </tr>
                                            <tr>
                                                <td>Martena</td>
                                                <td>Mccray</td>
                                                <td>Post-Sales support</td>
                                                <td>Edinburgh</td>
                                                <td>$324,050</td>
                                            </tr>
                                            <tr>
                                                <td>Unity</td>
                                                <td>Butler</td>
                                                <td>Marketing Designer</td>
                                                <td>San Francisco</td>
                                                <td>$85,675</td>
                                            </tr>
                                            <tr>
                                                <td>Howard</td>
                                                <td>Hatfield</td>
                                                <td>Office Manager</td>
                                                <td>San Francisco</td>
                                                <td>$164,500</td>
                                            </tr>
                                            <tr>
                                                <td>Hope</td>
                                                <td>Fuentes</td>
                                                <td>Secretary</td>
                                                <td>San Francisco</td>
                                                <td>$109,850</td>
                                            </tr>
                                            <tr>
                                                <td>Vivian</td>
                                                <td>Harrell</td>
                                                <td>Financial Controller</td>
                                                <td>San Francisco</td>
                                                <td>$452,500</td>
                                            </tr>
                                            <tr>
                                                <td>Timothy</td>
                                                <td>Mooney</td>
                                                <td>Office Manager</td>
                                                <td>London</td>
                                                <td>$136,200</td>
                                            </tr>
                                            <tr>
                                                <td>Jackson</td>
                                                <td>Bradshaw</td>
                                                <td>Director</td>
                                                <td>New York</td>
                                                <td>$645,750</td>
                                            </tr>
                                            <tr>
                                                <td>Olivia</td>
                                                <td>Liang</td>
                                                <td>Support Engineer</td>
                                                <td>Singapore</td>
                                                <td>$234,500</td>
                                            </tr>
                                            <tr>
                                                <td>Bruno</td>
                                                <td>Nash</td>
                                                <td>Software Engineer</td>
                                                <td>London</td>
                                                <td>$163,500</td>
                                            </tr>
                                            <tr>
                                                <td>Sakura</td>
                                                <td>Yamamoto</td>
                                                <td>Support Engineer</td>
                                                <td>Tokyo</td>
                                                <td>$139,575</td>
                                            </tr>
                                            <tr>
                                                <td>Thor</td>
                                                <td>Walton</td>
                                                <td>Developer</td>
                                                <td>New York</td>
                                                <td>$98,540</td>
                                            </tr>
                                            <tr>
                                                <td>Finn</td>
                                                <td>Camacho</td>
                                                <td>Support Engineer</td>
                                                <td>San Francisco</td>
                                                <td>$87,500</td>
                                            </tr>
                                            <tr>
                                                <td>Serge</td>
                                                <td>Baldwin</td>
                                                <td>Data Coordinator</td>
                                                <td>Singapore</td>
                                                <td>$138,575</td>
                                            </tr>
                                            <tr>
                                                <td>Zenaida</td>
                                                <td>Frank</td>
                                                <td>Software Engineer</td>
                                                <td>New York</td>
                                                <td>$125,250</td>
                                            </tr>
                                            <tr>
                                                <td>Zorita</td>
                                                <td>Serrano</td>
                                                <td>Software Engineer</td>
                                                <td>San Francisco</td>
                                                <td>$115,000</td>
                                            </tr>
                                            <tr>
                                                <td>Jennifer</td>
                                                <td>Acosta</td>
                                                <td>Junior Javascript Developer</td>
                                                <td>Edinburgh</td>
                                                <td>$75,650</td>
                                            </tr>
                                            <tr>
                                                <td>Cara</td>
                                                <td>Stevens</td>
                                                <td>Sales Assistant</td>
                                                <td>New York</td>
                                                <td>$145,600</td>
                                            </tr>
                                            <tr>
                                                <td>Hermione</td>
                                                <td>Butler</td>
                                                <td>Regional Director</td>
                                                <td>London</td>
                                                <td>$356,250</td>
                                            </tr>
                                            <tr>
                                                <td>Lael</td>
                                                <td>Greer</td>
                                                <td>Systems Administrator</td>
                                                <td>London</td>
                                                <td>$103,500</td>
                                            </tr>
                                            <tr>
                                                <td>Jonas</td>
                                                <td>Alexander</td>
                                                <td>Developer</td>
                                                <td>San Francisco</td>
                                                <td>$86,500</td>
                                            </tr>
                                            <tr>
                                                <td>Shad</td>
                                                <td>Decker</td>
                                                <td>Regional Director</td>
                                                <td>Edinburgh</td>
                                                <td>$183,000</td>
                                            </tr>
                                            <tr>
                                                <td>Michael</td>
                                                <td>Bruce</td>
                                                <td>Javascript Developer</td>
                                                <td>Singapore</td>
                                                <td>$183,000</td>
                                            </tr>
                                            <tr>
                                                <td>Donna</td>
                                                <td>Snider</td>
                                                <td>Customer Support</td>
                                                <td>New York</td>
                                                <td>$112,000</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="card">            
                            <div class="card-body">
                                <h4 class="card-title">Silahkan Pilih Layanan</h4>
                                <div class="row m-t-40">
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <a href="{{route('home')}}"><div class="card card-hover no-card-border">
                                            <div class="box bg-light-success text-center card-body">
                                                <h1 class="m-b-0" style="color:black !important;">999%</h1>
                                                <h6 class="text-muted">(999 dari 999)</h6>
                                                <div class="btn bg-success text-white text-center"><b>Anak Pendidikan Dasar</b></div>
                                            </div>
                                        </div></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div> -->
            </div>
            @yield('footer')
        </div>
    </div>
    <script src="{{asset('adminbite-10/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('adminbite-10/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- apps -->
    <script src="{{asset('adminbite-10/dist/js/app.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/app.init.light-sidebar.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/app-style-switcher.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('adminbite-10/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset('adminbite-10/dist/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('adminbite-10/dist/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('adminbite-10/dist/js/custom.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/extra-libs/jqbootstrapvalidation/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }(window, document, jQuery);
    </script>
    <script>

    </script>
    <script>
    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();
    $("#recoverform").fadeOut();
    $("#rra").fadeOut();
    $("#salahnik").fadeOut();
    // ============================================================== 
    // Login and Recover Password 
    // ============================================================== 
    $('#to-login').on("click", function() {
        $("#salahnik").slideUp();
        $("#loginform").slideDown();
    });
    $('#to-login2').on("click", function() {
        $("#recoverform").slideUp();
        $("#loginform").slideDown();
    });
    $('#to-recover').on("click", function() {
        var nik = $('#nik').val();
        $.ajax({
        url: "/nik_exists/"+nik,
        type: "GET",
        success: function(data){
            if(data){
                // document.getElementById("mynik").innerHTML = data['nik']; 
                // document.getElementById("mynama").innerHTML = data['nama']; 
                // document.getElementById("mynik1").innerHTML = data['nik']; 
                // document.getElementById("mynama1").innerHTML = data['nama']; 
                // if(data['jenis_kelamin'] == "L")
                // {
                //     document.getElementById("myjk").innerHTML = "Laki-Laki";
                // }
                // else
                // {
                //     document.getElementById("myjk").innerHTML = "Perempuan";
                // }
                 
                // document.getElementById("myagama").innerHTML = data['agama']; 
                // document.getElementById("mygd").innerHTML = data['gol_darah']; 
                // document.getElementById("mykwn").innerHTML = data['kewarganegaraan']; 
                // document.getElementById("myal").innerHTML = data['alamat']; 
                // document.getElementById("myal").innerHTML += ", "+data['kelurahan']; 
                // document.getElementById("myal").innerHTML += ", "+data['kecamatan']; 
                // document.getElementById("myal").innerHTML += ", "+data['kota']; 
                // document.getElementById("myal").innerHTML += ", "+data['provinsi']; 
                document.getElementById("namawarga").innerHTML = data['nama'];
                document.getElementById("nikwarga").innerHTML = data['nik'];
                $("#loginform").slideUp();
                $("#recoverform").slideDown();

            }
            else{
                $("#loginform").slideUp();
                $("#salahnik").slideDown();
            }
        }
        });
    });
    </script>
    <script src="{{asset('adminbite-10/assets/libs/toastr/build/toastr.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/extra-libs/toastr/toastr-init.js')}}"></script>
    @yield('custom-bg')
    <script src="{{asset('adminbite-10/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
</body>
</html>