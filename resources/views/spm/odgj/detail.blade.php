<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')@yield('head-ripel01')
<title>Detail ODGJ</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Penderita ODGJ</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Penderita ODGJ</li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Penderita ODGJ</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="row">
                                <div class="card-body p-b-0 m-l-10">
                                    <h4 class="card-title">Riwayat Pelayanan </h4>
                                    <div class="d-flex no-block align-items-center m-b-10">
                                        <div class="m-r-10">
                                            <img src="{{asset('adminbite-10/assets/images/users/user.png')}}" alt="user" class="rounded-circle" width="45">
                                        </div>
                                        <div class="">
                                            <h5 class="m-b-0 font-16 font-medium">{{ $data_nik['nama']}}</h5>
                                            <span>{{$data_nik['nik']}}</span>
                                            <!-- <form method="POST" id="tambahpel-form" action="{{url('/mulai-catat-usia-lanjut')}}" style="display: none;">{{ csrf_field() }}
                                                <input type="text" maxlength="16" name="nik" value="">
                                            </form> -->
                                            <h5 class="card-title m-b-0">
                                                {{ date('d M Y',strtotime($data_nik['tgl_lahir']))}}
                                                <span class="btn waves-effect waves-light btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mulai lahir sejak {{ date('d M Y',strtotime($data_nik['tgl_lahir']))}}">
                                                    Mulai Lahir
                                                </span>
                                            </h5>
                                        </div> 
                                    </div>
                                </div>
                                <div class="card-body col-lg-3 col-md-12">
                                    <h4 class="card-title m-b-0">
                                        Capaian SPM
                                        <span class="btn bg-info text-white btn-outline" data-toggle="tooltip" data-placement="top" title="" data-original-title="">
                                            {{ number_format(0,2)}}%
                                        </span>
                                    </h4>
                                    <br>       
                                    <div class="d-flex flex-row text-center">
                                        <div class="btn-group m-r-10">
                                            <a href="{{url('/catat-odgj')}}"><button type="button" class="btn btn-info">
                                                <i class="mdi mdi-book-open-page-variant"></i> Tambah Pelayanan
                                            </button></a>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="row">
                                <div class="card-body p-b-0 p-t-0">
                                    <div class="d-md-flex align-items-center">
                                        <div>    
                                        </div>
                                        <div class="ml-auto">
                                            <div class="dl">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="zero_config" class="table table-striped table-bordered display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Tanggal Pelayanan</th>
                                                        <th>Pelayanan pada Usia ke-</th>
                                                        <th>Fasilitas Kesehatan</th>
                                                        <th>Tenaga Kesehatan</th>
                                                        <th>Capaian</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $no=1; @endphp
                                                    @foreach($pelayanan_all as $i => $pel)
                                                    <tr> 
                                                    <td>{{ $no++}}</td>
                                                    <td>{{ date('d M Y',strtotime($pel['tanggal_pelayanan']))}}</td>
                                                    <td>
                                                        @if($usiapel[$i]['years'] ==0)
                                                            @if($usiapel[$i]['months'] == 0)
                                                                {{ $usiapel[$i]['days'] }} Hari
                                                                <br> <small class="text-muted">{{ $usiapel[$i]['hours'] }} Jam</small> 
                                                            @else
                                                                {{ $usiapel[$i]['months'] }} Bulan <br> <small class="text-muted">{{ $usiapel[$i]['days'] }} Hari</small> 
                                                            @endif
                                                        @else
                                                            {{ $usiapel[$i]['years'] }} Tahun <br> <small class="text-muted"> {{ $usiapel[$i]['months'] }} Bulan {{ $usiapel[$i]['days'] }} Hari</small> 
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(explode('_',$pel['lokasi'])[0] != null ) {{ explode('_',$pel['lokasi'])[0] }} @else - @endif <br>
                                                        <small class="text-muted"> @if(explode('_',$pel['lokasi'])[1] != null ) {{ explode('_',$pel['lokasi'])[1] }} @else - @endif </small>
                                                    </td>
                                                    <td>
                                                        @if(explode('_',$pel['tenaga_kerja'])[0] != null ) {{ explode('_',$pel['tenaga_kerja'])[0] }} @else - @endif <br>
                                                        <small class="text-muted"> @if(explode('_',$pel['tenaga_kerja'])[1] != null ) {{ explode('_',$pel['tenaga_kerja'])[1] }} @else - @endif </small>
                                                    </td>
                                                    <td><span class="label label-info">{{ number_format(0,2)}}%</span></td>
                                                    <td>                                        
                                                        <a href="{{ url('/detail-pelayanan-odgj/'.$pel['id']) }}">
                                                            <button type="submit" class="btn btn-info btn-xs">
                                                                Detail
                                                            </button>
                                                        </a>
                                                        
                                                        <a href="{{ url('/ubah-pelayanan-odgj/'.$pel['id']) }}">
                                                            <button type="submit" class="btn btn-warning btn-xs">
                                                                Ubah
                                                            </button>
                                                        </a>
                                                    </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>