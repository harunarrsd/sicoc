<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="GIK">
<meta name="author" content="GIK">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('adminbite-10/assets/images/sicoc-favicon.png')}}">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/select2/dist/css/select2.min.css')}}">
<link href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/morris.js/morris.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/toastr/build/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet">
<style>
    .sidebar-item a{
        font-weight:600;
    }
    .tx-c{
        text-align:center;
    }
    .bg-y{
        background:yellow;
    }
    .sidebar-link .icon-Record{
        visibility: visible !important;
    }
</style>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.date.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.time.css')}}">
    <link type="text/css" href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
            <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}">
<title>Catatan Pelayanan Ibu Hamil</title>
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Ibu Hamil</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Ibu Hamil</li>
                                    <li class="breadcrumb-item active" aria-current="page">Catat Pelayanan</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="card" id="step3-add">
                    <div class="row">
                        <div class="col-lg-12 col-xlg-12 col-md-12">
                            <div class="card-body p-b-0 m-l-10">
                                <h4 class="card-title">Catat Pelayanan Ibu Hamil </h4>
                                <h6 class="card-subtitle">Pencatatan 10T Pelayanan Ibu Hamil sesuai SPM</h6>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-xlg-6 col-md-12">
                            <div class="card-body p-b-0 m-l-10">
                                <div class="row">
                                    <div class="d-flex no-block align-items-center m-b-10 col-lg-8 col-xlg-8 col-md-12">
                                        <div class="">
                                            <h5 class="m-b-0 font-16 font-medium">ak</h5>
                                            <span>1324</span>
                                            <h5 class="card-title m-b-0">
                                                {{ date('d M Y', strtotime(1999-03-07)) }}
                                                <span class="btn waves-effect waves-light btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mulai hamil sejak {{ date('d M Y', strtotime(1999-03-07)) }}">
                                                    Mulai Hamil
                                                </span>
                                            </h5> 
                                        </div> 
                                    </div>
                                    <div class="card-title col-lg-4 col-xlg-4 col-md-12">
                                        
                                            <h5>Capaian SPM</h5>
                                            <span class="btn bg-info text-white btn-outline" data-toggle="tooltip" data-placement="top" title="">
                                                {{ number_format(0,2) }}%
                                            </span>
                                                                                       
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <form method="POST" action="{{url('/')}}">{{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tanggal Pelayanan</label> <span class="text-danger">*</span>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                        </div>
                                        <input type="date" class="form-control pickadate-disable" name="tanggal_pelayanan" required/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Fasilitas Kesahatan</label> <span class="text-danger">*</span>    
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="fasilitas_kesehatan" id="selectfas" required="" aria-invalid="true" class="form-control select2 custom-select" style="width: 82%; height:36px;" onchange="getMitra()">
                                                <option value="">Pilih</option>
                                                <option value="Rumah Sakit">Rumah Sakit</option>
                                                <option value="Puskesmas">Puskesmas</option>
                                                <option value="Klinik">Kilnik</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                        <label>Nama Fasilitas Pelayanan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-map-alt"></i></span>
                                            </div>
                                            <select name="selectlok" id="selectlok" aria-invalid="true" class="form-control select2 custom-select" style="width: 82%; height:36px;" onchange="getTenaga()">
                                                <option value="">Pilih</option> 
                                                 
                                            </select>
                                            <input type="text" name="lokasi_pelayanan" id="lokasi_pelayanan" class="form-control" style="display:none;" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                        <label>Tenaga Kesehatan</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="tenaga_kesehatan" id="selecttenaga" required="" class="form-control select2 custom-select" aria-invalid="true" style="width: 82%; height:36px;" onchange="getNamaStk()">
                                                <option value="">Pilih</option>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                        <label>Nama Dokter/Bidan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="selectnamaSTK" id="selectnamaSTK" aria-invalid="true" class="form-control select2 custom-select" style="width: 82%; height:36px;" onchange="getNamaStk2()">
                                                <option value="">Pilih</option>                                                    
                                            </select>
                                            <input type="text" id="nama_stk" name="nama_stk" class="form-control" style="display:none;">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                        <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                            <label>Berat Badan</label> 
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                </div>
                                                <input type="number" name="berat" class="form-control" placeholder="Co : 76" min="10" max="200">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">kg</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                            <label>Tinggi Badan</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                </div>
                                                <input type="number" name="tinggi" class="form-control" placeholder="Co : 165" min="50" max="200">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">cm</span>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label for="exampleInputEmail1">Tekanan Darah</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="number" name="tekanan_darah1" class="form-control" placeholder="Co : 120">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">/</span>
                                            </div>
                                            <input type="number" name="tekanan_darah2" class="form-control" placeholder="Co : 80">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">mmHG</span>
                                            </div>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label>Nilai Status Gizi (Lingkar Lengan Atas/LiLA)</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="number" name="lila1" class="form-control" placeholder="Co : 24">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">,</span>
                                            </div>
                                            <input type="number" name="lila2" class="form-control" placeholder="00" min="0" max="99">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">cm</span>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <input type="text" name="tanggal_hamil" value="" style="display:none;">
                        <input type="text" name="id" value="" style="display:none;">
                        <div class="col-lg-6 col-xlg-6 col-md-12">
                            <div class="card-body">
                                    <div class="form-group">
                                        <label>Tinggi Puncak Rahim (Fundus Uteril)</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="number" name="tprahim1" class="form-control" placeholder="Co : 36">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">,</span>
                                            </div>
                                            <input type="number" name="tprahim2" class="form-control" placeholder="00" min="0" max="99">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">cm</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                            <label>Presentasi Janin</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                </div>
                                                <input type="text" name="presensijanin" class="form-control" placeholder="Letak Posisi Janin">
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                            <label>Denyut Jantung Janin (DJJ)</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                </div>
                                                <input type="number" name="djj" class="form-control" placeholder="Co : 120">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">x/menit</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label> Imunisasi </label>
                                        <div class="input-group mb-3">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1" name="skrining_imunisasi">
                                                <label class="custom-control-label" for="customCheck1">Skrining Status Imunisasi Tetanus </label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck2" name="imunisasi_tt">
                                                <label class="custom-control-label" for="customCheck2">Imunisasi Tetanus Toksoid (TT)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Jumlah Tablet Tambah Darah</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon22"><i class="ti-package"></i></span>
                                            </div>
                                            <input type="number" class="form-control" placeholder="Co : 12" name="tablet_tambah_darah">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Tablet</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label> Tes Laboratorium </label>
                                        <div class="input-group mb-3">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck3" name="tes_kehamilan">
                                                <label class="custom-control-label p-r-5" for="customCheck3">Tes Kehamilan</label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck4" name="tes_hb">
                                                <label class="custom-control-label" for="customCheck4">Pemeriksaan Hemoglobin Darah (Hb)</label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck5" name="tes_gd">
                                                <label class="custom-control-label p-r-5" for="customCheck5">Pemeriksaan Golongan Darah</label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck6" name="tes_protein_urin">
                                                <label class="custom-control-label" for="customCheck6">Pemeriksaan Protein Urin</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label> Penanganan dan Konseling </label>
                                        <div class="input-group mb-3">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck7" name="tatalaksana">
                                                <label class="custom-control-label" for="customCheck7">Tatalaksana/penanganan kasus sesuai kewenangan </label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck8" name="temu_wicara">
                                                <label class="custom-control-label" for="customCheck8">Temu Wicara (Konseling)</label>
                                            </div>
                                        </div>
                                        <a class="btn btn-success" href="{{url('/catat-ibu-hamil')}}">< Kembali</a>
                                        <button type="submit" class="btn btn-info m-r-10" id="step3-add-to-send-data">Kirimkan ></button> 
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    
    <script>
        function getMitra(){
            var fas = document.getElementById("selectfas").value;
            $('#selectlok').find('option').not(':first').remove();
            $('#selecttenaga').find('option').not(':first').remove();
            $('#selectnamaSTK').find('option').not(':first').remove();
            $.ajax({
                type:'GET',
                url:'/findmitra',
                data:{id:fas},
                dataType:'json',
                success:function(data){
                    var len = 0;
                    if(data['data'] != null){
                        len = data['data'].length;
                    }
                    for(var i=0;i<len;i++){
                        var id = data['data'][i]['id'];
                        var jenis = data['data'][i]['jenis_mitra'];
                        var nama = data['data'][i]['nama'];

                        var option = "<option value='"+id+"_"+jenis+" "+nama+"'>"+jenis+" "+nama+"</option>";
                        $("#selectlok").append(option);
                    }
                    
                }
            });
        }
        function getTenaga(){
            var lok = document.getElementById("selectlok").value;
            var lok_ok = lok.split("_");
            $('#selectnamaSTK').find('option').not(':first').remove();
            $('#selecttenaga').find('option').not(':first').remove();
            $.ajax({
                type:'GET',
                url:'/findtenaga',
                data:{id:lok_ok[0]},
                dataType:'json',
                success:function(data){
                    var tenaga = data['data'].split("_");
                    for(var i=0;i<tenaga.length;i++){
                        var option = "<option value='"+tenaga[i]+"'>"+tenaga[i]+"</option>";
                        $("#selecttenaga").append(option);
                    }
                }
            });
            document.getElementById("lokasi_pelayanan").value = lok_ok[1];
        }
        function getNamaStk(){
            var tenaga = document.getElementById("selecttenaga").value;
            var lok = document.getElementById("selectlok").value;
            var lok_ok = lok.split("_");
            $('#selectnamaSTK').find('option').not(':first').remove();
            $.ajax({
                type:'GET',
                url:'/findnamastk',
                data:{tenaga:tenaga,mitra:lok_ok[0]},
                dataType:'json',
                success:function(data){
                    var nama = data['data'].split("_");
                    for(var i=0;i<nama.length;i++){
                        var option = "<option value='"+nama[i]+"'>"+nama[i]+"</option>";
                        $("#selectnamaSTK").append(option);
                    }
                }
            });
        }
        function getNamaStk2(){
            var selectnamaSTK = document.getElementById("selectnamaSTK").value;
            document.getElementById("nama_stk").value = selectnamaSTK;
        }
    </script>
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/select2/select2.init.js')}}"></script>
    <!-- Picker Date Style -->
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.date.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.time.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/legacy.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <!-- <script src="{{asset('adminbite-10/assets/libs/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/datetimepicker/datetimepicker.init.js')}}"></script> -->
    
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js')}}"></script>
    <script>
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    $('#date-fr').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', lang: 'fr', weekStart: 1, cancelText: 'ANNULER' });
    $('#date-end').bootstrapMaterialDatePicker({ weekStart: 0 });
    $('#date-start').bootstrapMaterialDatePicker({ weekStart: 0 }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });
    </script>
    <script type="text/javascript">
        $('.pickadate-disable').pickadate({
            disable: [
            ]
        });
    </script>
</body>
</html>