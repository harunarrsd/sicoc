<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="GIK">
<meta name="author" content="GIK">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('adminbite-10/assets/images/sicoc-favicon.png')}}">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/select2/dist/css/select2.min.css')}}">
<link href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/morris.js/morris.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/toastr/build/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet">
<style>
    .sidebar-item a{
        font-weight:600;
    }
    .tx-c{
        text-align:center;
    }
    .bg-y{
        background:yellow;
    }
    .sidebar-link .icon-Record{
        visibility: visible !important;
    }
</style>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.date.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.time.css')}}">
    <link type="text/css" href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
            <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}">
<title>Perubahan Data ODGJ</title>
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan ODGJ</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Orang dengan Gangguan Jiwa</li>
                                    <li class="breadcrumb-item active" aria-current="page">Ubah ODGJ</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
               <div class="col-12">   
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ session()->get('success')}}
                        </div>
                    @endif
                    @if(session()->has('danger'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ session()->get('danger')}}
                        </div>
                    @endif 
                    <div class="card">
                        <div class="card-body p-b-0">
                            <div class="d-md-flex align-items-center">
                                <div>
                                    <h4 class="card-title">Perubahan Penderita ODGJ</h4>
                                    <h6 class="card-subtitle">Perubahan Data Penderita ODGJ pada SICOC</h6>
                                </div>
                                <div class="ml-auto">
                                    <div class="dl">
                                        <div class="input-group mb-3">
                                            <a href="{{url('/detail-penderita-odgj/'.$data['id'])}}">
                                                <button type="button" class="btn btn-success m-r-10">
                                                    Capaian {{ $nama }} : {{ number_format(0,2) }}% -- Cek Detail Capaian SPM >
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                            
                        <div class="row">
                            <div class="card-body col-md-12 col-lg-6">
                                <form method="POST" action="{{url('/edit-penderita-odgj')}}">{{ csrf_field() }}
                                    <div class="form-group m-0 col-lg-12 col-xlg-6 col-md-12">
                                        <label>Nomor Induk Kependudukan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="select" id="select" required="" class="form-control select2 custom-select" aria-invalid="true" style="width: 90%; height:36px;" onchange="myFunction()">
                                                <option value="{{ $gab }}">{{ $nik }} -- {{ $nama }}</option>
                                                @foreach($nikca as $i => $item)                    
                                                    <option value="{{ $gabungan[$i] }}">{{$item}} -- {{ $kriteria[$i] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 col-xlg-6 col-md-12 m-0">
                                        <label>Tanggal Mulai Menderita ODGJ</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                            </div>
                                            <input type="date" name="tanggal_menderita" class="form-control" value="{{ explode(' ',$data['tanggal_menderita'])[0] }}">
                                            <input type="text" name="id" class="form-control" value="{{ $data['id'] }}" style="display:none;">
                                            <input type="text" id="nik" name="nik" class="form-control" value="{{ explode('_',$gab)[0] }}" style="display:none;">
                                            <input type="text" name="totalcapaian" class="form-control" value="{{ $data['totalcapaian'] }}" style="display:none;">
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-12 col-xlg-6 col-md-12">
                                        <label>Status Kehamilan </label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="status" id="select" class="form-control" aria-invalid="true" >
                                                <option value="{{ $data['status'] }}">{{ $data['status'] }}</option>
                                                <option value="Sudah">Selesai Tahap Pelayanan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 col-xlg-6 col-md-12 m-0">   
                                    </div>
                            </div>
                            <div class="card-body col-md-12 col-lg-6">
                                <div class="form-group col-lg-12 col-xlg-6 col-md-12 m-0">
                                    <label>Nama Penderita ODGJ</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                        </div>
                                        <input type="text" id="nama_odgj" class="form-control" disabled value="{{ explode('_',$gab)[1] }}">
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 col-xlg-6 col-md-12 m-0">
                                    <label>Tanggal Lahir</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                        </div>
                                        <input type="date" disabled id="tanggal_lahir" class="form-control" value="{{ explode(' ',explode('_',$gab)[2])[0] }}">
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 col-xlg-6 col-md-12 m-0">
                                    <label>Status Perkawinan</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                        </div>
                                        <input type="text" disabled id="status_kawin" class="form-control" value="{{ explode('_',$gab)[3] }}">
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 col-xlg-6 col-md-12 m-0">
                                    <div class="form-group p-0 col-lg-4 col-md-6 col-sm-12">
                                    <button type="submit" class="btn btn-info m-r-10">Kirimkan Perubahan ></button> 
                                </div>                
                            </div>
                            
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <script>
        function myFunction() {
        var x = document.getElementById("select").value;

        var element = x.split("_");
        document.getElementById("nik").value = element[0];
        document.getElementById("nama_odgj").value = element[1];
        document.getElementById("tanggal_lahir").value = element[2].split(" ")[0];
        document.getElementById("status_kawin").value = element[3];
        }
    </script>
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/select2/select2.init.js')}}"></script>
    <!-- Picker Date Style -->
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.date.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.time.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/legacy.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <!-- <script src="{{asset('adminbite-10/assets/libs/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/datetimepicker/datetimepicker.init.js')}}"></script> -->
    
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js')}}"></script>
    <script>
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    $('#date-fr').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', lang: 'fr', weekStart: 1, cancelText: 'ANNULER' });
    $('#date-end').bootstrapMaterialDatePicker({ weekStart: 0 });
    $('#date-start').bootstrapMaterialDatePicker({ weekStart: 0 }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });
    </script>
    <script type="text/javascript">
        $('.pickadate-disable').pickadate({
            disable: [

            ]
        });
    </script>
</body>
</html>