<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>SPM Penderita  Tuberkulosis</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Tuberkulosis</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active" aria-current="page">Standar Pelayanan Minimal</li>                                
                                    <li class="breadcrumb-item active" aria-current="page">Tuberkulosis</li>
                                    <li class="breadcrumb-item active" aria-current="page">SPM Penderita Tuberkulosis</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
                <div class="card-body bg-light">
                    <div class="d-md-flex align-items-center">
                        <div><h2>Pelayanan ODGJ</h2></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Penyataan Standar</h4>
                            Setiap orang dengan TB mendapatkan pelayanan TB sesuai standar.
                            Pemerintah Kabupaten/Kota mempunyai kewajiban untuk
                            memberikan pelayanan kesehatan sesuai standar kepada seluruh
                            orang dengan TB sebagai upaya pencegahan di wilayah kerjanya.
 
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Pelayanan Tuberkulosis Sesuai Standar </h4>
                            <p> Pelayanan Tuberkulosis Sesuai Standar adalah pelayanan
                                kesehatan diberikan kepada seluruh orang dengan TB yang
                                dilakukan oleh tenaga kesehatan sesuai kewenangannya di FKTP
                                (puskesmas dan jaringannya) dan di FKTL baik pemerintah
                                maupun swasta</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Pelayanan yang diberikan sesuai Pedoman Penanggulangan TB</h4>
                            <p> 1) Penegakan diagnosis TB dilakukan secara bakteriologis dan
                                klinis serta dapat didukung dengan pemeriksaan penunjang
                                lainnya.<br>
                                2) Dilakukan pemeriksaan pemantauan kemajuan pengobatan
                                pada akhir pengobatan intensif, bulan ke 5 dan akhir
                                pengobatan.<br>
                                3) Pengobatan dengan menggunakan Obat Anti Tuberkulosis
                                (OAT) dengan panduan OAT standar. <br>   
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Definisi Operasional Capaian Kerja</h4>
                            Capaian kinerja Pemerintah Daerah Kabupaten/Kota dalam
                            memberikan pelayanan orang dengan TB dinilai dari persentase
                            jumlah orang yang mendapatkan pelayanan TB sesuai standar di
                            wilayah kerjanya dalam kurun waktu satu tahun.
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4><i class="fas fa-chevron-circle-right m-r-10 m-b-10"></i> Referensi</h4>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 75 Tahun 2014 tentang Pusat Kesehatan Masyarakat;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 5 Tahun 2014 tentang Panduan Praktik Klinis;</h6>
                    <h6><i class="fas fa-square"></i> Pedoman Pencegahan dan Pengendalian Pemasungan Orang Dengan Gangguan Jiwa (ODGJ);</h6>
                    <h6><i class="fas fa-square"></i> Keputusan Menteri Kesehatan Nomor 279/Menkes/SK/IV/2006 tentang Pedoman Penyelenggaraan Upaya Keperawatan Kesehatan Masyarakat di Puskesmas;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor HK 02.02/Menkes/148/I/2010 tentang Ijin dan Penyelenggaraan Praktik Keperawatan;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 17 Tahun 2013 tentang Perubahan Atas Peraturan Menteri Kesehatan Nomor HK 02.02/Menkes/148/I/2010 tentang Praktik Keperawatan;</h6>
                    <h6><i class="fas fa-square"></i> Buku Keperawatan Jiwa Masyarakat (Community Mental Health Nursing).</h6>

                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>