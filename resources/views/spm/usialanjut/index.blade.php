<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>SPM Usia Lanjut</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Usia Lanjut</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active" aria-current="page">Standar Pelayanan Minimal</li>                                
                                    <li class="breadcrumb-item active" aria-current="page">Usia Lanjut</li>
                                    <li class="breadcrumb-item active" aria-current="page">SPM Usia Lanjut</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
                <div class="card-body bg-light">
                    <div class="d-md-flex align-items-center">
                        <div><h2>Pelayanan Usia Lanjut</h2></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Penyataan Standar</h4>
                            Setiap warga negara Indonesia usia 60 tahun ke atas mendapatkan
                            skrining kesehatan sesuai standar.
                            Pemerintah Daerah Kabupaten/Kota wajib memberikan skrining
                            kesehatan sesuai standar pada warga negara usia 60 tahun ke atas
                            di wilayah kerjanya minimal 1 kali dalam kurun waktu satu tahun
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Pelayanan skrining kesehatan</h4>
                            Pelayanan skrining kesehatan warga negara usia 60 tahun ke
                            atas sesuai standar adalah :
                            a) Dilakukan sesuai kewenangan oleh :
                            (1) Dokter;
                            (2) Bidan;
                            (3) Perawat;
                            (4) Nutrisionis/Tenaga Gizi;
                            (5) Kader Posyandu lansia/Posbindu
                            b) Pelayanan skrining kesehatan diberikan di Puskesmas dan
                            jaringannya, fasilitas pelayanan kesehatan lainnya,
                            maupun pada kelompok lansia, bekerja sama dengan
                            pemerintah daerah.
                            c) Pelayanan skrining kesehatan minimal dilakukan sekali
                            setahun.
                            d) Lingkup skrining adalah sebagai berikut :
                            (1) Deteksi hipertensi dengan mengukur tekanan darah.
                            (2) Deteksi diabetes melitus dengan pemeriksaan kadar
                            gula darah.
                            (3) Deteksi kadar kolesterol dalam darah
                            (4) Deteksi gangguan mental emosional dan perilaku,
                            termasuk kepikunan menggunakan Mini Cog atau
                            Mini Mental Status Examination (MMSE)/Test Mental
                            Mini atau Abreviated Mental Test (AMT) dan Geriatric
                            Depression Scale (GDS).
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Pengunjung berisiko</h4>
                            <p>Pengunjung yang ditemukan memiliki faktor risiko wajib
                                dilakukan intervensi secara dini.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Pengunjung penderita penyakit</h4>
                            Pengunjung yang ditemukan menderita penyakit wajib ditangani
                            atau dirujuk ke fasilitas pelayanan kesehatan yang mampu
                            menanganinya.
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Definisi Operasional Capaian Kinerja</h4>
                            Capaian kinerja Pemerintah Daerah Kabupaten/Kota dalam
                            memberikan skrining kesehatan pada warga negara usia 60 tahun
                            keatas dinilai dari persentase pengunjung berusia 60 tahun keatas
                            yang mendapatkan skrining kesehatan sesuai standar minimal 1 kali
                            di wilayah kerjanya dalam kurun waktu satu tahun.
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4><i class="fas fa-chevron-circle-right m-r-10 m-b-10"></i> Referensi</h4>
                    <h6><i class="fas fa-square"></i> Keputusan Menteri Kesehatan Nomor 854 Tahun 2009 tentang Pedoman Pengendalian Penyakit Jantung dan Pembuluh Darah;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 79 Tahun 2014 tentang Penyelenggaraan Pelayanan Geriatri di Rumah Sakit;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 67 Tahun 2015 tentang Penyelenggaraan Pelayanan Kesehatan Lanjut Usia di Puskesmas;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 71 Tahun 2015 tentang Penanggulangan Penyakit Tidak Menular;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 25 Tahun 2016 tentang Rencana Aksi Nasional Kesehatan Lanjut Usia Tahun 2016 – 2019.</h6>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>