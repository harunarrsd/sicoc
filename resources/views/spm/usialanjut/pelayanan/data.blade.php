<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')@yield('head-ripel01')
<title>Data Pelayanan Usia Lanjut</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Usia Lanjut</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Usia Lanjut</li>
                                    <li class="breadcrumb-item active" aria-current="page">Data Pelayanan Usia Lanjut</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-md-flex align-items-center">
                                    <div>
                                        <h4 class="card-title">Data Pelayanan Usia Lanjut</h4>
                                        <h6 class="card-subtitle">Standar Pelayanan Minimal</h6>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="dl">
                                            <div class="btn-group">
                                                <a href="{{url('/catat-usia-lanjut')}}"><button type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Catat Pelayanan Ibu Hamil">
                                                    <i class="mdi mdi-plus"></i> Tambah Pelayanan Usia Lanjut
                                                </button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive m-t-15">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Kode Pelayanan</th>
                                                <th>Nama </th>
                                                <th>Tanggal Pelayanan</th>
                                                <th>Pelayanan pada Usia ke-</th>
                                                <th>Capaian Pelayanan</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($pelayanan_usia_lanjut as $i => $pel)
                                            <tr>
                                                <td>{{ $pel['id']}}</td>
                                                <td>{{ $datawarga[$i]['nama'] }}</td>
                                                <td>{{ date('d M Y',strtotime($pel['tanggal_pelayanan']))}}</td>
                                                <td>{{ $usiapel[$i]['years']}} Tahun</td>
                                                <td><span class="label label-info">{{ number_format($cap_pel[$i],2)}}%</span>
                                                
                                                </td>
                                                <td>           
                                                    <a href="{{ url('/detail-pelayanan-usia-lanjut/'.$pel['id'])}}">
                                                        <button type="submit" class="btn btn-info btn-xs">
                                                            Detail
                                                        </button>
                                                    </a>
                                                    
                                                    <a href="{{ url('/ubah-pelayanan-usia-lanjut/'.$pel['id'])}}">
                                                        <button type="submit" class="btn btn-warning btn-xs">
                                                            Ubah
                                                        </button>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>