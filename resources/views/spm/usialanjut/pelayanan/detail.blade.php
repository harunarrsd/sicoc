<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>Detail Pelayanan Usia Lanjut</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.date.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.time.css')}}">
    <link type="text/css" href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
            <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Usia Lanjut</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Usia Lanjut</li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Pelayanan</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="col-12 p-0">
                    <div class="card">
                        <div class="card-body wizard-content">
                            <div class="row">
                                <div class="col-lg-7 col-xlg-7 col-md-6 col-sm-12 p-b-0">
                                    <div class="card-body m-0">
                                        <h4 class="card-title">Detail Pelayanan Usia Lanjut </h4>
                                        <h6 class="card-subtitle">Pelayanan Usia Lanjut sesuai SPM</h6>
                                    </div> 
                                </div>
                                <div class="col-lg-3 col-xlg-3 col-md-3 col-sm-12">
                                    <h5 class="m-b-0 font-16 font-medium">{{ $datawarga['nama']}}</h5>
                                    <span>{{ $datawarga['nik'] }}</span>
                                    <h5 class="card-title m-b-0">
                                        {{ date('d M Y',strtotime($datawarga['tgl_lahir']))}}
                                        <span class="btn waves-effect waves-light btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mulai lahir sejak {{ date('d M Y',strtotime($datawarga['tgl_lahir']))}}">
                                            Mulai Lahir
                                        </span>
                                    </h5> 
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2 col-sm-12">
                                    <div class="card-title">    
                                        <h5>Capaian SPM</h5>
                                        <span class="btn bg-info text-white btn-outline" data-toggle="tooltip" data-placement="top" title="" data-original-title="% dan total T dari 40T">
                                            {{ number_format($capaian_fix,2)}}%
                                        </span>                                                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-xlg-6 col-md-12">
                                    <div class="card-body">
                                        <div class="form-group m-0 col-lg-12 col-xlg-12 col-md-12 p-0">
                                            <label>Tanggal Pelayanan</label> <span class="text-danger">*</span>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                                </div>
                                                <input type="date" disable readonly value="{{ explode(' ',$pelayanan['tanggal_pelayanan'])[0] }}" class="form-control pickadate-disable" name="tanggal_pelayanan" required/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label>Fasilitas Kesahatan</label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                        </div>
                                                        <select disable readonly name="fasilitas_kesehatan" id="select" required="" class="form-control" aria-invalid="true">
                                                            @if(explode('_',$pelayanan['lokasi'])[0] == "Puskesmas")
                                                            <option value="">Pilih</option>
                                                            <option value="Puskesmas" selected>Puskesmas</option>
                                                            <option value="Rumah Sakit">Rumah Sakit</option>
                                                            <option value="Klinik">Klinik</option>
                                                            <option value="Kelompok Lansia">Kelompok Lansia Kerjasama Pemerintah</option>
                                                            @elseif(explode('_',$pelayanan['lokasi'])[0] == "Rumah Sakit")
                                                            <option value="">Pilih</option>
                                                            <option value="Puskesmas">Puskesmas</option>
                                                            <option value="Rumah Sakit" selected>Rumah Sakit</option>
                                                            <option value="Klinik">Klinik</option>
                                                            <option value="Kelompok Lansia">Kelompok Lansia Kerjasama Pemerintah</option>
                                                            @elseif(explode('_',$pelayanan['lokasi'])[0] == "Klinik")
                                                            <option value="">Pilih</option>
                                                            <option value="Puskesmas">Puskesmas</option>
                                                            <option value="Rumah Sakit">Rumah Sakit</option>
                                                            <option value="Klinik" selected>Klinik</option>
                                                            <option value="Kelompok Lansia">Kelompok Lansia Kerjasama Pemerintah</option>
                                                            @elseif(explode('_',$pelayanan['lokasi'])[0] == "Kelompok Lansia")
                                                            <option value="">Pilih</option>
                                                            <option value="Puskesmas">Puskesmas</option>
                                                            <option value="Rumah Sakit">Rumah Sakit</option>
                                                            <option value="Klinik">Klinik</option>
                                                            <option value="Kelompok Lansia" selected>Kelompok Lansia Kerjasama Pemerintah</option>            
                                                            @else
                                                            <option value="" selected>Pilih</option>
                                                            <option value="Puskesmas">Puskesmas</option>
                                                            <option value="Rumah Sakit">Rumah Sakit</option>
                                                            <option value="Klinik">Klinik</option>
                                                            <option value="Kelompok Lansia">Kelompok Lansia Kerjasama Pemerintah</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                            </div>
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label>Lokasi Puskesmas</label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon11"><i class="ti-map-alt"></i></span>
                                                        </div>
                                                        <input type="text" disable readonly name="lokasi_pelayanan" value="{{ explode('_',$pelayanan['lokasi'])[1]}}" class="form-control" placeholder="Co :Puskesmas Beji">
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Tenaga Kesehatan</label> <span class="text-danger">*</span>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                    </div>
                                                    <select name="tenaga_kesehatan" disable readonly id="select" required="" class="form-control" aria-invalid="true">
                                                        @if(explode('_',$pelayanan['tenaga_kerja'])[0] == "Dokter")
                                                        <option value="">Pilih</option>
                                                        <option value="Dokter" selected>Dokter</option>
                                                        <option value="Bidan">Bidan</option>
                                                        <option value="Perawat">Perawat</option>
                                                        <option value="Nutrisionis/Tenaga Gizi">Nutrisionis/Tenaga Gizi</option>
                                                        <option value="Kader Posyandu lansia/Posbindu">Kader Posyandu lansia/Posbindu</option>
                                                        @elseif(explode('_',$pelayanan['tenaga_kerja'])[0] == "Bidan")
                                                        <option value="">Pilih</option>
                                                        <option value="Dokter">Dokter</option>
                                                        <option value="Bidan" selected>Bidan</option>
                                                        <option value="Perawat">Perawat</option>
                                                        <option value="Nutrisionis/Tenaga Gizi">Nutrisionis/Tenaga Gizi</option>
                                                        <option value="Kader Posyandu lansia/Posbindu">Kader Posyandu lansia/Posbindu</option>
                                                        @elseif(explode('_',$pelayanan['tenaga_kerja'])[0] == "Perawat")
                                                        <option value="">Pilih</option>
                                                        <option value="Dokter">Dokter</option>
                                                        <option value="Bidan">Bidan</option>
                                                        <option value="Perawat" selected>Perawat</option>
                                                        <option value="Nutrisionis/Tenaga Gizi">Nutrisionis/Tenaga Gizi</option>
                                                        <option value="Kader Posyandu lansia/Posbindu">Kader Posyandu lansia/Posbindu</option>
                                                        @elseif(explode('_',$pelayanan['tenaga_kerja'])[0] == "Nutrisionis/Tenaga Gizi")
                                                        <option value="">Pilih</option>
                                                        <option value="Dokter">Dokter</option>
                                                        <option value="Bidan">Bidan</option>
                                                        <option value="Perawat">Perawat</option>
                                                        <option value="Nutrisionis/Tenaga Gizi" selected>Nutrisionis/Tenaga Gizi</option>
                                                        <option value="Kader Posyandu lansia/Posbindu">Kader Posyandu lansia/Posbindu</option>
                                                        @elseif(explode('_',$pelayanan['tenaga_kerja'])[0] == "Kader Posyandu lansia/Posbindu")
                                                        <option value="">Pilih</option>
                                                        <option value="Dokter">Dokter</option>
                                                        <option value="Bidan">Bidan</option>
                                                        <option value="Perawat">Perawat</option>
                                                        <option value="Nutrisionis/Tenaga Gizi">Nutrisionis/Tenaga Gizi</option>
                                                        <option value="Kader Posyandu lansia/Posbindu" selected>Kader Posyandu lansia/Posbindu</option>
                                                        @else
                                                        <option value="" selected>Pilih</option>
                                                        <option value="Dokter">Dokter</option>
                                                        <option value="Bidan">Bidan</option>
                                                        <option value="Perawat">Perawat</option>
                                                        <option value="Nutrisionis/Tenaga Gizi">Nutrisionis/Tenaga Gizi</option>
                                                        <option value="Kader Posyandu lansia/Posbindu">Kader Posyandu lansia/Posbindu</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Nama Dokter/Bidan</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                    </div>
                                                    <input type="text" name="nama_stk" disable readonly value="{{ explode('_',$pelayanan['tenaga_kerja'])[1] }}" class="form-control" placeholder="Co : dr. Ahmad Riza">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-6 col-xlg-6 col-md-12">
                                    <div class="card-body">            
                                        <label>Deteksi Hipertensi dan Diabetes Melitus</label> 
                                        <div class="row">
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Perhitungan Tekanan Darah</label> 
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                    </div>
                                                    <input type="number" name="tekanan_darah" disable readonly value="{{ $pelayanan['tekanan_darah']}}" class="form-control" placeholder="Co : 76" min="0">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">x/menit</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Perhitungan Kadar Gula Darah</label> 
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                    </div>
                                                    <input type="number" name="gula_darah" disable readonly value="{{ $pelayanan['tekanan_darah']}}" class="form-control" placeholder="Co : 76" min="0">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">mg/dL</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <label>Deteksi mental emosional dan  Kolesterol </label> 
                                        <div class="row">
                                            <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                                <label>Tes Gangguan Emosional</label> 
                                                <div class="input-group mb-3 bt-switch">
                                                @if($pelayanan['tes_gangguan_emosional'] == "y")
                                                    <input type="checkbox" name="tes_emo" disable readonly data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @else
                                                    <input type="checkbox" name="tes_emo" disable readonly data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                                <label>Tes Gangguan Perilaku</label>
                                                <div class="input-group mb-3 bt-switch">
                                                @if($pelayanan['tes_gangguan_perilaku'] == "y")
                                                    <input type="checkbox" name="tes_perilaku" disable readonly checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @else
                                                    <input type="checkbox" name="tes_perilaku" disable readonly data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @endif                                                
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                                <label>Tes Kadar Kolesterol Darah</label> 
                                                <div class="input-group mb-3 bt-switch">
                                                @if($pelayanan['tes_kolesterol_darah'] == "y")
                                                    <input type="checkbox" name="tes_kolesterol" disable readonly checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @else
                                                    <input type="checkbox" name="tes_kolesterol" disable readonly data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @endif
                                                </div>
                                            </div>
                                            <div class="form-group m-0 col-lg-12 col-xlg-12 col-md-12 m-0">
                                                <label>Cara Deteksi Kepikunan</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-location-pin"></i></span>
                                                    </div>
                                                    <select name="kepikunan" id="select" disable readonly required="" class="form-control" aria-invalid="true">
                                                        @if($pelayanan['cara_deteksi_kepikunan'] == "Mini Cog")
                                                        <option value="">Pilih</option>
                                                        <option value="Mini Cog" selected>Menggunakan Mini Cog</option>
                                                        <option value="MMSE">Mini Mental Status Examination (MMSE)/Test Mental Mini</option>
                                                        <option value="AMT&GDS">Abreviated Mental Test (AMT) dan Geriatric Depression Scale (GDS)</option>
                                                        <option value="Tidak Dideteksi">Tidak Dideteksi</option>
                                                        @elseif($pelayanan['cara_deteksi_kepikunan'] == "MMSE")
                                                        <option value="">Pilih</option>
                                                        <option value="Mini Cog">Menggunakan Mini Cog</option>
                                                        <option value="MMSE" selected>Mini Mental Status Examination (MMSE)/Test Mental Mini</option>
                                                        <option value="AMT&GDS">Abreviated Mental Test (AMT) dan Geriatric Depression Scale (GDS)</option>
                                                        <option value="Tidak Dideteksi">Tidak Dideteksi</option>
                                                        @elseif($pelayanan['cara_deteksi_kepikunan'] == "AMT&GDS")
                                                        <option value="">Pilih</option>
                                                        <option value="Mini Cog">Menggunakan Mini Cog</option>
                                                        <option value="MMSE">Mini Mental Status Examination (MMSE)/Test Mental Mini</option>
                                                        <option value="AMT&GDS" selected>Abreviated Mental Test (AMT) dan Geriatric Depression Scale (GDS)</option>
                                                        <option value="Tidak Dideteksi">Tidak Dideteksi</option>
                                                        @elseif($pelayanan['cara_deteksi_kepikunan'] == "Tidak Dideteksi")
                                                        <option value="">Pilih</option>
                                                        <option value="Mini Cog">Menggunakan Mini Cog</option>
                                                        <option value="MMSE">Mini Mental Status Examination (MMSE)/Test Mental Mini</option>
                                                        <option value="AMT&GDS">Abreviated Mental Test (AMT) dan Geriatric Depression Scale (GDS)</option>
                                                        <option value="Tidak Dideteksi" selected>Tidak Dideteksi</option>
                                                        @else
                                                        <option value="" selected>Pilih</option>
                                                        <option value="Mini Cog">Menggunakan Mini Cog</option>
                                                        <option value="MMSE">Mini Mental Status Examination (MMSE)/Test Mental Mini</option>
                                                        <option value="AMT&GDS">Abreviated Mental Test (AMT) dan Geriatric Depression Scale (GDS)</option>
                                                        <option value="Tidak Dideteksi">Tidak Dideteksi</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <!-- Picker Date Style -->
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.date.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.time.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/legacy.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <!-- <script src="{{asset('adminbite-10/assets/libs/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/datetimepicker/datetimepicker.init.js')}}"></script> -->
    
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js')}}"></script>
    <!-- Switch Style -->
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/js/bootstrap-switch.min.js')}}"></script>
    <!-- Wizard Style -->
    <script src="{{asset('adminbite-10/assets/libs/jquery-steps/build/jquery.steps.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script>
    //Basic Example
    $("#example-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true
    });

    // Basic Example with form
    var form = $("#example-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password"
            }
        }
    });
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    });

    // Advance Example

    var form = $("#example-advanced-form").show();

    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            // Forbid next action on "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age-2").val()) < 18) {
                return false;
            }
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onStepChanged: function(event, currentIndex, priorIndex) {
            // Used to skip the "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
                form.steps("next");
            }
            // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3) {
                form.steps("previous");
            }
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password-2"
            }
        }
    });

    // Dynamic Manipulation
    $("#example-manipulation").steps({
        headerTag: "h3",
        bodyTag: "section",
        enableAllSteps: true,
        enablePagination: false
    });

    //Vertical Steps

    $("#example-vertical").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        stepsOrientation: "vertical"
    });

    //Custom design form example
    $(".tab-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onFinished: function(event, currentIndex) {
            document.getElementById('wizard-form').submit();

        }
    });


    var form = $(".validation-wizard").show();

    $(".validation-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onStepChanging: function(event, currentIndex, newIndex) {
            return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
        },
        onFinishing: function(event, currentIndex) {
            return form.validate().settings.ignore = ":disabled", form.valid()
        },
        onFinished: function(event, currentIndex) {
            document.getElementById('wizard-form').submit();
        }
    }), $(".validation-wizard").validate({
        ignore: "input[type=hidden]",
        errorClass: "text-danger",
        successClass: "text-success",
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element)
        },
        rules: {
            email: {
                email: !0
            }
        }
    })
    </script>
    <!-- Switch Style     -->
    <script>
    $(".bt-switch input[type='checkbox'], .bt-switch input[type='radio']").bootstrapSwitch();
    var radioswitch = function() {
        var bt = function() {
            $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioState")
            }), $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck")
            }), $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck", !1)
            })
        };
        return {
            init: function() {
                bt()
            }
        }
    }();
    $(document).ready(function() {
        init()
    });
    </script>
    <script>
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    $('#date-fr').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', lang: 'fr', weekStart: 1, cancelText: 'ANNULER' });
    $('#date-end').bootstrapMaterialDatePicker({ weekStart: 0 });
    $('#date-start').bootstrapMaterialDatePicker({ weekStart: 0 }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });
    </script>
    <script type="text/javascript">
        $('.pickadate-disable').pickadate({
            disable: [
                
                1, 7
            ]
        });
    </script>
</body>
</html>