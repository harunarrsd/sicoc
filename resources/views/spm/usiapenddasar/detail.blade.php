<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')@yield('head-ripel01')
<title>Detail Usia Pendidikan Dasar</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Usia Pendidikan Dasar</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Usia Pendidikan Dasar</li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Usia Pendidikan Dasar</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                    @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('success')}}
                            </div>
                        @endif
                        @if(session()->has('danger'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('danger')}}
                            </div>
                        @endif
                        <div class="card">
                            <div class="row">
                                <div class="card-body p-b-0 m-l-10">
                                    <h4 class="card-title">Riwayat Pelayanan </h4>
                                    <div class="d-flex no-block align-items-center m-b-10">
                                        <div class="m-r-10">
                                            <img src="{{asset('adminbite-10/assets/images/users/user.png')}}" alt="user" class="rounded-circle" width="45">
                                        </div>
                                        <div class="">
                                            <h5 class="m-b-0 font-16 font-medium">{{ $datawarga['nama']}}</h5>
                                            <span>{{ $datawarga['nik']}}</span>
                                            <h5 class="card-title m-b-0">
                                                {{ date('d M Y',strtotime($datawarga['tgl_lahir']))}}
                                                <span class="btn waves-effect waves-light btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mulai lahir sejak {{ date('d M Y',strtotime($datawarga['tgl_lahir']))}}">
                                                    Mulai Lahir
                                                </span>
                                            </h5>
                                        </div> 
                                    </div>
                                </div>
                                <div class="card-body col-lg-3 col-md-12">
                                    <h4 class="card-title m-b-0">
                                        Capaian SPM
                                        <span class="btn bg-info text-white btn-outline" data-toggle="tooltip" data-placement="top" title="" data-original-title="">
                                            {{ number_format($capaian_super_total,2) }}%
                                        </span>
                                    </h4>                                    
                                    <div class="d-flex flex-row text-center">
                                        <div class="p-10 p-l-0 b-r">
                                            <h6 class="font-light">Kelas 1</h6><b>{{ number_format($capaian_super_total1,2) }}%</b></div>
                                        <div class="p-10 b-r">
                                            <h6 class="font-light">Kelas 7</h6><b>{{ number_format($capaian_super_total7,2) }}%</b></div>
                                        
                                    </div>
                                </div>                                
                            </div>
                            <div class="row">
                                <div class="card-body p-b-0 p-t-0">
                                    <div class="d-md-flex align-items-center">
                                        <div>                           
                                            <ul class="nav nav-tabs customtab m-l-10" role="tablist">
                                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home2" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Kelas 1</span></a> </li>
                                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile2" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Kelas 7</span></a> </li>
                                            </ul>
                                        </div>
                                        <form method="POST" id="withnik-form4" action="{{url('/mulai-catat-usia-pend-dasar')}}" style="display: none;">{{ csrf_field() }}
                                            <input type="text" maxlength="16" name="nik" value="{{ Session::get('withnik4') }}">
                                        </form>
                                        @if($usia_penddasar["years"] >= 5 && $usia_penddasar["years"] <= 15 && $usiapenddasar['status_usia_pend_dasar'] == "Tahap Pelayanan" )
                                        <div class="ml-auto">
                                            <div class="dl">
                                                <div class="btn-group m-r-10">
                                                <a  href="{{ url('/mulai-catat-usia-pend-dasar') }}" onclick="event.preventDefault(); document.getElementById('withnik-form4').submit();">
                                                    <button type="button" class="btn btn-info">                                                    
                                                        <i class="mdi mdi-book-open-page-variant"></i> Tambah Pelayanan
                                                </button></a>     </div>                                           
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="home2" role="tabpanel">
                                <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="zero_config" class="table table-striped table-bordered display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>Kode Pelayanan</th>
                                                        <th>Tanggal Pelayanan</th>
                                                        <th>Fasilitas Kesehatan</th>
                                                        <th>Tenaga Kesehatan</th>
                                                        <th>Capaian</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($pelayanan as $i => $pel)
                                                    @if($pel['kelas'] == 1)
                                                        <td>{{ $pel['id']}}</td>
                                                        <td>
                                                            {{ date('d M Y',strtotime($pel['tanggal_pelayanan'])) }} <br>
                                                            <small class="text-muted"> {{ date('h:i:s',strtotime($pel['tanggal_pelayanan'])) }} </small>
                                                        </td>
                                                        <td>
                                                            @if(explode('_',$pel['lokasi'])[0] != null){{ explode('_',$pel['lokasi'])[0] }} @else - @endif <br>
                                                            <small class="text-muted"> @if(explode('_',$pel['lokasi'])[1] != null){{ explode('_',$pel['lokasi'])[1] }} @else - @endif </small>
                                                        </td>
                                                        <td>
                                                            @if(explode('_',$pel['tenaga_kerja'])[0] != null){{ explode('_',$pel['tenaga_kerja'])[0] }} @else - @endif <br>
                                                            <small class="text-muted"> @if(explode('_',$pel['tenaga_kerja'])[1] != null){{ explode('_',$pel['tenaga_kerja'])[1] }} @else - @endif </small>
                                                        </td>
                                                        <td><span class="label label-info">{{ number_format($cap_pel[$i])}}%</span></td>
                                                        <td>
                                                            <a href="{{ url('/detail-pelayanan-usia-pend-dasar/'.$pel['id']) }}">
                                                                <button type="submit" class="btn btn-info btn-xs">
                                                                    Detail
                                                                </button>
                                                            </a>
                                                            
                                                            <a href="{{ url('/ubah-pelayanan-usia-pend-dasar/'.$pel['id']) }}">
                                                                <button type="submit" class="btn btn-warning btn-xs">
                                                                    Ubah
                                                                </button>
                                                            </a>
                                                            
                                                            <a alt="default" class="btn btn-danger btn-xs" href="{{ url('/hapus-pelayanan-usia-pend-dasar/'.$pel['id']) }}">
                                                                Hapus
                                                            </a>         
                                                        </td>
                                                        </tr>
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="profile2" role="tabpanel">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="default_order" class="table table-striped table-bordered display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>Kode Pelayanan</th>
                                                        <th>Tanggal Pelayanan</th>
                                                        <th>Fasilitas Kesehatan</th>
                                                        <th>Tenaga Kesehatan</th>
                                                        <th>Capaian</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($pelayanan as $i => $pel)
                                                    @if($pel['kelas'] == 7)
                                                        <td>{{ $pel['id']}}</td>
                                                        <td>
                                                            {{ date('d M Y',strtotime($pel['tanggal_pelayanan'])) }} <br>
                                                            <small class="text-muted"> {{ date('h:i:s',strtotime($pel['tanggal_pelayanan'])) }} </small>
                                                        </td>
                                                        <td>
                                                            @if(explode('_',$pel['lokasi'])[0] != null){{ explode('_',$pel['lokasi'])[0] }} @else - @endif <br>
                                                            <small class="text-muted"> @if(explode('_',$pel['lokasi'])[1] != null){{ explode('_',$pel['lokasi'])[1] }} @else - @endif </small>
                                                        </td>
                                                        <td>
                                                            @if(explode('_',$pel['tenaga_kerja'])[0] != null){{ explode('_',$pel['tenaga_kerja'])[0] }} @else - @endif <br>
                                                            <small class="text-muted"> @if(explode('_',$pel['tenaga_kerja'])[1] != null){{ explode('_',$pel['tenaga_kerja'])[1] }} @else - @endif </small>
                                                        </td>
                                                        <td><span class="label label-info">{{ number_format($cap_pel[$i])}}%</span></td>
                                                        <td>
                                                            <a href="{{ url('/detail-pelayanan-usia-pend-dasar/'.$pel['id']) }}">
                                                                <button type="submit" class="btn btn-info btn-xs">
                                                                    Detail
                                                                </button>
                                                            </a>
                                                            
                                                            <a href="{{ url('/ubah-pelayanan-usia-pend-dasar/'.$pel['id']) }}">
                                                                <button type="submit" class="btn btn-warning btn-xs">
                                                                    Ubah
                                                                </button>
                                                            </a>
                                                            
                                                            <a alt="default" class="btn btn-danger btn-xs" href="{{ url('/hapus-pelayanan-usia-pend-dasar/'.$pel['id']) }}">
                                                                Hapus
                                                            </a>         
                                                        </td>
                                                        </tr>
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>