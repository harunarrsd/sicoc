<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>SPM Usia Pendidikan Dasar</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Usia Pendidikan Dasar</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active" aria-current="page">Standar Pelayanan Minimal</li>                                
                                    <li class="breadcrumb-item active" aria-current="page">Usia Pendidikan Dasar</li>
                                    <li class="breadcrumb-item active" aria-current="page">SPM Usia Pendidikan Dasar</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
                <div class="card-body bg-light">
                    <div class="d-md-flex align-items-center">
                        <div><h2>Pelayanan Usia Pendidikan Dasar</h2></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Penyataan Standar</h4>
                            Setiap anak pada usia pendidikan dasar mendapatkan skrining
                            kesehatan sesuai standar.
                            Pemerintah Daerah Kabupaten/Kota wajib melakukan penjaringan
                            kesehatan kepada anak usia pendidikan dasar di wilayah
                            kabupaten/kota tersebut pada waktu kelas 1 dan kelas 7
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Pelayanan Kesehatan Usia Pend. Dasar</h4>
                            Pelayanan kesehatan usia pendidikan dasar adalah penjaringan
                            kesehatan yang diberikan kepada anak usia pendidikan dasar,
                            minimal satu kali pada kelas 1 dan kelas 7 yang dilakukan oleh
                            Puskesmas.
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Standar pelayanan penjaringan kesehatan</h4>
                            <p>Standar pelayanan penjaringan kesehatan adalah pelayanan
                                yang meliputi :
                                a) Penilaian status gizi (tinggi badan, berat badan, tanda
                                klinis anemia);
                                b) Penilaian tanda vital (tekanan darah, frekuensi nadi dan
                                napas);
                                c) Penilaian kesehatan gigi dan mulut;
                                d) Penilaian ketajaman indera penglihatan dengan poster
                                snellen;
                                e) Penilaian ketajaman indera pendengaran dengan garpu
                                tala;</p>
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Semua anak usia pendidikan dasar</h4>
                            Semua anak usia pendidikan dasar di wilayah kabupaten/kota
                            adalah semua peserta didik kelas 1 dan kelas 7 di satuan
                            pendidikan dasar yang berada di wilayah kabupaten/kota.
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Defenisi Operasional Capaian Kinerja</h4>
                            <p>Capaian kinerja Pemerintah Daerah Kabupaten/Kota dalam
                                memberikan pelayanan skrining kesehatan anak usia pendidikan
                                dasar dinilai dari cakupan pelayanan kesehatan pada usia
                                pendidikan dasar sesuai standar di wilayah kabupaten/kota
                                tersebut dalam kurun waktu satu tahun ajaran.</p>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4><i class="fas fa-chevron-circle-right m-r-10 m-b-10"></i> Referensi</h4>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 25 Tahun 2014 tentang Upaya Kesehatan Anak;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 75 Tahun 2014 tentang Pusat Kesehatan Masyarakat;</h6>
                    <h6><i class="fas fa-square"></i> Rapor Kesehatan Ku untuk peserta didik SD/MI dan Rapor Kesehatan Ku untuk peserta didik SMP/MTs, SMA/MA/SMK.</h6>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>