<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>Detail Pelayanan Usia Pendidikan Dasar</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.date.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.time.css')}}">
    <link type="text/css" href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
            <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Usia Pendidikan Dasar</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Usia Pendidikan Dasar</li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Pelayanan</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="col-12 p-0">
                    <div class="card">
                        <div class="card-body wizard-content">
                            <div class="row">
                                <div class="col-lg-7 col-xlg-7 col-md-6 col-sm-12 p-b-0">
                                    <div class="card-body m-0">
                                        <h4 class="card-title">Detail Pelayanan Usia Pendidikan Dasar </h4>
                                        <h6 class="card-subtitle">Pelayanan Usia Pendidikan Dasar sesuai SPM</h6>
                                    </div> 
                                </div>
                                <div class="col-lg-3 col-xlg-3 col-md-3 col-sm-12">
                                    <h5 class="m-b-0 font-16 font-medium">{{ $datawarga['nama']}}</h5>
                                    <span>{{ $datawarga['nik']}}</span>
                                    <h5 class="card-title m-b-0">
                                        {{date('d M Y',strtotime(explode(' ',$datawarga['tgl_lahir'])[0]))}}
                                        <span class="btn waves-effect waves-light btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mulai lahir sejak {{explode(' ',$datawarga['tgl_lahir'])[0]}}">
                                            Mulai Lahir
                                        </span>
                                    </h5> 
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2 col-sm-12">
                                    <div class="card-title">    
                                        <h5>Capaian SPM</h5>
                                        <span class="btn bg-info text-white btn-outline" data-toggle="tooltip" data-placement="top" title="" data-original-title="% dan total T dari 40T">
                                            {{number_format($capaian_super_total,2)}}%
                                        </span>                                                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-xlg-6 col-md-12">
                                    <div class="card-body">
                                        <div class="row p-0">
                                            <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                                <label>Tanggal Pelayanan</label> <span class="text-danger">*</span>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                                    </div>
                                                    <input type="date" disabled readonly value="{{ explode(' ',$pelayanan['tanggal_pelayanan'])[0] }}" class="form-control pickadate-disable" name="tanggal_pelayanan" required/>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Sekolah Kelas</label> <span class="text-danger">*</span>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                    </div>
                                                    <select name="kelas" id="select" required="" disabled readonly class="form-control" aria-invalid="true">
                                                        @if($pelayanan['kelas'] == 1)
                                                            <option value="">Pilih</option>
                                                            <option value="1" selected>Kelas 1</option>
                                                            <option value="7">Kelas 7</option>
                                                        @elseif($pelayanan['kelas'] == 7)
                                                            <option value="">Pilih</option>
                                                            <option value="1">Kelas 1</option>
                                                            <option value="7" selected>Kelas 7</option>
                                                        @else
                                                            <option value="" selected>Pilih</option>
                                                            <option value="1">Kelas 1</option>
                                                            <option value="7">Kelas 7</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Fasilitas Kesahatan</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-location-pin"></i></span>
                                                    </div>
                                                    <input type="text" name="fasilitas_kesehatan" disabled readonly class="form-control" value="Puskesmas" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Lokasi Puskesmas</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-map-alt"></i></span>
                                                    </div>
                                                    <input type="text" value="{{ explode('_',$pelayanan['lokasi'])[1]}}" disabled readonly name="lokasi_pelayanan" class="form-control" placeholder="Co :Puskesmas Beji">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Tenaga Kesehatan</label> <span class="text-danger">*</span>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                    </div>
                                                    <select name="tenaga_kesehatan" id="select"  disabled readonly required="" class="form-control" aria-invalid="true">
                                                        @if(explode('_',$pelayanan['tenaga_kerja'])[0] == "Bidan")
                                                            <option value="">Pilih</option>
                                                            <option value="Bidan" selected>Bidan</option>
                                                            <option value="Perawat">Perawat</option>
                                                            <option value="Dokter/DLP">Dokter/DLP</option>
                                                            <option value="Dokter Spesialis Anak">Dokter Spesialis Anak</option>
                                                        @elseif(explode('_',$pelayanan['tenaga_kerja'])[0] == "Perawat")
                                                            <option value="">Pilih</option>
                                                            <option value="Bidan">Bidan</option>
                                                            <option value="Perawat"selected>Perawat</option>
                                                            <option value="Dokter/DLP">Dokter/DLP</option>
                                                            <option value="Dokter Spesialis Anak">Dokter Spesialis Anak</option>
                                                        @elseif(explode('_',$pelayanan['tenaga_kerja'])[0] == "Dokter/DLP")
                                                            <option value="">Pilih</option>
                                                            <option value="Bidan">Bidan</option>
                                                            <option value="Perawat">Perawat</option>
                                                            <option value="Dokter/DLP" selected>Dokter/DLP</option>
                                                            <option value="Dokter Spesialis Anak">Dokter Spesialis Anak</option>
                                                        @elseif(explode('_',$pelayanan['tenaga_kerja'])[0] == "Dokter Spesialis Anak")
                                                            <option value="">Pilih</option>
                                                            <option value="Bidan">Bidan</option>
                                                            <option value="Perawat">Perawat</option>
                                                            <option value="Dokter/DLP">Dokter/DLP</option>
                                                            <option value="Dokter Spesialis Anak" selected>Dokter Spesialis Anak</option>
                                                        @else
                                                            <option value="" selected>Pilih</option>
                                                            <option value="Bidan">Bidan</option>
                                                            <option value="Perawat">Perawat</option>
                                                            <option value="Dokter/DLP">Dokter/DLP</option>
                                                            <option value="Dokter Spesialis Anak">Dokter Spesialis Anak</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Nama Dokter/Bidan</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                    </div>
                                                    <input type="text" name="nama_stk" disabled readonly value="{{explode('_',$pelayanan['tenaga_kerja'])[1]}}" class="form-control" placeholder="Co : dr. Ahmad Riza">
                                                </div>
                                            </div>
                                        </div>
                                        <label>Penilaian Status Gizi</label> 
                                        <div class="row">
                                            <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                                <label>Penimbangan Berat Badan</label> 
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                    </div>
                                                    <input type="number"  disabled readonly name="berat" value="{{ explode('_',$pelayanan['status_gizi'])[0] }}" class="form-control" placeholder="0" min="0">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">kg</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                                <label>Pengukuran Tinggi Badan</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                    </div>
                                                    <input type="number" disabled readonly name="{{ explode('_',$pelayanan['status_gizi'])[1] }}" value="171" class="form-control" placeholder="0" min="0">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">cm</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                                <label> Pengecekan Tanda Klinis Anemia </label>
                                                <div class="input-group mb-3 bt-switch">
                                                    @if(explode('_',$pelayanan['status_gizi'])[2] == "y")
                                                        <input type="checkbox"disabled readonly  name="anemia" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                    @else
                                                        <input type="checkbox"disabled readonly  name="anemia" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-xlg-6 col-md-12">
                                    <div class="card-body">            
                                        <label>Penilaian Tanda Vital</label> 
                                        <div class="row">
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Tekanan Darah</label> 
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                    </div>
                                                    <input type="number" disabled readonly name="tekanan_darah" value="{{explode('_',$pelayanan['tanda_vital'])[0]}}" class="form-control" placeholder="Co : 119" min="0">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">x/menit</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Frekuensi Nadi dan Napas</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                    </div>
                                                    <input type="number" disabled readonly name="nadi_napas" value="{{explode('_',$pelayanan['tanda_vital'])[1]}}" class="form-control" placeholder="Co : 100" min="0">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">x/menit</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <label>Penilaian Kesehatan Gigi dan Mulut</label> 
                                        <div class="row">
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Penilaian Kesehatan Gigi</label> 
                                                <div class="input-group mb-3 bt-switch">
                                                @if(explode('_',$pelayanan['gigi_mulut'])[0] == "y")
                                                    <input type="checkbox" disabled readonly name="gigi" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @else
                                                    <input type="checkbox" disabled readonly name="gigi" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Penilaian Kesehatan Mulut</label>
                                                <div class="input-group mb-3 bt-switch">
                                                @if(explode('_',$pelayanan['gigi_mulut'])[1] == "y")
                                                    <input type="checkbox" disabled readonly name="mulut" data-size="small" checked data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @else
                                                    <input type="checkbox" disabled readonly name="mulut" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @endif
                                                </div>
                                            </div>
                                        </div>
                                        <label>Penilaian Ketajaman Indera</label> 
                                        <div class="row">
                                                <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label>Penilaian Indera Penglihatan <small> poster snellen </small></label> 
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if(explode('_',$pelayanan['ketajaman_indera'])[0] == "y")                                   
                                                        <input type="checkbox" disabled readonly name="mata" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                    @else
                                                        <input type="checkbox" disabled readonly name="mata" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label>Penilaian Indera Pendengaran <small> garpu tala </small></label>
                                                    <div class="input-group mb-3 bt-switch">
                                                    @if(explode('_',$pelayanan['ketajaman_indera'])[1] == "y")                                           
                                                        <input type="checkbox" disabled readonly name="telinga" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                    @else
                                                        <input type="checkbox" disabled readonly name="telinga" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                    @endif
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <!-- Picker Date Style -->
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.date.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.time.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/legacy.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <!-- <script src="{{asset('adminbite-10/assets/libs/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/datetimepicker/datetimepicker.init.js')}}"></script> -->
    
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js')}}"></script>
    <!-- Switch Style -->
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/js/bootstrap-switch.min.js')}}"></script>
    <!-- Wizard Style -->
    <script src="{{asset('adminbite-10/assets/libs/jquery-steps/build/jquery.steps.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script>
    //Basic Example
    $("#example-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true
    });

    // Basic Example with form
    var form = $("#example-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password"
            }
        }
    });
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    });

    // Advance Example

    var form = $("#example-advanced-form").show();

    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            // Forbid next action on "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age-2").val()) < 18) {
                return false;
            }
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onStepChanged: function(event, currentIndex, priorIndex) {
            // Used to skip the "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
                form.steps("next");
            }
            // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3) {
                form.steps("previous");
            }
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password-2"
            }
        }
    });

    // Dynamic Manipulation
    $("#example-manipulation").steps({
        headerTag: "h3",
        bodyTag: "section",
        enableAllSteps: true,
        enablePagination: false
    });

    //Vertical Steps

    $("#example-vertical").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        stepsOrientation: "vertical"
    });

    //Custom design form example
    $(".tab-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onFinished: function(event, currentIndex) {
            document.getElementById('wizard-form').submit();

        }
    });


    var form = $(".validation-wizard").show();

    $(".validation-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onStepChanging: function(event, currentIndex, newIndex) {
            return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
        },
        onFinishing: function(event, currentIndex) {
            return form.validate().settings.ignore = ":disabled", form.valid()
        },
        onFinished: function(event, currentIndex) {
            document.getElementById('wizard-form').submit();
        }
    }), $(".validation-wizard").validate({
        ignore: "input[type=hidden]",
        errorClass: "text-danger",
        successClass: "text-success",
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element)
        },
        rules: {
            email: {
                email: !0
            }
        }
    })
    </script>
    <!-- Switch Style     -->
    <script>
    $(".bt-switch input[type='checkbox'], .bt-switch input[type='radio']").bootstrapSwitch();
    var radioswitch = function() {
        var bt = function() {
            $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioState")
            }), $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck")
            }), $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck", !1)
            })
        };
        return {
            init: function() {
                bt()
            }
        }
    }();
    $(document).ready(function() {
        init()
    });
    </script>
    <script>
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    $('#date-fr').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', lang: 'fr', weekStart: 1, cancelText: 'ANNULER' });
    $('#date-end').bootstrapMaterialDatePicker({ weekStart: 0 });
    $('#date-start').bootstrapMaterialDatePicker({ weekStart: 0 }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });
    </script>
    <script type="text/javascript">
        $('.pickadate-disable').pickadate({
            disable: [
                
                1, 7
            ]
        });
    </script>
</body>
</html>