<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>Perubahan Data Usia Pendidikan Dasar</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Usia Pendidikan Dasar</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Usia Pendidikan Dasar</li>
                                    <li class="breadcrumb-item active" aria-current="page">Ubah Usia Pendidikan Dasar</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
               <div class="col-12">    
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Perubahan Usia Pendidikan Dasar</h4>
                            <h6 class="card-subtitle m-b-30">Perubahan Data Usia Pendidikan Dasar</h6>
                            <form method="POST" action="{{ url('/edit-usia-pend-dasar')}}">{{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Nomor Induk Kependudukan</label><span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="select" id="select" required="" class="form-control" aria-invalid="true" onchange="myFunction()">
                                                <option value="">Pilih</option>
                                                @foreach($list_nik as $i => $nik)
                                                    <option value="{{ $nik }}_{{$list_ttl[$i]}}">{{ $list_name[$i] }} -- {{ $nik }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                        <label>Tanggal Lahir</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                            </div>
                                            <input type="date" id="tanggal_lahir" name="tanggal_lahir" class="form-control" disabled required>
                                            <input type="text" id="id" name="id" class="form-control" style="display:none;" value="{{ $usiapenddasar['id']}}">
                                            <input type="text" id="nik" name="nik" class="form-control" style="display:none;">
                                        </div>
                                    </div>
                                </div>
                                <div class="row p-l-10">
                                    
                                    <a href="{{ url('/detail-usia-pend-dasar/'.$usiapenddasar['id'])}}">
                                        <button type="button" class="btn btn-success m-r-10">
                                            {{number_format($capaian_super_total,2)}}% -- Detail Capaian SPM >
                                        </button>
                                    </a>
                                    <button type="submit" class="btn btn-info m-r-10">Kirimkan Perubahan ></button> 
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @yield('js-ripel01')
            <script>
                function myFunction() {
                var x = document.getElementById("select").value;

                var element = x.split("_");
                document.getElementById("nik").value = element[0];
                document.getElementById("tanggal_lahir").value = element[1].split(" ")[0];
                }
            </script>
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>