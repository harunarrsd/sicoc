<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>SPM Usia Produktif</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Usia Produktif</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active" aria-current="page">Standar Pelayanan Minimal</li>                                
                                    <li class="breadcrumb-item active" aria-current="page">Usia Produktif</li>
                                    <li class="breadcrumb-item active" aria-current="page">SPM Usia Produktif</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
                <div class="card-body bg-light">
                    <div class="d-md-flex align-items-center">
                        <div><h2>Pelayanan Usia Produktif</h2></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Penyataan Standar</h4>
                            Setiap warga negara Indonesia usia 15–59 tahun mendapatkan
                            skrining kesehatan sesuai standar.
                            Pemerintah Daerah Kabupaten/Kota wajib memberikan skrining
                            kesehatan sesuai standar pada warga negara usia 15–59 tahun di
                            wilayah kerjanya dalam kurun waktu satu tahun.
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Pelayanan skrining kesehatan</h4>
                            Pelayanan skrining kesehatan usia 15–59 tahun sesuai standar
                            adalah:
                            a) Pelayanan skrining kesehatan usia 15–59 tahun diberikan
                            sesuai kewenanganya oleh:
                            (1) Dokter;
                            (2) Bidan;
                            (3) Perawat;
                            (4) Nutrisionis/Tenaga Gizi.
                            (5) Petugas Pelaksana Posbindu PTM terlatih
                            b) Pelayanan skrining kesehatan usia 15–59 tahun dilakukan
                            di Puskesmas dan jaringannya (Posbindu PTM) serta
                            fasilitas pelayanan kesehatan lainnya yang bekerja sama
                            dengan pemerintah daerah.
                            c) Pelayanan skrining kesehatan usia15–59 tahun minimal
                            dilakukan satu tahun sekali.
                            d) Pelayanan skrining kesehatan usia 15–59 tahun meliputi :
                            (1) Deteksi kemungkinan obesitas dilakukan dengan
                            memeriksa tinggi badan dan berat badan serta lingkar
                            perut.
                            (2) Deteksi hipertensi dengan memeriksa tekanan darah
                            sebagai pencegahan primer.
                            (3) Deteksi kemungkinan diabetes melitus menggunakan
                            tes cepat gula darah.
                            (4) Deteksi gangguan mental emosional dan perilaku.
                            (5) Pemeriksaan ketajaman penglihatan
                            (6) Pemeriksaan ketajaman pendengaran
                            (7) Deteksi dini kanker dilakukan melalui pemeriksaan
                            payudara klinis dan pemeriksaan IVA khusus untuk
                            wanita usia 30–59 tahun.
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Pengunjung penderita kelainan</h4>
                            <p>Pengunjung yang ditemukan menderita kelainan wajib ditangani
                                atau dirujuk ke fasilitas pelayanan kesehatan yang mampu
                                menanganinya. </p>
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="card-body">
                            <h4 class="card-title">Definisi Operasional Capaian Kerja</h4>
                            Capaian kinerja Pemerintah Daerah Kabupaten/Kota dalam
                            memberikan pelayanan skrining kesehatan warga negara berusia
                            usia 15–59 tahun dinilai dari persentase pengunjung usia 15–59
                            tahun yang mendapat pelayanan skrining kesehatan sesuai standar
                            di wilayah kerjanya dalam kurun waktu satu tahun.
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4><i class="fas fa-chevron-circle-right m-r-10 m-b-10"></i> Referensi</h4>
                    <h6><i class="fas fa-square"></i> Keputusan Menteri Kesehatan Nomor 854/Menkes/SK/IX/2009 tentang Pedoman Pengendalian Penyakit Jantung dan Pembuluh Darah;</h6>
                    <h6><i class="fas fa-square"></i> Peraturan Menteri Kesehatan Nomor 71 Tahun 2015 tentang Penanggulangan Penyakit Tidak Menular;</h6>
                    <h6><i class="fas fa-square"></i> Pedoman Penjaringan Kesehatan Anak Satuan Lanjutan;</h6>
                    <h6><i class="fas fa-square"></i> Pedoman Pelayanan Kesehatan Peduli Remaja;</h6>
                    <h6><i class="fas fa-square"></i> Rapor Kesehatan Ku untuk peserta didik SD/MI dan Rapor Kesehatan Ku untuk peserta didik SMP/MTs, SMA/MA/SMK;</h6>
                    <h6><i class="fas fa-square"></i> Pedoman Umum Pengendalian Obesitas, Jakarta; Departemen Kesehatan;</h6>
                    <h6><i class="fas fa-square"></i> Manual Peralatan Skrining dan Monitoring Faktor Risiko Diabetes Melitus dan Penyakit Metabolik Lainnya, Jakarta; Departemen Kesehatan;</h6>
                    <h6><i class="fas fa-square"></i> Petunjuk Teknis Pengukuran Faktor Risiko Diabetes Mellitus, Edisi 2 Jakarta; Kementerian Kesehatan;</h6>
                    <h6><i class="fas fa-square"></i> Pedoman Pengukuran Tekanan Darah;</h6>
                    <h6><i class="fas fa-square"></i> Pedoman Pengendalian Hipertensi;</h6>
                    <h6><i class="fas fa-square"></i> Konsensus Pengelolaan Diabetes Melitus di Indonesia. Jakarta, Sekretariat PB Perkeni;</h6>
                    <h6><i class="fas fa-square"></i> Pedoman Kesehatan Jiwa;</h6>
                    <h6><i class="fas fa-square"></i> Pedoman Umum Penyelenggraan Posbindu PTM;</h6>
                    <h6><i class="fas fa-square"></i> Petunjuk Teknis Penyelenggaraan Posbindu PTM;</h6>
                    <h6><i class="fas fa-square"></i> Petunjuk Teknis Penyelenggaraan CERDIK disekolah.</h6>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>