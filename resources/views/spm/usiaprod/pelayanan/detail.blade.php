<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>Detail Pelayanan Usia Produktif</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.date.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.time.css')}}">
    <link type="text/css" href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
            <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Usia Produktif</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Usia Produktif</li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Pelayanan</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="col-12 p-0">
                    <div class="card">
                        <div class="card-body wizard-content">
                            <div class="row">
                                <div class="col-lg-7 col-xlg-7 col-md-6 col-sm-12 p-b-0">
                                    <div class="card-body m-0">
                                        <h4 class="card-title">Detail Pelayanan Usia Produktif </h4>
                                        <h6 class="card-subtitle">Pelayanan Usia Produktif sesuai SPM</h6>
                                    </div> 
                                </div>
                                <div class="col-lg-3 col-xlg-3 col-md-3 col-sm-12">
                                    <h5 class="m-b-0 font-16 font-medium">{{$datawarga['nama']}}</h5>
                                    <span>{{$datawarga['nik']}}</span>
                                    <h5 class="card-title m-b-0">
                                        {{date('d M Y',strtotime($datawarga['tgl_lahir']))}}
                                        <span class="btn waves-effect waves-light btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mulai lahir sejak {{date('d M Y',strtotime($datawarga['tgl_lahir']))}}">
                                            Mulai Lahir
                                        </span>
                                    </h5> 
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2 col-sm-12">
                                    <div class="card-title">    
                                        <h5>Capaian SPM</h5>
                                        <span class="btn bg-info text-white btn-outline" data-toggle="tooltip" data-placement="top" title="" data-original-title="% dan total T dari 40T">
                                            {{ number_format($capaian_fix,2)}}%
                                        </span>                                                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-xlg-6 col-md-12">
                                    <div class="card-body">
                                        <div class="form-group m-0 col-lg-12 col-xlg-12 col-md-12 p-0">
                                            <label>Tanggal Pelayanan</label> <span class="text-danger">*</span>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                                </div>
                                                <input type="text" disabled readonly value="{{ explode(' ',$pelayanan['tanggal_pelayanan'])[0] }}" class="form-control pickadate-disable" name="tanggal_pelayanan" required/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Fasilitas Kesahatan</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-location-pin"></i></span>
                                                    </div>
                                                    <select disabled readonly name="fasilitas_kesehatan" id="select" required="" class="form-control" aria-invalid="true">
                                                        @if(explode('_',$pelayanan['lokasi'])[0] == "Puskesmas")
                                                        <option value="">Pilih</option>
                                                        <option value="Puskesmas" selected>Puskesmas</option>
                                                        <option value="Posbindu PTM">Posbindu PTM</option>
                                                        <option value="Rumah Sakit">Rumah Sakit</option>
                                                        <option value="Klinik">Klinik</option>
                                                        @elseif(explode('_',$pelayanan['lokasi'])[0] == "Posbindu PTM")
                                                        <option value="">Pilih</option>
                                                        <option value="Puskesmas">Puskesmas</option>
                                                        <option value="Posbindu PTM" selected>Posbindu PTM</option>
                                                        <option value="Rumah Sakit">Rumah Sakit</option>
                                                        <option value="Klinik">Klinik</option>
                                                        @elseif(explode('_',$pelayanan['lokasi'])[0] == "Rumah Sakit")
                                                        <option value="">Pilih</option>
                                                        <option value="Puskesmas">Puskesmas</option>
                                                        <option value="Posbindu PTM">Posbindu PTM</option>
                                                        <option value="Rumah Sakit" selected>Rumah Sakit</option>
                                                        <option value="Klinik">Klinik</option>
                                                        @elseif(explode('_',$pelayanan['lokasi'])[0] == "Klinik")
                                                        <option value="">Pilih</option>
                                                        <option value="Puskesmas">Puskesmas</option>
                                                        <option value="Posbindu PTM">Posbindu PTM</option>
                                                        <option value="Rumah Sakit">Rumah Sakit</option>
                                                        <option value="Klinik" selected>Klinik</option>
                                                        @else
                                                        <option value="" selected>Pilih</option>
                                                        <option value="Puskesmas">Puskesmas</option>
                                                        <option value="Posbindu PTM">Posbindu PTM</option>
                                                        <option value="Rumah Sakit">Rumah Sakit</option>
                                                        <option value="Klinik">Klinik</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                    <label>Lokasi Puskesmas</label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon11"><i class="ti-map-alt"></i></span>
                                                        </div>
                                                        <input type="text" disabled readonly value="{{ explode('_',$pelayanan['lokasi'])[1] }}" name="lokasi_pelayanan" class="form-control" placeholder="Co :Puskesmas Beji">
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Tenaga Kesehatan</label> <span class="text-danger">*</span>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                    </div>
                                                    <select disabled readonly name="tenaga_kesehatan" id="select" required="" class="form-control" aria-invalid="true">
                                                        @if(explode('_',$pelayanan['tenaga_kerja'])[0] == "Dokter")
                                                        <option value="">Pilih</option>
                                                        <option value="Dokter" selected>Dokter</option>
                                                        <option value="Bidan">Bidan</option>
                                                        <option value="Perawat">Perawat</option>
                                                        <option value="Nutrisionis/Tenaga Gizi">Nutrisionis/Tenaga Gizi</option>
                                                        <option value="Petugas Pelaksana Posbindu PTM terlatih">Petugas Pelaksana Posbindu PTM terlatih</option>
                                                        @elseif(explode('_',$pelayanan['tenaga_kerja'])[0] == "Bidan")
                                                        <option value="">Pilih</option>
                                                        <option value="Dokter">Dokter</option>
                                                        <option value="Bidan" selected>Bidan</option>
                                                        <option value="Perawat">Perawat</option>
                                                        <option value="Nutrisionis/Tenaga Gizi">Nutrisionis/Tenaga Gizi</option>
                                                        <option value="Petugas Pelaksana Posbindu PTM terlatih">Petugas Pelaksana Posbindu PTM terlatih</option>
                                                        @elseif(explode('_',$pelayanan['tenaga_kerja'])[0] == "Perawat")
                                                        <option value="">Pilih</option>
                                                        <option value="Dokter">Dokter</option>
                                                        <option value="Bidan">Bidan</option>
                                                        <option value="Perawat" selected>Perawat</option>
                                                        <option value="Nutrisionis/Tenaga Gizi">Nutrisionis/Tenaga Gizi</option>
                                                        <option value="Petugas Pelaksana Posbindu PTM terlatih">Petugas Pelaksana Posbindu PTM terlatih</option>
                                                        @elseif(explode('_',$pelayanan['tenaga_kerja'])[0] == "Nutrisionis/Tenaga Gizi")
                                                        <option value="">Pilih</option>
                                                        <option value="Dokter">Dokter</option>
                                                        <option value="Bidan">Bidan</option>
                                                        <option value="Perawat">Perawat</option>
                                                        <option value="Nutrisionis/Tenaga Gizi" selected>Nutrisionis/Tenaga Gizi</option>
                                                        <option value="Petugas Pelaksana Posbindu PTM terlatih">Petugas Pelaksana Posbindu PTM terlatih</option>
                                                        @elseif(explode('_',$pelayanan['tenaga_kerja'])[0] == "Petugas Pelaksana Posbindu PTM terlatih")
                                                        <option value="">Pilih</option>
                                                        <option value="Dokter">Dokter</option>
                                                        <option value="Bidan">Bidan</option>
                                                        <option value="Perawat">Perawat</option>
                                                        <option value="Nutrisionis/Tenaga Gizi">Nutrisionis/Tenaga Gizi</option>
                                                        <option value="Petugas Pelaksana Posbindu PTM terlatih" selected>Petugas Pelaksana Posbindu PTM terlatih</option>
                                                        @else
                                                        <option value="" selected>Pilih</option>
                                                        <option value="Dokter">Dokter</option>
                                                        <option value="Bidan">Bidan</option>
                                                        <option value="Perawat">Perawat</option>
                                                        <option value="Nutrisionis/Tenaga Gizi">Nutrisionis/Tenaga Gizi</option>
                                                        <option value="Petugas Pelaksana Posbindu PTM terlatih">Petugas Pelaksana Posbindu PTM terlatih</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Nama Dokter/Bidan</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                    </div>
                                                    <input type="text" disabled readonly value="{{ explode('_',$pelayanan['tenaga_kerja'])[1] }}" name="nama_stk" class="form-control" placeholder="Co : dr. Ahmad Riza">
                                                </div>
                                            </div>
                                        </div>
                                        <label>Deteksi Kemungkinan Obesitas</label> 
                                        <div class="row">
                                            <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                                    <label>Penimbangan Berat Badan</label> 
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                        </div>
                                                        <input type="number" disabled readonly value="{{explode('_',$pelayanan['berat_tinggi'])[0]}}" name="berat" class="form-control" placeholder="0" min="0">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">kg</span>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                                <label>Pengukuran Tinggi Badan</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                    </div>
                                                    <input type="number" name="tinggi"  disabled readonly value="{{explode('_',$pelayanan['berat_tinggi'])[1]}}" class="form-control" placeholder="0" min="0">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">cm</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4 col-xlg-4 col-md-12 m-0">
                                                <label> Pengukuran Lingkar Perut </label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                    </div>
                                                    <input type="number" disabled readonly name="perut" value="{{$pelayanan['lingkar_perut']}}" class="form-control" placeholder="0" min="0">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">cm</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-xlg-6 col-md-12">
                                    <div class="card-body">            
                                        <label>Deteksi Hipertensi dan Diabetes Melitus</label> 
                                        <div class="row">
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Tekanan Darah</label> 
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                                    </div>
                                                    <input type="number" disabled readonly name="tekanan_darah" value="{{ $pelayanan['tekanan_darah'] }}" class="form-control" placeholder="Co : 76" min="0">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">x/menit</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Tes Cepat Gula Darah </label> 
                                                <div class="input-group mb-3 bt-switch">
                                                @if($pelayanan['tes_cepat_gula_darah'] == "y")
                                                    <input type="checkbox" disabled readonly name="tes_gula_darah" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @else
                                                    <input type="checkbox" disabled readonly name="tes_gula_darah" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @endif
                                                </div>
                                            </div>
                                        </div>
                                        <label>Deteksi gangguan mental emosional dan perilaku</label> 
                                        <div class="row">
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Tes Gangguan Emosional</label> 
                                                <div class="input-group mb-3 bt-switch">
                                                @if($pelayanan['tes_gangguan_emosional'] == "y")
                                                    <input type="checkbox" disabled readonly name="tes_emo"  checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @else
                                                    <input type="checkbox" disabled readonly name="tes_emo" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Tes Gangguan Perilaku</label>
                                                <div class="input-group mb-3 bt-switch">
                                                @if($pelayanan['tes_gangguan_perilaku'] == "y")
                                                    <input type="checkbox" disabled readonly name="tes_perilaku" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @else
                                                    <input type="checkbox" disabled readonly name="tes_perilaku" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @endif
                                                </div>
                                            </div>
                                        </div>
                                        <label>Pemeriksaan Ketajaman Indera</label> 
                                        <div class="row">
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Pemeriksaan Indera Penglihatan <small> poster snellen </small></label> 
                                                <div class="input-group mb-3 bt-switch">
                                                @if($pelayanan['periksa_indra_penglihatan'] == "y")
                                                    <input type="checkbox" disabled readonly name="mata"checked  data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @else
                                                    <input type="checkbox" disabled readonly name="mata" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Pemeriksaan Indera Pendengaran <small> garpu tala </small></label>
                                                <div class="input-group mb-3 bt-switch">
                                                @if($pelayanan['periksa_indra_pendengaran'] == "y")
                                                    <input type="checkbox" disabled readonly name="telinga" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @else
                                                    <input type="checkbox" disabled readonly name="telinga" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @endif
                                                </div>
                                            </div>
                                        </div>
                                        <label>Deteksi Dini Kanker </label> <small> khusus untuk wanita usia 30–59 tahun. </small>
                                        <div class="row">
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Pemeriksaan Payudara Klinis </label> 
                                                <div class="input-group mb-3 bt-switch">
                                                @if($pelayanan['pemeriksaan_payudara_klinis'] == "y")
                                                    <input type="checkbox" disabled readonly name="pemeriksaan_payudara_klinis"checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @else
                                                    <input type="checkbox" disabled readonly name="pemeriksaan_payudara_klinis" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6 col-xlg-6 col-md-12 m-0">
                                                <label>Pemeriksaan IVA</label>
                                                <div class="input-group mb-3 bt-switch">
                                                @if($pelayanan['pemeriksaan_iva'] == "y")
                                                    <input type="checkbox" disabled readonly name="pemeriksaan_iva" checked data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @else
                                                    <input type="checkbox" disabled readonly name="pemeriksaan_iva" data-size="small" data-on-color="info" data-off-color="default" data-on-text="Sudah" data-off-text="Belum">
                                                @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <!-- Picker Date Style -->
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.date.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.time.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/legacy.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <!-- <script src="{{asset('adminbite-10/assets/libs/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/datetimepicker/datetimepicker.init.js')}}"></script> -->
    
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js')}}"></script>
    <!-- Switch Style -->
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/js/bootstrap-switch.min.js')}}"></script>
    <!-- Wizard Style -->
    <script src="{{asset('adminbite-10/assets/libs/jquery-steps/build/jquery.steps.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script>
    //Basic Example
    $("#example-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true
    });

    // Basic Example with form
    var form = $("#example-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password"
            }
        }
    });
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    });

    // Advance Example

    var form = $("#example-advanced-form").show();

    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            // Forbid next action on "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age-2").val()) < 18) {
                return false;
            }
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onStepChanged: function(event, currentIndex, priorIndex) {
            // Used to skip the "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
                form.steps("next");
            }
            // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3) {
                form.steps("previous");
            }
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password-2"
            }
        }
    });

    // Dynamic Manipulation
    $("#example-manipulation").steps({
        headerTag: "h3",
        bodyTag: "section",
        enableAllSteps: true,
        enablePagination: false
    });

    //Vertical Steps

    $("#example-vertical").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        stepsOrientation: "vertical"
    });

    //Custom design form example
    $(".tab-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onFinished: function(event, currentIndex) {
            document.getElementById('wizard-form').submit();

        }
    });


    var form = $(".validation-wizard").show();

    $(".validation-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onStepChanging: function(event, currentIndex, newIndex) {
            return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
        },
        onFinishing: function(event, currentIndex) {
            return form.validate().settings.ignore = ":disabled", form.valid()
        },
        onFinished: function(event, currentIndex) {
            document.getElementById('wizard-form').submit();
        }
    }), $(".validation-wizard").validate({
        ignore: "input[type=hidden]",
        errorClass: "text-danger",
        successClass: "text-success",
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element)
        },
        rules: {
            email: {
                email: !0
            }
        }
    })
    </script>
    <!-- Switch Style     -->
    <script>
    $(".bt-switch input[type='checkbox'], .bt-switch input[type='radio']").bootstrapSwitch();
    var radioswitch = function() {
        var bt = function() {
            $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioState")
            }), $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck")
            }), $(".radio-switch").on("switch-change", function() {
                $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck", !1)
            })
        };
        return {
            init: function() {
                bt()
            }
        }
    }();
    $(document).ready(function() {
        init()
    });
    </script>
    <script>
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    $('#date-fr').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', lang: 'fr', weekStart: 1, cancelText: 'ANNULER' });
    $('#date-end').bootstrapMaterialDatePicker({ weekStart: 0 });
    $('#date-start').bootstrapMaterialDatePicker({ weekStart: 0 }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });
    </script>
    <script type="text/javascript">
        $('.pickadate-disable').pickadate({
            disable: [
                
                1, 7
            ]
        });
    </script>
</body>
</html>