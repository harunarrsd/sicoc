<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>Catatan Pelayanan Usia Produktif</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pelayanan Usia Produktif</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Usia Produktif</li>
                                    <li class="breadcrumb-item active" aria-current="page">Catat Pelayanan</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row" id="step1">
                    <div class="col-lg-12 col-xlg-12 col-md-12">
                        @if(!empty($alert))
                            <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-times"></i> {{ $alert }}
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-body wizard-content">
                                <form method="POST" action="{{url('/mulai-catat-usia-prod')}}">{{ csrf_field() }}
                                <div class="form-group">
                                    <h5>Masukkan Nomor Induk Kependudukan <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <div class="input-group">
                                        <input type="text" id="step1-nik" maxlength="16" name="nik" class="form-control" required data-validation-required-message="Tidak boleh kosong" required data-validation-containsnumber-regex="(\d)+" data-validation-containsnumber-message="Mohon Masukkan NIK dengan benar, hanya berupa angka">
                                        <button class="btn btn-info m-l-5 text-white" type="submit">Lanjutkan ></button>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>