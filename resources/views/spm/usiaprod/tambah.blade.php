<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="GIK">
<meta name="author" content="GIK">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('adminbite-10/assets/images/sicoc-favicon.png')}}">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/select2/dist/css/select2.min.css')}}">
<link href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/morris.js/morris.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/toastr/build/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet">
<style>
    .sidebar-item a{
        font-weight:600;
    }
    .tx-c{
        text-align:center;
    }
    .bg-y{
        background:yellow;
    }
    .sidebar-link .icon-Record{
        visibility: visible !important;
    }
</style>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.date.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.time.css')}}">
    <link type="text/css" href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
            <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}">
            <link href="{{asset('adminbite-10/assets/libs/sweetalert2/dist/sweetalert2.min.css')}}" rel="stylesheet">
<title>Tambah Usia Produktif</title>
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                    <h4 class="page-title">Pelayanan Usia Produktif</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Standar Pelayanan Minimal</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Usia Produktif</li>
                                    <li class="breadcrumb-item active" aria-current="page">Pendaftaran Usia Produktif</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
               <div class="col-12">  
                   @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('success')}}
                            </div>
                        @endif
                        @if(session()->has('danger'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('danger')}}
                            </div>
                        @endif  
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Pendaftaran Usia Produktif</h4>
                            <h6 class="card-subtitle m-b-30">Pendaftaran Usia Produktif</h6>
                            <form method="POST" action="{{url('/save-usia-prod')}}">{{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Nama </label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="select" id="select" required="" class="form-control select2 custom-select" style="width: 90%; height:10px;" aria-invalid="true" onchange="getDataCaProd();">
                                                <option value="">Pilih</option>
                                                @foreach($list_ca_prod as $i => $ca_prod)
                                                    <option value="{{ $ca_prod['id'] }}">{{ $ca_prod['nama'] }} -- {{ $ca_prod['nik'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <input type="text" name="created_by" id="created_by" class="form-control" value="{{ Auth::user()['name'] }}" style="display:none;">
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Nomor Induk Kependudukan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled id="nik" name="nik" placeholder="Terisi Otomatis" />
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Nomor Kartu Keluarga</label> 
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled id="no_kk" name="no_kk" placeholder="Terisi Otomatis" />
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Tanggal Lahir</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled id="tanggal_lahir" name="tanggal_lahir" placeholder="Terisi Otomatis" />
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Waktu Lahir</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled id="waktu_lahir" name="waktu_lahir" placeholder="Terisi Otomatis" />
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Jenis Kelamin</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled id="jenis_kelamin" name="jenis_kelamin" placeholder="Terisi Otomatis" />
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Nomor Telepon</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled id="no_telp" name="no_telp" placeholder="Terisi Otomatis" />
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-10 col-xlg-10 col-md-12">
                                        <label>Alamat</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled id="alamat" name="alamat" placeholder="Terisi Otomatis" />
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-2 col-xlg-2 col-md-12">
                                        <label>Kode Pos</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-timer"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled id="kode_pos" name="kode_pos" placeholder="Terisi Otomatis" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-4 col-md-6 col-sm-12 m-0 p-0">
                                    <button type="submit" class="btn btn-info">Kirimkan ></button> 
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/select2/select2.init.js')}}"></script>
    <!-- Picker Date Style -->
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.date.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.time.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/legacy.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <!-- <script src="{{asset('adminbite-10/assets/libs/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/datetimepicker/datetimepicker.init.js')}}"></script> -->
    
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js')}}"></script>
    <script>
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    $('#date-fr').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', lang: 'fr', weekStart: 1, cancelText: 'ANNULER' });
    $('#date-end').bootstrapMaterialDatePicker({ weekStart: 0 });
    $('#date-start').bootstrapMaterialDatePicker({ weekStart: 0 }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });
    </script>
    <script type="text/javascript">
        
        
        $('.pickadate-disable').pickadate({
            disabled:[]
        });
        

    </script>
    <script src="{{asset('adminbite-10/assets/libs/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/sweetalert2/sweet-alert.init.js')}}"></script>
    <script>
        function getDataCaProd(){
            var select = document.getElementById("select").value;
            $.ajax({
                type:'GET',
                url:'/getDataWarga',
                data:{id:select},
                dataType:'json',
                success:function(data){
                    console.log(data);
                    if(data.data.nik != null){document.getElementById("nik").value = data.data.nik;}else{document.getElementById("nik").value = "-";}
                    if(data.data.no_kk != null){document.getElementById("no_kk").value = data.data.no_kk;}else{document.getElementById("no_kk").value = "-";}
                    if(data.data.tempat_lahir != null || data.data.tgl_lahir != null ){document.getElementById("tanggal_lahir").value = data.data.tempat_lahir+", "+data.data.tgl_lahir.split(" ")[0]}else{document.getElementById("tanggal_lahir").value = "-";}
                    if(data.data.tempat_lahir != null){document.getElementById("waktu_lahir").value = data.data.tgl_lahir.split(" ")[1];}else{document.getElementById("waktu_lahir").value = "-";}
                    if(data.data.jenis_kelamin == "P")
                    {document.getElementById("jenis_kelamin").value = "Perempuan";
                    }else if(data.data.jenis_kelamin == "L"){
                        document.getElementById("jenis_kelamin").value = "Laki-Laki";
                    }else{
                        document.getElementById("jenis_kelamin").value = "-";
                    }
                    if(data.data.no_telp != null){document.getElementById("no_telp").value = data.data.no_telp;}else{document.getElementById("no_telp").value = "-";}
                    if(data.data.alamat != null){document.getElementById("alamat").value = data.data.alamat;}else{document.getElementById("alamat").value = "-";}
                    if(data.data.kode_pos != null){document.getElementById("kode_pos").value = data.data.kode_pos;}else{document.getElementById("kode_pos").value = "-";}
                }
            });            
        }
    </script>

</body>
</html>