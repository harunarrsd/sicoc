<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')@yield('head-ripel01')
<title>Data User</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Manage Users</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Data Masyakarat dan Akun User</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Users</li>
                                    <li class="breadcrumb-item active" aria-current="page">Manage Users</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('success')}}
                            </div>
                        @endif
                        @if(session()->has('danger'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('danger')}}
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-body">    
                                <div class="d-md-flex align-items-center m-b-10">
                                    <div>
                                        <h4 class="card-title">Data Users</h4>
                                        <h6 class="card-subtitle">Semua User yang terlibat pada SICOC</h6>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="dl">
                                            <div class="btn-group">
                                                <a href="{{ url('/tambah-user')}}"><button type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top">
                                                    <i class="mdi mdi-plus"></i> Tambah Users Baru
                                                </button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    @if(Auth::user()['level'] == 1)    
                                    <div class="col-sm-12 col-lg-3 text-center">
                                        <div class="card bg-light-info no-card-border">
                                            <div class="card-body">
                                                <h1 class="m-0 p-0">{{ $total_super_admin}}</h1>
                                                <span>Super Admin</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-3 text-center">
                                        <div class="card bg-light-success no-card-border">
                                            <div class="card-body">
                                                <h1 class="m-0 p-0">{{ $total_admin}}</h1>
                                                <span>Admin</span> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-3 text-center">
                                        <div class="card bg-light-warning no-card-border">
                                            <div class="card-body">
                                                <h1 class="m-0 p-0">{{ $total_tenaga_kerja}}</h1>
                                                <span>Tenaga Kerja</span>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-3 text-center">
                                        <div class="card bg-light-danger no-card-border">
                                            <div class="card-body">        
                                                <h1 class="m-0 p-0">{{ $total_masyarakat}}</h1>
                                                <span>Masyarakat</span>    
                                            </div>
                                        </div>
                                    </div>
                                    @elseif(Auth::user()['level'] == 2) 
                                    <div class="col-sm-12 col-lg-4 text-center">
                                        <div class="card bg-light-success no-card-border">
                                            <div class="card-body">
                                                <h1 class="m-0 p-0">{{ $total_admin}}</h1>
                                                <span>Admin</span> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-4 text-center">
                                        <div class="card bg-light-warning no-card-border">
                                            <div class="card-body">
                                                <h1 class="m-0 p-0">{{ $total_tenaga_kerja}}</h1>
                                                <span>Tenaga Kerja</span>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-4 text-center">
                                        <div class="card bg-light-danger no-card-border">
                                            <div class="card-body">        
                                                <h1 class="m-0 p-0">{{ $total_masyarakat}}</h1>
                                                <span>Masyarakat</span>    
                                            </div>
                                        </div>
                                    </div>
                                    @endif   
                                </div>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table bg-white table-bordered nowrap display">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Lengkap</th>
                                                <th>Email</th>
                                                <th>Username</th>
                                                <th>Role</th>
                                                <th>Joining date</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $no=1;@endphp
                                            @foreach($user_all as $i => $user)
                                            @if(Auth::user()['level'] == 1) 
                                            <tr>
                                                <td>
                                                    {{ $no++}}
                                                </td>
                                                <td>
                                                    <a href="javscript:void(0)">
                                                        <img src="{{asset('adminbite-10/assets/images/users/user.png')}}" alt="user" class="rounded-circle" width="30" /> {{ $user['name']}}</a>
                                                </td>
                                                <td>{{ $user['email']}}</td>
                                                <td>{{ $user['username']}}</td>
                                                <td>
                                                    @if($user['level'] == 1)
                                                    <span class="label label-info">Super Admin</span>
                                                    @elseif($user['level'] == 2)
                                                    <span class="label label-success">Admin</span>
                                                    @elseif($user['level'] == 3)
                                                    <span class="label label-warning">Tenaga Kerja</span>
                                                    @elseif($user['level'] == 4)
                                                    <span class="label label-danger">Masyarakat</span>
                                                    @endif
                                                </td>
                                                <td>{{ date('d M Y',strtotime($user['created_at']))}}</td>

                                                <td>
                                                    <a href="{{ url('/detail-user/'.$user->id )}}">
                                                        <button type="submit" class="btn btn-info btn-xs">
                                                            Detail
                                                        </button>
                                                    </a>
                                                    
                                                    <a href="{{ url('/ubah-user/'.$user->id)}}">
                                                        <button type="submit" class="btn btn-warning btn-xs">
                                                            Ubah
                                                        </button>
                                                    </a>

                                                    <button alt="default" class="btn btn-danger btn-xs" data-href="{{ url('/hapus-user/'.$user->id)}}" data-toggle="modal" data-target="#confirm-delete">
                                                        Hapus
                                                    </button>
                                                </td>
                                            </tr>
                                            @elseif(Auth::user()['level'] == 2) 
                                            @if($user['level'] != 1)
                                            <tr>
                                                <td>
                                                    {{ $user['id']}}
                                                </td>
                                                <td>
                                                    <a href="javscript:void(0)">
                                                        <img src="{{asset('adminbite-10/assets/images/users/user.png')}}" alt="user" class="rounded-circle" width="30" /> {{ $user['name']}}</a>
                                                </td>
                                                <td>{{ $user['email']}}</td>
                                                <td>{{ $user['username']}}</td>
                                                <td>
                                                    @if($user['level'] == 2)
                                                    <span class="label label-success">Admin</span>
                                                    @elseif($user['level'] == 3)
                                                    <span class="label label-warning">Tenaga Kerja</span>
                                                    @elseif($user['level'] == 4)
                                                    <span class="label label-danger">Masyarakat</span>
                                                    @endif
                                                </td>
                                                <td>{{ date('d M Y',strtotime($user['created_at']))}}</td>

                                                <td>
                                                    <a href="{{ url('/detail-user/'.$user->id )}}">
                                                        <button type="submit" class="btn btn-info btn-xs">
                                                            Detail
                                                        </button>
                                                    </a>
                                                    
                                                    <a href="{{ url('/ubah-user/'.$user->id)}}">
                                                        <button type="submit" class="btn btn-warning btn-xs">
                                                            Ubah
                                                        </button>
                                                    </a>

                                                    <button alt="default" class="btn btn-danger btn-xs" data-href="{{ url('/hapus-user/'.$user->id)}}" data-toggle="modal" data-target="#confirm-delete">
                                                        Hapus
                                                    </button>
                                                        
                                                </td>
                                            </tr>
                                            @endif
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            Hapus
                        </div>
                        <div class="modal-body">
                            Anda yakin ingin menghapus ?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            <a class="btn btn-danger btn-ok text-white">Hapus</a>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
    </script>
    
</body>
</html>