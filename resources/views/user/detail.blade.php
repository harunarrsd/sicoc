<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')@yield('head-ripel01')
<title>Detail User</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Detail Users</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Data Warga dan User Account</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Users</li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Users</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-md-flex align-items-center m-b-10">
                                    <div>
                                        <h3 class="card-title">Profil User</h3>
                                        <h6 class="card-subtitle m-b-0">Detail User SICOC</h6>   
                                    </div>
                                    <div class="ml-auto">
                                        <div class="dl">
                                            <div class="col-lg-3 col-md-3 col-sm-6">
                                                <div class="white-box text-center"> <img src="{{asset('adminbite-10/assets/images/users/user.png')}}" style="weight:50px;height:50px;"> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h2 class="m-t-10">{{ $user['name'] }}</h2>
                                        <p>{{ $user['bio'] }}</p>
                                        <button class="btn btn-dark btn-rounded m-r-5">{{ $user['username'] }}</button>
                                        <button class="btn btn-primary btn-rounded"> {{ $user['email'] }} </button>
                                        @if($user['level'] == 1)
                                            <button class="btn btn-info btn-rounded"> Super Admin </button>
                                        @elseif($user['level'] == 2)
                                            <button class="btn btn-info btn-success"> Admin </button>
                                        @elseif($user['level'] == 3)
                                            <button class="btn btn-warning btn-rounded"> Tenaga Kerja </button>
                                        @elseif($user['level'] == 4)
                                            <button class="btn btn-danger btn-rounded"> Masyarakat </button>
                                        @endif
                                        <div class="table-responsive m-t-20">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td>Nama Lengkap</td>
                                                        <td>email</td>
                                                        <td>Username</td>
                                                        <td>Tanggal Bergabung</td>
                                                        <td>Role</td>
                                                    </tr>
                                                    <tr>
                                                        <td> {{ $user['name'] }} </td>
                                                        <td> {{ $user['email'] }} </td>
                                                        <td> {{ $user['username'] }} </td>
                                                        <td> {{ date('d F, Y',strtotime($user['created_at'])) }} {{ date('H:i',strtotime($user['created_at'])) }} </td>
                                                        <td> 
                                                            @if($user['level'] == 1)
                                                                Super Admin
                                                            @elseif($user['level'] == 2)
                                                                Admin 
                                                            @elseif($user['level'] == 3)
                                                                Tenaga Kerja
                                                            @elseif($user['level'] == 4)
                                                                Masyarakat
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
</body>
</html>