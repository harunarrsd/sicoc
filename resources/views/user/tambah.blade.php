<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')
<title>Tambah User</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
    <link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Tambah User</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Data Warga dan User Account</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Users</li>
                                    <li class="breadcrumb-item active" aria-current="page">Tambah User</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-md-flex align-items-center m-b-10">
                                    <div>
                                        <h4 class="card-title"> Pendaftaran User </h4>
                                        <h6 class="card-subtitle"> Pendaftaran User pada aplikasi SICOC</h6>   
                                    </div>
                                    <div class="ml-auto">
                                        <div class="dl">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <form method="POST" class="was-validated" action="{{ url('/save-user')}}">{{ csrf_field() }}
                                <div class="card-body col-md-12 col-lg-12 col-sm-12 m-0 p-b-0">
                                    <div class="row"> 
                                        <div class="col-md-6 col-lg-6 col-sm-12">
                                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                <label for="name">Nama<span class="text-danger">*</span> :</label>
                                                <input id="name" type="text" placeholder="Masukkan Nama Lengkap" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                                @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-12">
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="name">Email<span class="text-danger">*</span> :</label>
                                                <input id="email" type="email" placeholder="Masukkan Email Valid"  class="form-control" name="email" value="{{ old('email') }}" required>
                                                @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="level">Level<span class="text-danger">*</span> :</label>
                                                <select class="custom-select form-control" required id="level" name="level" onchange="myFunction()">
                                                    @if(Auth::user()['level'] == 1)    
                                                    <option value="">Pilih</option>
                                                    <option value="1">Super Admin</option>
                                                    <option value="2">Admin</option>
                                                    <option value="3">Tenaga Kerja</option>
                                                    <option value="4">Masyarakat</option>
                                                    @elseif(Auth::user()['level'] == 2)
                                                    <option value="">Pilih</option>
                                                    <option value="2">Admin</option>
                                                    <option value="3">Tenaga Kerja</option>
                                                    <option value="4">Masyarakat</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-12">
                                            <div class="form-group{{ $errors->has('instansi') ? ' has-error' : '' }}">
                                                <label for="instansi">Instansi<span class="text-danger">*</span> :</label>
                                                <input id="instansi" type="text" placeholder="Masukkan Nama Instansi terkait" class="form-control" name="instansi" value="{{ old('instansi') }}" required>
                                                @if ($errors->has('instansi'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('instansi') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 col-sm-12">
                                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                                <label for="username">Username<span class="text-danger">*</span> :</label>
                                                <input id="username" type="text" placeholder="Masukkan Username"  class="form-control" name="username" value="{{ old('username') }}" required>
                                                @if ($errors->has('username'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('username') }}</strong>
                                                    </span>
                                                @endif
                                                <select name="usernamewarga" id="usernamewarga" required class="custom-select form-control" onchange="myFunction2()">
                                                    <option value="" selected>Pilih</option>
                                                    @foreach($list_nik as $i => $nik)
                                                        <option value="{{ $nik }}_{{ $list_name[$i]}}">{{ $nik}}</option>
                                                    @endforeach
                                                    <option value="Bukan Masyarakat">Bukan Masyarakat</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="col-md-3 col-lg-3 col-sm-12">
                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label for="password">Password<span class="text-danger">*</span> :</label>
                                                <input id="password" type="password" placeholder="Masukkan Password" class="form-control" name="password" required>
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-3 col-sm-12">
                                            <div class="form-group">  
                                                <label for="confirm">Konfirmasi Password<span class="text-danger">*</span> :</label>
                                                <input id="confirm-password" name="confirm_password"  placeholder="Sesuaikan dengan Password" type="password" class="form-control" required onchange="check_pass()">
                                                <div id="message-confirm-password" class="invalid-feedback">Harus Sesuai dengan Password</div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <!-- <div class="card-body p-t-0"> 
                                    <div class="custom-control col-md-3 custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input" id="customControlValidation1" required>
                                        <label class="custom-control-label" for="customControlValidation1">Syarat dan Ketentuan</label>
                                        <div class="invalid-feedback">Setujui Syarat dan Ketentuan berlaku</div>
                                    </div>
                                </div> -->
                                <div class="card-body p-t-0 p-b-0"> 
                                    <button id="submit" type="submit" class="btn btn-info">
                                        Daftar
                                    </button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    <script src="{{asset('adminbite-10/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('adminbite-10/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- apps -->
    <script src="{{asset('adminbite-10/dist/js/app.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/app.init.light-sidebar.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/app-style-switcher.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('adminbite-10/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset('adminbite-10/dist/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('adminbite-10/dist/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('adminbite-10/dist/js/custom.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/extra-libs/jqbootstrapvalidation/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }(window, document, jQuery);
    </script>
    <script>
    $('[data-toggle="tooltip"]').tooltip();
    </script>
    <script>
        $("#usernamewarga").fadeOut();
        // $("#message-confirm-password").fadeOut();
        // $("#username").fadeOut();
        function myFunction() {
            var x = document.getElementById("level").value;
            if(x == 4){
                document.getElementById("instansi").value = "Warga Depok";                
                $("#username").slideUp();
                $("#usernamewarga").slideDown();

                var z = document.getElementById("usernamewarga").value;
                var y = document.getElementById("name").value;
                var elementz = z.split('_');
                if( y != elementz[1]){
                    $("#usernamewarga").val("");
                }
                $("#username").val("---");
            }
            else{
                $("#usernamewarga").val("Bukan Masyarakat");
                $("#usernamewarga").slideUp();
                $("#username").slideDown();
                
            }
        }
    </script>
    <script>
        function myFunction2() {  
            var z = document.getElementById("usernamewarga").value;
            var elementz = z.split('_');
            if(z != "Bukan Masyarakat" && z != ""){
                document.getElementById("name").value = elementz[1];
            }        
        }
        function check_pass(){
            $pass = document.getElementById("password").value;
            $cpass = document.getElementById("confirm-password").value;
            if($cpass == $pass){
                document.getElementById("submit").disabled = false;
                $("#message-confirm-password").slideUp();
            }
            else{
                document.getElementById("submit").disabled = true;
                $("#message-confirm-password").slideDown();
            }
        }
    </script>
    
</body>
</html>