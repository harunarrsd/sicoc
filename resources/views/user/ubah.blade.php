<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')@yield('head-ripel01')
<title>Ubah User</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Ubah Users</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Data Warga dan User Account</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Users</li>
                                    <li class="breadcrumb-item active" aria-current="page">Ubah Users</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <form method="POST" class="was-validated" action="{{ url('/edit-user')}}">{{ csrf_field() }}
                                    <div class="form-body">
                                        <div class="d-md-flex align-items-center m-b-10">
                                            <div>
                                                <h4 class="card-title"> Ubah User </h4>
                                                <h6 class="card-subtitle"> Perubahan data User SICOC</h6>   
                                            </div>
                                            <div class="ml-auto">
                                                <div class="dl">
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6 col-sm-12">
                                                <div class="form-group">
                                                    <label for="name">Nama<span class="text-danger">*</span> :</label>
                                                    <input id="name" type="text" value="{{ $user['name']}}" placeholder="Masukkan Nama Lengkap" class="form-control" name="name" required autofocus>
                                                    <input id="id" type="text" value="{{ $user['id']}}"class="form-control" name="id" required style="display:none;">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-sm-12">
                                                <div class="form-group">
                                                    <label for="name">Email<span class="text-danger">*</span> :</label>
                                                    <input id="email" type="email" value="{{ $user['email']}}" placeholder="Masukkan Email Valid"  class="form-control" name="email" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6 col-sm-12">
                                                <div class="form-group">
                                                    <label for="level">Level<span class="text-danger">*</span> :</label>
                                                    <select class="custom-select form-control" required id="level" name="level" onchange="myFunction()">
                                                        @if($user['level'] == 1)
                                                            <option value="">Pilih</option>
                                                            <option value="1" selected>Super Admin</option>
                                                            <option value="2">Admin</option>
                                                            <option value="3">Tenaga Kerja</option>
                                                            <option value="4">Masyarakat</option>
                                                        @elseif($user['level'] == 2)                                                    
                                                            <option value="">Pilih</option>
                                                            <option value="1">Super Admin</option>
                                                            <option value="2" selected>Admin</option>
                                                            <option value="3">Tenaga Kerja</option>
                                                            <option value="4">Masyarakat</option>
                                                        @elseif($user['level'] == 3)
                                                            <option value="">Pilih</option>
                                                            <option value="1">Super Admin</option>
                                                            <option value="2">Admin</option>
                                                            <option value="3" selected>Tenaga Kerja</option>
                                                            <option value="4">Masyarakat</option>
                                                        @elseif($user['level'] == 4)
                                                            <option value="">Pilih</option>
                                                            <option value="1">Super Admin</option>
                                                            <option value="2">Admin</option>
                                                            <option value="3">Tenaga Kerja</option>
                                                            <option value="4" selected>Masyarakat</option>
                                                        @else
                                                            <option value="" selected>Pilih</option>
                                                            <option value="1">Super Admin</option>
                                                            <option value="2">Admin</option>
                                                            <option value="3">Tenaga Kerja</option>
                                                            <option value="4">Masyarakat</option>
                                                        @endif
                                                    </select>

                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-sm-12">
                                                <div class="form-group">
                                                    <label for="instansi">Instansi<span class="text-danger">*</span> :</label>
                                                    <input id="instansi" type="text" value="{{$user['instansi']}}" placeholder="Masukkan Nama Instansi terkait" class="form-control" name="instansi"required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6 col-sm-12">
                                                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                                    <label for="username">Username<span class="text-danger">*</span> :</label>
                                                    <input id="username" type="text" value="{{$user['username']}}" placeholder="Masukkan Username"  class="form-control" name="username" required>
                                                    
                                                    <select name="usernamewarga" id="usernamewarga" class="custom-select form-control" onchange="myFunction2()">
                                                        <option value="" selected>Pilih</option>
                                                        @foreach($list_nik as $i => $nik)
                                                            <option value="{{ $nik }}_{{ $list_name[$i]}}">{{ $nik}}</option>
                                                        @endforeach
                                                        <option value="Bukan Masyarakat">Bukan Masyarakat</option>
                                                    </select>

                                                    <label for="gantipassword" class="p-t-20">Apakah ingin mengganti Password?<span class="text-danger">*</span> :</label>
                                                    <select class="custom-select form-control" required id="gantipassword" name="gantipassword" onchange="myFunction3()">
                                                        <option value="0">Tidak Ingin Ganti Password</option>
                                                        <option value="1">Ingin Ganti Password</option>   
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-sm-12">
                                                <div class="form-group">
                                                    <label for="bio">Deskripsi/Bio :</label>
                                                    <textarea id="bio" class="form-control" name="bio" placeholder="Tuliskan Bio/Deskripsi Anda" rows="5">{{$user['bio']}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-lg-4 col-sm-12">
                                                <div class="form-group">
                                                    <label id="labelpasswordlama" for="passwordlama">Password Lama<span class="text-danger">*</span> :</label>
                                                    <input id="passwordlama" type="password" placeholder="Masukkan Password Lama" class="form-control" name="passwordlama" onchange="check_oldPass()">
                                                    <div id="messagePasswordLama" class="invalid-feedback">Sesuaikan dengan Password Lama</div>                                                
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4 col-sm-12">
                                                <div class="form-group">
                                                    <label id="labelpassword" for="password">Password Baru<span class="text-danger">*</span> :</label>
                                                    <input id="password" type="password" placeholder="Masukkan Password" class="form-control" name="password">
                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4 col-sm-12">
                                                <div class="form-group">  
                                                    <label id="labelconfirmpassword" for="confirm">Konfirmasi Password Baru<span class="text-danger">*</span> :</label>
                                                    <input id="Passwordagain" name="Passwordagain"  placeholder="Sesuaikan dengan Password" type="password" class="form-control" onchange="check_pass()">
                                                    <div id="message-confirm-password" class="invalid-feedback">Harus Sesuai dengan Password Baru</div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" id="submit"class="btn btn-success"> <i class="fa fa-check"></i> Ubah</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <script>
    ! function(window, document, $) {
        "use strict";
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }(window, document, jQuery);
    </script>
    <script>
    $('[data-toggle="tooltip"]').tooltip();
    </script>
    <script>
        var validateconfirm = 0;
        var validateoldpass = 0;
        $("#labelpasswordlama").fadeOut();
        $("#passwordlama").fadeOut();
        $("#labelpassword").fadeOut();
        $("#password").fadeOut();
        $("#labelconfirmpassword").fadeOut();
        $("#Passwordagain").fadeOut();
        $("#message-confirm-password").fadeOut();

        function myFunction3() { 
            var gantipassword = document.getElementById("gantipassword").value;
            if(gantipassword == 1){
                $("#labelpasswordlama").slideDown();
                $("#passwordlama").slideDown();
                $("#labelpassword").slideDown();
                $("#password").slideDown();
                $("#labelconfirmpassword").slideDown();
                $("#Passwordagain").slideDown();
                $("#message-confirm-password").slideDown();


                $('#passwordlama').attr('required', true);
                $('#password').attr('required', true);
                $('#Passwordagain').attr('required', true);
                document.getElementById("passwordlama").required = true;
                document.getElementById("password").required = true;
                document.getElementById("Passwordagain").required = true;
            }
            else{
                $("#labelpasswordlama").slideUp();
                $("#passwordlama").slideUp();
                $("#labelpassword").slideUp();
                $("#password").slideUp();
                $("#labelconfirmpassword").slideUp();
                $("#Passwordagain").slideUp();
                $("#message-confirm-password").slideUp();


                $('#passwordlama').removeAttr('required');
                $('#password').removeAttr('required');
                $('#Passwordagain').removeAttr('required');
                document.getElementById("passwordlama").required = false;
                document.getElementById("password").required = false;
                document.getElementById("Passwordagain").required = false;

            }
        } 


        var x = document.getElementById("level").value;
        if(x == 4){
            $("#username").fadeOut();
            $("#usernamewarga").val("{{$user['username']}}_{{$user['name']}}");
        }
        else{
            $("#usernamewarga").fadeOut();
        }
        function myFunction() {
            var x = document.getElementById("level").value;
            if(x == 4){
                document.getElementById("instansi").value = "Warga Depok";                
                $("#username").slideUp();
                $("#usernamewarga").slideDown();

                // var z = document.getElementById("usernamewarga").value;
                // var y = document.getElementById("name").value;
                // var elementz = z.split('_');
                // if( y == null && y != elementz[1]){
                //     $("#usernamewarga").val("");
                // }
                // $("#username").val("---");

                $('#usernamewarga').attr('required', true);
                document.getElementById("usernamewarga").required = true;
            }
            else{
                $("#usernamewarga").val("");
                $("#usernamewarga").slideUp();
                $("#username").slideDown();
                
                $('#usernamewarga').removeAttr('required');
                document.getElementById("usernamewarga").required = false;
            }
        }
        function myFunction2() {  
            var z = document.getElementById("usernamewarga").value;
            var elementz = z.split('_');
            if(z != ""){
                document.getElementById("name").value = elementz[1];
            }        
        }
        function check_pass(){
            $pass = document.getElementById("password").value;
            $cpass = document.getElementById("Passwordagain").value;
            if($cpass == $pass){
                $("#message-confirm-password").slideUp();
                validateconfirm = 1;
                if(validateoldpass == 0){
                    document.getElementById("submit").disabled = true;
                }
                else{
                    document.getElementById("submit").disabled = false;
                }
            }
            else{
                validateconfirm = 0;
                document.getElementById("submit").disabled = true;
                $("#message-confirm-password").slideDown();

            }
        }
        function check_oldPass(){
            passwordlama = document.getElementById("passwordlama").value;
            iduser = document.getElementById("id").value;
            $.ajax({
                type:'GET',
                url:'/findPass',
                data:{id:iduser,lama:passwordlama},
                dataType:'json',
                success:function(data){
                    console.log(data.message);
                    if(data.message == "error"){
                        $("#messagePasswordLama").fadeIn();
                        validateoldpass = 0;
                        document.getElementById("submit").disabled = true;                        
                    }
                    else{
                        validateoldpass = 1;
                        if(validateconfirm == 0){
                            document.getElementById("submit").disabled = true;
                        }
                        else{
                            document.getElementById("submit").disabled = false;
                        }
                        $("#messagePasswordLama").fadeOut();
                    }

                },
                error:function(){
                    alert('eror');
                }
            });
        }
    </script>
</body>
</html>