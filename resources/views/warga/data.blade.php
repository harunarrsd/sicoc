<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
@yield('head-home')@yield('head-ripel01')
<title>Data Masyarakat</title>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<body>
    <!-- @yield('main-preloader') -->
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Manage Masyarakat</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Data Masyarakat dan Akun User</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Masyarakat</li>
                                    <li class="breadcrumb-item active" aria-current="page">Manage Masyarakat</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('success')}}
                            </div>
                        @endif
                        @if(session()->has('danger'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('danger')}}
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-body">    
                                <div class="d-md-flex align-items-center m-b-10">
                                    <div>
                                        <h4 class="card-title">Data Masyarakat</h4>
                                        <h6 class="card-subtitle">Semua Masyarakat yang terlibat pada SICOC</h6>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="dl">
                                            <div class="btn-group">
                                                <a href="{{ url('/tambah-warga')}}"><button type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top">
                                                    <i class="mdi mdi-plus"></i> Tambah Warga Baru
                                                </button></a>
                                            </div>
                                            <a href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <button type="button" class="btn" data-toggle="tooltip" data-placement="top">
                                                    <i class="ti-info"></i> Masyarakat pada SPM
                                                </button>
                                            </a>
                                            <div class="dropdown-menu animated bounceInDown">
                                                <div class="mega-dropdown-menu row p-10">
                                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <h3>Detail Masyarakat</h3>
                                                                        <h6 class="card-subtitle">Informasi Masyarakat pada SPM</h6></div>
                                                                    <div class="col-12">
                                                                        <div class="progress">
                                                                            <div class="progress-bar bg-info" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-lg-3 text-center">
                                                        <div class="card bg-light-info no-card-border">
                                                            <div class="card-body">
                                                                <h1 class="m-0 p-0">{{$warga_total_nik}}</h1>
                                                                <span>Total Masyarakat</span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-lg-3 text-center">
                                                        <div class="card bg-light-info no-card-border">
                                                            <div class="card-body">
                                                                <h1 class="m-0 p-0">{{ $warga_spm}}</h1>
                                                                <span>Masyarakat terdaftar SPM</span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-lg-3 text-center">
                                                        <div class="card bg-light-info no-card-border">
                                                            <div class="card-body">
                                                                <h1 class="m-0 p-0">{{ $warga_belum_spm}}</h1>
                                                                <span>Belum terdaftar SPM</span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-lg-3 text-center">
                                                        <div class="card bg-light-success no-card-border">
                                                            <div class="card-body">
                                                                <h1 class="m-0 p-0">{{$ibuhamil_total_nik}}</h1>
                                                                <span>{{$ibuhamil_total_nik}} NIK dari {{$ibuhamil_total}} Ibu Hamil</span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-lg-3 text-center">
                                                        <div class="card bg-light-success no-card-border">
                                                            <div class="card-body">
                                                                <h1 class="m-0 p-0">{{$ibubersalin_total_nik}}</h1>
                                                                <span>{{$ibubersalin_total_nik}} NIK dari {{$ibubersalin_total}} Ibu Bersalin</span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-lg-3 text-center">
                                                        <div class="card bg-light-success no-card-border">
                                                            <div class="card-body">
                                                                <h1 class="m-0 p-0">{{$bayibarulahir_total_nik}}</h1>
                                                                <span>{{$bayibarulahir_total_nik}} NIK dari {{$bayibarulahir_total}} Bayi Baru Lahir</span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-lg-3 text-center">
                                                        <div class="card bg-light-success no-card-border">
                                                            <div class="card-body">
                                                                <h1 class="m-0 p-0">{{$balita_total_nik}}</h1>
                                                                <span>{{$balita_total_nik}} NIK dari {{$balita_total}} Balita</span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-lg-3 text-center">
                                                        <div class="card bg-light-warning no-card-border">
                                                            <div class="card-body">
                                                                <h1 class="m-0 p-0">{{$usiapenddasar_total_nik}}</h1>
                                                                <span>{{$usiapenddasar_total_nik}} NIK dari {{$usiapenddasar_total}} Usia Pend. Dasar</span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-lg-3 text-center">
                                                        <div class="card bg-light-warning no-card-border">
                                                            <div class="card-body">
                                                                <h1 class="m-0 p-0">{{$usiaprod_total_nik}}</h1>
                                                                <span>{{$usiaprod_total_nik}} NIK  dari {{$usiaprod_total}} Usia Produktif</span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-lg-3 text-center">
                                                        <div class="card bg-light-warning no-card-border">
                                                            <div class="card-body">
                                                                <h1 class="m-0 p-0">{{$usialanjut_total_nik}}</h1>
                                                                <span>{{$usialanjut_total_nik}} NIK dari {{$usialanjut_total}} Usia Lanjut</span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-lg-3 text-center">
                                                        <div class="card bg-light-warning no-card-border">
                                                            <div class="card-body">
                                                                <h1 class="m-0 p-0">{{$hipertensi_total}}</h1>
                                                                <span>{{$hipertensi_total_nik}} NIK dari {{$hipertensi_total}} SPM Hipertensi</span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-lg-3 text-center">
                                                        <div class="card bg-light-danger no-card-border">
                                                            <div class="card-body">
                                                                <h1 class="m-0 p-0">{{$dm_total}}</h1>
                                                                <span>{{$dm_total_nik}} NIK dari {{$dm_total}} SPM DM</span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-lg-3 text-center">
                                                        <div class="card bg-light-danger no-card-border">
                                                            <div class="card-body">
                                                                <h1 class="m-0 p-0">{{$tb_total}}</h1>
                                                                <span>{{$tb_total_nik}} NIK dari {{$tb_total}} SPM Tuberkulosis</span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-lg-3 text-center">
                                                        <div class="card bg-light-danger no-card-border">
                                                            <div class="card-body">
                                                                <h1 class="m-0 p-0">{{$odgj_total}}</h1>
                                                                <span>{{$odgj_total}} NIK dari {{$odgj_total}} SPM ODGJ</span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-lg-3 text-center">
                                                        <div class="card bg-light-danger no-card-border">
                                                            <div class="card-body">
                                                                <h1 class="m-0 p-0">{{$hiv_total}}</h1>
                                                                <span>{{$hiv_total}} NIK dari {{$hiv_total}} SPM berisiko HIV</span> 
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-lg-4 text-center">
                                        <div class="card bg-light-info no-card-border">
                                            <div class="card-body">
                                                <h1 class="m-0 p-0">{{ $warga_total_nik}}</h1>
                                                <span>Total Masyarakat</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-4 text-center">
                                        <div class="card bg-light-success no-card-border">
                                            <div class="card-body">
                                                <h1 class="m-0 p-0">{{ $warga_spm}}</h1>
                                                <span>Masyarakat terdaftar SPM</span> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-4 text-center">
                                        <div class="card bg-light-warning no-card-border">
                                            <div class="card-body">
                                                <h1 class="m-0 p-0">{{ $warga_belum_spm}}</h1>
                                                <span>Masyarakat belum terdaftar SPM</span>    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table bg-white table-bordered nowrap display">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>NIK</th>
                                                <th>Nama Lengkap</th>
                                                <th>TTL</th>
                                                <th>L/P</th>
                                                <th>Usia</th>
                                                <th>Layanan SPM</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $no=1; @endphp
                                            @foreach($warga_all as $i => $warga)
                                            <tr>
                                                <td>
                                                    {{ $no++}} 
                                                </td>
                                                <td>
                                                    {{ $warga['nik']}}
                                                </td>
                                                <td>
                                                    {{ $warga['nama']}}</a>
                                                </td>
                                                <td>{{ $warga['tempat_lahir']}} <br> <small class="text-muted">{{date('d M Y',strtotime($warga['tgl_lahir']))}} </small></td>
                                                <td>{{ $warga['jenis_kelamin']}}</td>
                                                <td>
                                                    @if($usia[$i]['years'] ==0)
                                                        @if($usia[$i]['months'] == 0)
                                                            {{ $usia[$i]['days'] }} Hari
                                                            <br> <small class="text-muted">{{ $usia[$i]['hours'] }} Jam</small> 
                                                        @else
                                                            {{ $usia[$i]['months'] }} Bulan <br> <small class="text-muted">{{ $usia[$i]['days'] }} Hari</small> 
                                                        @endif
                                                    @else
                                                        {{ $usia[$i]['years'] }} Tahun <br> <small class="text-muted"> {{ $usia[$i]['months'] }} Bulan {{ $usia[$i]['days'] }} Hari</small> 
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($layanan_spm[$i] == "")
                                                    <span class="badge btn-xs badge-warning">Belum terdaftar</span>
                                                    @else
                                                    @foreach(explode("_",$layanan_spm[$i]) as $spm)
                                                        @if($spm != "")
                                                        <span class="badge btn-xs badge-info">{{ $spm }}</span>
                                                        @endif
                                                    @endforeach
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ url('/detail-warga/'.$warga->id )}}">
                                                        <button type="submit" class="btn btn-info btn-xs">
                                                            Detail
                                                        </button>
                                                    </a>
                                                    
                                                    <a href="{{ url('/ubah-warga/'.$warga->id)}}">
                                                        <button type="submit" class="btn btn-warning btn-xs">
                                                            Ubah
                                                        </button>
                                                    </a>

                                                    <button alt="default" class="btn btn-danger btn-xs" data-href="{{ url('/hapus-warga/'.$warga->id)}}" data-toggle="modal" data-target="#confirm-delete">
                                                        Hapus
                                                    </button>
                                                </td>
                                            </tr>
                                            @endforeach
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            Hapus
                        </div>
                        <div class="modal-body">
                            Anda yakin ingin menghapus ?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            <a class="btn btn-danger btn-ok text-white">Hapus</a>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <script src="{{asset('adminbite-10/assets/libs/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/sweetalert2/sweet-alert.init.js')}}"></script>
    <script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
    </script>    
</body>
</html>