<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="GIK">
<meta name="author" content="GIK">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('adminbite-10/assets/images/sicoc-favicon.png')}}">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/select2/dist/css/select2.min.css')}}">
<link href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/morris.js/morris.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/toastr/build/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet">
<style>
    .sidebar-item a{
        font-weight:600;
    }
    .tx-c{
        text-align:center;
    }
    .bg-y{
        background:yellow;
    }
    .sidebar-link .icon-Record{
        visibility: visible !important;
    }
</style>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.date.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.time.css')}}">
    <link type="text/css" href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
            <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}">
            <link href="{{asset('adminbite-10/assets/libs/sweetalert2/dist/sweetalert2.min.css')}}" rel="stylesheet">
<title>Detail Masyarakat</title>
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Detail Masyarakat</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Data Masyarakat dan Akun User</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Masyarakat</li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Masyarakat</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
               <div class="col-12">    
                    <div class="card">
                        <div class="row">
                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                <div class="card-body p-b-0 m-b-30">
                                    <div class="d-md-flex align-items-center">
                                        <div>
                                            <h4 class="card-title"> Detail Masyarakat </h4>
                                            <h6 class="card-subtitle"> Detail Masyarakat pada SICOC</h6>
                                        </div>
                                        <div class="ml-auto">
                                            <div class="dl">
                                                <a href="{{url('/ubah-warga/'.$warga['id'])}}">
                                                    <button type="button" class="btn btn-warning">
                                                        <i class="mdi mdi-book-open-page-variant"></i> Ubah Masyarakat
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">   
                                <div class="row">
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>NIK</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($warga['nik'] != null)
                                                <input type="text" id="nik" name="nik" value="{{$warga['nik']}}" class="form-control bg-success text-white" disabled>
                                            @else
                                                <input type="text" id="nik" name="nik" value="-" class="form-control bg-danger text-white" disabled>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>No. Kartu Keluarga</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($warga['no_kk'] != null)
                                                <input type="text" id="kk" name="kk" value="{{$warga['no_kk']}}" class="form-control bg-success text-white" disabled>
                                            @else
                                                <input type="text" id="kk" name="kk" value="-" class="form-control bg-danger text-white" disabled>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Nama Lengkap</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($warga['nama'] != null)
                                                <input type="text" name="nama" disabled value="{{$warga['nama']}}" class="form-control bg-success text-white">
                                            @else
                                                <input type="text" name="nama" disabled value="-" class="form-control bg-danger text-white">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Tempat Lahir</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($warga['tempat_lahir'] != null)
                                                <input type="text" name="tempat_lahir" disabled value="{{$warga['tempat_lahir']}}" class="form-control bg-success text-white">
                                            @else
                                                <input type="text" name="tempat_lahir" disabled value="-" class="form-control bg-danger text-white">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Tanggal Lahir</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-timer"></i></span>
                                            </div>
                                            @if($warga['tgl_lahir'] != null)
                                                <input type="text" name="tgl_lahir" disabled value="{{date('d F, Y', strtotime($warga['tgl_lahir']))}}" class="form-control bg-success text-white">
                                            @else
                                                <input type="text" name="tgl_lahir" disabled value="-" class="form-control bg-danger text-white">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Waktu Lahir</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-timer"></i></span>
                                            </div>
                                            @if($warga['tgl_lahir'] != null)
                                                <input type="text" id="timepicker" name="waktu_lahir" disabled value="{{date('h:i', strtotime($warga['tgl_lahir']))}}" class="form-control bg-success text-white">
                                            @else
                                                <input type="text" id="timepicker" name="waktu_lahir" disabled value="-" class="form-control bg-danger text-white">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Jenis Kelamin</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($warga['jenis_kelamin'] == "L" || $warga['jenis_kelamin'] == "P")
                                                <select name="jenis_kelamin" id="jenis_kelamin" disabled class="form-control bg-success text-white" >
                                                @if($warga['jenis_kelamin'] == "L" )
                                                <option value="L" selected>Laki Laki</option>
                                                @elseif($warga['jenis_kelamin'] == "P")
                                                <option value="P" selected>Perempuan</option>
                                                @endif
                                            @else
                                                <select name="jenis_kelamin" id="jenis_kelamin" disabled class="form-control select2 custom-select bg-danger text-white" style="width:82%;">
                                                <option value="">-</option>
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Golongan Darah</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($warga['gol_darah'] != null)
                                            <select name="gol_darah" id="gol_darah" disabled class="form-control bg-success text-white">
                                                <option value="{{$warga['gol_darah']}}" selected>{{$warga['gol_darah']}}</option>
                                            @else
                                            <select name="gol_darah" id="gol_darah" disabled class="form-control bg-danger text-white">
                                                <option value="" selected>-</option>
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Agama</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($warga['agama'] != null)
                                                <input type="text" name="agama" disabled value="{{$warga['agama']}}" class="form-control bg-success text-white">
                                            @else
                                                <input type="text" name="agama" disabled value="-" class="form-control bg-danger text-white">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-12 col-xlg-12 col-md-12">
                                        <label>Alamat</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($warga['alamat'] != null)
                                                <textarea name="alamat" id="textarea" class="form-control bg-success text-white" disabled>{{$warga['alamat']}}</textarea>
                                            @else
                                                <textarea name="alamat" id="textarea" class="form-control bg-danger text-white" disabled>-</textarea>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Kecamatan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($warga['kecamatan'] !=null)
                                            <select name="kecamatan" id="kecamatan" disabled class="form-control bg-success text-white">
                                            <option value="{{$warga['kecamatan']}}">{{$warga['kecamatan']}}</option>
                                            @else
                                            <select name="kecamatan" id="kecamatan" disabled class="form-control bg-danger text-white">
                                            <option value="">-</option>
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Kelurahan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-map-alt"></i></span>
                                            </div>
                                            @if($warga['kelurahan'] !=null)
                                            <select name="selectkelurahan" id="selectkelurahan" disabled class="form-control bg-success text-white">
                                            <option value="{{$warga['kelurahan']}}">{{$warga['kelurahan']}}</option>
                                            @else
                                            <select name="kecamatan" id="kecamatan" disabled class="form-control bg-danger text-white">
                                            <option value="">-</option>
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Kode Pos</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($warga['kode_pos'] != null)
                                                <input type="text" id="kode_pos" name="kode_pos" disabled value="{{$warga['kode_pos']}}" class="form-control bg-success text-white">
                                            @else
                                                <input type="text" id="kode_pos" name="kode_pos" disabled value="-" class="form-control bg-danger text-white">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Nomor Telepon</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($warga['no_telp'] != null)
                                                <input type="text" id="no_telp" name="no_telp" disabled value="{{$warga['no_telp']}}" class="form-control bg-success text-white">
                                            @else
                                                <input type="text" id="no_telp" name="no_telp" disabled value="-" class="form-control bg-danger text-white">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Status Perkawinan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($warga['status_perkawinan'] !=null)
                                            <select name="status_perkawinan" id="status_perkawinan" disabled class="form-control bg-success text-white">
                                            <option value="{{$warga['status_perkawinan']}}">{{$warga['status_perkawinan']}}</option>
                                            @else
                                            <select name="status_perkawinan" id="status_perkawinan" disabled class="form-control bg-danger text-white">
                                            <option value="">-</option>
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Pekerjaan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($warga['pekerjaan'] != null)
                                                <input type="text" id="pekerjaan" name="pekerjaan" disabled value="{{$warga['pekerjaan']}}" class="form-control bg-success text-white">
                                            @else
                                                <input type="text" id="pekerjaan" name="pekerjaan" disabled value="-" class="form-control bg-danger text-white">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Kewarganegaraan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            @if($warga['kewarganegaraan'] != null)
                                                <input type="text" id="kewarganegaraan" name="kewarganegaraan" disabled value="{{$warga['kewarganegaraan']}}" class="form-control bg-success text-white">
                                            @else
                                                <input type="text" id="kewarganegaraan" name="kewarganegaraan" disabled value="-" class="form-control bg-danger text-white">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <script>

        function kecChange(){
            var kec = document.getElementById("kecamatan").value;
            $('#selectkelurahan').find('option').not(':first').remove();
            $.ajax({
                type:'GET',
                url:'/getKelurahan',
                data:{kec:kec},
                dataType:'json',
                success:function(data){
                    console.log(data);
                    var len = 0;
                    if(data['data'] != null){
                        len = data['data'].length;
                    }
                    for(var i=0;i<len;i++){
                        var id = data['data'][i]['id'];
                        var nama = data['data'][i]['nama_kelurahan'];
                        var kode_pos = data['data'][i]['kode_pos'];

                        var option = "<option value='"+id+"_"+nama+"_"+kode_pos+"'>"+nama+"</option>";
                        $("#selectkelurahan").append(option);
                    }
                }
            });
        }
        function kelChange(){
            var kec = document.getElementById("kecamatan").value;
            var kel = document.getElementById("selectkelurahan").value;
            var splitkel = kel.split("_");
            document.getElementById("kelurahan").value = splitkel[1];
            document.getElementById("kode_pos").value = splitkel[2];
            
        }
    </script>
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/select2/select2.init.js')}}"></script>
    <!-- Picker Date Style -->
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.date.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.time.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/legacy.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/datetimepicker/datetimepicker.init.js')}}"></script>
    
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js')}}"></script>
    <script>
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    $('#date-fr').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', lang: 'fr', weekStart: 1, cancelText: 'ANNULER' });
    $('#date-end').bootstrapMaterialDatePicker({ weekStart: 0 });
    $('#date-start').bootstrapMaterialDatePicker({ weekStart: 0 }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });
    </script>
    <script src="{{asset('adminbite-10/assets/libs/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/sweetalert2/sweet-alert.init.js')}}"></script>
</body>
</html>