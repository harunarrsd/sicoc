<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('content.head')@include('content.main')@include('content.js')
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="GIK">
<meta name="author" content="GIK">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('adminbite-10/assets/images/sicoc-favicon.png')}}">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/select2/dist/css/select2.min.css')}}">
<link href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/morris.js/morris.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/toastr/build/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet">
<style>
    .sidebar-item a{
        font-weight:600;
    }
    .tx-c{
        text-align:center;
    }
    .bg-y{
        background:yellow;
    }
    .sidebar-link .icon-Record{
        visibility: visible !important;
    }
</style>
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('adminbite-10/assets/libs/jquery-steps/steps.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.date.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/pickadate/lib/themes/default.time.css')}}">
    <link type="text/css" href="{{asset('adminbite-10/dist/css/style.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
            <link rel="stylesheet" type="text/css" href="{{asset('adminbite-10/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}">
            <link href="{{asset('adminbite-10/assets/libs/sweetalert2/dist/sweetalert2.min.css')}}" rel="stylesheet">
<title>Tambah Masyarakat</title>
<body>
    @yield('main-preloader')
    <div id="main-wrapper">
        @yield('main-topbar')
        @yield('main-asidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Tambah Masyarakat</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Data Masyarakat dan Akun User</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Masyarakat</li>
                                    <li class="breadcrumb-item active" aria-current="page">Tambah Masyarakat</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-10">
               <div class="col-12">    
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title"> Tambah Masyarakat </h4>
                            <h6 class="card-subtitle"> Pendaftaran Masyarakat pada SICOC</h6>   
                            <form method="POST" action="{{ url('/save-warga')}}">{{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>NIK</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" id="nik" name="nik" placeholder="Masukkan NIK" maxlength="16" minlength="16" class="form-control" required 
                                            data-validation-containsnumber-regex="(\d)+" 
                                            data-validation-containsnumber-message="Masukkan NIK yang valid"
                                            data-validation-required-message="Tidak Boleh dikosongkan" onchange="nikChange();">
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>No. Kartu Keluarga</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" id="kk" name="kk" placeholder="Masukkan No. KK" maxlength="16" minlength="16" class="form-control" required 
                                            data-validation-containsnumber-regex="(\d)+" 
                                            data-validation-containsnumber-message="Masukkan No. KK yang valid"
                                            data-validation-required-message="Tidak Boleh dikosongkan" onchange="kkChange();">
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Nama Lengkap</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama Lengkap" required data-validation-required-message="Tidak Boleh dikosongkan">
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Tempat Lahir</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" name="tempat_lahir" placeholder="Masukkan Tempat Lahir" class="form-control" required data-validation-required-message="Tidak Boleh dikosongkan">
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Tanggal Lahir</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-timer"></i></span>
                                            </div>
                                            <input type="date" name="tgl_lahir" class="form-control pickadate-monyear-dropdown" required>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Waktu Lahir</label> <span class="text-danger">*</span>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-timer"></i></span>
                                            </div>
                                            <input id="timepicker"  name="waktu_lahir" class="form-control" required data-validation-required-message="Tidak Boleh dikosongkan">
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Jenis Kelamin</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="jenis_kelamin" id="jenis_kelamin" required class="form-control select2 custom-select" style="width:82%;">
                                                <option value="">Pilih</option>
                                                <option value="L">Laki Laki</option>
                                                <option value="P">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Golongan Darah</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="gol_darah" id="gol_darah" required class="form-control select2 custom-select" style="width:82%;" data-validation-required-message="Tidak Boleh dikosongkan">
                                                <option value="">Pilih</option>
                                                <option value="AB">AB</option>
                                                <option value="O">O</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Agama</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" name="agama" class="form-control" placeholder="Masukkan Agama anda" required >
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-12 col-xlg-12 col-md-12">
                                        <label>Alamat</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <textarea name="alamat" id="textarea" class="form-control" required placeholder="Masukkan Alamat Tinggal"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-6 col-xlg-6 col-md-12">
                                        <label>Kecamatan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="kecamatan" id="kecamatan" style="width:92%;" required class="form-control select2 custom-select" onchange="kecChange();">
                                                <option value="">Pilih</option>
                                                <option value="Beji">Beji</option>
                                                <option value="Bojongsari">Bojongsari</option>
                                                <option value="Cilodong">Cilodong</option>
                                                <option value="Cimanggis">Cimanggis</option>
                                                <option value="Cinere">Cinere</option>
                                                <option value="Cipayung">Cipayung</option>
                                                <option value="Limo">Limo</option>
                                                <option value="Pancoran Mas">Pancoran Mas</option>
                                                <option value="Sawangan">Sawangan</option>
                                                <option value="Sukmajaya">Sukmajaya</option>
                                                <option value="Tapos">Tapos</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Kelurahan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon11"><i class="ti-map-alt"></i></span>
                                            </div>
                                            <select name="selectkelurahan" id="selectkelurahan" aria-invalid="true" class="form-control select2 custom-select" style="width: 82%; height:36px;" onchange="kelChange()">
                                                <option value="">Pilih</option> 
                                                 
                                            </select>
                                            <input type="text" name="kelurahan" id="kelurahan" class="form-control" style="display:none;" >
                                            <input type="text" name="created_by" id="created_by" class="form-control" value="{{ Auth::user()['name'] }}" style="display:none;" >
                                            <input type="text" name="id_created_by" id="id_created_by" class="form-control" value="{{ Auth::user()['id'] }}" style="display:none;" >                              
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Kode Pos</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" id="kode_pos" name="kode_pos" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Nomor Telepon</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" name="no_telp" placeholder="Masukkan Nomor Telepon Valid" class="form-control" required 
                                            data-validation-containsnumber-regex="(\d)+" 
                                            data-validation-containsnumber-message="Masukkan No. Telepon yang valid"
                                            data-validation-required-message="Tidak Boleh dikosongkan">
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Status Perkawinan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <select name="status_perkawinan" id="status_perkawinan" data-validation-required-message="Tidak Boleh dikosongkan" style="width:82%;" required class="form-control select2 custom-select">
                                                <option value="">Pilih</option>
                                                <option value="Belum Menikah">Belum Menikah</option>
                                                <option value="Menikah">Menikah</option>
                                                <option value="Bercerai">Bercerai</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Pekerjaan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" name="pekerjaan" placeholder="Masukkan pekerjaan anda" class="form-control" required data-validation-required-message="Tidak Boleh dikosongkan">
                                        </div>
                                    </div>
                                    <div class="form-group m-0 col-lg-3 col-xlg-3 col-md-12">
                                        <label>Kewarganegaraan</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="text" name="kewarganegaraan" placeholder="Masukkan kewarganegaraan sesuai alamat tinggal" class="form-control" required data-validation-required-message="Tidak Boleh dikosongkan">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                    <button type="submit" class="btn btn-info">Kirimkan ></button> 
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @yield('main-footer')
        </div>
    </div>
    @yield('js-ripel01')
    <script>
        function nikChange(){
            var nik = document.getElementById("nik").value;
            var list_nik = {!!json_encode($list_nik)!!};
            
            if(isNaN(nik)){
                document.getElementById("nik").value = '';
                $('#nik').attr('placeholder','NIK harus berupa Angka Seluruhnya');
            }
            else{
                if(list_nik.includes(nik)){
                    document.getElementById("nik").value = '';
                    $('#nik').attr('placeholder','NIK Sudah Terdaftar Sebelumnya');
                }else{
                    if(nik.length == 16){
                        console.log("True");
                    }
                    else{
                        document.getElementById("nik").value = '';
                        $('#nik').attr('placeholder','NIK Tidak Valid');
                    }
                }
            }            
        }
        function kkChange(){
            var kk = document.getElementById("kk").value;
            if(isNaN(kk)){
                document.getElementById("kk").value = '';
                $('#kk').attr('placeholder','No.KK harus berupa Angka Seluruhnya');
            }
            else{
                if(kk.length == 16){
                    console.log("True");
                }
                else{
                    document.getElementById("kk").value = '';
                    $('#kk').attr('placeholder','No. KK Tidak Valid');
                }
            }  
        }
        function kecChange(){
            var kec = document.getElementById("kecamatan").value;
            $('#selectkelurahan').find('option').not(':first').remove();
            $.ajax({
                type:'GET',
                url:'/getKelurahan',
                data:{kec:kec},
                dataType:'json',
                success:function(data){
                    console.log(data);
                    var len = 0;
                    if(data['data'] != null){
                        len = data['data'].length;
                    }
                    for(var i=0;i<len;i++){
                        var id = data['data'][i]['id'];
                        var nama = data['data'][i]['nama_kelurahan'];
                        var kode_pos = data['data'][i]['kode_pos'];

                        var option = "<option value='"+id+"_"+nama+"_"+kode_pos+"'>"+nama+"</option>";
                        $("#selectkelurahan").append(option);
                    }
                }
            });
        }
        function kelChange(){
            var kec = document.getElementById("kecamatan").value;
            var kel = document.getElementById("selectkelurahan").value;
            var splitkel = kel.split("_");
            document.getElementById("kelurahan").value = splitkel[1];
            document.getElementById("kode_pos").value = splitkel[2];
            
        }
    </script>
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/select2/dist/js/select2.min.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/select2/select2.init.js')}}"></script>
    <!-- Picker Date Style -->
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.date.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/picker.time.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/pickadate/lib/compressed/legacy.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('adminbite-10/dist/js/pages/forms/datetimepicker/datetimepicker.init.js')}}"></script>
    
    <script src="{{asset('adminbite-10/assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js')}}"></script>
    <script>
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    $('#date-fr').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', lang: 'fr', weekStart: 1, cancelText: 'ANNULER' });
    $('#date-end').bootstrapMaterialDatePicker({ weekStart: 0 });
    $('#date-start').bootstrapMaterialDatePicker({ weekStart: 0 }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });
    </script>
    <script src="{{asset('adminbite-10/assets/libs/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
    <script src="{{asset('adminbite-10/assets/libs/sweetalert2/sweet-alert.init.js')}}"></script>
</body>
</html>