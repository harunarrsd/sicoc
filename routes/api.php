<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/user', 'APIController@AllUser');
Route::post('/login', 'APIController@login');
Route::post('/tambahpasien', 'APIController@saveWarga');
Route::get('/datapasien', 'APIController@dataWarga');

//Auth
Route::post('/auth-login', 'APIController@AuthLogin');

//Ibu Hamil
Route::get('/data-ibu-hamil', 'APIController@dataIbuHamil');
Route::get('/dropdown-nik-tambah-ibu-hamil', 'APIController@dropdownNNIKTambahIbuHamil');
Route::post('/tambah-ibu-hamil', 'APIController@tambahIbuHamil');
Route::post('/tambah-pel-ibu-hamil', 'APIController@tambahPelayananIbuHamil');


?>