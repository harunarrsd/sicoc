<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();
Route::get('/register', 'HomeController@index')->name('home');
Route::get('/verify', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');


//Users
Route::get('/users', 'UserController@index')->name('data-user');
Route::get('/tambah-user', 'UserController@tambah')->name('tambah-user');
Route::post('/save-user', 'UserController@save');
Route::get('/detail-user/{id}', 'UserController@detail')->name('detail-user');
Route::get('/ubah-user/{id}', 'UserController@ubah')->name('ubah-user');
Route::get('/hapus-user/{id}', 'UserController@delete');
Route::post('/edit-user', 'UserController@edit');
Route::get('/findPass', 'UserController@findPass');

//Warga
Route::get('/warga', 'WargaController@index')->name('data-warga');
Route::get('/tambah-warga', 'WargaController@tambah')->name('tambah-warga');
Route::post('/save-warga', 'WargaController@save');
Route::get('/getKelurahan', 'WargaController@getKelurahan');
Route::get('/detail-warga/{id}', 'WargaController@detail')->name('detail-warga');
Route::get('/ubah-warga/{id}', 'WargaController@ubah')->name('ubah-warga');
Route::post('/edit-warga', 'WargaController@edit');
Route::get('/hapus-warga/{id}', 'WargaController@delete');
Route::get('/getDataWarga', 'WargaController@getDataWarga');

// Ibu Hamil
Route::get('/ibu-hamil', 'IbuHamilController@index');
Route::get('/data-ibu-hamil', 'IbuHamilController@data')->name('data-ibu-hamil');
Route::get('/detail-ibu-hamil/{id}', 'IbuHamilController@detail')->name('detail-ibu-hamil');
Route::get('/ubah-ibu-hamil/{id}', 'IbuHamilController@ubah')->name('ubah-ibu-hamil');
Route::get('/tambah-ibu-hamil', 'IbuHamilController@tambah')->name('tambah-ibu-hamil');
Route::post('/simpan-ibu-hamil', 'IbuHamilController@save');
Route::post('/simpanubah-ibu-hamil', 'IbuHamilController@edit');
Route::get('/hapus-ibu-hamil/{id}', 'IbuHamilController@delete');
Route::get('/pelayanan-ibu-hamil', 'IbuHamilController@indexPelayanan');
Route::get('/detail-pelayanan-ibu-hamil/{id}', 'IbuHamilController@detailPelayanan');
Route::get('/ubah-pelayanan-ibu-hamil/{id}', 'IbuHamilController@ubahPelayanan');
Route::get('/catat-ibu-hamil', 'IbuHamilController@inputnikPelayanan')->name('catat-ibu-hamil');
Route::post('/mulai-catat-ibu-hamil', 'IbuHamilController@inputPelayanan');
Route::post('/simpan-catat-ibu-hamil', 'IbuHamilController@addPelayanan');
Route::post('/simpanubah-catat-ibu-hamil', 'IbuHamilController@editPelayanan');
Route::get('/hapus-pelayanan-ibu-hamil/{id}', 'IbuHamilController@deletePelayanan');
Route::get('/findmitra', 'IbuHamilController@findmitra');
Route::get('/findtenaga', 'IbuHamilController@findtenaga');
Route::get('/findnamastk', 'IbuHamilController@findnamastk');
Route::get('/getUsiaSaatPelayanan', 'IbuHamilController@getUsiaSaatPelayanan');

//Ibu Bersalin
Route::get('/ibu-bersalin', 'IbuBersalinController@index');
Route::get('/data-ibu-bersalin', 'IbuBersalinController@data')->name('data-ibu-bersalin');
Route::get('/tambah-ibu-bersalin', 'IbuBersalinController@tambah')->name('tambah-ibu-bersalin');
Route::post('/simpantambah-ibu-bersalin', 'IbuBersalinController@save');
Route::get('/detail-ibu-bersalin/{id}', 'IbuBersalinController@detail')->name('detail-ibu-bersalin');
Route::get('/ubah-ibu-bersalin/{id}', 'IbuBersalinController@ubah')->name('ubah-ibu-bersalin');
Route::post('/simpanubah-ibu-bersalin', 'IbuBersalinController@edit');
Route::get('/hapus-ibu-bersalin/{id}', 'IbuBersalinController@delete');
Route::get('/getHpht', 'IbuBersalinController@getHpht');
Route::get('/getUsiaKehamilan', 'IbuBersalinController@getUsiaKehamilan');

//Anak Baru Lahir
Route::get('/bayi-baru-lahir', 'BayiBaruLahirController@index');
Route::get('/data-bayi-baru-lahir', 'BayiBaruLahirController@data')->name('data-bayi-baru-lahir');
Route::get('/tambah-bayi-baru-lahir', 'BayiBaruLahirController@tambah')->name('tambah-bayi-baru-lahir');
Route::get('/detail-bayi-baru-lahir/{id}', 'BayiBaruLahirController@detail')->name('detail-bayi-baru-lahir');
Route::get('/ubah-bayi-baru-lahir/{id}', 'BayiBaruLahirController@ubah')->name('detail-bayi-baru-lahir');
Route::post('/edit-bayi-baru-lahir', 'BayiBaruLahirController@edit');
Route::post('/simpantambah-bayi-baru-lahir', 'BayiBaruLahirController@save');
Route::get('/catat-bayi-baru-lahir', 'BayiBaruLahirController@inputnikPelayanan');
Route::post('/mulai-catat-bayi-baru-lahir', 'BayiBaruLahirController@inputPelayanan');
Route::post('/simpan-catat-bayi-baru-lahir', 'BayiBaruLahirController@addPelayanan');
Route::get('/pelayanan-bayi-baru-lahir', 'BayiBaruLahirController@indexPelayanan');
Route::get('/detail-pelayanan-bayi-baru-lahir/{id}', 'BayiBaruLahirController@detailPelayanan');
Route::get('/ubah-pelayanan-bayi-baru-lahir/{id}', 'BayiBaruLahirController@ubahPelayanan');
Route::post('/edit-pelayanan-bayi-baru-lahir', 'BayiBaruLahirController@editPelayanan');
Route::get('/hapus-bayi-baru-lahir/{id}', 'BayiBaruLahirController@delete');

//Balita 
Route::get('/balita', 'BalitaController@index');
Route::get('/data-balita', 'BalitaController@data')->name('data-balita');
Route::get('/tambah-balita', 'BalitaController@tambah')->name('tambah-balita');
Route::post('/save-balita', 'BalitaController@save');
Route::get('/detail-balita/{id}', 'BalitaController@detail')->name('detail-balita');
Route::get('/ubah-balita/{id}', 'BalitaController@ubah')->name('ubah-balita');
Route::post('/edit-balita', 'BalitaController@edit');
Route::get('/pelayanan-balita', 'BalitaController@indexPelayanan');
Route::get('/catat-balita', 'BalitaController@inputnikPelayanan');
Route::post('/mulai-catat-balita', 'BalitaController@inputPelayanan');
Route::post('/simpan-catat-balita', 'BalitaController@addPelayanan');
Route::get('/detail-pelayanan-balita/{id}', 'BalitaController@detailPelayanan');
Route::get('/ubah-pelayanan-balita/{id}', 'BalitaController@ubahPelayanan');
Route::post('/edit-pelayanan-balita', 'BalitaController@editPelayanan');
Route::get('/hapus-balita/{id}', 'BalitaController@delete');
Route::get('/hapus-pelayanan-balita/{id}', 'BalitaController@delete_pelayanan');


//Usia Pend Dasar 
Route::get('/usia-pend-dasar', 'UsiaPendidikanDasarController@index');
Route::get('/data-usia-pend-dasar', 'UsiaPendidikanDasarController@data')->name('data-usia-pend-dasar');
Route::get('/detail-usia-pend-dasar/{id}', 'UsiaPendidikanDasarController@detail')->name('detail-usia-pend-dasar');
Route::get('/ubah-usia-pend-dasar/{id}', 'UsiaPendidikanDasarController@ubah')->name('ubah-usia-pend-dasar');
Route::get('/tambah-usia-pend-dasar', 'UsiaPendidikanDasarController@tambah')->name('tambah-usia-pend-dasar');
Route::post('/save-usia-pend-dasar', 'UsiaPendidikanDasarController@save');
Route::post('/edit-usia-pend-dasar', 'UsiaPendidikanDasarController@edit');
Route::get('/pelayanan-usia-pend-dasar', 'UsiaPendidikanDasarController@indexPelayanan');
Route::get('/catat-usia-pend-dasar', 'UsiaPendidikanDasarController@inputnikPelayanan');
Route::post('/mulai-catat-usia-pend-dasar', 'UsiaPendidikanDasarController@inputPelayanan');
Route::get('/detail-pelayanan-usia-pend-dasar/{id}', 'UsiaPendidikanDasarController@detailPelayanan');
Route::get('/ubah-pelayanan-usia-pend-dasar/{id}', 'UsiaPendidikanDasarController@ubahPelayanan');
Route::post('/edit-pelayanan-usia-pend-dasar', 'UsiaPendidikanDasarController@editPelayanan');
Route::post('/save-pelayanan-usia-pend-dasar', 'UsiaPendidikanDasarController@savePelayanan');
Route::get('/hapus-usia-pend-dasar/{id}', 'UsiaPendidikanDasarController@delete');
Route::get('/hapus-pelayanan-usia-pend-dasar/{id}', 'UsiaPendidikanDasarController@delete_pelayanan');


//Usia Produktif
Route::get('/usia-prod', 'UsiaProduktifController@index');
Route::get('/data-usia-prod', 'UsiaProduktifController@data')->name('data-usia-prod');
Route::get('/tambah-usia-prod', 'UsiaProduktifController@tambah')->name('tambah-usia-prod');
Route::post('/save-usia-prod', 'UsiaProduktifController@save');
Route::get('/detail-usia-prod/{id}', 'UsiaProduktifController@detail')->name('detail-usia-prod');
Route::get('/ubah-usia-prod/{id}', 'UsiaProduktifController@ubah')->name('ubah-usia-prod');
Route::post('/edit-usia-prod', 'UsiaProduktifController@edit');
Route::get('/pelayanan-usia-prod', 'UsiaProduktifController@indexPelayanan');
Route::get('/catat-usia-prod', 'UsiaProduktifController@inputnikPelayanan');
Route::post('/mulai-catat-usia-prod', 'UsiaProduktifController@inputPelayanan');
Route::get('/detail-pelayanan-usia-prod/{id}', 'UsiaProduktifController@detailPelayanan');
Route::get('/ubah-pelayanan-usia-prod/{id}', 'UsiaProduktifController@ubahPelayanan');
Route::post('/save-pelayanan-usia-prod', 'UsiaProduktifController@savePelayanan');
Route::post('/edit-pelayanan-usia-prod', 'UsiaProduktifController@editPelayanan');
Route::get('/hapus-usia-prod/{id}', 'UsiaProduktifController@delete');
Route::get('/hapus-pelayanan-usia-prod/{id}', 'UsiaProduktifController@delete_pelayanan');

//Usia Lanjut
Route::get('/usia-lanjut', 'UsiaLanjutController@index');
Route::get('/data-usia-lanjut', 'UsiaLanjutController@data')->name('data-usia-lanjut');
Route::get('/tambah-usia-lanjut', 'UsiaLanjutController@tambah')->name('tambah-usia-lanjut');
Route::post('/save-usia-lanjut', 'UsiaLanjutController@save');
Route::get('/detail-usia-lanjut/{id}', 'UsiaLanjutController@detail')->name('detail-usia-lanjut');
Route::get('/ubah-usia-lanjut/{id}', 'UsiaLanjutController@ubah')->name('ubah-usia-lanjut');
Route::post('/edit-usia-lanjut', 'UsiaLanjutController@edit');
Route::get('/pelayanan-usia-lanjut', 'UsiaLanjutController@indexPelayanan');
Route::get('/catat-usia-lanjut', 'UsiaLanjutController@inputnikPelayanan');
Route::post('/mulai-catat-usia-lanjut', 'UsiaLanjutController@inputPelayanan');
Route::get('/detail-pelayanan-usia-lanjut/{id}', 'UsiaLanjutController@detailPelayanan');
Route::get('/ubah-pelayanan-usia-lanjut/{id}', 'UsiaLanjutController@ubahPelayanan');
Route::post('/save-pelayanan-usia-lanjut', 'UsiaLanjutController@savePelayanan');
Route::post('/edit-pelayanan-usia-lanjut', 'UsiaLanjutController@editPelayanan');
Route::get('/hapus-usia-lanjut/{id}', 'UsiaLanjutController@delete');
Route::get('/hapus-pelayanan-usia-lanjut/{id}', 'UsiaLanjutController@delete_pelayanan');

//Penderita Hipertensi
Route::get('/penderita-hipertensi', 'HipertensiController@index');
Route::get('/data-penderita-hipertensi', 'HipertensiController@data')->name('data-penderita-hipertensi');
Route::get('/tambah-penderita-hipertensi', 'HipertensiController@tambah')->name('tambah-penderita-hipertensi');
Route::post('/save-penderita-hipertensi', 'HipertensiController@save');
Route::get('/detail-penderita-hipertensi/{id}', 'HipertensiController@detail')->name('detail-penderita-hipertensi');
Route::get('/ubah-penderita-hipertensi/{id}', 'HipertensiController@ubah')->name('ubah-penderita-hipertensi');
Route::post('/edit-penderita-hipertensi', 'HipertensiController@edit');
Route::get('/hapus-penderita-hipertensi/{id}', 'HipertensiController@delete');
Route::get('/catat-penderita-hipertensi', 'HipertensiController@inputnikPelayanan');
Route::post('/mulai-catat-penderita-hipertensi', 'HipertensiController@inputPelayanan');

//Penderita DM
Route::get('/penderita-dm', 'DMController@index');
Route::get('/data-penderita-dm', 'DMController@data')->name('data-penderita-dm');
Route::get('/tambah-penderita-dm', 'DMController@tambah')->name('tambah-penderita-dm');
Route::post('/save-penderita-dm', 'DMController@save');
Route::get('/detail-penderita-dm/{id}', 'DMController@detail')->name('detail-penderita-dm');
Route::get('/ubah-penderita-dm/{id}', 'DMController@ubah')->name('ubah-penderita-dm');
Route::post('/edit-penderita-dm', 'DMController@edit');
Route::get('/hapus-penderita-dm/{id}', 'DMController@delete');
Route::get('/catat-penderita-dm', 'DMController@inputnikPelayanan');
Route::post('/mulai-catat-penderita-dm', 'DMController@inputPelayanan');


//ODGJ
Route::get('/odgj', 'ODGJController@index');
Route::get('/data-odgj', 'ODGJController@data')->name('data-odgj');
Route::get('/tambah-odgj', 'ODGJController@tambah')->name('tambah-odgj');
Route::post('/save-odgj', 'ODGJController@save');
Route::get('/detail-odgj/{id}', 'ODGJController@detail')->name('detail-odgj');
Route::get('/ubah-odgj/{id}', 'ODGJController@ubah')->name('ubah-odgj');
Route::post('/edit-odgj', 'ODGJController@edit');
Route::get('/hapus-odgj/{id}', 'ODGJController@delete');
Route::get('/catat-odgj', 'ODGJController@inputnikPelayanan');
Route::post('/mulai-catat-odgj', 'ODGJController@inputPelayanan');

//TB
Route::get('/tb', 'TBController@index');
Route::get('/data-tb', 'TBController@data')->name('data-tb');
Route::get('/tambah-tb', 'TBController@tambah')->name('tambah-tb');
Route::post('/save-tb', 'TBController@save');


//HIV
Route::get('/hiv', 'HIVController@index');
Route::get('/data-hiv', 'HIVController@data')->name('data-hiv');
Route::get('/tambah-hiv', 'HIVController@tambah')->name('tambah-hiv');
Route::post('/save-hiv', 'HIVController@save');


